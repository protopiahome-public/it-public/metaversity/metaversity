<?php

class test_results
{
	protected $errors = array();
	protected $stat_assert_count = 0;

	public function add_error($descr, $result_data_file_name = false)
	{
		$error = array(
			"backtrace" => debug_backtrace(),
			"descr" => $descr,
			"result_data_file_name" => $result_data_file_name,
		);
		$this->errors[] = $error;
	}
	
	public function get_stat_assert_count()
	{
		return $this->stat_assert_count;
	}
	
	public function increment_assert_counter()
	{
		++$this->stat_assert_count;
	}

	public function is_error_occured()
	{
		return sizeof($this->errors) > 0;
	}

	public function get_errors_raw()
	{
		return $this->errors;
	}

	public function get_errors_html()
	{
		global $test_loader;

		// html: base
		$html = '<html><head><title>"' . htmlspecialchars($test_loader->get_file_restriction()) . '". Test files: ' . $test_loader->get_stat_file_count() . ', classes: ' . $test_loader->get_stat_class_count() . ', methods: ' . $test_loader->get_stat_method_count() . ', asserts: ' . $this->get_stat_assert_count() . '</title>';
		$html .= '<style type="text/css">
.included-files {margin: 0 0 6px;}
.included-files .header {font-weight: bold; margin-bottom: 2px;}
.red-bar {padding: 4px; margin-bottom: 10px; background: #D00; color: #fff;}
.green-bar {padding: 4px; margin-bottom: 10px; background: #080; color: #fff;}
.error {}
.error .file- {padding: 4px; background: #dfd; border: 1px solid #bdb;}
.error .descr- {padding: 4px;}
.error .more- {padding: 0 4px 4px;}
.stat {color: #666;}
</style></head><bodt>';

		// html: included files
		$included_files = $test_loader->get_included_files();
		if (sizeof($included_files))
		{
			$html .= '<div class="included-files"><div class="header">Included files</div>' . join('<br/>', $included_files) . '</div>';
		}
		
		// html: bar
		if ($this->is_error_occured())
		{
			$error_count = sizeof($this->errors);
			$errors_text = $error_count == 1 ? " error" : " errors";
			$html .= '<div class="red-bar">' . $error_count . $errors_text . '</div>';
		}
		else 
		{
			$html .= '<div class="green-bar">OK</div>';
		}
		
		// html: errors
		foreach ($this->errors as $error)
		{
			$idx = 1;
			while ((substr($error["backtrace"][$idx]["file"], -9, 9) != ".test.php") and isset($error["backtrace"][$idx + 1]))
			{
				$idx++;
			}
			//$idx--;
			//dd();
			$html .= '<div class="error">';
			$file = str_replace("\\\\", "\\", $error["backtrace"][$idx]["file"]);
			$file = str_replace("\\", "/", $file);
			$file_base = realpath(PATH_HOME);
			$file_base = str_replace("\\\\", "\\", $file_base);
			$file_base = str_replace("\\", "/", $file_base);
			//dd($file_base, $file);
			if (substr($file, 0, strlen($file_base)) === $file_base)
			{
				$file = substr($file, strlen($file_base));
			}
			$html .= '<div class="file-">' . $file . ' : ' . $error["backtrace"][$idx]["line"] . ' / ' . $error["backtrace"][$idx]["function"] . '</div>';
			$html .= '<div class="descr-">' . htmlspecialchars($error["descr"]) . '</div>';
			if ($error["result_data_file_name"])
			{
				$html .= '<div class="more-"><a href="../last_test_results_data/' . $error["result_data_file_name"] . '">More info</a></div>';
			}
			$html .= '</div>';
		}

		// html: stat
		$html .= '<hr/><div class="stat">test files: ' . $test_loader->get_stat_file_count() . ', classes: ' . $test_loader->get_stat_class_count() . ', methods: ' . $test_loader->get_stat_method_count() . ', asserts: ' . $this->get_stat_assert_count() . '</div>';

		$html .= '</body></html>';
		// return
		return $html;
	}

}

?>