<?php

class test_loader
{

	protected $file_restriction = "";

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var test_results
	 */
	protected $test_results;
	protected $stat_file_count = 0;
	protected $stat_class_count = 0;
	protected $stat_method_count = 0;
	protected $included_files = array();

	public function __construct($file_restriction)
	{
		$this->file_restriction = $file_restriction;

		global $db;
		$this->db = $db;

		global $test_results;
		$this->test_results = $test_results;
	}

	public function run()
	{
		$this->remove_old_results_data();
		$error_files_suffix = substr(md5(rand()), 0, 10) . ".html";
		
		global $config;
		global $xerror;
		/* @var $xerror xerror */
		
		$classes = get_declared_classes();

		$modules = scandir(PATH_MODULES);
		foreach ($modules as $module)
		{
			if ($module == "." || $module == "..")
			{
				continue;
			}

			$module_path = PATH_MODULES . "/" . $module;
			if (is_dir($module_path))
			{
				dir_map($module_path . "/tests", array($this, "_include_files"));
			}
		}

		$classes = array_diff(get_declared_classes(), $classes);
		foreach ($classes as $class)
		{
			if (substr($class, -5, 5) !== "_test")
			{
				continue;
			}
			$class_instance = new $class();
			/* @var $class_instance base_test */
			$this->stat_class_count++;
			$class_instance->set_up();
			$methods = get_class_methods($class);
			foreach ($methods as $method)
			{
				if (substr($method, -5, 5) == "_test")
				{
					$class_instance->$method();
					if ($xerror->is_error_occured())
					{
						$error_file_name = "{$class}-{$method}-{$error_files_suffix}";
						file_put_contents(PATH_WWW . "/last_test_results_data/{$error_file_name}", $xerror->get_errors_html());
						$this->test_results->add_error("Error occured while running {$class}::{$method}()", $error_file_name);
					}
					$xerror->clear_errors();
					$this->stat_method_count++;
				}
			}
			$class_instance->tear_down();
			if ($class_instance->get_test_db_name())
			{
				$this->db->select_db($config["test_db_name"]);
			}
		}
	}

	public function _include_files($file_name)
	{
		if (substr($file_name, -9, 9) == ".test.php" and !strpos($file_name, "/base."))
		{
			if (!$this->file_restriction or strpos($file_name, $this->file_restriction))
			{
				require_once $file_name;
				$this->stat_file_count++;
				if (isset($_GET["file"]))
				{
					$this->included_files[] = $file_name;
				}
			}
		}
	}

	public function remove_old_results_data()
	{
		$dir = PATH_WWW . "/last_test_results_data";
		foreach (scandir($dir) as $file_name)
		{
			$file_path = $dir . "/" . $file_name;
			if ($file_name[0] != "." and !is_dir($file_path))
			{
				unlink($file_path);
			}
		}
	}

	public function get_stat_file_count()
	{
		return $this->stat_file_count;
	}

	public function get_stat_class_count()
	{
		return $this->stat_class_count;
	}

	public function get_stat_method_count()
	{
		return $this->stat_method_count;
	}

	public function get_included_files()
	{
		return $this->included_files;
	}

	public function get_file_restriction()
	{
		return $this->file_restriction;
	}

}

?>