<?php

require_once PATH_CORE . "/base_http_issues.php";
require_once PATH_CORE . "/loader/base_loader.php";
require_once PATH_CORE . "/loader/ajax_loader.php";

class ajax_ctrl_tester extends base_tester
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var test_results
	 */
	protected $test_results;
	protected $ctrl_name;
	protected $is_ajax_page;

	/**
	 * @var ajax_loader 
	 */
	protected $ajax_loader;

	/**
	 * @var base_ajax_ctrl
	 */
	protected $ctrl_instance;
	protected $ctrl_data;

	public function __construct($ctrl_name, $is_ajax_page = true)
	{
		global $ajax_loader;
		global $db;

		$this->ctrl_name = $ctrl_name;
		$this->is_ajax_page = $is_ajax_page;
		$ajax_loader = $this->ajax_loader = new ajax_loader();
		$this->db = $db;

		global $test_results;
		$this->test_results = $test_results;
	}

	public function run_and_assert_data($correct_data)
	{
		$this->run();
		$this->assert_data($correct_data);
	}

	protected function run()
	{
		$class = $this->ctrl_name . ($this->is_ajax_page ? "_ajax_page" : "_ajax_ctrl");
		$this->ctrl_instance = new $class();
		$this->ajax_loader->add_ctrl($this->ctrl_instance);
		$this->ajax_loader->run();
		$this->ctrl_data = json_decode($this->ajax_loader->get_content(), true);
	}

	protected function assert_data($correct_data)
	{
		if ($correct_data !== $this->ctrl_data)
		{
			$real_data = var_export($this->ctrl_data, true);
			$correct_data = var_export($correct_data, true);
			$this->test_results->add_error("Returned data is not correct: {$real_data} !== {$correct_data}");
		}
		$this->test_results->increment_assert_counter();
	}

}

?>