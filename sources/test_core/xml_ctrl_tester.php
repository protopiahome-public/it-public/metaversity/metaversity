<?php

require_once PATH_CORE . "/base_http_issues.php";
require_once PATH_CORE . "/loader/base_loader.php";
require_once PATH_CORE . "/loader/xml_loader.php";

class xml_ctrl_tester extends base_tester
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var test_results
	 */
	protected $test_results;

	/**
	 * @var base_xml_ctrl 
	 */
	protected $xml_ctrl;

	/**
	 * @var base_xml_ctrl
	 */
	protected $ctrl_instance;
	protected $xml;

	/**
	 * @var xdom
	 */
	protected $xdom;

	public function __construct(base_xml_ctrl $xml_ctrl)
	{
		$this->xml_ctrl = $xml_ctrl;

		global $db;
		$this->db = $db;

		global $test_results;
		$this->test_results = $test_results;
	}

	public function run()
	{
		$xml_loader = new xml_loader();
		$xml_loader->add_xml($this->xml_ctrl);
		$this->xml = $xml_loader->get_xml();
		$this->xdom = xdom::create_from_string($this->xml);
		return;
	}

	public function assert_node_exists($xpath)
	{
		$nodes = $this->xdom->xpath($xpath);
		if (!sizeof($nodes))
		{

			$this->test_results->add_error("Node does not exist: {$xpath}");
		}
		$this->test_results->increment_assert_counter();
	}

	public function get_text($xpath)
	{
		$node = $this->xdom->xpath_one($xpath);
		if (!$node)
		{
			return null;
		}
		return $node->get_text();
	}

	public function get_xml()
	{
		return $this->xml;
	}

}

?>