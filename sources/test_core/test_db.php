<?php

class test_db
{

	/**
	 * Project tables ordered by 
	 */
	public static $tables = array();

	public static function init()
	{
		global $db;
		if (substr($db->get_db_name(), -5, 5) != "_test")
		{
			trigger_error("For safety of your data testing is allowed for only /_test$/ tables");
		}
		foreach (array_reverse(self::$tables) as $table => $inserts)
		{
			$db->sql("TRUNCATE TABLE {$table}");
		}
		foreach (self::$tables as $table => $inserts)
		{
			foreach ($inserts as $insert)
			{
				if ($insert != "")
				{
					$db->sql($insert);
				}
			}
		}
	}

}

?>