<?php

class base_test
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var mcache
	 */
	protected $mcache;

	/**
	 * @var user
	 */
	protected $user;
	protected $test_db_name;

	/**
	 * @var test_results
	 */
	protected $test_results;

	public function __construct()
	{
		global $db;
		$this->db = $db;

		global $mcache;
		$this->mcache = $mcache;

		global $user;
		$this->user = $user;

		global $test_results;
		$this->test_results = $test_results;
	}

	public function set_up()
	{
		
	}

	public function tear_down()
	{
		
	}

	public function set_user_admin()
	{
		$this->user->set_user_id(1);
		$this->user->set_user_data(array(
			"id" => "1",
			"login" => "login",
			"is_admin" => "1",
		));
	}

	public function set_user_guest()
	{
		$this->user->set_user_id(0);
		$this->user->set_user_data(array());
	}

	public function get_test_db_name()
	{
		return $this->test_db_name;
	}

	protected function select_db($db_suffix)
	{
		global $config;
		$this->db->select_db($config["test_db_name"] . "_" . $db_suffix);
		$this->test_db_name = $db_suffix;
	}

	protected function assert_true($real_value)
	{
		if ($real_value !== true)
		{
			$real_value = var_export($real_value, true);
			$this->test_results->add_error("The value is not true: {$real_value}");
		}
		$this->test_results->increment_assert_counter();
	}

	protected function assert_false($real_value)
	{
		if ($real_value !== false)
		{
			$real_value = var_export($real_value, true);
			$this->test_results->add_error("The value is not false: {$real_value}");
		}
		$this->test_results->increment_assert_counter();
	}

	protected function assert_null($real_value)
	{
		if (!is_null($real_value))
		{
			$real_value = var_export($real_value, true);
			$this->test_results->add_error("The value is not NULL: {$real_value}");
		}
		$this->test_results->increment_assert_counter();
	}

	protected function assert_fail($message)
	{
		$this->test_results->add_error("FAIL: " . $message);
		$this->test_results->increment_assert_counter();
	}

	protected function assert_equal($real_value, $correct_value)
	{
		if ($real_value != $correct_value)
		{
			$real_value = var_export($real_value, true);
			$correct_value = var_export($correct_value, true);
			$this->test_results->add_error("Values are not equal: {$real_value} != {$correct_value}");
		}
		$this->test_results->increment_assert_counter();
	}

	protected function assert_identical($real_value, $correct_value)
	{
		if ($real_value !== $correct_value)
		{
			//$real_value_type = gettype($real_value);
			//$correct_value_type = gettype($correct_value);
			$real_value = var_export($real_value, true);
			$correct_value = var_export($correct_value, true);
			//$this->test_results->add_error("Values are not identical: {$real_value_type}[{$real_value}] vs. {$correct_value_type}[{$correct_value}]");
			$this->test_results->add_error("Values are not identical: {$real_value} !== {$correct_value}");
		}
		$this->test_results->increment_assert_counter();
	}

	protected function assert_db_row_count($table_name, $row_count, $sql_end = "")
	{
		$allow = $this->db->test_get_allow_queries_beyond_transaction();
		$this->db->test_set_allow_queries_beyond_transaction(true);
		$sql = "SELECT COUNT(*) FROM {$table_name} {$sql_end}";
		$real_row_count = $this->db->get_value($sql);
		if ($real_row_count != $row_count)
		{
			$real_row_count = var_export($real_row_count, true);
			$row_count = var_export($row_count, true);
			$this->test_results->add_error("DB: Incorrect row count: {$real_row_count} != {$row_count} (table {$table_name}" . ($sql_end ? ", " . $sql_end : "") . ")");
		}
		$this->db->test_set_allow_queries_beyond_transaction($allow);
		$this->test_results->increment_assert_counter();
	}

	protected function assert_db_row_count_more_or_equal_than($table_name, $row_count, $sql_end = "")
	{
		$allow = $this->db->test_get_allow_queries_beyond_transaction();
		$this->db->test_set_allow_queries_beyond_transaction(true);
		$sql = "SELECT COUNT(*) FROM {$table_name} {$sql_end}";
		$real_row_count = $this->db->get_value($sql);
		if ($real_row_count < $row_count)
		{
			$real_row_count = var_export($real_row_count, true);
			$row_count = var_export($row_count, true);
			$this->test_results->add_error("DB: Incorrect row count: {$real_row_count} < {$row_count} (table {$table_name}" . ($sql_end ? ", " . $sql_end : "") . ")");
		}
		$this->db->test_set_allow_queries_beyond_transaction($allow);
		$this->test_results->increment_assert_counter();
	}

	protected function assert_db_value($table_name, $column_name, $pk, $correct_value, $pk_column_name = "id")
	{
		$allow = $this->db->test_get_allow_queries_beyond_transaction();
		$this->db->test_set_allow_queries_beyond_transaction(true);
		$real_value = $this->db->get_value("SELECT `{$column_name}` FROM `{$table_name}` WHERE {$pk_column_name} = {$pk}");
		if ($real_value != $correct_value)
		{
			$this->test_results->add_error("DB: Real value != Correct value: [{$real_value}] != [{$correct_value}]");
		}
		$this->db->test_set_allow_queries_beyond_transaction($allow);
		$this->test_results->increment_assert_counter();
	}

	protected function assert_db_row_exists($sql)
	{
		$allow = $this->db->test_get_allow_queries_beyond_transaction();
		$this->db->test_set_allow_queries_beyond_transaction(true);
		if (!$this->db->row_exists($sql))
		{
			$this->test_results->add_error("DB: Row does not exist: [{$sql}]");
		}
		$this->db->test_set_allow_queries_beyond_transaction($allow);
		$this->test_results->increment_assert_counter();
	}

	protected function assert_db_row_not_exist($sql)
	{
		$allow = $this->db->test_get_allow_queries_beyond_transaction();
		$this->db->test_set_allow_queries_beyond_transaction(true);
		if ($this->db->row_exists($sql))
		{
			$this->test_results->add_error("DB: Row(s) exist: [{$sql}]");
		}
		$this->db->test_set_allow_queries_beyond_transaction($allow);
		$this->test_results->increment_assert_counter();
	}

	protected function assert_db_table_exists($table_name)
	{
		$allow = $this->db->test_get_allow_queries_beyond_transaction();
		$this->db->test_set_allow_queries_beyond_transaction(true);
		$table_name_quoted = $this->db->escape($table_name);
		if (!$this->db->row_exists("SHOW TABLES LIKE '{$table_name_quoted}'"))
		{
			$this->test_results->add_error("DB: Table does not exist: [{$table_name}]");
		}
		$this->db->test_set_allow_queries_beyond_transaction($allow);
		$this->test_results->increment_assert_counter();
	}

	protected function assert_db_table_not_exist($table_name)
	{
		$allow = $this->db->test_get_allow_queries_beyond_transaction();
		$this->db->test_set_allow_queries_beyond_transaction(true);
		$table_name_quoted = $this->db->escape($table_name);
		if ($this->db->row_exists("SHOW TABLES LIKE '{$table_name_quoted}'"))
		{
			$this->test_results->add_error("DB: Table exists: [{$table_name}]");
		}
		$this->db->test_set_allow_queries_beyond_transaction($allow);
		$this->test_results->increment_assert_counter();
	}

}

?>