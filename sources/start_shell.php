<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
$global_start_time = microtime(true);

if ($_SERVER["argc"] < 2)
{
	die("Need 2 arguments:\n1: 1 (DEV_HOST) or 2 (PRODUCTION_HOST)\n2: Name of the host\n");
}

if ($_SERVER["argv"][1] == "1")
{
	$_SERVER["DEV_HOST"] = $_SERVER["argv"][2];
}
elseif ($_SERVER["argv"][1] == "2")
{
	$_SERVER["PRODUCTION_HOST"] = $_SERVER["argv"][2];
}
else
{
	die("Need 2 arguments:\n1: 1 (DEV_HOST) or 2 (PRODUCTION_HOST)\n2: Name of the host\n");
}

require_once PATH_CONFIG . "/_global_conf.php";

require_once PATH_INTCMF . "/auxil.php";
require_once PATH_SOURCES . "/autoload.php";
require_once PATH_SOURCES . "/init_shell.php";

//=== lang / translations ===//
require_once PATH_INTCMF . "/lang.php";
$lang = new lang("ru");

function parse_params($params)
{
	$config = array();
	$used_param_indices = array();
	// Help?
	foreach ($params as $param_name => $param_settings)
	{
		if (isset($_SERVER["SERVER_PROTOCOL"]))
		{
			switch ($param_settings["type"])
			{
				case "HELP":
					foreach ($param_settings["names"] as $name)
					{
						if (isset($_GET[$name]))
						{
							return array($param_name => 1);
						}
					}
					break;
			}
		}
		else
		{
			switch ($param_settings["type"])
			{
				case "HELP":
					foreach ($param_settings["names"] as $name)
					{
						for ($i = 1; $i < sizeof($_SERVER["argv"]); $i++)
						{
							$param = $_SERVER["argv"][$i];
							if ($name == $param)
							{
								return array($param_name => 1);
							}
						}
					}
					break;
			}
		}
	}
	// Retreiving params
	foreach ($params as $param_name => $param_settings)
	{
		if (isset($_SERVER["SERVER_PROTOCOL"]))
		{
			switch ($param_settings["type"])
			{
				case "HELP":
					break;

				case "EXISTS":
				case "VALUE":
				case "DEFAULT":
					foreach ($param_settings["names"] as $name)
					{
						if (isset($_GET[$name]))
						{
							$config[$param_name] = $_GET[$name];
						}
					}
					break;

				default:
					die("Error: Unknown param type ({$param_settings["type"]})\n");
					break;
			}
		}
		else
		{
			switch ($param_settings["type"])
			{
				case "HELP":
					break;

				case "EXISTS":
					foreach ($param_settings["names"] as $name)
					{
						for ($i = 1; $i < sizeof($_SERVER["argv"]); $i++)
						{
							$param = $_SERVER["argv"][$i];
							if ($name == $param)
							{
								$config[$param_name] = 1;
								$used_param_indices[$i] = true;
								break 2;
							}
						}
					}
					break;
				case "VALUE":
					foreach ($param_settings["names"] as $name)
					{
						for ($i = 1; $i < sizeof($_SERVER["argv"]); $i++)
						{
							$param = $_SERVER["argv"][$i];
							if ($name == $param)
							{
								if (isset($_SERVER["argv"][$i + 1]))
								{
									$config[$param_name] = $_SERVER["argv"][$i + 1];
								}
								$used_param_indices[$i] = true;
								$used_param_indices[$i + 1] = true;
								break 2;
							}
							elseif (substr($param, 0, strlen($name) + 1) == $name . "=")
							{
								$config[$param_name] = substr($param, strlen($name) + 1);
								$used_param_indices[$i] = true;
								break 2;
							}
						}
					}
					break;

				case "DEFAULT":
					break;

				default:
					die("Error: Unknown param type ({$param_settings["type"]})\n");
					break;
			}
		}
	}
	// Default parameter
	if (!isset($_SERVER["SERVER_PROTOCOL"]))
	{
		foreach ($params as $param_name => $param_settings)
		{
			if ($param_settings["type"] == "DEFAULT" and ! isset($config[$param_name]))
			{
				$ok = false;
				for ($i = 1; $i < sizeof($_SERVER["argv"]); $i++)
				{
					if (!isset($used_param_indices[$i]))
					{
						$config[$param_name] = $_SERVER["argv"][$i];
						$used_param_indices[$i] = true;
						$ok = true;
						break;
					}
				}
				if (!$ok)
				{
					die("Error: Incorrect input format. Type {$_SERVER["argv"][0]} --help\n");
				}
			}
		}
		for ($i = 1; $i < sizeof($_SERVER["argv"]); $i++)
		{
			if (!isset($used_param_indices[$i]))
			{
				die("Error: Incorrect input format. Type {$_SERVER["argv"][0]} --help\n");
			}
		}
	}
	// Checking values
	foreach ($params as $param_name => $param_settings)
	{
		switch ($param_settings["type"])
		{
			case "HELP":
			case "EXISTS":
				if (isset($config[$param_name]))
				{
					$config[$param_name] = $config[$param_name] === "" ? true : ($config[$param_name] ? true : false);
				}
				else
				{
					$config[$param_name] = false;
				}
				break;

			case "VALUE":
			case "DEFAULT":
				if (isset($param_settings["strtoupper"]) and isset($config[$param_name]))
				{
					$config[$param_name] = strtoupper($config[$param_name]);
				}
				if (isset($param_settings["strtolower"]) and isset($config[$param_name]))
				{
					$config[$param_name] = strtolower($config[$param_name]);
				}
				if (isset($config[$param_name]))
				{
					if (isset($param_settings["values"]))
					{
						$ok = false;
						foreach ($param_settings["values"] as $param_value => $allowed_values)
						{
							if (in_array($config[$param_name], $allowed_values))
							{
								$config[$param_name] = $param_value;
								$ok = true;
								break;
							}
						}
						if (!$ok)
						{
							die("Error: Value '{$config[$param_name]}' is not allowed for the parameter {$param_name}\n");
						}
					}
				}
				elseif (isset($param_settings["default"]))
				{
					$config[$param_name] = $param_settings["default"];
				}
				else
				{
					die("Error: No value for the parameter '{$param_name}'\n");
				}
				break;
		}
	}
	return $config;
}

?>