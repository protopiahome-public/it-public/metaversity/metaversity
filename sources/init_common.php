<?php

//=== localization ===//
setlocale(LC_ALL, "ru_RU.UTF-8", "rus_rus");
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
date_default_timezone_set("Europe/Moscow");

//=== error ===//
require_once PATH_INTCMF . "/xerror.php";
$xerror = new xerror(PATH_SOURCES, PATH_HOME, PATH_INTCMF . "/xerror.xslt", "UTF-8", $config["debug_ip"], PATH_LOG . "/errors", "print_errors_function", $config["errors_email"]);

//=== db ===//
require_once PATH_INTCMF . "/db.php";
require_once PATH_INTCMF . "/select_sql.php";
$db = new db($config["db_host"], $config["db_user"], $config["db_pass"], $config["db_name"], isset($config["db_time_zone"]) ? $config["db_time_zone"] : "");
//$db->sql("SET max_allowed_packet = 32 * 1024 * 1024");
if (isset($_GET["sql"]))
{
	if ($xerror->is_debug_mode())
	{
		$db->set_debug_mode(true);
	}
}

//=== memcache ===//
require_once PATH_INTCMF . "/mcache.php";
$mcache = new mcache($config["memcache_host"], $config["memcache_port"]);
$mcache->set_prefix($config["memcache_prefix"]);

//=== cache ===//
require_once PATH_CORE . "/cache.php";
require_once PATH_CORE . "/cache_tag.php";
$tag = new cache_tag();
$cache = new cache();

//=== xml classes ===//
require_once PATH_INTCMF . "/xnode.php";
require_once PATH_INTCMF . "/xdom.php";
?>