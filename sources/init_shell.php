<?php

function print_errors_function()
{
	global $xerror;
	/* @var $xerror xerror */
	if ($xerror->is_debug_mode())
	{
		$html = $xerror->get_errors_html();
		$matches = null;
		if (preg_match("/<!--(.*?)-->/s", $html, $matches))
		{
			echo $matches[1];
		}
		else
		{
			echo $html;
			echo "Error occured (error html can not be parsed)\n";
		}
	}
	else
	{
		echo "Error occured\n";
	}
}

require_once PATH_SOURCES . "/init_common.php";
?>