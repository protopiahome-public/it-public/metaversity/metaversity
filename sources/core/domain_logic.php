<?php

class domain_logic
{
	/**
	 * @var request
	 */
	protected $request;

	/**
	 * @var db
	 */
	protected $db;
	protected $is_on;
	protected $main_host_name;
	protected $main_host_prefix;
	protected $host;
	protected $is_main_host_subdomain;
	protected $current_domain_stream_id = false;

	public function __construct()
	{
		global $request;
		$this->request = $request;

		global $db;
		$this->db = $db;

		global $config;
		$this->is_on = $config["main_host_name"] ? true : false;
		$this->main_host_name = $this->is_on ? $config["main_host_name"] : false;
		$this->main_host_prefix = $config["main_host_prefix"];
		$this->host = $this->request->get_host();
		$this->is_main_host_subdomain = str_ends($this->host, "." . $this->main_host_name);
	}

	public function is_on()
	{
		return $this->is_on;
	}

	public function get_main_host_display_name()
	{
		return $this->is_on ? $this->main_host_name : $this->host;
	}

	public function get_main_prefix($no_protocol = false)
	{
		if ($this->is_on)
		{
			return ($no_protocol ? "//" : $this->request->get_protocol(true)) . $this->main_host_name . $this->main_host_prefix;
		}
		else
		{
			return $this->request->get_full_prefix();
		}
	}

	public function is_main_host()
	{
		if (!$this->is_on)
		{
			return null;
		}
		return $this->host == $this->main_host_name;
	}

	public function is_main_host_subdomain($host = null)
	{
		return $host ? str_ends($host, "." . $this->main_host_name) : $this->is_main_host_subdomain;
	}

	public function get_streams_prefix($no_protocol = false)
	{
		if ($this->is_on)
		{
			return ($no_protocol ? "//" : $this->request->get_protocol(true)) . $this->main_host_name . "/streams";
		}
		else
		{
			return $this->request->get_full_prefix() . "/streams";
		}
	}

	public function get_users_host()
	{
		return "users." . $this->main_host_name;
	}

	public function get_users_prefix($no_protocol = false)
	{
		if ($this->is_on)
		{
			return ($no_protocol ? "//" : $this->request->get_protocol(true)) . $this->get_users_host();
		}
		else
		{
			return $this->request->get_full_prefix() . "/users";
		}
	}

	public function is_users_host()
	{
		if (!$this->is_on)
		{
			return null;
		}
		return $this->host == $this->get_users_host();
	}

	public function get_students_host()
	{
		return "students." . $this->main_host_name;
	}

	public function get_students_prefix()
	{
		if ($this->is_on)
		{
			return $this->request->get_protocol(true) . $this->get_students_host();
		}
		else
		{
			return $this->request->get_full_prefix() . "/students";
		}
	}

	public function is_students_host()
	{
		if (!$this->is_on)
		{
			return null;
		}
		return $this->host == $this->get_students_host();
	}

	public function is_special_subdomain()
	{
		return $this->is_users_host() || $this->is_students_host();
	}

	// @todo remove
	public function get_tests_host()
	{
		return "tests." . $this->main_host_name;
	}

	// @todo remove
	public function is_tests_host()
	{
		if (!$this->is_on)
		{
			return null;
		}
		return $this->host == $this->get_tests_host();
	}

	// @todo remove
	public function is_self_authed_subdomain()
	{
		return $this->is_tests_host();
	}

	public function get_current_domain_stream_id()
	{
		if ($this->current_domain_stream_id === false)
		{
			$this->current_domain_stream_id = self::get_stream_id_by_domain_name($this->host);
		}
		return $this->current_domain_stream_id;
	}

	public function is_current_domain_stream_domain($stream_id)
	{
		if (!$this->is_on)
		{
			return false;
		}

		return $stream_id == $this->get_current_domain_stream_id();
	}

	public function get_stream_id_by_domain_name($domain_name)
	{
		$domain_info = domain_parser_helper::get_domain_info($domain_name, $this->main_host_name);
		return $domain_info && $domain_info["type"] === "stream" ? $domain_info["id"] : null;
	}

	public function is_www_subdomain()
	{
		if (!$this->is_on)
		{
			return false;
		}
		if ($this->is_main_host_subdomain and str_begins($this->host, "www."))
		{
			return true;
		}
		return false;
	}

	public function get_cookie_domain()
	{
		if ($this->is_on)
		{
			return "." . $this->main_host_name;
		}
		else
		{
			return "";
		}
	}

	public function get_cookie_path()
	{
		if ($this->is_on)
		{
			return $this->host == $this->main_host_name ? $this->main_host_prefix . "/" : "/";
		}
		else
		{
			return $this->request->get_prefix() . "/";
		}
	}

}

?>