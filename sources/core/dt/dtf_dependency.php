<?php

class dtf_dependency
{

	private $inspected_column;
	private $activation_value;
	private $actions;

	public function __construct($inspected_column, $activation_value, $actions)
	{
		$this->inspected_column = $inspected_column;
		$this->activation_value = $activation_value;
		$this->actions = $actions;
	}

	public function get_inspected_column()
	{
		return $this->inspected_column;
	}

	public function get_activation_value()
	{
		return $this->activation_value;
	}

	public function get_actions()
	{
		return $this->actions;
	}

}

?>