<?php

class db_xml_converter
{

	/**
	 * @var db
	 */
	protected $db;

	public function __construct()
	{
		global $db;
		$this->db = $db;
	}

	public function build_xml(xnode $parent_node, $db_result, $data_one_row, $row_name = null, $auxil_data = array(), $column_type_array = array())
	{
		if (empty($db_result))
		{
			return;
		}
		$row = null;
		if ($this->db->is_db_result($db_result))
		{
			$row = $this->db->fetch_array($db_result);
		}
		else
		{
			$row = reset($db_result);
		}
		while ($row)
		{
			if ($data_one_row)
			{
				$row_node = $parent_node;
			}
			else
			{
				$row_node = $parent_node->create_child_node($row_name);
			}
			$remove_columns = array();
			foreach ($row as $column_name => $value)
			{
				if (isset($remove_columns[$column_name]))
				{
					continue;
				}
				$type = db_xml_type_helper::get_type($column_name, $remove_columns, $column_type_array);
				switch ($type)
				{
					case "node":
						$row_node->create_child_node($column_name, $value);
						break;

					case "xhtml":
						if ($value)
						{
							$row_node->import_xdom(xdom::create_from_string("<{$column_name}><div class=\"xhtml\">{$value}</div></{$column_name}>"));
						}
						else
						{
							$row_node->create_child_node($column_name);
						}
						break;

					case "xhtml_edit":
						$row_node->create_child_node($column_name, $value);
						break;
						
					case "url_name":
						{
							$url = isset($auxil_data[$column_name . "_prefix"]) ? $auxil_data[$column_name . "_prefix"] . "/" : "";
							if (isset($row["url_name_prev"]))
							{
								$url .= $row["url_name_prev"] . "/";
								$row_node->set_attr("url_prev", $url);
							}
							if ($value)
							{
								$url .= $value . "/";
							}
							$row_node->set_attr("url", $url);
						}
						break;

					case "image_simple":
						if ($value)
						{
							$width = $value;
							$height = $row[$column_name . "_height"];
							$scale_factor = false;
							if (isset($auxil_data[$column_name . "_scale_max_width"]) && $auxil_data[$column_name . "_scale_max_width"] < $width)
							{
								$scale_factor = $auxil_data[$column_name . "_scale_max_width"] / $width;
							}
							if (isset($auxil_data[$column_name . "_scale_max_height"]) && $auxil_data[$column_name . "_scale_max_height"] < $height)
							{
								$scale_factor_2 = $auxil_data[$column_name . "_scale_max_height"] / $height;
								if ($scale_factor === false or $scale_factor_2 < $scale_factor)
								{
									$scale_factor = $scale_factor_2;
								}
							}
							if ($scale_factor !== false)
							{
								$width = round($width * $scale_factor);
								$height = round($height * $scale_factor);
							}
							$node = $row_node->create_child_node($column_name)
							->set_attr("is_uploaded", "1")
							->set_attr("width", $width)
							->set_attr("height", $height);
							$url_suffix = "";
							if (isset($row[$column_name . "_version"]))
							{
								$url_suffix = "?version=" . $row[$column_name . "_version"];
							}
							if (isset($row[$column_name . "_url"]))
							{
								$node->set_attr("url", $row[$column_name . "_url"] . $url_suffix);
							}
							elseif (isset($auxil_data[$column_name . "_url"]))
							{
								$node->set_attr("url", $auxil_data[$column_name . "_url"] . $url_suffix);
							}
							elseif (isset($auxil_data[$column_name . "_prefix"]))
							{
								$node->set_attr("url", $auxil_data[$column_name . "_prefix"] . "/" . $row["id"] . ".jpeg" . $url_suffix);
							}
						}
						else
						{
							$node = $row_node->create_child_node($column_name)->set_attr("is_uploaded", 0);
							if (isset($auxil_data[$column_name . "_stub_url"]))
							{
								$node->set_attr("width", $auxil_data[$column_name . "_stub_width"]);
								$node->set_attr("height", $auxil_data[$column_name . "_stub_height"]);
								$node->set_attr("url", $auxil_data[$column_name . "_stub_url"]);
							}
						}
						break;
						
					case "file_simple":
						if ($value)
						{
							$node = $row_node->create_child_node($column_name)
							->set_attr("is_uploaded", "1")
							->set_attr("file_name", $value)
							->set_attr("file_size", format_file_size($row[$column_name . "_file_size"]));
							$url_suffix = "";
							if (isset($row[$column_name . "_version"]))
							{
								$url_suffix = "?version=" . $row[$column_name . "_version"];
							}
							if (isset($auxil_data[$column_name . "_complex_url_prefix"]))
							{
								$node->set_attr("url", $auxil_data[$column_name . "_complex_url_prefix"] . $row["id"] . $auxil_data[$column_name . "_complex_url_suffix"] . $url_suffix);
							}
						}
						else
						{
							$node = $row_node->create_child_node($column_name)->set_attr("is_uploaded", 0);
						}
						break;
						
					case "intmarkup":
						$column_node = $row_node->create_child_node($column_name);
						if ($value["html"])
						{
							$column_node->import_xdom(xdom::create_from_string("<div class=\"intmarkup\">{$value["html"]}</div>"));
						}
						if (sizeof($value["files"]))
						{
							intmarkup_helper::fill_files_node($column_node->create_child_node("files"), $value["files"]);
						}
						break;

					default:
						$row_node->set_attr($column_name, $value);
				}
			}
			if ($data_one_row)
			{
				break;
			}
			if ($this->db->is_db_result($db_result))
			{
				$row = $this->db->fetch_array($db_result);
			}
			else
			{
				$row = next($db_result);
			}
		}
	}

}

?>