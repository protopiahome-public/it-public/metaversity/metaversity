<?php

final class cache
{

	/**
	 * @var mcache
	 */
	private $mcache;
	private $cache_key = "@";
	private $tag_keys = array();

	public function __construct()
	{
		global $mcache;
		$this->mcache = $mcache;
	}

	public function init($cache_key, $tag_keys)
	{
		$this->cache_key = $cache_key;
		$this->tag_keys = $tag_keys;
	}

	public function get()
	{
		return $this->mcache->get($this->cache_key);
	}

	public function set($value, $expire = null)
	{
		$this->mcache->set($this->cache_key, $value, $this->tag_keys, $expire);
	}

	public function delete()
	{
		$this->mcache->delete($this->cache_key);
	}

}

?>