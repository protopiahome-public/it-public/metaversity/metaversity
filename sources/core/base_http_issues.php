<?php

/**
 * @require response (constants definition from the response class)
 */
abstract class base_http_issues
{

	private $redirect_url = null;
	private $redirect_type = REDIRECT_302_FOUND;
	private $is_error_403 = false;
	private $is_error_404 = false;

	public function get_redirect_url()
	{
		return $this->redirect_url;
	}

	public function get_redirect_type()
	{
		return $this->redirect_type;
	}

	public function is_error_403()
	{
		return $this->is_error_403;
	}

	public function is_error_404()
	{
		return $this->is_error_404;
	}

	public function set_redirect_url($redirect_url)
	{
		$this->redirect_url = $redirect_url;
	}

	public function set_redirect_type($redirect_type)
	{
		$this->redirect_type = $redirect_type;
	}

	public function set_redirect_type_permanent()
	{
		$this->set_redirect_type(REDIRECT_301_MOVED_PERMANENTLY);
	}

	public function set_redirect_type_temporary()
	{
		$this->set_redirect_type(REDIRECT_302_MOVED_TEMPORARILY);
	}

	public function set_redirect_type_simple()
	{
		$this->set_redirect_type(REDIRECT_302_FOUND);
	}

	public function set_error_403($is_error_403 = true)
	{
		$this->is_error_403 = $is_error_403;
	}

	public function set_error_404($is_error_404 = true)
	{
		$this->is_error_404 = $is_error_404;
	}

}

?>