<?php

final class cache_tag
{

	/**
	 * @var mcache
	 */
	private $mcache;
	private $key;

	public function __construct()
	{
		global $mcache;
		$this->mcache = $mcache;
	}

	public function init($key)
	{
		$this->key = $key;
	}

	public function update()
	{
		$this->mcache->tag_update($this->key);
	}

	public function get_key()
	{
		return $this->key;
	}

}

?>