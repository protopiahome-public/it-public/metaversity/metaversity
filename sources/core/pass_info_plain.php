<?php

require_once PATH_CORE . "/pass_info.php";

class pass_info_plain extends pass_info
{

	protected $data;
	protected $vars_dump_exists = false;

	public function __construct()
	{
		global $session;
		$this->session = $session;

		$this->data = array();
		$this->xdom = xdom::create("pass_info");
	}

	public function write_error($error_name, $error_descr = "")
	{
		$this->data["ERRORS"][$error_name] = $error_descr;
	}

	public function write_field_error($field_name, $error_name, $error_descr = "")
	{
		$this->data["ERRORS_FIELDS"][$field_name][$error_name] = $error_descr;
	}

	public function write_info($info_name, $info_descr = "")
	{
		if (is_array($info_descr))
		{
			$this->data["INFOS"]["info_name"] = array();
			foreach ($info_descr as $name => $value)
			{
				$this->data["INFOS"][$info_name][$name] = $value;
			}
		}
		else
		{
			$this->data["INFOS"][$info_name] = $info_descr;
		}
	}

	public function dump_vars($post_only = false)
	{
		if ($this->vars_dump_exists)
		{
			return;
		}
		$this->vars_dump_exists = true;
		$this->data["VARS"] = array();
		foreach (($post_only ? $_POST : $_REQUEST) as $idx => $val)
		{
			if (is_array($val))
			{
				// Client might send arrays... i.e. <input type="checkbox" name="foo[]" />
				$this->data["VARS"][$idx] = array();
				foreach ($val as $idx2 => $val2)
				{
					$this->data["VARS"][$idx][$idx2] = $val2;
				}
			}
			else
			{
				$this->data["VARS"][$idx][$val] = array();
			}
		}
	}

	public function vars_dump_exists()
	{
		return $this->vars_dump_exists;
	}

	public function store()
	{
		if (count($this->data))
		{
			$session_id = $this->session->get_session_id();
			pass_info_cache::init($session_id)->set($this->data);
		}
	}

	/**
	 * @return xdom
	 */
	public function get_data()
	{
		return $this->data;
	}

}

?>