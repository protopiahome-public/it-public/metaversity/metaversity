<?php

require_once PATH_CORE . "/pass_info.php";
require_once PATH_INTCMF . "/url.php";

class save_loader extends base_loader
{

	protected $ctrls = array();

	/**
	 * @var pass_info
	 */
	protected $pass_info = null;

	public function autoload()
	{
		$p2 = $this->request->get_folders(2);
		$p3 = $this->request->get_folders(3);

		if ($p3 !== false)
		{
			$this->set_error_404();
			return;
		}

		if (!preg_match("#^[a-z0-9_\-]+$#", $p2))
		{
			$this->set_error_404();
			return;
		}

		global $__autoload_cache;
		$class = $p2 . "_save_page";
		if (!isset($__autoload_cache[$class]))
		{
			$this->set_error_404();
			return;
		}

		$this->add_ctrl(new $class());


		if (!$this->user->get_api_mode())
		{
			foreach ($this->ctrls as $ctrl)
			{
				/* @var $ctrl base_save_ctrl */
				if (!$ctrl->xss_allowed and $this->request->is_cross_site_request($allow_empty_referrer = true))
				{
					response::set_content('
		<p><strong>Error:</strong> cross-site requests are deprecated.</p>
		<p>Please check that your browser sends information about referrer page.
			Or simply change your browser to the latest version of
			<a href="http://www.opera.com/">Opera</a>,
			<a href="http://www.mozilla.com/">Mozilla Firefox</a>,
			<a href="http://www.google.com/chrome/">Google Chrome</a> or
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home">Internet Explorer</a>.
		</p>
		<p>Before this, you won\'t be able to send POST or AJAX requests to the site.</p>
		<p>Sorry, but this all is for safety of your data.</p>
		<p><a href="' . $this->request->get_full_prefix() . '/">Go to the main page</a></p>
		', "text/html");
					finalize();
					die;
				}
			}
		}
	}

	public function run()
	{
		global $config;
		if (isset($config["is_archived"]) and $config["is_archived"] and ! $this->user->is_admin())
		{
			$this->set_content_type_text();
			$this->set_content("Project is archived. save modules are turned off");
			return;
		}

		$this->pass_info = new pass_info();
		$check_ok = null;

		if ($this->is_error_404())
		{
			$this->build_response($check_ok);
			return;
		}

		if ($this->run_set_up())
		{
			if ($this->stop_load)
			{
				// Do nothing
			}
			elseif ($this->run_start())
			{
				if ($this->stop_load)
				{
					// Do nothing
				}
				elseif ($this->run_check_rights())
				{
					$check_ok = $this->run_check();
					$check_ok ? $this->run_commit() : $this->run_rollback();
				}
				else
				{
					$this->set_error_403();
				}
			}
			else
			{
				$this->set_error_404();
			}
		}
		else
		{
			$this->set_error_404();
		}
		$this->build_response($check_ok);
	}

	private function run_set_up()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($save_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $save_ctrl base_save_ctrl */
			$save_ctrl->set_pass_info($this->pass_info);
			$save_ctrl->mixin_call_method_with_mixins("init");
			if ($this->ctrls[0]->autostart_db_transaction() && !$this->db->is_transaction_started())
			{
				$this->db->begin();
			}
			$ok = $save_ctrl->mixin_call_method_with_mixins("set_up", array(), "chain");
			if (!$ok)
			{
				$this->write_fail("SET_UP", $save_ctrl);
				return false;
			}
			next($this->ctrls);
		}
		return true;
	}

	private function run_start()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($save_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $save_ctrl base_save_ctrl */
			$before_ok = $save_ctrl->mixin_call_method_with_mixins("on_before_start", array(), "chain");
			if (!$before_ok)
			{
				$this->write_fail("BEFORE_START", $save_ctrl);
				return false;
			}
			$ok = $save_ctrl->mixin_call_method_with_mixins("start", array(), "chain");
			if (!$ok)
			{
				$this->write_fail("START", $save_ctrl);
				return false;
			}
			$after_ok = $save_ctrl->mixin_call_method_with_mixins("on_after_start", array(), "chain");
			if (!$after_ok)
			{
				$this->write_fail("AFTER_START", $save_ctrl);
				return false;
			}
			next($this->ctrls);
		}
		return true;
	}

	private function run_check_rights()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($save_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $save_ctrl base_save_ctrl */
			$ok = $save_ctrl->mixin_call_method_with_mixins("check_rights", array(), "chain");
			if (!$ok)
			{
				$this->write_fail("CHECK_RIGHTS", $save_ctrl);
				return false;
			}
			next($this->ctrls);
		}
		return true;
	}

	private function run_check()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		$check_ok = true;
		while (($save_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $save_ctrl base_save_ctrl */
			$before_ok = $save_ctrl->mixin_call_method_with_mixins("on_before_check", array(), "and");
			if (!$before_ok)
			{
				$this->write_fail("BEFORE_CHECK", $save_ctrl);
				$check_ok = false;
			}
			else
			{
				$ok = $save_ctrl->mixin_call_method_with_mixins("check", array(), "and");
				if (!$ok)
				{
					$this->write_fail("CHECK", $save_ctrl);
					$check_ok = false;
				}
				else
				{
					$save_ctrl->mixin_call_method_with_mixins("on_after_check", array(), "and");
				}
			}
			next($this->ctrls);
		}
		return $check_ok;
	}

	private function run_commit()
	{
		if ($this->stop_load)
		{
			return true;
		}
		$commit_ok = true;
		reset($this->ctrls);
		while (($save_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $save_ctrl base_save_ctrl */
			$before_ok = $save_ctrl->mixin_call_method_with_mixins("on_before_commit", array(), "chain");
			if (!$before_ok)
			{
				$this->write_fail("BEFORE_COMMIT", $save_ctrl);
				$commit_ok = false;
				break;
			}
			else
			{
				$ok = $save_ctrl->mixin_call_method_with_mixins("commit", array(), "chain");
				if (!$ok)
				{
					$this->write_fail("COMMIT", $save_ctrl);
					$commit_ok = false;
					break;
				}
				else
				{
					$save_ctrl->mixin_call_method_with_mixins("on_after_commit");
				}
			}
			next($this->ctrls);
		}
		if ($commit_ok && !$this->stop_load)
		{
			if ($this->ctrls[0]->autostart_db_transaction() && $this->db->is_transaction_started())
			{
				$this->db->commit();
			}
			reset($this->ctrls);
			while (($save_ctrl = current($this->ctrls)) && !$this->stop_load)
			{
				/* @var $save_ctrl base_save_ctrl */
				$save_ctrl->mixin_call_method_with_mixins("clean_cache");
				next($this->ctrls);
			}
		}
		else
		{
			if ($this->ctrls[0]->autostart_db_transaction() && $this->db->is_transaction_started())
			{
				$this->db->rollback();
			}
		}
	}

	private function run_rollback()
	{
		if ($this->stop_load)
		{
			return true;
		}
		reset($this->ctrls);
		while (($save_ctrl = current($this->ctrls)) && !$this->stop_load)
		{
			/* @var $save_ctrl base_save_ctrl */
			$save_ctrl->mixin_call_method_with_mixins("rollback");
			next($this->ctrls);
		}
		if ($this->ctrls[0]->autostart_db_transaction() && $this->db->is_transaction_started())
		{
			$this->db->rollback();
		}
	}

	private function build_response($check_ok)
	{
		reset($this->ctrls);
		while (($save_ctrl = current($this->ctrls)))
		{
			if ($save_ctrl->is_error_403())
			{
				$this->set_error_403();
			}
			if ($save_ctrl->is_error_404())
			{
				$this->set_error_404();
			}
			$redirect_url = $save_ctrl->get_redirect_url();
			if ($redirect_url)
			{
				$this->set_redirect_url($redirect_url);
			}
			next($this->ctrls);
		}

		if ($this->is_error_403() or $this->is_error_404())
		{
			$this->set_redirect_url($this->request->get_prefix() . "/");
			if ($this->xerror->is_debug_mode())
			{
				$this->pass_info->store();
			}
		}
		else
		{
			$this->pass_info->store();
		}

		if (!$this->get_redirect_url())
		{
			if ($check_ok === true and isset($_REQUEST["retpath"]))
			{
				$this->set_redirect_url($_REQUEST["retpath"]);
			}
			elseif ($check_ok === false and isset($_REQUEST["errpath"]))
			{
				$this->set_redirect_url($_REQUEST["errpath"]);
			}
			elseif ($this->request->get_referrer())
			{
				$this->set_redirect_url($this->request->get_referrer());
			}
			else
			{
				$this->set_redirect_url($this->request->get_prefix() . "/");
			}
		}

		if ($this->xerror->is_debug_mode())
		{
			$referrer = $this->request->get_referrer();
			if ($referrer)
			{
				$referrer_url = new url($referrer);
				if (!is_null($referrer_url->get_param("nextxml")))
				{
					$redirect_url = $this->get_redirect_url();
					if (substr($redirect_url, 0, 1) == "/")
					{
						$redirect_url = $this->request->get_protocol() . "://" . $this->request->get_host() . $redirect_url;
					}
					$new_url = new url($redirect_url);
					if (!$new_url->get_error_code())
					{
						$new_url->set_param("xml");
						$this->set_redirect_url($new_url->get_url());
					}
				}
			}
		}
	}

	public function add_ctrl(base_save_ctrl $save_ctrl)
	{
		$this->ctrls[] = $save_ctrl;
	}

	public function write_fail($name, base_save_ctrl $save_ctrl)
	{
		$pass_info_xdom = $this->pass_info->get_xdom();
		$pass_info_xdom->create_child_node("error", $name)
				->set_attr("name", "FAIL")
				->set_attr("ctrl", get_class($save_ctrl));
	}

}

?>