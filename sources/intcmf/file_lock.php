<?php

class file_lock
{

	const MODE_READ = 1;
	const MODE_WRITE = 2;
	const MODE_READ_WRITE = 3;
	const MODE_APPEND = 4;
	const ERR_OK = 0;
	const ERR_OPEN = 1;
	const ERR_LOCK = 2;
	const ERR_READ = 3;
	const ERR_WRITE = 4;
	const ERR_UNLOCK = 5;
	const ERR_CLOSE = 6;

	protected $file_path;
	protected $mode;
	protected $handle;
	protected $last_error = 0;

	public function __construct($file_path, $mode)
	{
		$this->file_path = $file_path;
		$this->mode = $mode;
		$this->open();
	}

	// @todo is double open possible?
	public function open()
	{
		switch ($this->mode)
		{
			case self::MODE_READ:
				$file_mode = "rb";
				break;
			case self::MODE_WRITE:
				$file_mode = "cb";
				break;
			case self::MODE_READ_WRITE:
				$file_mode = "c+b";
				break;
			case self::MODE_APPEND:
				$file_mode = "ab";
				break;
		}
		$this->handle = fopen($this->file_path, $file_mode);
		if (false === $this->handle)
		{
			$this->last_error = self::ERR_OPEN;
			$this->handle = null;
			return false;
		}
		if (false === flock($this->handle, $this->mode === self::MODE_READ ? LOCK_SH : LOCK_EX))
		{
			$this->last_error = self::ERR_LOCK;
			$this->handle = null;
			return false;
		}
		return true;
	}

	public function get_last_error()
	{
		return $this->last_error;
	}
	
	public function is_ok()
	{
		return $this->last_error === self::ERR_OK;
	}

	public function get_contents()
	{
		if (!$this->handle)
		{
			return false;
		}
		if ($this->mode !== self::MODE_READ && $this->mode !== self::MODE_READ_WRITE)
		{
			trigger_error("Trying to read; file is opened for writing only ('{$this->file_path}')");
			return false;
		}
		fseek($this->handle, 0);
		clearstatcache(true, $this->file_path);
		$size = filesize($this->file_path);
		if (!$size)
		{
			return "";
		}
		$data = fread($this->handle, $size);
		if ($data === false)
		{
			$this->last_error = self::ERR_READ;
		}
		return $data;
	}
	
	public function fseek($offset)
	{
		fseek($this->handle, $offset);
	}
	
	public function get_line()
	{
		if ($this->mode !== self::MODE_READ && $this->mode !== self::MODE_READ_WRITE)
		{
			trigger_error("Trying to read; file is opened for writing only ('{$this->file_path}')");
			return false;
		}
		return fgets($this->handle);
	}

	public function write($data)
	{
		if (!$this->handle)
		{
			return false;
		}
		if ($this->mode === self::MODE_READ)
		{
			trigger_error("Trying to write; file is opened for reading only ('{$this->file_path}')");
			return false;
		}
		if ($this->mode !== self::MODE_APPEND)
		{
			if (false === ftruncate($this->handle, 0))
			{
				$this->last_error = self::ERR_WRITE;
				return false;
			}
			if (fseek($this->handle, 0) !== 0)
			{
				$this->last_error = self::ERR_WRITE;
				return false;
			}
		}
		if (!strlen($data))
		{
			return true;
		}
		$len = fwrite($this->handle, $data);
		if (!$len)
		{
			$this->last_error = self::ERR_WRITE;
			return false;
		}
		return true;
	}

	public function close()
	{
		if (!$this->handle)
		{
			return false;
		}
		$ok = true;
		if (false === flock($this->handle, LOCK_UN))
		{
			$ok = false;
			$this->last_error = self::ERR_UNLOCK;
		}
		if (false === fclose($this->handle))
		{
			$ok = false;
			$this->last_error = self::ERR_CLOSE;
		}
		return $ok;
	}

}

?>