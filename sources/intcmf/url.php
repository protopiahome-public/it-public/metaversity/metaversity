<?php

define("URL_PARSE_ERROR_BAD_PROTOCOL", 100);
define("URL_PARSE_ERROR_BAD_HOST", 110);
define("URL_PARSE_ERROR_BAD_PORT", 120);
define("URL_PARSE_ERROR_BAD_FOLDERS_STRING", 130);

class url
{

	protected $start_url;
	protected $protocol;
	protected $host;
	protected $host_raw;
	protected $port;
	protected $folders_string;
	protected $params_string;
	protected $folders = null;
	protected $params = array();
	protected $error_code = 0;

	public function __construct($url)
	{
		$this->start_url = $url;
		$this->parse();
	}

	/**
	 * Should extract url_parser class from this method
	 *
	 * @return bool
	 */
	protected function parse()
	{
		$url = $this->start_url;

		// Protocol
		if (strtolower(substr($url, 0, 7)) == "http://")
		{
			$this->protocol = "http";
			$next = 7;
		}
		elseif (strtolower(substr($url, 0, 8)) == "https://")
		{
			$this->protocol = "https";
			$next = 8;
		}
		else
		{
			$this->error_code = URL_PARSE_ERROR_BAD_PROTOCOL;
			return false;
		}
		$url = substr($url, $next);

		// Params
		$question_char_pos = strpos($url, "?");
		if ($question_char_pos !== false)
		{
			$this->params_string = substr($url, $question_char_pos);
			$params = explode("&", substr($this->params_string, 1));
			foreach ($params as $param)
			{
				if ($param == "")
				{
					continue;
				}
				$param_parts = explode("=", $param);
				if (isset($param_parts[1]))
				{
					$idx = $param_parts[0];
					$val = $param_parts[1];
				}
				else
				{
					$idx = $param;
					$val = "";
				}
				$this->params[urldecode($idx)] = urldecode($val);
			}
			$url = substr($url, 0, $question_char_pos);
		}

		// Host finding
		$first_slash_pos = strpos($url, "/");
		if ($first_slash_pos !== false)
		{
			$this->host = substr($url, 0, $first_slash_pos);
			$url = substr($url, $first_slash_pos);
		}
		else
		{
			$this->host = $url;
			$url = "/";
		}

		// Port finding
		$host_parts = explode(":", $this->host, 2);
		if (isset($host_parts[1]))
		{
			$this->port = $host_parts[1];
			$this->host_raw = $host_parts[0];
		}
		else
		{
			$this->port = 80;
			$this->host_raw = $this->host;
		}

		// Host check
		$ok = preg_match("#^[0-9a-z]([0-9a-z\-]*[0-9a-z])?(\.[0-9a-z]([0-9a-z\-]*[0-9a-z])?)*$#i", $this->host_raw);
		if (!$ok or $this->host_raw == "")
		{
			$this->error_code = URL_PARSE_ERROR_BAD_HOST;
			return false;
		}
		else
		{
			$this->host_raw = strtolower($this->host_raw);
			$this->host = strtolower($this->host);
		}

		// Port check
		if ($this->port !== 80)
		{
			$ok = true;
			$i = 0;
			while ($i < strlen($this->port))
			{
				$c = $this->port[$i];
				if ($c < '0' or $c > '9')
				{
					$ok = false;
					break;
				}
				$i++;
			}
			if (!$ok or $this->port == "")
			{
				$this->error_code = URL_PARSE_ERROR_BAD_PORT;
				return false;
			}
			else
			{
				$this->port = (int) $this->port;
				$this->host = $this->host_raw . ":" . $this->port;
			}
		}

		// Folders
		$this->folders_string = $url;

		// Folders check
		/* if (!preg_match("/[]/", $this->folders_string))
		  {

		  } */
	}

	public function get_error_code()
	{
		return $this->error_code;
	}

	public function get_url()
	{
		$url = $this->protocol . "://";
		$url .= $this->host;
		$url .= $this->folders_string;
		if (sizeof($this->params) > 0)
		{
			$first = true;
			$lambda = create_function('$item, $key, $user_data', '
				$user_data["url"] .= $user_data["first"] ? "?" : "&";
				if ($user_data["first"])
				{
					$user_data["first"] = false;
				}
				$user_data["url"] .= rawurlencode($key);
				if ($item !== false)
				{
					$user_data["url"] .= "=" . rawurlencode((string) $item);
				}
			');
			array_walk($this->params, $lambda, array(
				"url" => &$url,
				"first" => &$first
			));
		}
		return $url;
	}

	public function set_param($name, $value = false)
	{
		$this->params[$name] = $value;
	}

	public function remove_param($name)
	{
		unset($this->params[$name]);
	}

	public function get_folders_string()
	{
		return $this->folders_string;
	}

	public function get_folder($idx)
	{
		return isset($this->folders[$idx]) ? $this->folders[$idx] : null;
	}

	public function get_params()
	{
		return $this->params;
	}

	public function get_params_string()
	{
		return $this->params_string;
	}

	public function get_param($name)
	{
		if (isset($this->params[$name]))
		{
			return $this->params[$name];
		}
		else
		{
			return null;
		}
	}

}

?>