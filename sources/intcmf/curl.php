<?php

class curl
{

	protected $url = null;
	// Documentation for options: http://php.net/manual/en/function.curl-setopt.php
	protected $options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HEADER => false,
		CURLOPT_CONNECTTIMEOUT => 5,
		CURLOPT_TIMEOUT => 15,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_COOKIESESSION => true,
		CURLOPT_ENCODING => '',
		CURLOPT_USERAGENT => "Mozilla/4.0 (compatible; IntCMF/client; bot)",
	);
	protected $max_download_size = 0;
	protected $ch = null;
	protected $downloaded_size = 0;
	protected $response;
	protected $http_code;

	public function __construct($url = null)
	{
		$this->url = $url;
		if (isset($_SERVER["HTTP_HOST"]))
		{
			$this->options[CURLOPT_USERAGENT] = "Mozilla/4.0 (compatible; {$_SERVER["HTTP_HOST"]}/client; bot)";
		}
	}

	public function set_url($url)
	{
		$this->url = $url;
	}

	public function set_option($option, $value)
	{
		$this->options[$option] = $value;
	}

	public function set_post_data($post_data)
	{
		$this->options[CURLOPT_POSTFIELDS] = $post_data;
	}

	public function set_referer($referer)
	{
		$this->options[CURLOPT_REFERER] = $referer;
	}

	public function disable_ssl_check()
	{
		$this->options[CURLOPT_SSL_VERIFYPEER] = 0;
	}

	public function set_user_agent($user_agent)
	{
		$this->options[CURLOPT_USERAGENT] = $user_agent;
	}

	public function set_max_download_size($max_download_size)
	{
		$this->max_download_size = $max_download_size;
	}

	public function exec()
	{
		// @todo options cache and changing options between requests
		if (is_null($this->ch))
		{
			$this->ch = curl_init($this->url);
		}
		else
		{
			$this->options[CURLOPT_URL] = $this->url;
		}
		$auto_options = array();
		$ignore_response = false;
		if ($this->max_download_size > 0)
		{
			$auto_options[CURLOPT_WRITEFUNCTION] = array($this, "_curl_write_function_callback");
			$ignore_response = true;
		}
		$options = $this->options + $auto_options;
		curl_setopt_array($this->ch, $options);
		$result = curl_exec($this->ch);
		if (!$ignore_response)
		{
			$this->response = $result;
		}
		$this->http_code = $this->get_info(CURLINFO_HTTP_CODE);
		return $this->is_ok() ? $this->response : false;
	}

	public function is_ok()
	{
		return false !== $this->response && !$this->get_error_code() && 200 === $this->http_code;
	}

	public function get_response()
	{
		return $this->response;
	}

	public function get_http_code()
	{
		return $this->http_code;
	}

	public function get_error_code()
	{
		return curl_errno($this->ch);
	}

	public function get_error_description()
	{
		return curl_error($this->ch);
	}

	public function get_mime_type()
	{
		$mime_type = null;
		$content_type_header = $this->get_info(CURLINFO_CONTENT_TYPE);
		$matches = null;
		if ($content_type_header and preg_match("/^([a-z0-9_.+\/-]+)/i", $content_type_header, $matches))
		{
			$mime_type = $matches[1];
		}
		return $mime_type;
	}

	public function get_charset()
	{
		$charset = null;
		$content_type_header = $this->get_info(CURLINFO_CONTENT_TYPE);
		$matches = null;
		if ($content_type_header and preg_match("/charset=([a-z0-9-]+?)(;|\$)/i", $content_type_header, $matches))
		{
			$charset = $matches[1];
		}
		return $charset;
	}

	// Opt list: http://php.net/manual/en/function.curl-getinfo.php
	public function get_info($opt)
	{
		if (is_null($this->ch))
		{
			return null;
		}
		return curl_getinfo($this->ch, $opt);
	}

	public function _curl_write_function_callback($ch, $data)
	{
		$len = strlen($data);
		$this->downloaded_size += $len;
		$this->response .= $data;
		if ($this->downloaded_size > $this->max_download_size)
		{
			return 0;
		}
		return $len;
	}

}

?>