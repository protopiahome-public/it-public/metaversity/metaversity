<?php

class xml_builder
{

	public static function file_size(xnode $parent_node, $size)
	{

		if ($size < 1024)
		{
			$parent_node->set_attr("size", $size);
			$parent_node->set_attr("size_unit", "B");
		}
		else
		{
			$size = $size / 1024;
			if (round($size) < 1024)
			{
				$parent_node->set_attr("size", human_round($size));
				$parent_node->set_attr("size_unit", "KB");
			}
			else
			{
				$size = $size / 1024;
				if (round($size) < 1024)
				{
					$parent_node->set_attr("size", human_round($size));
					$parent_node->set_attr("size_unit", "MB");
				}
				else
				{
					$size = $size / 1024;
					if (round($size) < 1024)
					{
						$parent_node->set_attr("size", human_round($size));
						$parent_node->set_attr("size_unit", "GB");
					}
					else
					{
						$size = $size / 1024;
						$parent_node->set_attr("size", human_round($size));
						$parent_node->set_attr("size_unit", "TB");
					}
				}
			}
		}
	}

	public static function tree(xnode $parent_node, tree $tree, $node_name = "item", $attrs = null, $fill_contents_callback = null)
	{
		self::get_tree_node($parent_node, $tree, $node_name, $tree->get_root(), $attrs, $fill_contents_callback);
	}

	protected static function get_tree_node(xnode $parent_node, tree $tree, $node_name, $node_data, $attrs, $fill_contents_callback = null, $level = 1, &$descendants_ids = array())
	{
		foreach ($node_data["children"] as $child_data)
		{
			$item_node = $parent_node->create_child_node($node_name);
			$item_node->set_attr("level", $level);

			$child_descendants_ids = array();
			if (count($child_data["children"]))
			{
				self::get_tree_node($item_node, $tree, $node_name, $child_data, $attrs, $fill_contents_callback, $level + 1, $child_descendants_ids);
				$descendants_ids = array_merge($descendants_ids, $child_descendants_ids);
			}
			
			if ($fill_contents_callback)
			{
				call_user_func($fill_contents_callback, $item_node, $child_data["contents"]);
			}
			else
			{
				foreach ($child_data["contents"] as $attr_name => $attr_value)
				{
					if (empty($attrs) || in_array($attr_name, $attrs))
					{
						$item_node->set_attr($attr_name, $attr_value);
					}
				}
			}

			$ancestor_ids = array();
			if ($tree->is_with_ancestors())
			{
				foreach ($child_data["ancestors"] as $ancestor)
				{
					$ancestor_ids[] = $ancestor["id"];
				}
			}
			$item_node->set_attr("ancestors", join(",", $ancestor_ids));
			$item_node->set_attr("descendants", join(",", $child_descendants_ids));
			array_push($descendants_ids, $child_data["id"]);
		}
	}

}

?>
