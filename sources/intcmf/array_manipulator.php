<?php

class array_manipulator
{

	const FILTER_MODE_EQ = 1;
	const FILTER_MODE_EQ_IF_NOT_EMPTY = 2;

	/**
	 * Usage: $array = array_manipulator::filter($array, array(<cond>));
	 * <cond> is one of the following:
	 * - array(FILTER_MODE_EQ, <column>, <value>)
	 * - array(FILTER_MODE_EQ_IF_NOT_EMPTY, <column>, <value>)
	 */
	public static function filter($array, $conditions = array())
	{
		$result = array();
		foreach ($array as $key => $val)
		{
			foreach ($conditions as $cond)
			{
				switch ($cond[0])
				{
					case self::FILTER_MODE_EQ:
						if (!(isset($val[$cond[1]]) and $val[$cond[1]] == $cond[2]))
						{
							continue 3;
						}
						break;
					case self::FILTER_MODE_EQ_IF_NOT_EMPTY:
						if (isset($val[$cond[1]]) and ! empty($val[$cond[1]]) and $val[$cond[1]] != $cond[2])
						{
							continue 3;
						}
						break;
				}
			}
			$result[$key] = $val;
		}
		return $result;
	}

	public static function merge_sorted($array1, $array2, $column, $order = SORT_ASC)
	{
		$result = array();
		$is_desc = $order === SORT_DESC;
		$val1 = reset($array1);
		$val2 = reset($array2);
		while ($val1 !== false and $val2 !== false)
		{
			if (($val1[$column] < $val2[$column]) xor $is_desc)
			{
				$result[] = $val1;
				$val1 = next($array1);
			}
			else
			{
				$result[] = $val2;
				$val2 = next($array2);
			}
		}
		if ($val1 !== false)
		{
			do
			{
				$result[] = $val1;
				$val1 = next($array1);
			}
			while ($val1 !== false);
		}
		elseif ($val2 !== false)
		{
			do
			{
				$result[] = $val2;
				$val2 = next($array2);
			}
			while ($val2 !== false);
		}
		return $result;
	}

}

?>