<?php

/**
 * An interface to the beanstalk queue service. Implements the beanstalk
 * protocol spec 1.9. Where appropriate the documentation from the protcol
 * has been added to the docblocks in this class.
 *
 * @link https://github.com/kr/beanstalkd/blob/master/doc/protocol.txt
 */
class beanstalk
{

	protected $is_connected = false;
	protected $host;
	protected $port;
	protected $persistent;
	protected $timeout;
	protected $conn;

	public function __construct($host = "127.0.0.1", $port = 11300, $is_persistent = false, $timeout = 2)
	{
		$this->host = $host;
		$this->port = $port;
		$this->persistent = $is_persistent;
		$this->timeout = $timeout;
	}

	public function __destruct()
	{
		$this->disconnect();
	}

	public function connect()
	{
		if (isset($this->conn))
		{
			$this->disconnect();
		}

		$function = $this->persistent ? "pfsockopen" : "fsockopen";
		$error_num = 0;
		$error_str = "";
		$params = array($this->host, $this->port, &$error_num, &$error_str);
		if ($this->timeout)
		{
			$params[] = $this->timeout;
		}
		$this->conn = call_user_func_array($function, $params);

		if (!empty($error_num) || !empty($error_str))
		{
			$this->_error("{$error_num}: {$error_str}");
		}

		$this->is_connected = is_resource($this->conn);

		if ($this->is_connected)
		{
			stream_set_timeout($this->conn, -1);
		}
		return $this->is_connected;
	}

	public function disconnect()
	{
		if (!is_resource($this->conn))
		{
			$this->is_connected = false;
		}
		else
		{
			$this->_write('quit');
			$this->is_connected = !fclose($this->conn);

			if (!$this->is_connected)
			{
				$this->conn = null;
			}
		}
		return !$this->is_connected;
	}

	protected function _error($message)
	{
		trigger_error("Beanstalk error: " . $message);
	}

	protected function _write($data)
	{
		if (!$this->is_connected)
		{
			$this->connect();
		}

		$data .= "\r\n";
		return fwrite($this->conn, $data, strlen($data));
	}

	protected function _read($length = null)
	{
		if (!$this->is_connected)
		{
			$this->connect();
		}

		if ($length)
		{
			if (feof($this->conn))
			{
				return false;
			}
			$data = stream_get_contents($this->conn, $length + 2);
			$meta = stream_get_meta_data($this->conn);

			if ($meta["timed_out"])
			{
				$this->_error("Connection timeout while reading data");
			}
			$packet = rtrim($data, "\r\n");
		}
		else
		{
			$packet = stream_get_line($this->conn, 16384, "\r\n");
		}

		return $packet;
	}

	/* Producer Commands */

	/**
	 * The `put` command is for any process that wants to insert a job into the queue.
	 *
	 * @param integer $priority Jobs with smaller priority values will be scheduled
	 *        before jobs with larger priorities. The most urgent priority is
	 *        0; the least urgent priority is 4294967295.
	 * @param integer $delay Seconds to wait before putting the job in the
	 *        ready queue.  The job will be in the "delayed" state during this time.
	 * @param integer $ttr Time to run - Number of seconds to allow a worker to
	 *        run this job.  The minimum ttr is 1.
	 * @param string $data The job body.
	 * @return integer|boolean `false` on error otherwise an integer indicating
	 *         the job id.
	 */
	public function put($priority, $delay, $ttr, $data)
	{
		$this->_write(sprintf("put %d %d %d %d\r\n%s", $priority, $delay, $ttr, strlen($data), $data));
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'INSERTED':
			case 'BURIED':
				return (integer) strtok(' '); // job id
			case 'EXPECTED_CRLF':
			case 'JOB_TOO_BIG':
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * The `use` command is for producers. Subsequent put commands will put
	 * jobs into the tube specified by this command. If no use command has
	 * been issued, jobs will be put into the tube named `default`.
	 *
	 * @param string $tube A name at most 200 bytes. It specifies the tube to
	 *        use. If the tube does not exist, it will be created.
	 * @return string|boolean `false` on error otherwise the name of the tube.
	 */
	public function use_tube($tube)
	{
		$this->_write(sprintf('use %s', $tube));
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'USING':
				return strtok(' ');
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Pause a tube delaying any new job in it being reserved for a given time.
	 *
	 * @param string $tube The name of the tube to pause.
	 * @param integer $delay Number of seconds to wait before reserving any more
	 *        jobs from the queue.
	 * @return boolean `false` on error otherwise `true`.
	 */
	public function pause_tube($tube, $delay)
	{
		$this->_write(sprintf('pause-tube %s %d', $tube, $delay));
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'PAUSED':
				return true;
			case 'NOT_FOUND':
			default:
				$this->_error($status);
				return false;
		}
	}

	/* Worker Commands */

	/**
	 * Reserve a job (with a timeout).
	 *
	 * @param integer $timeout If given specifies number of seconds to wait for
	 *        a job. `0` returns immediately.
	 * @return array|false `false` on error otherwise an array holding job id
	 *         and body.
	 */
	public function reserve($timeout = null)
	{
		if (isset($timeout))
		{
			$this->_write(sprintf('reserve-with-timeout %d', $timeout));
		}
		else
		{
			$this->_write('reserve');
		}
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'RESERVED':
				return array(
					'id' => (integer) strtok(' '),
					'body' => $this->_read((integer) strtok(' '))
				);
			case 'DEADLINE_SOON':
			case 'TIMED_OUT':
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Removes a job from the server entirely.
	 *
	 * @param integer $id The id of the job.
	 * @return boolean `false` on error, `true` on success.
	 */
	public function delete($id)
	{
		$this->_write(sprintf('delete %d', $id));
		$status = $this->_read();

		switch ($status)
		{
			case 'DELETED':
				return true;
			case 'NOT_FOUND':
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Puts a reserved job back into the ready queue.
	 *
	 * @param integer $id The id of the job.
	 * @param integer $priority Priority to assign to the job.
	 * @param integer $delay Number of seconds to wait before putting the job in the ready queue.
	 * @return boolean `false` on error, `true` on success.
	 */
	public function release($id, $priority, $delay)
	{
		$this->_write(sprintf('release %d %d %d', $id, $priority, $delay));
		$status = $this->_read();

		switch ($status)
		{
			case 'RELEASED':
			case 'BURIED':
				return true;
			case 'NOT_FOUND':
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Puts a job into the `buried` state Buried jobs are put into a FIFO
	 * linked list and will not be touched until a client kicks them.
	 *
	 * @param integer $id The id of the job.
	 * @param integer $priority *New* priority to assign to the job.
	 * @return boolean `false` on error, `true` on success.
	 */
	public function bury($id, $priority)
	{
		$this->_write(sprintf('bury %d %d', $id, $priority));
		$status = $this->_read();

		switch ($status)
		{
			case 'BURIED':
				return true;
			case 'NOT_FOUND':
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Allows a worker to request more time to work on a job.
	 *
	 * @param integer $id The id of the job.
	 * @return boolean `false` on error, `true` on success.
	 */
	public function touch($id)
	{
		$this->_write(sprintf('touch %d', $id));
		$status = $this->_read();

		switch ($status)
		{
			case 'TOUCHED':
				return true;
			case 'NOT_TOUCHED':
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Adds the named tube to the watch list for the current connection.
	 *
	 * @param string $tube Name of tube to watch.
	 * @return integer|boolean `false` on error otherwise number of tubes in watch list.
	 */
	public function watch($tube)
	{
		$this->_write(sprintf('watch %s', $tube));
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'WATCHING':
				return (integer) strtok(' ');
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Remove the named tube from the watch list.
	 *
	 * @param string $tube Name of tube to ignore.
	 * @return integer|boolean `false` on error otherwise number of tubes in watch list.
	 */
	public function ignore($tube)
	{
		$this->_write(sprintf('ignore %s', $tube));
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'WATCHING':
				return (integer) strtok(' ');
			case 'NOT_IGNORED':
			default:
				$this->_error($status);
				return false;
		}
	}

	/* Other Commands */

	/**
	 * Inspect a job by its id.
	 *
	 * @param integer $id The id of the job.
	 * @return string|boolean `false` on error otherwise the body of the job.
	 */
	public function peek($id)
	{
		$this->_write(sprintf('peek %d', $id));
		return $this->_peek_read();
	}

	/**
	 * Inspect the next ready job.
	 *
	 * @return string|boolean `false` on error otherwise the body of the job.
	 */
	public function peek_ready()
	{
		$this->_write('peek-ready');
		return $this->_peek_read();
	}

	/**
	 * Inspect the job with the shortest delay left.
	 *
	 * @return string|boolean `false` on error otherwise the body of the job.
	 */
	public function peek_delayed()
	{
		$this->_write('peek-delayed');
		return $this->_peek_read();
	}

	/**
	 * Inspect the next job in the list of buried jobs.
	 *
	 * @return string|boolean `false` on error otherwise the body of the job.
	 */
	public function peek_buried()
	{
		$this->_write('peek-buried');
		return $this->_peek_read();
	}

	/**
	 * Handles response for all peek methods.
	 *
	 * @return string|boolean `false` on error otherwise the body of the job.
	 */
	protected function _peek_read()
	{
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'FOUND':
				return array(
					'id' => (integer) strtok(' '),
					'body' => $this->_read((integer) strtok(' '))
				);
			case 'NOT_FOUND':
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Moves jobs into the ready queue (applies to the current tube).
	 *
	 * If there are buried jobs those get kicked only otherwise delayed
	 * jobs get kicked.
	 *
	 * @param integer $bound Upper bound on the number of jobs to kick.
	 * @return integer|boolean False on error otherwise number of jobs kicked.
	 */
	public function kick($bound)
	{
		$this->_write(sprintf('kick %d', $bound));
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'KICKED':
				return (integer) strtok(' ');
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * This is a variant of the kick command that operates with a single
	 * job identified by its job id. If the given job id exists and is in a
	 * buried or delayed state, it will be moved to the ready queue of the
	 * the same tube where it currently belongs.
	 *
	 * @param integer $id The job id.
	 * @return boolean `false` on error `true` otherwise.
	 */
	public function kick_job($id)
	{
		$this->_write(sprintf('kick-job %d', $id));
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'KICKED':
				return true;
			case 'NOT_FOUND':
			default:
				$this->_error($status);
				return false;
		}
	}

	/* Stats Commands */

	/**
	 * Gives statistical information about the specified job if it exists.
	 *
	 * @param integer $id The job id.
	 * @return string|boolean `false` on error otherwise a string with a yaml formatted dictionary.
	 */
	public function stats_job($id)
	{
		$this->_write(sprintf('stats-job %d', $id));
		return $this->_stats_read();
	}

	/**
	 * Gives statistical information about the specified tube if it exists.
	 *
	 * @param string $tube Name of the tube.
	 * @return string|boolean `false` on error otherwise a string with a yaml formatted dictionary.
	 */
	public function stats_tube($tube)
	{
		$this->_write(sprintf('stats-tube %s', $tube));
		return $this->_stats_read();
	}

	/**
	 * Gives statistical information about the system as a whole.
	 *
	 * @return string|boolean `false` on error otherwise a string with a yaml formatted dictionary.
	 */
	public function stats()
	{
		$this->_write('stats');
		return $this->_stats_read();
	}

	/**
	 * Returns a list of all existing tubes.
	 *
	 * @return string|boolean `false` on error otherwise a string with a yaml formatted list.
	 */
	public function list_tubes()
	{
		$this->_write('list-tubes');
		return $this->_stats_read();
	}

	/**
	 * Returns the tube currently being used by the producer.
	 *
	 * @return string|boolean `false` on error otherwise a string with the name of the tube.
	 */
	public function list_tube_used()
	{
		$this->_write('list-tube-used');
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'USING':
				return strtok(' ');
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Returns a list of tubes currently being watched by the worker.
	 *
	 * @return string|boolean `false` on error otherwise a string with a yaml formatted list.
	 */
	public function list_tubes_watched()
	{
		$this->_write('list-tubes-watched');
		return $this->_stats_read();
	}

	/**
	 * Handles responses for all stat methods.
	 *
	 * @param boolean $decode Whether to decode data before returning it or not. Default is `true`.
	 * @return array|string|boolean `false` on error otherwise statistical data.
	 */
	protected function _stats_read($decode = true)
	{
		$status = strtok($this->_read(), ' ');

		switch ($status)
		{
			case 'OK':
				$data = $this->_read((integer) strtok(' '));
				return $decode ? $this->_decode($data) : $data;
			default:
				$this->_error($status);
				return false;
		}
	}

	/**
	 * Decodes YAML data. This is a super naive decoder which just works on
	 * a subset of YAML which is commonly returned by beanstalk.
	 *
	 * @param string $data The data in YAML format, can be either a list or a dictionary.
	 * @return array An (associative) array of the converted data.
	 */
	protected function _decode($data)
	{
		$data = array_slice(explode("\n", $data), 1);
		$result = array();

		foreach ($data as $key => $value)
		{
			if ($value[0] === '-')
			{
				$value = ltrim($value, '- ');
			}
			elseif (strpos($value, ':') !== false)
			{
				list($key, $value) = explode(':', $value);
				$value = ltrim($value, ' ');
			}
			if (is_numeric($value))
			{
				$value = (integer) $value == $value ? (integer) $value : (float) $value;
			}
			$result[$key] = $value;
		}
		return $result;
	}

}

?>