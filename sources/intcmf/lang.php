<?php

class lang
{

	protected $current_lang_code;
	protected $translations = array();
	protected $all_keys = array();

	public function __construct($current_lang_code = null, $supported_lang_codes = array())
	{
		if (!$current_lang_code)
		{
			if (!$supported_lang_codes)
			{
				trigger_error("Language auto-detection does not work without supplying list of supported languages");
			}
			$current_lang_code = $this->get_autodetected_lang_code($supported_lang_codes);
		}

		$this->current_lang_code = $current_lang_code;
		$this->reload_translations();
	}

	protected function get_autodetected_lang_code($supported_lang_codes)
	{
		$langs = array();
		if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
		{
			// break up string into pieces (languages and q factors)
			$lang_parse = null;
			preg_match_all("/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i", $_SERVER["HTTP_ACCEPT_LANGUAGE"], $lang_parse);

			if (count($lang_parse[1]))
			{
				// create a list like "en" => 0.8
				$langs = array_combine($lang_parse[1], $lang_parse[4]);

				// set default to 1 for any without q factor
				foreach ($langs as $lang => $val)
				{
					if ($val === "")
					{
						$langs[$lang] = 1;
					}
				}

				// sort list based on value	
				arsort($langs, SORT_NUMERIC);
			}
		}
		$index = array_flip($supported_lang_codes);
		foreach ($langs as $lang => $val)
		{
			$lang2 = substr($lang, 0, 2);
			if (strlen($lang2) !== 2)
			{
				continue;
			}
			if (isset($index[$lang2]))
			{
				return $lang2;
			}
		}

		return reset($supported_lang_codes);
	}

	public function reload_translations($use_cache = true, $collect_all_keys = false)
	{
		$original_file_path = $this->get_lang_file_path();
		$cache_file_path = $this->get_cache_file_path();
		if (!file_exists($original_file_path))
		{
			trigger_error("Language file does not exist (current_lang = '{$this->current_lang_code}')");
			return;
		}
		if ($use_cache and file_exists($cache_file_path) and filemtime($cache_file_path) >= filemtime($original_file_path))
		{
			$this->translations = unserialize(file_get_contents_safe($cache_file_path));
			return;
		}
		$f = fopen($original_file_path, "r");
		$state = 0;
		$key = "";
		while (false !== ($line = fgets($f)))
		{
			$line = trim($line);
			if (!$line)
			{
				$state = 0;
				continue;
			}
			if (0 === $state)
			{
				$sep_pos = strrpos($line, "//");
				if ($sep_pos === false)
				{
					$text = $line;
					$context = "";
				}
				else
				{
					$text = trim(substr($line, 0, $sep_pos));
					$context = trim(substr($line, $sep_pos + 2));
				}
				$key = $this->get_key($text, $context);
				if ($collect_all_keys)
				{
					$this->all_keys[$key] = true;
				}
				$state = 1;
			}
			elseif (1 === $state)
			{
				if ($line === "_")
				{
					$line = "";
				}
				if (!isset($this->translations[$key]))
				{
					$this->translations[$key] = $line;
				}
				else
				{
					$this->translations[$key] .= "\n" . $line;
				}
			}
		}
		fclose($f);
		if ($use_cache)
		{
			file_put_contents_safe($cache_file_path, serialize($this->translations));
		}
	}

	public function get_current_lang_code()
	{
		return $this->current_lang_code;
	}

	public function get_lang_file_path()
	{
		return PATH_CONFIG . "/lang_" . $this->current_lang_code . ".txt";
	}

	public function get_cache_file_path()
	{
		return PATH_CACHE . "/lang_" . $this->current_lang_code . "_cache.data";
	}

	public function trans($text, $context = "")
	{
		$key = $this->get_key($text, $context);
		return isset($this->translations[$key]) ? $this->translations[$key] : $text;
	}

	public function trans_exists($text, $context = "")
	{
		$key = $this->get_key($text, $context);
		return isset($this->translations[$key]);
	}

	public function trans_html($html)
	{
		$trans = $this->trans($html);
		$args = func_get_args();
		for ($i = 1; $i < sizeof($args); ++$i)
		{
			if ($args[$i] !== false)
			{
				$trans = str_replace("%{$i}%", htmlspecialchars($args[$i]), $trans);
			}
		}
		return $trans;
	}
	
	// @todo \r\ns, etc.
	public function trans_html_debug($html)
	{
		$text = "LANG  = " . $this->current_lang_code . "\n";
		$text .= "KEY   = " . (false === $html ? "(INVALID)" : $html) . "\n";
		$text .= "TRANS = " . $this->trans($html) . "\n";
		$args = func_get_args();
		for ($i = 1; $i < sizeof($args); ++$i)
		{
			if ($args[$i] !== false)
			{
				$text .= "%{$i}%   = $args[$i]\n";
			}
		}
		response::set_content_text($text);
	}

	public function trans_html_xslt_debug($node_set)
	{
		$html = $this->get_html_from_node_set($node_set);
		$params = func_get_args();
		array_shift($params);
		array_unshift($params, $html);
		call_user_func_array(array($this, "trans_html_debug"), $params);
		return new DOMDocument;
	}

	public function trans_html_xslt($node_set)
	{
		$html = $this->get_html_from_node_set($node_set);
		if (false === $html)
		{
			return new DOMDocument;
		}
		$params = func_get_args();
        array_shift($params);
        array_unshift($params, $html);
        $trans = call_user_func_array(array($this, "trans_html"), $params);
		$doc = new DOMDocument;
		$doc->loadXml("<x-root>{$trans}</x-root>", LIBXML_NOERROR);
		if (!$doc->getElementsByTagName("x-root")->length)
		{
			trigger_error("Error occurred while parsing translation (XML-compatible string is expected): '{$trans}' (translation for '{$html}')");
			return new DOMDocument;
		}
		return $doc;
	}

	private function get_html_from_node_set($node_set)
	{
		if (!is_array($node_set))
		{
			trigger_error("Error: \$node_set is not an array (" . gettype($node_set) . ")");
			return false;
		}
		if (!isset($node_set[0]))
		{
			trigger_error("Error: \$node_set is empty");
			return false;
		}
		if (!$node_set[0] instanceof DOMDocument)
		{
			trigger_error("Error: \$node_set[0] is not DOMDocument");
			return false;
		}
		return trim(preg_replace("/[\\x00-\\x20]+/", " ", $node_set[0]->saveHTML()));
	}

	public function key_exists($text, $context = "")
	{
		$key = $this->get_key($text, $context);
		return isset($this->all_keys[$key]);
	}

	public function get_key($text, $context)
	{
		return $text . " // " . $context;
	}

	public function get_all_keys()
	{
		return $this->all_keys;
	}

}

?>