<?php

/**
 * @todo autmatic catching of the content (ob_start...) - is it good ???
 * @todo is headers escaping neccessary?
 * @todo set default value for cookie path to $prefix (class initialization?)
 *
 */
class output_buffer
{
	private static $main_header = null;
	private static $headers = array();
	private static $cookies = array();
	private static $content_exists = false;
	private static $content = "";

	public static function set_main_header($value)
	{
		self::$main_header = $value;
	}

	public static function set_header($name, $value)
	{
		self::$headers[strtolower($name)] = array(
			'name' => $name,
			'value' => $value
		);
	}

	public static function set_cookie($name, $value = "1", $expires = 0, $path = "", $domain = "", $secure = false, $httponly = false)
	{
		self::$cookies[$name] = array(
			"value" => $value,
			"expires" => $expires,
			"path" => $path,
			"domain" => $domain,
			"secure" => $secure,
			"httponly" => $httponly
		);
	}

	public static function delete_cookie($name, $path = "", $domain = "")
	{
		self::set_cookie($name, "0", time() - 315360000, $path, $domain); // minus 10 years
	}

	public static function set_content($content)
	{
		self::$content_exists = true;
		self::$content = $content;
	}

	public static function append_content($additional_content)
	{
		self::$content_exists = true;
		self::$content .= $additional_content;
	}

	public static function clean_all()
	{
		self::$main_header = null;
		self::$headers = array();
		self::$cookies = array();
		self::$content_exists = false;
		self::$content = "";
	}

	public static function clean_content()
	{
		self::$content_exists = false;
		self::$content = "";
	}

	public static function content_exists()
	{
		return self::$content_exists;
	}

	protected static function write_headers()
	{
		if (!is_null(self::$main_header))
		{
			header(self::$main_header);
		}
		foreach (self::$headers as $header_name => $header_desc_array)
		{
			header($header_desc_array['name'] . ': ' . $header_desc_array['value'], false);
		}
		foreach (self::$cookies as $cookie_name => $cookie_desc_array)
		{
			header('Set-Cookie: ' . rawurlencode($cookie_name) . '=' . rawurlencode($cookie_desc_array['value']) . (!($cookie_desc_array['expires']) ? '' : "; Expires=" . date("r", $cookie_desc_array['expires'])) . (empty($cookie_desc_array['path']) ? '' : "; path=" . $cookie_desc_array['path']) . (empty($cookie_desc_array['domain']) ? '' : '; domain=' . $cookie_desc_array['domain']) . (!$cookie_desc_array['secure'] ? '' : '; secure') . (!$cookie_desc_array['httponly'] ? '' : '; HttpOnly'), false);
		}
	}

	protected static function write_content()
	{
		if (self::$content_exists)
		{
			echo self::$content;
		}
	}

	public static function finalize()
	{
		self::write_headers();
		self::write_content();
	}

	public static function get_content()
	{
		return self::$content_exists ? self::$content : "";
	}

}

?>