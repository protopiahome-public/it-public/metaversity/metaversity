<?php

class ximage
{

	// Init
	protected $src_path;
	protected $use_image_magick = true;
	// Image props
	protected $width;
	protected $height;
	protected $file_size;
	protected $mime_type = null;
	protected $image_type;
	protected $data_loaded = false;
	// Internal
	protected $image_resource = false; // GD only

	public function __construct($src_path, $use_image_magick = true)
	{
		$this->src_path = $src_path;
		$this->use_image_magick = $use_image_magick;
	}

	public function image_type_test($allowed_types = array(IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_PNG))
	{
		$this->load_image_attrs();
		return in_array($this->image_type, $allowed_types);
	}

	/**
	 * @return ximage
	 */
	public function resize($width, $height, $path, $resize_to_fill = false)
	{
		// If 0 leave original size
		$width = $width > 0 ? $width : $this->get_width();
		$height = $height > 0 ? $height : $this->get_height();

		if (!$resize_to_fill)
		{
			if (($width >= $this->width) && ($height >= $this->height))
			{
				$new_width = $this->width;
				$new_height = $this->height;
			}
			elseif (($width < $this->width) && ($height < $this->height))
			{
				if (($this->width / $this->height) > ($width / $height))
				{
					$factor = $this->width / $width;
				}
				else
				{
					$factor = $this->height / $height;
				}

				$new_width = $this->width / $factor;
				$new_height = $this->height / $factor;
			}
			elseif ($width < $this->width)
			{
				$factor = $this->width / $width;
				$new_width = $this->width / $factor;
				$new_height = $this->height / $factor;
			}
			elseif ($height < $this->height)
			{
				$factor = $this->height / $height;
				$new_width = $this->width / $factor;
				$new_height = $this->height / $factor;
			}
		}
		else
		{
			if ($this->width == $width and $this->height >= $height or $this->height == $height and $this->width >= $width)
			{
				$new_width = $this->width;
				$new_height = $this->height;
			}
			else
			{
				$factor1 = $width / $this->width;
				$factor2 = $height / $this->height;
				$factor = max($factor1, $factor2);
				$new_width = round($this->width * $factor);
				$new_height = round($this->height * $factor);
			}
		}

		if ($this->use_image_magick)
		{
			$new_width = (int) $new_width;
			$new_height = (int) $new_height;

			global $config;
			$cmd = '"' . $config["imagemagick_path"] . 'convert" -quality 90 -thumbnail ' . $new_width . 'x' . $new_height . ' ' . $this->src_path . '[0] ' . $path;

			exec($cmd, $output, $ret);
			if ($ret)
			{
				$f = fopen(PATH_LOG . "/im.log", "a+");
				fwrite($f, "[{$ret}] {$cmd}\n" . join('\n', $output) . "\n");
				fclose($f);
			}
		}
		else
		{
			if (!$this->load_gd_resource())
			{
				return null;
			}

			$resize_resource = imagecreatetruecolor($new_width, $new_height);

			imagecopyresized($resize_resource, $this->image_resource, 0, 0, 0, 0, $new_width, $new_height, $this->width, $this->get_height());

			/* switch ($this->image_type)
			  {
			  case IMAGETYPE_JPEG:
			  {
			  imagejpeg($resize_resource, $path);
			  break;
			  }
			  case IMAGETYPE_GIF:
			  {
			  imagegif($resize_resource, $path);
			  break;
			  }
			  case IMAGETYPE_PNG:
			  {
			  imagepng($resize_resource, $path);
			  break;
			  }
			  } */

			imagejpeg($resize_resource, $path, 85);
		}

		return new ximage($path, $this->use_image_magick);
	}

	/**
	 * @return ximage
	 */
	public function crop($width, $height, $path, $crop_from_center = false)
	{
		$width = $width > 0 ? $width : $this->get_width();
		$height = $height > 0 ? $height : $this->get_height();

		if ($this->use_image_magick)
		{
			global $config;
			$x = 0;
			$y = 0;
			if ($crop_from_center)
			{
				if ($this->get_width() > $width)
				{
					$x = round(($this->get_width() - $width) / 2);
				}
				if ($this->get_height() > $height)
				{
					$y = round(($this->get_height() - $height) / 2);
				}
			}
			$cmd = '"' . $config["imagemagick_path"] . 'convert" -quality 90 -crop ' . $width . 'x' . $height . '+' . $x . '+' . $y . ' ' . $this->src_path . ' ' . $path;
			exec($cmd, $output, $ret);
			if ($ret)
			{
				$f = fopen(PATH_LOG . "/im.log", "a+");
				fwrite($f, "[{$ret}] {$cmd}\n" . join('\n', $output) . "\n");
				fclose($f);
			}
		}
		else
		{
			trigger_error("Unsupported");
		}

		return new ximage($path, $this->use_image_magick);
	}

	/**
	 * @return ximage
	 */
	public function optimize($path)
	{
		if ($this->use_image_magick)
		{
			global $config;
			$cmd = '"' . $config["imagemagick_path"] . 'convert" -quality 80 ' . $this->src_path . ' ' . $path;

			exec($cmd, $output, $ret);
			if ($ret)
			{
				$f = fopen(PATH_LOG . "/im.log", "a+");
				fwrite($f, "[{$ret}] {$cmd}\n" . join('\n', $output) . "\n");
				fclose($f);
			}
		}
		else
		{
			trigger_error("Unsupported");
		}

		return new ximage($path, $this->use_image_magick);
	}

	public function get_width()
	{
		$this->load_image_attrs();
		return $this->width;
	}

	public function get_height()
	{
		$this->load_image_attrs();
		return $this->height;
	}

	public function width_max_test($constraint)
	{
		return ($this->get_width() <= $constraint);
	}

	public function width_min_test($constraint)
	{
		return ($this->get_width() >= $constraint);
	}

	public function height_max_test($constraint)
	{
		return ($this->get_height() <= $constraint);
	}

	public function height_min_test($constraint)
	{
		return ($this->get_height() >= $constraint);
	}

	public function size_min_test($min_width, $min_height)
	{
		return $this->width_min_test($min_width) && $this->height_min_test($min_height);
	}

	public function size_max_test($max_width, $max_height)
	{
		return $this->width_max_test($max_width) && $this->height_max_test($max_height);
	}

	public function is_horizontal()
	{
		return ($this->get_width() > $this->get_height());
	}

	protected function load_image_attrs()
	{
		if (!$this->data_loaded)
		{
			if (file_exists($this->src_path) and filesize($this->src_path))
			{
				list($width, $height, $type, $attr) = getimagesize($this->src_path);
				$this->width = $width;
				$this->height = $height;
				$this->image_type = $type;
				$this->mime_type = image_type_to_mime_type($type);
				$this->file_size = filesize($this->file_size);
			}
			$this->data_loaded = true;
		}
	}

	protected function load_gd_resource()
	{
		$this->load_image_attrs();

		$image_path = $this->src_path;

		switch ($this->image_type)
		{
			case IMAGETYPE_JPEG:
				{
					$this->image_resource = imagecreatefromjpeg($image_path);
					break;
				}
			case IMAGETYPE_GIF:
				{
					$this->image_resource = imagecreatefromgif($image_path);
					break;
				}
			case IMAGETYPE_PNG:
				{
					$this->image_resource = imagecreatefrompng($image_path);
					break;
				}
		}
		if ($this->image_resource)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}

?>