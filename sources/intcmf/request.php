<?php

/**
 * @uses auxil.php
 * 
 * @todo dm9 - iconv() russian params (see cioway)
 */
class request
{
	protected $main_prefix = false;
	protected $project_path = "";
	protected $require_trailing_slash = true;
	protected $remove_dynamical_params = true;
	protected $allowed_params_strings = array(); // "?" or "?xml" etc.
	protected $trailing_slash_exclusions = array();


	/* URI INFO */
	protected $protocol = "";
	protected $host = "";
	protected $host_raw = "";
	protected $port = "";
	protected $port_string = "";
	protected $request_uri = "";

	/**
	 * Folders without URL decode
	 * 
	 * @var array
	 */
	protected $raw_folders = array();
	protected $folders = array();
	protected $folders_string = ""; // / or /folder or /folder1/folder2
	protected $raw_folders_string = ""; //without urldecode
	protected $params_string = "";
	protected $trailing_slash = false;

	/* PROJECT PATH INFO */
	protected $prefix = "/";
	protected $full_prefix = "";
	protected $prefix_error = 0;

	/* REDIRECT INFO */
	protected $need_redirect = false;
	protected $canonical_path = "";

	/* AUX REQUEST INFO */
	protected $referrer = "";
	protected $referrer_title = null;
	protected $user_agent = "";
	protected $user_agent_name = ""; // "ie", "opera", "gecko" or "" (the last means that agent is not detected)
	protected $user_agent_version = "";
	protected $remote_addr = "";

	public function __construct($project_path, $require_trailing_slash = true, $remove_dynamical_params = true, $allowed_params_strings = array(), $trailing_slash_exclusions = array())
	{
		$this->project_path = $project_path;
		$this->require_trailing_slash = $require_trailing_slash;
		$this->remove_dynamical_params = $remove_dynamical_params;
		$this->allowed_params_strings = $allowed_params_strings;
		$this->trailing_slash_exclusions = $trailing_slash_exclusions;
		$this->process();
	}

	protected function process()
	{
		$this->load_uri_info();
		$this->load_path_info();
		$this->check_redirects();
		$this->load_aux_info();
	}

	private function load_uri_info()
	{
		// Protocol
		$this->protocol = isset($_SERVER["HTTPS"]) && !empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] !== "off" ? "https" : "http";

		// Host and port
		$host = "__undefined__";
		if (isset($_SERVER["HTTP_HOST"]))
		{
			$host = $_SERVER["HTTP_HOST"];
		}
		elseif (isset($_SERVER["SERVER_NAME"]))
		{
			$host = $_SERVER["SERVER_NAME"];
		}
		$host = explode(":", $host, 2);
		$this->host_raw = strtolower($host[0]);
		if (substr($this->host_raw, -1, 1) == ".")
		{
			// test - BEGIN
			$message = date("r") . "\t" . $this->host_raw . "\t" . (isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "-") . "\n";
			file_put_contents(PATH_LOG . "/strange_domain.log", $message, FILE_APPEND);
			// test - END
			$this->host_raw = substr($this->host_raw, 0, -1);
		}
		$this->port = isset($host[1]) ? (int) $host[1] : 80;
		if ($this->port != 80)
		{
			$this->port_string = ":" . $this->port;
		}

		$this->host = $this->host_raw . $this->port_string;

		// Request URI
		$this->request_uri = $_SERVER["REQUEST_URI"];

		// Cut params
		$params_pos = strpos($this->request_uri, "?");
		if ($params_pos !== false)
		{
			$this->params_string = substr($this->request_uri, $params_pos);
			$this->folders_string = univers_filename(substr($this->request_uri, 0, $params_pos));
		}
		else
		{
			$this->folders_string = univers_filename($this->request_uri);
		}

		if ($this->folders_string !== mb_convert_encoding(mb_convert_encoding($this->folders_string, "UTF-32", "UTF-8"), "UTF-8", "UTF-32"))
		{
			/**
			 * @todo encoding - from config
			 */
			$this->folders_string = iconv("WINDOWS-1251", "UTF-8", $this->folders_string);
		}

		// Trailing slash
		$this->trailing_slash = substr($this->folders_string, -1, 1) == "/";
		$this->folders_string = trailing_slash_remove($this->folders_string);

		// Folders
		$folders = explode("/", $this->folders_string);
		foreach ($folders as $folder)
		{
			if ($folder !== "")
			{
				$this->raw_folders[] = $folder;
				$this->folders[] = urldecode($folder);
			}
		}

		$this->raw_folders_string = $this->folders_string;
		$this->folders_string = urldecode($this->folders_string);
	}

	private function load_path_info()
	{
		$document_root = trailing_slash_remove(univers_filename(realpath($_SERVER["DOCUMENT_ROOT"])));
		$project_path = trailing_slash_remove(univers_filename(realpath($this->project_path)));
		if (substr($project_path, 0, strlen($document_root)) != $document_root)
		{
			$this->prefix_error = 1;
		}
		$prefix = substr($project_path, strlen($document_root));
		if (!$prefix)
		{
			$prefix = "";
		}
		$prefix_level = substr_count($prefix, "/");
		if (substr($this->folders_string, 0, strlen($prefix)) != $prefix)
		{
			$this->prefix_error = 2;
		}
		$this->prefix = $prefix;
		$this->full_prefix = $this->protocol . "://" . $this->host_raw . $this->port_string . $this->prefix;
		$this->folders_string = substr($this->folders_string, strlen($prefix));
		$this->raw_folders_string = substr($this->raw_folders_string, strlen($prefix));
		array_splice($this->folders, 0, $prefix_level);
		array_splice($this->raw_folders, 0, $prefix_level);
	}

	private function check_redirects()
	{
		if ($this->folders_string != ($this->folders ? "/" : "") . join("/", $this->folders))
		{
			$this->need_redirect = true;
		}

		$last_folder = end($this->folders);
		$require_trailing_slash = $this->require_trailing_slash;
		if (in_array($last_folder, $this->trailing_slash_exclusions))
		{
			$require_trailing_slash = !$require_trailing_slash;
		}

		if ($require_trailing_slash != $this->trailing_slash)
		{
			$this->need_redirect = true;
		}
		if ($this->params_string and $this->remove_dynamical_params and ! in_array($this->params_string, $this->allowed_params_strings))
		{
			$this->need_redirect = true;
		}
		if ($this->need_redirect)
		{
			$this->canonical_path = $this->protocol . "://" . $this->host_raw . $this->port_string . $this->prefix . "/" . join("/", $this->raw_folders) . ($require_trailing_slash && $this->folders ? "/" : "") . ((!$this->remove_dynamical_params or in_array($this->params_string, $this->allowed_params_strings)) ? $this->params_string : "");
		}
	}

	private function load_aux_info()
	{
		$this->referrer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "";
		$this->user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
		$this->remote_addr = $_SERVER["REMOTE_ADDR"];
		if (strpos($this->user_agent, "MSIE") !== false)
		{
			$this->user_agent_name = "ie";
			if (preg_match("/^[^(]+\([^(]+MSIE\s([0-9]{1,2}\.[0-9]{1,10})/", $this->user_agent, $matches))
			{
				$this->user_agent_version = $matches[1];
			}
		}
		if (strpos($this->user_agent, "Opera") !== false)
		{
			$this->user_agent_name = "opera";
		}
		if (strpos($this->user_agent, "Gecko") !== false)
		{
			$this->user_agent_name = "gecko";
		}
	}

	/**
	 * Returns protocol
	 * 
	 * @return string
	 */
	public function get_protocol($with_slashes = false)
	{
		return $this->protocol . ($with_slashes ? "://" : "");
	}

	/**
	 * Returns host
	 *
	 * @return string
	 */
	public function get_host()
	{
		return $this->host;
	}

	/**
	 * Returns host without 'www'
	 *
	 * @return string
	 */
	public function get_host_without_www()
	{
		if (substr($this->host, 0, 4) == "www.")
		{
			return substr($this->host, 4);
		}
		else
		{
			return $this->host;
		}
	}

	/**
	 * Return raw host
	 *
	 * @return string
	 */
	public function get_host_raw()
	{
		return $this->host_raw;
	}

	/**
	 * Return port
	 *
	 * @return string
	 */
	public function get_port()
	{
		return $this->port;
	}

	/**
	 * Return port string
	 *
	 * @return string
	 */
	public function get_port_string()
	{
		return $this->port_string;
	}

	/**
	 * Return full uri
	 *
	 * @return string
	 */
	public function get_full_request_uri()
	{
		return $this->protocol . "://" . $this->host_raw . $this->port_string . $this->request_uri;
	}

	/**
	 * Return request uri
	 *
	 * @return string
	 */
	public function get_request_uri()
	{
		return $this->request_uri;
	}

	/**
	 * Returns folders from array by number and sets the type
	 *
	 * @param int $number
	 * @param string $type
	 * @return mixed
	 */
	public function get_folders($index, $type = "string")
	{
		if (!is_good_id($index))
		{
			return false;
		}
		if (!isset($this->folders[$index - 1]))
		{
			return false;
		}
		$val = $this->folders[$index - 1];

		return settype($val, $type) ? $val : false;
	}

	public function get_all_folders($trailing_slash = true, $prefix = false)
	{
		$res = "";
		if ($prefix)
		{
			$res .= $this->get_prefix() . "/";
		}
		$res .= join("/", $this->folders);
		if ($res and $trailing_slash)
		{
			$res .= "/";
		}
		return $res;
	}

	public function get_all_folders_array()
	{
		return $this->folders;
	}

	public function folder_shift()
	{
		$this->folders_string = substr($this->folders_string, strpos($this->folders_string, "/", 1));
		$this->raw_folders_string = substr($this->raw_folders_string, strpos($this->raw_folders_string, "/", 1));
		return array_shift($this->folders);
	}

	/**
	 * Return raw folders string (without urldecode)
	 *
	 * @return string
	 */
	public function get_raw_folders_string()
	{
		return $this->raw_folders_string;
	}

	/**
	 * Return folders string
	 *
	 * @return string
	 */
	public function get_folders_string()
	{
		return $this->folders_string;
	}

	/**
	 * Return params string
	 *
	 * @return string
	 */
	public function get_params_string()
	{
		return $this->params_string;
	}

	/**
	 * Return trailing slash
	 *
	 * @return bool
	 */
	public function is_trailing_slash_exists()
	{
		return $this->trailing_slash;
	}

	/**
	 * Return prefix
	 *
	 * @return string
	 */
	public function get_prefix()
	{
		return $this->prefix;
	}

	/**
	 * Return full prefix (with protocol, host and port).
	 *
	 * @return string
	 */
	public function get_full_prefix()
	{
		return $this->protocol . "://" . $this->host_raw . $this->port_string . $this->prefix;
	}

	/**
	 * Return prefix error
	 *
	 * @return string
	 */
	public function is_prefix_error()
	{
		return $this->prefix_error;
	}

	/**
	 * Returns whether the redirect is neccessary
	 *
	 * @return bool
	 */
	public function is_redirect_needed()
	{
		return $this->need_redirect;
	}

	/**
	 * Return cannonical path
	 *
	 * @return string
	 */
	public function get_canonical_path()
	{
		return $this->canonical_path;
	}

	/**
	 * Returns referrer
	 *
	 * @return string
	 */
	public function get_referrer()
	{
		return $this->referrer;
	}

	/**
	 * Returns referrer page title (if exists).
	 *
	 * @return string
	 */
	public function get_referrer_title()
	{
		if ($this->referrer_title === null)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->referrer);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$page = curl_exec($ch);

			$page = str_replace("\r\n", "\n", $page);
			$page = str_replace("\r", "\n", $page);

			$page_split = explode("\n", $page);

			$headers = array();
			foreach ($page_split as $line)
			{
				if ($line == "")
				{
					break;
				}
				$header = explode(":", $line);
				if (count($header) == 2)
				{
					$header[0] = strtoupper(trim($header[0]));
					$header[1] = strtoupper(trim($header[1]));

					$headers[$header[0]] = $header[1];
				}
			}

			$encoding = false;
			if (false && isset($headers["CONTENT-TYPE"]))
			{
				ereg("CHARSET\=(.*)$", $headers["CONTENT-TYPE"], $encoding_match);
				if (isset($encoding_match[1]))
				{
					$encoding = $encoding_match[1];
				}
			}
			else
			{
				if (!preg_match("#\<meta[^>]*http\-equiv\=[\"']?content-type[\"']?[^>]*content=[\"']?text\/html\; charset\=([a-z0-9\-]*)[\"']?[^>]*\>#i", $page, $encoding_match))
				{
					preg_match("#\<meta[^>]*content=[\"']?text\/html\; charset\=([a-z0-9\-]*)[\"']?[^>]*http\-equiv\=[\"']?content-type[\"']?[^>]*\>#i", $page, $encoding_match);
				}
				if (isset($encoding_match[1]))
				{
					$encoding = strtoupper($encoding_match[1]);
				}
			}

			if ($encoding)
			{
				$page = iconv($encoding, "UTF-8", $page);
			}

			preg_match("#\<title[^>]*\>(.*?)\<\/title\>#i", $page, $match);

			$this->referrer_title = isset($match[1]) ? $match[1] : "";

			curl_close($ch);
		}

		return $this->referrer_title;
	}

	/**
	 * Return IP of client
	 *
	 * @return string
	 */
	public function get_remote_addr()
	{
		return $this->remote_addr;
	}

	/**
	 * Returns user agent
	 *
	 * @return string
	 */
	public function get_user_agent()
	{
		return $this->user_agent;
	}

	/**
	 * Returns user agent name ("ie", "opera" or "gecko")
	 *
	 * @return string
	 */
	public function get_user_agent_name()
	{
		return $this->user_agent_name;
	}

	/**
	 * Returns user agent version. Works now only for IE
	 *
	 * @return string
	 */
	public function get_user_agent_version()
	{
		return $this->user_agent_version;
	}

	/**
	 * Performs the check if the request was sent from the same domain.
	 * Can be used to protect the site from Cross-Site Request Forgery (CSRF)
	 * attacks.
	 *
	 * @return boolean
	 */
	public function is_cross_site_request($allow_empty_referrer = false)
	{
		if ($allow_empty_referrer and ! $this->referrer)
		{
			return false;
		}
		return substr($this->referrer, 0, strlen($this->full_prefix)) !== $this->full_prefix;
	}

	public function get_get_param($name)
	{
		return isset($_GET[$name]) && !is_array($_GET[$name]) ? (string) $_GET[$name] : null;
	}

	public function get_get_param_as_array($name)
	{
		return isset($_GET[$name]) && is_array($_GET[$name]) ? $_GET[$name] : array();
	}

	public function get_get_params_starting_with($name_begin)
	{
		return $this->get_params_starting_with($_GET, $name_begin);
	}

	public function get_post_param($name)
	{
		return isset($_POST[$name]) && !is_array($_POST[$name]) ? (string) $_POST[$name] : null;
	}

	public function get_post_param_as_array($name)
	{
		return isset($_POST[$name]) && is_array($_POST[$name]) ? $_POST[$name] : array();
	}

	public function get_post_params_starting_with($name_begin)
	{
		return $this->get_params_starting_with($_POST, $name_begin);
	}

	public function get_request_param($name)
	{
		return isset($_REQUEST[$name]) && !is_array($_REQUEST[$name]) ? (string) $_REQUEST[$name] : null;
	}

	public function get_request_param_as_array($name)
	{
		return isset($_REQUEST[$name]) && is_array($_REQUEST[$name]) ? $_REQUEST[$name] : array();
	}

	public function get_request_params_starting_with($name_begin)
	{
		return $this->get_params_starting_with($_REQUEST, $name_begin);
	}

	private function get_params_starting_with($arr, $name_begin)
	{
		$trunc_len = strlen($name_begin);
		$result = array();
		foreach ($arr as $key => $value)
		{
			if (str_begins($key, $name_begin))
			{
				$result[substr($key, $trunc_len)] = $value;
			}
		}
		return $result;
	}

}

function GET($name)
{
	global $request;
	/* @var $request request */
	return $request->get_get_param($name);
}

function POST($name)
{
	global $request;
	/* @var $request request */
	return $request->get_post_param($name);
}

function REQUEST($name)
{
	global $request;
	/* @var $request request */
	return $request->get_request_param($name);
}

function GET_AS_ARRAY($name)
{
	global $request;
	/* @var $request request */
	return $request->get_get_param_as_array($name);
}

function POST_AS_ARRAY($name)
{
	global $request;
	/* @var $request request */
	return $request->get_post_param_as_array($name);
}

function REQUEST_AS_ARRAY($name)
{
	global $request;
	/* @var $request request */
	return $request->get_post_param_as_array($name);
}

function GET_STARTING_WITH($name_begin)
{
	global $request;
	/* @var $request request */
	return $request->get_get_params_starting_with($name_begin);
}

function POST_STARTING_WITH($name_begin)
{
	global $request;
	/* @var $request request */
	return $request->get_post_params_starting_with($name_begin);
}

function REQUEST_STARTING_WITH($name_begin)
{
	global $request;
	/* @var $request request */
	return $request->get_request_params_starting_with($name_begin);
}

function COOKIE($name)
{
	return isset($_COOKIE[$name]) ? strval($_COOKIE[$name]) : null;
}

?>