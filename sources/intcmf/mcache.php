<?php

class mcache
{

	private $started = false;

	/**
	 * @var Memcache
	 */
	private $memcache;
	private $prefix = "";
	private $now;
	private $expire_default = 432000; // 5 days

	public function __construct($memcache_host, $memcache_port = "11211", $expire_default = null)
	{
		if (!is_null($memcache_host))
		{
			$this->memcache = new Memcache;
			if (!$this->memcache->connect($memcache_host, $memcache_port))
			{
				trigger_error("Cannot connect to the memcache server ({$memcache_host}:{$memcache_port})");
			}
			$this->started = true;
		}
		if (!is_null($expire_default))
		{
			$this->expire_default = $expire_default;
		}
		$this->now = str_replace(",", ".", microtime(true));
	}

	public function set_prefix($prefix)
	{
		$this->prefix = $prefix === "" ? $prefix : ($prefix . "|");
	}

	public function add_raw($key, $value, $expire = null)
	{
		if (is_null($expire))
		{
			$expire = $this->expire_default;
		}
		return $this->memcache->add($this->make_prefix($key), $value, 0, $expire);
	}

	public function set_raw($key, $value, $expire = null)
	{
		if (is_null($expire))
		{
			$expire = $this->expire_default;
		}
		if ($this->memcache->replace($this->make_prefix($key), $value, 0, $expire) === false)
		{
			return $this->memcache->set($this->make_prefix($key), $value, 0, $expire);
		}
		return true;
	}

	public function get_raw($key)
	{
		return $this->unmake_prefix($this->memcache->get($this->make_prefix($key)));
	}

	public function delete_raw($key)
	{
		return $this->memcache->delete($this->make_prefix($key));
	}

	public function set_no_tags($key, $value, $expire = null)
	{
		$key = $this->normalize_key($key);
		return $this->set_raw($key, $value, $expire);
	}

	public function get_no_tags($key)
	{
		$key = $this->normalize_key($key);
		return $this->unmake_prefix($this->memcache->get($this->make_prefix($key)));
	}

	public function get_multi_no_tags($keys, $is_tag_keys = false)
	{
		$result = array();
		if (!empty($keys))
		{
			$result = $this->normalize_keys($keys, true, $is_tag_keys);
			$result_raw = $this->unmake_prefix($this->memcache->get($this->make_prefix($keys)));
			$result = array_merge($result, $result_raw);
		}
		return $result;
	}

	public function tag_update($tag_key)
	{
		$tag_key = $this->normalize_key($tag_key, true);
		return $this->set_raw($tag_key, $this->now);
	}

	public function tags_get($tag_keys)
	{
		return $this->get_multi_no_tags($tag_keys, true);
	}

	public function set($key, $value, $tag_keys = null, $expire = null)
	{
		$key = $this->normalize_key($key);
		$tags = $this->tags_get($tag_keys);
		foreach ($tags as $idx => &$val)
		{
			if (is_null($val))
			{
				$val = $this->now;
				$this->add_raw($idx, $val);
			}
		}
		$data = array("val" => $value, "tags" => $tags);
		return $this->set_raw($key, $data, $expire);
	}

	public function get($key)
	{
		$key = $this->normalize_key($key);
		$data = $this->get_raw($key);
		if (!$data or ! isset($data["val"]) or ! isset($data["tags"]))
		{
			return null;
		}
		$tags = $this->unmake_prefix($this->memcache->get($this->make_prefix(array_keys($data["tags"]))));
		foreach ($data["tags"] as $idx => $val)
		{
			if (!isset($tags[$idx]))
			{
				return null;
			}
			if ((float) $tags[$idx] > (float) $val)
			{
				return null;
			}
		}
		return $data["val"];
	}

	public function delete($key)
	{
		$key = $this->normalize_key($key);
		return $this->delete_raw($key);
	}

	public function get_multi($keys)
	{
		$result = array();
		$result = $this->normalize_keys($keys, true);
		$result_raw = $this->unmake_prefix($this->memcache->get($this->make_prefix($keys)));

		$tag_keys_normalized = array();
		foreach ($result as $result_key => &$result_val)
		{
			if (isset($result_raw[$result_key]["tags"]))
			{
				foreach ($result_raw[$result_key]["tags"] as $tag_key => $val)
				{
					$tag_keys_normalized[$tag_key] = true;
				}
			}
		}
		$tag_keys_normalized = array_keys($tag_keys_normalized);
		$tags = $this->unmake_prefix($this->memcache->get($this->make_prefix($tag_keys_normalized)));

		foreach ($result as $result_key => &$result_val)
		{
			if (!isset($result_raw[$result_key]) or ! isset($result_raw[$result_key]["val"]) or ! isset($result_raw[$result_key]["tags"]))
			{
				$result_val = null;
				continue;
			}
			foreach ($result_raw[$result_key]["tags"] as $idx => $val)
			{
				if (!isset($tags[$idx]))
				{
					$result_val = null;
					continue 2;
				}
				if ((float) $tags[$idx] > (float) $val)
				{
					$result_val = null;
					continue 2;
				}
			}
			$result_val = $result_raw[$result_key]["val"];
		}
		return $result;
	}

	public function flush()
	{
		return $this->memcache->flush();
	}

	private function normalize_key($key, $is_tag_key = false)
	{
		if (is_array($key))
		{
			$key = join("#", $key);
		}
		else
		{
			$key = (string) $key;
		}
		return $is_tag_key ? "%" . $key : $key;
	}

	private function normalize_keys(&$keys, $return_blank_array = false, $is_tag_keys = false)
	{
		if (!is_array($keys))
		{
			return false;
		}
		$result = array();
		foreach ($keys as &$key)
		{
			$key = $this->normalize_key($key, $is_tag_keys);
			if ($return_blank_array)
			{
				$result[$key] = null;
			}
		}
		return $return_blank_array ? $result : true;
	}

	private function make_prefix($data)
	{
		if (is_array($data))
		{
			foreach ($data as $key => $value)
			{
				$data[$key] = $this->prefix . $value;
			}
		}
		else
		{
			$data = $this->prefix . $data;
		}
		return $data;
	}

	private function unmake_prefix($data)
	{
		$data_new = array();
		if (is_array($data))
		{
			foreach ($data as $key => $value)
			{
				if (substr($key, 0, strlen($this->prefix)) == $this->prefix)
				{
					$data_new[substr($key, strlen($this->prefix))] = $value;
				}
				else
				{
					$data_new[$key] = $value;
				}
			}
		}
		else
		{
			$data_new = $data;
		}

		return $data_new;
	}

	public function increase_now_for_test()
	{
		$this->now = str_replace(",", ".", 0.0001 + (float) $this->now);
	}

}

?>
