<?php

/**
 * @uses xnode class
 *
 */
class paging
{

	/**
	 * @var xnode
	 */
	protected $parent_node;

	public function __construct(xnode $parent_node)
	{
		$this->parent_node = $parent_node;
	}

	/**
	 * Creates pages nodes. It is 'pages' node with 'page' subnodes. 'page' subnode has attributes:
	 *   @type - ['page' | 'current' | 'delimiter' | 'prev' | 'next']
	 *   [optional] @number - number of page
	 *
	 * $neighbour_pages - count of page links displayed before and after the current page
	 * (other pages after the first one and before last will be replaced by delimeter)
	 * 
	 * @param int $page_count
	 * @param int $current_page
	 * @param int $neighbour_pages
	 * @return xnode
	 */
	public function process($page_count, $current_page, $neighbour_pages = 1)
	{
		$pages_node = $this->parent_node->create_child_node("pages");
		$pages_node->set_attr("current_page", $current_page);
		$pages_node->set_attr("page_count", $page_count);

		if ($page_count == -1)
		{
			$page_count = $current_page + 1;
		}
		
		if ($page_count < 2)
		{
			return $pages_node;
		}


		$page_from = $first_page = 1;
		$page_to = $last_page = $page_count;

		if ($current_page - $page_from > $neighbour_pages)
		{
			$page_from = $current_page - $neighbour_pages;
		}

		if ($page_to - $current_page > $neighbour_pages)
		{
			$page_to = $current_page + $neighbour_pages;
		}

		if ($page_from == 3)
		{
			$page_from--;
		}

		if ($page_to == $last_page - 2)
		{
			$page_to++;
		}

		if ($first_page != $current_page)
		{
			$pages_node->create_child_node("page")->set_attr("type", "prev")->set_attr("number", $current_page - 1);
		}

		if ($first_page != $page_from)
		{
			$pages_node->create_child_node("page")->set_attr("type", "page")->set_attr("number", $first_page);

			if ($page_from != 2)
			{
				$pages_node->create_child_node("page")->set_attr("type", "delimeter");
			}
		}

		for ($i = $page_from; $i <= $page_to; $i++)
		{
			$type = $i == $current_page ? "current" : "page";
			$pages_node->create_child_node("page")->set_attr("type", $type)->set_attr("number", $i);
		}

		if ($last_page != $page_to)
		{
			if ($page_to != $last_page - 1)
			{
				$pages_node->create_child_node("page")->set_attr("type", "delimeter");
			}

			$pages_node->create_child_node("page")->set_attr("type", "page")->set_attr("number", $last_page);
		}

		if ($last_page != $current_page)
		{
			$pages_node->create_child_node("page")->set_attr("type", "next")->set_attr("number", $current_page + 1);
		}

		return $pages_node;
	}

}

?>