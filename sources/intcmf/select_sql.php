<?php

class select_sql
{

	protected $select_fields_array = array();
	protected $from_array = array();
	protected $join_array = array();
	protected $where_array = array();
	protected $where_in_array = array();
	protected $group_by_array = array();
	protected $having_array = array();
	protected $order_array = array();
	protected $limit = null;
	protected $select_modifier_array = array();
	protected $terminal_modifier_array = array();

	public function __construct($from = null, $alias = "")
	{
		if (!is_null($from))
		{
			$this->add_from($from, $alias);
		}
	}

	public function add_select_fields($select_fields)
	{
		$this->select_fields_array[] = $select_fields;
	}

	public function add_from($from, $alias = "")
	{
		if (substr($from, 0, 1) !== "`" and strpos($from, " ") === false)
		{
			$from = "`" . $from . "`";
		}
		if ($alias)
		{
			$from .= " " . $alias;
		}
		$this->from_array[] = $from;
	}

	public function add_join($join)
	{
		$this->join_array[] = $join;
	}

	public function add_where($where)
	{
		if ($where)
		{
			$this->where_array[] = $where;
		}
	}

	public function add_where_in($column, array $keys)
	{
		if (count($keys))
		{
			if (!isset($this->where_in_array[$column]))
			{
				$this->where_in_array[$column] = array();
			}
			foreach ($keys as $key)
			{
				$this->where_in_array[$column][] = $key;
			}
		}
		else
		{
			$this->add_where("FALSE");
		}
	}

	public function add_group_by($group_by)
	{
		$this->group_by_array[] = $group_by;
	}

	public function add_having($having)
	{
		$this->having_array[] = $having;
	}
	
	public function clear_order()
	{
		$this->order_array = array();
	}

	public function add_order($order)
	{
		$this->order_array[] = $order;
	}

	public function prepend_order($order)
	{
		array_unshift($this->order_array, $order);
	}
	
	public function set_limit($limit)
	{
		$this->limit = $limit;
	}

	public function add_select_modifier($modifier)
	{
		$this->select_modifier_array[] = $modifier;
	}

	public function add_terminal_modifier($modifier)
	{
		$this->terminal_modifier_array[] = $modifier;
	}

	public function set_SQL_CALC_FOUND_ROWS_modifier()
	{
		$this->select_modifier_array[] = "SQL_CALC_FOUND_ROWS";
	}

	public function set_DISTINCT_modifier()
	{
		$this->select_modifier_array[] = "DISTINCT";
	}

	public function set_FOR_UPDATE_modifier()
	{
		$this->terminal_modifier_array[] = "FOR UPDATE";
	}

	public function set_LOCK_IN_SHARE_MODE_modifier()
	{
		$this->terminal_modifier_array[] = "LOCK IN SHARE MODE";
	}

	public function get_select_fields()
	{
		return empty($this->select_fields_array) ? "*" : join(", ", $this->select_fields_array);
	}

	public function get_from($raw = false)
	{
		return ($raw ? "" : "FROM ") . join(", ", $this->from_array);
	}

	public function get_joins()
	{
		return empty($this->join_array) ? "" : join("\n", $this->join_array);
	}

	public function get_where($raw = false)
	{
		$wheres = array();
		foreach ($this->where_array as $where)
		{
			$wheres[] = "({$where})";
		}
		foreach ($this->where_in_array as $column => $keys)
		{
			$wheres[] = "{$column} IN (" . implode(",", $keys) . ")";
		}
		return empty($wheres) ? "" : ($raw ? "" : "WHERE ") . join(" AND ", $wheres);
	}

	public function get_group_by($raw = false)
	{
		return empty($this->group_by_array) ? "" : ($raw ? "" : "GROUP BY ") . join(", ", $this->group_by_array);
	}

	public function get_having($raw = false)
	{
		$havings = array();
		foreach ($this->having_array as $having)
		{
			$havings[] = "({$having})";
		}
		return empty($havings) ? "" : ($raw ? "" : "HAVING ") . join(" AND ", $havings);
	}

	public function get_order($raw = false)
	{
		return empty($this->order_array) ? "" : ($raw ? "" : "ORDER BY ") . join(", ", $this->order_array);
	}

	public function get_limit($raw = false)
	{
		return is_null($this->limit) ? "" : ($raw ? "" : "LIMIT ") . $this->limit;
	}

	public function get_select_modifiers()
	{
		return empty($this->select_modifier_array) ? "" : join("\n", $this->select_modifier_array);
	}

	public function get_terminal_modifiers()
	{
		return empty($this->terminal_modifier_array) ? "" : join("\n", $this->terminal_modifier_array);
	}

	public function get_sql()
	{
		if (!sizeof($this->from_array))
		{
			return "'FROM' is empty";
		}
		$sql = "
SELECT 
{$this->get_select_modifiers()}
{$this->get_select_fields()}
{$this->get_from()}
{$this->get_joins()}
{$this->get_where()}
{$this->get_group_by()}
{$this->get_having()}
{$this->get_order()}
{$this->get_limit()}
{$this->get_terminal_modifiers()}
		";
		return $sql;
	}

}

?>