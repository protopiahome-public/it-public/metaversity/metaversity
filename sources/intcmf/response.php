<?php

define("REDIRECT_301_MOVED_PERMANENTLY", "HTTP/1.1 301 Moved Permanently");
define("REDIRECT_302_MOVED_TEMPORARILY", "HTTP/1.1 302 Moved Temporarily");
define("REDIRECT_302_FOUND", "HTTP/1.1 302 Found");

class response
{

	public static function set_redirect($redirect_url, $redirect_type = REDIRECT_302_FOUND)
	{
		output_buffer::set_main_header($redirect_type);
		output_buffer::set_header("Location", $redirect_url);
	}

	public static function set_redirect_permanent($redirect_url)
	{
		self::set_redirect($redirect_url, REDIRECT_301_MOVED_PERMANENTLY);
	}

	public static function set_redirect_temporary($redirect_url)
	{
		self::set_redirect($redirect_url, REDIRECT_302_MOVED_TEMPORARILY);
	}

	public static function set_redirect_simple($redirect_url)
	{
		self::set_redirect($redirect_url, REDIRECT_302_FOUND);
	}

	public static function set_content($content, $content_type = null)
	{
		if ($content_type)
		{
			self::set_content_type($content_type);
		}
		output_buffer::set_content($content);
	}
	
	public static function append_content($additional_content)
	{
		output_buffer::append_content($additional_content);
	}

	public static function set_content_html($content_html, $encoding = "UTF-8")
	{
		self::set_content_type_html($encoding);
		self::set_content($content_html);
	}

	public static function set_content_text($content_text, $encoding = "UTF-8")
	{
		self::set_content_type_text($encoding);
		self::set_content($content_text);
	}

	public static function set_content_xml($content_xml, $encoding = "UTF-8")
	{
		self::set_content_type_xml($encoding);
		self::set_content($content_xml);
	}

	public static function set_content_xslt($content_xslt, $encoding = "UTF-8")
	{
		self::set_content_type_xslt($encoding);
		self::set_content($content_xslt);
	}

	public static function set_content_js($content_js, $encoding = "UTF-8")
	{
		self::set_content_type_js($encoding);
		self::set_content($content_js);
	}

	public static function set_content_type($content_type, $encoding = null)
	{
		if (is_null($encoding))
		{
			output_buffer::set_header("Content-Type", "{$content_type}");
		}
		else 
		{
			output_buffer::set_header("Content-Type", "{$content_type}; charset={$encoding}");
		}
	}

	public static function set_content_type_html($encoding = "UTF-8")
	{
		self::set_content_type("text/html", $encoding);
	}

	public static function set_content_type_text($encoding = "UTF-8")
	{
		self::set_content_type("text/plain", $encoding);
	}

	public static function set_content_type_xml($encoding = "UTF-8")
	{
		self::set_content_type("text/xml", $encoding);
	}

	public static function set_content_type_xslt($encoding = "UTF-8")
	{
		self::set_content_type("application/xslt+xml", $encoding);
	}

	public static function set_content_type_js($encoding = "UTF-8")
	{
		self::set_content_type("text/javascript", $encoding);
	}
	
	public static function set_content_type_json($encoding = "UTF-8")
	{
		self::set_content_type("application/json", $encoding);
	}
	
	public static function set_content_type_attach()
	{
		self::set_content_type("application/octet-stream");
	}

	public static function set_error_403()
	{
		output_buffer::set_main_header("HTTP/1.1 403 Forbidden");
	}

	public static function set_error_404()
	{
		output_buffer::set_main_header("HTTP/1.1 404 Not Found");
	}

	public static function set_error_500()
	{
		output_buffer::set_main_header("HTTP/1.1 500 Internal Server Error");
	}
	
	public static function set_no_cache_headers()
	{
		output_buffer::set_header("Expires", "Mon, 26 Jul 1997 00:00:00 GMT");
		output_buffer::set_header("Cache-Control", "no-cache, no-store, must-revalidate");
		output_buffer::set_header("Pragma", "no-cache");
	}

}

?>