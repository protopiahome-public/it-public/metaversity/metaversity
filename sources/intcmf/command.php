<?php

class command
{

	protected static $prerun_command = null;
	protected $command;
	protected $timeout = 0;
	protected $output;
	protected $return_code;

	public function __construct($command)
	{
		$this->command = $command;
	}

	public static function set_prerun_command($prerun_command = null)
	{
		self::$prerun_command = $prerun_command;
	}

	/**
	 * Will require 'timeout' external program (available on Linux)
	 * See https://www.gnu.org/software/coreutils/manual/html_node/timeout-invocation.html
	 */
	public function set_timeout($timeout, $allow_unsupported_timeout = false)
	{
		if ($allow_unsupported_timeout and !self::supports_timeout())
		{
			$this->timeout = 0;
		}
		else
		{
			$this->timeout = (int) $timeout;
		}
	}

	public function exec()
	{
		$cmd = $this->command;
		if ($this->timeout > 0)
		{
			$cmd = "timeout {$this->timeout} " . $cmd;
		}
		if (self::$prerun_command)
		{
			exec(self::$prerun_command);
		}
		exec($cmd, $this->output, $this->return_code);
		//echo "[{$cmd}]==>[{$this->return_code}]\n";
	}

	public function is_ok()
	{
		return $this->return_code === 0;
	}

	public function is_timeout_exit()
	{
		return $this->return_code === 137;
	}

	public function get_output()
	{
		return join("\n", $this->output);
	}

	public function get_return_code()
	{
		return $this->return_code;
	}

	public static function supports_timeout()
	{
		static $supports_timeout = null;
		if ($supports_timeout === null)
		{
			if (is_windows())
			{
				$supports_timeout = false;
			}
			else
			{
				$output = null;
				$return_code = null;
				exec("timeout 2 ls", $output, $return_code);
				$supports_timeout = 0 === $return_code;
			}
		}
		return $supports_timeout;
	}

}

?>