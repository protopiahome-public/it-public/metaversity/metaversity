<?php

require_once dirname(__FILE__) . "/../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";
require_once PATH_INTCMF . "/command.php";

$PATH_HOSTS = PATH_TMP . "/letsencrypt.hosts";
$PATH_LOG = PATH_LOG . "/letsencrypt.log";

$start_time = microtime(true);

/* @var $db db */

$force = isset($_SERVER["argv"][3]) && "force" === $_SERVER["argv"][3];

$hosts = $db->fetch_column_values("SELECT name FROM stream ORDER BY name");
$hosts[] = "students";
$hosts[] = "users";
$hosts[] = "www";

$new_hosts = get_hosts_str($hosts);
if (!$force)
{
	$old_hosts = file_get_contents_safe($PATH_HOSTS);
	if ($old_hosts === $new_hosts)
	{
		die(1);
	}
}

if (!isset($config["letsencrypt_cmd"]) || !$config["letsencrypt_cmd"])
{
	die(2);
}

$cmd = $config["letsencrypt_cmd"];
$cmd .= ' -n';
$cmd .= ' --expand';
$cmd .= ' --allow-subset-of-names';
$cmd .= ' --agree-tos';
$cmd .= ' -m ' . $config["letsencrypt_email"];
$cmd .= ' certonly --webroot -w ' . realpath(PATH_WWW);
$cmd .= ' -d ' . $config["letsencrypt_main_host"];
foreach ($hosts as $host)
{
	$cmd .= ' -d ' . $host . '.' . $config["letsencrypt_main_host"];
}

write_log_str("[START] " . date("c") . "\n");
write_log("", $cmd);

$command = new command($cmd);
$command->exec();
write_log("", "", $command);

if ($command->is_ok())
{
	file_put_contents_safe($PATH_HOSTS, $new_hosts);

	$set_name = get_latest_set_name();
	if ($set_name)
	{
		$latest_dir = "{$config["letsencrypt_cert_dir"]}/live/{$config["letsencrypt_main_host"]}-latest";
		$certs_dir = "{$config["letsencrypt_cert_dir"]}/live/{$set_name}";
		$cmd_rename = "rm -f \"{$latest_dir}\"; ln -sf {$certs_dir} {$latest_dir}";
		$command_rename = new command($cmd_rename);
		$command_rename->exec();
		write_log("[RENAME]", $cmd_rename, $command_rename);
		
		cleanup($config["letsencrypt_cert_dir"] . "/live", $set_name);
		cleanup($config["letsencrypt_cert_dir"] . "/archive", $set_name);
		cleanup($config["letsencrypt_cert_dir"] . "/renewal", $set_name, ".conf");
	}
	
	$command_restart = new command($config["letsencrypt_restart"]);
	$command_restart->exec();
	write_log("[RESTART]", $config["letsencrypt_restart"], $command_restart);
}

write_log_str("[FINISH] Time=" . (microtime(true) - $start_time) . "\n\n");

function cleanup($dir, $latest_set_name, $suffix = "")
{
	$files = scan_dir($dir, $suffix);
	if (isset($files[$latest_set_name]))
	{
		foreach ($files as $set_name => $file_path)
		{
			if ($set_name != $latest_set_name and file_exists($file_path))
			{
				is_dir($file_path) ? full_rmdir($file_path) : unlink($file_path);
				write_log("[CLEANUP]", "Removing {$file_path}");
			}
		}
	}
}

function write_log($header = null, $command_str = null, command $command = null)
{
	$log = "";
	if ($header)
	{
		$log .= "{$header}" . "\n";
	}
	if ($command_str)
	{
		$log .= "COMMAND: " . $command_str . "\n";
	}
	if ($command)
	{
		$log .= "RETCODE: " . $command->get_return_code() . "\n";
		$out = trim($command->get_output());
		$log .= $out ? $out . "\n" : "";
	}
	write_log_str($log);
}

function write_log_str($str)
{
	global $PATH_LOG;
	file_put_contents_safe($PATH_LOG, $str, true);
}

function get_hosts_str($hosts)
{
	return join(",", $hosts);
}

function get_latest_set_name()
{
	global $config;

	$latest_set_name = false;
	$latest_mtime = -1;
	$live_dir = $config["letsencrypt_cert_dir"] . "/live";
	foreach (scan_dir($live_dir) as $set_name => $dir_path)
	{
		if (is_dir($dir_path))
		{
			$cert_file_path = $dir_path . "/cert.pem";
			if (file_exists($cert_file_path))
			{
				$mtime = filemtime($cert_file_path);
				if ($mtime > $latest_mtime)
				{
					$latest_mtime = $mtime;
					$latest_set_name = $set_name;
				}
			}
		}
	}
	return $latest_set_name;
}

function scan_dir($dir, $suffix = "")
{
	global $config;
	
	$result = array();
	foreach (scandir($dir) as $file_name)
	{
		$file_path = $dir . "/" . $file_name;
		$host_escaped = preg_quote($config["letsencrypt_main_host"]);
		$suffix_escaped = preg_quote($suffix);
		if (preg_match("#^({$host_escaped}(-\d+)?){$suffix_escaped}$#", $file_name, $matches))
		{
			$set_name = $matches[1];
			$result[$set_name] = $file_path;
		}
	}
	return $result;
}

?>