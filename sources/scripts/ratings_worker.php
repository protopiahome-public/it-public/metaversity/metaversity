<?php

require_once dirname(__FILE__) . "/../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once PATH_INTCMF . "/beanstalk.php";
/* @var $db db */

file_put_contents(PATH_TMP . "/ratings-worker-" . getmypid(), "");

$bt = new beanstalk($config["beanstalk_host"], $config["beanstalk_port"]);
$bt->connect();
if (!$bt->connect())
{
	die("Not connected");
}
$bt->watch("mv-ratings");

echo "Connected (pid=" . getmypid() . ")\n";

do
{
	echo "Waiting...\n";
	$db->disconnect();
	$task = $bt->reserve();
	echo "Received stream_id: {$task["body"]}\n";
	$bt->bury($task["id"], 0);
	$xerror->is_error_occured() ? process_errors($task["body"], true) : null;
	write_log("Received\t{$task["body"]}");
	$start_time = microtime(true);
	math_rating_cache_helper::build_cache($task["body"]);
	write_log("Completed\t{$task["body"]}\t" . (microtime(true) - $start_time));
	$xerror->is_error_occured() ? process_errors($task["body"]) : $bt->delete($task["id"]);
}
while (true);

function write_log($message)
{
	$text = "";
	$text .= date("c") . "\t";
	$text .= getmypid() . "\t";
	$text .= $message . "\n";
	file_put_contents_safe(PATH_LOG . "/ratings.log", $text, true);
}

function process_errors($id = 0, $die = false)
{
	global $xerror;
	/* @var $xerror xerror */
	$html = $xerror->get_errors_html();
	file_put_contents(PATH_LOG . "/errors/ratings_worker_{$id}_" . date("Y-m-d_H-i-s") . ".html", $html);
	if (preg_match("/<!--(.*?)-->/s", $html, $matches))
	{
		echo $matches[1];
	}
	else
	{
		echo "Error occured (error html can not be parsed)\n";
	}
	$xerror->clear_errors();
	if ($die)
	{
		die(7);
	}
}

?>