<?php

require_once dirname(__FILE__) . "/../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

if (isset($_SERVER["argv"][3]) && is_good_id($_SERVER["argv"][3]))
{
	$stream_id = $_SERVER["argv"][3];
	$start_time = microtime(true);
	math_rating_cache_helper::build_cache($stream_id, true);
	write_log("Precalculated\t{$stream_id}\t" . (microtime(true) - $start_time));
	die;
}

if (!$config["beanstalk_host"])
{
	echo "Beanstalk is not configured\n";
	die(2);
}

$THREADS = 1;

$restart = isset($_SERVER["argv"][3]) && $_SERVER["argv"][3] === "restart";
$stop = isset($_SERVER["argv"][3]) && $_SERVER["argv"][3] === "stop";

$count = 0;
foreach (scandir(PATH_TMP) as $file_name)
{
	if (!preg_match("/^ratings-worker-(\d+)$/", $file_name, $matches))
	{
		continue;
	}
	$pid = $matches[1];
	if (file_exists("/proc/" . $pid))
	{
		// Additional check
		if (trim(file_get_contents("/proc/{$pid}/comm")) === "php")
		{
			if ($stop or $restart)
			{
				// Need to kill them
				echo "Killing {$pid}\n";
				exec("kill -9 {$pid}");
			}
			else
			{
				// Process is running
				++$count;
			}
		}
	}
	else
	{
		// Process is dead
		@unlink(PATH_TMP . "/" . $file_name);
	}
}

if (!$stop)
{
	for (; $count < $THREADS; ++$count)
	{
		$out = array();
		exec('nohup php ' . dirname(__FILE__) . "/ratings_worker.php" . ' ' . $_SERVER["argv"][1] . ' ' . $_SERVER["argv"][2] . ' > /dev/null 2>&1 & echo $!', $out);
		if (!is_array($out) or ! isset($out[0]))
		{
			$out = array("ERROR");
		}
		echo "Started new process {$out[0]}\n";
	}
}

function write_log($message)
{
	$text = "";
	$text .= date("c") . "\t";
	$text .= "-" . "\t";
	$text .= $message . "\n";
	file_put_contents_safe(PATH_LOG . "/ratings.log", $text, true);
}

?>