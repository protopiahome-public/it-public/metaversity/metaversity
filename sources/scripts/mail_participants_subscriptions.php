<?php

require_once dirname(__FILE__) . "/../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once(PATH_MODULE_SITE_LIB . "/mail/mail_html.php");

/* @var $db db */

$debug = isset($_SERVER["argv"][3]) && $_SERVER["argv"][3] === "-debug";

$requests = $db->fetch_all("
	SELECT activity_id, user_id, role_id, email_to_send
	FROM activity_participant
	WHERE status = 'premoderation'
		AND change_time < DATE_SUB(NOW(), INTERVAL 15 MINUTE)
		AND email_to_send <> ''
");

foreach ($requests as $request)
{
	$activity_id = $request["activity_id"];
	$user_id = $request["user_id"];
	$role_id = $request["role_id"];
	$body = $request["email_to_send"];

	if (!$body)
	{
		continue;
	}
	$users = activity_participants_subscriptions_helper::fetch_participants_subscriptions_to_send($activity_id);
	$emails = get_emails(array_keys($users));
	foreach ($emails as $email)
	{
		if ($debug)
		{
			echo "{$email}: {$activity_id} {$user_id}\n";
		}
		else
		{
			$html_email = new mail_html($email, $config["from"], null, $body);
			$html_email->send();

			$log = date("r") . "\t{$email}\t{$activity_id}\t{$user_id}\n";
			file_put_contents_safe(PATH_LOG . "/stream_admin_participants_subscriptions_mail.log", $log, true);
		}
	}

	$requests = $db->sql("
		UPDATE activity_participant
		SET email_to_send = NULL
		WHERE activity_id = {$activity_id}
			AND role_id = {$role_id}
			AND user_id = {$user_id}
	");
}

function get_emails($user_ids)
{
	global $db;
	static $cache = array();
	$new_ids = array();
	foreach ($user_ids as $user_id)
	{
		if (!isset($cache[$user_id]))
		{
			$new_ids[] = $user_id;
		}
	}
	if (sizeof($new_ids))
	{
		$new_emails = $db->fetch_column_values("
			SELECT id, email
			FROM user
			WHERE id IN (" . join(", ", $new_ids) . ")
		", "email", "id");
		array_merge_good($cache, $new_emails);
	}
	$emails = array();
	foreach ($user_ids as $user_id)
	{
		if (isset($cache[$user_id]))
		{
			$emails[] = $cache[$user_id];
		}
	}
	return $emails;
}

?>