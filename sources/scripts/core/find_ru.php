<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

$directories = array(
	array(
		"dir" => PATH_XSLT,
		"ext" => ".xslt",
	),
	array(
		"dir" => PATH_WWW . "/js",
		"ext" => ".js",
	),
	array(
		"dir" => PATH_WWW . "/css",
		"ext" => ".css",
	),
	array(
		"dir" => PATH_SOURCES,
		"ext" => ".php",
	),
);

$search = array();
$i = 3;
while (isset($_SERVER["argv"][$i]))
{
	$search[] = $_SERVER["argv"][$i];
	++$i;
}

foreach ($directories as $directory_data)
{
	$files = dir_list_plain($directory_data["dir"], true);
	foreach ($files as $file_path)
	{
		foreach ($search as $term)
		{
			if (false === strpos($file_path, $term))
			{
				continue 2;
			}
		}
		if (str_ends($file_path, $directory_data["ext"]))
		{
			$f = fopen($file_path, "r");
			$line_num = 0;
			while (false !== ($line = fgets($f)))
			{
				++$line_num;
				if (preg_match("/[абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ]/u", $line))
				{
					echo $file_path . " : " . $line_num . "\n";
				}
			}
		}
	}
}
?>