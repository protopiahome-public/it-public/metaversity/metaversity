<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

function check_files($dir, $types)
{
	if (strstr($dir, "tiny_mce"))
	{
		return;
	}
	$files = scandir($dir);

	foreach ($files as $file_name)
	{
		$file_path = $dir . "/" . $file_name;
		if ($file_name == "." || $file_name == "..")
		{
			continue;
		}

		$matches = null;
		if (is_dir($file_path))
		{
			check_files($file_path, $types);
		}
		elseif (preg_match("/\.([^.]+)$/", $file_name, $matches))
		{
			$ext = $matches[1];
			if (in_array($ext, $types))
			{
				$content = file_get_contents($file_path);
				if (strstr($content, "\r\n") !== false)
				{
					$content = str_replace("\r\n", "\n", $content);
					file_put_contents($file_path, $content);
					echo $file_path . "\n";
				}
			}
		}
	}
}

$types = array("php", "xml", "xslt", "js", "htaccess");
check_files(PATH_HOME, $types);
?>