<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

__autoload_debug_save();

$used = array();
$used_file_path = PATH_TMP . "/autoload_list.log";
if (file_exists($used_file_path))
{
	$lines = explode("\n", file_get_contents_safe($used_file_path));
	foreach ($lines as $line)
	{
		$used[trim($line)] = true;
	}
}

$files = dir_list_plain(PATH_MODULES, true);
$count = 0;
foreach ($files as $file_path)
{
	if (!strpos($file_path, "/autoloaded/"))
	{
		continue;
	}
	$file_path = univers_filename($file_path, true);
	if (isset($used[$file_path]))
	{
		continue;
	}
	++$count;
	echo $file_path . "\n";
}
echo "TOTAL: {$count} files\n";
?>