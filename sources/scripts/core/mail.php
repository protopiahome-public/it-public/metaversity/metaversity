<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once(PATH_MODULE_SITE_LIB . "/mail/mail_html.php");

$global_start_time = microtime(true);

$emails = $db->fetch_all("SELECT * FROM email ORDER BY id LIMIT 64");

$k = 0;
$k_ok = 0;
foreach ($emails as $email)
{
	$html_email = new mail_html($email["to_email"], $config["from"], $email["subject"], $email["html"]);
	$ret_code = $html_email->send();
	$success = $ret_code === MAIL_OK ? "1" : "0";
	
	$subject_quoted = $db->escape($html_email->get_subject());
	$db->sql("
		INSERT INTO email_log
		SELECT NULL, to_email, to_user_id, '{$subject_quoted}', add_time, html, NOW(), {$success}
		FROM email
		WHERE id = {$email["id"]}
	");
	$db->sql("
		DELETE FROM email
		WHERE id = {$email["id"]}
	");
	$k++;
	$k_ok += $success ? 1 : 0;
}

$time = microtime(true) - $global_start_time;
$date = date("r");
$log_run = "{$date}\t{$time}\n";
file_put_contents(PATH_LOG . "/mail_run.log", $log_run, FILE_APPEND);
if ($k)
{
	$log_send = "{$date}\tProcessed:\t{$k}\tSent:\t{$k_ok}\tTime:\t{$time}\n";
	file_put_contents(PATH_LOG . "/mail_send.log", $log_send, FILE_APPEND);
}
?>