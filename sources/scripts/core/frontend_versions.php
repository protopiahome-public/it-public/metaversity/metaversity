<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

$versions_file = PATH_PRIVATE_DATA . "/versions/frontend_versions.json";

$is_versions_file_changed = false;

if (file_exists($versions_file))
{
	$versions = json_decode(file_get_contents($versions_file), true);
}
else
{
	$versions = array();
	$is_versions_file_changed = true;
}

$xslt_files = dir_list_plain(PATH_XSLT, true);
foreach ($xslt_files as $xslt_file)
{
	$is_file_changed = false;
	if (substr($xslt_file, -5) == ".xslt")
	{
		$xslt_file_content = file_get_contents($xslt_file);
		$xslt_file_content = preg_replace("#\.(js|css|jpg|png|gif|ico)\"#", ".\\1?v1\"", $xslt_file_content, -1, $replace_count);
		$is_file_changed = $replace_count > 0;
		preg_match_all("#\"\\{\\\$main_prefix\\}(.*?)\?v([0-9]+)\"#", $xslt_file_content, $matches, PREG_SET_ORDER);
		foreach ($matches as $match)
		{
			if (!file_exists(PATH_WWW . $match[1]))
			{
				continue;
			}
			$file_data = file_get_contents(PATH_WWW . $match[1]);
			$file_hash = md5($file_data);
			if (isset($versions[$match[1]]))
			{
				if ($file_hash != $versions[$match[1]]["hash"])
				{
					$old_version = $versions[$match[1]]["version"];
					$versions[$match[1]]["hash"] = $file_hash;
					$versions[$match[1]]["version"] ++;
					$is_versions_file_changed = true;
					echo "{$match[1]} version incremented from {$old_version} to {$versions[$match[1]]["version"]}\n";
				}
				if ($versions[$match[1]]["version"] != $match[2])
				{
					$xslt_file_content = str_replace($match[0], "\"{\$main_prefix}{$match[1]}?v{$versions[$match[1]]["version"]}\"", $xslt_file_content);
					$is_file_changed = true;
					echo "{$match[1]} version changed from {$match[2]} to {$versions[$match[1]]["version"]} in {$xslt_file}\n";
				}
			}
			else
			{
				$versions[$match[1]] = array(
					"hash" => $file_hash,
					"version" => (int) $match[2],
				);
				$is_versions_file_changed = true;
				echo "{$match[1]} added with version {$versions[$match[1]]["version"]} from {$xslt_file}\n";
			}
		}
		if ($is_file_changed)
		{
			file_put_contents($xslt_file, $xslt_file_content);
		}
	}
}

if ($is_versions_file_changed)
{
	ksort($versions);
	file_put_contents($versions_file, json_encode($versions, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
}
?>