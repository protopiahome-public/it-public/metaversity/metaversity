<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

$found_keys = array();

function add_key($lang_code, $text, $context, $file_name, $translation = null)
{
	global $found_keys;
	static $cache = array();
	if (!isset($cache[$lang_code]))
	{
		$lang = new lang($lang_code);
		$lang->reload_translations($use_cache = false, $collect_all_keys = true);
		$cache[$lang_code] = array(
			"lang" => $lang,
			"lang_file_path" => $lang->get_lang_file_path(),
			"something_added" => false,
			"new" => array(),
		);
	}
	$lang = &$cache[$lang_code]["lang"];
	$lang_file_path = &$cache[$lang_code]["lang_file_path"];
	$something_added = &$cache[$lang_code]["something_added"];
	$new = &$cache[$lang_code]["new"];
	/* @var $lang lang */
	if (trim($text) !== $text)
	{
		echo "[ERROR] Untrimmed text '{$text}' - {$file_name}\n";
		return;
	}
	if (trim($context) !== $context)
	{
		echo "[ERROR] Untrimmed context '{$context}' - {$file_name}\n";
		return;
	}
	$key = $lang->get_key($text, $context);

	$found_keys[$key] = true;

	$key_human = $text . (($context or false !== strpos($text, "//")) ? " // {$context}" : "");
	if (!$lang->key_exists($text, $context) and ! $lang->key_exists("#UNUSED# " . $text, $context) and ! isset($new[$key]))
	{
		$new[$key] = true;
		if (!$something_added)
		{
			$something_added = true;
			file_put_contents_safe($lang_file_path, str_replace("\r\n", "\n", trim(file_get_contents_safe($lang_file_path))));
		}

		$lang_text = file_get_contents_safe($lang_file_path);
		if ($translation === null)
		{
			file_put_contents_safe($lang_file_path, "\n\n" . $key_human . "\n", true);
			echo "Added '{$key_human}' - {$file_name} ({$lang_code})\n";
		}
		else
		{
			file_put_contents_safe($lang_file_path, "\n\n" . $key_human . "\n" . $translation . "\n", true);
			echo "Added '{$key_human}' ('{$translation}') - {$file_name} ({$lang_code})\n";
			return true;
		}
	}
	elseif ($translation !== null and ! isset($found_keys[$key]))
	{
		$old_translate = $lang->trans($text, $context);
		echo "[ERROR] '{$key_human}' ('{$translation}') already translated as '{$old_translate}' - {$file_name} ({$lang_code})\n";
	}

	return false;
}

$new = array(); // What is this?
$xslt_files = dir_list_plain(PATH_XSLT, true);
foreach ($xslt_files as $xslt_file)
{
	if (substr($xslt_file, -5) == ".xslt")
	{
		$s_pos = strrpos($xslt_file, "/");
		$xslt_file_name = $s_pos === false ? $xslt_file : substr($xslt_file, $s_pos + 1);
		//preg_match_all("#<xsl:value-of\s+select=\"php:function\('([^']+)',\s+'([^']+)'(,\s+'([^']+)')?\)\"/>#", $xslt_file_contents, $matches, PREG_SET_ORDER);
		//preg_match_all("#\"\{php:function\('([^']+)',\s+'([^']+)'(,\s+'([^']+)')?\)\}\"#", $xslt_file_contents, $matches2, PREG_SET_ORDER);
		//$matches = array_merge($matches, $matches2);
		//preg_match_all("#select=\"php:function\('([^']+)',\s+'([^']+)'(,\s+'([^']+)')?\)\"#", $xslt_file_contents, $matches3, PREG_SET_ORDER);
		//$matches = array_merge($matches, $matches3);
		$xslt_file_contents = file_get_contents($xslt_file);
		preg_match_all("#php:function\('trans_pre',\s+'([^']+)',\s+'([^']+)'(,\s+'([^']+)')?\)#", $xslt_file_contents, $matches, PREG_SET_ORDER);
		if (!empty($matches))
		{
			foreach ($matches as $match)
			{
				$translation = unhtmlspecialchars($match[1]);
				$text = unhtmlspecialchars($match[2]);
				$context = isset($match[4]) ? unhtmlspecialchars($match[4]) : "";
				$replace_need = add_key($lang->get_current_lang_code(), $text, $context, $xslt_file_name, $translation);
				if ($replace_need)
				{
					$match[2] = htmlspecialchars($match[2]);
					if (isset($match[4]))
					{
						$match[4] = htmlspecialchars($match[4]);
						$xslt_file_contents = str_replace($match[0], "php:function('trans', '{$match[2]}', '{$match[4]}')", $xslt_file_contents);
					}
					else
					{
						$xslt_file_contents = str_replace($match[0], "php:function('trans', '{$match[2]}')", $xslt_file_contents);
					}
					file_put_contents_safe($xslt_file, $xslt_file_contents);
				}
			}
		}
		$xslt_file_contents = file_get_contents($xslt_file);
		preg_match_all("#php:function\('([^']+)',\s+'([^']+)'(,\s+'([^']+)')?\)#", $xslt_file_contents, $matches, PREG_SET_ORDER);
		if (!empty($matches))
		{
			foreach ($matches as $match)
			{
				if ($match[1] !== 'trans')
				{
					continue;
				}
				$text = unhtmlspecialchars($match[2]);
				$context = isset($match[4]) ? unhtmlspecialchars($match[4]) : "";
				add_key($lang->get_current_lang_code(), $text, $context, $xslt_file_name);
			}
		}
		preg_match_all("#<xsl:call-template\s+(?:[^>]+\s)?name=\"count_case_lang\"[^>]*>(.*?)</xsl:call-template>#s", $xslt_file_contents, $matches, PREG_SET_ORDER);
		if (!empty($matches))
		{
			foreach ($matches as $match)
			{
				if (!preg_match("#<xsl:with-param\s+(?:[^>]+\s)?name=\"word\"[^>]+select=\"'([^>]+)'\"[^>]*/>#", $match[1], $match2))
				{
					continue;
				}
				$text = unhtmlspecialchars($match2[1]);
				add_key("ru", $text, "COUNT_CASE_1", $xslt_file_name);
				add_key("ru", $text, "COUNT_CASE_2", $xslt_file_name);
				add_key("ru", $text, "COUNT_CASE_5", $xslt_file_name);
				add_key("en", $text, "COUNT_CASE_1", $xslt_file_name);
				add_key("en", $text, "COUNT_CASE_N", $xslt_file_name);
			}
		}
		preg_match_all("#<xsl:call-template\s+(?:[^>]+\s)?name=\"trans\"[^>]*>(.*?)</xsl:call-template>#s", $xslt_file_contents, $matches, PREG_SET_ORDER);
		if (!empty($matches))
		{
			foreach ($matches as $match)
			{
				if (!preg_match("#<xsl:with-param\s+(?:[^>]+\s)?name=\"html\">(.*?)</xsl:with-param>#s", $match[1], $match2))
				{
					continue;
				}
				$text = $match2[1];
				$text = preg_replace("#(</[^<>]+>)\s+(<)#s", "\\1\\2", $text);
				$text = preg_replace("#<xsl:text>([^<]*)</xsl:text>#s", "\\1", $text);
				$text = trim(preg_replace("/[\\x00-\\x20]+/", " ", $text));
				if (preg_match("#</?xsl:#", $text))
				{
					echo "[ERROR] using <xsl:...> tag inside \$html - {$xslt_file_name}\n";
					continue;
				}
				if (preg_match("#<[^<>{]+(\{\{)*\{(?!\{)#", $text))
				{
					echo "[ERROR] using dynamic XSLT (braces {}) inside \$html - {$xslt_file_name}\n";
					continue;
				}
				add_key($lang->get_current_lang_code(), $text, "", $xslt_file_name);
			}
		}
	}
}

$php_files = dir_list_plain(PATH_SOURCES . "/modules", true);
foreach ($php_files as $php_file)
{
	if (substr($php_file, -4) == ".php")
	{
		$s_pos = strrpos($php_file, "/");
		$php_file_name = $s_pos === false ? $php_file : substr($php_file, $s_pos + 1);
		$php_file_contents = file_get_contents($php_file);
		preg_match_all("#[^a-zA-Z0-9\_\$\:\>]trans\s*\(\s*\"([^\"]+)\"(,\s*\"([^\"]+)\")?\s*\)#", $php_file_contents, $matches, PREG_SET_ORDER);
		if (!empty($matches))
		{
			foreach ($matches as $match)
			{
				$text = $match[1];
				$context = isset($match[3]) ? $match[3] : "";
				add_key($lang->get_current_lang_code(), $text, $context, $php_file_name);
			}
		}
		preg_match_all("#[^a-zA-Z0-9\_\$\:\>]trans_html\s*\(\s*\"(.+?[^\\\\])\"#", $php_file_contents, $matches, PREG_SET_ORDER);
		if (!empty($matches))
		{
			foreach ($matches as $match)
			{
				$text = str_replace('\"', '"', $match[1]);
				add_key($lang->get_current_lang_code(), $text, "", $php_file_name);
			}
		}
	}
}

// Unused keys
$lang = new lang($lang->get_current_lang_code());
$lang->reload_translations(false, true);
$all_keys = $lang->get_all_keys();
foreach ($all_keys as $key => $value)
{
	if (strstr($key, "===") !== false || strstr($key, "COUNT_CASE_") !== false)
	{
		continue;
	}

	$key_params = explode(" // ", $key);
	if (!strlen($key_params[1]))
	{
		$key_human = $key_params[0];
	}
	else
	{
		$key_human = $key;
	}

	if (preg_match("/^#UNUSED# (.*)$/", $key, $matches))
	{
		$new_key = $matches[1];
		preg_match("/^#UNUSED# (.*)$/", $key_human, $human_matches);
		$new_key_human = $human_matches[1];

		if (isset($found_keys[$new_key]))
		{
			$lang_text = file_get_contents_safe($lang->get_lang_file_path());
			$lang_text = str_replace($key_human, $new_key_human, $lang_text);
			file_put_contents($lang->get_lang_file_path(), $lang_text);
			echo "'{$key}' (ru) unmarked as #UNUSED# \n";
		}
	}
	elseif (!isset($found_keys[$key]))
	{
		$lang_text = file_get_contents_safe($lang->get_lang_file_path());
		$lang_text = explode("\n", $lang_text);
		foreach ($lang_text as $lang_line_number => &$lang_line)
		{

			if ($lang_line == $key_human && ($lang_line_number == 0 || $lang_text[$lang_line_number - 1] == ""))
			{
				if (strlen($key_params[1]))
				{
					$lang_line = "#UNUSED# " . $key_params[0] . " // " . $key_params[1];
				}
				else
				{
					$lang_line = "#UNUSED# " . $lang_line;
				}
				file_put_contents($lang->get_lang_file_path(), implode("\n", $lang_text));
				echo "'{$key}' (ru) marked as #UNUSED# \n";
				break;
			}
		}
	}
}
?>