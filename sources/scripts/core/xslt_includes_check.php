<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

$xslt_files = dir_list_plain(PATH_XSLT, true);
$not_found = array();
foreach ($xslt_files as $xslt_file)
{
	if (substr($xslt_file, -5) == ".xslt")
	{
		$xslt_file_content = file_get_contents($xslt_file);
		preg_match_all("#<xsl:(include|import)[^>]+href=\"([^\"]*)\"#", $xslt_file_content, $matches);
		if (count($matches[2]))
		{
//			echo "{$xslt_file}:\n";
			foreach ($matches[2] as $file_link)
			{
//				echo "  {$file_link}: ";
				if (file_exists(dirname($xslt_file) . "/" . $file_link))
				{
//					echo "existed\n";
				}
				else
				{
//					echo "NOT existed!\n";
					$not_found[] = array("file" => $xslt_file, "link" => $file_link);
				}
			}
		}
	}
}

if (count($not_found))
{
	echo "NOT FOUND:\n";
	foreach ($not_found as $not_found_item)
	{
		echo "{$not_found_item["file"]}:\n";
		echo "  {$not_found_item["link"]}:\n";
	}
}
else
{
	echo "ALL FOUND!\n";
}
?>