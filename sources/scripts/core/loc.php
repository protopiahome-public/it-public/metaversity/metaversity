<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

print_dir("PHP", PATH_HOME, "php");
print_dir("XSLT", PATH_XSLT, "xslt");
print_dir("JS", PATH_WWW, "js");
print_dir("CSS", PATH_WWW, "css");

function print_dir($name, $dir, $ext)
{
	list($file_count, $bytes, $loc) = calc($dir, $ext);
	$bytes_human = format_file_size($bytes);
	echo "{$name}: {$file_count} FILES, {$bytes_human}, {$loc} LoC\n";
}

function calc($dir, $ext)
{
	$file_count = 0;
	$bytes = 0;
	$loc = 0;
	$files = dir_list_plain($dir, true);
	foreach ($files as $file_path)
	{
		if (!str_ends($file_path, "." . $ext))
		{
			continue;
		}
		++$file_count;
		$bytes += filesize($file_path);
		$f = fopen($file_path, "r");
		while (false !== ($s = fgets($f)))
		{
			if (trim($s))
			{
				++$loc;
			}
		}
		fclose($f);
	}
	return array($file_count, $bytes, $loc);
}

?>