<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

/* @var $db db */

$TOTAL = $db->get_value("SELECT COUNT(*) FROM user");
$db_res = $db->sql("
	SELECT id, first_name, last_name
	FROM user
");
$NOW = 0;
while ($row = $db->fetch_array($db_res))
{
	progress($NOW, $TOTAL, "", false);
	$first_name = mb_convert_case($row["first_name"], MB_CASE_TITLE);
	$last_name = mb_convert_case($row["last_name"], MB_CASE_TITLE);
	$first_name_en = text_processor::translate_to_en($first_name);
	$last_name_en = text_processor::translate_to_en($last_name);
	$first_name_escaped = $db->escape($first_name);
	$last_name_escaped = $db->escape($last_name);
	$first_name_en_escaped = $db->escape($first_name_en);
	$last_name_en_escaped = $db->escape($last_name_en);
	$db->sql("
		UPDATE user
		SET 
			first_name = '{$first_name_escaped}',
			last_name = '{$last_name_escaped}',
			first_name_en = '{$first_name_en_escaped}',
			last_name_en = '{$last_name_en_escaped}'
		WHERE id = {$row["id"]}
	");
}
progress_clear();

function progress_echo_tabbed($str, $end = "\n")
{
	progress_echo(" - " . $str, $end);
}

function progress(&$NOW, $TOTAL, $end = "", $debounce = true)
{
	++$NOW;
	if (!$debounce || ($NOW & 1023) === 1)
	{
		progress_clear();
		echo " > {$NOW} of {$TOTAL}" . (strlen($end) ? " " . $end : "");
	}
}

function progress_echo($str, $end = "\n")
{
	progress_clear();
	echo $str . $end;
}

function progress_clear()
{
	echo "\r                                        \r";
}

?>