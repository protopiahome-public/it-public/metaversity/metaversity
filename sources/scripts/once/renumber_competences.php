<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

/* @var $db db */
$db->sql("UPDATE competence_full SET old_id = id");
//$db->sql("UPDATE competence_group SET old_id = id");
$db->sql("UPDATE competence_translation_formulae SET old_eq_competence_list = eq_competence_list");

$competences = $db->fetch_all("SELECT id FROM competence_full ORDER BY id");
$new_competence_id = 0;
$competences_ids = array();
foreach ($competences as $competence)
{
	$new_competence_id++;
	$db->sql("UPDATE competence_full SET id = {$new_competence_id} WHERE id = {$competence["id"]}");
	$competences_ids[$competence["id"]] = $new_competence_id;
}
$new_competence_id++;
$db->sql("ALTER TABLE competence_full AUTO_INCREMENT = {$new_competence_id}");

//$competence_groups = $db->fetch_all("SELECT id FROM competence_group ORDER BY id");
//$new_competence_group_id = 0;
//foreach ($competence_groups as $competence_group)
//{
//	$new_competence_group_id++;
//	$db->sql("UPDATE competence_group SET id = {$new_competence_group_id} WHERE id = {$competence_group["id"]}");
//}
//$new_competence_group_id++;
//$db->sql("ALTER TABLE competence_group AUTO_INCREMENT = {$new_competence_group_id}");

$competence_translation_formulaes = $db->fetch_all("SELECT * FROM competence_translation_formulae");
foreach ($competence_translation_formulaes as $competence_translation_formulae)
{
	$competence_translation_formulae["eq_competence_list"] = preg_replace_callback("#[0-9]+#", function($matches) {
		global $competences_ids;
		return $competences_ids[$matches[0]];
	}, $competence_translation_formulae["eq_competence_list"]);
	
	$db->sql("
		UPDATE competence_translation_formulae
		SET eq_competence_list = '{$competence_translation_formulae["eq_competence_list"]}'
		WHERE competence_id = {$competence_translation_formulae["competence_id"]}
			AND eq_competence_set_id = {$competence_translation_formulae["eq_competence_set_id"]}
	");
}