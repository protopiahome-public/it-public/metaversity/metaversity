<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

$db->begin();

$fields_to_process = array(
	array(
		"table" => "comment",
		"id_field" => "id",
		"fields" => array("html"),
	),
);

foreach ($fields_to_process as $data)
{
	$db_res = $db->sql("
		SELECT {$data["id_field"]} as id, " . join(", ", $data["fields"]) . "
		FROM {$data["table"]}
	");
	while ($row = $db->fetch_array($db_res))
	{
		$sets = array();
		foreach ($data["fields"] as $field)
		{
			$html = $row[$field];
			$html = text_processor::tidy($html);
			$html_quoted = $db->escape($html);
			$sets[] = "`{$field}` = '{$html_quoted}'";
		}
		$db->sql("
			UPDATE {$data["table"]}
			SET " . join(", ", $sets) . "
			WHERE {$data["id_field"]} = {$row["id"]}
		");
	}
}

$db->commit();
?>