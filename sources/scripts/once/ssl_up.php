<?php

require_once dirname(__FILE__) . "/../../../www/paths.php";
require_once PATH_SOURCES . "/start_shell.php";

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

/* @var $db db */

$db->begin();

$tables = array(
	"activity" => array("descr", "moderators"),
	"news_item" => array("announce", "descr"),
	"system_news_item" => array("announce", "descr"),
	"position" => array("descr"),
	"role" => array("descr"),
	"study_material" => array("descr"),
	"user" => array("about"),
);

foreach ($tables as $table => $fields)
{
	echo "{$table}:\n";
	$sql_fields = array("id");
	foreach ($fields as $field)
	{
		$sql_fields[] = "{$field}_text";
		$sql_fields[] = "{$field}_html";
	}
	$sql = "SELECT " . join(", ", $sql_fields) . " FROM {$table}";
	$NOW = 0;
	$all = $db->fetch_all($sql);
	foreach ($all as $row)
	{
		++$NOW;
		echo "\r{$NOW} of " . sizeof($all);
		$update_array = array();
		foreach ($fields as $field)
		{
			$text = $row[$field . "_text"];
			$html_old = $row[$field . "_html"];
			$html = text_processor::intmarkup_process($text);
			if (!strlen($html) && strlen($html_old))
			{
				$html = " ";
			}
			$update_array[$field . "_html"] = "'" . $db->escape($html) . "'";
		}
		$db->update_by_array($table, $update_array, "id = {$row["id"]}");
	}
	echo "\r- completed: {$NOW} records\n";
}

$db->commit();
?>