<?php

final class users_taxonomy extends base_taxonomy
{

	public function run()
	{
		global $config;

		$p = $this->get_parts_relative();

		if (($page = $this->is_page_folder($p[1])) and $p[2] === null)
		{
			//users/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new users_xml_page($page));
		}
		elseif ($p[1] !== false and $user_id = $this->get_user_id_by_login($p[1]))
		{
			//users/<login>/...
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $user_id);
			if ($p[2] === null)
			{
				//users/<login>/
				$this->xml_loader->add_xml_with_xslt(new user_profile_xml_page($user_id));
				$this->xml_loader->add_xml(new user_streams_xml_page($user_id, 1));
			}
			elseif ($p[2] === "streams" and ( $page = $this->is_page_folder($p[3])) and $p[4] === null)
			{
				//users/<login>/streams/
				$this->xml_loader->add_xml_with_xslt(new user_streams_xml_page($user_id, $page));
			}
			elseif ($p[2] === "results")
			{
				//users/<login>/results/...
				if ($p[3] === null)
				{
					//users/<login>/results/
					$this->xml_loader->add_xml_with_xslt(new user_position_results_streams_xml_page($user_id, $page));
					$this->xml_loader->add_xml(new math_user_streams_xml_ctrl($user_id, 0));
				}
				elseif ($p[3] === "all" and $page = $this->is_page_folder($p[4]) and $p[5] === null)
				{
					//users/<login>/results/all/[page-<page>/]
					$this->xml_loader->add_xml(new user_position_results_streams_xml_page($user_id, $page));
					$this->xml_loader->add_xslt("user_position_results_streams_all", "users");
					$this->xml_loader->add_xml(new math_user_streams_xml_ctrl($user_id, $page));
				}
				elseif (($stream_id = $this->get_stream_id_by_name($p[3])))
				{
					$stream_obj = stream_obj::instance($stream_id);
					$this->xml_loader->add_xml_by_class_name("stream_short_xml_ctrl", $stream_id);
					$this->xml_loader->add_xml(new stream_access_xml_ctrl($stream_obj->get_access()));
					$user_obj = user_obj::instance($user_id);

					if (!user_access_helper::can_access_results($user_obj, $stream_obj))
					{
						$this->xml_loader->add_xml(new user_current_xml_ctrl($user_id));
						$this->xml_loader->set_error_403();
						$this->xml_loader->set_error_403_xslt("user_position_results_403", "users");
					}
					elseif ($p[4] === null)
					{
						//users/<login>/results/<stream_id>/
						$this->xml_loader->add_xml_with_xslt(new user_position_results_positions_xml_page());
						$this->xml_loader->add_xml(new math_user_position_results_xml_ctrl($user_id, $stream_obj));
					}
					elseif ($p[4] === "tsv")
					{
						//users/<login>/results/<stream_id>/tsv/
						$this->xml_loader->add_xml(new math_user_position_results_tsv_xml_page($user_id, $stream_obj));
					}
					elseif (is_good_id($p[4]) and $p[5] === null)
					{
						//users/<login>/results/<stream_id>/<position_id>/
						$this->xml_loader->add_xml_with_xslt(new user_position_details_xml_page());
						$this->xml_loader->add_xml(new math_position_details_xml_ctrl($stream_obj, $p[4], $user_id));
						$this->xml_loader->add_xml(new position_full_xml_ctrl($p[4], $stream_obj->get_id()));
					}
				}
			}
			elseif ($p[2] === "competences")
			{
				//users/<login>/competences/...
				if ($page = $this->is_page_folder($p[3]) and $p[4] === null)
				{
					//users/<login>/competences/
					$this->xml_loader->add_xml_with_xslt(new user_competence_results_streams_xml_page($user_id, $page));
					$this->xml_loader->add_xml(new math_user_streams_xml_ctrl($user_id, $page));
				}
				elseif (($stream_id = $this->get_stream_id_by_name($p[3])))
				{
					$stream_obj = stream_obj::instance($stream_id);
					$this->xml_loader->add_xml_by_class_name("stream_short_xml_ctrl", $stream_id);
					$user_obj = user_obj::instance($user_id);

					if (!user_access_helper::can_access_results($user_obj, $stream_obj))
					{
						$this->xml_loader->add_xml(new user_current_xml_ctrl($user_id));
						$this->xml_loader->set_error_403();
						$this->xml_loader->set_error_403_xslt("user_competence_results_403", "users");
					}
					elseif ($p[4] === null)
					{
						//users/<login>/competences/<stream_id>/
						$this->xml_loader->add_xml_with_xslt(new user_competence_results_tree_xml_page());
						$this->xml_loader->add_xml(new math_user_competence_results_xml_ctrl($stream_id, $user_id, $stream_obj->get_competence_set_id()));
						$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($stream_obj->get_competence_set_id(), false, true, $stream_id));
					}
					elseif (is_good_id($p[4]) and $page = $this->is_page_folder($p[5]) and $p[6] === null)
					{
						//users/<login>/competences/<stream_id>/<competence_id>/
						$this->xml_loader->add_xml_with_xslt(new user_competence_details_xml_page());
						$this->xml_loader->add_xml(new math_user_competence_details_xml_ctrl($user_id, $stream_obj->get_competence_set_id(), $p[4], $page));
					}
				}
			}
			elseif ($p[2] === "log" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//users/<login>/log/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new user_log_xml_page($user_id, $page));
			}
			elseif ($this->user->is_admin() and $p[2] === "news" and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				//users/<login>/news/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new user_news_xml_page($user_id, $page, true));
			}
		}
	}

	private function get_user_id_by_login($login)
	{
		return url_helper::get_user_id_by_login($login);
	}

	private function get_stream_id_by_name($name)
	{
		return url_helper::get_stream_id_by_name($name);
	}

}

?>