<?php

require_once PATH_MODULES . "/stream_math/lib/all_competence_results_loader.php";
require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class math_user_position_results_tsv_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "credit_expert_user_id",
			"ctrl" => "user_short",
		),
	);
	protected $xml_attrs = array("user_id", "stream_id", "competence_set_id");
	protected $xml_row_name = "position";
	// Internal
	/**
	 * @var sort_easy_processor
	 */
	protected $sort_processor;
	protected $user_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $stream_id;
	protected $competence_set_id;
	protected $math_position_results;
	protected $position_ids = null;

	public function __construct($user_id, stream_obj $stream_obj)
	{
		$this->user_id = $user_id;
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("match", "p.id", "p.id");
		$processor->add_order("title", "p.title", "p.title DESC");
		$this->add_easy_processor($processor);

		$this->sort_processor = $processor;
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->load_results();

		$select_sql->add_from("position p");
		$select_sql->add_select_fields("p.id, p.title");
		$select_sql->add_select_fields("up.user_id IS NOT NULL AS is_focus");
		$select_sql->add_select_fields("pc.expert_user_id AS credit_expert_user_id, pc.comment AS credit_comment");
		$select_sql->add_join("LEFT JOIN user_position up ON up.position_id = p.id AND up.user_id = {$this->user_id}");
		$select_sql->add_join("LEFT JOIN position_credit pc ON pc.position_id = p.id AND pc.user_id = {$this->user_id}");
		$select_sql->add_where("p.stream_id = {$this->stream_id}");
		$select_sql->add_where("p.competence_count_calc > 0 OR up.user_id IS NOT NULL");
		if (!is_null($this->position_ids))
		{
			$select_sql->add_where("p.id IN (" . join(", ", $this->position_ids) . ") OR up.user_id IS NOT NULL");
		}
		$this->data = $this->db->fetch_all($select_sql->get_sql(), "id");

		$this->update_data();
	}

	private function load_results()
	{
		$loader = new all_competence_results_loader($this->competence_set_id, $this->user_id, true);
		$math_competence_results = $loader->get_competence_results();

		$math_positions = math_positions_helper::fetch_stream_positions_data($this->stream_id, $this->competence_set_id);

		$this->math_position_results = math_position_results_helper::get_position_results($math_positions, $math_competence_results, $this->competence_set_id);
		if (isset($this->math_position_results[$this->user_id]))
		{
			arsort($this->math_position_results[$this->user_id]);
		}
	}

	private function update_data()
	{
		foreach ($this->data as &$row)
		{
			if (isset($this->math_position_results[$this->user_id][$row["id"]]))
			{
				$row["match"] = $this->math_position_results[$this->user_id][$row["id"]];
			}
			else
			{
				$row["match"] = 0;
			}
		}
		unset($row);

		if ($this->sort_processor->get_order() === "match")
		{
			$this->data = array_sort($this->data, "match", !$this->sort_processor->get_back() ? SORT_DESC : SORT_ASC);
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		$text = "";
		$text .= trans("Student") . "\t" . $this->user->get_user_id() . "\t" . $this->user->get_login() . "\t" . $this->user->get_visible_name() . "\n";
		$text .= trans("Stream") . "\t" . $this->stream_obj->get_title() . " (" . $this->stream_obj->get_title_short() . ")" . "\n";
		$text .= trans("Positions") . "\n";
		$text .= "\n";
		$text .= "title";
		$text .= "\t" . "is_focus";
		$text .= "\t" . "match";
		$text .= "\t" . "credit_by_match";
		$text .= "\t" . "credit_expert_user_id";
		$text .= "\t" . "credit_expert_user_login";
		$text .= "\t" . "credit_expert_user_visible_name";
		$text .= "\t" . "credit_comment";
		$text .= "\n";
		foreach ($this->data as $row)
		{
			$credit_comment = $row["credit_comment"];
			$credit_comment = preg_replace("/[\\r\\n]+/", " // ", $credit_comment);
			$credit_comment = preg_replace("/[\\x00-\\x20]+/", " ", $credit_comment);
			$text .= $row["title"];
			$text .= "\t" . ($row["is_focus"] ? "1" : "0");
			$text .= "\t" . $row["match"];
			$min_match = $this->stream_obj->get_position_credit_min_match();
			$text .= "\t" . ($min_match > 0 && $row["match"] >= $min_match ? "1" : "0");
			if ($row["credit_expert_user_id"])
			{
				$text .= "\t" . $row["credit_expert_user_id"];
				$expert_user = new user($row["credit_expert_user_id"]);
				$text .= "\t" . $expert_user->get_login();
				$text .= "\t" . $expert_user->get_visible_name();
				$text .= "\t" . $credit_comment;
			}
			$text .= "\n";
		}
		response::set_content_text($text);
		//dd($this->data);
	}

}

?>