<?php

require_once PATH_INTCMF . "/array_manipulator.php";

class user_log_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "news_item";
	protected $xml_attrs = array("user_id", "mark_count");
	protected $dependencies_settings = array();
	// Internal
	protected $user_id;
	protected $page;

	/**
	 * @var multi_link_sql_filter
	 */
	protected $stream_sql_filter;
	protected $mark_count = 0;

	public function __construct($user_id, $page)
	{
		$this->user_id = $user_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, 50));

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter($this->stream_sql_filter = new multi_link_sql_filter("stream", trans("Stream"), "u.id", "stream_user_link", "user_id", "stream_id", "stream", "CONCAT('(', title_short, ') ', title)"));
		$this->stream_sql_filter->set_inactive_title(trans("All streams"));
		$this->stream_sql_filter->set_table2_title_short_column("title_short");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = user_log_helper::get_news_feed($this->user_id);
		$this->restrict_stream_list();
		if ($this->stream_sql_filter->get_value())
		{
			$this->data = array_manipulator::filter($this->data, array(
					array(array_manipulator::FILTER_MODE_EQ, "stream_id", $this->stream_sql_filter->get_value()),
			));
		}
		$this->calc_mark_count();
	}

	protected function restrict_stream_list()
	{
		$stream_ids = array(0);
		if ($this->stream_sql_filter->get_value())
		{
			$stream_ids[$this->stream_sql_filter->get_value()] = true;
		}
		foreach ($this->data as $row)
		{
			$stream_ids[$row["stream_id"]] = true;
		}
		$this->stream_sql_filter->set_table2_where("id IN (" . join(", ", array_keys($stream_ids)) . ")");
	}

	protected function calc_mark_count()
	{
		foreach ($this->data as $row)
		{
			if (isset($row["mark"]))
			{
				++$this->mark_count;
			}
		}
	}

	protected function postprocess_data()
	{
		activity_marks_access_helper::filter_user_marks($this->data, $this->user_id);
	}

	protected function postprocess()
	{
		news_ctrl_loader_helper::load_xml_ctrls($this->data, $this->xml_loader);
	}

}

?>