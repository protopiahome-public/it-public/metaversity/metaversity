<?php

require_once PATH_INTCMF . "/array_manipulator.php";

class user_news_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "news_item";
	protected $xml_attrs = array("user_id", "via_profile");
	protected $dependencies_settings = array();
	// Internal
	protected $user_id;

	/**
	 * @var user
	 */
	protected $current_user;
	protected $page;
	protected $via_profile;

	/**
	 * @var select_sql_filter
	 */
	protected $news_type_sql_filter;

	public function __construct($user_id, $page, $via_profile = false)
	{
		$this->user_id = $user_id;
		$this->page = $page;
		$this->via_profile = $via_profile;
		parent::__construct();
	}

	public function init()
	{
		$this->current_user = $this->user_id == $this->user->get_user_id() ? $this->user : new user($this->user_id);

		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, 25));

		if ($this->user_id)
		{
			$processor = new sql_filters_easy_processor();
			$options = array(
				news_helper::TYPE_SYSTEM_TEXT => trans("Metaversity news"),
				news_helper::TYPE_STREAM_TEXT => trans("Stream's news"),
				news_helper::TYPE_ACTIVITY_STATUS => trans("Status changes"),
				news_helper::TYPE_MARK => trans("New marks"),
				news_helper::TYPE_CREDIT => trans("New credits"),
			);
			if ($this->current_user->is_moderator_anywhere())
			{
				$options[news_helper::TYPE_ACTIVITY_MODERATION] = trans("Moderation");
			}
			$processor->add_sql_filter($this->news_type_sql_filter = new select_sql_filter("news_type", trans("Type"), $options));
			$this->news_type_sql_filter->set_inactive_title(trans("Show all", "FEMALE"));
			$this->add_easy_processor($processor);
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$news_type = $this->user_id ? $this->news_type_sql_filter->get_value() : news_helper::TYPE_SYSTEM_TEXT;

		$this->data = array();

		// 1,2. Roles and marks
		if (!$news_type or $news_type === news_helper::TYPE_ACTIVITY_STATUS or $news_type === news_helper::TYPE_MARK)
		{
			$data2 = user_log_helper::get_news_feed($this->user_id, null, true, $news_type);
			$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
		}

		// 3. Credits
		if (!$news_type or $news_type === news_helper::TYPE_CREDIT)
		{
			$data2 = user_credits_helper::get_news_feed($this->user_id);
			$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
		}

		// 4. System news
		if (!$news_type or $news_type === news_helper::TYPE_SYSTEM_TEXT)
		{
			$data2 = system_news_helper::get_news_feed($this->current_user->is_moderator_anywhere());
			$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
		}

		if (!$news_type or $news_type === news_helper::TYPE_STREAM_TEXT or $news_type === news_helper::TYPE_ACTIVITY_MODERATION)
		{
			// Fetching list of user's streams
			$user_stream_statuses = user_stream_status_helper::get_user_streams($this->user_id);
			foreach ($user_stream_statuses as $stream_id => $status_data)
			{
				$stream_access = stream_obj::instance($stream_id)->get_access();
				$this->xml_loader->add_xml(new stream_access_xml_ctrl($stream_access));
			}

			// 5. stream news (text)
			if (!$news_type or $news_type === news_helper::TYPE_STREAM_TEXT)
			{
				foreach ($user_stream_statuses as $stream_id => $status_data)
				{
					$data2 = stream_news_helper::get_news_feed($stream_id);
					if ($this->current_user->get_city_id())
					{
						$data2 = array_manipulator::filter($data2, array(
								array(array_manipulator::FILTER_MODE_EQ_IF_NOT_EMPTY, "city_id", $this->current_user->get_city_id()),
						));
					}
					$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
				}
			}

			// 6. Moderator's news
			if (!$news_type or $news_type === news_helper::TYPE_ACTIVITY_MODERATION)
			{
				foreach ($user_stream_statuses as $stream_id => $status_data)
				{
					if (stream_obj::instance($stream_id)->get_access()->is_moderator())
					{
						$data2 = stream_activity_participants_moderation_helper::get_news_feed($stream_id);
						$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
					}
				}
			}
		}
	}

	protected function postprocess()
	{
		news_ctrl_loader_helper::load_xml_ctrls($this->data, $this->xml_loader);
	}

}

?>