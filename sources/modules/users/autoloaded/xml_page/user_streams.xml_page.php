<?php

class user_streams_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_short"
		),
	);
	protected $xml_attrs = array("user_id");
	protected $xml_row_name = "stream";
	// Internal
	protected $user_id;
	protected $page;

	public function __construct($user_id, $page)
	{
		$this->user_id = $user_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("stream d");
		$select_sql->add_select_fields("d.id");
		$select_sql->add_select_fields("l.status, l.group_id, g.title AS group_title");
		$select_sql->add_select_fields("l.mark_count_calc");
		$select_sql->add_select_fields("l.add_time");
		$select_sql->add_join("LEFT JOIN stream_user_link l ON l.stream_id = d.id AND l.user_id = {$this->user_id}");
		$select_sql->add_join("LEFT JOIN `group` g ON g.id = l.group_id");
		$select_sql->add_where("l.status <> 'deleted' AND l.status <> 'pretender'");
		$select_sql->add_order("l.mark_count_calc DESC, l.add_time DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql(), "id");
	}

	protected function postprocess_data()
	{
		foreach ($this->data as &$row)
		{
			$row["positions"] = array();
		}
		unset($row);

		if (!sizeof($this->data))
		{
			return;
		}

		$positions = $this->db->fetch_all("
			SELECT p.stream_id, up.position_id AS id, p.title
			FROM user_position up
			JOIN position p ON p.id = up.position_id
			WHERE up.user_id = {$this->user_id}
				AND p.stream_id IN (" . join(", ", array_keys($this->data)) . ")
			ORDER BY p.title
		");
		foreach ($positions as $position)
		{
			$stream_id = $position["stream_id"];
			if (isset($this->data[$stream_id]))
			{
				$this->data[$stream_id]["positions"][] = array(
					"id" => $position["id"],
					"title" => $position["title"],
				);
			}
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		foreach ($this->data as $row)
		{
			$stream_node = $xdom->create_child_node("stream");
			foreach ($row as $idx => $val)
			{
				if (!is_array($val))
				{
					$stream_node->set_attr($idx, $val);
				}
			}
			if (sizeof($row["positions"]))
			{
				$this->db_xml_converter->build_xml($stream_node->create_child_node("positions"), $row["positions"], false, "position");
			}
		}
	}

}

?>