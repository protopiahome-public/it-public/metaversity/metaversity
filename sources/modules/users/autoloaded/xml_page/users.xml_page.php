<?php

class users_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short"
		),
	);
	protected $xml_row_name = "user";
	// Internal
	/**
	 * @var multi_link_sql_filter
	 */
	protected $stream_sql_filter;

	public function __construct($page)
	{
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", trans("Name/Login"), array("u.login", "u.first_name", "u.last_name", "u.first_name_en", "u.last_name_en"), trans("Search by name or login")));
		$processor->add_sql_filter(new foreign_key_sql_filter("city", trans("City"), "u.city_id", "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0"));
		$processor->add_sql_filter($this->stream_sql_filter = new multi_link_sql_filter("stream", trans("Stream"), "u.id", "stream_user_link", "user_id", "stream_id", "stream", "CONCAT('(', title_short, ') ', title)"));
		$this->stream_sql_filter->set_table2_title_short_column("title_short");
		$this->stream_sql_filter->set_additional_filter_where("filter_multi_link_stream.status <> 'deleted' AND filter_multi_link_stream.status <> 'pretender'");
		if (($stream_id = $this->stream_sql_filter->get_value()))
		{
			$this->stream_sql_filter->reset_on_change("group");
			$processor->add_sql_filter(new foreign_key_sql_filter("group", trans("Group"), "filter_multi_link_stream.group_id", "group", "title", "id", "stream_id = {$stream_id}"));
		}
		$this->add_easy_processor($processor);

		$processor = new sort_easy_processor();
		if ($this->stream_sql_filter->is_active())
		{
			$processor->add_order("mark-count", "filter_multi_link_stream.mark_count_calc DESC", "filter_multi_link_stream.mark_count_calc ASC");
		}
		else
		{
			$processor->add_order("mark-count", "u.mark_count_calc DESC", "u.mark_count_calc ASC");
		}
		if ($this->lang->get_current_lang_code() === "en")
		{
			$processor->add_order("name", "u.first_name_en ASC, u.last_name_en ASC", "u.first_name_en DESC, u.last_name_en DESC");
		}
		else
		{
			$processor->add_order("name", "u.first_name ASC, u.last_name ASC", "u.first_name DESC, u.last_name DESC");
		}
		$processor->add_order("reg-time", "u.add_time DESC", "u.add_time ASC");
		$processor->add_order("id", "id ASC", "id DESC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("user u");
		$select_sql->add_select_fields("u.id");
		if ($this->stream_sql_filter->is_active())
		{
			$select_sql->add_select_fields("filter_multi_link_stream.mark_count_calc");
		}
		else
		{
			$select_sql->add_select_fields("u.mark_count_calc");
		}
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

	protected function append_xml()
	{
		$xdom = xdom::create("stream_user_links");

		$user_id_array = $this->get_selected_ids();

		if (sizeof($user_id_array))
		{
			$db_result = $this->db->sql("
				SELECT l.user_id, l.stream_id, l.status, l.mark_count_calc
				FROM stream_user_link l
				WHERE 
					l.user_id IN (" . join(",", $user_id_array) . ")
					AND l.status <> 'pretender' AND l.status <> 'deleted'
				ORDER BY l.mark_count_calc DESC
			");
			$data = array();
			while ($row = $this->db->fetch_array($db_result))
			{
				array_key_set($data, $row["user_id"]);
				$data[$row["user_id"]][] = $row;
			}
			foreach ($data as $user_id => $user_data)
			{
				$stream_node = $xdom->create_child_node("user")->set_attr("id", $user_id);
				foreach ($user_data as $stream_data)
				{
					$stream_node->create_child_node("stream")
						->set_attr("id", $stream_data["stream_id"])
						->set_attr("status", $stream_data["status"])
						->set_attr("mark_count_calc", $stream_data["mark_count_calc"]);
					$this->xml_loader->add_xml_by_class_name("stream_short_xml_ctrl", $stream_data["stream_id"]);
				}
			}
		}
		return $xdom->get_xml(true);
	}

}

?>