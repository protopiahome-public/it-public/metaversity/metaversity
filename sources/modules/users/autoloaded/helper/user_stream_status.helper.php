<?php

class user_stream_status_helper extends base_static_db_helper
{

	public static function update_is_moderator_anywhere_calc_status($user_id)
	{
		self::db()->sql("
			UPDATE user u
			LEFT JOIN (
				SELECT l.user_id
				FROM stream_user_link l
				WHERE l.status = 'admin' or l.status = 'moderator'
					AND l.user_id = {$user_id}
			) x ON x.user_id = u.id
			SET u.is_moderator_anywhere_calc = u.is_admin OR x.user_id IS NOT NULL
			WHERE u.id = {$user_id}
		");
		user_cache_tag::init($user_id)->update();
	}

	public static function is_moderator_anywhere($user_id)
	{
		return self::db()->row_exists("SELECT id FROM user WHERE id = {$user_id} AND is_moderator_anywhere_calc = 1");
	}

	public static function get_user_group_id_cached($user_id, $stream_id)
	{
		$result = self::get_user_group_info_cached($user_id, $stream_id);
		return $result["group_id"] ? (int) $result["group_id"] : null;
	}

	public static function get_user_study_level_id_cached($user_id, $stream_id)
	{
		$result = self::get_user_group_info_cached($user_id, $stream_id);
		return $result["study_level_id"] ? (int) $result["study_level_id"] : null;
	}

	public static function get_user_group_info_cached($user_id, $stream_id)
	{
		static $cache = array();
		$key = $user_id . "-" . $stream_id;
		if (!isset($cache[$key]))
		{
			$cache[$key] = self::get_user_group_info($user_id, $stream_id);
		}
		return $cache[$key];
	}

	public static function get_user_group_info($user_id, $stream_id)
	{
		$select_sql = new select_sql("stream_user_link l");
		$select_sql->add_select_fields("l.group_id, g.study_level_id");
		$select_sql->add_join("JOIN `group` g ON g.id = l.group_id");
		$select_sql->add_where("l.user_id = {$user_id} AND l.stream_id = {$stream_id}");
		$select_sql->add_where("l.status <> 'deleted' AND l.status <> 'pretender'");
		$result = self::db()->get_row($select_sql->get_sql());
		if (!$result)
		{
			$result = array(
				"group_id" => null,
				"study_level_id" => null,
			);
		}
		return $result;
	}

	public static function get_user_streams($user_id)
	{
		$result = array();
		if ($user_id)
		{
			$select_sql = new select_sql("stream_user_link l");
			$select_sql->add_select_fields("l.stream_id AS id, l.status, g.study_level_id, l.group_id");
			$select_sql->add_join("LEFT JOIN `group` g ON g.id = l.group_id");
			$select_sql->add_where("l.user_id = {$user_id}");
			$select_sql->add_where("l.status <> 'deleted' AND l.status <> 'pretender'");
			$result = self::db()->fetch_all($select_sql->get_sql(), "id");
		}
		return $result;
	}

}

?>