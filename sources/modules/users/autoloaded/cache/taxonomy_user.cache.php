<?php

class taxonomy_user_cache extends base_cache
{

	public static function init($login, $user_id = null)
	{
		$tag_keys = array();
		$tag_keys[] = user_cache_tag::init($user_id)->get_key();
		return parent::get_cache(__CLASS__, $login, $tag_keys);
	}

}

?>