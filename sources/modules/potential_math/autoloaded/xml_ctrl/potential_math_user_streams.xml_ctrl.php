<?php

class potential_math_user_streams_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_short"
		),
	);
	protected $xml_row_name = "stream";
	// Internal
	protected $user_id;
	protected $try_stream_ids;
	protected $fallback_stream_id;
	protected $potential_stream_id;

	public function __construct($user_id, $try_stream_ids = array(), $fallback_stream_id = null)
	{
		$this->user_id = $user_id;
		$this->try_stream_ids = $try_stream_ids;
		$this->fallback_stream_id = $fallback_stream_id;
		parent::__construct();
		$this->calc();
	}

	private function calc()
	{
		$this->data = stream_user_positions_helper::get_user_streams_with_positions($this->user_id, $this->fallback_stream_id);
		foreach ($this->try_stream_ids as $try_stream_id)
		{
			if (is_good_id($try_stream_id) and isset($this->data[$try_stream_id]))
			{
				$this->potential_stream_id = (int) $try_stream_id;
				$this->data[$this->potential_stream_id]["is_selected"] = true;
				break;
			}
		}
		if (!$this->potential_stream_id and sizeof($this->data))
		{
			$row1 = reset($this->data);
			$this->potential_stream_id = $row1["id"];
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		// Already done in calc()
	}

	public function get_potential_stream_id()
	{
		return $this->potential_stream_id;
	}

}

?>