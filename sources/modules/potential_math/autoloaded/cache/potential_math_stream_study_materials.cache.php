<?php

class potential_math_stream_study_materials_cache extends base_cache
{

	public static function init($stream_id, $competence_set_id)
	{
		$tag_keys = array();
		$tag_keys[] = stream_study_materials_cache_tag::init($stream_id)->get_key();
		$tag_keys[] = competence_set_competences_cache_tag::init($competence_set_id)->get_key();
		return parent::get_cache(__CLASS__, $stream_id, $tag_keys);
	}

}

?>