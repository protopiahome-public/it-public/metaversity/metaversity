<?php

class potential_math_format_roles_cache extends base_cache
{

	public static function init($format_id, $competence_set_id)
	{
		$tag_keys = array();
		$tag_keys[] = format_cache_tag::init($format_id)->get_key();
		$tag_keys[] = competence_set_competences_cache_tag::init($competence_set_id)->get_key();
		return parent::get_cache(__CLASS__, $format_id, $tag_keys);
	}

}

?>