<?php

class potential_math_study_material_helper extends base_static_db_helper
{

	public static function get_stream_study_materials($stream_id, $competence_set_id)
	{
		$result = potential_math_stream_study_materials_cache::init($stream_id, $competence_set_id)->get();
		if ($result)
		{
			return $result;
		}
		$competences_data = self::db()->fetch_all("
			SELECT sm.id AS study_material_id, rcl.competence_id, rcl.mark
			FROM study_material sm
			JOIN rate_competence_link rcl ON rcl.rate_id = sm.rate_id
			JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE sm.stream_id = {$stream_id} AND c.competence_set_id = {$competence_set_id}
		");
		$result = self::db()->fetch_column_values("
			SELECT id
			FROM study_material
		", array(), "id");
		foreach ($competences_data as $competence_data)
		{
			list($study_material_id, $competence_id, $mark) = array_values($competence_data);
			$result[$study_material_id][$competence_id] = $mark;
		}
		potential_math_stream_study_materials_cache::init($stream_id, $competence_set_id)->set($result);
		return $result;
	}

	public static function get_stream_study_material_potential(stream_obj $stream_obj, $study_material_id, $required_results, $format_numbers = false, $need_details = false)
	{
		$competences = self::get_stream_study_materials($stream_obj->get_id(), $stream_obj->get_competence_set_id());
		if (!isset($competences[$study_material_id]))
		{
			return 0.0;
		}
		$potential = potential_math_common_helper::calc_potential($competences[$study_material_id], $required_results, $need_details);
		if ($format_numbers)
		{
			if (!$need_details)
			{
				$potential = potential_math_common_helper::potential_number_format($potential);
			}
			else
			{
				$potential[0] = potential_math_common_helper::potential_number_format($potential[0]);
			}
		}
		return $potential;
	}

	public static function fill_stream_study_materials_potential(stream_obj $stream_obj, &$study_materials, $required_results, $format_numbers = false)
	{
		$competences = self::get_stream_study_materials($stream_obj->get_id(), $stream_obj->get_competence_set_id());
		$potentials = potential_math_common_helper::calc_potential_multi($competences, $required_results);
		foreach ($study_materials as &$study_material_row)
		{
			$study_material_row["potential"] = isset($potentials[$study_material_row["id"]]) ? $potentials[$study_material_row["id"]] : 0.0;
			if ($format_numbers)
			{
				$study_material_row["potential"] = potential_math_common_helper::potential_number_format($study_material_row["potential"]);
			}
		}
		unset($study_material_row);
	}

}

?>