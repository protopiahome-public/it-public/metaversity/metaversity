<?php

class potential_math_competence_helper extends base_static_db_helper
{

	public static function get_competence_data($competence_id_raw)
	{
		if (!is_good_id($competence_id_raw))
		{
			return null;
		}
		$competence_id = $competence_id_raw;
		$data = self::db()->get_row("
			SELECT competence_id AS id, title, competence_set_id 
			FROM competence_calc 
			WHERE competence_id = {$competence_id}
		");
		if (!$data)
		{
			return null;
		}
		$data["stream_id"] = competence_set_stream_relation_helper::get_corresponding_stream_id($data["competence_set_id"], true);
		if (!$data["stream_id"])
		{
			return null;
		}
		return $data;
	}

}

?>