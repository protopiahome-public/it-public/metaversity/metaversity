<?php

class potential_math_common_helper
{

	/**
	 *              proposal
	 *    required  0   1   2
	 *           0  0   0   0
	 * useful    1  0  .12 .3
	 * required  2  0  .28 .7
	 * key       3  0  .4   1
	 */
	public static function calc_potential($proposal_competences, $required_results, $need_details = false)
	{
		$mult1_index = array(
			0 => 0.0,
			1 => 0.3,
			2 => 0.7,
			3 => 1.0,
		);
		// $proposal_competences are in [0..2] (float)
		// $required_competences are in [0..3] (int)
		$potential = 0.0;
		$details = array();
		foreach ($required_results as $competence_id => $required_value)
		{
			$mult1 = $mult1_index[$required_value];
			if ($mult1 > 0 and isset($proposal_competences[$competence_id]))
			{
				$proposal_value = $proposal_competences[$competence_id];
				if (is_array($proposal_value) and $need_details)
				{
					$competence_id = $proposal_value[1];
					$proposal_value = $proposal_value[0];
				}
				if ($proposal_value > 2.0)
				{
					trigger_error("Unexpected value of \$proposal_value: '{$proposal_value}'");
				}
				$mult2 = 0.1 * $proposal_value * $proposal_value + 0.3 * $proposal_value;
				$delta = $mult1 * $mult2;
				$potential += $delta * 2;
				if ($need_details)
				{
					array_key_set($details, $competence_id, 0);
					$details[$competence_id] += $delta * 2;
				}
			}
		}
		return !$need_details ? $potential : array($potential, $details);
	}

	public static function calc_potential_multi($proposal_multi_competences, $required_results, $need_details = false)
	{
		$result = array();
		foreach ($proposal_multi_competences as $key => $proposal_competences)
		{
			$result[$key] = self::calc_potential($proposal_competences, $required_results, $need_details);
		}
		arsort($result);
		return $result;
	}

	public static function translate_competences_multi($proposal_multi_competences, $competence_set_id, $required_results_competence_set_id, $need_details = false)
	{
		if ($competence_set_id == $required_results_competence_set_id)
		{
			return $proposal_multi_competences;
		}
		$translator_direct = math_direct_translators_helper::get_direct_translators_data_grouped_by_competence_set_cached($required_results_competence_set_id, $competence_set_id);
		$translator = math_direct_translators_helper::get_translator_via_central($required_results_competence_set_id, $competence_set_id);
		// Direct translator has higer priority
		array_merge_good($translator, $translator_direct);
		$result = array();
		foreach ($proposal_multi_competences as $key => $proposal_competences)
		{
			$result[$key] = array();
			foreach ($translator as $competence_id => $eq_competence_ids)
			{
				foreach ($eq_competence_ids as $eq_competence_id)
				{
					if (isset($proposal_competences[$eq_competence_id]))
					{
						array_key_set($result[$key], $competence_id, $need_details ? array(0, array()) : 0);
						$val = $need_details ? $result[$key][$competence_id][0] : $result[$key][$competence_id];
						if ($proposal_competences[$eq_competence_id] > $val)
						{
							$result[$key][$competence_id] = $need_details ? array($proposal_competences[$eq_competence_id], $eq_competence_id) : $proposal_competences[$eq_competence_id];
						}
					}
				}
			}
		}
		return $result;
	}

	public static function potential_number_format($potential)
	{
		//return number_format($potential, 1, ".", "");
		return (string) (int) round($potential);
	}

	public static function potential_impact_number_format($potential_impact)
	{
		return number_format($potential_impact, 1, ".", "");
	}

}

?>