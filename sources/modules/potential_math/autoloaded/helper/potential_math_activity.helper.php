<?php

class potential_math_activity_helper extends base_static_db_helper
{

	public static function get_format_roles($format_id, $competence_set_id)
	{
		$result = potential_math_format_roles_cache::init($format_id, $competence_set_id)->get();
		if ($result)
		{
			return $result;
		}
		$competences_data = self::db()->fetch_all("
			SELECT role.id AS role_id, rcl.competence_id, rcl.mark
			FROM role
			JOIN rate_competence_link rcl ON rcl.rate_id = role.rate_id
			JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE role.format_id = {$format_id} AND c.competence_set_id = {$competence_set_id}
		");
		$result = self::db()->fetch_column_values("
			SELECT id
			FROM role
			WHERE format_id = {$format_id}
		", array(), "id");
		foreach ($competences_data as $competence_data)
		{
			list($role_id, $competence_id, $mark) = array_values($competence_data);
			$result[$role_id][$competence_id] = $mark;
		}
		potential_math_format_roles_cache::init($format_id, $competence_set_id)->set($result);
		return $result;
	}

	public static function get_format_roles_potential($format_id, $competence_set_id, $required_results, $required_results_competence_set_id, $need_details = false)
	{
		$roles_competences = self::get_format_roles($format_id, $competence_set_id);
		$roles_competences = potential_math_common_helper::translate_competences_multi($roles_competences, $competence_set_id, $required_results_competence_set_id, $need_details);
		return potential_math_common_helper::calc_potential_multi($roles_competences, $required_results, $need_details);
	}

	static $format_roles_potential_cache = array();

	public static function clean_format_roles_potential_cache()
	{
		self::$format_roles_potential_cache = array();
	}

	public static function get_format_roles_potential_cached($format_id, $competence_set_id, $required_results, $required_results_competence_set_id, $need_details = false)
	{
		if (!$need_details && isset(self::$format_roles_potential_cache[$format_id]))
		{
			return self::$format_roles_potential_cache[$format_id];
		}
		$result = self::get_format_roles_potential($format_id, $competence_set_id, $required_results, $required_results_competence_set_id, $need_details);
		if (!$need_details)
		{
			self::$format_roles_potential_cache[$format_id] = $result;
		}
		return $result;
	}

	public static function get_activity_roles_potential(stream_obj $stream_obj, $activity_id_or_row, $required_results, $format_numbers = false, $need_details = false)
	{
		$activity_row = activity_helper::get_row($activity_id_or_row);
		$activity_stream_id = $activity_row["stream_id"];
		$activity_stream_obj = $activity_stream_id === $stream_obj->get_id() ? $stream_obj : stream_obj::instance($activity_stream_id);
		/* @var $activity_stream_obj stream_obj */
		$format_id = $activity_row["format_id"];
		// BE CAREFUL ABOUT THE CACHE!
		$result = self::get_format_roles_potential_cached($format_id, $activity_stream_obj->get_competence_set_id(), $required_results, $stream_obj->get_competence_set_id(), $need_details);
		$roles = activity_roles_helper::fetch_roles_for_calculus($stream_obj, $activity_row);
		foreach ($result as $role_id => $potential)
		{
			if (!isset($roles["roles"][$role_id]))
			{
				unset($result[$role_id]);
			}
		}
		if ($format_numbers)
		{
			foreach ($result as &$potential)
			{
				if (!$need_details)
				{
					$potential = potential_math_common_helper::potential_number_format($potential);
				}
				else
				{
					$potential[0] = potential_math_common_helper::potential_number_format($potential[0]);
				}
			}
			unset($potential);
		}
		return $result;
	}

	public static function fill_activities_potential(stream_obj $stream_obj, &$activities, $required_results, $format_numbers = false)
	{
		foreach ($activities as &$activity_row)
		{
			$roles = self::get_activity_roles_potential($stream_obj, $activity_row, $required_results);
			$max_potential = reset($roles);
			$activity_row["potential"] = $max_potential ? : 0.0;
			if ($format_numbers)
			{
				$activity_row["potential"] = potential_math_common_helper::potential_number_format($activity_row["potential"]);
			}
		}
		unset($activity_row);
	}

}

?>