<?php

class potential_math_common_helper_test extends base_test
{

	public function set_up()
	{
		competence_direct_translators_cache::init(1)->set(array(
			102 => array(
				array(2, 202),
			),
			103 => array(
				array(2, 203),
				array(3, 303),
				array(4, 403),
			),
		));
	}

	public function calc_potential_test()
	{
		$proposal_competences = array(
			1 => 2,
		);
		$required_results = array(
			1 => 3,
			11 => 3,
			111 => 3,
		);
		$potential = potential_math_common_helper::calc_potential($proposal_competences, $required_results);
		$this->assert_identical(number_format($potential, 3, ".", ""), "2.000");
	}

	public function calc_potential_2_test()
	{
		$proposal_competences = array(
			1 => 1,
		);
		$required_results = array(
			1 => 3,
			11 => 3,
			111 => 3,
		);
		$potential = potential_math_common_helper::calc_potential($proposal_competences, $required_results);
		$this->assert_identical(number_format($potential, 3, ".", ""), "0.800");
	}

	public function calc_potential_3_test()
	{
		$proposal_competences = array(
			1 => 2,
			11 => 2,
		);
		$required_results = array(
			1 => 3,
			11 => 3,
			111 => 3,
		);
		$potential = potential_math_common_helper::calc_potential($proposal_competences, $required_results);
		$this->assert_identical(number_format($potential, 3, ".", ""), "4.000");
	}

	public function calc_potential_4_test()
	{
		$proposal_competences = array(
			1 => 2,
			11 => 1,
		);
		$required_results = array(
			1 => 3,
			11 => 3,
			111 => 3,
		);
		$potential = potential_math_common_helper::calc_potential($proposal_competences, $required_results);
		$this->assert_identical(number_format($potential, 3, ".", ""), "2.800");
	}

	public function calc_potential_5_test()
	{
		$proposal_competences = array(
			1 => 2,
			11 => 1,
		);
		$required_results = array(
			1 => 3,
			11 => 3,
			111 => 3,
		);
		list($potential, $details) = potential_math_common_helper::calc_potential($proposal_competences, $required_results, true);
		$this->assert_identical(number_format($potential, 3, ".", ""), "2.800");
		$this->assert_identical(array(1 => 2.0, 11 => 0.8), $details);
	}

	public function calc_potential_22_test()
	{
		$proposal_competences = array(
			1 => 2,
			2 => 2,
			18 => 2,
			5 => 1,
		);
		$required_results = array(
			1 => 3, // 2 => 1
			2 => 1, // 2 => 0.3
			3 => 0, // 0 => 
			4 => 2, // 0 => 
			5 => 2, // 1 => 0.28
		);
		$potential = potential_math_common_helper::calc_potential($proposal_competences, $required_results);
		$this->assert_identical(number_format($potential, 3, ".", ""), "3.160");
	}

	public function calc_potential_multi_test()
	{
		$proposal_multi_competences = array(
			"x" => array(
				1 => 2,
				2 => 2,
				18 => 2,
				5 => 1,
			),
			"44" => array(),
			"2" => array(
				5 => 2,
			),
		);
		$required_results = array(
			1 => 3, // 2 => 1
			2 => 1, // 2 => 0.3
			3 => 0, // 0 => 
			4 => 2, // 0 => 
			5 => 2, // 1 => 0.28
		);
		$potential = potential_math_common_helper::calc_potential_multi($proposal_multi_competences, $required_results);
		$this->assert_identical($potential, array(
			"x" => 3.16,
			"2" => 1.4,
			"44" => 0.0,
		));
	}

	public function calc_potential_multi_2_test()
	{
		$proposal_multi_competences = array(
			"44" => array(
				1 => 2,
			),
			"45" => array(
				1 => 2,
				11 => 1,
			),
		);
		$required_results = array(
			1 => 3,
			11 => 3,
			111 => 3,
		);
		$potential = potential_math_common_helper::calc_potential_multi($proposal_multi_competences, $required_results, true);
		$this->assert_identical(array(
			"45" => array(
				2.8,
				array(1 => 2.0, 11 => 0.8),
			),
			"44" => array(
				2.0,
				array(1 => 2.0),
			),
			), $potential);
	}

	public function potential_number_format_test()
	{
		$this->assert_identical(potential_math_common_helper::potential_number_format(1.582), "2");
	}

}

?>