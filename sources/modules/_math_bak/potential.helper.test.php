<?php

class potential_helper_test extends base_test
{

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function set_up()
	{
		$this->db->sql("INSERT IGNORE INTO competence_set (id, title) VALUES (1, '')");
		$this->db->sql("INSERT IGNORE INTO competence_set (id, title) VALUES (2, '')");
		$this->db->sql("REPLACE INTO stream (id, name, title, title_short, competence_set_id) VALUES (1, 'test1', 'test1', 'test1', 1)");
		$this->db->sql("REPLACE INTO stream (id, name, title, title_short, competence_set_id) VALUES (2, 'test2', 'test2', 'test2', 2)");
		$this->db->sql("REPLACE INTO stream (id, name, title, title_short, competence_set_id) VALUES (3, 'test3', 'test3', 'test3', 1)");
		$this->db->sql("DELETE FROM competence_calc WHERE competence_id IN (1, 2, 3, 4, 5)");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (1, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (2, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (3, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (4, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (5, 2, '')");
		$this->db->sql("INSERT IGNORE INTO user (id, login) VALUES (1, '#test1')");
		$this->db->sql("INSERT IGNORE INTO user (id, login) VALUES (2, '#test2')");
		$this->db->sql("INSERT IGNORE INTO format_group (id, stream_id) VALUES (1, 1)");
		$this->db->sql("INSERT IGNORE INTO format_group (id, stream_id) VALUES (2, 2)");
		$this->db->sql("INSERT IGNORE INTO format (id, stream_id, format_group_id) VALUES (1, 1, 1)");
		$this->db->sql("INSERT IGNORE INTO format (id, stream_id, format_group_id) VALUES (2, 1, 1)");
		$this->db->sql("INSERT IGNORE INTO format (id, stream_id, format_group_id) VALUES (9, 2, 2)");

		// positions
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (101, 6)");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (102, 6)");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (109, 6)");
		$this->db->sql("INSERT INTO position (id, stream_id, title, rate_id) VALUES (1, 1, '', 101) ON DUPLICATE KEY UPDATE rate_id = VALUES(rate_id)");
		$this->db->sql("INSERT INTO position (id, stream_id, title, rate_id) VALUES (2, 1, '', 102) ON DUPLICATE KEY UPDATE rate_id = VALUES(rate_id)");
		$this->db->sql("INSERT INTO position (id, stream_id, title, rate_id) VALUES (9, 2, '', 109) ON DUPLICATE KEY UPDATE rate_id = VALUES(rate_id)");
		$this->db->sql("DELETE FROM rate_competence_link WHERE rate_id IN (101, 102, 109)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (101, 1, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (101, 2, 1)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (101, 5, 1)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (102, 2, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (102, 3, 1)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (109, 1, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (109, 2, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (109, 3, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (109, 4, 2)");

		// roles
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (201, 5)");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (202, 5)");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (203, 5)");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (209, 5)");
		$this->db->sql("DELETE FROM role WHERE id IN (1, 2, 3, 9)");
		$this->db->sql("DELETE FROM role WHERE format_id IN (1, 2, 9)");
		$this->db->sql("INSERT INTO role (id, stream_id, format_id, rate_id, title) VALUES (1, 1, 1, 201, 'a')");
		$this->db->sql("INSERT INTO role (id, stream_id, format_id, rate_id, title) VALUES (2, 1, 1, 202, 'b')");
		$this->db->sql("INSERT INTO role (id, stream_id, format_id, rate_id, title) VALUES (3, 1, 2, 203, 'c')");
		$this->db->sql("INSERT INTO role (id, stream_id, format_id, rate_id, title) VALUES (9, 2, 9, 209, 'd')");
		$this->db->sql("DELETE FROM rate_competence_link WHERE rate_id IN (201, 202, 203, 209)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (201, 1, 2)"); //role.id=1
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (201, 2, 1)"); //role.id=1
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (201, 5, 1)"); //role.id=1
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (202, 2, 1)"); //role.id=2
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (202, 3, 2)"); //role.id=2
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (203, 2, 2)"); //role.id=3
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (203, 3, 1)"); //role.id=3
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (203, 4, 2)"); //role.id=3
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (209, 1, 2)"); //role.id=9
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (209, 2, 2)"); //role.id=9
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (209, 3, 2)"); //role.id=9
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (209, 4, 2)"); //role.id=9
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (209, 5, 2)"); //role.id=9
		// activities
		$this->db->sql("DELETE FROM activity WHERE id IN (1, 2, 3, 9)");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, title) VALUES (1, 1, 1, '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, title) VALUES (2, 1, 2, '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, title) VALUES (3, 1, 1, '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, title) VALUES (9, 2, 9, '')");

		// user_positions
		$this->db->sql("DELETE FROM user_position WHERE user_id IN (1, 2)");
		$this->db->sql("INSERT INTO user_position (user_id, position_id) VALUES (1, 1)");
		$this->db->sql("INSERT INTO user_position (user_id, position_id) VALUES (1, 2)");
		$this->db->sql("INSERT INTO user_position (user_id, position_id) VALUES (1, 9)");
		$this->db->sql("INSERT INTO user_position (user_id, position_id) VALUES (2, 1)");

		// study materials
		$this->db->sql("DELETE FROM study_material");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (501, 7)");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (502, 7)");
		$this->db->sql("INSERT IGNORE INTO rate (id, rate_type_id) VALUES (503, 7)");
		$this->db->sql("INSERT INTO study_material (id, stream_id, title, rate_id) VALUES (1, 1, '', 501)");
		$this->db->sql("INSERT INTO study_material (id, stream_id, title, rate_id) VALUES (2, 1, '', 502)");
		$this->db->sql("INSERT INTO study_material (id, stream_id, title, rate_id) VALUES (3, 1, '', 503)");
		$this->db->sql("DELETE FROM rate_competence_link WHERE rate_id IN (501, 502, 503)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (501, 1, 2)"); //study_material.id=1
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (503, 2, 2)"); //study_material.id=3
		// user1: 1 => 2, 2 => 2, 3 => 1
		// user2: 1 => 2, 2 => 1
		// role1: 1 => 2, 2 => 1                 // format1 // activities 1, 3
		// role2:         2 => 1, 3 => 2         // format1 // activities 1, 3
		// role3:         2 => 2, 3 => 1, 4 => 2 // format2 // activities 2
		// sm1:   1 => 2
		// sm2:
		// sm3:           2 => 2

		$this->stream_obj = stream_obj::instance(1);
	}

	public function get_competence_list_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		$result = potential_helper::get_positions_competence_list($this->stream_obj, $user_id = 1);
		$this->assert_equal($result, array(1 => 2, 2 => 2, 3 => 1));

		$result = potential_helper::get_positions_competence_list($this->stream_obj, $user_id = 2);
		$this->assert_equal($result, array(1 => 2, 2 => 1));

		$this->db->test_end();
	}

	public function get_match_test()
	{
		$result = potential_helper::get_match(array(1 => 1, 2 => 1, 7 => 2), array(1 => 2, 3 => 1, 7 => 2));
		$this->assert_identical($result, 6);
	}

	public function get_match_multi_test()
	{
		$result = potential_helper::get_match_multi(array(
				4 => array(1 => 1, 2 => 1, 7 => 2),
				47 => array(1 => 1),
				99 => array(3 => 1, 1 => 2),
				), array(1 => 2, 3 => 1, 7 => 2));
		$this->assert_identical($result, array(4 => 6, 99 => 5, 47 => 2));
	}

	public function get_format_match_test()
	{
		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_format_match($this->stream_obj, $format_id = 1, $user_id = 1);
		$this->assert_identical($result, array(1 => 6, 2 => 4));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_format_match($this->stream_obj, $format_id = 1, $user_id = 1);
		$this->assert_identical($result, array(1 => 6, 2 => 4));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);
	}

	public function activity_fetch_roles_test()
	{
		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 1);
		$this->assert_equal($result, array("format_id" => 1, "roles" => array(1 => true, 2 => true)));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 1);
		$this->assert_equal($result, array("format_id" => 1, "roles" => array(1 => true, 2 => true)));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 1);
		$this->assert_equal($result, array("format_id" => 1, "roles" => array(1 => true, 2 => true)));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 2);
		$this->assert_equal($result, array("format_id" => 2, "roles" => array(3 => true)));

		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 3);
		$this->assert_equal($result, array("format_id" => 1, "roles" => array(1 => true, 2 => true)));
	}

	public function activity_fetch_roles_2_test()
	{
		$this->mcache->flush();
		$this->db->test_begin();
		$this->db->begin();
		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (1, 1, 0)");
		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (2, 3, 0)");
		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (3, 2, 0)");

		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 1);
		$this->assert_equal($result, array("format_id" => 1, "roles" => array(2 => true)));

		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 2);
		$this->assert_equal($result, array("format_id" => 2, "roles" => array()));

		$result = activity_roles_helper::fetch_roles_for_calculus($this->stream_obj, 3);
		$this->assert_equal($result, array("format_id" => 1, "roles" => array(1 => true)));

		$this->db->test_end();
	}

	public function activity_user_match_test()
	{
		$this->mcache->flush();
		$this->db->test_begin();
		$this->db->begin();

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_activity_user_match($this->stream_obj, $activity_id = 1, $user_id = 1);
		$this->assert_equal($result, array("roles" => array(
				1 => 6,
				2 => 4,
			), "maximum_match" => 6));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_activity_user_match($this->stream_obj, $activity_id = 1, $user_id = 1);
		$this->assert_equal($result, array("roles" => array(
				1 => 6,
				2 => 4,
			), "maximum_match" => 6));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$result = potential_helper::get_activity_user_match($this->stream_obj, $activity_id = 2, $user_id = 2);
		$this->assert_equal($result, array("roles" => array(
				3 => 2,
			), "maximum_match" => 2));

		$result = potential_helper::get_activity_user_match($this->stream_obj, $activity_id = 3, $user_id = 1);
		$this->assert_equal($result, array("roles" => array(
				1 => 6,
				2 => 4,
			), "maximum_match" => 6));

		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (3, 1, 0)");
		$this->mcache->flush();

		$result = potential_helper::get_activity_user_match($this->stream_obj, $activity_id = 3, $user_id = 1);
		$this->assert_equal($result, array("roles" => array(
				2 => 4,
			), "maximum_match" => 4));

		$this->db->test_end();
	}

	public function get_activities_user_match_maximum_no_debug_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		// Checking that "9" is not included since its stream_id = 2
		$this->db->sql("DELETE FROM activity WHERE id NOT IN (1, 2, 9)");

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_activities_user_match_maximum($this->stream_obj, $user_id = 1);
		$this->assert_equal($result, 6);
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_activities_user_match_maximum($this->stream_obj, $user_id = 1);
		$this->assert_equal($result, 6);
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$this->db->test_end();
	}

	public function get_activities_user_match_maximum_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		$activities_data = $this->db->fetch_all("SELECT * FROM activity WHERE id IN (1, 2)");

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_activities_user_match_maximum($this->stream_obj, $user_id = 1, $activities_data, $debug_no_cache = true);
		$this->assert_equal($result, 6);
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_activities_user_match_maximum($this->stream_obj, $user_id = 1, $activities_data, $debug_no_cache = true);
		$this->assert_equal($result, 6);
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (1, 1, 0)");
		$this->mcache->flush();

		$result = potential_helper::get_activities_user_match_maximum($this->stream_obj, $user_id = 1, $activities_data, $debug_no_cache = true);
		$this->assert_equal($result, 5);

		$this->db->test_end();
	}

	public function fill_activities_potential_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		$this->mcache->flush();
		$activities_data = $this->db->fetch_all("SELECT * FROM activity WHERE id IN (1, 2, 3) ORDER BY id");
		potential_helper::fill_activities_potential($this->stream_obj, $activities_data, $user_id = 1, $debug = true);
		$result = array_column($activities_data, "potential");
		$this->assert_equal($result, array(10, 9 /* ceil(5*10/6) */, 10));

		$this->mcache->flush();
		$this->db->sql("DELETE FROM rate_competence_link WHERE rate_id = 201 AND competence_id = 1");
		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (2, 3, 0)");
		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (1, 2, 0)");
		$this->db->sql("INSERT INTO activity_role_link (activity_id, role_id, enabled) VALUES (3, 2, 0)");
		$activities_data = $this->db->fetch_all("SELECT * FROM activity WHERE id IN (1, 2, 3) ORDER BY id");
		potential_helper::fill_activities_potential($this->stream_obj, $activities_data, $user_id = 1, $debug = true);
		$result = array_column($activities_data, "potential");
		$this->assert_equal($result, array(10, 0, 10));

		$this->db->test_end();
	}

	public function get_activity_user_potential_test()
	{
		$this->mcache->flush();

		$debug_activities_data = $this->db->fetch_all("SELECT * FROM activity WHERE id IN (1, 2, 3) ORDER BY id");
		$result = potential_helper::get_activity_user_potential($this->stream_obj, $activity_id = 1, $user_id = 1, $debug_activities_data);
		$this->assert_equal($result, array(
			"roles" => array(1 => 10, 2 => 7 /* ceil(4/6*10) */),
			"maximum_match" => 10,
		));
	}

	public function get_study_materials_match_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_study_materials_match($this->stream_obj, $user_id = 2);
		$this->assert_equal($result, array(1 => 4, 3 => 2, 2 => 0));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = potential_helper::get_study_materials_match($this->stream_obj, $user_id = 2);
		$this->assert_equal($result, array(1 => 4, 3 => 2, 2 => 0));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$this->db->test_end();
	}

	public function fill_study_materials_potential_test()
	{
		$data = array(
			array("id" => 1),
			array("id" => 2),
			array("id" => 3),
		);
		potential_helper::fill_study_materials_potential($this->stream_obj, $data, 2);
		$this->assert_equal($data, array(
			array("id" => 1, "potential" => 10),
			array("id" => 2, "potential" => 0),
			array("id" => 3, "potential" => 5),
		));
	}

}

?>