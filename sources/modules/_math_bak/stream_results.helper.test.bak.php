<?php

class stream_results_helper_test extends base_test
{

	public function set_up()
	{
		$this->db->sql("DELETE FROM role");
		$this->db->sql("DELETE FROM position");
		$this->db->sql("DELETE FROM activity_participant_mark");
		$this->db->sql("DELETE FROM activity_competence_mark");
		$this->db->sql("DELETE FROM rate");
	}

	public function result_number_format_test()
	{
		$this->assert_identical(stream_results_helper::results_number_format(1 + 2 / 3), "1,67");
	}

	public function get_competence_mark_1_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				3 => array(
					"count" => 4,
					"roles" => array(123 => true, 124 => true),
				),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "2.33");
	}

	public function get_competence_mark_2_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				1 => array(
					"count" => 999,
					"roles" => array(1 => true, 2 => true, 3 => true, 4 => true),
				),
				3 => array(
					"count" => 2,
					"roles" => array(123 => true, 124 => true),
				),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "2.25");
	}

	public function get_competence_mark_3_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				2 => array(
					"count" => 999,
					"roles" => array(1 => true, 2 => true, 3 => true, 4 => true),
				),
				3 => array(
					"count" => 1.4,
					"roles" => array(123 => true, 124 => true),
				),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "2.35");
	}

	public function get_competence_mark_4_test()
	{
		$mark = stream_results_helper::get_competence_mark(array());
		$this->assert_identical(number_format($mark, 2, ".", ""), "0.00");
	}

	public function get_competence_mark_5_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				3 => array(
					"count" => 1,
					"roles" => array(123 => true),
				),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "1.00");
	}

	public function get_competence_mark_6_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				1 => array(
					"count" => 1,
					"roles" => array(123 => true),
				),
				3 => array(
					"count" => 1,
					"roles" => array(123 => true),
				),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "2.00");
	}

	public function get_competence_mark_7_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				1 => array("count" => 7, "roles" => array(1, 2, 3)),
				2 => array("count" => 3, "roles" => array(1)),
				3 => array("count" => 3, "roles" => array(1, 2)),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "2.33");
	}

	public function get_competence_mark_8_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				3 => array(
					"count" => 0.7,
					"roles" => array(123 => true),
				),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "1.00");
	}

	public function get_competence_mark_9_test()
	{
		$mark = stream_results_helper::get_competence_mark(array(
				1 => array(
					"count" => 0.7,
					"roles" => array(123 => true),
				),
				3 => array(
					"count" => 0.7,
					"roles" => array(123 => true),
				),
		));
		$this->assert_identical(number_format($mark, 2, ".", ""), "2.00");
	}

	public function get_competences_results_from_data_test()
	{
		$res = stream_results_helper::get_plain_competences_results_from_data(array(
				// user_id => competence_id => competence_data
				4 => array(
					121 => array(),
					3 => array(
						3 => array(
							"count" => 4,
							"roles" => array(123 => true, 124 => true),
						),
					),
				),
				5 => array(
					15 => array(
						2 => array(
							"count" => 999,
							"roles" => array(1 => true, 2 => true, 3 => true, 4 => true),
						),
						3 => array(
							"count" => 2,
							"roles" => array(123 => true, 124 => true),
						),
					),
				),
		));
		$res[4][121] = number_format($res[4][121], 2, ".", "");
		$res[4][3] = number_format($res[4][3], 2, ".", "");
		$res[5][15] = number_format($res[5][15], 2, ".", "");
		$this->assert_equal($res, array(
			4 => array(121 => "0.00", 3 => "2.33"),
			5 => array(15 => "2.50"),
		));
	}

	public function get_integral_mark_from_match_percent_test()
	{
		$mark = stream_results_helper::get_integral_mark_from_match_percent(100);
		$mark_formatted = number_format($mark, 2, ".", "");
		$this->assert_identical($mark, 2.0);
		$this->assert_identical($mark_formatted, "2.00");

		$mark = stream_results_helper::get_integral_mark_from_match_percent(70);
		$mark_formatted = number_format($mark, 2, ".", "");
		$this->assert_identical($mark, 2.0);
		$this->assert_identical($mark_formatted, "2.00");

		$mark = stream_results_helper::get_integral_mark_from_match_percent(35);
		$mark_formatted = number_format($mark, 2, ".", "");
		$this->assert_identical($mark_formatted, "1.05");

		$mark = stream_results_helper::get_integral_mark_from_match_percent(66);
		$mark_formatted = number_format($mark, 2, ".", "");
		$this->assert_identical($mark_formatted, "1.98");

		$mark = stream_results_helper::get_integral_mark_from_match_percent(0);
		$mark_formatted = number_format($mark, 2, ".", "");
		$this->assert_identical($mark_formatted, "0.00");
	}

	public function use_integral_competences_for_results_test()
	{
		$plain_results = array(
			1 => array(1 => 0.25, 4 => 0.5),
			2 => array(2 => 1.5, 3 => 1),
			3 => array(2 => 1.5, 3 => 1, 4 => 0.01),
			4 => array(2 => 1.5, 3 => 1, 4 => 3),
		);
		$integral_data = array(
			1 => true,
			2 => true,
			3 => array(1 => 1, 2 => 2),
			4 => array(3 => 3),
		);
		$results_correct = array(
			1 => array(1 => "0.25", 4 => "0.50", 3 => "0.25"),
			2 => array(2 => "1.50", 3 => "1.50", 4 => "1.50"),
			3 => array(2 => "1.50", 3 => "1.50", 4 => "1.50"),
			4 => array(2 => "1.50", 3 => "1.50", 4 => "3.00"),
		);
		$results_info_correct = array(
			1 => array(3 => array("integral" => true)),
			2 => array(3 => array("integral" => true), 4 => array("integral" => true)),
			3 => array(3 => array("integral" => true), 4 => array("integral" => true)),
			4 => array(3 => array("integral" => true)),
		);
		list($results, $results_info) = stream_results_helper::use_integral_competences_for_results($plain_results, $integral_data);
		foreach ($results as &$data1)
		{
			foreach ($data1 as &$data2)
			{
				$data2 = number_format($data2, 2, ".", "");
			}
			unset($data2);
		}
		unset($data1);
		$this->assert_identical($results, $results_correct);
		$this->assert_identical($results_info, $results_info_correct);
	}

	public function get_competences_results_test()
	{
		$this->db->test_begin();
		$this->db->begin();
		$this->db->sql("INSERT IGNORE INTO user (id, login) VALUES (1, '#test1')");
		$this->db->sql("INSERT IGNORE INTO user (id, login) VALUES (2, '#test2')");
		$this->db->sql("INSERT IGNORE INTO competence_set (id, title) VALUES (1, '')");
		$this->db->sql("INSERT IGNORE INTO competence_set (id, title) VALUES (2, '')");
		$this->db->sql("DELETE FROM competence_calc WHERE competence_id IN (1, 2, 3, 4, 5, 6)");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (1, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (2, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (3, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (4, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (5, 2, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (6, 1, '')");
		$this->db->sql("DELETE FROM integral_competence");
		$this->db->sql("REPLACE INTO stream (id, name, title, title_short, competence_set_id) VALUES (1, 'test1', 'test1', 'test1', 1)");
		$this->db->sql("REPLACE INTO stream (id, name, title, title_short, competence_set_id) VALUES (2, 'test2', 'test2', 'test2', 2)");
		$this->db->sql("REPLACE INTO stream (id, name, title, title_short, competence_set_id) VALUES (3, 'test3', 'test3', 'test3', 1)");
		$this->db->sql("INSERT IGNORE INTO format_group (id, stream_id) VALUES (1, 1)");
		$this->db->sql("INSERT IGNORE INTO format_group (id, stream_id) VALUES (2, 2)");
		$this->db->sql("INSERT IGNORE INTO format (id, stream_id, format_group_id) VALUES (1, 1, 1)");
		$this->db->sql("INSERT IGNORE INTO format (id, stream_id, format_group_id) VALUES (2, 2, 2)");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (1, 1, 5)");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (2, 1, 5)");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (3, 1, 5)");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (4, 1, 5)");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (5, 1, 5)");
		$this->db->sql("INSERT INTO role (id, stream_id, title, format_id, rate_id) VALUES (1, 1, '', 1, 1)");
		$this->db->sql("INSERT INTO role (id, stream_id, title, format_id, rate_id) VALUES (2, 1, '', 1, 2)");
		$this->db->sql("INSERT INTO role (id, stream_id, title, format_id, rate_id) VALUES (3, 1, '', 1, 3)");
		$this->db->sql("INSERT INTO role (id, stream_id, title, format_id, rate_id) VALUES (4, 1, '', 1, 4)");
		$this->db->sql("INSERT INTO role (id, stream_id, title, format_id, rate_id) VALUES (5, 2, '', 2, 5)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (1, 1, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (2, 2, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (2, 3, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (3, 4, 1)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (4, 4, 1)");
		$this->db->sql("DELETE FROM activity WHERE id IN (1, 2, 3, 4, 5, 6, 7)");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, weight, title) VALUES (1, 1, 1, 'hi', '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, weight, title) VALUES (2, 1, 1, 'hi', '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, weight, title) VALUES (3, 1, 1, 'hi', '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, weight, title) VALUES (4, 1, 1, 'hi', '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, weight, title) VALUES (5, 1, 1, 'hi', '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, weight, title) VALUES (6, 1, 1, 'hi', '')");
		$this->db->sql("INSERT INTO activity (id, stream_id, format_id, weight, title) VALUES (7, 2, 2, 'hi', '')");
		$this->db->sql("DELETE FROM module WHERE id IN (1)");
		$this->db->sql("INSERT INTO module (id, stream_id, title) VALUES (1, 1, '')");
		$this->db->sql("INSERT INTO activity_module_link (activity_id, module_id) VALUES (1, 1)");
		$this->db->sql("INSERT INTO activity_module_link (activity_id, module_id) VALUES (6, 1)");
		$this->db->sql("INSERT INTO activity_participant_mark (`activity_id`, `user_id`, `role_id`, `mark`, `change_time`, `changer_user_id`) VALUES (1, 1, 1, 1, '2014-01-01', 1)");
		$this->db->sql("INSERT INTO activity_participant_mark (`activity_id`, `user_id`, `role_id`, `mark`, `change_time`, `changer_user_id`) VALUES (1, 2, 2, 3, '2014-01-01', 1)");
		$this->db->sql("INSERT INTO activity_participant_mark (`activity_id`, `user_id`, `role_id`, `mark`, `change_time`, `changer_user_id`) VALUES (2, 1, 3, 3, '2014-01-01', 1)");
		$this->db->sql("INSERT INTO activity_participant_mark (`activity_id`, `user_id`, `role_id`, `mark`, `change_time`, `changer_user_id`) VALUES (6, 1, 4, 3, '2014-01-01', 1)");
		// Not used because mark = 1 and rate_competence_link = 1
		$this->db->sql("INSERT INTO activity_participant_mark (`activity_id`, `user_id`, `role_id`, `mark`, `change_time`, `changer_user_id`) VALUES (1, 1, 4, 1, '2014-01-01', 1)");
		// Not used because stream_id = 2 for activity_id = 2
		$this->db->sql("INSERT INTO activity_participant_mark (`activity_id`, `user_id`, `role_id`, `mark`, `change_time`, `changer_user_id`) VALUES (7, 1, 4, 3, '2014-01-01', 1)");

		//       cmp1  cmp2  cmp3  cmp4
		// user1 1                 3(1) 3(1)
		// user2       3     3

		$stream_obj = stream_obj::instance(1);

		/*		 * ******************************************************************* */
		/* BASIC TEST */

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "0.25", 4 => "0.50"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "0.25", 4 => "0.50"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		/*		 * ******************************************************************* */
		/* INTEGRAL COMPETENCES TEST */

		$this->db->sql("INSERT INTO integral_competence (competence_id, included_competence_id, mark) VALUES (4, 2, 2)");
		$this->db->sql("INSERT INTO integral_competence (competence_id, included_competence_id, mark) VALUES (4, 3, 3)");
		$this->db->sql("UPDATE competence_full SET is_integral = 1 WHERE id = 4");

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$res[2][4] = number_format($res[2][4], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "0.25", 4 => "0.50"),
			2 => array(2 => "1.00", 3 => "1.00", 4 => "1.20"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$res[2][4] = number_format($res[2][4], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "0.25", 4 => "0.50"),
			2 => array(2 => "1.00", 3 => "1.00", 4 => "1.20"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$this->db->sql("UPDATE competence_full SET is_integral = 0 WHERE id = 4");

		/*		 * ******************************************************************* */
		/* MODULE TEST */

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj, $module_id = 1);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "0.25", 4 => "0.25"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj, $module_id = 1);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "0.25", 4 => "0.25"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		/*		 * ******************************************************************* */
		/* TEST OF COMPETENCE MARKS */

		$this->db->sql("INSERT INTO activity_competence_mark (`activity_id`, `user_id`, `competence_id`, `mark`, `change_time`, `changer_user_id`) VALUES (1, 1, 1, 3, '2014-01-01', 1)");

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "2.00", 4 => "0.50"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "2.00", 4 => "0.50"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		/*		 * ******************************************************************* */
		/* LOW WEIGHT TEST (COMPETENCE MARKS INCLUDED) */

		$this->db->sql("UPDATE activity SET weight = 'low' WHERE id = 2");

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "2.00", 4 => "0.38"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "2.00", 4 => "0.38"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		/*		 * ******************************************************************* */
		/* LOW WEIGHT TEST OF COMPETENCE MARKS */

		$this->db->sql("INSERT INTO activity_competence_mark (`activity_id`, `user_id`, `competence_id`, `mark`, `change_time`, `changer_user_id`) VALUES (2, 1, 1, 3, '2014-01-01', 1)");

		$this->mcache->flush();

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "2.13", 4 => "0.38"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		list($res, ) = stream_results_helper::get_competences_results($stream_obj);
		$res[1][1] = number_format($res[1][1], 2, ".", "");
		$res[1][4] = number_format($res[1][4], 2, ".", "");
		$res[2][2] = number_format($res[2][2], 2, ".", "");
		$res[2][3] = number_format($res[2][3], 2, ".", "");
		$this->assert_equal($res, array(
			1 => array(1 => "2.13", 4 => "0.38"),
			2 => array(2 => "1.00", 3 => "1.00"),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		/*		 * ******************************************************************* */

		$this->db->test_end();
	}

	public function get_position_match_percent_1_test()
	{
		$match = stream_results_helper::get_position_match_percent(array(
				1 => 2,
				2 => 1,
				), array(
				1 => 1,
				2 => 1,
		));
		$this->assert_identical($match, 67);
	}

	public function get_position_match_percent_no_round_test()
	{
		$match = stream_results_helper::get_position_match_percent(array(
				1 => 2,
				2 => 1,
				), array(
				1 => 1,
				2 => 1,
				), false);
		$this->assert_identical(number_format($match, 2, ".", ""), "66.67");
	}

	public function get_position_match_percent_2_test()
	{
		$match = stream_results_helper::get_position_match_percent(array(
				1 => 2,
				2 => 1,
				), array(
				1 => 2,
				2 => 2,
		));
		$this->assert_identical($match, 100);
	}

	public function get_position_match_percent_3_test()
	{
		$match = stream_results_helper::get_position_match_percent(array(
				), array(
				1 => 2,
				2 => 2,
		));
		$this->assert_identical($match, 0);
	}

	public function get_position_match_percent_4_test()
	{
		$match = stream_results_helper::get_position_match_percent(array(
				100 => 2,
				200 => 3,
				500 => 2,
				), array(
				100 => 3,
				200 => 2,
				600 => 3,
		));
		$this->assert_identical($match, 57);
	}

	public function get_positions_results_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		$this->db->sql("DELETE FROM competence_calc WHERE competence_id IN (1, 2, 3, 4, 5, 6)");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (1, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (2, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (3, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (4, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (5, 1, '')");
		$this->db->sql("INSERT INTO competence_calc (competence_id, competence_set_id, title) VALUES (6, 2, '')");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (1, 1, 6)");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (2, 1, 6)");
		$this->db->sql("INSERT INTO rate (id, competence_set_id, rate_type_id) VALUES (3, 1, 6)");
		$this->db->sql("INSERT INTO position (id, stream_id, title, rate_id) VALUES (1, 1, '', 1)");
		$this->db->sql("INSERT INTO position (id, stream_id, title, rate_id) VALUES (2, 1, '', 2)");
		$this->db->sql("INSERT INTO position (id, stream_id, title, rate_id) VALUES (3, 1, '', 3)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (1, 1, 1)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (1, 5, 1)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (1, 6, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (2, 1, 2)");
		$this->db->sql("INSERT INTO rate_competence_link (rate_id, competence_id, mark) VALUES (2, 2, 2)");

		// cmp         1 2 3 4 5
		// ---------------------
		// pos1 (mark) 1       1
		// pos2 (mark) 2 2
		// usr1 (mark) 1 1
		// usr2 (mark) 1 2   3

		$stream_obj = stream_obj::instance(1);

		$this->mcache->flush();

		stream_results_competences_cache::init($stream_obj, null)->set(array(
			array(
				1 => array(1 => 1, 2 => 1),
				2 => array(1 => 1, 2 => 2, 4 => 3),
			),
			array(),
		));

		$q = $this->db->debug_get_query_count();
		$result = stream_results_helper::get_positions_results($stream_obj);
		$this->assert_identical($result, array(
			1 => array(2 => 50, 1 => 50),
			2 => array(2 => 75, 1 => 50),
			3 => array(2 => 0, 1 => 0),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = stream_results_helper::get_positions_results($stream_obj);
		$this->assert_identical($result, array(
			1 => array(2 => 50, 1 => 50),
			2 => array(2 => 75, 1 => 50),
			3 => array(2 => 0, 1 => 0),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		// Module test

		stream_results_competences_cache::init($stream_obj, 123456)->set(array(
			array(
				1 => array(1 => 1, 2 => 1),
				2 => array(1 => 1, 2 => 2, 4 => 3),
			),
			array(),
		));

		$q = $this->db->debug_get_query_count();
		$result = stream_results_helper::get_positions_results($stream_obj, 123456);
		$this->assert_identical($result, array(
			1 => array(2 => 50, 1 => 50),
			2 => array(2 => 75, 1 => 50),
			3 => array(2 => 0, 1 => 0),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q > 0);

		$q = $this->db->debug_get_query_count();
		$result = stream_results_helper::get_positions_results($stream_obj, 123456);
		$this->assert_identical($result, array(
			1 => array(2 => 50, 1 => 50),
			2 => array(2 => 75, 1 => 50),
			3 => array(2 => 0, 1 => 0),
		));
		$this->assert_true($this->db->debug_get_query_count() - $q == 0);

		$this->db->test_end();
	}

	public function get_user_results_test()
	{
		$this->db->test_begin();
		$this->db->begin();

		$this->db->sql("INSERT IGNORE INTO user (id, login) VALUES (1, '#test1')");
		$this->db->sql("INSERT IGNORE INTO user (id, login) VALUES (2, '#test2')");
		$this->db->sql("INSERT IGNORE INTO user (id, login) VALUES (3, '#test2')");
		$this->db->sql("UPDATE user SET city_id = 1 WHERE id IN (1, 2)");
		$this->db->sql("UPDATE user SET city_id = 2 WHERE id IN (3)");

		$stream_obj = stream_obj::instance(1);

		$this->mcache->flush();

		stream_results_cache::init($stream_obj, null)->set(array(
			1 => array(2 => 50, 1 => 50, 3 => 0),
			2 => array(2 => 75, 1 => 50, 3 => 25),
			3 => array(2 => 0, 1 => 0, 3 => 0),
			4 => array(3 => 48, 1 => 10, 2 => 10),
		));

		$result = stream_results_helper::get_user_results($stream_obj, 2);
		$this->assert_identical($result, array(
			2 => array("match" => 75, "place" => 1, "city_place" => 1),
			1 => array("match" => 50, "place" => 1, "city_place" => 1),
			4 => array("match" => 10, "place" => 2, "city_place" => 1),
			3 => array("match" => 0, "place" => 1, "city_place" => 1),
		));

		$this->db->test_end();
	}

	public function tear_down()
	{
		$this->mcache->flush();
	}

}

?>