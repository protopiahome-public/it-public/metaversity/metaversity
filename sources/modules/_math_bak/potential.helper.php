<?php

class potential_helper extends base_static_db_helper
{

	public static function fill_activities_potential(stream_obj $stream_obj, &$activities, $user_id, $debug = false)
	{
		if (!$user_id)
		{
			return;
		}
		$match_max = self::get_activities_user_match_maximum($stream_obj, $user_id, $debug ? $activities : null, $debug);
		if ($match_max == 0)
		{
			foreach ($activities as &$activity_row)
			{
				$activity_row["potential"] = 0;
			}
			unset($activity_row);
			return;
		}
		foreach ($activities as &$activity_row)
		{
			$activity_match = self::get_activity_user_match($stream_obj, $activity_row, $user_id);
			$activity_row["potential"] = self::get_potential($activity_match["maximum_match"], $match_max);
		}
		unset($activity_row);
	}

	public static function get_activities_user_match_maximum(stream_obj $stream_obj, $user_id, $debug_activities_data = null, $debug_no_cache = false)
	{
		$stream_id = $stream_obj->get_id();
		$cache_key = "{$stream_id}-{$user_id}";
		static $local_cache = array();
		if (isset($local_cache[$cache_key]) and !$debug_no_cache)
		{
			return $local_cache[$cache_key];
		}
		$result = stream_activities_user_match_maximum_cache::init($stream_obj, $user_id)->get();
		if ($result)
		{
			$local_cache[$cache_key] = $result;
			return $result;
		}
		if (!is_null($debug_activities_data))
		{
			$activities_data = $debug_activities_data;
		}
		else
		{
			$select_sql = new select_sql();
			$select_sql->add_select_fields("a.*");
			$select_sql->add_from("activity a");
			$select_sql->add_where("a.stream_id = {$stream_id}");
			$activities_data = self::db()->fetch_all($select_sql->get_sql());
		}
		$result = 0;
		foreach ($activities_data as $activity_row)
		{
			$match = self::get_activity_user_match($stream_obj, $activity_row, $user_id);
			if ($match["maximum_match"] > $result)
			{
				$result = $match["maximum_match"];
			}
		}

		stream_activities_user_match_maximum_cache::init($stream_obj, $user_id)->set($result);
		$local_cache[$cache_key] = $result;
		return $result;
	}

	public static function get_activity_user_match(stream_obj $stream_obj, $activity_id_or_row, $user_id)
	{
		$activity_row = activity_helper::get_row($activity_id_or_row);
		$activity_id = $activity_row["id"];
		$format_id = $activity_row["format_id"];
		$result = stream_activity_user_match_cache::init($stream_obj, $activity_id, $user_id, $format_id)->get();
		if ($result)
		{
			return $result;
		}
		$format_match = self::get_format_match($stream_obj, $format_id, $user_id);
		$roles = activity_roles_helper::fetch_roles_for_calculus($stream_obj, $activity_row);
		foreach ($format_match as $role_id => $match)
		{
			if (!isset($roles["roles"][$role_id]))
			{
				unset($format_match[$role_id]);
			}
		}
		$maximum_match = reset($format_match);
		$result = array(
			"roles" => $format_match,
			"maximum_match" => $maximum_match,
		);
		stream_activity_user_match_cache::init($stream_obj, $activity_id, $user_id, $format_id)->set($result);
		return $result;
	}

	public static function get_format_match(stream_obj $stream_obj, $format_id, $user_id)
	{
		$result = stream_format_user_matches_cache::init($stream_obj, $format_id, $user_id)->get();
		if ($result)
		{
			return $result;
		}
		$competence_set_id = $stream_obj->get_competence_set_id();
		$competences_data = self::db()->fetch_all("
			SELECT role.id AS role_id, rcl.competence_id, rcl.mark
			FROM role
			INNER JOIN rate ON rate.id = role.rate_id
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = rate.id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE role.format_id = {$format_id} AND c.competence_set_id = {$competence_set_id}
		");
		$data = self::db()->fetch_column_values("
			SELECT id
			FROM role
			WHERE format_id = {$format_id}
		", true, "id");
		foreach ($data as $role_id => &$role_data)
		{
			$role_data = array();
		}
		unset($role_data);
		foreach ($competences_data as $competence_data)
		{
			list($role_id, $competence_id, $mark) = array_values($competence_data);
			$data[$role_id][$competence_id] = $mark;
		}
		$user_competence_list = self::get_positions_competence_list($stream_obj, $user_id);
		$result = self::get_match_multi($data, $user_competence_list);
		stream_format_user_matches_cache::init($stream_obj, $format_id, $user_id)->set($result);
		return $result;
	}

	public static function get_positions_competence_list(stream_obj $stream_obj, $user_id)
	{
		$result = self::db()->fetch_column_values("
			SELECT rcl.competence_id, MAX(rcl.mark) AS mark
			FROM user_position up
			INNER JOIN position p ON p.id = up.position_id
			INNER JOIN rate r ON r.id = p.rate_id
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = r.id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE up.user_id = {$user_id}
				AND p.stream_id = {$stream_obj->get_id()}
				AND c.competence_set_id = {$stream_obj->get_competence_set_id()}
			GROUP BY rcl.competence_id
		", "mark", "competence_id");
		return $result;
	}

	public static function get_activity_user_potential(stream_obj $stream_obj, $activity_id_or_row, $user_id, $debug_activities_data = null)
	{
		$match_max = self::get_activities_user_match_maximum($stream_obj, $user_id, $debug_activities_data, !is_null($debug_activities_data));
		$result = self::get_activity_user_match($stream_obj, $activity_id_or_row, $user_id);
		$result["maximum_match"] = self::get_potential($result["maximum_match"], $match_max);
		foreach ($result["roles"] as $role_id => $match)
		{
			$result["roles"][$role_id] = self::get_potential($match, $match_max);
		}
		return $result;
	}

	public static function fill_study_materials_potential(stream_obj $stream_obj, &$study_materials, $user_id)
	{
		if (!$user_id)
		{
			return;
		}
		$match_data = self::get_study_materials_match($stream_obj, $user_id);
		$match_max = reset($match_data);
		foreach ($study_materials as &$study_material_row)
		{
			$study_material_id = $study_material_row["id"];
			$match = isset($match_data[$study_material_id]) ? $match_data[$study_material_id] : 0;
			$study_material_row["potential"] = $match_max == 0 ? 0 : self::get_potential($match, $match_max);
		}
		unset($study_material_row);
	}

	public static function get_study_materials_match(stream_obj $stream_obj, $user_id)
	{
		$result = stream_study_materials_user_match_cache::init($stream_obj, $user_id)->get();
		if ($result)
		{
			return $result;
		}
		$competence_set_id = $stream_obj->get_competence_set_id();
		$competences_data = self::db()->fetch_all("
			SELECT sm.id AS study_material_id, rcl.competence_id, rcl.mark
			FROM study_material sm
			INNER JOIN rate ON rate.id = sm.rate_id
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = rate.id
			LEFT JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE c.competence_set_id = {$competence_set_id}
		");
		$data = self::db()->fetch_column_values("
			SELECT id
			FROM study_material
		", array(), "id");
		foreach ($competences_data as $competence_data)
		{
			list($study_material_id, $competence_id, $mark) = array_values($competence_data);
			$data[$study_material_id][$competence_id] = $mark;
		}
		$user_competence_list = self::get_positions_competence_list($stream_obj, $user_id);
		$result = self::get_match_multi($data, $user_competence_list);
		stream_study_materials_user_match_cache::init($stream_obj, $user_id)->set($result);
		return $result;
	}

	public static function get_match($array1, $array2)
	{
		$result = 0;
		foreach ($array1 as $idx => $val)
		{
			if (isset($array2[$idx]))
			{
				$result += $val * $array2[$idx];
			}
		}
		return $result;
	}

	public static function get_match_multi($multi_array, $array2)
	{
		$result = array();
		foreach ($multi_array as $idx => $array1)
		{
			$result[$idx] = self::get_match($array1, $array2);
		}
		arsort($result);
		return $result;
	}

	public static function get_potential($match, $match_max)
	{
		if ($match_max == 0)
		{
			return 0;
		}
		$potential = ceil($match * 10 / $match_max);
		return $potential;
		//return $potential > 10 ? 10 : $potential;
	}

}

?>