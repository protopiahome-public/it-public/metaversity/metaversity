<?php

class stream_results_helper extends base_static_db_helper
{

	const PROVE_MARK_COUNT = 2.5;
	const PROVE_ROLE_COUNT = 3;
	const MAX_MARK = 3;
	const LOW_WEIGHT_FACTOR = 0.5;

	public static function get_competences_results(stream_obj $stream_obj, $module_id = null)
	{
		$pair = stream_results_competences_cache::init($stream_obj, $module_id)->get();
		if ($pair)
		{
			return $pair;
		}
		$plain_results = self::get_plain_competences_results_from_data(self::get_plain_competences_data($stream_obj, $module_id));
		$integral_data = integral_competences_helper::get_all_competences_in_calc_order($stream_obj->get_competence_set_id());
		$pair = self::use_integral_competences_for_results($plain_results, $integral_data);
		stream_results_competences_cache::init($stream_obj, $module_id)->set($pair);
		return $pair;
	}

	public static function get_plain_competences_results_from_data($competences_data)
	{
		$result = array();
		foreach ($competences_data as $user_id => $user_competences_data)
		{
			$result[$user_id] = array();
			foreach ($user_competences_data as $competence_id => $competence_data)
			{
				$result[$user_id][$competence_id] = self::get_competence_mark($competence_data);
			}
		}
		return $result;
	}

	public static function get_plain_competences_data(stream_obj $stream_obj, $module_id = null)
	{
		$result = array();

		// Data for role marks
		$select_sql = new select_sql("activity_participant_mark", "apm");
		$select_sql->add_select_fields("apm.user_id, rcl.competence_id, apm.role_id, rcl.mark as rate_mark, apm.mark, a.weight");
		$select_sql->add_join("INNER JOIN role r ON r.id = apm.role_id");
		$select_sql->add_join("INNER JOIN rate_competence_link rcl ON rcl.rate_id = r.rate_id");
		$select_sql->add_join("INNER JOIN activity a ON a.id = apm.activity_id");
		$select_sql->add_join("INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id");
		//$select_sql->add_where("a.stream_id = {$stream_obj->get_id()}");
		$select_sql->add_where("c.competence_set_id = {$stream_obj->get_competence_set_id()}");
		if ($module_id)
		{
			$select_sql->add_join("INNER JOIN activity_module_link aml ON aml.activity_id = apm.activity_id");
			$select_sql->add_where("aml.module_id = {$module_id}");
		}
		$data = self::db()->fetch_all($select_sql->get_sql());

		$select_sql2 = new select_sql("activity_participant_mark", "apm");
		$select_sql2->add_select_fields("apm.user_id, ctd.competence_id, apm.role_id, rcl.mark as rate_mark, apm.mark, a.weight");
		$select_sql2->add_join("INNER JOIN role r ON r.id = apm.role_id");
		$select_sql2->add_join("INNER JOIN rate_competence_link rcl ON rcl.rate_id = r.rate_id");
		$select_sql2->add_join("INNER JOIN activity a ON a.id = apm.activity_id");
		$select_sql2->add_join("INNER JOIN competence_translation_direct ctd ON ctd.eq_competence_id = rcl.competence_id");
		$select_sql2->add_join("INNER JOIN competence_calc c ON c.competence_id = ctd.competence_id");
		$select_sql2->add_where("c.competence_set_id = {$stream_obj->get_competence_set_id()}");
		if ($module_id)
		{
			$select_sql2->add_join("INNER JOIN activity_module_link aml ON aml.activity_id = apm.activity_id");
			$select_sql2->add_where("aml.module_id = {$module_id}");
		}
		$data2 = self::db()->fetch_all($select_sql2->get_sql());

		$data = array_merge($data, $data2);

		foreach ($data as $row)
		{
			$user_id = (int) $row["user_id"];
			$competence_id = (int) $row["competence_id"];
			$role_id = (int) $row["role_id"];
			$mark = $row["rate_mark"] == 1 ? 1 : (int) $row["mark"];
			$mult = $row["rate_mark"] == 1 ? ($row["mark"] > 1 ? 1.0 : 0.5) : 1.0;
			$weight_is_low = $row["weight"] == activity_helper::WEIGHT_LOW;
			if (is_null($mark))
			{
				continue;
			}
			if (!isset($result[$user_id]))
			{
				$result[$user_id] = array();
			}
			if (!isset($result[$user_id][$competence_id]))
			{
				$result[$user_id][$competence_id] = array();
			}
			if (!isset($result[$user_id][$competence_id][$mark]))
			{
				$result[$user_id][$competence_id][$mark] = array(
					"count" => 0,
					"roles" => array(),
				);
			}
			$result[$user_id][$competence_id][$mark]["count"] += ($weight_is_low ? self::LOW_WEIGHT_FACTOR : 1.0) * $mult;
			$result[$user_id][$competence_id][$mark]["roles"][$role_id] = true;
		}
		// Data for competence marks
		if (!$module_id)
		{
			$select_sql = new select_sql("activity_competence_mark", "acm");
			$select_sql->add_select_fields("acm.activity_id, acm.user_id, acm.competence_id, acm.mark, a.weight");
			$select_sql->add_join("INNER JOIN activity a ON a.id = acm.activity_id");
			$select_sql->add_join("INNER JOIN competence_calc c ON c.competence_id = acm.competence_id");
			//$select_sql->add_where("a.stream_id = {$stream_obj->get_id()}");
			$select_sql->add_where("c.competence_set_id = {$stream_obj->get_competence_set_id()}");
			$data = self::db()->fetch_all($select_sql->get_sql());

			$select_sql2 = new select_sql("activity_competence_mark", "acm");
			$select_sql2->add_select_fields("acm.activity_id, acm.user_id, ctd.competence_id, acm.mark, a.weight");
			$select_sql2->add_join("INNER JOIN activity a ON a.id = acm.activity_id");
			$select_sql2->add_join("INNER JOIN competence_translation_direct ctd ON ctd.eq_competence_id = acm.competence_id");
			$select_sql2->add_join("INNER JOIN competence_calc c ON c.competence_id = ctd.competence_id");
			//$select_sql2->add_where("a.stream_id = {$stream_obj->get_id()}");
			$select_sql2->add_where("c.competence_set_id = {$stream_obj->get_competence_set_id()}");
			$data2 = self::db()->fetch_all($select_sql2->get_sql());

			$data = array_merge($data, $data2);

			foreach ($data as $row)
			{
				$activity_id = (int) $row["activity_id"];
				$user_id = (int) $row["user_id"];
				$competence_id = (int) $row["competence_id"];
				$mark = (int) $row["mark"];
				$weight_is_low = $row["weight"] == activity_helper::WEIGHT_LOW;
				if (!isset($result[$user_id]))
				{
					$result[$user_id] = array();
				}
				if (!isset($result[$user_id][$competence_id]))
				{
					$result[$user_id][$competence_id] = array();
				}
				if (!isset($result[$user_id][$competence_id][$mark]))
				{
					$result[$user_id][$competence_id][$mark] = array(
						"count" => 0,
						"roles" => array(),
					);
				}
				$result[$user_id][$competence_id][$mark]["count"] += $weight_is_low ? self::LOW_WEIGHT_FACTOR : 1.0;
				$result[$user_id][$competence_id][$mark]["roles"]["a" . $activity_id] = true;
			}
		}
		return $result;
	}

	public static function use_integral_competences_for_results($plain_results, $integral_data)
	{
		$results = $plain_results;
		$results_info = array();
		foreach ($results as $user_id => &$user_results)
		{
			$results_info[$user_id] = array();
			foreach ($integral_data as $competence_id => $included_competences_index)
			{
				if ($included_competences_index === true)
				{
					continue;
				}
				$percent = self::get_position_match_percent($included_competences_index, $user_results, false);
				$mark = self::get_integral_mark_from_match_percent($percent);
				if ($mark > 0)
				{
					if (!isset($user_results[$competence_id]) or $user_results[$competence_id] < $mark)
					{
						$results_info[$user_id][$competence_id] = array(
							"integral" => true,
						);
						$user_results[$competence_id] = $mark;
					}
				}
			}
		}
		unset($user_results);
		return array($results, $results_info);
	}

	public static function get_position_match_percent($position_data, $user_competences_results, $round = true)
	{
		$num = 0;
		$den = 0;
		foreach ($position_data as $competence_id => $mark)
		{
			$user_mark = isset($user_competences_results[$competence_id]) ? $user_competences_results[$competence_id] : 0;
			$num += $user_mark <= $mark ? $user_mark : $mark;
			$den += $mark;
		}
		$result = $den == 0 ? 0 : ($round ? (int) round($num * 100 / $den) : $num * 100 / $den);
		return $result > 100 ? 100 : $result;
	}

	public static function get_integral_mark_from_match_percent($percent)
	{
		return min((float) $percent / 100 * 3, 2.0);
	}

	public static function get_competence_mark($competence_data)
	{
		$mark = 0.0;
		for ($m = 1; $m <= self::MAX_MARK; ++$m)
		{
			if (!isset($competence_data[$m]))
			{
				continue;
			}
			$mark_count = $competence_data[$m]["count"];
			$role_count = sizeof($competence_data[$m]["roles"]);
			if ($mark_count == 0 or $role_count == 0)
			{
				continue;
			}
			if ($mark < $m - 1)
			{
				$mark_count = max($mark_count - 0.5, 0);
				$role_count = max($role_count - 1, 0);
			}
			if ($mark_count == 0 and $mark == 0.0 and $m > 2)
			{
				$mark = $m - 2;
			}
			else
			{
				$delta = min($role_count / self::PROVE_ROLE_COUNT, $mark_count / self::PROVE_MARK_COUNT);
				$delta = min($delta, 1);
				$mark = $m - 1 + $delta;
			}
		}
		return $mark;
	}

	public static function results_number_format($result)
	{
		return number_format($result, 2, ",", "");
	}

	public static function validate_module_id(stream_obj $stream_obj, $module_id)
	{
		if (is_array($module_id) || !is_good_id($module_id))
		{
			return null;
		}
		if (!self::db()->row_exists("
			SELECT id 
			FROM module 
			WHERE id = {$module_id} 
				AND stream_id = {$stream_obj->get_id()}
				AND show_in_results = 1
		"))
		{
			return null;
		}
		return (int) $module_id;
	}

	public static function get_user_results(stream_obj $stream_obj, $user_id, $module_id = null)
	{
		$city_id = self::db()->get_value("SELECT city_id FROM user WHERE id = {$user_id}");
		$city_users = self::db()->fetch_column_values("SELECT id FROM user WHERE city_id = {$city_id}", true, "id");
		$positions_results = self::get_positions_results($stream_obj, $module_id);
		$user_results = array();
		foreach ($positions_results as $position_id => $data)
		{
			if (!isset($data[$user_id]))
			{
				$data[$user_id] = 0;
			}
			$user_results[$position_id] = array(
				"match" => $data[$user_id],
			);
			$idx = $city_idx = 0;
			$last_place = $city_last_place = 0;
			$last_match_percent = $city_last_match_percent = -1;
			foreach ($data as $data_user_id => $match_percent)
			{
				++$idx;
				$place = $last_match_percent == $match_percent ? $last_place : $idx;
				if (isset($city_users[$data_user_id]))
				{
					++$city_idx;
					$city_place = $city_last_match_percent == $match_percent ? $city_last_place : $city_idx;
					if ($data_user_id == $user_id)
					{
						$user_results[$position_id]["place"] = $place;
						$user_results[$position_id]["city_place"] = $city_place;
						break;
					}
					$city_last_place = $city_place;
					$city_last_match_percent = $match_percent;
				}
				$last_place = $place;
				$last_match_percent = $match_percent;
			}
		}
		$user_results = array_sort($user_results, "match", SORT_DESC);
		return $user_results;
	}

	public static function get_positions_results(stream_obj $stream_obj, $module_id = null)
	{
		$result = stream_results_cache::init($stream_obj, $module_id)->get();
		if ($result)
		{
			return $result;
		}
		list($competences_results, ) = self::get_competences_results($stream_obj, $module_id);
		$positions_data = self::get_positions_data($stream_obj);
		$result = array();
		foreach ($positions_data as $position_id => $position_data)
		{
			$result[$position_id] = array();
			foreach ($competences_results as $user_id => $user_competences_results)
			{
				$result[$position_id][$user_id] = self::get_position_match_percent($position_data, $user_competences_results);
			}
			arsort($result[$position_id]);
		}
		stream_results_cache::init($stream_obj, $module_id)->set($result);
		return $result;
	}

	public static function get_positions_data(stream_obj $stream_obj)
	{
		$data = self::db()->fetch_column_values("
			SELECT id 
			FROM position
			WHERE stream_id = {$stream_obj->get_id()}
		", array(), "id");
		$competences = self::db()->fetch_all("
			SELECT p.id, rcl.competence_id, rcl.mark
			FROM position p
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = p.rate_id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE p.stream_id = {$stream_obj->get_id()}
				AND c.competence_set_id = {$stream_obj->get_competence_set_id()}
			ORDER BY p.id
		");
		foreach ($competences as $row)
		{
			$id = (int) $row["id"];
			$competence_id = (int) $row["competence_id"];
			$mark = (int) $row["mark"];
			$data[$id][$competence_id] = $mark;
		}
		return $data;
	}

}

?>