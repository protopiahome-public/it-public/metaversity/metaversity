<?php

class intmarkup_xml_dtf extends base_xml_dtf
{

	/**
	 * @var intmarkup_dtf
	 */
	protected $dtf;

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		$select_sql->add_select_fields("dt." . $this->field_name . ($is_edit_mode ? "_text" : "_html") . " AS " . $this->field_name);
	}

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		global $config;
		$max_file_size = isset($config["intmarkup_max_file_size"]) ? $config["intmarkup_max_file_size"] : 2 * 1024 * 1024;

		$dtf_node = parent::fill_doctype_xml($parent_node);
		if (($max_length = $this->dtf->get_max_length()))
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		$dtf_node->set_attr("editor_height", $this->dtf->get_editor_height());
		if ($this->dtf->get_upload_object_type())
		{
			$upload_node = $dtf_node->create_child_node("upload");
			$upload_node->set_attr("object_type", $this->dtf->get_upload_object_type());
			$upload_node->set_attr("url_base", $this->dtf->get_upload_url_base());
			$upload_node->set_attr("max_file_size", $max_file_size);
			$upload_node->set_attr("max_file_size_human", format_file_size($max_file_size));
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node("field");
		$dtf_node->set_attr("name", $this->field_name);
		$dtf_node->create_child_node("text", $db_row[$this->field_name]);
		$this->create_files_node($dtf_node, $db_row);
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node($this->field_name);
		$tidy_config = array(
			"output-xhtml" => true,
			"show-body-only" => true,
			"numeric-entities" => true,
			"newline" => "LF",
			"error-file" => PATH_LOG . "/tidy_errors.log",
		);
		$db_row[$this->field_name] = tidy_parse_string($db_row[$this->field_name], $tidy_config, "UTF8");
		$content_xdom = xdom::create_from_string("<root><div class=\"intmarkup\">{$db_row[$this->field_name]}</div></root>");
		$dtf_node->import_xdom($content_xdom, true);
		$this->create_files_node($dtf_node, $db_row);
		return $dtf_node;
	}

	private function create_files_node($dtf_node, $db_row)
	{
		$id = intmarkup_helper::normalized_id_from_row($db_row, $this->dtf->get_upload_pk_column());
		if ($this->dtf->get_upload_object_type())
		{
			$files = intmarkup_helper::get_file_list($this->dtf->get_upload_object_type(), $id);
			$files_node = $dtf_node->create_child_node("files");
			$files_node->set_attr("url_base", $this->dtf->get_upload_url_base());
			intmarkup_helper::fill_files_node($files_node, $files);
		}
	}

}

?>