<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class intmarkup_save_dtf extends base_save_dtf
{

	/**
	 * @var intmarkup_dtf
	 */
	protected $dtf;
	protected $value;
	protected $has_files;
	protected $file_ids;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		if ($this->dtf->is_important())
		{
			if (trim($this->value) === "")
			{
				$this->write_error("UNFILLED");
				return false;
			}
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$len = mb_strlen($this->value);
			if ($len > $max_length)
			{
				$this->write_error("TOO_LONG", $len);
				return false;
			}
		}

		$this->has_files = false;
		$file_ids_raw = POST($this->field_name . "_files");
		if ($file_ids_raw === "-")
		{
			$this->file_ids = "-";
			$this->has_files = $this->document_id && intmarkup_helper::object_has_files($this->dtf->get_upload_object_type(), intmarkup_helper::normalize_id($this->document_id));
		}
		else
		{
			$this->file_ids = array();
			foreach (explode(",", $file_ids_raw) as $file_id)
			{
				$file_id = trim($file_id);
				if (!is_good_id($file_id))
				{
					continue;
				}
				$this->file_ids[] = $file_id;
			}
			$field_node = $this->pass_info->get_xdom()->create_child_node("field");
			$field_node->set_attr("name", $this->dtf->get_field_name());
			$files = intmarkup_helper::get_file_list_from_array($this->dtf->get_upload_object_type(), intmarkup_helper::normalize_id($this->document_id), $this->file_ids);
			$this->has_files = sizeof($files) > 0;
			intmarkup_helper::fill_files_node($field_node->create_child_node("files"), $files);
		}

		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$write_value = text_processor::intmarkup_process($this->value);
		if ($write_value === "" and $this->has_files)
		{
			$write_value = " ";
		}
		$update_array[$this->field_name . "_text"] = "'" . $this->db->escape($this->value) . "'";
		$update_array[$this->field_name . "_html"] = "'" . $this->db->escape($write_value) . "'";
	}

	public function commit($last_id)
	{
		if ($this->file_ids === "-")
		{
			return true;
		}
		$object_type = $this->dtf->get_upload_object_type();
		$object_id = intmarkup_helper::normalize_id($last_id);
		$current_files = intmarkup_helper::get_file_list($object_type, $object_id);
		foreach ($this->file_ids as $file_id)
		{
			if (isset($current_files[$file_id]))
			{
				unset($current_files[$file_id]);
			}
		}
		intmarkup_helper::clean_files($object_type, $object_id, $current_files);
		intmarkup_helper::update_files($object_type, $object_id, $this->file_ids);
	}

}

?>