<?php

class intmarkup_dtf extends base_dtf
{

	private $is_important = false;
	private $max_length = 16777215; // 2^24 - 1, i.e. MEDIUMTEXT
	private $editor_height = 200;
	private $upload_object_type = null;
	private $upload_url_base = null;
	private $upload_pk_column = null;

	public function setup_upload($object_type, $url_base, $pk_column = null)
	{
		$this->upload_object_type = $object_type;
		$this->upload_url_base = $url_base;
		$this->upload_pk_column = $pk_column;
	}

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_max_length($max_length)
	{
		if (is_good_num($max_length))
		{
			$this->max_length = $max_length;
		}
	}

	public function set_editor_height($editor_height)
	{
		if (is_good_num($editor_height))
		{
			$this->editor_height = $editor_height;
		}
	}

	public function get_text_column_name()
	{
		return $this->text_column_name;
	}

	public function get_upload_object_type()
	{
		return $this->upload_object_type;
	}

	public function get_upload_url_base()
	{
		return $this->upload_url_base;
	}

	public function get_upload_pk_column()
	{
		return $this->upload_pk_column;
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_max_length()
	{
		return $this->max_length;
	}

	public function get_editor_height()
	{
		return $this->editor_height;
	}

}

?>