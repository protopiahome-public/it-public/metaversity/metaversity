<?php

class intmarkup_upload_file_ajax_page extends base_ajax_ctrl
{

	protected $max_file_size;
	protected $tmp_file_name;
	protected $file_name;
	protected $mime_type;
	protected $size;
	protected $error_code;
	protected $user_id;
	protected $last_id;

	public function start()
	{
		global $config;
		$this->max_file_size = isset($config["intmarkup_max_file_size"]) ? $config["intmarkup_max_file_size"] : 2 * 1024 * 1024;

		if (!isset($_FILES["filedata"]))
		{
			return false;
		}
		$this->tmp_file_name = $_FILES["filedata"]["tmp_name"];
		$this->file_name = $_FILES["filedata"]["name"];
		$this->mime_type = $_FILES["filedata"]["type"];
		$this->size = $_FILES["filedata"]["size"];
		$this->error_code = $_FILES["filedata"]["error"];
		$this->user_id = $this->user->get_user_id();
		return true;
	}

	public function check_rights()
	{
		return $this->user_id > 0;
	}

	public function check()
	{
		if ($this->size > $this->max_file_size)
		{
			$this->error_code = 100;
		}
		return 0 == $this->error_code;
	}

	public function commit()
	{
		$this->file_name = mb_substr(preg_replace("#[\x{00}-\x{1F}]#", "", $this->file_name), 0, 255);
		$this->mime_type = mb_substr(preg_replace("#[\x{00}-\x{1F}]#", "", $this->mime_type), 0, 255);
		$ext = "";
		if (preg_match("/\.[~!\$a-zA-Z0-9_-]{1,9}$/", $this->file_name, $matches))
		{
			$ext = strtolower($matches[0]);
		}
		$data = array(
			"object_type" => "'user'",
			"object_id" => $this->user_id,
			"mime_type" => "'" . $this->db->escape($this->mime_type) . "'",
			"size" => $this->size,
			"ext" => "'" . $ext . "'",
			"file_name" => "'" . $this->db->escape($this->file_name) . "'",
			"add_time" => "NOW()",
		);
		$this->db->insert_by_array("intmarkup_file", $data);
		$this->last_id = $this->db->get_last_id();
		$file_path = intmarkup_helper::get_file_path("user", $this->user_id, $this->last_id, $ext, true);
		if (!move_uploaded_file($this->tmp_file_name, $file_path))
		{
			return false;
		}
		if ($this->size != filesize($file_path))
		{
			return false;
		}
		return true;
	}

	public function get_data()
	{
		//file_put_contents("D:/Tmp/fu/files.txt", var_export($_FILES, true) . "\n\n", FILE_APPEND);
		//file_put_contents("D:/Tmp/fu/post.txt", var_export($_POST, true) . "\n\n", FILE_APPEND);
		$result = array(
			"error_code" => $this->error_code,
		);
		if (!$this->error_code)
		{
			$result["file_id"] = $this->last_id;
			$result["file_name"] = $this->file_name;
			$result["is_image"] = intmarkup_helper::is_image($this->file_name);
			$result["size"] = $this->size;
			$result["size_human"] = format_file_size($this->size);
		}
		return $result;
	}

}

?>