<?php

abstract class base_intmarkup_file_download_xml_ctrl extends base_xml_ctrl
{

	// To setup
	protected $object_type;
	// Internal
	protected $file_id;
	protected $file_data;
	protected $object_id;

	public function __construct($file_id)
	{
		$this->file_id = $file_id;
		parent::__construct();
	}

	public function get_xml()
	{
		if (!is_good_id($this->file_id))
		{
			$this->set_error_404();
			return false;
		}

		$this->file_data = intmarkup_helper::get_file_data($this->file_id);
		if (!$this->file_data)
		{
			$this->set_error_404();
			return false;
		}
		$object_types = is_array($this->object_type) ? $this->object_type : array($this->object_type);
		if (in_array($this->file_data["object_type"], $object_types))
		{
			$this->object_id = $this->file_data["object_id"];
			if (!$this->check_file_rights())
			{
				$this->set_error_404();
				return false;
			}
		}
		elseif ($this->file_data["object_type"] == "user")
		{
			if ($this->file_data["object_id"] != $this->user->get_user_id())
			{
				response::set_content_text("The file is inside a non-saved document.");
				return true;
			}
		}
		else
		{
			$this->set_error_404();
			return false;
		}

		$file_path = intmarkup_helper::get_file_path_from_file_data($this->file_data);
		if (!file_exists($file_path))
		{
			response::set_content_text("File was not found on the server.");
			return true;
		}

		$content_disposition = "Attachment";
		if (preg_match("#^image/|^text/|pdf#", $this->file_data["mime_type"]))
		{
			$content_disposition = "Inline";
		}

		$browser = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
		$file_name = $this->file_data["file_name"];
		if (preg_match("/MSIE/", $browser) and !preg_match("/Opera/i", $browser))
		{
			$file_name = iconv("UTF-8", "WINDOWS-1251", $file_name);
		}
		output_buffer::set_header("Content-Type", $content_disposition == "Inline" ? $this->file_data["mime_type"] : "application/octet-stream");
		output_buffer::set_header("Content-Length", filesize($file_path));
		output_buffer::set_header("Content-Disposition", "{$content_disposition}; filename=\"{$file_name}\"");
		output_buffer::set_header("Cache-Control", "must-revalidate");
		response::set_content(file_get_contents_safe($file_path));
		return true;
	}

	/**
	 * @return bool
	 */
	abstract protected function check_file_rights();
}

?>
