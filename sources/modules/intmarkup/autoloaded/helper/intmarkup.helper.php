<?php

class intmarkup_helper extends base_static_db_helper
{

	public static function get_file_path($object_type, $object_id, $file_id, $ext, $create_dir = false)
	{
		$dir = self::get_storage_dir($object_type, $object_id, $create_dir);
		$file_path = $dir . "/" . $file_id . $ext;
		return $file_path;
	}

	public static function get_file_path_from_file_data($file_data)
	{
		return self::get_file_path($file_data["object_type"], $file_data["object_id"], $file_data["id"], $file_data["ext"]);
	}

	public static function get_storage_dir($object_type, $object_id, $create = false)
	{
		$type_dir = PATH_PRIVATE_DATA . "/files/" . $object_type;
		if ($create && !is_dir($type_dir))
		{
			mkdir($type_dir, 0777);
		}
		$dir = $type_dir . "/" . $object_id;
		if ($create && !is_dir($dir))
		{
			mkdir($dir, 0777);
		}
		return $dir;
	}

	public static function get_file_data($file_id)
	{
		$file_data = self::db()->get_row("
			SELECT *
			FROM intmarkup_file
			WHERE id = {$file_id}
		", "id");
		return $file_data;
	}

	public static function object_has_files($object_type, $object_id)
	{
		$has = self::db()->row_exists("
			SELECT *
			FROM intmarkup_file
			WHERE object_type = '{$object_type}'
				AND object_id = '{$object_id}'
			LIMIT 1
		");
		return $has;
	}

	public static function get_file_list($object_type, $object_id)
	{
		$files = self::db()->fetch_all("
			SELECT *
			FROM intmarkup_file
			WHERE object_type = '{$object_type}'
				AND object_id = '{$object_id}'
			ORDER BY position, id
		", "id");
		return $files;
	}

	public static function fill_files_node(xnode $files_node, $files)
	{
		foreach ($files as $file_data)
		{
			$file_node = $files_node->create_child_node("file");
			$file_node->set_attr("id", $file_data["id"]);
			$file_node->set_attr("file_name", $file_data["file_name"]);
			$file_node->set_attr("mime_type", $file_data["mime_type"]);
			$file_node->set_attr("ext", $file_data["ext"]);
			$file_node->set_attr("is_image", self::is_image($file_data["file_name"]));
			$file_node->set_attr("size", $file_data["size"]);
			$file_node->set_attr("size_human", format_file_size($file_data["size"]));
		}
	}

	public static function get_file_list_from_array($object_type, $object_id, $file_ids)
	{
		global $user;
		/* @var $user user */
		$user_id = $user->get_user_id();

		$files = array();
		if ($file_ids)
		{
			$files_raw = self::db()->fetch_all("SELECT * FROM intmarkup_file WHERE id IN (" . join(",", $file_ids) . ")", "id");
			foreach ($file_ids as $file_id)
			{
				if (isset($files_raw[$file_id]))
				{
					$file_data = $files_raw[$file_id];
					if ($file_data["object_type"] == $object_type and $file_data["object_id"] == $object_id or
						$user_id and $file_data["object_type"] == "user" and $file_data["object_id"] == $user_id)
					{
						$files[$file_id] = $file_data;
					}
				}
			}
		}
		return $files;
	}

	public static function clean_files($object_type, $object_id, $files)
	{
		$file_ids = array();
		foreach ($files as $file_data)
		{
			$file_path = intmarkup_helper::get_file_path($object_type, $object_id, $file_data["id"], $file_data["ext"]);
			unlink_safe($file_path);
			$file_ids[] = $file_data["id"];
		}
		if (sizeof($file_ids))
		{
			self::db()->sql("DELETE FROM intmarkup_file WHERE id IN (" . join(",", $file_ids) . ")");
		}
	}

	public static function update_files($object_type, $object_id, $file_ids)
	{
		global $user;
		/* @var $user user */
		$user_id = $user->get_user_id();

		$files = self::get_file_list_from_array($object_type, $object_id, $file_ids);
		$position = 0;
		foreach ($files as $file_id => $file_data)
		{
			$new_file_data = array(
				"object_type" => "'{$object_type}'",
				"object_id" => "'{$object_id}'",
				"position" => ++$position,
			);
			self::db()->update_by_array("intmarkup_file", $new_file_data, "id = {$file_data["id"]}");
			if ($file_data["object_type"] == "user")
			{
				$old_file_path = intmarkup_helper::get_file_path("user", $user_id, $file_data["id"], $file_data["ext"]);
				$new_file_path = intmarkup_helper::get_file_path($object_type, $object_id, $file_data["id"], $file_data["ext"], true);
				rename_safe($old_file_path, $new_file_path);
			}
		}
	}

	public static function normalize_id($id)
	{
		if (is_array($id))
		{
			ksort($id);
			return join("-", $id);
		}
		else
		{
			return $id;
		}
	}

	public static function normalized_id_from_row($row, $pk_column)
	{
		if (is_array($pk_column))
		{
			sort($pk_column);
			$id = array();
			foreach ($pk_column as $key)
			{
				$id[] = $row[$key];
			}
			return join("-", $id);
		}
		else
		{
			return $row[$pk_column ? : "id"];
		}
	}
	
	public static function is_image($file_name)
	{
		return preg_match("/\.(jpe?g|gif|png)$/i", $file_name);
	}

}

?>