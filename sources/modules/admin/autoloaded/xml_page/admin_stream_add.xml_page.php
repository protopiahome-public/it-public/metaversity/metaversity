<?php

class admin_stream_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	protected $dt_name = "stream";
	protected $axis_name = "edit_admin";

	protected function modify_xml(xdom $xdom)
	{
		$streams_data = $this->db->fetch_all("SELECT id, title, title_short FROM stream ORDER BY title");
		$this->db_xml_converter->build_xml($xdom->create_child_node("streams"), $streams_data, false, "stream");
		return;
	}
	
}

?>