<?php

class admin_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "sys_params";
	protected $axis_name = "edit";
	protected $enable_blocks = false;

	public function __construct()
	{
		parent::__construct(1);
	}

}

?>