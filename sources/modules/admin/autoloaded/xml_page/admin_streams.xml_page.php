<?php

class admin_streams_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "stream";

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("stream d");
		$select_sql->add_select_fields("d.id, d.title_short, d.list_show, d.title, d.city_count_calc");
		$select_sql->add_select_fields("l.title as lang_title");
		$select_sql->add_join("LEFT JOIN lang l ON l.code = d.lang_code");
		$select_sql->add_order("d.list_position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>