<?php

class admin_cities_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "city";

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("streams", "stream_count_calc DESC", "stream_count_calc ASC");
		$processor->add_order("groups", "group_count_calc DESC", "group_count_calc ASC");
		$processor->add_order("fake", "is_fake DESC", "is_fake ASC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("city");
		$select_sql->add_select_fields("id, title, title_en, is_fake, stream_count_calc, group_count_calc");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>