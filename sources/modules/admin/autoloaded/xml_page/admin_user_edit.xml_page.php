<?php

class admin_user_edit_xml_page extends base_dt_edit_xml_ctrl
{

	protected $dependencies_settings = array(
		array(
			"ctrl" => "user_short",
			"column" => "id",
		),
	);
	// Settings
	protected $dt_name = "user";
	protected $axis_name = "edit_by_admin";
	protected $enable_blocks = true;
	
	protected function postprocess()
	{
		$streams = $this->db->fetch_column_values("
			SELECT d.id, l.status
			FROM stream d
			LEFT JOIN stream_user_link l ON l.stream_id = d.id AND l.user_id = {$this->id}
			WHERE d.list_show = 1
			ORDER BY list_position
		", "status", "id");
		
		$statuses_xdom = xdom::create("stream_statuses");
		foreach ($streams as $stream_id => $status)
		{
			$statuses_xdom->create_child_node("stream")
				->set_attr("id", $stream_id)
				->set_attr("status", $status);
			$this->xml_loader->add_xml(new stream_short_xml_ctrl($stream_id));
		}
		
		$this->xml .= $statuses_xdom->get_xml(true);
	}

}

?>