<?php

class admin_admins_xml_page extends base_easy_xml_ctrl
{

	protected $dependencies_settings = array(
		array(
			"ctrl" => "user_short",
			"column" => "user_id",
		),
	);
	protected $xml_row_name = "admin";
	protected $page;

	public function __construct($page)
	{
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 25));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id as user_id");
		$select_sql->add_from("user");
		$select_sql->add_where("is_admin = 1");
		$select_sql->add_order("last_login_time DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>