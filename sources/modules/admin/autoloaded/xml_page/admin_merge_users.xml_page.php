<?php

class admin_merge_users_xml_page extends base_xml_ctrl
{
	public function get_xml()
	{
		$xdom = xdom::create($this->name);
		
		$main_account_user_id = $this->get_user_id(GET("main_account_login"));
		$account_to_merge_user_id = $this->get_user_id(GET("account_to_merge_login"));
		if ($main_account_user_id)
		{
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $main_account_user_id);
			$xdom->set_attr("main_account_user_id", $main_account_user_id);
		}
		if ($account_to_merge_user_id)
		{
			$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $account_to_merge_user_id);
			$xdom->set_attr("account_to_merge_user_id", $account_to_merge_user_id);
		}
		if ($main_account_user_id and $account_to_merge_user_id)
		{
			$xdom->set_attr("step", 2);
		}
		else
		{
			$xdom->set_attr("step", 1);
		}
		return $xdom->get_xml(true);
	}
	
	private function get_user_id($login)
	{
		$login = trim(mb_substr($login, 0, 255));
		if (!$login)
		{
			return false;
		}
		$login_escaped = $this->db->escape($login);
		$user_id = $this->db->get_value("SELECT id FROM user WHERE login = '{$login_escaped}'");
		return $user_id ? $user_id : false;
	}

}

?>