<?php

class admin_system_news_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "system_news_item";
	// Internal
	protected $page;

	public function __construct($page)
	{
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sort_easy_processor();
		$processor->add_order("time", "add_time DESC", "add_time ASC");
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("moderators", "is_for_moderators DESC", "is_for_moderators ASC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("system_news_item");
		$select_sql->add_select_fields("id, title, is_for_moderators, add_time");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>