<?php

class admin_city_delete_xml_page extends base_delete_xml_ctrl
{

	protected $db_table = "city";

	protected function modify_xml(xdom $xdom)
	{
		$user_count = $this->db->get_value("
			SELECT COUNT(id)
			FROM user
			WHERE city_id = {$this->id}
		");
		$users = $this->db->fetch_all("
			SELECT id, login
			FROM user
			WHERE city_id = {$this->id}
			ORDER BY login
			LIMIT 10
		");
		$xdom->set_attr("user_count", $user_count);
		foreach ($users as $user)
		{
			$xdom->create_child_node("user")->set_attr("id", $user["id"])->set_attr("login", $user["login"]);
		}
	}

}

?>