<?php

// @todo remove and make like stream_admin.menu.xslt is made.
class admin_menu_xml_ctrl extends base_menu_xml_ctrl
{

	protected function get_menu()
	{
		return array(
			"general" => array(
				"title" => trans("General", "Administration"),
				"path" => "",
			),
			"news" => array(
				"title" => trans("News"),
				"path" => "news",
			),
			"cities" => array(
				"title" => trans("Cities"),
				"path" => "cities",
			),
			"streams" => array(
				"title" => trans("Streams"),
				"path" => "streams",
			),
			"users" => array(
				"title" => trans("Users"),
				"path" => "users",
			),
			"merge-users" => array(
				"title" => trans("Account merge"),
				"path" => "merge-users",
			),
			"admins" => array(
				"title" => trans("Administrators"),
				"path" => "admins",
			),
		);
	}

	protected function get_prefix()
	{
		return $this->domain_logic->get_main_prefix() . "/admin";
	}

}

?>