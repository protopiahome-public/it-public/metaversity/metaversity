<?php

class admin_streams_sort_ajax_page extends base_sort_ajax_ctrl
{

	/* Settings */
	protected $table_name = "stream";
	protected $position_column = "list_position";
	
	public function check_rights()
	{
		return $this->user->is_admin();
	}

}

?>