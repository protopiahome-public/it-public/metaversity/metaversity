<?php

class admin_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		$this->xml_loader->set_error_403_xslt("admin_403", "admin");

		if (!$this->user->is_admin())
		{
			$this->xml_loader->set_error_403();
			return;
		}

		//admin/...
		$this->xml_loader->add_xml(new admin_menu_xml_ctrl());
		if ($p[1] === null)
		{
			//admin/
			$this->xml_loader->add_xml_with_xslt(new admin_xml_page());
		}
		elseif ($p[1] === "users")
		{
			//admin/users/...
			if ($page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				//admin/users/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new admin_users_xml_page());
				$this->xml_loader->add_xml(new users_xml_page($page));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/users/<id>/
				$this->xml_loader->add_xml_with_xslt(new admin_user_edit_xml_page($p[2]));
			}
		}
		elseif ($p[1] === "admins" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//admin/admins/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new admin_admins_xml_page($page));
		}
		elseif ($p[1] === "merge-users" and $p[2] === null)
		{
			//admin/merge-users/
			$this->xml_loader->add_xml_with_xslt(new admin_merge_users_xml_page());
		}
		elseif ($p[1] === "cities")
		{
			//admin/cities/...
			if ($p[2] === null)
			{
				//admin/cities/
				$this->xml_loader->add_xml_with_xslt(new admin_cities_xml_page());
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/cities/add/
				$this->xml_loader->add_xml_with_xslt(new admin_city_add_xml_page());
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/cities/<id>/
				$this->xml_loader->add_xml_with_xslt(new admin_city_edit_xml_page($p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/cities/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new admin_city_delete_xml_page($p[2]));
			}
		}
		elseif ($p[1] === "streams")
		{
			//admin/streams/...
			if ($p[2] === null)
			{
				//admin/streams/
				$this->xml_loader->add_xml_with_xslt(new admin_streams_xml_page());
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/streams/add/
				$this->xml_loader->add_xml_with_xslt(new admin_stream_add_xml_page());
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/streams/<id>/
				$this->xml_loader->add_xml(new stream_current_xml_ctrl($p[2]));
				$this->xml_loader->add_xml_with_xslt(new admin_stream_edit_xml_page($p[2]));
			}
		}
		elseif ($p[1] === "news")
		{
			//admin/news/...
			if ($page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				//admin/news/
				$this->xml_loader->add_xml_with_xslt(new admin_system_news_xml_page($page));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/news/add/
				$this->xml_loader->add_xml_with_xslt(new admin_system_news_item_add_xml_page());
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/news/<id>/
				$this->xml_loader->add_xml_with_xslt(new admin_system_news_item_edit_xml_page($p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/news/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new admin_system_news_item_delete_xml_page($p[2]));
			}
		}
	}

}

?>