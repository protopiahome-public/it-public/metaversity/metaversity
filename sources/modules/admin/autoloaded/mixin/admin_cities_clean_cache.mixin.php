<?php

class admin_cities_clean_cache_mixin extends base_mixin
{

	public function clean_cache()
	{
		cities_cache_tag::init()->update();
		streams_check_list_cache::init()->delete();
	}

}

?>