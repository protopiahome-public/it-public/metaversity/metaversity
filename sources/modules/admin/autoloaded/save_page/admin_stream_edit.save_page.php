<?php

class admin_stream_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"stream_edit_before_start",
		"competence_set_update_title_after_stream_edit",
	);
	// Settings
	protected $dt_name = "stream";
	protected $axis_name = "edit_admin";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	public function clean_cache()
	{
		stream_cache_tag::init($this->id)->update();
		if ($this->old_db_row["name"] !== $this->updated_db_row["name"])
		{
			global $mcache;
			/* @var $mcache mcache */
			// @todo
			// Not sure it is really needed, done for extra safety
			$mcache->flush();
		}
	}

}

?>