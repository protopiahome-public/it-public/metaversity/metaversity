<?php

class admin_merge_users_save_page extends base_save_ctrl
{
	protected $COLUMNS_TO_UPDATE = array(
		array("email", "to_user_id"),
		array("email_log", "to_user_id"),
		array("user_position_log", "changer_user_id"),
		array("activity_competence_mark", "changer_user_id"),
		array("activity_competence_mark_log", "changer_user_id"),
		array("activity_participant_log", "changer_user_id"),
		array("activity_participant_mark", "changer_user_id"),
		array("activity_participant_mark_log", "changer_user_id"),
		array("competence_set", "adder_user_id"),
		array("news_item", "adder_user_id"),
		array("comment", "author_user_id"),
		array("user_foreign_statistic", "user_id"),
		array("activity_participant", "changer_user_id"),
		array("stream_admin", "adder_admin_user_id"),
		array("stream_admin", "editor_admin_user_id"),
		array("position_credit", "expert_user_id"),
		array("position_credit_log", "expert_user_id"),
		array("system_news_item", "adder_user_id"),
	);
	protected $COLUMNS_TO_INSERT_IGNORE = array(
		array("study_material_user_link", "user_id"),
		array("comment_subscription", "user_id"),
		array("activity_participant_status_calc", "user_id"),
		array("activity_participants_subscription", "user_id"),
		array("stream_admin", "user_id"),
		array("stream_user_link", "user_id"),
	);
	protected $COLUMNS_TO_PROCESS_MANUALLY = array(
		array("user_position", "user_id"),
		array("user_position_log", "user_id"),
		array("activity_participant", "user_id"),
		array("activity_participant_log", "user_id"),
		array("activity_participant_mark", "user_id"),
		array("activity_participant_mark_log", "user_id"),
		array("activity_competence_mark", "user_id"),
		array("activity_competence_mark_log", "user_id"),
		array("position_credit", "user_id"),
		array("position_credit_log", "user_id"),
	);
	protected $COLUMNS_TO_IGNORE = array(
		array("user_merge_log", "merger_user_id"),
		array("user_merge_log", "remove_user_id"),
		array("user_merge_log", "merge_into_user_id"),
	);
	protected $COLUMNS_TO_DELETE = array();

	/**
	 * @var mcache
	 */
	private $mcache;
	protected $main_user_id;
	protected $main_login;
	protected $merge_user_id;
	protected $columns;
	protected $primary_keys;

	public function __construct()
	{
		global $mcache;
		$this->mcache = $mcache;

		parent::__construct();
	}

	public function start()
	{
		$this->main_user_id = POST("main_account_user_id");
		if (!is_good_id($this->main_user_id))
		{
			return false;
		}
		if (!$row = $this->db->get_row("SELECT * FROM user WHERE id = {$this->main_user_id}"))
		{
			return false;
		}
		$this->main_login = $row["login"];

		$this->merge_user_id = POST("account_to_merge_user_id");
		if (!is_good_id($this->merge_user_id))
		{
			return false;
		}
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = {$this->merge_user_id}"))
		{
			return false;
		}

		return true;
	}

	public function check()
	{
		if ($this->main_user_id == $this->merge_user_id)
		{
			$this->pass_info->write_error("IDENTICAL_IDS");
			return false;
		}
		$this->init_column_list_for_check();
		if (($error = $this->check_db_initial()))
		{
			$this->pass_info->write_error($error);
			return false;
		}
		if (($error = $this->check_all_columns_included()))
		{
			$this->pass_info->write_error($error);
			return false;
		}
		if (($error = $this->check_insert_ignore_correctness()))
		{
			$this->pass_info->write_error($error);
			return false;
		}
		return true;
	}

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	public function commit()
	{
		foreach ($this->COLUMNS_TO_UPDATE as $data)
		{
			list($table_name, $column_name) = $data;
			$this->db->sql("
				UPDATE {$table_name}
				SET {$column_name} = {$this->main_user_id}
				WHERE {$column_name} = {$this->merge_user_id}
			");
		}
		foreach ($this->COLUMNS_TO_INSERT_IGNORE as $data)
		{
			$table_name = $data[0];
			$column_name = $data[1];
			$update_columns = isset($data[2]) ? $data[2] : array();
			$this->db->sql("
				DROP TEMPORARY TABLE IF EXISTS tmp_merge;
			");
			$this->db->sql("
				CREATE TEMPORARY TABLE tmp_merge
				SELECT *
				FROM {$table_name}
				WHERE {$column_name} = {$this->merge_user_id}
			");
			$this->db->sql("
				UPDATE tmp_merge
				SET {$column_name} = {$this->main_user_id}
			");
			foreach ($update_columns as $update_column_name => $update_column_value)
			{
				$this->db->sql("
					UPDATE tmp_merge
					SET {$update_column_name} = '{$update_column_value}'
				");
			}
			$this->db->sql("
				DELETE FROM {$table_name}
				WHERE {$column_name} = {$this->merge_user_id}
			");
			$this->db->sql("
				INSERT IGNORE INTO {$table_name}
				SELECT *
				FROM tmp_merge
			");
		}
		$this->fetch_primary_keys();
		$this->process_table_with_log("user_position", "user_position_log");
		$this->process_table_with_log("activity_participant", "activity_participant_log");
		$this->process_table_with_log("activity_participant_mark", "activity_participant_mark_log");
		$this->process_table_with_log("activity_competence_mark", "activity_competence_mark_log");
		$this->process_table_with_log("position_credit", "position_credit_log");
		foreach ($this->COLUMNS_TO_DELETE as $data)
		{
			list($table_name, $column_name) = $data;
			$this->db->sql("
				DELETE FROM {$table_name}
				WHERE {$column_name} = {$this->merge_user_id}
			");
		}
		$this->db->sql("
			DELETE FROM user
			WHERE id = {$this->merge_user_id}
		");
		$this->db->sql("
			INSERT INTO user_merge_log (merge_time, merger_user_id, remove_user_id, merge_into_user_id)
			VALUES (
				NOW(),
				{$this->user->get_user_id()},
				{$this->merge_user_id},
				{$this->main_user_id}
			)
		");
		$this->pass_info->write_info("MERGED");
		$this->pass_info->write_info("main_login", $this->main_login);
		return !$this->xerror->is_error_occured();
	}

	public function clean_cache()
	{
//		user_cache_tag::init($this->main_user_id)->update();
//		user_cache_tag::init($this->merge_user_id)->update();
//		user_positions_cache_tag::init($this->main_user_id)->update();
//		user_positions_cache_tag::init($this->merge_user_id)->update();
//		user_list_cache_tag::init()->update();
		$this->mcache->flush();
	}

	public function on_after_commit()
	{
		$this->db->commit();
		$this->exec_sql_script("activity_participants");
		$this->exec_sql_script("stream_count_calc");
		$this->exec_sql_script("resume_admin_status_after_user_merge");
		$this->exec_sql_script("auxil");
		return true;
	}

	private function exec_sql_script($script_file_name)
	{
		global $config;

		$mysql_cmd = "mysql -h {$config["db_host"]}";
		if ($config["db_user"])
		{
			$mysql_cmd .= " -u {$config["db_user"]}";
		}
		if ($config["db_pass"])
		{
			$mysql_cmd .= " -p{$config["db_pass"]}";
		}
		$this->db->sql("INSERT INTO user_merge_run_log (sql_script_name, date) VALUES ('{$script_file_name}', NOW())");
		$last_id = $this->db->get_last_id();
		$sql_file_path = PATH_DB . "/scripts/" . $script_file_name . ".sql";
		if (!file_exists($sql_file_path))
		{
			$log_message = date("r") . "\t";
			$log_message .= $this->main_user_id . "\t";
			$log_message .= $this->merge_user_id . "\t";
			$log_message .= $script_file_name . "\t";
			$log_message .= "FILE_NOT_FOUND" . "\n";
			file_put_contents(PATH_LOG . "/merge_users_sql_failed.log", $log_message, FILE_APPEND);
			$this->pass_info->write_info("SQL_UPDATE_FAILED");
			return false;
		}
		$cmd = "{$mysql_cmd} {$config["db_name"]} < {$sql_file_path} 2>&1";
		$output = null;
		$return_var = null;
		exec($cmd, $output, $return_var);
		if ($return_var != 0)
		{
			$log_message = date("r") . "\t";
			$log_message .= $this->main_user_id . "\t";
			$log_message .= $this->merge_user_id . "\t";
			$log_message .= $script_file_name . "\t";
			$log_message .= "FILE_NOT_FOUND" . "\n";
			file_put_contents(PATH_LOG . "/merge_users_sql_failed.log", $log_message, FILE_APPEND);
			$this->pass_info->write_info("MYSQL_EXEC_FAILED");
			return false;
		}
		$this->db->sql("UPDATE user_merge_run_log SET ok = 1 WHERE id = {$last_id}");
		return true;
	}

	private function fetch_primary_keys()
	{
		$this->primary_keys = array();
		$constraints = $this->db->fetch_all("
			SELECT TABLE_NAME, COLUMN_NAME
			FROM information_schema.KEY_COLUMN_USAGE
			WHERE 
				TABLE_SCHEMA = '{$this->db->get_db_name()}'
				AND REFERENCED_TABLE_SCHEMA IS NULL
				AND CONSTRAINT_NAME = 'PRIMARY'
		");
		foreach ($constraints as $data)
		{
			$table_name = $data["TABLE_NAME"];
			$column_name = $data["COLUMN_NAME"];
			if (!isset($this->primary_keys[$table_name]))
			{
				$this->primary_keys[$table_name] = array();
			}
			$this->primary_keys[$table_name][] = $column_name;
		}
	}

	public function process_table_with_log($table_name, $log_table_name, $user_id_column_name = "user_id")
	{
		if (sizeof($this->primary_keys[$table_name]) <= 1)
		{
			trigger_error("Incompatibility in logged table (code = 1)");
		}
		if ($this->primary_keys[$log_table_name] !== array("id"))
		{
			trigger_error("Incompatibility in logged table (code = 2)");
		}
		if (!in_array($user_id_column_name, $this->primary_keys[$table_name]))
		{
			trigger_error("Incompatibility in logged table (code = 3)");
		}
		// Data 1
		$data1 = $this->db->fetch_all("
			SELECT *
			FROM {$table_name}
			WHERE {$user_id_column_name} = {$this->main_user_id}
		");
		$data1_keys = array();
		foreach ($data1 as $row)
		{
			$key_parts = array();
			foreach ($this->primary_keys[$table_name] as $column_name)
			{
				if ($column_name != $user_id_column_name)
				{
					$key_parts[] = $row[$column_name];
				}
			}
			$key = join("#", $key_parts);
			$data1_keys[$key] = $row;
		}
		// Data 2
		$data2 = $this->db->fetch_all("
			SELECT *
			FROM {$table_name}
			WHERE {$user_id_column_name} = {$this->merge_user_id}
		");
		$data2_keys = array();
		foreach ($data2 as $row)
		{
			$key_parts = array();
			foreach ($this->primary_keys[$table_name] as $column_name)
			{
				if ($column_name != $user_id_column_name)
				{
					$key_parts[] = $row[$column_name];
				}
			}
			$key = join("#", $key_parts);
			$data2_keys[$key] = $row;
		}
		// Merging logic
		foreach ($data2_keys as $key => $row)
		{
			if (!isset($data1_keys[$key]))
			{
				// Escaped row
				$row_escaped = array();
				foreach ($row as $column_name => $value)
				{
					$row_escaped[$column_name] = $value === NULL ? "NULL" : "'" . $this->db->escape($value) . "'";
				}
				// Main table
				$row_to_insert = $row_escaped;
				$row_to_insert[$user_id_column_name] = $this->main_user_id;
				$this->db->insert_by_array($table_name, $row_to_insert, true);
				// Building WHERE part
				$where_sql = "";
				foreach ($this->primary_keys[$table_name] as $column_name)
				{
					if ($column_name != $user_id_column_name)
					{
						$where_sql .= " AND `{$column_name}` = {$row_escaped[$column_name]}";
					}
				}
				// Log table cleanup
				$this->db->sql("
					DELETE FROM {$log_table_name}
					WHERE {$user_id_column_name} = {$this->main_user_id}
						{$where_sql}
				");
				// Log table update
				$this->db->sql("
					UPDATE {$log_table_name}
					SET {$user_id_column_name} = {$this->main_user_id}
					WHERE {$user_id_column_name} = {$this->merge_user_id}
						{$where_sql}
				");
			}
		}
		$this->db->sql("
			DELETE FROM {$table_name}
			WHERE {$user_id_column_name} = {$this->merge_user_id}
		");
		$this->db->sql("
			DELETE FROM {$log_table_name}
			WHERE {$user_id_column_name} = {$this->merge_user_id}
		");
	}

	private function init_column_list_for_check()
	{
		$this->columns = $this->db->fetch_all("
			SELECT TABLE_NAME, COLUMN_NAME
			FROM information_schema.COLUMNS
			WHERE 
				TABLE_SCHEMA = '{$this->db->get_db_name()}'
				AND column_name LIKE '%user_id%'
		");
	}

	private function check_db_initial()
	{
		$fk_idx = array();
		foreach ($this->columns as $data)
		{
			$key = $data["TABLE_NAME"] . "#" . $data["COLUMN_NAME"];
			$fk_idx[$key] = true;
		}
		$constraints = $this->db->fetch_all("
			SELECT TABLE_NAME, COLUMN_NAME
			FROM information_schema.KEY_COLUMN_USAGE
			WHERE 
				TABLE_SCHEMA = '{$this->db->get_db_name()}'
				AND REFERENCED_TABLE_SCHEMA = '{$this->db->get_db_name()}'
				AND REFERENCED_TABLE_NAME = 'user'
				AND REFERENCED_COLUMN_NAME = 'id'
		");
		foreach ($constraints as $data)
		{
			$key = $data["TABLE_NAME"] . "#" . $data["COLUMN_NAME"];
			if (isset($fk_idx[$key]))
			{
				unset($fk_idx[$key]);
			}
			else
			{
				return "INITIAL_DB_CHECK_FAILED";
			}
		}
		return 0;
	}

	private function check_all_columns_included()
	{
		$fk_idx = array();
		foreach ($this->columns as $data)
		{
			$key = $data["TABLE_NAME"] . "#" . $data["COLUMN_NAME"];
			$fk_idx[$key] = true;
		}
		$known_columns = array_merge($this->COLUMNS_TO_UPDATE, $this->COLUMNS_TO_INSERT_IGNORE, $this->COLUMNS_TO_DELETE, $this->COLUMNS_TO_PROCESS_MANUALLY, $this->COLUMNS_TO_IGNORE);
		foreach ($known_columns as $data)
		{
			$key = $data[0] . "#" . $data[1];
			if (isset($fk_idx[$key]))
			{
				unset($fk_idx[$key]);
			}
			else
			{
				return "ALL_COLUMNS_INCLUDED_EXTRA_COLUMN_DB_CHECK_FAILED";
			}
		}
		return sizeof($fk_idx) === 0 ? 0 : "ALL_COLUMNS_INCLUDED_ALGO_OUTDATED_DB_CHECK_FAILED";
	}

	private function check_insert_ignore_correctness()
	{
		$constraints = $this->db->fetch_column_values("
			SELECT REFERENCED_TABLE_NAME
			FROM information_schema.KEY_COLUMN_USAGE
			WHERE 
				TABLE_SCHEMA = '{$this->db->get_db_name()}'
				AND REFERENCED_TABLE_SCHEMA = '{$this->db->get_db_name()}'
				AND REFERENCED_COLUMN_NAME = 'id'
		", true, "REFERENCED_TABLE_NAME");
		foreach ($this->COLUMNS_TO_INSERT_IGNORE as $data)
		{
			$table_name = $data[0];
			//$column_name = $data[1];
			if (isset($constraints[$table_name]))
			{
				return "INSERT_IGNORE_UNALLOWED_DEPENDANCE_DB_CHECK_FAILED";
			}
		}
		return 0;
	}

}

?>