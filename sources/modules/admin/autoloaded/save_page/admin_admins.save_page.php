<?php

class admin_admins_save_page extends base_admin_admins_save_ctrl
{

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	protected function get_access_save($user_id)
	{
		return new site_access_save($user_id);
	}
	
	public function clean_cache()
	{
		parent::clean_cache();
		user_stream_status_helper::update_is_moderator_anywhere_calc_status($this->user_id);
	}

}

?>