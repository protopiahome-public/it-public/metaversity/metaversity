<?php

class admin_system_news_item_edit_save_page extends base_dt_edit_save_ctrl
{

	// Settings
	protected $dt_name = "system_news_item";
	protected $axis_name = "edit";

	public function check_rights()
	{
		return $this->user->is_admin();
	}
	
	public function clean_cache()
	{
		system_news_cache_tag::init()->update();
		system_news_item_cache_tag::init($this->id)->update();
	}
	
}

?>