<?php

class admin_city_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"admin_cities_clean_cache",
	);
	// Settings
	protected $db_table = "city";

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("city.id NOT IN (SELECT city_id FROM user)");
	}

	public function on_after_commit()
	{
		stream_calc_helper::update_stream_city_count_calc();
		return true;
	}
	
}

?>