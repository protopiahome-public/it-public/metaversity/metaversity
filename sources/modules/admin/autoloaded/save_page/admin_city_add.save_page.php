<?php

class admin_city_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"admin_cities_clean_cache",
	);
	// Settings
	protected $dt_name = "city";
	protected $axis_name = "edit";

	public function check_rights()
	{
		return $this->user->is_admin();
	}

}

?>