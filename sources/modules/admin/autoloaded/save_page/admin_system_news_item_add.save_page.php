<?php

class admin_system_news_item_add_save_page extends base_dt_add_save_ctrl
{

	// Settings
	protected $dt_name = "system_news_item";
	protected $axis_name = "edit";

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	public function on_before_commit()
	{
		$this->update_array["adder_user_id"] = $this->user->get_user_id();
		return true;
	}
	
	public function clean_cache()
	{
		system_news_cache_tag::init()->update();
	}

}

?>