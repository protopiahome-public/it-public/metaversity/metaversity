<?php

class admin_user_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $dt_name = "user";
	protected $axis_name = "edit_by_admin";

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	public function clean_cache()
	{
		user_cache_tag::init($this->id)->update();
		user_list_cache_tag::init()->update();
	}
	

}

?>