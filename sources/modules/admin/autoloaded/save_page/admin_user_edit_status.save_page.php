<?php

class admin_user_edit_status_save_page extends base_admin_pretenders_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
	);
	protected $allow_nonstrict_members = true;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	protected function get_access_save($user_id)
	{
		return new stream_access_save($this->stream_obj, $user_id);
	}

}

?>