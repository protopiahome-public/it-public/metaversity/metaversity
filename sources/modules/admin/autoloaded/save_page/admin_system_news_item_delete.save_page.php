<?php

class admin_system_news_item_delete_save_page extends base_delete_save_ctrl
{

	// Settings
	protected $db_table = "system_news_item";

	public function check_rights()
	{
		return $this->user->is_admin();
	}
	
	public function clean_cache()
	{
		system_news_cache_tag::init()->update();
		system_news_item_cache_tag::init($this->id)->update();
	}

}

?>