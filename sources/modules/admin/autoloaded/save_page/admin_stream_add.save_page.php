<?php

require_once PATH_INTCMF . "/file_lock.php";

class admin_stream_add_save_page extends base_dt_add_save_ctrl
{

	protected $dt_name = "stream";
	protected $axis_name = "edit_admin";
	protected $lock;
	protected $copy_tree = false;
	protected $copy_positions = false;
	protected $copy_roles = false;
	protected $competence_set_id;
	protected $copy_from_stream_id;
	protected $copy_from_stream_row;
	protected $copy_from_competence_set_id;

	public function on_before_start()
	{
		$this->lock = new file_lock(PATH_TMP . "/stream_add.lock", file_lock::MODE_WRITE);
		return true;
	}
	
	public function on_after_start()
	{
		$this->copy_from_stream_id = POST("copy-from");
		if (is_good_id($this->copy_from_stream_id))
		{
			$this->copy_from_stream_row = $this->db->get_row("SELECT * FROM stream WHERE id = {$this->copy_from_stream_id} LOCK IN SHARE MODE");
			if (!$this->copy_from_stream_row)
			{
				return false;
			}
			$this->copy_from_competence_set_id = $this->copy_from_stream_row["competence_set_id"];
		}
		else
		{
			$this->copy_from_stream_id = null;
		}
		$this->copy_tree = POST("copy-tree") === "1";
		$this->copy_positions = POST("copy-positions") === "1";
		$this->copy_roles = POST("copy-roles") === "1";
		return true;
	}

	public function check_rights()
	{
		return $this->user->is_admin();
	}

	public function on_before_check()
	{
		if (($this->copy_positions or $this->copy_roles) and !$this->copy_tree)
		{
			$this->pass_info->write_error("COPY_TREE");
			return false;
		}
		$something_checked = $this->copy_tree || $this->copy_positions || $this->copy_roles;
		if ($something_checked && !$this->copy_from_stream_id)
		{
			$this->pass_info->write_error("COPY_FROM_NOT_SELECTED");
			return false;
		}
		elseif (!$something_checked && $this->copy_from_stream_id)
		{
			$this->pass_info->write_error("COPY_NOTHING_CHECKED");
			return false;
		}
		return true;
	}

	public function on_before_commit()
	{
		$this->add_competence_set();
		$this->update_array["competence_set_id"] = $this->competence_set_id;
		return true;
	}

	public function on_after_commit()
	{
		if ($this->copy_tree)
		{
			$this->copy_tree();
			if ($this->copy_positions)
			{
				$this->copy_positions();
			}
			if ($this->copy_roles)
			{
				$this->copy_formats();
				$this->copy_roles();
			}
		}
		if (!$this->copy_roles)
		{
			$this->create_archive_format_group();
		}
		return;
	}
	
	public function clean_cache()
	{
		streams_check_list_cache::init()->delete();
	}

	private function add_competence_set()
	{
		$data = array(
			"title" => "'" . $this->db->escape($this->updated_db_row["title"]) . "'",
			"add_time" => "NOW()",
		);
		$this->db->insert_by_array("competence_set", $data);
		$this->competence_set_id = $this->db->get_last_id();
	}

	private function copy_tree()
	{
		$data = $this->db->fetch_all("
			SELECT * 
			FROM competence_group 
			WHERE competence_set_id = {$this->copy_from_competence_set_id}
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("competence_group"));
			$this->column_set_value($data, "competence_set_id", $this->competence_set_id);
			$this->db->multi_insert("competence_group", array_keys($data[0]), $data, true);
			$this->db->sql("
				UPDATE competence_group
				SET parent_id = parent_id + {$this->get_shift("competence_group")}
				WHERE competence_set_id = {$this->competence_set_id}
			");
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM competence_full 
			WHERE competence_set_id = {$this->copy_from_competence_set_id}
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("competence_full"));
			$this->column_set_value($data, "competence_set_id", $this->competence_set_id);
			$this->db->multi_insert("competence_full", array_keys($data[0]), $data, true);
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM competence_calc
			WHERE competence_set_id = {$this->copy_from_competence_set_id}
		");
		if ($data)
		{
			$this->column_increment($data, "competence_id", $this->get_shift("competence_full")); // It's correct, NOT $this->get_shift("competence_calc")
			$this->column_set_value($data, "competence_set_id", $this->competence_set_id);
			$this->db->multi_insert("competence_calc", array_keys($data[0]), $data, true);
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM competence_link
			WHERE competence_group_id IN (
				SELECT id FROM competence_group
				WHERE competence_set_id = {$this->copy_from_competence_set_id}
			)
		");
		if ($data)
		{
			$this->column_increment($data, "competence_id", $this->get_shift("competence_full"));
			$this->column_increment($data, "competence_group_id", $this->get_shift("competence_group"));
			$this->db->multi_insert("competence_link", array_keys($data[0]), $data, true);
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM integral_competence
			WHERE competence_id IN (
				SELECT id FROM competence_full
				WHERE competence_set_id = {$this->copy_from_competence_set_id}
			)
		");
		if ($data)
		{
			$this->column_increment($data, "competence_id", $this->get_shift("competence_full"));
			$this->column_increment($data, "included_competence_id", $this->get_shift("competence_full"));
			$this->db->multi_insert("integral_competence", array_keys($data[0]), $data, true);
		}
	}

	private function copy_positions()
	{
		$data = $this->db->fetch_all("
			SELECT * 
			FROM rate
			WHERE id IN (
				SELECT rate_id FROM position
				WHERE stream_id = {$this->copy_from_stream_id}
			)
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("rate"));
			$this->column_set_value($data, "competence_set_id", $this->competence_set_id);
			$this->db->multi_insert("rate", array_keys($data[0]), $data, true);
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM rate_competence_link
			WHERE rate_id IN (
				SELECT rate_id FROM position
				WHERE stream_id = {$this->copy_from_stream_id}
			)
		");
		if ($data)
		{
			$this->column_increment($data, "rate_id", $this->get_shift("rate"));
			$this->column_increment($data, "competence_id", $this->get_shift("competence_full"));
			$this->db->multi_insert("rate_competence_link", array_keys($data[0]), $data, true);
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM position
			WHERE stream_id = {$this->copy_from_stream_id}
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("position"));
			$this->column_set_value($data, "stream_id", $this->last_id);
			$this->column_increment($data, "rate_id", $this->get_shift("rate"));
			$this->db->multi_insert("position", array_keys($data[0]), $data, true);
		}
	}
	
	private function copy_formats()
	{
		$data = $this->db->fetch_all("
			SELECT * 
			FROM format_group 
			WHERE stream_id = {$this->copy_from_stream_id}
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("format_group"));
			$this->column_set_value($data, "stream_id", $this->last_id);
			$this->db->multi_insert("format_group", array_keys($data[0]), $data, true);
			$this->db->sql("
				UPDATE format_group
				SET parent_id = parent_id + {$this->get_shift("format_group")}
				WHERE stream_id = {$this->last_id}
			");
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM format 
			WHERE stream_id = {$this->copy_from_stream_id}
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("format"));
			$this->column_set_value($data, "stream_id", $this->last_id);
			$this->column_increment($data, "format_group_id", $this->get_shift("format_group"));
			$this->db->multi_insert("format", array_keys($data[0]), $data, true);
		}
	}

	private function copy_roles()
	{
		$this->get_shift("rate", $remove_cache = true);
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM rate
			WHERE id IN (
				SELECT rate_id FROM role
				WHERE stream_id = {$this->copy_from_stream_id}
			)
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("rate"));
			$this->column_set_value($data, "competence_set_id", $this->competence_set_id);
			$this->db->multi_insert("rate", array_keys($data[0]), $data, true);
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM rate_competence_link
			WHERE rate_id IN (
				SELECT rate_id FROM role
				WHERE stream_id = {$this->copy_from_stream_id}
			)
		");
		if ($data)
		{
			$this->column_increment($data, "rate_id", $this->get_shift("rate"));
			$this->column_increment($data, "competence_id", $this->get_shift("competence_full"));
			$this->db->multi_insert("rate_competence_link", array_keys($data[0]), $data, true);
		}
		
		$data = $this->db->fetch_all("
			SELECT * 
			FROM role
			WHERE stream_id = {$this->copy_from_stream_id}
		");
		if ($data)
		{
			$this->column_increment($data, "id", $this->get_shift("role"));
			$this->column_set_value($data, "stream_id", $this->last_id);
			$this->column_increment($data, "format_id", $this->get_shift("format"));
			$this->column_increment($data, "rate_id", $this->get_shift("rate"));
			$this->db->multi_insert("role", array_keys($data[0]), $data, true);
		}
	}
	
	private function create_archive_format_group()
	{
		$this->db->sql("
			INSERT INTO format_group 
				(stream_id, title, add_time, is_archive)
			VALUES
				({$this->last_id}, 'ARCHIVE', NOW(), 1)
		");
	}

	private function get_shift($table_name, $remove_cache = false)
	{
		static $shifts = array();
		if (!isset($shifts[$table_name]) || $remove_cache)
		{
			$max_id = $this->db->get_value("SELECT MAX(id) FROM {$table_name}");
			if (!is_good_id($max_id))
			{
				trigger_error("Unexpected error");
			}
			$shifts[$table_name] = $max_id + 100;
		}
		return $shifts[$table_name];
	}

	private function column_set_value(&$data, $column_name, $new_value)
	{
		foreach ($data as &$row)
		{
			$row[$column_name] = $new_value;
		}
	}

	private function column_increment(&$data, $column_name, $delta)
	{
		foreach ($data as &$row)
		{
			$row[$column_name] += $delta;
		}
	}

}

?>