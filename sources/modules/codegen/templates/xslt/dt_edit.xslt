<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:template match="DT_NAME_edit">
		<form action="{$save_prefix}/DT_NAME_edit/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value=""/>
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
</xsl:stylesheet>
