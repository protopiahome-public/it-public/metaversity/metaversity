$dtf = new foreign_key_dtf("DTF_NAME", "DTF_TITLE", "DTF_FOREIGN_TABLE");
$dtf->set_importance(true);
$dtf->set_restrict_items_sql("
		SELECT id, title 
		FROM DTF_FOREIGN_TABLE
		ORDER BY id
	", "
		SELECT id, title 
		FROM DTF_FOREIGN_TABLE
		ORDER BY id AND id = %s
	");
$this->add_field($dtf);