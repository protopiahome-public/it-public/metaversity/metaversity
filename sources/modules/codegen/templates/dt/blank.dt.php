<?php

class DT_NAME_dt extends base_dt
{

	protected function init()
	{
		$dtf = new string_dtf("title", "Название");
		$dtf->set_importance(true);
		$this->add_field($dtf);

		$this->add_fields_in_axis("edit", array(
			"title",
		));
		$this->add_fields_in_axis("full", array(
			"title",
		));
	}

}

?>