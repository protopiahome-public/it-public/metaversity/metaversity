<?php

class codegen_dt_elements_xml_page extends base_easy_xml_ctrl
{
	protected $path;

	protected function load_data(\select_sql $select_sql = null)
	{
		$this->path = PATH_SOURCES . "/modules/codegen/templates/dt_elements";
		$files = scandir($this->path);
		foreach ($files as $file)
		{
			if (preg_match("#^dtf_(.*).tpl#", $file, $matches))
			{
				$this->data["dtf"][$matches[1]] = file_get_contents($this->path . "/" . $file);
			}
			elseif ($file == "axis.tpl")
			{
				$this->data["axis"] = file_get_contents($this->path . "/" . $file);
			}
		}
	}

	protected function fill_xml(\xdom $xdom)
	{
		foreach ($this->data["dtf"] as $dtf => $code)
		{
			$xdom->create_child_node("dtf", $code)->set_attr("dtf", $dtf);
		}
		$xdom->create_child_node("axis", $this->data["axis"]);
	}

}

?>