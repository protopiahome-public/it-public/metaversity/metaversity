<?php

class codegen_easy_save_page extends base_codegen_save_ctrl
{

	public function commit()
	{
		$ctrl_name = REQUEST("ctrl_name");
		$module_name = REQUEST("module_name");
		if (!preg_match("/^[a-z0-9_]+$/", $ctrl_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		if (!preg_match("/^[a-z0-9_]+$/", $module_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		$with_xslt = REQUEST("with_xslt");
		$with_save_page = REQUEST("with_save_page");

		$this->process_file(self::TYPE_XML_PAGE, "easy", array("CTRL_NAME" => $ctrl_name), $module_name, $ctrl_name);
		if ($with_xslt)
		{
			$this->process_file(self::TYPE_XSLT, "blank", array("CTRL_NAME" => $ctrl_name), $module_name, $ctrl_name);
		}
		if ($with_save_page)
		{
			$this->process_file(self::TYPE_SAVE_PAGE, "blank", array("CTRL_NAME" => $ctrl_name), $module_name, $ctrl_name);
		}

		$this->pass_info->write_info("SUCCESS");
		return true;
	}

}

?>