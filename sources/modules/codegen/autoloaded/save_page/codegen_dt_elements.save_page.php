<?php

class codegen_dt_elements_save_page extends base_codegen_save_ctrl
{

	public function commit()
	{
		$dt_name = REQUEST("dt_name");
		$module_name = REQUEST("module_name");
		$elements = REQUEST("elements");
		if (!preg_match("/^[a-z0-9_]+$/", $dt_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		if (!preg_match("/^[a-z0-9_]+$/", $module_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}

		$this->process_file(self::TYPE_DT, "elements", array("DT_NAME" => $dt_name, "ELEMENTS" => $elements), $module_name, $dt_name);

		$this->pass_info->write_info("SUCCESS");
		return true;
	}

}

?>