<?php

class codegen_dt_show_save_page extends base_codegen_save_ctrl
{

	public function commit()
	{
		$dt_name = REQUEST("dt_name");
		$module_name = REQUEST("module_name");
		$axis_name = REQUEST("axis_name");
		if (!preg_match("/^[a-z0-9_]+$/", $dt_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		if (!preg_match("/^[a-z0-9_]+$/", $module_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		if (!preg_match("/^[a-z0-9_]+$/", $axis_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}

		$this->process_file(self::TYPE_XML_CTRL, "dt_show", array("DT_NAME" => $dt_name, "AXIS_NAME" => $axis_name), $module_name, "{$dt_name}_{$axis_name}");

		$this->pass_info->write_info("SUCCESS");
		return true;
	}

}

?>