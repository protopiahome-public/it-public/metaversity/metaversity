<?php

class codegen_dt_save_page extends base_codegen_save_ctrl
{

	public function commit()
	{
		$dt_name = REQUEST("dt_name");
		$module_name = REQUEST("module_name");
		$axis_add = REQUEST("axis_add");
		$axis_edit = REQUEST("axis_edit");
		if (!preg_match("/^[a-z0-9_]+$/", $dt_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		if (!preg_match("/^[a-z0-9_]+$/", $module_name))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		if (!preg_match("/^[a-z0-9_]+$/", $axis_add))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}
		if (!preg_match("/^[a-z0-9_]+$/", $axis_edit))
		{
			$this->pass_info->write_error("WRONG_INPUT");
			return false;
		}

		$filename_add = "{$dt_name}_add";
		$filename_edit = "{$dt_name}_edit";
		$this->process_file(self::TYPE_DT, "blank", array("DT_NAME" => $dt_name), $module_name, $dt_name);
		$this->process_file(self::TYPE_XML_PAGE, "dt_add", array("DT_NAME" => $dt_name, "AXIS_ADD" => $axis_add), $module_name, $filename_add);
		$this->process_file(self::TYPE_XSLT, "dt_add", array("DT_NAME" => $dt_name, "AXIS_ADD" => $axis_add), $module_name, $filename_add);
		$this->process_file(self::TYPE_SAVE_PAGE, "dt_add", array("DT_NAME" => $dt_name, "AXIS_ADD" => $axis_add), $module_name, $filename_add);
		$this->process_file(self::TYPE_XML_PAGE, "dt_edit", array("DT_NAME" => $dt_name, "AXIS_EDIT" => $axis_edit), $module_name, $filename_edit);
		$this->process_file(self::TYPE_XSLT, "dt_edit", array("DT_NAME" => $dt_name, "AXIS_EDIT" => $axis_edit), $module_name, $filename_edit);
		$this->process_file(self::TYPE_SAVE_PAGE, "dt_edit", array("DT_NAME" => $dt_name, "AXIS_EDIT" => $axis_edit), $module_name, $filename_edit);

		$this->pass_info->write_info("SUCCESS");
		return true;
	}

}

?>