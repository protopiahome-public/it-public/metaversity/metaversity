<?php

final class codegen_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		if ($p[1] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new codegen_xml_page());
		}
		elseif ($p[1] === "blank" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new codegen_blank_xml_page());
		}
		elseif ($p[1] === "dt" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new codegen_dt_xml_page());
		}
		elseif ($p[1] === "dt-elements" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new codegen_dt_elements_xml_page());
		}
		elseif ($p[1] === "dt-show" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new codegen_dt_show_xml_page());
		}
		elseif ($p[1] === "easy" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new codegen_easy_xml_page());
		}
	}

}

?>