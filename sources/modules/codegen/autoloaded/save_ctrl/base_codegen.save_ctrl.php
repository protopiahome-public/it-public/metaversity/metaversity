<?php

abstract class base_codegen_save_ctrl extends base_save_ctrl
{
	const TYPE_SAVE_PAGE = "save_page";
	const TYPE_XML_PAGE = "xml_page";
	const TYPE_XML_CTRL = "xml_ctrl";
	const TYPE_XSLT = "xslt";
	const TYPE_DT = "dt";

	public function check_rights()
	{
		return $this->xerror->is_debug_mode();
	}

	protected function process_file($type, $template_name, $args, $module_name, $target_file_name)
	{
		$template_file = PATH_MODULES . "/codegen/templates/{$type}/{$template_name}.{$type}";
		if ($type != self::TYPE_XSLT)
		{
			$template_file .= ".php";
		}
		$template = file_get_contents($template_file);
		foreach ($args as $name => $value)
		{
			$template = str_replace($name, $value, $template);
		}
		if ($type == self::TYPE_XSLT)
		{
			$filename = PATH_XSLT . "/{$module_name}/{$target_file_name}.xslt";
		}
		else
		{
			$filename = PATH_MODULES . "/{$module_name}/autoloaded/{$type}/{$target_file_name}.{$type}.php";
		}
		if (file_exists($filename))
		{
			$this->pass_info->write_error("FILE_ALREADY_EXISTS", $filename);
			return;
		}
		file_put_contents($filename, $template);
		$this->pass_info->write_info("FILE_CREATED", $filename);
		//echo $filename . "<br/>";
	}

}

?>