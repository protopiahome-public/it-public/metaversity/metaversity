<?php

require_once PATH_MODULES . "/stream_math/lib/all_competence_results_loader.php";

class stream_competence_results_loader extends all_competence_results_loader
{

	// Internal
	protected $stream_id;

	public function __construct($competence_set_id, $user_id = null, $short = false, $stream_id = null)
	{
		parent::__construct($competence_set_id, $user_id, $short);
		$this->stream_id = $stream_id;
	}

	protected function get_all_users_ids()
	{
		return $this->db->fetch_column_values("SELECT user_id FROM stream_user_link WHERE stream_id = {$this->stream_id}");
	}

}

?>