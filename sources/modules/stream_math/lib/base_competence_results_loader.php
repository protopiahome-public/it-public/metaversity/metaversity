<?php

require_once PATH_INTCMF . "/file_lock.php";

abstract class base_competence_results_loader
{

	// Settings
	protected $cache_name = null; // null means no cache; otherwise it must be a string /^[a-zA-Z0-9_]+$/ which is unique for the whole project
	// Internal
	/**
	 * @var db
	 */
	protected $db;
	protected $competence_set_id;
	protected $user_id;
	protected $short = false;
	protected $math_competence_results = null;
	protected $math_competence_results_short = null;
	protected $fetch_users_ids = null;

	/**
	 * @param $user_id is null to fetch all users
	 */
	public function __construct($competence_set_id, $user_id = null, $short = false)
	{
		global $db;
		$this->db = $db;

		$this->competence_set_id = $competence_set_id;
		$this->user_id = $user_id;
		$this->short = $short;
	}

	abstract protected function get_all_users_ids();

	/**
	 * array(
	 *   <user_id>, <type>, <stream_id>, <competence_set_id>, <activity_id>, <weight>, <role_id>, <competence_id>, <mark>, <rater_user_id>, <is_important>
	 * )
	 * MUST be sorted by mark change time DESC
	 */
	abstract protected function fetch_marks_raw();

	public function get_competence_results()
	{
		if ($this->short)
		{
			if (is_null($this->math_competence_results_short))
			{
				$this->calc_competence_results();
			}
			return $this->math_competence_results_short;
		}
		else
		{
			if (is_null($this->math_competence_results))
			{
				$this->calc_competence_results();
			}
			return $this->math_competence_results;
		}
	}

	protected function calc_competence_results()
	{
		if ($this->cache_name)
		{
			$file_lock = new file_lock(PATH_TMP . "/competence_results_{$this->cache_name}.lock", file_lock::MODE_WRITE);
			if (!$file_lock->is_ok())
			{
				trigger_error("Cannot acquire lock '{$this->cache_name}'");
				return false;
			}
		}
		$this->math_competence_results = array();
		$this->math_competence_results_short = array();
		if ($this->cache_name)
		{
			$all_users_ids = $this->user_id ? array($this->user_id) : $this->get_all_users_ids();
			$this->fetch_users_ids = array();
			foreach ($all_users_ids as $user_id)
			{
				$skip = false;
				if ($this->short)
				{
					$res = math_user_competence_results_cache::init($this->cache_name, $user_id, true)->get();
					if ($res and isset($res["sets"][$this->competence_set_id]))
					{
						$this->math_competence_results_short[$user_id] = $res;
						$skip = true;
					}
				}
				if (!$skip)
				{
					$res = math_user_competence_results_cache::init($this->cache_name, $user_id, false)->get();
					if ($res)
					{
						$this->math_competence_results[$user_id] = $res;
					}
					else
					{
						$this->fetch_users_ids[] = $user_id;
					}
				}
			}
		}
		else
		{
			$this->fetch_users_ids = $this->user_id ? array($this->user_id) : null;
		}
		if (!is_array($this->fetch_users_ids) || sizeof($this->fetch_users_ids))
		{
			$math_marks_raw = $this->fetch_marks_raw();
			$math_competence_results = math_competence_results_helper::init_competence_results($math_marks_raw);
			array_merge_good($this->math_competence_results, $math_competence_results);
		}
		math_competence_results_helper::prepare_competence_results($this->math_competence_results, $this->competence_set_id);
		if ($this->cache_name)
		{
			$this->store();
			$file_lock->close();
		}
	}

	private function store()
	{
		foreach ($this->math_competence_results as $user_id => $user_competence_results)
		{
			$user_competence_results_short = null;
			if ($this->short)
			{
				$user_competence_results_short = $this->get_short_results($user_competence_results);
				$this->math_competence_results_short[$user_id] = $user_competence_results_short;
			}
			if (!isset($user_competence_results["store"]))
			{
				continue;
			}
			unset($user_competence_results["store"]);
			$stream_ids = array();
			foreach ($user_competence_results["marks"] as $mark_data)
			{
				$type = $mark_data[0];
				if ("role" === $type)
				{
					$stream_id = $mark_data[1];
					$stream_ids[$stream_id] = true;
				}
			}
			$stream_ids = array_keys($stream_ids);
			$ready_competence_set_ids = array();
			foreach ($user_competence_results["sets"] as $competence_set_id => $competence_set_data)
			{
				if (isset($competence_set_data["ready"]))
				{
					$ready_competence_set_ids[] = $competence_set_id;
				}
			}
			math_user_competence_results_cache::init($this->cache_name, $user_id, false, $stream_ids, $ready_competence_set_ids)->set($user_competence_results);
			if (is_null($user_competence_results_short))
			{
				$user_competence_results_short = $this->get_short_results($user_competence_results);
			}
			math_user_competence_results_cache::init($this->cache_name, $user_id, true, $stream_ids, $ready_competence_set_ids)->set($user_competence_results_short);
		}
	}

	private function get_short_results($user_competence_results)
	{
		$result = array(
			"sets" => array(),
		);
		foreach ($user_competence_results["sets"] as $competence_set_id => $competence_set_data)
		{
			if (isset($competence_set_data["ready"]))
			{
				$result["sets"][$competence_set_id] = array(
					"marks" => array(),
				);
				foreach ($competence_set_data["marks"] as $competence_id => $competence_data)
				{
					if (isset($competence_data["result"]))
					{
						$result["sets"][$competence_set_id]["marks"][$competence_id] = $competence_data["result"];
					}
				}
			}
		}
		return $result;
	}

}

?>