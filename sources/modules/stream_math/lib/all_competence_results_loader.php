<?php

require_once PATH_MODULES . "/stream_math/lib/base_competence_results_loader.php";

/**
 * @todo rater_user_id --> changer_user_id
 */
class all_competence_results_loader extends base_competence_results_loader
{

	// Settings
	protected $cache_name = "all";

	protected function fetch_marks_raw()
	{
		if (is_array($this->fetch_users_ids) and ! sizeof($this->fetch_users_ids))
		{
			return array();
		}
		$result = array();
		// Marks for roles
		$user_where = is_null($this->fetch_users_ids) ? "" : " AND apm.user_id IN (" . join(", ", $this->fetch_users_ids) . ")";
		$db_result = $this->db->sql("
			SELECT
				apm.user_id, 
				'" . math_competence_results_helper::TYPE_ROLE . "', 
				a.stream_id, 
				c.competence_set_id, 
				apm.activity_id, 
				a.weight, 
				apm.role_id, 
				rcl.competence_id, 
				apm.mark, 
				apm.changer_user_id AS rater_user_id, 
				rcl.mark = 2 AS is_important,
				apm.change_time AS time
			FROM activity_participant_mark apm
			JOIN activity a ON a.id = apm.activity_id
			JOIN stream d ON d.id = a.stream_id
			JOIN role r ON r.id = apm.role_id
			JOIN rate ON rate.id = r.rate_id
			JOIN rate_competence_link rcl ON rcl.rate_id = rate.id
			JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE c.competence_set_id = d.competence_set_id
			{$user_where}
		");
		while (($row = $this->db->fetch_row($db_result)))
		{
			$result[] = $row;
		}
		// Competence marks
		$user_where = is_null($this->fetch_users_ids) ? "" : " WHERE acm.user_id IN (" . join(", ", $this->fetch_users_ids) . ")";
		$db_result = $this->db->sql("
			SELECT
				acm.user_id, 
				'" . math_competence_results_helper::TYPE_COMPETENCE . "', 
				a.stream_id, 
				c.competence_set_id, 
				acm.activity_id, 
				a.weight, 
				null, /* role_id */ 
				acm.competence_id, 
				acm.mark, 
				acm.changer_user_id AS rater_user_id, 
				1 AS is_important,
				acm.change_time AS time
			FROM activity_competence_mark acm
			JOIN activity a ON a.id = acm.activity_id
			JOIN competence_calc c ON c.competence_id = acm.competence_id
			{$user_where}
		");
		while (($row = $this->db->fetch_row($db_result)))
		{
			$result[] = $row;
		}

		// Works faster without conversion to ints
//		foreach ($result as &$row)
//		{
//			$row[11] = $this->db->parse_datetime($row[11]);
//		}
		$result = array_sort($result, 11, SORT_DESC);

		return $result;
	}

	protected function get_all_users_ids()
	{
		return $this->db->fetch_column_values("SELECT id FROM user");
	}

}

?>