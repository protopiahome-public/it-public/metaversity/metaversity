<?php

require_once PATH_MODULES . "/stream_math/lib/stream_competence_results_loader.php";

class math_rating_cache_helper extends base_static_db_helper
{

	const CACHE_LIFE_TIME = 900; // seconds

	public static function get_cache($stream_id)
	{
		return math_ratings_cache::init($stream_id)->get();
	}

	public static function build_cache($stream_id, $force = false)
	{
		if (!$force and self::get_cache($stream_id))
		{
			return;
		}

		$stream_obj = stream_obj::instance($stream_id);
		$math_positions = math_positions_helper::fetch_stream_positions_data($stream_id, $stream_obj->get_competence_set_id());

		$loader = new stream_competence_results_loader($stream_obj->get_competence_set_id(), null, false, $stream_id);
		$math_competence_results = $loader->get_competence_results();

		$math_position_results = math_position_results_helper::get_position_results($math_positions, $math_competence_results, $stream_obj->get_competence_set_id());

		$result = array(
			"ratings" => $math_position_results,
			"time" => time(),
		);
		math_ratings_cache::init($stream_id)->set($result, self::CACHE_LIFE_TIME);
		return $result;
	}

	public static function check_and_request_build($stream_id)
	{
		$ratings = math_rating_cache_helper::get_cache($stream_id);
//		if (!$ratings)
//		{
//			$ratings = math_rating_cache_helper::build_cache($this->stream_id);
//			return $ratings;
//		}
		if ($ratings)
		{
			return $ratings;
		}
		global $config;
		if ($config["beanstalk_host"])
		{
			require_once PATH_INTCMF . "/beanstalk.php";
			$beanstalk = new beanstalk($config["beanstalk_host"], $config["beanstalk_port"]);
			$beanstalk->connect();
			$beanstalk->use_tube("mv-ratings");
			$beanstalk->put(0, 0, 600, $stream_id);
		}
		return false;
	}

}

?>