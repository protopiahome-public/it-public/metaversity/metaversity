<?php

class math_position_results_helper
{

	public static function get_position_results($math_positions, $math_competence_results, $competence_set_id)
	{
		$result = array();
		foreach ($math_competence_results as $user_id => $user_competence_results)
		{
			$math_plain_competence_results = $user_competence_results["sets"][$competence_set_id]["marks"];
			$result[$user_id] = array();
			foreach ($math_positions as $position_id => $math_position)
			{
				$result[$user_id][$position_id] = self::get_position_match_percent($math_position, $math_plain_competence_results);
			}
			//$result[$user_id] = array_sort($result[$user_id], "match", SORT_DESC);
		}
		return $result;
	}

	public static function get_position_match_percent($math_position, $math_plain_competence_results, $round = true)
	{
		$num = 0;
		$den = 0;
		foreach ($math_position as $competence_id => $mark)
		{
			if (isset($math_plain_competence_results[$competence_id]) and ! is_array($math_plain_competence_results[$competence_id]))
			{
				$user_mark = $math_plain_competence_results[$competence_id];
			}
			elseif (isset($math_plain_competence_results[$competence_id]["result"]))
			{
				$user_mark = $math_plain_competence_results[$competence_id]["result"];
			}
			else
			{
				$user_mark = 0;
			}
			$num += $user_mark <= $mark ? $user_mark : $mark;
			$den += $mark;
		}
		$result = $den == 0 ? 0 : ($round ? (int) round($num * 100 / $den) : $num * 100 / $den);
		return $result > 100 ? 100 : $result;
	}

}

?>