<?php

class math_positions_helper extends base_static_db_helper
{

	public static function fetch_stream_position_data($position_id, $stream_id, $competence_set_id)
	{
		$competences = self::db()->fetch_column_values("
			SELECT rcl.competence_id, rcl.mark
			FROM position p
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = p.rate_id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE p.id = {$position_id} AND p.stream_id = {$stream_id}
				AND c.competence_set_id = {$competence_set_id}
		", "mark", "competence_id");
		$result = array($position_id => $competences);
		return $result;
	}

	public static function fetch_stream_positions_data($stream_id, $competence_set_id)
	{
		$result = math_stream_positions_cache::init($stream_id, $competence_set_id)->get();
		if (is_array($result))
		{
			return $result;
		}
		$result = self::db()->fetch_column_values("
			SELECT id 
			FROM position
			WHERE stream_id = {$stream_id}
		", array(), "id");
		$competences = self::db()->fetch_all("
			SELECT p.id, rcl.competence_id, rcl.mark
			FROM position p
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = p.rate_id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE p.stream_id = {$stream_id}
				AND c.competence_set_id = {$competence_set_id}
			ORDER BY p.id
		");
		foreach ($competences as $row)
		{
			$id = (int) $row["id"];
			if (isset($result[$id]))
			{
				$competence_id = (int) $row["competence_id"];
				$mark = (int) $row["mark"];
				$result[$id][$competence_id] = $mark;
			}
		}
		math_stream_positions_cache::init($stream_id, $competence_set_id)->set($result);
		return $result;
	}

}

?>