<?php

class math_test_helper extends base_static_db_helper
{

	public static function clean()
	{
		self::db()->sql("UPDATE sys_params SET central_competence_set_id = 1");
		sys_params_helper::clear_cache();
		
		self::db()->sql("TRUNCATE TABLE competence_translation_direct");
		self::db()->sql("TRUNCATE TABLE competence_translation_formulae");
		self::db()->sql("TRUNCATE TABLE competence_translator");
		self::db()->sql("TRUNCATE TABLE integral_competence");
		
	}

}

?>