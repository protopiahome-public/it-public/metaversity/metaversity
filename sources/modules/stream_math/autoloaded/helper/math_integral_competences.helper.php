<?php

class math_integral_competences_helper
{

	const INTEGRAL_MARK_LIMIT = 2.0;

	public static function calc_integral_competences(&$math_competence_results, $competence_set_id)
	{
		$integral_data = integral_competences_helper::get_all_competences_in_calc_order($competence_set_id);
		foreach ($math_competence_results as &$user_competence_results)
		{
			if (isset($user_competence_results["sets"][$competence_set_id]["ready"]))
			{
				continue;
			}
			if (!isset($user_competence_results["sets"][$competence_set_id]["marks"]))
			{
				continue;
			}
			$math_plain_competence_results = &$user_competence_results["sets"][$competence_set_id]["marks"];
			foreach ($integral_data as $competence_id => $included_competences_index)
			{
				if ($included_competences_index === true)
				{
					continue;
				}
				$percent = math_position_results_helper::get_position_match_percent($included_competences_index, $math_plain_competence_results, false);
				$competence_result = self::get_competence_result_from_match_percent($percent);
				if ($competence_result > 0)
				{
					if (!isset($math_plain_competence_results[$competence_id]["result"]) or $math_plain_competence_results[$competence_id]["result"] < $competence_result)
					{
//						$marks_competence_ids = array($competence_id => false);
//						foreach ($included_competences_index as $included_competence_id => $tmp)
//						{
//							$marks_competence_ids[$included_competence_id] = true;
//						}
//						$marks_n = array();
//						foreach ($marks_competence_ids as $marks_competence_id => $mark_marks_imported)
//						{
//							if (isset($math_plain_competence_results[$marks_competence_id]["marks"]))
//							{
//								if ($mark_marks_imported)
//								{
//									$marks_n[] = math_competence_results_helper::mark_marks_imported($math_plain_competence_results[$marks_competence_id]["marks"]);
//								}
//								else
//								{
//									$marks_n[] = $math_plain_competence_results[$marks_competence_id]["marks"];
//								}
//							}
//						}
						array_key_set($math_plain_competence_results, $competence_id, array(
							"marks" => array(),
						));
						$math_plain_competence_results[$competence_id]["result"] = $competence_result;
						$math_plain_competence_results[$competence_id]["result_type"] = math_competence_results_helper::RESULT_TYPE_INTEGRAL;
					}
				}
			}
			unset($math_plain_competence_results);
		}
		unset($user_competence_results);
	}

	public static function get_competence_result_from_match_percent($percent)
	{
		return min((float) $percent / 100 * 3, self::INTEGRAL_MARK_LIMIT);
	}

}

?>