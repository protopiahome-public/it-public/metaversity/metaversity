<?php

class math_marks_helper extends base_static_db_helper
{

	public static function get_mark_key($type, $activity_id, $target_id)
	{
		return $activity_id . "-" . $type . $target_id;
	}

	public static function get_mark_key_selective($type, $activity_id, $role_id, $competence_id)
	{
		return $activity_id . "-" . $type . ($type === math_competence_results_helper::TYPE_ROLE ? $role_id : $competence_id);
	}

	public static function copy_prepared_competence_data(&$data, $competence_data = null)
	{
		if ($competence_data)
		{
			$data["result"] = math_competence_results_helper::results_number_format($competence_data["result"]);
			$data["result_type"] = isset($competence_data["result_type"]) ? $competence_data["result_type"] : math_competence_results_helper::RESULT_TYPE_DEFAULT;
			$data["mark_count"] = sizeof($competence_data["marks"]);
			$data["rater_count"] = $competence_data["rater_count"];
			$data["show_link"] = self::show_link($competence_data);
		}
		else
		{
			$data["result"] = math_competence_results_helper::results_number_format(0.0);
			$data["show_link"] = false;
		}
	}

	public static function show_link($competence_data)
	{
		if (!$competence_data)
		{
			return false;
		}
		return $competence_data["result"] > 0 || sizeof($competence_data["marks"]);
	}

}

?>