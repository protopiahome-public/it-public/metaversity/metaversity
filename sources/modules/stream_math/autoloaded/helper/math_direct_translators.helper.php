<?php

class math_direct_translators_helper extends base_static_db_helper
{

	public static function apply_direct_translators(&$math_competence_results, $competence_set_id)
	{
		$direct_translators_data = self::fetch_direct_translators_data($competence_set_id);
		foreach ($math_competence_results as &$user_competence_results)
		{
			if (isset($user_competence_results["sets"][$competence_set_id]["ready"]))
			{
				continue;
			}
			array_key_set($user_competence_results["sets"], $competence_set_id, array(
				"marks" => array(),
			));
			$math_plain_competence_results = &$user_competence_results["sets"][$competence_set_id]["marks"];
			foreach ($direct_translators_data as $competence_id => $competence_data_n)
			{
				$marks_n = array();
				$change = false;
				foreach ($competence_data_n as $competence_data)
				{
					list($eq_competence_set_id, $eq_competence_id) = $competence_data;
					if (isset($user_competence_results["sets"][$eq_competence_set_id]["marks"][$eq_competence_id]["marks"]))
					{
						$marks_to_merge = math_competence_results_helper::remove_imported_marks($user_competence_results["sets"][$eq_competence_set_id]["marks"][$eq_competence_id]["marks"]);
						if (sizeof($marks_to_merge))
						{
							$marks_n[] = $marks_to_merge;
							$change = true;
						}
					}
				}
				if ($change)
				{
					if (isset($math_plain_competence_results[$competence_id]["marks"]))
					{
						$marks_n[] = $math_plain_competence_results[$competence_id]["marks"];
					}
					$math_plain_competence_results[$competence_id]["marks"] = math_competence_results_helper::merge_marks($marks_n);
				}
			}
			unset($math_plain_competence_results);
		}
		unset($user_competence_results);
	}

	public static function get_direct_translators_data_grouped_by_competence_set_cached($competence_set_id, $eq_competence_set_id = null)
	{
		static $cache = array();
		if (!isset($cache[$competence_set_id]))
		{
			$cache[$competence_set_id] = self::get_direct_translators_data_grouped_by_competence_set($competence_set_id);
		}
		$result = $cache[$competence_set_id];
		if ($eq_competence_set_id)
		{
			return isset($result[$eq_competence_set_id]) ? $result[$eq_competence_set_id] : array();
		}
		else
		{
			return $result;
		}
	}

	public static function get_translator_via_central($competence_set_id, $eq_competence_set_id)
	{
		$central_competence_set_id = sys_params_helper::get_central_competence_set_id();
		$translator1 = self::get_direct_translators_data_grouped_by_competence_set_cached($competence_set_id, $central_competence_set_id);
		$translator2 = self::get_direct_translators_data_grouped_by_competence_set_cached($central_competence_set_id, $eq_competence_set_id);
		$result = array();
		foreach ($translator1 as $competence_id => $eq_competence_ids)
		{
			foreach ($eq_competence_ids as $eq_competence_id)
			{
				if (isset($translator2[$eq_competence_id]))
				{
					array_key_set($result, $competence_id, array());
					foreach ($translator2[$eq_competence_id] as $eq_competence_id_2)
					{
						$result[$competence_id][$eq_competence_id_2] = true;
					}
				}
			}
		}
		foreach ($result as $competence_id => &$eq_competence_ids)
		{
			$result[$competence_id] = array_keys($eq_competence_ids);
		}
		unset($eq_competence_ids);
		return $result;
	}

	public static function get_direct_translators_data_grouped_by_competence_set($competence_set_id)
	{
		$direct_translators_data = self::fetch_direct_translators_data($competence_set_id);
		$result = array();
		foreach ($direct_translators_data as $competence_id => $data)
		{
			foreach ($data as $eq_data)
			{
				list($eq_competence_set_id, $eq_competence_id) = $eq_data;
				array_key_set($result, $eq_competence_set_id);
				array_key_set($result[$eq_competence_set_id], $competence_id);
				$result[$eq_competence_set_id][$competence_id][] = (int) $eq_competence_id;
			}
		}
		return $result;
	}

	private static function fetch_direct_translators_data($competence_set_id)
	{
		$result = competence_direct_translators_cache::init($competence_set_id)->get();
		if (is_array($result))
		{
			return $result;
		}
		$data = self::db()->fetch_all("
			SELECT ctd.competence_id, ctd.eq_competence_set_id, ctd.eq_competence_id
			FROM competence_translation_direct ctd
			JOIN competence_calc c1 ON c1.competence_id = ctd.competence_id
			JOIN competence_calc c2 ON c2.competence_id = ctd.eq_competence_id
			WHERE ctd.competence_set_id = {$competence_set_id}
		");
		$result = array();
		foreach ($data as $row)
		{
			array_key_set($result, $row["competence_id"]);
			$result[$row["competence_id"]][] = array($row["eq_competence_set_id"], $row["eq_competence_id"]);
		}
		competence_direct_translators_cache::init($competence_set_id)->set($result);
		return $result;
	}

}

?>