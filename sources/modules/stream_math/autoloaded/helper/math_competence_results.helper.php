<?php

/*
 * $math_competence_results = array(
 *   <user_id> => array(
 *     "marks" => array(
 *       // sorted by mark time DESC
 *       <mark_idx> => array("role"|"competence", <stream_id>, <competence_set_id>, <activity_id>, <weight>, <role_id>|<competence_id>, <mark>, <rater_user_id>),
 *     ),
 *     "sets" => array(
 *       <competence_set_id> => array(
 *         "marks" => array( // $math_plain_competence_results = array(
 *           <competence_id> => array(
 *             "marks" => array(
 *               // any order
 *               array(<mark_idx>, $is_important=<boolean>, $is_imported=<boolean>),
 *             ),
 *             "result" => <float>,
 *             ["result_type" => "integral"|"formulae",]
 *             "rater_count" => <int>,
 *           ),
 *         ),
 *         ["ready" => true,]
 *       ),
 *     ),
 *   ),
 * );
 */

class math_competence_results_helper
{

	const TYPE_ROLE = "role";
	const TYPE_COMPETENCE = "competence";
	const RESULT_TYPE_DEFAULT = "default";
	const RESULT_TYPE_INTEGRAL = "integral";
	const RESULT_TYPE_FORMULAE = "formulae";
	const PROVE_MARK_COUNT = 5;
	const PROVE_ROLE_COUNT = 3;
	const MAX_MARK = 3;

	// @todo test
	public static function init_competence_results($math_marks_raw)
	{
		$result = array();
		$index = array();
		foreach ($math_marks_raw as $row)
		{
			list($user_id, $type, $stream_id, $competence_set_id, $activity_id, $weight, $role_id, $competence_id, $mark, $rater_user_id, $is_important) = $row;
			array_key_set($result, $user_id, array(
				"marks" => array(),
				"sets" => array(),
				"store" => true,
			));
			array_key_set($result[$user_id]["sets"], $competence_set_id, array(
				"marks" => array(),
			));
			array_key_set($result[$user_id]["sets"][$competence_set_id]["marks"], $competence_id, array(
				"marks" => array(),
			));
			array_key_set($index, $user_id);
			$mark_key = math_marks_helper::get_mark_key_selective($type, $activity_id, $role_id, $competence_id);
			if (isset($index[$user_id][$mark_key]))
			{
				// mark exists
				$mark_idx = $index[$user_id][$mark_key];
			}
			else
			{
				$mark_idx = sizeof($result[$user_id]["marks"]);
				$index[$user_id][$mark_key] = $mark_idx;
				$result[$user_id]["marks"][] = array($type, $stream_id, $competence_set_id, $activity_id, $weight, $role_id ? $role_id : $competence_id, $mark, $rater_user_id);
			}
			$is_imported = false;
			$result[$user_id]["sets"][$competence_set_id]["marks"][$competence_id]["marks"][] = array($mark_idx, $is_important == 1, $is_imported);
		}
		return $result;
	}

	public static function prepare_competence_results(&$math_competence_results, $competence_set_id)
	{
		$central_competence_set_id = sys_params_helper::get_central_competence_set_id();
		self::prepare_competence_set($math_competence_results, $central_competence_set_id);
		if ($competence_set_id != $central_competence_set_id)
		{
			self::prepare_competence_set($math_competence_results, $competence_set_id);
		}
	}

	public static function results_number_format($result)
	{
		return number_format($result, 2, ".", "");
	}

	private static function prepare_competence_set(&$math_competence_results, $competence_set_id)
	{
		// all these functions MUST check that competence set is not "ready" for a user
		math_direct_translators_helper::apply_direct_translators($math_competence_results, $competence_set_id);
		self::calc_competence_results($math_competence_results, $competence_set_id);
		// missed formulae translation
		math_integral_competences_helper::calc_integral_competences($math_competence_results, $competence_set_id);
		self::calc_rater_counts($math_competence_results, $competence_set_id);
		self::mark_as_ready($math_competence_results, $competence_set_id);
		return $math_competence_results;
	}

	public static function merge_marks($marks_n)
	{
		$result = array();
		foreach ($marks_n as $marks)
		{
			foreach ($marks as $mark)
			{
				list($mark_idx, $is_important, $is_imported) = $mark;
				if (isset($result[$mark_idx]))
				{
					$result[$mark_idx][1] = $result[$mark_idx][1] || $is_important;
					$result[$mark_idx][2] = $result[$mark_idx][2] && $is_imported;
				}
				else
				{
					$result[$mark_idx] = $mark;
				}
			}
		}
		return array_values($result);
	}

	public static function mark_marks_imported($marks)
	{
		$result = array();
		foreach ($marks as $mark)
		{
			list($mark_idx, $is_important, ) = $mark;
			$result[] = array($mark_idx, $is_important, true);
		}
		return $result;
	}

	public static function remove_imported_marks($marks)
	{
		$result = array();
		foreach ($marks as $mark)
		{
			list(,, $is_imported) = $mark;
			if (!$is_imported)
			{
				$result[] = $mark;
			}
		}
		return $result;
	}

	public static function test_calc_competence_results(&$math_competence_results, $competence_set_id)
	{
		return self::calc_competence_results($math_competence_results, $competence_set_id);
	}

	private static function calc_competence_results(&$math_competence_results, $competence_set_id)
	{
		foreach ($math_competence_results as &$user_competence_results)
		{
			if (isset($user_competence_results["sets"][$competence_set_id]["ready"]))
			{
				continue;
			}
			array_key_set($user_competence_results["sets"], $competence_set_id, array(
				"marks" => array(),
			));
			foreach ($user_competence_results["sets"][$competence_set_id]["marks"] as $competence_id => $competence_data)
			{
				$stat = array();
				foreach ($competence_data["marks"] as $mark)
				{
					list($mark_idx, $is_important, $is_imported) = $mark;
					$mark_info = $user_competence_results["marks"][$mark_idx];
					// @todo redefinition of $mark
					list($type, $stream_id, $mark_competence_set_id, $activity_id, $weight, $target_id, $mark, $rater_user_id) = $mark_info;
					if ($mark < 1)
					{
						continue;
					}
					$effective_mark = !$is_important ? 1 : (int) $mark;
					$mult = !$is_important ? ($mark > 1 ? 1.0 : 0.5) : 1.0;
					if ($effective_mark)
					{
						array_key_set($stat, $effective_mark, array(
							"count" => 0.0,
							"roles" => array(),
						));
						$stat[$effective_mark]["count"] += ($weight === 'hi' ? 2.0 : 1.0) * $mult;
						$stat[$effective_mark]["roles"][$type . ($type === self::TYPE_ROLE ? $target_id : $activity_id)] = true;
					}
				}
				$user_competence_results["sets"][$competence_set_id]["marks"][$competence_id]["result"] = self::get_competence_result($stat);
			}
		}
		unset($user_competence_results);
	}

	public static function test_get_competence_result($stat)
	{
		return self::get_competence_result($stat);
	}

	private static function get_competence_result($stat)
	{
		$competence_result = 0.0;
		for ($m = 1; $m <= self::MAX_MARK; ++$m)
		{
			if (!isset($stat[$m]))
			{
				continue;
			}
			$mark_count = $stat[$m]["count"];
			$role_count = sizeof($stat[$m]["roles"]);
			if ($mark_count == 0 or $role_count == 0)
			{
				continue;
			}
			if ($competence_result < $m - 1)
			{
				$mark_count = max($mark_count - 1, 0); //@todo 2 or 1???
				$role_count = max($role_count - 1, 0);
			}
			if ($mark_count == 0 and $competence_result == 0.0 and $m > 2)
			{
				$competence_result = $m - 2; // "3" only
			}
			else
			{
				$delta = min($role_count / self::PROVE_ROLE_COUNT, $mark_count / self::PROVE_MARK_COUNT);
				$delta = min($delta, 1);
				$competence_result = $m - 1 + $delta;
			}
		}
		return $competence_result;
	}

	private static function calc_rater_counts(&$math_competence_results, $competence_set_id)
	{
		foreach ($math_competence_results as &$user_competence_results)
		{
			if (isset($user_competence_results["sets"][$competence_set_id]["ready"]))
			{
				continue;
			}
			if (!isset($user_competence_results["sets"][$competence_set_id]["marks"]))
			{
				continue;
			}
			$math_plain_competence_results = &$user_competence_results["sets"][$competence_set_id]["marks"];
			foreach ($math_plain_competence_results as $competence_id => &$competence_data)
			{
				$competence_marks = $competence_data["marks"];
				$raters = array();
				foreach ($competence_marks as $mark)
				{
					list($mark_idx, $is_important, $is_imported) = $mark;
					$mark_info = $user_competence_results["marks"][$mark_idx];
					list(,,,,,,, $rater_user_id) = $mark_info;
					$raters[$rater_user_id] = true;
				}
				$competence_data["rater_count"] = sizeof($raters);
			}
			unset($competence_data);
			unset($math_plain_competence_results);
		}
		unset($user_competence_results);
	}

	private static function mark_as_ready(&$math_competence_results, $competence_set_id)
	{
		foreach ($math_competence_results as &$user_competence_results)
		{
			if (isset($user_competence_results["sets"][$competence_set_id]["ready"]))
			{
				continue;
			}
			//@todo test these 2 lines
			$user_competence_results["sets"][$competence_set_id]["ready"] = true;
			$user_competence_results["store"] = true;
		}
		unset($user_competence_results);
	}

}

?>