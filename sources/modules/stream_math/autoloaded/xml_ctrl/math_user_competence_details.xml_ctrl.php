<?php

require_once PATH_MODULES . "/stream_math/lib/all_competence_results_loader.php";

class math_user_competence_details_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "mark";
	protected $xml_attrs = array("user_id", "competence_set_id", "competence_id", "competence_title", "competence_is_deleted");
	protected $dependencies_settings = array(
		array(
			"column" => "stream_id",
			"ctrl" => "stream_short",
		),
	);
	// Internal
	protected $page;
	protected $user_id;
	protected $competence_set_id;
	protected $competence_id;
	protected $competence_title;
	protected $competence_is_deleted;
	protected $competence_row;
	protected $user_competence_results;
	protected $competence_data = null;
	protected $integral_data = null;

	public function __construct($user_id, $competence_set_id, $competence_id, $page)
	{
		$this->user_id = $user_id;
		$this->competence_set_id = $competence_set_id;
		$this->competence_id = $competence_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, 50));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->load_competence_row();
		if (!$this->competence_row)
		{
			$this->set_error_404();
			return;
		}
		$this->competence_title = $this->competence_row["title"];
		$this->competence_is_deleted = $this->competence_row["is_deleted"] == 1;
		$this->load_competence_data();
		$this->load_integral_data();
		$this->build_mark_list();
//		if (!math_marks_helper::show_link($this->competence_data))
//		{
//			$this->set_error_404();
//		}
	}

	protected function postprocess_data()
	{
		$this->update_mark_list();
	}

	private function load_competence_row()
	{
		$this->competence_row = $this->db->get_row("
			SELECT id, title, is_deleted
			FROM competence_full
			WHERE id = {$this->competence_id} AND competence_set_id = {$this->competence_set_id}
		");
	}

	private function load_competence_data()
	{
		$loader = new all_competence_results_loader($this->competence_set_id, $this->user_id);
		$this->user_competence_results = $loader->get_competence_results();

		if (isset($this->user_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$this->competence_id]))
		{
			$this->competence_data = &$this->user_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$this->competence_id];
		}
		elseif ($this->competence_is_deleted)
		{
			$this->set_error_404();
		}
	}

	private function load_integral_data()
	{
		if (isset($this->competence_data["result_type"]) and $this->competence_data["result_type"] === math_competence_results_helper::RESULT_TYPE_INTEGRAL)
		{
			$integral_data = integral_competences_helper::get_all_competences_in_calc_order($this->competence_set_id);
			if (!isset($integral_data[$this->competence_id]) or empty($this->competence_id))
			{
				trigger_error("Integral result without integral data: competence_set_id={$this->competence_set_id}, competence_id={$this->competence_id}");
			}
			$titles = $this->db->fetch_column_values("
				SELECT id, title
				FROM competence_full
				WHERE id IN (" . join(", ", array_keys($integral_data[$this->competence_id])) . ")
			", "title", "id");
			$this->integral_data = array();
			foreach ($integral_data[$this->competence_id] as $competence_id => $required_result)
			{
				$this->integral_data[$competence_id] = array(
					"id" => $competence_id,
					"title" => $titles[$competence_id],
					"required_result" => $required_result,
				);
				if (isset($this->user_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$competence_id]))
				{
					math_marks_helper::copy_prepared_competence_data($this->integral_data[$competence_id], $this->user_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$competence_id]);
				}
				else
				{
					math_marks_helper::copy_prepared_competence_data($this->integral_data[$competence_id]);
				}
			}
			$this->integral_data = array_sort($this->integral_data, "id");
		}
	}

	private function build_mark_list()
	{
		$this->data = array();
		if ($this->competence_data and isset($this->competence_data["marks"]))
		{
			$this->competence_data["marks"] = array_sort($this->competence_data["marks"], 0); // Sorting by mark time
			foreach ($this->competence_data["marks"] as $mark)
			{
				list($mark_idx, $is_important, ) = $mark;
				$mark_info = $this->user_competence_results[$this->user_id]["marks"][$mark_idx];
				list($type, $stream_id, $mark_competence_set_id, $activity_id, $weight, $target_id, $mark, $rater_user_id) = $mark_info;
				$this->data[math_marks_helper::get_mark_key($type, $activity_id, $target_id)] = array(
					"type" => $type,
					"stream_id" => $stream_id,
					"activity_id" => $activity_id,
					"weight" => $weight,
					($type === math_competence_results_helper::TYPE_ROLE ? "role_id" : "competence_id") => $target_id,
					"mark" => $mark,
					"rater_user_id" => $rater_user_id,
					"is_important" => $is_important,
				);
			}
		}
	}

	private function update_mark_list()
	{
		$role_marks = array();
		$competence_marks = array();
		foreach ($this->data as $row)
		{
			if ($row["type"] === math_competence_results_helper::TYPE_ROLE)
			{
				$role_marks[] = "activity_id = {$row["activity_id"]} AND user_id = {$this->user_id} AND role_id = {$row["role_id"]}";
			}
			else
			{
				$competence_marks[] = "activity_id = {$row["activity_id"]} AND user_id = {$this->user_id} AND competence_id = {$row["competence_id"]}";
			}
		}
		if (sizeof($role_marks))
		{
			$info = $this->db->fetch_all("
				SELECT activity_id, role_id, comment, change_time AS mark_time
				FROM activity_participant_mark
				WHERE " . join(" OR ", $role_marks) . "
			");
			foreach ($info as $mark_info)
			{
				$key = math_marks_helper::get_mark_key(math_competence_results_helper::TYPE_ROLE, $mark_info["activity_id"], $mark_info["role_id"]);
				if (isset($this->data[$key]))
				{
					$this->data[$key]["comment"] = $mark_info["comment"];
					$this->data[$key]["mark_time"] = $mark_info["mark_time"];
				}
			}
		}
		if (sizeof($competence_marks))
		{
			$info = $this->db->fetch_all("
				SELECT activity_id, competence_id, comment, change_time AS mark_time
				FROM activity_competence_mark
				WHERE " . join(" OR ", $competence_marks) . "
			");
			foreach ($info as $mark_info)
			{
				$key = math_marks_helper::get_mark_key(math_competence_results_helper::TYPE_COMPETENCE, $mark_info["activity_id"], $mark_info["competence_id"]);
				if (isset($this->data[$key]))
				{
					$this->data[$key]["comment"] = $mark_info["comment"];
					$this->data[$key]["mark_time"] = $mark_info["mark_time"];
				}
			}
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		$data = array();
		math_marks_helper::copy_prepared_competence_data($data, $this->competence_data);
		$this->db_xml_converter->build_xml($xdom, array($data), true);
		if ($this->integral_data)
		{
			$this->db_xml_converter->build_xml($xdom->create_child_node("integral_data"), $this->integral_data, false, "competence");
		}
	}

	protected function postprocess()
	{
		foreach ($this->data as $row)
		{
			if (isset($row["stream_id"]))
			{
				if (isset($row["activity_id"]))
				{
					$this->xml_loader->add_xml_by_class_name("activity_short_xml_ctrl", $row["activity_id"], $row["stream_id"]);
				}
				if (isset($row["role_id"]))
				{
					$this->xml_loader->add_xml_by_class_name("role_short_xml_ctrl", $row["role_id"], $row["stream_id"]);
				}
				if (isset($row["competence_id"]))
				{
					$this->xml_loader->add_xml_by_class_name("competence_title_xml_ctrl", $row["competence_id"]);
				}
				if (isset($row["rater_user_id"]))
				{
					$this->xml_loader->add_xml_by_class_name("stream_user_short_xml_ctrl", $row["rater_user_id"], $row["stream_id"]);
				}
			}
		}
	}

}

?>