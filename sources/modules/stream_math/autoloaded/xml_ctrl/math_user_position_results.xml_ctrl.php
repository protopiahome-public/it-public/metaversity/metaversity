<?php

require_once PATH_MODULES . "/stream_math/lib/all_competence_results_loader.php";

class math_user_position_results_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "credit_expert_user_id",
			"ctrl" => "user_short",
		),
	);
	protected $xml_attrs = array("user_id", "stream_id", "competence_set_id");
	protected $xml_row_name = "position";
	// Internal
	/**
	 * @var sort_easy_processor
	 */
	protected $sort_processor;
	protected $user_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $stream_id;
	protected $competence_set_id;
	protected $top_only;
	protected $math_position_results;
	protected $position_ids = null;

	public function __construct($user_id, stream_obj $stream_obj, $top_only = false)
	{
		$this->user_id = $user_id;
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		$this->top_only = $top_only;
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("match", "p.id", "p.id");
		$processor->add_order("title", "p.title", "p.title DESC");
		$this->add_easy_processor($processor);

		$this->sort_processor = $processor;
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->load_results();
		$this->load_position_ids();

		$select_sql->add_from("position p");
		$select_sql->add_select_fields("p.id, p.title");
		$select_sql->add_select_fields("up.user_id IS NOT NULL AS is_focus");
		$select_sql->add_select_fields("pc.expert_user_id AS credit_expert_user_id, pc.comment AS credit_comment");
		$select_sql->add_join("LEFT JOIN user_position up ON up.position_id = p.id AND up.user_id = {$this->user_id}");
		$select_sql->add_join("LEFT JOIN position_credit pc ON pc.position_id = p.id AND pc.user_id = {$this->user_id}");
		$select_sql->add_where("p.stream_id = {$this->stream_id}");
		$select_sql->add_where("p.competence_count_calc > 0 OR up.user_id IS NOT NULL");
		if (!is_null($this->position_ids))
		{
			$select_sql->add_where("p.id IN (" . join(", ", $this->position_ids) . ") OR up.user_id IS NOT NULL");
		}
		$this->data = $this->db->fetch_all($select_sql->get_sql(), "id");

		$this->update_data();
	}

	private function load_results()
	{
		$loader = new all_competence_results_loader($this->competence_set_id, $this->user_id, true);
		$math_competence_results = $loader->get_competence_results();

		$math_positions = math_positions_helper::fetch_stream_positions_data($this->stream_id, $this->competence_set_id);

		$this->math_position_results = math_position_results_helper::get_position_results($math_positions, $math_competence_results, $this->competence_set_id);
		if (isset($this->math_position_results[$this->user_id]))
		{
			arsort($this->math_position_results[$this->user_id]);
		}
	}

	private function load_position_ids()
	{
		if ($this->top_only)
		{
			$this->position_ids = array(0); // zero to simplify WHERE ... IN(...) operator
			if (isset($this->math_position_results[$this->user_id]))
			{
				foreach ($this->math_position_results[$this->user_id] as $position_id => $tmp)
				{
					$this->position_ids[] = $position_id;
					if (sizeof($this->position_ids) >= 4) // zero adds one
					{
						break;
					}
				}
			}
		}
	}

	private function update_data()
	{
		foreach ($this->data as &$row)
		{
			if (isset($this->math_position_results[$this->user_id][$row["id"]]))
			{
				$row["match"] = $this->math_position_results[$this->user_id][$row["id"]];
			}
			else
			{
				$row["match"] = 0;
			}
		}
		unset($row);

		if ($this->sort_processor->get_order() === "match")
		{
			$this->data = array_sort($this->data, "match", !$this->sort_processor->get_back() ? SORT_DESC : SORT_ASC);
		}
	}

}

?>