<?php

require_once PATH_MODULES . "/stream_math/lib/all_competence_results_loader.php";

class math_position_details_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "competence";
	protected $xml_attrs = array("user_id", "stream_id", "competence_set_id", "position_id", "position_title", "match");
	protected $dependencies_settings = array(
		array(
			"column" => "stream_id",
			"ctrl" => "stream_short",
		),
	);
	// Internal
	protected $user_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $stream_id;
	protected $competence_set_id;
	protected $position_id;
	protected $position_row;
	protected $position_title;
	protected $match;
	protected $math_competence_results;
	protected $math_positions;
	protected $position_competences = array();
	protected $credit;

	public function __construct(stream_obj $stream_obj, $position_id, $user_id = null)
	{
		$this->user_id = $user_id;
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		$this->position_id = $position_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->load_position_row();
		if (!$this->position_row)
		{
			$this->set_error_404();
			return;
		}
		$this->position_title = $this->position_row["title"];
		$this->load_position_data();
		if ($this->user_id)
		{
			$this->load_math();
			$this->load_credit_data();
		}
	}

	private function load_position_row()
	{
		$this->position_row = $this->db->get_row("
			SELECT id, title
			FROM position
			WHERE id = {$this->position_id} AND stream_id = {$this->stream_id}
		");
	}

	private function load_position_data()
	{
		$this->math_positions = array(
			$this->position_id => array(),
		);
		$math_positions_raw = math_positions_helper::fetch_stream_positions_data($this->stream_id, $this->competence_set_id);
		if (isset($math_positions_raw[$this->position_id]))
		{
			$this->math_positions[$this->position_id] = $math_positions_raw[$this->position_id];
		}

		$competences = $this->math_positions[$this->position_id];
		$titles = array();
		if ($competences)
		{
			$titles = $this->db->fetch_column_values("
				SELECT id, title
				FROM competence_full
				WHERE id IN (" . join(", ", array_keys($competences)) . ")
			", "title", "id");
		}
		$this->data = array();
		foreach ($competences as $competence_id => $required_result)
		{
			$this->data[$competence_id] = array(
				"id" => $competence_id,
				"title" => $titles[$competence_id],
				"required_result" => $required_result,
			);
			if (isset($this->math_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$competence_id]))
			{
				math_marks_helper::copy_prepared_competence_data($this->data[$competence_id], $this->math_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$competence_id]);
			}
			else
			{
				math_marks_helper::copy_prepared_competence_data($this->data[$competence_id]);
			}
		}
		$this->data = array_sort($this->data, "id");
	}

	private function load_math()
	{
		$loader = new all_competence_results_loader($this->competence_set_id, $this->user_id);
		$this->math_competence_results = $loader->get_competence_results();

		$competences = $this->math_positions[$this->position_id];
		foreach (array_keys($competences) as $competence_id)
		{
			if (isset($this->math_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$competence_id]))
			{
				math_marks_helper::copy_prepared_competence_data($this->data[$competence_id], $this->math_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"][$competence_id]);
			}
			else
			{
				math_marks_helper::copy_prepared_competence_data($this->data[$competence_id]);
			}
		}

		$math_position_results = math_position_results_helper::get_position_results($this->math_positions, $this->math_competence_results, $this->competence_set_id);
		$this->match = 0;
		if (isset($math_position_results[$this->user_id][$this->position_id]))
		{
			$this->match = $math_position_results[$this->user_id][$this->position_id];
		}
	}

	private function load_credit_data()
	{
		$this->credit = position_credit_helper::get_credit($this->position_id, $this->user_id);
	}

	public function modify_xml(xdom $xdom)
	{
		if ($this->credit)
		{
			$this->db_xml_converter->build_xml($xdom->create_child_node("credit"), array($this->credit), true);
			if ($this->credit["expert_user_id"])
			{
				$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $this->credit["expert_user_id"]);
			}
		}
	}

}

?>