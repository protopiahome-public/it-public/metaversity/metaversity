<?php

require_once PATH_MODULES . "/stream_math/lib/stream_competence_results_loader.php";

class math_rating_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
		array(
			"column" => "credit_expert_user_id",
			"ctrl" => "user_short",
		),
	);
	protected $xml_attrs = array("position_id", "stream_id", "competence_set_id", "position_no_competences", "cache_miss", "cache_time", "cache_unit");
	protected $xml_row_name = "user";
	// Internal
	protected $position_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $stream_id;
	protected $competence_set_id;
	protected $page;
	protected $position_no_competences = false;
	protected $cache_miss = false;
	protected $cache_time; // int
	protected $cache_unit; // "minute" or "second"

	public function __construct($position_id, stream_obj $stream_obj, $page)
	{
		$this->position_id = $position_id;
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, 50));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$position_select_sql = new select_sql();
		$position_select_sql->add_from("position p");
		$position_select_sql->add_select_fields("p.id, p.title, p.descr_html");
		$position_select_sql->add_where("p.id = {$this->position_id}");
		$position_select_sql->add_where("p.stream_id = {$this->stream_id}");
		$position_data = $this->db->get_row($position_select_sql->get_sql());
		if (!$position_data)
		{
			$this->set_error_404();
			return;
		}
		$this->load_rating();
	}

	private function load_rating()
	{
		$math_positions = math_positions_helper::fetch_stream_position_data($this->position_id, $this->stream_id, $this->competence_set_id);
		$math_position_data = reset($math_positions);
		if (!sizeof($math_position_data))
		{
			$this->position_no_competences = true;
			return;
		}

		// Alternative method without cache
//		$loader = new stream_competence_results_loader($this->competence_set_id, null, true, $this->stream_id);
//		$math_competence_results = $loader->get_competence_results();
//
//		$ratings = array(
//			"ratings" => math_position_results_helper::get_position_results($math_positions, $math_competence_results, $this->competence_set_id),
//			"time" => time(),
//		);

		$ratings = math_rating_cache_helper::check_and_request_build($this->stream_id);
		$this->cache_miss = !$ratings;
		if (!$ratings)
		{
			return;
		}

		$this->cache_time = time() - $ratings["time"];
		$this->cache_unit = $this->cache_time >= 60 ? "minute" : "second";
		$this->cache_time = $this->cache_time >= 60 ? round($this->cache_time / 60) : $this->cache_time;

		$this->data = array();
		foreach ($ratings["ratings"] as $user_id => $user_positions)
		{
			$this->data[] = array(
				"id" => $user_id,
				"match" => isset($user_positions[$this->position_id]) ? $user_positions[$this->position_id] : 0,
			);
		}
		$this->data = array_sort($this->data, "match", SORT_DESC);
	}

	public function postprocess_data()
	{
		if ($this->data)
		{
			$user_ids = array_column($this->data, "id");
			$credits = position_credit_helper::get_credits($this->position_id, $user_ids);
			foreach ($this->data as &$row)
			{
				$user_id = $row["id"];
				if (isset($credits[$user_id]))
				{
					$row["credit_expert_user_id"] = $credits[$user_id]["expert_user_id"];
					$row["credit_comment"] = $credits[$user_id]["comment"];
				}
			}
			unset($row);
		}
	}

}

?>