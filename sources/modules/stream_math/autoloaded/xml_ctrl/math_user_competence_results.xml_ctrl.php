<?php

require_once PATH_MODULES . "/stream_math/lib/all_competence_results_loader.php";

class math_user_competence_results_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("stream_id", "user_id", "competence_set_id", "show_linear");
	protected $xml_row_name = "competence";
	// Internal
	protected $stream_id;
	protected $user_id;
	protected $competence_set_id;
	protected $show_linear = false;

	/**
	 * @var select_sql_filter
	 */
	protected $sort_mode_sql_filter;

	public function __construct($stream_id, $user_id, $competence_set_id)
	{
		$this->stream_id = $stream_id;
		$this->user_id = $user_id;
		$this->competence_set_id = $competence_set_id;
		parent::__construct();
	}

	public function init()
	{
		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter($this->sort_mode_sql_filter = new select_sql_filter("sort-mode", trans("Sort mode"), array(
			"diff" => trans("Sort by deficit"),
			"required" => trans("Sort by required level, inside level – by deficit"),
		)));
		$this->sort_mode_sql_filter->set_inactive_title(trans("Tree mode"));
		$this->add_easy_processor($processor);
	}

	public function load_data(select_sql $select_sql = null)
	{
		$this->data = array();

		$loader = new all_competence_results_loader($this->competence_set_id, $this->user_id);
		$user_competence_results = $loader->get_competence_results();

		if (isset($user_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"]))
		{
			foreach ($user_competence_results[$this->user_id]["sets"][$this->competence_set_id]["marks"] as $competence_id => $competence_data)
			{
				$this->data[$competence_id] = array("id" => $competence_id);
				math_marks_helper::copy_prepared_competence_data($this->data[$competence_id], $competence_data);
			}
		}

		$required_competences = stream_user_positions_helper::get_stream_user_required_competence_results($this->stream_id, $this->user_id, $this->competence_set_id);
		foreach ($required_competences as $competence_id => $required_result)
		{
			if (!isset($this->data[$competence_id]))
			{
				$this->data[$competence_id] = array("id" => $competence_id);
			}
			$this->data[$competence_id]["required_result"] = $required_result;
		}

		if ("diff" === $this->sort_mode_sql_filter->get_value())
		{
			foreach ($this->data as &$row)
			{
				$a = isset($row["result"]) ? $row["result"] : 0.0;
				$b = isset($row["required_result"]) ? $row["required_result"] : 0.0;
				$row["sort"] = ($b - $a) * 100 + $b;
			}
			unset($row);
			$this->data = array_sort($this->data, "sort", SORT_DESC);
			$this->show_linear = true;
		}
		elseif ("required" === $this->sort_mode_sql_filter->get_value())
		{
			foreach ($this->data as &$row)
			{
				$a = isset($row["result"]) ? $row["result"] : 0.0;
				$b = isset($row["required_result"]) ? $row["required_result"] : 0.0;
				$row["sort"] = $b * 100 - $a;
			}
			unset($row);
			$this->data = array_sort($this->data, "sort", SORT_DESC);
			$this->show_linear = true;
		}
	}

}

?>