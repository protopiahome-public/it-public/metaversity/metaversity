<?php

class math_user_streams_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "stream";
	protected $xml_attrs = array("results_access_level");
	// Internal
	protected $user_id;

	/**
	 * @var user_obj
	 */
	protected $user_obj;
	protected $results_access_level;
	protected $page;

	public function __construct($user_id, $page)
	{
		$this->user_id = $user_id;
		$this->user_obj = user_obj::instance($this->user_id);
		$this->results_access_level = $this->user_obj->get_results_access_level();
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		if ($this->page)
		{
			$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

			$processor = new sort_easy_processor();
			$processor->add_order("marks", "dul.mark_count_calc DESC", "dul.mark_count_calc ASC");
			$processor->add_order("positions", "dul.position_count_calc DESC", "dul.position_count_calc ASC");
			$processor->add_order("title", "d.title", "d.title DESC");
			$this->add_easy_processor($processor);

			$this->sort_processor = $processor;
		}
	}

	protected function load_data(select_sql $select_sql = null)
	{
		//$central_stream_id = competence_restrictions_helper::get_corresponding_stream_id(sys_params_helper::get_central_competence_set_id());

		$select_sql->add_from("stream d");
		$select_sql->add_join("LEFT JOIN stream_user_link dul ON dul.stream_id = d.id AND dul.user_id = {$this->user_id}");
		$select_sql->add_select_fields("d.id, d.name");
		if ($this->lang->get_current_lang_code() === "en")
		{
			$select_sql->add_select_fields("d.title_en AS title, d.title_short_en AS title_short");
		}
		else
		{
			$select_sql->add_select_fields("d.title, d.title_short");
		}
		//$select_sql->add_select_fields("d.id = {$central_stream_id} AS is_central");
		$select_sql->add_select_fields("IFNULL(dul.mark_count_calc, 0) AS mark_count_calc");
		$select_sql->add_select_fields("IFNULL(dul.position_count_calc, 0) AS position_count_calc");
		//$select_sql->prepend_order("d.id = {$central_stream_id} DESC");
		if (!$this->page)
		{
			//$select_sql->add_where("d.id = {$central_stream_id} OR dul.mark_count_calc > 0 OR dul.status <> 'deleted' AND dul.status <> 'pretender'");
			$select_sql->add_where("dul.mark_count_calc > 0 OR dul.status <> 'deleted' AND dul.status <> 'pretender'");
			$select_sql->add_order("dul.mark_count_calc DESC");
			$select_sql->set_limit(5);
		}
		$this->data = $this->db->fetch_all($select_sql->get_sql(), "id");
		//array_move_element_up($this->data, $central_stream_id);
	}

	protected function process_data()
	{
		if (!$this->page and $this->data)
		{
			foreach ($this->data as &$row)
			{
				$stream_id = $row["id"];
				$this->xml_loader->add_xml_by_class_name("stream_short_xml_ctrl", $stream_id);
				if (user_access_helper::can_access_results($this->user_obj, stream_obj::instance($stream_id)))
				{
					$this->xml_loader->add_xml(new math_user_position_results_xml_ctrl($this->user_id, stream_obj::instance($stream_id), true));
				}
				else
				{
					$row["access_denied"] = true;
				}
			}
			unset($row);
		}
	}

}

?>