<?php

class math_user_competence_results_cache extends base_cache
{

	public static function init($cache_name, $user_id, $short, $stream_ids = array(), $ready_competence_set_ids = array())
	{
		$tag_keys = array();
		$tag_keys[] = user_marks_cache_tag::init($user_id)->get_key();
		foreach ($stream_ids as $stream_id)
		{
			// @todo test
			$tag_keys[] = stream_roles_cache_tag::init($stream_id)->get_key();
		}
		foreach ($ready_competence_set_ids as $competence_set_id)
		{
			// @todo test
			$tag_keys[] = competence_set_translators_cache_tag::init($competence_set_id)->get_key();
		}
		return parent::get_cache(__CLASS__, $cache_name, $user_id, $short ? "1" : "0", $tag_keys);
	}

}

?>