<?php

class math_rating_wait_for_cache_ajax_page extends base_wait_for_cache_ajax_ctrl
{

	protected function get_cache()
	{
		$stream_id = REQUEST("stream_id");
		if (!is_good_id($stream_id))
		{
			return false;
		}
		return math_ratings_cache::init($stream_id);
	}

}

?>