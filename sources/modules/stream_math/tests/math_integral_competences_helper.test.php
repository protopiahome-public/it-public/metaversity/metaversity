<?php

class math_integral_competences_helper_test extends base_test
{

	public function calc_integral_competences_test()
	{
		integral_competences_calc_order_cache::init(1)->set(array(
			103 => array(
				101 => 2.0,
				102 => 2.0,
			),
			104 => array(
				101 => 2.0,
				102 => 2.0,
			),
			105 => array(
				101 => 2.0,
				102 => 2.0,
			),
		));

		$math_competence_results = $this->val();
		$correct_value = $math_competence_results;
		$new = array(
			"marks" => array(
				array(0, true, true),
			),
			"result" => 1.5,
			"result_type" => "integral",
		);
		$correct_value[1]["sets"][1]["marks"][103] = $new;
		$new["marks"] = array();
		$correct_value[1]["sets"][1]["marks"][104] = $new;
		math_integral_competences_helper::calc_integral_competences($math_competence_results, 1);
		$this->assert_identical($math_competence_results, $correct_value);
	}

	public function chain_test()
	{
		integral_competences_calc_order_cache::init(1)->set(array(
			103 => array(
				101 => 2.0,
				102 => 2.0,
			),
			104 => array(
				101 => 2.0,
				102 => 2.0,
			),
			109 => array(
				103 => 3.0,
				104 => 3.0,
			),
		));

		$math_competence_results = $this->val();
		$math_competence_results[1]["sets"][1]["marks"][103]["marks"][0][2] = false; // !

		$correct_value = $math_competence_results;
		$correct_value[1]["sets"][1]["marks"][103] = array(
			"marks" => array(
				array(0, true, false), // !
			),
			"result" => 1.5,
			"result_type" => "integral",
		);
		$correct_value[1]["sets"][1]["marks"][104] = array(
			"marks" => array(),
			"result" => 1.5,
			"result_type" => "integral",
		);
		$correct_value[1]["sets"][1]["marks"][109] = array(
			"marks" => array(),
			"result" => 1.5,
			"result_type" => "integral",
		);
		math_integral_competences_helper::calc_integral_competences($math_competence_results, 1);
		$this->assert_identical($math_competence_results, $correct_value);
	}

	public function ready_check_test()
	{
		$math_competence_results = $this->val();
		$math_competence_results[1]["sets"][1]["ready"] = true;
		$correct_value = $math_competence_results;
		math_integral_competences_helper::calc_integral_competences($math_competence_results, 1);
		$this->assert_identical($math_competence_results, $correct_value);
	}

	private function val()
	{
		return array(
			1 => array(
				"marks" => array("..."),
				"sets" => array(
					1 => array(
						"marks" => array(
							101 => array(
								"marks" => array(
									array(0, true, false),
									array(2, false, true),
								),
								"result" => 1.0,
							),
							102 => array(
								"marks" => array(
									array(0, true, true),
									array(3, true, true),
									array(4, true, false),
								),
								"result" => 1.0,
							),
							103 => array(
								"marks" => array(
									array(0, true, true),
								),
								"result" => 0.25,
							),
							105 => array(
								"marks" => array("..."),
								"result" => 3.0, // too big to be overriden
							),
						),
					),
					2 => array("..."),
				),
			),
		);
	}

}

?>