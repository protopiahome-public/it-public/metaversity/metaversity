<?php

require_once PATH_MODULES . "/stream_math/lib/base_competence_results_loader.php";

class test1_competence_results_loader extends base_competence_results_loader
{

	// Settings
	protected $cache_name = "all";
	// Internal
	private $fetch_requests = array();
	private $data = array(
		// <user_id>, <type>, <stream_id>, <competence_set_id>, <activity_id>, <weight>, <role_id>, <competence_id>, <mark>, <rater_user_id>, <is_important>
		array(1, "role", 1, 1, 101, 'low', 1011, 101, 3, 1, true),
		array(1, "role", 2, 2, 201, 'hi', 2011, 201, 3, 2, true),
		array(1, "role", 2, 2, 201, 'hi', 2011, 202, 2, 2, false),
		array(1, "competence", 1, 1, 103, 'low', null, 101, 3, 1, true),
		array(2, "role", 1, 1, 101, 'low', 1011, 101, 3, 1, true),
	);

	public function set_cache_name($cache_name)
	{
		$this->cache_name = $cache_name;
	}

	public function get_fetch_requests()
	{
		$tmp = $this->fetch_requests;
		$this->fetch_requests = array();
		return $tmp;
	}

	protected function fetch_marks_raw()
	{
		$this->fetch_requests[] = $this->fetch_users_ids;
		if (is_null($this->fetch_users_ids))
		{
			return $this->data;
		}
		else
		{
			$result = array();
			foreach ($this->data as $row)
			{
				if (in_array($row[0], $this->fetch_users_ids))
				{
					$result[] = $row;
				}
			}
			return $result;
		}
	}

	protected function get_all_users_ids()
	{
		return array_values(array_unique(array_column($this->data, 0)));
	}

}

class base_competence_results_fetcher_test extends base_test
{

	public function set_up()
	{
		math_test_helper::clean();
	}

	public function load_all_test()
	{
		$this->mcache->flush();

		$fetcher = new test1_competence_results_loader(1);
		$math_competence_results = $fetcher->get_competence_results();
		$fetch_requests = $fetcher->get_fetch_requests();
		$this->assert_identical($fetch_requests, array(array(1, 2)));
		$this->assert_identical(array_keys($math_competence_results), array(1, 2));
	}

	public function load_user_test()
	{
		$this->mcache->flush();

		$fetcher = new test1_competence_results_loader(1, 1);
		$math_competence_results = $fetcher->get_competence_results();
		$fetch_requests = $fetcher->get_fetch_requests();
		$this->assert_identical($fetch_requests, array(array(1)));
		$this->assert_identical(array_keys($math_competence_results), array(1));
	}

	public function store_cache_test()
	{
		$this->mcache->flush();

		$fetcher = new test1_competence_results_loader(1);
		$fetcher->get_competence_results();
		$this->assert_true(is_array(math_user_competence_results_cache::init("all", 1, false)->get()));
	}

	public function cache_test()
	{
		$this->mcache->flush();

		$fetcher = new test1_competence_results_loader(1, 1);
		$math_competence_results = $fetcher->get_competence_results();
		$fetch_requests = $fetcher->get_fetch_requests();
		$this->assert_identical($fetch_requests, array(array(1)));
		$this->assert_identical(array_keys($math_competence_results), array(1));

		$fetcher = new test1_competence_results_loader(1);
		$math_competence_results = $fetcher->get_competence_results();
		$fetch_requests = $fetcher->get_fetch_requests();
		$this->assert_identical($fetch_requests, array(array(2)));
		$this->assert_identical(array_keys($math_competence_results), array(1, 2));
	}

	public function empty_cache_test()
	{
		$this->mcache->flush();

		$fetcher = new test1_competence_results_loader(1, 1);
		$fetcher->get_competence_results();

		$fetcher = new test1_competence_results_loader(1, 2);
		$fetcher->get_competence_results();

		$fetcher = new test1_competence_results_loader(1);
		$math_competence_results = $fetcher->get_competence_results();
		$fetch_requests = $fetcher->get_fetch_requests();
		$this->assert_identical($fetch_requests, array()); // fetch is not called
		$this->assert_identical(array_keys($math_competence_results), array(1, 2));
	}

	public function cache_dependency_stream_id_test()
	{
		$this->mcache->flush();

		$fetcher = new test1_competence_results_loader(1);
		$fetcher->get_competence_results();
		$this->assert_true(is_array(math_user_competence_results_cache::init("all", 1, false)->get()));

		$this->mcache->increase_now_for_test();
		stream_roles_cache_tag::init(999)->update();
		$this->assert_true(is_array(math_user_competence_results_cache::init("all", 1, false)->get()));

		$this->mcache->increase_now_for_test();
		stream_roles_cache_tag::init(1)->update();
		$this->assert_false(is_array(math_user_competence_results_cache::init("all", 1, false)->get()));
	}

	public function no_cache_test()
	{
		$this->mcache->flush();

		$fetcher = new test1_competence_results_loader(1, 1);
		$fetcher->set_cache_name(null);
		$math_competence_results = $fetcher->get_competence_results();
		$fetch_requests = $fetcher->get_fetch_requests();
		$this->assert_identical($fetch_requests, array(array(1)));
		$this->assert_identical(array_keys($math_competence_results), array(1));

		$fetcher = new test1_competence_results_loader(1);
		$fetcher->set_cache_name(null);
		$math_competence_results = $fetcher->get_competence_results();
		$fetch_requests = $fetcher->get_fetch_requests();
		$this->assert_identical($fetch_requests, array(null));
		$this->assert_identical(array_keys($math_competence_results), array(1, 2));
	}

	public function integration_test()
	{
		$fetcher = new test1_competence_results_loader(1);
		$math_competence_results = $fetcher->get_competence_results();
		$this->assert_identical($math_competence_results, array(
			1 => array(
				"marks" => array(
					array("role", 1, 1, 101, 'low', 1011, 3, 1),
					array("role", 2, 2, 201, 'hi', 2011, 3, 2),
					array("competence", 1, 1, 103, 'low', 101, 3, 1),
				),
				"sets" => array(
					1 => array(
						"marks" => array(
							101 => array(
								"marks" => array(
									array(0, true, false),
									array(2, true, false),
								),
								"result" => 2.2,
								"rater_count" => 1,
							),
						),
						"ready" => true,
					),
					2 => array(
						"marks" => array(
							201 => array(
								"marks" => array(
									array(1, true, false),
								),
							),
							202 => array(
								"marks" => array(
									array(1, false, false),
								),
							),
						),
					),
				),
				"store" => true,
			),
			2 => array(
				"marks" => array(
					array("role", 1, 1, 101, 'low', 1011, 3, 1),
				),
				"sets" => array(
					1 => array(
						"marks" => array(
							101 => array(
								"marks" => array(
									[0, true, false],
								),
								"result" => 1,
								"rater_count" => 1,
							),
						),
						"ready" => true,
					),
				),
				"store" => true,
			),
		));
	}

}

?>