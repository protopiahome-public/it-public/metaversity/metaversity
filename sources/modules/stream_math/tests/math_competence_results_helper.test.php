<?php

class math_competence_results_helper_test extends base_test
{

	public function set_up()
	{
		
	}

	public function calc_competence_results_zero_mark_test()
	{
		$math_competence_results = array(
			1 => array(
				"marks" => array(
					array("role", 1, 1, 1, 1.0, 1, 0, 1),
				),
				"sets" => array(
					1 => array(
						"marks" => array(
							444 => array(
								"marks" => array(
									array(0, false, false),
								),
							),
						),
					),
				),
			),
		);
		$expected_result = $math_competence_results;
		$expected_result[1]["sets"][1]["marks"][444]["result"] = 0.0;
		math_competence_results_helper::test_calc_competence_results($math_competence_results, 1);
		$this->assert_identical($math_competence_results, $expected_result);
	}

	public function get_competence_result_test()
	{
		$real_value = math_competence_results_helper::test_get_competence_result(array(
				1 => array("count" => 1, "roles" => 1),
		));
		$this->assert_identical($real_value, 1 / 5);
	}

	public function get_competence_result_2_test()
	{
		$real_value = math_competence_results_helper::test_get_competence_result(array(
				0 => array("count" => 1, "roles" => 1),
		));
		$this->assert_identical($real_value, 0.0);
	}

}

?>