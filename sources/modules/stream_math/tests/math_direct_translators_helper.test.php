<?php

class math_direct_translators_helper_test extends base_test
{

	public function set_up()
	{
		competence_direct_translators_cache::init(1)->set(array(
			102 => array(
				array(2, 202),
			),
			103 => array(
				array(2, 203),
				array(3, 303),
				array(4, 403),
			),
		));
	}

	public function calc_integral_competences_test()
	{
		$math_competence_results = $this->val();
		$correct_value = $math_competence_results;
		$correct_value[1]["sets"][1]["marks"][102]["marks"] = array(
			array(15, true, false),
			//array(16, false, false), // $is_imported
			array(3, true, false),
			array(0, true, true),
		);
		$correct_value[1]["sets"][1]["marks"][103] = array();
		$correct_value[1]["sets"][1]["marks"][103]["marks"] = array(
			array(16, true, false),
		);
		math_direct_translators_helper::apply_direct_translators($math_competence_results, 1);
		$this->assert_identical($math_competence_results, $correct_value);
	}

	public function ready_check_test()
	{
		$math_competence_results = $this->val();
		$math_competence_results[1]["sets"][1]["ready"] = true;
		$correct_value = $math_competence_results;
		math_integral_competences_helper::calc_integral_competences($math_competence_results, 1);
		$this->assert_identical($math_competence_results, $correct_value);
	}

	private function val()
	{
		return array(
			1 => array(
				"marks" => array("..."),
				"sets" => array(
					1 => array(
						"marks" => array(
							101 => array(
								"marks" => array(
									array(0, true, false),
									array(2, false, true),
								),
							),
							102 => array(
								"marks" => array(
									array(0, true, true),
									array(3, true, true),
								),
							),
						),
					),
					2 => array(
						"marks" => array(
							201 => array(
								"marks" => array(
								),
							),
							202 => array(
								"marks" => array(
									array(15, true, false),
									array(16, false, true),
									array(3, false, false),
								),
							),
							203 => array(
								"marks" => array(
									array(16, true, false),
								),
							),
						),
					),
					3 => array(
						"marks" => array(
							301 => array(
								"marks" => array(
								),
							),
						),
					),
				),
			),
		);
	}
	
	public function get_direct_translators_data_grouped_by_competence_set_test()
	{
		$real_value = math_direct_translators_helper::get_direct_translators_data_grouped_by_competence_set(1);
		$this->assert_identical($real_value, array(
			2 => array(
				102 => array(202),
				103 => array(203),
			),
			3 => array(
				103 => array(303),
			),
			4 => array(
				103 => array(403),
			),
		));
	}
	
	public function get_direct_translators_data_grouped_by_competence_set_2_test()
	{
		competence_direct_translators_cache::init(1)->set(array(
			102 => array(
				array(2, 202),
				array(2, 2029999),
			),
			103 => array(
				array(2, 203),
				array(3, 303),
				array(4, 403),
			),
		));
		
		$real_value = math_direct_translators_helper::get_direct_translators_data_grouped_by_competence_set(1);
		$this->assert_identical($real_value, array(
			2 => array(
				102 => array(202, 2029999),
				103 => array(203),
			),
			3 => array(
				103 => array(303),
			),
			4 => array(
				103 => array(403),
			),
		));
	}

}

?>