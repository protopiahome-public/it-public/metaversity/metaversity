<?php

class stream_admin_import_users_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
	);
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $data;

	public function start()
	{
		if (!$this->stream_obj->users_import_allowed())
		{
			return false;
		}
		return true;
	}

	public function check()
	{
		if (!is_null(POST("cancel")))
		{
			$this->pass_info->dump_vars(true);
			return false;
		}
		$this->data = stream_admin_import_users_helper::parse(POST("data"));
		stream_admin_import_users_helper::check($this->data, $this->stream_id);
		if (is_null(POST("force")))
		{
			$this->pass_info->dump_vars(true);
			if (sizeof($this->data))
			{
				$this->pass_info->write_info("FORCE_TO_SUBMIT");
				global $db_xml_converter;
				$db_xml_converter->build_xml($this->pass_info->get_xdom(), $this->data, false, "user");
			}
			return false;
		}
		return true;
	}

	public function commit()
	{
		return stream_admin_import_users_helper::apply($this->data, $this->stream_id, $this->pass_info);
	}
	
}

?>