<?php

class stream_admin_activity_edit_moderators_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
	);
	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $dt_name = "activity";
	protected $axis_name = "edit_moderators";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
		return true;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}

}

?>