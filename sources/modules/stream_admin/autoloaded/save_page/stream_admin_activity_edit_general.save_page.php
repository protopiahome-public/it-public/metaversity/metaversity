<?php

class stream_admin_activity_edit_general_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"stream_admin_activity_save_groups",
		"activity_calc",
	);
	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $dt_name = "activity";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function on_before_start()
	{
		$id = REQUEST("id");
		if (is_good_id($id))
		{
			if (!activity_edit_helper::can_edit_format($id))
			{
				$this->axis_name = "edit_without_format";
			}
		}
		return true;
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
		return true;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}

	public function on_before_commit()
	{
		if (POST("date_type") == "deadline")
		{
			$this->update_array["finish_time"] = "'0000-00-00 00:00:00'";
		}
		return true;
	}

	public function clean_cache()
	{
		activity_cache_tag::init($this->id)->update();
		stream_activities_cache_tag::init($this->stream_id)->update();
		stream_roles_cache_tag::init($this->stream_id)->update();
	}

}

?>