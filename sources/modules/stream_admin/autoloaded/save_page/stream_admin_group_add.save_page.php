<?php

class stream_admin_group_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_groups_clean_cache",
		"title_mdash",
	);
	// Settings
	/**
	 * @var group_dt
	 */
	protected $dt;
	protected $dt_name = "group";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_id($this->stream_id);
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["stream_id"] = $this->stream_id;
		return true;
	}

	public function on_after_commit()
	{
		stream_calc_helper::update_stream_group_count_calc($this->stream_id);
		if ($this->updated_db_row["study_level_id"])
		{
			stream_calc_helper::update_study_level_group_count_calc($this->updated_db_row["study_level_id"]);
		}
		if ($this->updated_db_row["city_id"])
		{
			stream_calc_helper::update_city_group_count_calc($this->updated_db_row["city_id"]);
		}
	}

}

?>