<?php

class stream_admin_translator_edit_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_admin_translator_common",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $eq_competence_set_id;
	protected $translator_is_added = true;
	protected $direct = array();
	protected $formulae = array();

	public function check()
	{
		list($this->direct, $this->formulae, $errors) = competence_translator_helper::parse_text($this->competence_set_id, $this->eq_competence_set_id, POST("text"), POST("skip_left_aliens") === "1", POST("skip_direct_right_aliens") === "1", POST("auto_add_via_titles") === "1");
		if (!empty($errors))
		{
			$errors_xdom = $this->pass_info->get_xdom();
			foreach ($errors as $line_num => $error)
			{
				$errors_xdom->create_child_node("error", isset($error[1]) ? $error[1] : "")
					->set_attr("name", "INVALID_FORMAT")
					->set_attr("descr", $error[0])
					->set_attr("line_num", $line_num);
			}
			$this->pass_info->dump_vars();
			return false;
		}
		return true;
	}

	public function commit()
	{
		competence_translator_helper::clean_competences($this->competence_set_id, $this->eq_competence_set_id);

		// direct
		foreach ($this->direct as &$row)
		{
			$row["competence_set_id"] = $this->competence_set_id;
			$row["eq_competence_set_id"] = $this->eq_competence_set_id;
		}
		unset($row);
		if (sizeof($this->direct))
		{
			$row1 = reset($this->direct);
			$this->db->multi_insert("competence_translation_direct", array_keys($row1), $this->direct);
		}

		// formulae
		foreach ($this->formulae as &$row)
		{
			$row["competence_set_id"] = $this->competence_set_id;
			$row["eq_competence_set_id"] = $this->eq_competence_set_id;
		}
		unset($row);
		if (sizeof($this->formulae))
		{
			$row1 = reset($this->formulae);
			$this->db->multi_insert("competence_translation_formulae", array_keys($row1), $this->formulae);
		}

		// stat
		$this->db->sql("
			UPDATE competence_translator
			SET 
				direct_count_calc = " . sizeof($this->direct) . ",
				formulae_count_calc = " . sizeof($this->formulae) . "
			WHERE competence_set_id = {$this->competence_set_id}
				AND eq_competence_set_id = {$this->eq_competence_set_id}
		");

		$this->pass_info->write_info("SAVED");
		return true;
	}

	public function clean_cache()
	{
		competence_set_translators_cache_tag::init($this->competence_set_id)->update();
	}

}

?>