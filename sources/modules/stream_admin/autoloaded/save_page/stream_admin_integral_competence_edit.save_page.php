<?php

class stream_admin_integral_competence_edit_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_admin_integral_competence_common",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $competence_id = 0;
	protected $competence_integral_state = "1";
	protected $marks = array();

	public function on_after_start()
	{
		$ids = $this->db->fetch_column_values("
			SELECT id
			FROM competence_full
			WHERE is_deleted = 0
				AND competence_set_id = {$this->competence_set_id}
		");
		$marks_raw = POST_AS_ARRAY("marks");
		$including_competences_index = integral_competences_helper::get_all_including_competences_index($this->competence_set_id, $this->competence_id);
		foreach ($ids as $id)
		{
			if ($id == $this->competence_id)
			{
				continue;
			}
			if (!isset($marks_raw[$id]) || !is_good_num($marks_raw[$id]))
			{
				continue;
			}
			if (!in_array($marks_raw[$id], integral_competences_helper::get_possible_marks()))
			{
				continue;
			}
			if ($marks_raw[$id] < 1)
			{
				continue;
			}
			if (isset($including_competences_index[$id]))
			{
				continue;
			}
			$this->marks[] = array(
				"competence_id" => $this->competence_id,
				"included_competence_id" => $id,
				"mark" => $marks_raw[$id],
			);
		}
		return true;
	}

	public function commit()
	{
		if ($this->competence_id)
		{
			$this->db->sql("DELETE FROM integral_competence WHERE competence_id = {$this->competence_id}");
		}
		if (sizeof($this->marks))
		{
			$row1 = reset($this->marks);
			$this->db->multi_insert("integral_competence", array_keys($row1), $this->marks);
		}
		$this->pass_info->write_info("SAVED");
		return true;
	}

	public function on_after_commit()
	{
		$this->db->sql("
			UPDATE competence_full
			SET 
				is_integral = 1, 
				integral_included_competence_count_calc = " . sizeof($this->marks) . "
			WHERE id = {$this->competence_id}
		");
		return true;
	}

}

?>