<?php

class stream_admin_news_item_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
	);
	// Settings
	/**
	 * @var news_item_dt
	 */
	protected $dt;
	protected $dt_name = "news_item";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["stream_id"] = $this->stream_id;
		$this->update_array["adder_user_id"] = $this->user->get_user_id();
		return true;
	}
	
	public function clean_cache()
	{
		stream_news_cache_tag::init($this->stream_id)->update();
	}

}

?>