<?php

class stream_admin_member_edit_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
	);
	// Settings
	protected $dt_name = "stream_user_link";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $user_id;

	/**
	 * @var stream_access
	 */
	protected $stream_access;

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_id($this->stream_id);
		return true;
	}

	public function on_after_start()
	{
		$this->user_id = $this->id["user_id"];
		$this->stream_access = new stream_access($this->stream_obj, $this->user_id);
		if (!$this->stream_access->is_member() and !$this->stream_access->is_pretender_strict())
		{
			return false;
		}
		return true;
	}
	
	public function on_after_commit()
	{
		stream_calc_helper::update_group_user_count_calc($this->stream_id);
	}
	
	public function clean_cache()
	{
		stream_user_cache_tag::init($this->stream_id, $this->user_id)->update();
	}

}

?>