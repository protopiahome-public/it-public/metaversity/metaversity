<?php

class stream_admin_position_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_positions_clean_cache",
	);
	// Settings
	/**
	 * @var position_dt
	 */
	protected $dt;
	protected $dt_name = "position";
	protected $axis_name = "edit";
	// Interenal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	
	/**
	 * @var rate_save_ctrl
	 */
	protected $rate_save_ctrl;
	
	public function init()
	{
		$this->save_loader->add_ctrl($this->rate_save_ctrl = new rate_save_ctrl($this->dt_name, array(0, 1, 2, 3)));
	}

	public function on_before_start()
	{
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		return true;
	}
	
	protected function on_after_dt_init()
	{
		$this->dt->set_stream_id($this->stream_id);
		return true;
	}

	public function on_after_start()
	{
		$this->rate_save_ctrl->set_rate_id(0);
		$this->rate_save_ctrl->set_competence_set_id($this->competence_set_id);
		return true;
	}
	
	public function on_before_commit()
	{
		$this->update_array["stream_id"] = $this->stream_id;
		return true;
	}

	public function on_after_commit()
	{
		$this->rate_save_ctrl->set_object_id($this->last_id);
	}
	
}

?>