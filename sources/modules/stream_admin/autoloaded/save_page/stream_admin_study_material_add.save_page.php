<?php

class stream_admin_study_material_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"stream_study_materials_clean_cache",
	);
	// Settings
	/**
	 * @var study_material_dt
	 */
	protected $dt;
	protected $dt_name = "study_material";
	protected $axis_name = "edit";
	// Internal
	protected $outer;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;

	/**
	 * @var rate_save_ctrl
	 */
	protected $rate_save_ctrl;

	public function init()
	{
		$this->outer = POST("outer") === "1";
		$this->save_loader->add_ctrl($this->rate_save_ctrl = new rate_save_ctrl($this->dt_name, array(0, 1, 2)));
	}

	public function on_before_start()
	{
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		if ($this->outer)
		{
			$this->axis_name = "edit_outer";
		}
		return true;
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
		return true;
	}

	public function on_after_start()
	{
		$this->rate_save_ctrl->set_rate_id(0);
		$this->rate_save_ctrl->set_competence_set_id($this->competence_set_id);
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["stream_id"] = $this->stream_id;
		if ($this->outer)
		{
			$this->update_array["is_outer"] = "1";
		}
		return true;
	}

	public function on_after_commit()
	{
		$this->rate_save_ctrl->set_object_id($this->last_id);
	}

}

?>