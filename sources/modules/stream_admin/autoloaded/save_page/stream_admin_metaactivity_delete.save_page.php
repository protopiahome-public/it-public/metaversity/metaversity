<?php

class stream_admin_metaactivity_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
	);
	// Settings
	protected $db_table = "metaactivity";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}
	
	public function clean_cache()
	{
		stream_metaactivities_cache_tag::init($this->stream_id)->update();
	}
	
}

?>