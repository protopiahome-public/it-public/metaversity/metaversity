<?php

class stream_admin_moderators_save_page extends base_admin_moderators_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
	);
	// Settings
	protected $do_member_check = true;
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function get_access_save($user_id)
	{
		return new stream_access_save($this->stream_obj, $user_id);
	}

	protected function get_rights_array()
	{
		return array();
	}

}

?>