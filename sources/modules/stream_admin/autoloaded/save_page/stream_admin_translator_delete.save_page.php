<?php

class stream_admin_translator_delete_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_admin_translator_common",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $eq_competence_set_id;
	protected $translator_is_added = true;

	public function commit()
	{
		if (POST("cancel") !== null)
		{
			return false;
		}
		competence_translator_helper::clean_competences($this->competence_set_id, $this->eq_competence_set_id);
		$this->db->sql("
			DELETE FROM competence_translator
			WHERE competence_set_id = {$this->competence_set_id}
				AND eq_competence_set_id = {$this->eq_competence_set_id}
		");
		$this->pass_info->write_info("DELETED");
		return true;
	}

	public function clean_cache()
	{
		competence_set_translators_cache_tag::init($this->competence_set_id)->update();
	}

}

?>