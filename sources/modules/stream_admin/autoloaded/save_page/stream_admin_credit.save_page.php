<?php

class stream_admin_credit_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"position_before_start",
		"user_before_start",
	);
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $position_id;
	protected $position_row;
	protected $user_id;
	protected $user_row;
	protected $comment;
	protected $is_deleted;

	public function start()
	{
		$this->comment = trim(POST("comment") ? : "");
		$this->is_deleted = POST("is_deleted") === "1";
		return true;
	}

	public function check()
	{
		if (!$this->is_deleted && strlen($this->comment) === 0)
		{
			$this->pass_info->write_error("EMPTY_COMMENT");
			$this->pass_info->dump_vars();
			return false;
		}
		return true;
	}

	public function commit()
	{
		position_credit_helper::change_credit($this->position_id, $this->user_id, $this->comment, $this->is_deleted);
		return true;
	}

}

?>