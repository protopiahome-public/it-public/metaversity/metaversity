<?php

class stream_admin_integral_competence_delete_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_admin_integral_competence_common",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $competence_id = 0;
	protected $competence_integral_state = "1";

	public function commit()
	{
		if (POST("cancel") !== null)
		{
			return false;
		}
		$this->db->sql("
			DELETE FROM integral_competence
			WHERE competence_id = {$this->competence_id}
		");
		$this->db->sql("
			UPDATE competence_full
			SET 
				is_integral = 0, 
				integral_included_competence_count_calc = 0
			WHERE id = {$this->competence_id}
		");
		$this->pass_info->write_info("DELETED");
		return true;
	}

}

?>