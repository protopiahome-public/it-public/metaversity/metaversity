<?php

class stream_admin_activity_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"stream_admin_activity_save_groups",
		"activity_calc",
	);
	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $dt_name = "activity";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["stream_id"] = $this->stream_id;
		return true;
	}

	public function on_after_commit()
	{
		$this->db->sql("
			INSERT INTO activity_participants_subscription (activity_id, user_id)
			VALUES ({$this->last_id}, {$this->user->get_user_id()})
		");
		return true;
	}

}

?>