<?php

class stream_admin_general_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"stream_edit_before_start",
		"stream_admin_check_rights",
		"competence_set_update_title_after_stream_edit",
	);
	// Settings
	protected $dt_name = "stream";
	protected $axis_name = "edit_general";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function on_after_commit()
	{
		stream_calc_helper::update_stream_city_count_calc($this->stream_id);
		stream_calc_helper::update_city_stream_count_calc();
		return true;
	}

	public function clean_cache()
	{
		stream_cache_tag::init($this->id)->update();
	}

}

?>