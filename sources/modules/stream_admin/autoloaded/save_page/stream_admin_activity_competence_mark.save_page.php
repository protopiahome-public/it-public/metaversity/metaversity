<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class stream_admin_activity_competence_mark_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"activity_before_start",
		"user_before_start",
		"stream_competence_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $activity_row;
	protected $user_id;
	protected $user_row;
	protected $competence_id;
	protected $competence_row;
	protected $city_id;
	protected $mark;
	protected $comment;

	/**
	 * @var stream_access
	 */
	protected $participant_stream_access;

	public function start()
	{
		$this->participant_stream_access = new stream_access($this->stream_obj, $this->user_id);
		if (!$this->participant_stream_access->is_member())
		{
			return false;
		}
		$this->mark = POST("mark");
		if (!activity_marks_helper::is_good_competence_mark($this->mark))
		{
			return false;
		}
		$this->comment = mb_substr(POST("comment"), 0, 16535);
		return true;
	}

	public function check()
	{
		if ($this->mark == 3 and strlen(trim($this->comment)) === 0)
		{
			$this->pass_info->write_error("EMPTY_COMMENT");
			$this->pass_info->dump_vars();
			return false;
		}
		return true;
	}

	public function commit()
	{
		activity_marks_helper::change_competence_mark($this->activity_id, $this->user_id, $this->competence_id, $this->stream_obj->get_competence_set_id(), $this->mark, $this->comment);
		return true;
	}

	public function clean_cache()
	{
		stream_marks_cache_tag::init($this->stream_obj->get_id())->update();
		user_marks_cache_tag::init($this->user_id)->update();
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

}

?>