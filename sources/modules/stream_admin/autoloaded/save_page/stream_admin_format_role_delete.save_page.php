<?php

class stream_admin_format_role_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"stream_format_clean_cache",
	);
	// Settings
	protected $db_table = "role";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $format_id;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}
	
	public function on_after_start()
	{
		$this->format_id = $this->old_db_row["format_id"];
		return true;
	}
	
	public function on_after_commit()
	{
		stream_calc_helper::update_format_role_count_calc($this->format_id);
	}
	
	public function clean_cache()
	{
		role_cache_tag::init($this->id)->update();
	}
	
}

?>