<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class stream_admin_activity_edit_roles_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"activity_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $activity_row;
	protected $data = array();

	public function check_rights()
	{
		return !activity_helper::is_longterm($this->activity_row);
	}

	public function check()
	{
		$roles = $this->db->fetch_all("SELECT * FROM role WHERE format_id = {$this->activity_row["format_id"]}", "id");
		$total_number_array = POST_AS_ARRAY("total_number");
		$descr_text_array = POST_AS_ARRAY("descr_text");
		$enabled_array = POST_AS_ARRAY("enabled");
		$auto_accept_array = POST_AS_ARRAY("auto_accept");
		foreach ($total_number_array as $role_id => $total_number)
		{
			if (!is_good_id($role_id) or ! isset($roles[$role_id]))
			{
				continue;
			}
			if (!is_good_num($total_number))
			{
				continue;
			}
			if ($total_number > 999)
			{
				$total_number = 999;
			}
			$enabled = isset($enabled_array[$role_id]) && $enabled_array[$role_id] === "1";
			$auto_accept = isset($auto_accept_array[$role_id]) && $auto_accept_array[$role_id] === "1";
			$descr_text = isset($descr_text_array[$role_id]) && is_string($descr_text_array[$role_id]) ? $descr_text_array[$role_id] : "";
			$descr_html = text_processor::intmarkup_process($descr_text);
			$data = array(
				"activity_id" => $this->activity_id,
				"role_id" => $role_id,
				"enabled" => (int) $enabled,
				"auto_accept" => (int) $auto_accept,
				"total_number" => $total_number,
			);
			$data["descr_text"] = "'" . $this->db->escape($descr_text) . "'";
			$data["descr_html"] = "'" . $this->db->escape($descr_html) . "'";
			$this->data[] = $data;
		}
		return true;
	}

	public function commit()
	{
		foreach ($this->data as $data)
		{
			$this->db->insert_by_array("activity_role_link", $data, true);
		}
		$this->pass_info->write_info("SAVED");
		return true;
	}

	public function clean_cache()
	{
		activity_cache_tag::init($this->activity_id)->update();
		stream_activities_cache_tag::init($this->stream_id)->update();
	}

}

?>