<?php

class stream_admin_integral_competence_add_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_admin_integral_competence_common",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $competence_id = 0;
	protected $competence_integral_state = "0";

	public function commit()
	{
		$this->db->sql("
			UPDATE competence_full
			SET 
				is_integral = 1, 
				integral_included_competence_count_calc = 0
			WHERE id = {$this->competence_id}
		");
		$this->pass_info->write_info("ADDED");
		return true;
	}

	public function on_after_commit()
	{
		$redirect_url = POST("retpath");
		if (strpos($redirect_url, "%ID%"))
		{
			$redirect_url = str_replace("%ID%", $this->competence_id, $redirect_url);
			$this->set_redirect_url($redirect_url);
		}
	}

}

?>