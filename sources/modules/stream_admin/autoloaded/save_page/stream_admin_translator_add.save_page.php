<?php

class stream_admin_translator_add_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_admin_translator_common",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $eq_competence_set_id;
	protected $translator_is_added = false;

	public function commit()
	{
		$this->db->sql("
			INSERT INTO competence_translator
				(competence_set_id, eq_competence_set_id)
			VALUES
				({$this->competence_set_id}, {$this->eq_competence_set_id})
		");
		competence_translator_helper::clean_competences($this->competence_set_id, $this->eq_competence_set_id);
		$this->pass_info->write_info("ADDED");
		return true;
	}

	public function on_after_commit()
	{
		$redirect_url = POST("retpath");
		if (strpos($redirect_url, "%ID%"))
		{
			$redirect_url = str_replace("%ID%", $this->eq_competence_set_id, $redirect_url);
			$this->set_redirect_url($redirect_url);
		}
	}

	public function clean_cache()
	{
		competence_set_translators_cache_tag::init($this->competence_set_id)->update();
	}

}

?>