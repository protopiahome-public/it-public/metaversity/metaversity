<?php

class stream_admin_subject_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
	);
	// Settings
	protected $db_table = "subject";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}

}

?>