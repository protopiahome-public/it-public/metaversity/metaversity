<?php

class stream_admin_subject_add_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
	);
	// Settings
	protected $dt_name = "subject";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function on_before_commit()
	{
		$this->update_array["stream_id"] = $this->stream_id;
		return true;
	}

}

?>