<?php

class stream_admin_group_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_rights",
		"stream_groups_clean_cache",
	);
	// Settings
	protected $db_table = "group";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}

	public function on_after_commit()
	{
		stream_calc_helper::update_stream_group_count_calc($this->stream_id);
		if ($this->old_db_row["study_level_id"])
		{
			stream_calc_helper::update_study_level_group_count_calc($this->old_db_row["study_level_id"]);
		}
		if ($this->old_db_row["city_id"])
		{
			stream_calc_helper::update_city_group_count_calc($this->old_db_row["city_id"]);
		}
		return true;
	}

}

?>