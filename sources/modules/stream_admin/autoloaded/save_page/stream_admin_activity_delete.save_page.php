<?php

class stream_admin_activity_delete_save_page extends base_delete_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
	);
	// Settings
	protected $db_table = "activity";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $affected_users;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}

	public function on_before_commit()
	{
		$this->affected_users = $this->db->fetch_column_values("
			SELECT DISTINCT user_id
			FROM activity_participant_mark
			WHERE activity_id = {$this->id}
		");
		return true;
	}

	public function on_after_commit()
	{
		activity_calc_helper::rebuild_activity_calc($this->id);
		foreach ($this->affected_users as $user_id)
		{
			stream_calc_helper::update_stream_user_link_mark_count_calc($this->stream_id, $user_id);
			stream_calc_helper::update_user_mark_count_calc($user_id);
		}
	}

	public function clean_cache()
	{
		activity_cache_tag::init($this->id)->update();
		stream_activities_cache_tag::init($this->stream_id)->update();
		stream_roles_cache_tag::init($this->stream_id)->update();
	}

}

?>