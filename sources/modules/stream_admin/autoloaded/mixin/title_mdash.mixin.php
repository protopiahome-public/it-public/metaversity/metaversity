<?php

class title_mdash_mixin extends base_mixin
{

	protected $last_id;
	protected $updated_db_row;

	/**
	 * @var base_dt
	 */
	protected $dt;

	public function on_after_commit()
	{
		if (isset($this->updated_db_row["title"]))
		{
			$title = $this->updated_db_row["title"];
			$title = preg_replace("/\s+[-–—]\s+/u", "–", $title);
			$title = preg_replace("/(\d)\s*[-–—]\s*/u", "\\1–", $title);
			$title = preg_replace("/\s*[-–—]\s*(\d)/u", "–\\1", $title);
			$title_escaped = $this->db->escape($title);
			$this->db->sql("
				UPDATE `{$this->dt->get_db_table()}`
				SET title = '{$title_escaped}'
				WHERE id = {$this->last_id}
			");
		}
		return true;
	}

}

?>