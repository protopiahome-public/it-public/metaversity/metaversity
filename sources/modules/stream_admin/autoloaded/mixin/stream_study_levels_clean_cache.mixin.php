<?php

class stream_study_levels_clean_cache_mixin extends base_mixin
{

	protected $stream_id;
	
	public function clean_cache()
	{
		stream_study_levels_cache_tag::init($this->stream_id)->update();
		streams_check_list_cache::init()->delete();
	}

}

?>