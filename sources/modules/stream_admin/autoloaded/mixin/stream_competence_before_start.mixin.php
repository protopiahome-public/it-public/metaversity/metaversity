<?php

class stream_competence_before_start_mixin extends base_mixin
{

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_id;
	protected $competence_row;

	public function on_before_start()
	{
		$this->competence_id = POST("competence_id");
		if (!is_good_id($this->competence_id))
		{
			return false;
		}
		$this->competence_row = $this->db->sql("
			SELECT * 
			FROM competence_full 
			WHERE id = {$this->competence_id} 
				AND competence_set_id = {$this->stream_obj->get_competence_set_id()}
				AND is_deleted = 0
			LOCK IN SHARE MODE
		");
		if (!$this->competence_row)
		{
			return false;
		}
		return true;
	}

}

?>