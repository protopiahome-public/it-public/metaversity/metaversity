<?php

class stream_format_clean_cache_mixin extends base_mixin
{

	protected $stream_id;
	protected $format_id;

	public function clean_cache()
	{
		format_cache_tag::init($this->format_id)->update();
		stream_formats_cache_tag::init($this->stream_id)->update();
		stream_roles_cache_tag::init($this->stream_id)->update();
	}

}

?>