<?php

class stream_admin_check_rights_mixin extends base_mixin
{

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	/**
	 * @var stream_access
	 */
	protected $stream_access;

	public function check_rights()
	{
		$this->stream_access = $this->stream_obj->get_access();
		return $this->stream_access->has_admin_rights();
	}

}

?>