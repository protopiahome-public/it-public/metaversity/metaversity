<?php

class stream_admin_translator_common_mixin extends base_mixin
{

	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $eq_competence_set_id;
	protected $translator_is_added;

	public function on_before_start()
	{
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		return true;
	}

	public function start()
	{
		$this->eq_competence_set_id = POST("eq_competence_set_id");
		if (!is_good_id($this->eq_competence_set_id))
		{
			return false;
		}
		if (!$this->db->row_exists("SELECT id FROM competence_set WHERE id = {$this->eq_competence_set_id}"))
		{
			return false;
		}
		$translator_is_added = competence_translator_helper::translator_exists($this->competence_set_id, $this->eq_competence_set_id);
		if ($translator_is_added !== $this->translator_is_added)
		{
			return false;
		}
		return true;
	}

	public function clean_cache()
	{
		
	}

}

?>