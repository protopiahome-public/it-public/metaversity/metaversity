<?php

class stream_groups_clean_cache_mixin extends base_mixin
{

	protected $stream_id;

	public function clean_cache()
	{
		stream_groups_cache_tag::init($this->stream_id)->update();
		streams_check_list_cache::init()->delete();
	}

}

?>