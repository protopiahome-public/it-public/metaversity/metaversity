<?php

class stream_study_materials_clean_cache_mixin extends base_mixin
{

	protected $stream_id;

	public function clean_cache()
	{
		stream_study_materials_cache_tag::init($this->stream_id)->update();
	}

}

?>