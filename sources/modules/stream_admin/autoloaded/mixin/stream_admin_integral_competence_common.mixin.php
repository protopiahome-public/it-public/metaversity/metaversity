<?php

class stream_admin_integral_competence_common_mixin extends base_mixin
{

	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $competence_id;
	protected $competence_integral_state;

	public function on_before_start()
	{
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		return true;
	}

	public function start()
	{
		$this->competence_id = POST("competence_id");
		if (!is_good_id($this->competence_id))
		{
			return false;
		}
		if (!$this->db->row_exists("
			SELECT *
			FROM competence_full
			WHERE id = {$this->competence_id}
				AND is_deleted = 0
				AND competence_set_id = {$this->competence_set_id}
				AND is_integral = {$this->competence_integral_state}
		"))
		{
			return false;
		}
		return true;
	}

	public function clean_cache()
	{
		competence_set_translators_cache_tag::init($this->competence_set_id)->update();
	}

}

?>