<?php

class stream_admin_activity_save_groups_mixin extends base_mixin
{

	protected $last_id;
	protected $stream_id;
	protected $updated_db_row;

	public function on_after_commit()
	{
		$city_id = $this->updated_db_row["city_id"];
		$city_sql = $city_id ? " AND city_id = {$city_id}" : "";
		$study_level_id = $this->updated_db_row["study_level_id"];
		$study_level_sql = $study_level_id ? " AND (study_level_id IS NULL OR study_level_id = {$study_level_id})" : "";
		$groups = $this->db->fetch_column_values("
			SELECT id
			FROM `group`
			WHERE stream_id = {$this->stream_id}
			{$city_sql}
			{$study_level_sql}
		", "id");
		$groups[] = 0;
		$this->db->sql("
			DELETE FROM activity_group_link
			WHERE activity_id = {$this->last_id}
				AND group_id NOT IN (" . join(", ", $groups) . ")
		");
		stream_calc_helper::update_activity_group_count_calc($this->last_id);
	}

}

?>