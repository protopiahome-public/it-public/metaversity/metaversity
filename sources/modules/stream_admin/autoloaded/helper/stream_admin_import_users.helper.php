<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class stream_admin_import_users_helper extends base_static_db_helper
{

	const MAX_USER_COUNT = 3000;
	const ROW_STATUS_PASS = "PASS";
	const ROW_STATUS_ERR_BAD_EMAIL = "ERR_BAD_EMAIL";
	const ROW_STATUS_TO_REG = "TO_REG";
	const ROW_STATUS_TO_JOIN = "TO_JOIN";

	public static function parse($data_str)
	{
		$lines = explode("\n", $data_str, self::MAX_USER_COUNT + 1);
		$lines = array_slice($lines, 0, self::MAX_USER_COUNT);
		$result = array();
		foreach ($lines as $line)
		{
			$line = trim($line);
			if (!strlen($line))
			{
				continue;
			}
			$parts = explode("\t", $line, 7);
			$email = self::prepare_str($parts[0], 100);
			if (!strlen($email))
			{
				continue;
			}
			$result[$email] = array(
				"status" => self::ROW_STATUS_PASS,
				"email" => $email,
				"first_name" => isset($parts[1]) ? mb_convert_case(self::prepare_str($parts[1], 60), MB_CASE_TITLE) : "",
				"last_name" => isset($parts[2]) ? mb_convert_case(self::prepare_str($parts[2], 60), MB_CASE_TITLE) : "",
				"login" => isset($parts[3]) ? self::prepare_str($parts[3], 40) : "",
				"imported_foreign_id" => isset($parts[4]) && is_good_id($parts[4]) ? (int) $parts[4] : null,
			);
		}
		return $result;
	}

	public static function check(&$data, $stream_id)
	{
		self::check_emails($data);
		self::check_logins($data);
		self::check_registered($data);
		self::check_joined($data, $stream_id);
	}

	private static function check_emails(&$data)
	{
		foreach ($data as $email => &$user)
		{
			if (strpos($email, "@") === false or strpos($email, " ") !== false)
			{
				$user["status"] = self::ROW_STATUS_ERR_BAD_EMAIL;
				continue;
			}
		}
		unset($user);
	}

	private static function check_logins(&$data)
	{
		foreach ($data as $email => &$user)
		{
			$user["login"] = self::preprocess_login($user["login"]);
			if (!self::is_good_login($user["login"]))
			{
				$user["login"] = "";
			}
			$email_parts = explode("@", $email, 2);
			$login2 = self::preprocess_login($email_parts[0]);
			if (self::is_good_login($login2))
			{
				$user["login2"] = $login2;
			}
		}
		unset($user);
	}

	private static function check_registered(&$data)
	{
		if (!sizeof($data))
		{
			return;
		}
		$emails_escaped = array();
		foreach ($data as $email => $user)
		{
			if ($user["status"] != self::ROW_STATUS_PASS)
			{
				continue;
			}
			$emails_escaped[] = "'" . self::db()->escape($email) . "'";
		}
		$regs = !sizeof($emails_escaped) ? array() : self::db()->fetch_all("
			SELECT id, login, email 
			FROM user
			WHERE email IN (" . join(", ", $emails_escaped) . ")
		", "email");
		foreach ($data as $email => &$user)
		{
			if ($user["status"] != self::ROW_STATUS_PASS)
			{
				continue;
			}
			if (isset($regs[$email]))
			{
				$user["id"] = $regs[$email]["id"];
				$user["current_login"] = $regs[$email]["login"];
			}
			else
			{
				$user["status"] = self::ROW_STATUS_TO_REG;
			}
		}
		unset($user);
	}

	private static function check_joined(&$data, $stream_id)
	{
		if (!sizeof($data))
		{
			return;
		}
		$user_ids = array();
		foreach ($data as $email => $user)
		{
			if ($user["status"] != self::ROW_STATUS_PASS)
			{
				continue;
			}
			if (isset($user["id"]))
			{
				$user_ids[$user["id"]] = $email;
			}
		}
		$joined = !sizeof($user_ids) ? array() : self::db()->fetch_column_values("
			SELECT user_id
			FROM stream_user_link
			WHERE stream_id = {$stream_id} AND user_id IN (" . join(", ", array_keys($user_ids)) . ")
		", true, "user_id");
		foreach ($user_ids as $id => $email)
		{
			if (!isset($joined[$id]))
			{
				$data[$email]["status"] = self::ROW_STATUS_TO_JOIN;
			}
		}
	}

	public static function apply($data, $stream_id, pass_info $pass_info)
	{
		self::apply_register($data, $stream_id, $pass_info);
		self::apply_join($data, $stream_id, $pass_info);
		$pass_info->write_info("SAVED", sizeof($data));
		return true;
	}

	private static function apply_register(&$data, $stream_id, pass_info $pass_info)
	{
		$logins = self::collect_reg_logins($data);
		$logins_used = self::get_used_logins_as_keys($logins);
		$default_import_city_id = self::db()->get_value("SELECT default_import_city_id FROM sys_params");
		foreach ($data as $email => &$user)
		{
			if ($user["status"] == self::ROW_STATUS_TO_REG)
			{
				if (isset($user["login"]) and strlen($user["login"]) and ! isset($logins_used[mb_strtolower($user["login"])]))
				{
					$login = $user["login"];
				}
				elseif (isset($user["login2"]) and strlen($user["login2"]) and ! isset($logins_used[mb_strtolower($user["login2"])]))
				{
					$login = $user["login2"];
				}
				else
				{
					$login = "[" . substr(sha1($email), 0, 38) . "]";
				}
				$user_data = array(
					"login" => "'" . self::db()->escape($login) . "'",
					"email" => "'" . self::db()->escape($email) . "'",
					"sex" => "'?'",
					"first_name" => "'" . self::db()->escape($user["first_name"]) . "'",
					"last_name" => "'" . self::db()->escape($user["last_name"]) . "'",
					"first_name_en" => "'" . self::db()->escape(text_processor::translate_to_en($user["first_name"])) . "'",
					"last_name_en" => "'" . self::db()->escape(text_processor::translate_to_en($user["last_name"])) . "'",
					"add_time" => "NOW()",
					"edit_time" => "NOW()",
					"imported_by_stream_id" => $stream_id,
					"city_id" => $default_import_city_id,
				);
				if ($user["imported_foreign_id"])
				{
					$user_data["imported_foreign_id"] = $user["imported_foreign_id"];
				}
				self::db()->insert_by_array("user", $user_data);
				$user["id"] = self::db()->get_last_id();
				if (substr($login, 0, 1) === "[")
				{
					$login = "[" . $user["id"] . "]";
					$login_escaped = self::db()->escape($login);
					self::db()->sql("UPDATE user SET login = '{$login_escaped}' WHERE id = {$user["id"]}");
				}
				$login_lower = mb_strtolower($login);
				$logins_used[$login_lower] = true;
				$pass_info->write_info("REGISTERED", $login);
			}
			elseif (isset($user["id"]))
			{
				$user_data = self::db()->get_row("
					SELECT login, first_name, last_name
					FROM user
					WHERE id = {$user["id"]}
				");
				$new_user_data_escaped = array();
				if (!strlen($user_data["first_name"]) && strlen($user["first_name"]))
				{
					$new_user_data_escaped["first_name"] = "'" . self::db()->escape($user["first_name"]) . "'";
					$new_user_data_escaped["first_name_en"] = "'" . self::db()->escape(text_processor::translate_to_en($user["first_name"])) . "'";
				}
				if (!strlen($user_data["last_name"]) && strlen($user["last_name"]))
				{
					$new_user_data_escaped["last_name"] = "'" . self::db()->escape($user["last_name"]) . "'";
					$new_user_data_escaped["last_name_en"] = "'" . self::db()->escape(text_processor::translate_to_en($user["last_name"])) . "'";
				}
				if ($new_user_data_escaped)
				{
					self::db()->update_by_array("user", $new_user_data_escaped, "id = {$user["id"]}");
					user_cache_tag::init($user["id"])->update();
					$pass_info->write_info("UPDATED", $user_data["login"]);
				}
			}
		}
		unset($user);
		user_list_cache_tag::init()->update();
	}

	private static function apply_join($data, $stream_id, pass_info $pass_info)
	{
		$user_ids = array();
		foreach ($data as $email => $user)
		{
			if (isset($user["id"]))
			{
				$user_ids[] = $user["id"];
			}
		}
		$affected_count = 0;
		if ($user_ids)
		{
			self::db()->sql("
				UPDATE stream_user_link
				SET 
					status = 'member',
					edit_time = NOW()
				WHERE stream_id = {$stream_id} 
					AND user_id IN (" . join(", ", $user_ids) . ")
					AND status = 'deleted'
			");
			$affected_count += self::db()->get_affected_row_count();

			$insert_sql = array();
			foreach ($user_ids as $user_id)
			{
				$insert_sql[] = "({$stream_id}, {$user_id}, 'member', NOW())";
			}
			self::db()->sql("
				INSERT IGNORE INTO stream_user_link
					(stream_id, user_id, status, add_time)
				VALUES
					" . join(", ", $insert_sql) . ";
			");
			$affected_count += self::db()->get_affected_row_count();
		}
		foreach ($user_ids as $user_id)
		{
			stream_user_cache_tag::init($stream_id, $user_id)->update();
		}

		$pass_info->write_info("JOINED", $affected_count);
	}

	private static function collect_reg_logins($data)
	{
		$result = array();
		foreach ($data as $email => $user)
		{
			if ($user["status"] == self::ROW_STATUS_TO_REG)
			{
				if (isset($user["login"]) and strlen($user["login"]))
				{
					$result[] = $user["login"];
				}
				if (isset($user["login2"]) and strlen($user["login2"]))
				{
					$result[] = $user["login2"];
				}
			}
		}
		return $result;
	}

	private static function get_used_logins_as_keys($logins)
	{
		$logins_escaped = array();
		foreach ($logins as $login)
		{
			$logins_escaped[] = "'" . self::db()->escape($login) . "'";
		}
		$result = !sizeof($logins_escaped) ? array() : self::db()->fetch_column_values("
			SELECT LOWER(login) AS login
			FROM user
			WHERE login IN (" . join(", ", $logins_escaped) . ")
		", true, "login");
		return $result;
	}

	private static function preprocess_login($login_raw)
	{
		$login_raw = preg_replace("/[_.-]+/", "-", $login_raw);
		$login_raw = preg_replace("/^-/", "", $login_raw);
		$login_raw = preg_replace("/-$/", "", $login_raw);
		return $login_raw;
	}

	private static function is_good_login($login_raw)
	{
		if (!preg_match("/^[a-zA-Z0-9\-]{4,40}$/", $login_raw))
		{
			return false;
		}
		if (strpos($login_raw, "--") !== false)
		{
			return false;
		}
		if (substr($login_raw, 0, 1) == "-" or substr($login_raw, -1, 1) == "-")
		{
			return false;
		}
		return true;
	}

	private static function prepare_str($str, $max_length)
	{
		return mb_substr(text_processor::clean_string($str), 0, $max_length);
	}

}

?>