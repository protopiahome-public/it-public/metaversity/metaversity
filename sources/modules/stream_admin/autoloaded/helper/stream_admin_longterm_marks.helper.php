<?php

class stream_admin_longterm_marks_helper extends base_static_db_helper
{

	public static function set_times($activity_id, &$start_time, &$finish_time)
	{
		$row = activity_helper::get_row($activity_id);
		if ($row["start_time"] and substr($row["start_time"], 0, 5) !== "0000-")
		{
			$start_time = $row["start_time"];
		}
		if ($row["finish_time"] and substr($row["finish_time"], 0, 5) !== "0000-")
		{
			$finish_time = $row["finish_time"];
		}
		return !!$start_time && !!$finish_time;
	}

	public static function get_user_competence_marks(stream_obj $stream_obj, $user_id, $activity_id, $competence_id = null)
	{
		$news_type_mark = news_helper::TYPE_MARK;
		$competence_where_sql = $competence_id ? "AND acm.competence_id = {$competence_id}" : "";
		$result = self::db()->fetch_all("
			SELECT 
				'{$news_type_mark}' AS 'news_type',
				acm.change_time AS time,
				'competence' AS type, a.stream_id,
				acm.activity_id, acm.competence_id,
				a.weight,
				acm.mark, acm.comment, acm.changer_user_id
			FROM activity_competence_mark acm
			INNER JOIN activity a ON a.id = acm.activity_id
			INNER JOIN competence_calc c ON c.competence_id = acm.competence_id
			INNER JOIN stream_user_link dul ON dul.user_id = acm.user_id AND dul.stream_id = a.stream_id
			WHERE c.competence_set_id = {$stream_obj->get_competence_set_id()}
				AND a.stream_id = {$stream_obj->get_id()}
				AND acm.activity_id = {$activity_id}
				AND dul.user_id = {$user_id}
				{$competence_where_sql}
		", "competence_id");
		return $result;
	}

	public static function get_user_marks(stream_obj $stream_obj, $start_time, $finish_time, $user_id, $competence_id, $activity_id)
	{
		$news_type_mark = news_helper::TYPE_MARK;
		$result = self::db()->fetch_all("
			SELECT 
				'{$news_type_mark}' AS 'news_type',
				apm.change_time AS time,
				'role' AS type, a.stream_id,
				apm.activity_id, apm.role_id,
				a.weight,
				l.mark > 1 AS is_important,
				apm.mark, apm.comment, apm.changer_user_id
			FROM activity_participant_mark apm
			INNER JOIN role r ON r.id = apm.role_id
			INNER JOIN rate_competence_link l ON l.rate_id = r.rate_id
			INNER JOIN activity a ON a.id = apm.activity_id
			INNER JOIN competence_calc c ON c.competence_id = l.competence_id
			INNER JOIN stream_user_link dul ON dul.user_id = apm.user_id AND dul.stream_id = a.stream_id
			WHERE c.competence_set_id = {$stream_obj->get_competence_set_id()}
				AND a.stream_id = {$stream_obj->get_id()}
				AND dul.user_id = {$user_id}
				AND l.competence_id = {$competence_id}
				AND apm.change_time BETWEEN '{$start_time}' AND '{$finish_time}'
			ORDER BY l.mark DESC, apm.mark DESC, apm.change_time DESC
		");
		$competence_mark = self::get_user_competence_marks($stream_obj, $user_id, $activity_id, $competence_id);
		if (sizeof($competence_mark))
		{
			array_unshift($result, reset($competence_mark));
		}
		return $result;
	}

	public static function build_raw_table(stream_obj $stream_obj, $start_time, $finish_time, $user_id = null)
	{
		$user_where_sql = $user_id ? "dul.user_id = {$user_id}" : "dul.status <> 'pretender' AND dul.status <> 'deleted'";
		self::db()->sql("
			CREATE TEMPORARY TABLE longterm_raw
			SELECT 
				apm.user_id,
				l.competence_id,
				apm.mark,
				IF(a.weight = 'hi', 2, 1) AS activity_weight
			FROM activity_participant_mark apm
			INNER JOIN role r ON r.id = apm.role_id
			INNER JOIN rate_competence_link l ON l.rate_id = r.rate_id
			INNER JOIN activity a ON a.id = apm.activity_id
			INNER JOIN competence_calc c ON c.competence_id = l.competence_id
			INNER JOIN stream_user_link dul ON dul.user_id = apm.user_id AND dul.stream_id = a.stream_id
			WHERE c.competence_set_id = {$stream_obj->get_competence_set_id()}
				AND a.stream_id = {$stream_obj->get_id()}
				AND {$user_where_sql}
				AND apm.change_time BETWEEN '{$start_time}' AND '{$finish_time}'
				AND l.mark > 1
				AND apm.mark > 1
			ORDER BY l.mark DESC
		");
	}

	public static function build_user_competence_stat_table()
	{
		self::db()->sql("
			CREATE TEMPORARY TABLE longterm_user_competence_stat
			SELECT 
				user_id, competence_id, 
				SUM(activity_weight) AS total_weight
			FROM longterm_raw
			GROUP BY user_id, competence_id
		");
	}

	public static function build_user_stat_table($min_weight)
	{
		self::db()->sql("
			CREATE TEMPORARY TABLE longterm_user_stat
			SELECT 
				user_id,
				COUNT(*) AS competence_count
			FROM longterm_user_competence_stat
			WHERE total_weight >= {$min_weight}
			GROUP BY user_id
		");
	}

	public static function build_user_competence_marks_stat_table(stream_obj $stream_obj, $activity_id)
	{
		self::db()->sql("
			CREATE TEMPORARY TABLE longterm_user_competence_marks_stat
			SELECT 
				user_id, COUNT(*) AS competence_mark_count
			FROM activity a
			INNER JOIN activity_competence_mark acm ON acm.activity_id = a.id
			INNER JOIN competence_calc c ON c.competence_id = acm.competence_id
			WHERE a.id = {$activity_id}
				AND c.competence_set_id = {$stream_obj->get_competence_set_id()}
				AND a.stream_id = {$stream_obj->get_id()}
			GROUP BY user_id
		");
	}
	
}

?>