<?php

class activity_edit_helper extends base_static_db_helper
{

	const SECTION_EDIT_GENERAL = "general";
	const SECTION_EDIT_MODERATORS = "moderators";
	const SECTION_EDIT_ROLES = "roles";
	const SECTION_EDIT_PARTICIPANTS_SUBSCRIPTIONS = "participants-subscriptions";
	const SECTION_EDIT_PARTICIPANTS = "participants";
	const SECTION_EDIT_MARKS = "marks";
	const SECTION_EDIT_LONGTERM_MARKS = "longterm-marks";

	public static function can_edit_format($activity_id)
	{
		$participant_exists = self::db()->row_exists("
			SELECT user_id
			FROM activity_participant
			WHERE activity_id = {$activity_id} AND (status <> '" . activity_participants_helper::PARTCIPANT_STATUS_DELETED . "')
			LIMIT 1
		");

		$participant_mark_exists = self::db()->row_exists("
			SELECT user_id
			FROM activity_participant_mark
			WHERE activity_id = {$activity_id}
			LIMIT 1
		");

		return !($participant_exists || $participant_mark_exists);
	}

}

?>