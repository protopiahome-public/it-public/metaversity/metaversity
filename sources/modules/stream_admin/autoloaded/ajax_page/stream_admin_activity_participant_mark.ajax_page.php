<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class stream_admin_activity_participant_mark_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"activity_before_start",
		"user_before_start",
		"role_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $activity_row;
	protected $role_id;
	protected $role_row;
	protected $user_id;
	protected $user_row;
	protected $mark;
	protected $comment;
	protected $separate_cities;

	public function start()
	{
		if (activity_participants_helper::get_current_status($this->activity_id, $this->role_id, $this->user_id) !== activity_participants_helper::PARTCIPANT_STATUS_ACCEPTED)
		{
			return false;
		}
		$this->mark = POST("mark");
		if (!activity_marks_helper::is_good_participant_mark($this->mark))
		{
			return false;
		}
		$this->comment = mb_substr(POST("comment"), 0, 16535);
		return true;
	}

	public function commit()
	{
		activity_marks_helper::change_participant_mark($this->activity_id, $this->role_id, $this->user_id, $this->mark, $this->comment);
		return true;
	}

	public function clean_cache()
	{
		stream_marks_cache_tag::init($this->stream_obj->get_id())->update();
		user_marks_cache_tag::init($this->user_id)->update();
	}

	public function get_data()
	{
		$stat = activity_participants_helper::fetch_participants_raw_stat($this->activity_id);
		array_merge_good($stat, activity_marks_helper::fetch_marks_stat($this->activity_id));
		return array("status" => "OK", "stat" => $stat);
	}

}

?>