<?php

require_once PATH_INTCMF . "/tree.php";

class stream_admin_formats_tree_ajax_page extends base_ajax_ctrl
{
	const FORMAT_TITLE_MAX_LENGTH = 40;
	const FORMAT_GROUP_TITLE_MAX_LENGTH = 100;

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
	);
	protected $autostart_db_transaction = false;
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function init()
	{
		$this->db->begin();
	}

	public function get_data()
	{
		$operation = REQUEST("operation");
		$result = false;
		switch ($operation)
		{
			case "get_nodes":
				$result = $this->get_nodes();
				break;
			case "rename_item":
				$result = $this->rename_item();
				break;
			case "rename_group":
				$result = $this->rename_group();
				break;
			case "delete_item":
				$result = $this->delete_item();
				break;
			case "delete_group":
				$result = $this->delete_group();
				break;
			case "move_item":
				$result = $this->move_item();
				break;
			case "move_group":
				$result = $this->move_group();
				break;
			case "create_group":
				$result = $this->create_group();
				break;
			case "create_item":
				$result = $this->create_item();
				break;
			case "clone_item":
				$result = $this->clone_item();
				break;
		}

		if (is_array($result) and isset($result["status"]) and $result["status"] === "OK")
		{
			if ($operation != "get_nodes")
			{
				stream_formats_cache_tag::init($this->stream_id)->update();
				stream_roles_cache_tag::init($this->stream_id)->update();
			}
			$this->db->commit();
		}
		else
		{
			$this->db->rollback();
		}

		if (!$result)
		{
			$result = array(
				"status" => "ERROR",
			);
		}

		return $result;
	}

	public function get_nodes()
	{
		$data_raw_plain = $this->db->fetch_all("
			SELECT id, parent_id, title, position
			FROM format_group
			WHERE stream_id = {$this->stream_id}
			ORDER BY position, id
		");
		$data_raw = array();
		foreach ($data_raw_plain as $row)
		{
			$data_raw[$row["id"]] = array(
				"text" => $row["title"],
				//"state" => array("opened" => true),
				"id" => "group_" . $row["id"],
				"type" => "group",
			);
		}
		foreach ($data_raw_plain as $row)
		{
			if ($row["parent_id"])
			{
				if (!isset($data_raw[$row["parent_id"]]["children"]))
				{
					$data_raw[$row["parent_id"]]["children"] = array();
				}
				$data_raw[$row["parent_id"]]["children"][] = &$data_raw[$row["id"]];
			}
		}
		$data = array();
		foreach ($data_raw_plain as $row)
		{
			if (!$row["parent_id"])
			{
				$data[] = &$data_raw[$row["id"]];
			}
		}
		$formats_data = $this->db->fetch_all("
			SELECT f.id, f.title, f.format_group_id as parent_id
			FROM format f
			WHERE stream_id = {$this->stream_id}
			ORDER BY f.position, f.id
		");
		foreach ($formats_data as $format_row)
		{
			if (isset($data_raw[$format_row["parent_id"]]))
			{
				if (!isset($data_raw[$format_row["parent_id"]]["children"]))
				{
					$data_raw[$format_row["parent_id"]]["children"] = array();
				}
				$data_raw[$format_row["parent_id"]]["children"][] = array(
					"text" => $format_row["title"],
					"id" => "format_" . $format_row["id"],
					"type" => "format",
				);
			}
		}
		return array(
			array(
				"text" => trans("Format groups"),
				"state" => array("opened" => true),
				"id" => "root_" . 0,
				"type" => "root",
				"children" => $data,
			)
		);
	}

	protected function create_item()
	{
		$group_id = POST("group_id");
		$title = POST("title");
		$title = $this->process_title($title, self::FORMAT_TITLE_MAX_LENGTH);
		$title_quoted = $this->db->escape($title);

		if (!is_good_id($group_id))
		{
			return false;
		}
		if (!$this->group_exists($group_id))
		{
			return false;
		}

		$position = $this->db->get_value("
			SELECT IF(COUNT(position), MAX(position) + 1, 0)
			FROM format
			WHERE format_group_id = {$group_id}
		");
		$this->db->sql("
			INSERT INTO format (stream_id, title, format_group_id, position, add_time, edit_time)
			VALUES ({$this->stream_id}, '{$title_quoted}', {$group_id}, {$position}, NOW(), NOW())
		");
		$id = $this->db->get_last_id();

		$this->rebuild_archived_state();

		return array(
			"status" => "OK",
			"id" => $id,
			"title" => $title,
		);
	}

	protected function clone_item()
	{
		$original_id = POST("original_id");
		if (!$this->item_exists($original_id))
		{
			return false;
		}
		$original_row = $this->db->get_row("SELECT * FROM format WHERE id = {$original_id}");
		$title = $original_row["title"];
		$postfix = " (" . trans("clone") . ")";
		$title = $this->process_title($title, self::FORMAT_TITLE_MAX_LENGTH - mb_strlen($postfix));
		$title .= $postfix;
		$title_quoted = $this->db->escape($title);
		$group_id = $original_row["format_group_id"];

		$position = $this->db->get_value("
			SELECT IF(COUNT(position), MAX(position) + 1, 0)
			FROM format
			WHERE format_group_id = {$group_id}
		");
		$this->db->sql("
			INSERT INTO format (stream_id, title, format_group_id, position, add_time, edit_time)
			VALUES ({$this->stream_id}, '{$title_quoted}', {$group_id}, {$position}, NOW(), NOW())
		");
		$id = $this->db->get_last_id();
		$roles = $this->db->fetch_all("
			SELECT * 
			FROM role
			WHERE format_id = {$original_id}
		");
		foreach ($roles as $role)
		{
			$this->db->sql("
				INSERT INTO rate (rate_type_id, competence_set_id, add_time, edit_time)
				(
					SELECT rate_type_id, competence_set_id, NOW(), NOW()
					FROM rate
					WHERE id = {$role["rate_id"]}
				)
			");
			$new_rate_id = $this->db->get_last_id();
			$this->db->sql("
				INSERT INTO rate_competence_link (rate_id, competence_id, mark)
				(
					SELECT {$new_rate_id}, competence_id, mark
					FROM rate_competence_link
					WHERE rate_id = {$role["rate_id"]}
				)
			");
			$this->db->sql("
				INSERT INTO role (stream_id, format_id, title, total_number, add_time, edit_time, competence_count_calc, rate_id, descr, auto_accept, descr_text, descr_html)
				(
					SELECT stream_id, {$id}, title, total_number, NOW(), NOW(), competence_count_calc, {$new_rate_id}, descr, auto_accept, descr_text, descr_html
					FROM role
					WHERE id = {$role["id"]}
				)
			");
			$new_role_id = $this->db->get_last_id();
			$this->db->sql("
				INSERT INTO role_study_level_link (role_id, study_level_id)
				(
					SELECT {$new_role_id}, study_level_id
					FROM role_study_level_link
					WHERE role_id = {$role["id"]}
				)
			");
		}

		$this->rebuild_archived_state();

		return array(
			"status" => "OK",
			"id" => $id,
			"title" => $title,
		);
	}

	protected function create_group()
	{
		$parent_id = POST("group_id");
		$title = $this->process_title(POST("title"), self::FORMAT_GROUP_TITLE_MAX_LENGTH);
		$title_quoted = $this->db->escape($title);

		if (!is_good_num($parent_id))
		{
			return false;
		}
		if ($parent_id)
		{
			if (!$this->group_exists($parent_id))
			{
				return false;
			}
		}

		$eq = $parent_id ? "=" : "IS";
		if ($parent_id == 0)
		{
			$parent_id = "NULL";
		}

		$position = $this->db->get_value("
			SELECT IF(COUNT(position), MAX(position) + 1, 0)
			FROM format_group
			WHERE parent_id {$eq} {$parent_id}
		");
		$this->db->sql("
			INSERT INTO format_group (stream_id, title, parent_id, position, add_time, edit_time)
			VALUES ({$this->stream_id}, '{$title_quoted}', {$parent_id}, {$position}, NOW(), NOW())
		");
		$id = $this->db->get_last_id();

		return array(
			"status" => "OK",
			"id" => $id,
			"title" => $title,
		);
	}

	protected function rename_item()
	{
		$id = POST("id");
		$title = POST("title");

		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->item_exists($id))
		{
			return false;
		}

		$title = $this->process_title($title, self::FORMAT_TITLE_MAX_LENGTH);
		$title_quoted = $this->db->escape($title);

		$this->db->sql("
			UPDATE format
			SET 
				title = '{$title_quoted}',
				edit_time = NOW()
			WHERE id = {$id} AND stream_id = {$this->stream_id}
		");

		format_cache_tag::init($id)->update();

		return array(
			"status" => "OK",
			"title" => $title,
		);
	}

	protected function rename_group()
	{
		$id = POST("id");
		$title = POST("title");

		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->group_exists($id))
		{
			return false;
		}
		if ($this->is_archive_group($id))
		{
			return array(
				"status" => "OK",
				"title" => "ARCHIVE",
			);
		}

		$title = $this->process_title($title, self::FORMAT_GROUP_TITLE_MAX_LENGTH);
		$title_quoted = $this->db->escape($title);

		$this->db->sql("
			UPDATE format_group
			SET 
				title = '{$title_quoted}',
				edit_time = NOW()
			WHERE id = {$id} AND stream_id = {$this->stream_id}
		");
		return array(
			"status" => "OK",
			"title" => $title,
		);
	}

	protected function delete_item()
	{
		$id = POST("id");

		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->item_exists($id))
		{
			return false;
		}
		if (!$this->item_is_deletable($id))
		{
			return array("status" => "ERROR", "error_code" => "CHECK", "error_descr" => "activity_exists");
		}

		$this->db->sql("
			DELETE FROM format
			WHERE id = {$id} AND stream_id = {$this->stream_id}
		");

		format_cache_tag::init($id)->update();

		return array(
			"status" => "OK",
		);
	}

	protected function delete_group($id = 0)
	{
		$id = POST("id");

		if (!is_good_id($id))
		{
			return false;
		}
		if (!$this->group_exists($id))
		{
			return false;
		}
		if (!$this->group_is_deletable($id))
		{
			return false;
		}

		$this->db->sql("
			DELETE FROM format_group
			WHERE id = {$id} AND stream_id = {$this->stream_id}
		");

		return array(
			"status" => "OK",
		);
	}

	protected function move_item()
	{
		$id = POST("id");
		$group_id = POST("group_id");
		$old_group_id = POST("old_group_id");
		$position = POST("position");

		if (!is_good_id($id) or ! is_good_id($group_id) or ! is_good_id($old_group_id) or ! is_good_num($position))
		{
			return false;
		}
		if (!$this->item_exists($id))
		{
			return false;
		}
		if (!$this->group_exists($old_group_id))
		{
			return false;
		}
		if (!$this->group_exists($group_id))
		{
			return false;
		}
		$this->db->sql("
			UPDATE format
			SET format_group_id = {$group_id}
			WHERE
				id = {$id}
				AND format_group_id = {$old_group_id}
				AND stream_id = {$this->stream_id}
		");

		$positions = $this->db->fetch_all("
			SELECT id, position
			FROM format
			WHERE
				format_group_id = {$group_id}
				AND id <> {$id}
				AND stream_id = {$this->stream_id}
			ORDER BY position, id
		");
		array_splice($positions, $position, 0, array(
			array(
				"id" => $id
			)
		));
		foreach ($positions as $item_position => $item)
		{
			$this->db->sql("
				UPDATE format 
				SET position = {$item_position}
				WHERE id = {$item["id"]} AND stream_id = {$this->stream_id}
			");
		}

		$this->rebuild_archived_state();

		return array(
			"status" => "OK",
		);
	}

	protected function move_group()
	{
		$id = POST("id");
		$parent_id = POST("group_id");
		$position = POST("position");

		if (!is_good_id($id) or ! is_good_num($parent_id) or ! is_good_num($position))
		{
			return false;
		}
		if (!$this->group_exists($id))
		{
			return false;
		}
		if ($parent_id)
		{
			if (!$this->group_exists($parent_id))
			{
				return false;
			}

			$ancestor_id = $parent_id;
			do
			{
				if ($ancestor_id == $id)
				{
					return array("status" => "ERROR", "error" => "recursive_move");
				}
			}
			while ($ancestor_id = $this->db->get_value("SELECT parent_id FROM format_group WHERE id = {$ancestor_id}"));
		}

		$eq = $parent_id ? "=" : "IS";
		if ($parent_id == 0)
		{
			$parent_id = "NULL";
		}

		$this->db->sql("
			UPDATE format_group
			SET parent_id = {$parent_id}
			WHERE id = {$id}
		");

		$positions = $this->db->fetch_all("
			SELECT id, position 
			FROM format_group 
			WHERE parent_id {$eq} {$parent_id} AND id <> {$id} AND stream_id = {$this->stream_id}
			ORDER BY position, id
		");
		array_splice($positions, $position, 0, array(
			array(
				"id" => $id
			)
		));
		foreach ($positions as $item_position => $item)
		{
			$this->db->sql("
				UPDATE format_group 
				SET position = {$item_position}
				WHERE id = {$item["id"]} AND stream_id = {$this->stream_id}
			");
		}

		$this->rebuild_archived_state();

		return array(
			"status" => "OK",
		);
	}

	protected function process_title($title, $max_length)
	{
		$title = trim(preg_replace("/[\\x00-\\x20]+/", " ", $title));
		if ($title === "")
		{
			$title = "Unnamed";
		}
		$title = mb_substr($title, 0, $max_length);
		return $title;
	}

	protected function group_exists($id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM format_group
			WHERE id = {$id} AND stream_id = {$this->stream_id}
		");
	}

	protected function item_exists($id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM format
			WHERE id = {$id} AND stream_id = {$this->stream_id}
		");
	}

	protected function item_is_deletable($id)
	{
		return !$this->db->row_exists("
			SELECT *
			FROM activity
			WHERE format_id = {$id}
		");
	}

	protected function group_is_deletable($id)
	{
		$child_group_exists = $this->db->row_exists("
			SELECT *
			FROM format_group
			WHERE parent_id = {$id} AND stream_id = {$this->stream_id}
		");

		$child_item_exists = $this->db->row_exists("
			SELECT *
			FROM format
			WHERE format_group_id = {$id} AND stream_id = {$this->stream_id}
		");

		return !$child_group_exists && !$child_item_exists && !$this->is_archive_group($id);
	}

	protected function is_archive_group($id)
	{
		return $this->db->row_exists("
			SELECT *
			FROM format_group
			WHERE id = {$id} AND is_archive = 1
		");
	}

	protected function rebuild_archived_state()
	{
		$groups = $this->db->fetch_all("
			SELECT id, parent_id, is_archive 
			FROM format_group
			WHERE stream_id = {$this->stream_id}
		");
		$tree = new tree($groups);
		$archive_groups = array();
		foreach ($tree->get_index() as $group_id => $group_data)
		{
			if ($group_data["contents"]["is_archive"] == 1)
			{
				$archive_groups[$group_id] = true;
				continue;
			}
			foreach ($group_data["ancestors"] as $ancestor_group_data)
			{
				if ($ancestor_group_data["contents"]["is_archive"] == 1)
				{
					$archive_groups[$group_id] = true;
					continue 2;
				}
			}
		}
		$this->db->sql("
			UPDATE format 
			SET is_archived = 0
			WHERE stream_id = {$this->stream_id}
		");
		if (sizeof($archive_groups))
		{
			$this->db->sql("
				UPDATE format 
				SET is_archived = 1 
				WHERE format_group_id IN (" . join(", ", array_keys($archive_groups)) . ")
					AND stream_id = {$this->stream_id}
			");
		}
	}

}

?>