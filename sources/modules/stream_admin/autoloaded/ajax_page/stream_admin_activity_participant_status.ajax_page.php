<?php

class stream_admin_activity_participant_status_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"activity_before_start",
		"user_before_start",
		"role_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $activity_row;
	protected $role_id;
	protected $role_row;
	protected $user_id;
	protected $user_row;
	protected $status;
	protected $city_id;
	protected $separate_cities;

	/**
	 * @var stream_access
	 */
	protected $participant_stream_access;

	public function start()
	{
		$this->status = POST("status");
		if (!activity_participants_helper::is_good_participant_status($this->status))
		{
			return false;
		}
		$this->participant_stream_access = new stream_access($this->stream_obj, $this->user_id);
		return true;
	}

	public function commit()
	{
		return activity_participants_helper::change_participant_status($this->activity_id, $this->role_id, $this->user_id, $this->status, $this->participant_stream_access->is_member());
	}

	public function get_data()
	{
		$stat = activity_participants_helper::fetch_participants_raw_stat($this->activity_id);
		array_merge_good($stat, activity_marks_helper::fetch_marks_stat($this->activity_id));
		return array("status" => "OK", "stat" => $stat);
	}

}

?>