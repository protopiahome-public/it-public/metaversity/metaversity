<?php

class stream_admin_activity_participants_subscription_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"stream_admin_check_moderator_rights",
		"activity_before_start",
		"user_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $activity_row;
	protected $user_id;
	protected $user_row;
	protected $is_subscribed;

	public function start()
	{
		$this->is_subscribed = POST("is_subscribed") === "1";
		return true;
	}

	public function check_rights()
	{
		return !activity_helper::is_longterm($this->activity_row);
	}

	public function check()
	{
		$allowed_users = activity_participants_subscriptions_helper::fetch_participants_subscriptions_to_edit($this->activity_id);
		if (!isset($allowed_users[$this->user_id]))
		{
			return false;
		}
		return true;
	}

	public function commit()
	{
		activity_participants_subscriptions_helper::change_participants_subscription($this->activity_id, $this->user_id, $this->is_subscribed);
		return true;
	}

	public function get_data()
	{
		return array("status" => "OK", "count" => activity_participants_subscriptions_helper::get_participants_subscriptions_count($this->activity_id));
	}

}

?>