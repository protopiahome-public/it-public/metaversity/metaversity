<?php

class stream_admin_users_xml_ctrl extends base_easy_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("user u");
		$select_sql->add_select_fields("u.id");
		$select_sql->add_join("JOIN stream_user_link l ON l.user_id = u.id");
		$select_sql->add_where("l.stream_id = {$this->stream_id}");
		$select_sql->add_where("l.status <> 'pretender' AND l.status <> 'deleted'");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>