<?php

class stream_admin_activity_participants_raw_stat_xml_ctrl extends base_xml_ctrl
{

	// Internal
	protected $activity_id;

	public function __construct($activity_id)
	{
		$this->activity_id = $activity_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_NO_CACHE;
		
		$stat = activity_participants_helper::fetch_participants_raw_stat($this->activity_id);
		return $this->get_node_string($this->name, array(
				"activity_id" => $this->activity_id,
				"accepted_count" => $stat["accepted_count"],
				"premoderation_count" => $stat["premoderation_count"],
		));
	}

}

?>