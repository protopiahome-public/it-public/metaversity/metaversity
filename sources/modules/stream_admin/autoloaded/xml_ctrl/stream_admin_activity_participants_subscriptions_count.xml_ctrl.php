<?php

class stream_admin_activity_participants_subscriptions_count_xml_ctrl extends base_xml_ctrl
{

	// Internal
	protected $activity_id;

	public function __construct($activity_id)
	{
		$this->activity_id = $activity_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_NO_CACHE;
		
		$count = activity_participants_subscriptions_helper::get_participants_subscriptions_count($this->activity_id);
		return $this->get_node_string($this->name, array(
				"activity_id" => $this->activity_id,
				"count" => $count,
		));
	}

}

?>