<?php

final class stream_admin_taxonomy extends base_taxonomy
{

	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	/**
	 * @var stream_access
	 */
	protected $stream_access;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $stream_obj->get_id();
		$this->stream_access = $stream_obj->get_access();
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		$p = $this->get_parts_relative();
		
		if (!$this->stream_access->has_moderator_rights())
		{
			$this->xml_loader->set_error_403();
			return;
		}
		
		$this->xml_loader->set_error_404_xslt("stream_admin_404", "stream_admin");

		if ($p[1] === "news")
		{
			if ($page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_admin_news_xml_page($this->stream_obj, $page));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_admin_news_item_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($id = $p[2]))
			{
				if ($p[3] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_news_item_edit_xml_page($this->stream_obj, $id));
				}
				elseif ($p[3] === "delete" and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_news_item_delete_xml_page($this->stream_obj, $id));
				}
			}
		}
		elseif (($file_id = $this->is_type_folder($p[1], "file-am")) and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_moderators_file_download_xml_page($this->stream_obj, $file_id));
		}
		elseif ($p[1] === "activities")
		{
			if (($params = activities_taxonomy_helper::parse_request_params($p, 2, activities_taxonomy_helper::MOD_NO_DATE)))
			{
				$this->xml_loader->add_xml_with_xslt(new stream_admin_activities_xml_page($params, $this->stream_id, true));
			}
			elseif ($p[2] === "calendar" and $p[3] === null)
			{
				//activities/calendar/
				$this->set_redirect_url("{$p[1]}/");
			}
			elseif ($p[2] === "add")
			{
				$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($activity_id = $p[2]))
			{
				$this->xml_loader->add_xml(new activity_full_xml_ctrl($activity_id, $this->stream_id));
				$this->xml_loader->add_xml(new stream_admin_activity_participants_subscriptions_count_xml_ctrl($activity_id));
				$this->xml_loader->add_xml(new stream_admin_activity_participants_raw_stat_xml_ctrl($activity_id));
				$this->xml_loader->add_xml(new stream_admin_activity_marks_stat_xml_ctrl($activity_id));
				if ($p[3] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_general_xml_page($this->stream_obj, $activity_id));
				}
				elseif ($p[3] === activity_edit_helper::SECTION_EDIT_MODERATORS and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_moderators_xml_page($this->stream_obj, $activity_id));
				}
				elseif ($p[3] === activity_edit_helper::SECTION_EDIT_ROLES and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_roles_xml_page($this->stream_obj, $activity_id));
				}
				elseif ($p[3] === activity_edit_helper::SECTION_EDIT_PARTICIPANTS_SUBSCRIPTIONS and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_participants_subscriptions_xml_page($this->stream_obj, $activity_id));
				}
				elseif ($p[3] === activity_edit_helper::SECTION_EDIT_PARTICIPANTS and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_participants_xml_page());
					$this->xml_loader->add_xml(new activity_participants_xml_ctrl($this->stream_obj, $activity_id));
					$this->xml_loader->add_xml(new stream_admin_users_xml_ctrl($this->stream_obj));
				}
				elseif ($p[3] === activity_edit_helper::SECTION_EDIT_MARKS and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_marks_xml_page());
					$this->xml_loader->add_xml(new activity_participants_xml_ctrl($this->stream_obj, $activity_id));
					$this->xml_loader->add_xml(new stream_admin_users_xml_ctrl($this->stream_obj));
				}
				elseif ($p[3] === activity_edit_helper::SECTION_EDIT_LONGTERM_MARKS)
				{
					if ($page = $this->is_page_folder($p[4]) and $p[5] === null)
					{
						$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_longterm_marks_users_xml_page($this->stream_obj, $activity_id, $page));
					}
					elseif (($user_id = $this->get_user_id_by_login($p[4])))
					{
						$this->xml_loader->add_xml(new stream_user_short_xml_ctrl($user_id, $this->stream_id));
						if ($p[5] === null)
						{
							$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_longterm_marks_user_xml_page($this->stream_obj, $activity_id, $user_id));
							$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->stream_obj->get_competence_set_id(), false, true, $this->stream_id));
						}
						elseif (is_good_id($p[5]) and $page = $this->is_page_folder($p[6]) and $p[7] === null)
						{
							$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_edit_longterm_marks_user_competence_xml_page($this->stream_obj, $activity_id, $user_id, $p[5], $page));
						}
					}
				}
				elseif ($p[3] === "delete" and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_activity_delete_xml_page($this->stream_obj, $activity_id));
				}
			}
		}
		elseif ($p[1] === "metaactivities")
		{
			if ($page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_admin_metaactivities_xml_page($this->stream_obj, $page));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_admin_metaactivity_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($id = $p[2]))
			{
				$this->xml_loader->add_xml(new metaactivity_short_xml_ctrl($id, $this->stream_id));
				if ($p[3] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_metaactivity_edit_xml_page($this->stream_obj, $id));
				}
				elseif ($p[3] === "delete" and $p[4] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_admin_metaactivity_delete_xml_page($this->stream_obj, $id));
				}
			}
		}
		elseif ($p[1] === "formats")
		{
			//admin/formats/...
			if ($p[2] === null)
			{
				//admin/formats/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_formats_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]))
			{
				$this->xml_loader->add_xml(new format_short_xml_ctrl($p[2], $this->stream_id));
				if ($p[3] === null or $p[3] === "roles" and $p[4] === null)
				{
					//admin/formats/<id>/[roles/]
					$this->xml_loader->add_xml_with_xslt(new stream_admin_format_roles_xml_page($this->stream_obj, $p[2]));
				}
				elseif ($p[3] === "roles" and $p[4] === "add" and $p[5] === null)
				{
					//admin/formats/<id>/roles/add/
					$this->xml_loader->add_xml_with_xslt(new stream_admin_format_role_add_xml_page($this->stream_obj, $p[2]));
				}
				elseif ($p[3] === "roles" and is_good_id($p[4]))
				{
					//admin/formats/<id>/roles/<id>/
					$this->xml_loader->add_xml(new role_short_xml_ctrl($p[4], $this->stream_id));
					if ($p[5] === null)
					{
						$this->xml_loader->add_xml_with_xslt(new stream_admin_format_role_edit_xml_page($this->stream_obj, $p[2], $p[4]));
					}
					elseif ($p[5] === "delete" and $p[6] === null)
					{
						$this->xml_loader->add_xml_with_xslt(new stream_admin_format_role_delete_xml_page($this->stream_obj, $p[2], $p[4]));
					}
				}
			}
		}
		elseif ($p[1] === "materials")
		{
			//admin/materials/...
			if ($p[2] === null)
			{
				//admin/materials/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_materials_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/materials/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_material_add_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === "outer" and $p[4] === null)
			{
				//admin/materials/add/outer/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_material_add_xml_page($this->stream_obj, true));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/materials/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_material_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/materials/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_material_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif (!$this->stream_access->has_admin_rights())
		{
			if ($p[1] === null and $this->stream_access->has_moderator_rights())
			{
				$this->set_redirect_url("news/");
			}
			else
			{
				$this->xml_loader->set_error_403_xslt("stream_admin_403", "stream_admin");
				$this->xml_loader->set_error_403();
			}
		}
		elseif ($p[1] === null)
		{
			//admin/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_general_xml_page($this->stream_id));
		}
		elseif ($p[1] === "tree" and $p[2] === null)
		{
			//admin/tree/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_competence_set_tree_xml_page($this->stream_obj->get_competence_set_id(), $this->stream_id));
		}
		elseif ($p[1] === "integral-competences")
		{
			//admin/integral-competences/...
			if ($p[2] === null)
			{
				//admin/integral-competences/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_integral_competences_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/integral-competences/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_integral_competence_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/integral-competences/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_integral_competence_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/integral-competences/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_integral_competence_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif ($p[1] === "translators")
		{
			//admin/translators/...
			if ($p[2] === null)
			{
				//admin/translators/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_translators_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/translators/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_translator_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/translators/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_translator_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/translators/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_translator_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif ($p[1] === "tree-stat" and $p[2] === null)
		{
			//admin/tree-stat/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_competence_set_tree_stat_xml_page($this->stream_obj));
		}
		elseif ($p[1] === "positions")
		{
			//admin/positions/...
			if ($p[2] === null)
			{
				//admin/positions/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_positions_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/positions/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_position_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/positions/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_position_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/positions/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_position_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif ($p[1] === "credits")
		{
			//credits/...
			if ($page = $this->is_page_folder($p[2]) and $p[3] === null)
			{
				//credits/[page-<page>/]
				$this->xml_loader->add_xml_with_xslt(new stream_admin_credits_users_xml_page());
				$this->xml_loader->add_xml(new stream_users_xml_page($this->stream_obj, $page));
			}
			elseif ($p[2] === "users" and $p[3] === null)
			{
				//credits/users/ --> /credits/
				$this->set_redirect_url("credits/");
			}
			elseif ($p[2] === "users" and $user_id = $this->get_user_id_by_login($p[3]))
			{
				//credits/users/<login>/...
				$this->xml_loader->add_xml(new stream_user_short_xml_ctrl($user_id, $this->stream_id));
				if ($p[4] === null)
				{
					//credits/users/<login>/
					$this->xml_loader->add_xml_with_xslt(new stream_admin_credits_user_xml_page());
					$this->xml_loader->add_xml(new math_user_position_results_xml_ctrl($user_id, $this->stream_obj));
				}
				elseif (is_good_id($p[4]) and $p[5] === null)
				{
					//credits/users/<login>/<position_id>/
					$this->xml_loader->add_xml_with_xslt(new stream_admin_credits_match_xml_page());
					$this->xml_loader->add_xml(new math_position_details_xml_ctrl($this->stream_obj, $p[4], $user_id));
					$this->xml_loader->add_xml(new position_full_xml_ctrl($p[4], $this->stream_id));
				}
			}
			elseif ($p[2] === "positions")
			{
				//positions/...
				if ($p[3] === null)
				{
					//positions/
					$this->xml_loader->add_xml_with_xslt(new stream_admin_credits_positions_xml_page());
					$this->xml_loader->add_xml(new stream_ratings_positions_xml_page($this->stream_obj));
				}
				elseif (is_good_id($p[3]) and $page = $this->is_page_folder($p[4]) and $p[5] === null)
				{
					//positions/<position_id>/
					$this->xml_loader->add_xml_with_xslt(new stream_admin_credits_position_xml_page());
					$this->xml_loader->add_xml(new position_full_xml_ctrl($p[3], $this->stream_id));
					$this->xml_loader->add_xml(new math_rating_xml_ctrl($p[3], $this->stream_obj, $page));
				}
			}
		}
		elseif ($p[1] === "subjects")
		{
			//admin/subjects/...
			if ($p[2] === null)
			{
				//admin/subjects/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_subjects_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/subjects/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_subject_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/subjects/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_subject_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/subjects/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_subject_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif ($p[1] === "study-levels")
		{
			//admin/study-levels/...
			if ($p[2] === null)
			{
				//admin/study-levels/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_levels_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/study-levels/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_level_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/study-levels/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_level_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/study-levels/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_level_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif ($p[1] === "study-directions")
		{
			//admin/study-directions/...
			if ($p[2] === null)
			{
				//admin/study-directions/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_directions_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/study-directions/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_direction_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/study-directions/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_direction_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/study-directions/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_study_direction_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif ($p[1] === "groups")
		{
			//admin/groups/...
			if ($p[2] === null)
			{
				//admin/groups/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_groups_xml_page($this->stream_obj));
			}
			elseif ($p[2] === "add" and $p[3] === null)
			{
				//admin/groups/add/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_group_add_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $p[3] === null)
			{
				//admin/groups/<id>/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_group_edit_xml_page($this->stream_obj, $p[2]));
			}
			elseif (is_good_id($p[2]) and $p[3] === "delete" and $p[4] === null)
			{
				//admin/groups/<id>/delete/
				$this->xml_loader->add_xml_with_xslt(new stream_admin_group_delete_xml_page($this->stream_obj, $p[2]));
			}
		}
		elseif ($p[1] == "members" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//admin/members/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_members_xml_page($this->stream_id, $page));
		}
		elseif ($p[1] == "members" and is_good_id($p[2]) and $p[3] === null)
		{
			//admin/members/<user_id>/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_member_edit_xml_page($this->stream_obj, array("stream_id" => $this->stream_id, "user_id" => $p[2]), 'members'));
		}
		elseif ($p[1] == "pretenders" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//admin/pretenders/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_pretenders_xml_page($this->stream_id, $page));
		}
		elseif ($p[1] == "pretenders" and is_good_id($p[2]) and $p[3] === null)
		{
			//admin/members/<user_id>/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_member_edit_xml_page($this->stream_obj, array("stream_id" => $this->stream_id, "user_id" => $p[2]), 'pretenders'));
		}
		elseif ($p[1] == "moderators" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//admin/moderators/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_moderators_xml_page($this->stream_id, $page));
		}
		elseif ($p[1] == "admins" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//admin/admins/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_admins_xml_page($this->stream_id, $page));
		}
		elseif ($p[1] == "import-users" and $p[2] === null)
		{
			//admin/import-users/
			$this->xml_loader->add_xml_with_xslt(new stream_admin_import_users_xml_page($this->stream_obj));
		}
	}

	private function get_user_id_by_login($login)
	{
		return url_helper::get_user_id_by_login($login);
	}

}

?>