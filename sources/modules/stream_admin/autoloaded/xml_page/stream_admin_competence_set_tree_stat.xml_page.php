<?php

class stream_admin_competence_set_tree_stat_xml_page extends base_xml_ctrl
{

	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $activity_index;
	protected $date_from;
	protected $date_to;
	protected $date_to_real;

	public function __construct(stream_obj $stream_obj)
	{
		parent::__construct();
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_NO_CACHE;
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, false, true, $this->stream_id));
		$this->setup_dates();
		$xdom = xdom::create($this->name);
		$xdom->set_attr("date_from", $this->date_from);
		$xdom->set_attr("date_to", $this->date_to);
		$this->db->sql("SET SESSION group_concat_max_len = 51200");

		foreach (array(true, false) as $only_important)
		{
			$this->build_position_stat($xdom, $only_important);
			$formats_raw_stat = $this->get_formats_raw_stat($only_important);
			$this->build_formats_stat($xdom, $only_important, $formats_raw_stat);
			$this->build_activities_stat($xdom, $only_important, $formats_raw_stat);
			$this->build_marks_stat($xdom, $only_important);
		}

		$xdom->create_child_node("position_index", json_encode($this->db->fetch_column_values("SELECT id, title FROM position WHERE stream_id = {$this->stream_id}", "title", "id")));
		$xdom->create_child_node("format_index", json_encode($this->db->fetch_column_values("SELECT id, title FROM format WHERE stream_id = {$this->stream_id}", "title", "id")));
		$xdom->create_child_node("role_index", json_encode($this->db->fetch_column_values("SELECT id, title FROM role WHERE stream_id = {$this->stream_id}", "title", "id")));
		$xdom->create_child_node("activity_index", json_encode($this->activity_index));
		return $xdom->get_xml(true);
	}

	private function build_marks_stat(xnode $parent_node, $only_important)
	{
		$date_sql = "";
		if ($this->date_from)
		{
			$date_sql .= " AND (a.start_time > '{$this->date_from}' OR a.finish_time > '{$this->date_from}')";
		}
		if ($this->date_to_real)
		{
			$date_sql .= " AND (a.start_time < '{$this->date_to_real}' AND a.start_time != '0000-00-00')";
		}
		$only_important_sql = $only_important ? " AND apm.mark >= 2 " : "";
		$stat = $this->db->fetch_all("
			SELECT
				c.competence_id AS id, 
				SUM(apm.mark) as rating,
				COUNT(apm.mark) as count
			FROM activity_participant_mark apm
			INNER JOIN activity a ON a.id = apm.activity_id
			INNER JOIN role ON role.id = apm.role_id
			INNER JOIN rate r ON r.id = role.rate_id
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = r.id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE a.stream_id = {$this->stream_id} AND c.competence_set_id = {$this->competence_set_id}
				{$only_important_sql}
				{$date_sql}
			GROUP BY c.competence_id
			ORDER BY rating DESC
		", "id");
		$only_important_sql = $only_important ? " AND acm.mark >= 2 " : "";
		$stat2 = $this->db->fetch_all("
			SELECT
				c.competence_id AS id, 
				SUM(acm.mark) as rating,
				COUNT(acm.mark) as count
			FROM activity_competence_mark acm
			INNER JOIN activity a ON a.id = acm.activity_id
			INNER JOIN competence_calc c ON c.competence_id = acm.competence_id
			WHERE a.stream_id = {$this->stream_id} AND c.competence_set_id = {$this->competence_set_id}
				{$only_important_sql}
			GROUP BY c.competence_id
			ORDER BY rating DESC
		", "id");
		foreach ($stat2 as $id => $row)
		{
			array_key_set($stat, $id, array(
				"id" => $id,
				"rating" => 0,
				"count" => 0,
			));
			$stat[$id]["rating"] += $row["rating"];
			$stat[$id]["count"] += $row["count"];
		}
		$stat = array_sort($stat, "rating", SORT_DESC);
		$this->build_stat_xml($parent_node, $stat, "mark", $only_important, $clickable = false);
	}

	private function build_activities_stat(xnode $parent_node, $only_important, $formats_raw_stat)
	{
		$date_sql = "";
		if ($this->date_from)
		{
			$date_sql .= " AND (start_time > '{$this->date_from}' OR finish_time > '{$this->date_from}')";
		}
		if ($this->date_to_real)
		{
			$date_sql .= " AND (start_time < '{$this->date_to_real}' AND start_time != '0000-00-00')";
		}
		$activities = $this->db->fetch_all("
			SELECT id, format_id, title
			FROM activity
			WHERE stream_id = {$this->stream_id}
				{$date_sql}
		");
		$this->activity_index = array();
		$formats = array();
		foreach ($activities as $activity)
		{
			$activity_id = (int) $activity["id"];
			$title = $activity["title"];
			$format_id = (int) $activity["format_id"];
			array_key_set($formats, $format_id);
			$formats[$format_id][] = $activity_id;
			$this->activity_index[$activity_id] = $title;
		}
		$stat = array();
		foreach ($formats_raw_stat as $row)
		{
			$competence_id = $row["competence_id"];
			$format_id = (int) $row["format_id"];
			if (!isset($formats[$format_id]))
			{
				continue;
			}
			array_key_set($stat, $competence_id, array(
				"id" => $competence_id,
				"rating" => 0,
				"count" => 0,
				"stat" => "",
			));
			$count = count($formats[$format_id]);
			$stat[$competence_id]["rating"] += $row["rating"] * $count;
			$stat[$competence_id]["count"] += $row["count"] * $count;
			foreach ($formats[$format_id] as $activity_id)
			{
				$stat[$competence_id]["stat"] .= "|" . $activity_id . "@" . $format_id . "@" . $row["stat"];
			}
		}
		$stat = array_sort($stat, "rating", SORT_DESC);
		$this->build_stat_xml($parent_node, $stat, "activity", $only_important);
	}

	private function build_formats_stat(xnode $parent_node, $only_important, $formats_raw_stat)
	{
		$stat = array();
		foreach ($formats_raw_stat as $row)
		{
			$competence_id = $row["competence_id"];
			$format_id = $row["format_id"];
			array_key_set($stat, $competence_id, array(
				"id" => $competence_id,
				"rating" => 0,
				"count" => 0,
				"stat" => "",
			));
			$stat[$competence_id]["rating"] += $row["rating"];
			$stat[$competence_id]["count"] += $row["count"];
			$stat[$competence_id]["stat"] .= "|" . $format_id . "@" . $row["stat"];
		}
		$stat = array_sort($stat, "rating", SORT_DESC);
		$this->build_stat_xml($parent_node, $stat, "format", $only_important);
	}

	private function get_formats_raw_stat($only_important)
	{
		$only_important_sql = $only_important ? " AND rcl.mark >= 2 " : "";
		$stat = $this->db->fetch_all("
			SELECT
				f.id AS format_id, c.competence_id, 
				MAX(rcl.mark) as max_mark,
				SUM(rcl.mark) as rating,
				COUNT(rcl.mark) as count,
				CAST(GROUP_CONCAT(CONCAT(role.id, ':', rcl.mark) ORDER BY rcl.mark DESC SEPARATOR ',') AS CHAR) AS stat
			FROM format f
			INNER JOIN role ON role.format_id = f.id
			INNER JOIN rate r ON r.id = role.rate_id
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = r.id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE f.stream_id = {$this->stream_id} AND c.competence_set_id = {$this->competence_set_id}
				{$only_important_sql}
			GROUP BY f.id, c.competence_id
			ORDER BY max_mark DESC, rating DESC
		");
		return $stat;
	}

	private function build_position_stat(xnode $parent_node, $only_important)
	{
		$only_important_sql = $only_important ? " AND rcl.mark >= 2 " : "";
		$stat = $this->db->fetch_all("
			SELECT
				c.competence_id AS id, 
				SUM(rcl.mark) as rating,
				COUNT(rcl.mark) as count,
				CAST(GROUP_CONCAT(CONCAT(p.id, ':', rcl.mark) ORDER BY rcl.mark DESC SEPARATOR ',') AS CHAR) AS stat
			FROM position p
			INNER JOIN rate r ON r.id = p.rate_id
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = r.id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE stream_id = {$this->stream_id} AND c.competence_set_id = {$this->competence_set_id}
				{$only_important_sql}
			GROUP BY c.competence_id
			ORDER BY rating DESC
		");
		$this->build_stat_xml($parent_node, $stat, "position", $only_important);
	}

	private function build_stat_xml(xnode $parent_node, $stat, $type, $only_important, $clickable = true)
	{
		$stat_node = $parent_node->create_child_node("stat");
		$stat_node->set_attr("type", $type);
		$stat_node->set_attr("only_important", $only_important);
		$stat_node->set_attr("clickable", $clickable);
		$stat0 = reset($stat);
		if ($stat0)
		{
			$stat_node->set_attr("max_rating", $stat0["rating"]);
		}
		$this->db_xml_converter->build_xml($stat_node, $stat, false, "competence");
	}
	
	function setup_dates()
	{
		$this->date_from = GET("from");
		if (!$this->check_date($this->date_from))
		{
			$this->date_from = null;
		}

		$this->date_to = GET("to");
		if (!$this->check_date($this->date_to))
		{
			$this->date_to = null;
		}
		
		$this->date_to_real = $this->date_to;
		if ($this->date_to_real)
		{
			$tmp = $this->db->parse_datetime($this->date_to_real);
			$tmp += 24 * 3600;
			$this->date_to_real = substr($this->db->get_datetime($tmp), 0, 10);
		}
	}

	function check_date($date)
	{
		$matches = null;
		if (!preg_match("/^(20\d\d)-(\d\d)-(\d\d)$/", $date, $matches))
		{
			return false;
		}
		$year = $matches[1];
		$month = $matches[2];
		$day = $matches[3];
		if (!$this->check_date_splitted($day, $month, $year))
		{
			return false;
		}
		return true;
	}

	function check_date_splitted($day, $month, $year)
	{
		if ($day < 1 || $day > 31)
		{
			return false;
		}
		if ($month < 1 || $month > 12)
		{
			return false;
		}
		if ($year < 2000 || $month > 2035)
		{
			return false;
		}
		return true;
	}

}

?>