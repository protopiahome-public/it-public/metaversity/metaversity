<?php

class stream_admin_position_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	protected $dt_name = "position";
	protected $axis_name = "edit";

	/**
	 * @var position_dt
	 */
	protected $dt;
	// Internal
	protected $options = null;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;

	public function __construct(stream_obj $stream_obj, $id)
	{
		$this->options = array(
			0 => "—",
			1 => trans("Useful", "COMPETENCE_IMPORTANCE"),
			2 => trans("Required", "COMPETENCE_IMPORTANCE"),
			3 => trans("Key competence", "COMPETENCE_IMPORTANCE"),
		);
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		parent::__construct($id);
	}

	public function init()
	{
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, false, true, $this->stream_id));
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_id($this->stream_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.rate_id");
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
	}

	public function postprocess()
	{
		$this->xml_loader->add_xml(new rate_xml_ctrl($this->data[0]["rate_id"], $this->competence_set_id, $this->options));
	}

}

?>