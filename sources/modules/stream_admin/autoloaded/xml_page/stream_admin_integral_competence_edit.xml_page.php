<?php

class stream_admin_integral_competence_edit_xml_page extends base_xml_ctrl
{

	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $competence_id;

	public function __construct(stream_obj $stream_obj, $competence_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		$this->competence_id = $competence_id;
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new integral_competence_xml_ctrl($this->competence_id, $this->competence_set_id));
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, false, true, $this->stream_id));
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_NO_CACHE;
		
		$xdom = xdom::create($this->name);
		$xdom->set_attr("competence_id", $this->competence_id);
		$xdom->set_attr("competence_set_id", $this->competence_set_id);
		integral_competences_helper::fill_options_xml($xdom);
		integral_competences_helper::fill_integral_competences_xml($xdom, $this->competence_set_id, $this->competence_id);
		return $xdom->get_xml(true);
	}

}

?>