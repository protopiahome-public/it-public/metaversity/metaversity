<?php

class stream_admin_activity_edit_longterm_marks_users_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	protected $xml_attrs = array("stream_id", "activity_id", "start_time", "finish_time", "dates_ok");
	protected $xml_row_name = "user";
	// Internal
	protected $page;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $start_time = null;
	protected $finish_time = null;
	protected $dates_ok;

	/**
	 * @var select_sql_filter 
	 */
	protected $weight_sql_filter;

	public function __construct(stream_obj $stream_obj, $activity_id, $page)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->activity_id = $activity_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$row = activity_helper::get_row($this->activity_id);
		if (!$row || $row["stream_id"] != $this->stream_id || !activity_helper::is_longterm($row))
		{
			$this->set_error_404();
			return;
		}

		$this->dates_ok = stream_admin_longterm_marks_helper::set_times($this->activity_id, $this->start_time, $this->finish_time);

		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", trans("Name/Login"), array("u.login", "u.first_name", "u.last_name", "u.first_name_en", "u.last_name_en"), trans("Search by name or login")));
		$processor->add_sql_filter(new foreign_key_sql_filter("city", trans("City"), "u.city_id", "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0"));
		$processor->add_sql_filter(new foreign_key_sql_filter("group", trans("Group"), "l.group_id", "group", "title", "id", "stream_id = {$this->stream_id}"));
		$this->add_easy_processor($processor);

		$this->weight_sql_filter = new select_sql_filter("weight", trans("Required total weight"), array(
			"4" => "4",
			"5" => "5",
			"6" => "6",
			"7" => "7",
			"8" => "8",
			"9" => "9",
			"10" => "10",
			"11" => "11",
			"12" => "12",
			"13" => "13",
			"14" => "14",
			"15" => "15",
		));
		// @todo not nice solution + value="(any)" by default (select_sql_filter requires modification)
		$this->weight_sql_filter->set_default_value("3");
		$this->weight_sql_filter->set_inactive_title("3");
		$processor->add_sql_filter($this->weight_sql_filter);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if ($this->dates_ok)
		{
			stream_admin_longterm_marks_helper::build_raw_table($this->stream_obj, $this->start_time, $this->finish_time);
			stream_admin_longterm_marks_helper::build_user_competence_stat_table();
			$weight = $this->weight_sql_filter->get_value();
			stream_admin_longterm_marks_helper::build_user_stat_table($weight ? $weight : 3);
			stream_admin_longterm_marks_helper::build_user_competence_marks_stat_table($this->stream_obj, $this->activity_id);

			$select_sql->add_select_fields("u.id");
			$select_sql->add_select_fields("IFNULL(s.competence_count, 0) AS competence_count");
			$select_sql->add_select_fields("IFNULL(s1.competence_mark_count, 0) AS competence_mark_count");
			$select_sql->add_from("stream_user_link l");
			$select_sql->add_join("JOIN user u ON u.id = l.user_id");
			$select_sql->add_join("LEFT JOIN longterm_user_stat s ON s.user_id = l.user_id");
			$select_sql->add_join("LEFT JOIN longterm_user_competence_marks_stat s1 ON s1.user_id = l.user_id");
			$select_sql->add_where("l.stream_id = {$this->stream_id}");
			$select_sql->add_where("l.status <> 'pretender' AND l.status <> 'deleted'");
			$select_sql->add_order("s.competence_count DESC");
			$this->data = $this->db->fetch_all($select_sql->get_sql());
		}
	}

}

?>