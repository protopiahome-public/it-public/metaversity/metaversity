<?php

class stream_admin_groups_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "group";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("city", "city_title ASC", "city_title DESC");
		$processor->add_order("level", "study_level_title ASC", "study_level_title DESC");
		$processor->add_order("users", "user_count_calc DESC", "user_count_calc ASC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("`group` g");
		$select_sql->add_select_fields("g.id, g.title, g.user_count_calc");
		$select_sql->add_where("g.stream_id = {$this->stream_id}");
		$select_sql->add_join("LEFT JOIN city c ON c.id = g.city_id");
		$select_sql->add_select_fields($this->lang->get_current_lang_code() === "en" ? "c.title_en AS city_title" : "c.title AS city_title");
		$select_sql->add_join("LEFT JOIN study_level sl ON sl.id = g.study_level_id");
		$select_sql->add_select_fields("sl.title AS study_level_title");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>