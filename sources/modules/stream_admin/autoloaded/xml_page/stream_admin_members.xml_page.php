<?php

class stream_admin_members_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "user";
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "user_short",
		),
	);
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct($stream_id, $page)
	{
		$this->stream_id = $stream_id;
		$this->page = $page;

		$this->stream_obj = stream_obj::instance($this->stream_id);

		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", trans("Name/Login"), array("u.login", "u.first_name", "u.last_name", "u.first_name_en", "u.last_name_en"), trans("Search by name or login")));
		$processor->add_sql_filter(new foreign_key_sql_filter("city", trans("City"), "u.city_id", "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0"));
		$processor->add_sql_filter(new foreign_key_sql_filter("group", trans("Group"), "l.group_id", "group", "title", "id", "stream_id = {$this->stream_id}"));
		$this->add_easy_processor($processor);

		$processor = new sort_easy_processor();
		$processor->add_order("time", "l.add_time DESC", "l.add_time ASC");
		if ($this->lang->get_current_lang_code() === "en")
		{
			$processor->add_order("name", "u.first_name_en ASC, u.last_name_en ASC", "u.first_name_en DESC, u.last_name_en DESC");
		}
		else
		{
			$processor->add_order("name", "u.first_name ASC, u.last_name ASC", "u.first_name DESC, u.last_name DESC");
		}
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("user u");
		$select_sql->add_select_fields("u.id, l.add_time");
		$select_sql->add_join("JOIN stream_user_link l ON l.user_id = u.id");
		$select_sql->add_join("LEFT JOIN `group` g ON g.id = l.group_id");
		$select_sql->add_select_fields("g.id AS group_id, g.title AS group_title");
		$select_sql->add_where("l.stream_id = {$this->stream_id}");
		$select_sql->add_where("l.status <> 'pretender' AND l.status <> 'deleted'");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>