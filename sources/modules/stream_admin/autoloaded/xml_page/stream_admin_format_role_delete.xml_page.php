<?php

class stream_admin_format_role_delete_xml_page extends base_delete_xml_ctrl
{

	// Settings
	protected $db_table = "role";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $format_id;

	public function __construct(stream_obj $stream_obj, $format_id, $role_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->format_id = $format_id;
		parent::__construct($role_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
		$select_sql->add_where("format_id = {$this->format_id}");
	}

}

?>