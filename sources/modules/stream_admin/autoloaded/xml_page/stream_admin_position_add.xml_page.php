<?php

class stream_admin_position_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	protected $dt_name = "position";
	protected $axis_name = "edit";
	// Internal
	protected $options = null;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;

	public function __construct(stream_obj $stream_obj)
	{
		$this->options = array(
			0 => "—",
			1 => trans("Useful", "COMPETENCE_IMPORTANCE"),
			2 => trans("Required", "COMPETENCE_IMPORTANCE"),
			3 => trans("Key competence", "COMPETENCE_IMPORTANCE"),
		);
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, false, true, $this->stream_id));
		$this->xml_loader->add_xml(new rate_xml_ctrl(0, $this->competence_set_id, $this->options));
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_id($this->stream_id);
	}

}

?>