<?php

class stream_admin_activity_edit_roles_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "role";
	protected $xml_attrs = array("activity_id");
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;

	public function __construct(stream_obj $stream_obj, $activity_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->activity_id = $activity_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$row = activity_helper::get_row($this->activity_id);
		if (!$row || $row["stream_id"] != $this->stream_id || activity_helper::is_longterm($row))
		{
			$this->set_error_404();
			return;
		}
		$this->data = activity_roles_helper::fetch_roles($this->activity_id, true);
		$format_id = $row["format_id"];
		if ($format_id)
		{
			$this->xml_loader->add_xml(new stream_study_level_restrictions_xml_ctrl($this->stream_id, "role", "role_study_level_link", "o.format_id = {$format_id}", $row["study_level_id"]));
		}
	}

}

?>