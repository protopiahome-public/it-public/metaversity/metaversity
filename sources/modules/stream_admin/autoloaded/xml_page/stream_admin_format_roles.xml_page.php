<?php

class stream_admin_format_roles_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "role";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $format_id;

	public function __construct(stream_obj $stream_obj, $format_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->format_id = $format_id;
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("number", "total_number DESC", "total_number ASC");
		$processor->add_order("auto-accept", "auto_accept DESC", "auto_accept ASC");
		$processor->add_order("competences", "competence_count_calc DESC", "competence_count_calc ASC");
		$this->add_easy_processor($processor);

		$current_level_id = null;
		$filter = GET_AS_ARRAY("filter");
		if (isset($filter["level"]) and is_good_id($filter["level"]))
		{
			$current_level_id = $filter["level"];
		}
		$this->xml_loader->add_xml(new stream_study_level_restrictions_xml_ctrl($this->stream_id, "role", "role_study_level_link", "o.format_id = {$this->format_id}", $current_level_id));

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new multi_link_or_empty_sql_filter("study_level", trans("Study level"), "role.id", "role_study_level_link", "role_id", "study_level_id", "study_level", "title", "id", "stream_id = {$this->stream_id}"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("role");
		$select_sql->add_select_fields("id, total_number, auto_accept, title, competence_count_calc");
		$select_sql->add_where("stream_id = {$this->stream_id}");
		$select_sql->add_where("format_id = {$this->format_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>