<?php

class stream_admin_integral_competences_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "competence";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("id", "id ASC", "id DESC");
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("number", "integral_included_competence_count_calc DESC", "integral_included_competence_count_calc ASC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("c.id, c.title, c.integral_included_competence_count_calc");
		$select_sql->add_from("competence_full c");
		$select_sql->add_where("c.competence_set_id = {$this->competence_set_id}");
		$select_sql->add_where("c.is_integral = 1");
		$select_sql->add_where("c.is_deleted = 0");
		$select_sql->add_order("c.id");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>