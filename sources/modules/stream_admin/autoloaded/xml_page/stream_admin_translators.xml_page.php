<?php

class stream_admin_translators_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "translator";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "s.title ASC", "s.title DESC");
		$processor->add_order("direct-count", "direct_count_calc DESC", "direct_count_calc ASC");
		$processor->add_order("formulae-count", "formulae_count_calc DESC", "formulae_count_calc ASC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("t.eq_competence_set_id, s.title AS eq_competence_set_title");
		$select_sql->add_select_fields("t.direct_count_calc, t.formulae_count_calc");
		$select_sql->add_from("competence_translator t");
		$select_sql->add_join("JOIN competence_set s ON s.id = t.eq_competence_set_id");
		$select_sql->add_where("t.competence_set_id = {$this->competence_set_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>