<?php

class stream_admin_study_materials_xml_page extends base_easy_xml_ctrl
{
	// Settings (base class)
	protected $xml_row_name = "study_material";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("add_time", "add_time DESC", "add_time ASC", true);
		$processor->add_order("competences", "competence_count_calc DESC", "competence_count_calc ASC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("study_material");
		$select_sql->add_where("stream_id = {$this->stream_id}");
		$select_sql->add_select_fields("id, title, competence_count_calc, is_outer, outer_url, add_time");
		$select_sql->add_order("position");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>