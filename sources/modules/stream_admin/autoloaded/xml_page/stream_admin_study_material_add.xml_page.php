<?php

class stream_admin_study_material_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("outer");

	/**
	 * @var study_material_dt
	 */
	protected $dt;
	protected $dt_name = "study_material";
	protected $axis_name = "edit";
	// Internal
	protected $options = null;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $outer;

	public function __construct(stream_obj $stream_obj, $outer = false)
	{
		$this->options = array(
			0 => "—",
			1 => trans("Improves indirectly", "COMPETENCE_IMPORTANCE"),
			2 => trans("Improves", "COMPETENCE_IMPORTANCE"),
		);
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		$this->outer = $outer;
		if ($outer)
		{
			$this->axis_name = "edit_outer";
		}
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, false, true, $this->stream_id));
		$this->xml_loader->add_xml(new rate_xml_ctrl(0, $this->competence_set_id, $this->options));
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
	}

}

?>