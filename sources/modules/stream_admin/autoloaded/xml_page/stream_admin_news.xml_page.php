<?php

class stream_admin_news_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "news_item_short",
			"param2" => "stream_id",
		),
	);
	protected $xml_row_name = "news_item";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $page;

	public function __construct(stream_obj $stream_obj, $page)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sql_filters_easy_processor();

		$this->city_sql_filter = new foreign_key_sql_filter("city", trans("City"), "city_id", "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0");
		$this->city_sql_filter->set_inactive_title(trans("Any city"));
		$processor->add_sql_filter($this->city_sql_filter);

		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("n.id");
		$select_sql->add_from("news_item n");
		$select_sql->add_where("n.stream_id = {$this->stream_id}");
		$select_sql->add_order("n.id DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>