<?php

class stream_admin_integral_competence_add_xml_page extends base_dt_add_xml_ctrl
{

	// Internal
	protected $competence_set_id;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new stream_admin_integral_competences_xml_page($this->stream_obj));
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, false, true, $this->stream_id));
	}

	public function get_xml()
	{
		$xdom = xdom::create($this->name);
		$xdom->set_attr("competence_set_id", $this->competence_set_id);
		return $xdom->get_xml(true);
	}

}

?>