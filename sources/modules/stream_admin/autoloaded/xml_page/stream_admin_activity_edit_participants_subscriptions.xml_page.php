<?php

class stream_admin_activity_edit_participants_subscriptions_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	protected $xml_row_name = "user";
	protected $xml_attrs = array("activity_id");
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;

	public function __construct(stream_obj $stream_obj, $activity_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->activity_id = $activity_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$row = activity_helper::get_row($this->activity_id);
		if (!$row || $row["stream_id"] != $this->stream_id || activity_helper::is_longterm($row))
		{
			$this->set_error_404();
			return;
		}
		$this->data = activity_participants_subscriptions_helper::fetch_participants_subscriptions_to_edit($this->activity_id);
	}

}

?>