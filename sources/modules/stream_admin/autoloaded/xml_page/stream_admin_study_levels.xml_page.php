<?php

class stream_admin_study_levels_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "study_level";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("groups", "group_count_calc DESC", "group_count_calc ASC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("study_level");
		$select_sql->add_select_fields("id, title, group_count_calc");
		$select_sql->add_where("stream_id = {$this->stream_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>