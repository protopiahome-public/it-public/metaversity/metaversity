<?php

class stream_admin_activity_edit_longterm_marks_user_competence_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("stream_id", "activity_id", "user_id", "start_time", "finish_time", "competence_id", "competence_title");
	protected $xml_row_name = "news_item";
	// Internal
	protected $page;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $user_id;
	protected $competence_id;
	protected $competence_row;
	protected $competence_title;
	protected $start_time = null;
	protected $finish_time = null;

	/**
	 * @var select_sql_filter
	 */
	protected $sort_mode_sql_filter;

	public function __construct(stream_obj $stream_obj, $activity_id, $user_id, $competence_id, $page)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->activity_id = $activity_id;
		$this->user_id = $user_id;
		$this->competence_id = $competence_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$row = activity_helper::get_row($this->activity_id);
		if (!$row || $row["stream_id"] != $this->stream_id || !activity_helper::is_longterm($row))
		{
			$this->set_error_404();
			return;
		}

		if (!stream_admin_longterm_marks_helper::set_times($this->activity_id, $this->start_time, $this->finish_time))
		{
			$this->set_error_404();
			return;
		}

		$this->load_competence_row();
		if (!$this->competence_row)
		{
			$this->set_error_404();
			return;
		}
		$this->competence_title = $this->competence_row["title"];

		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, 50));
	}

	private function load_competence_row()
	{
		$this->competence_row = $this->db->get_row("
			SELECT competence_id AS id, title
			FROM competence_calc
			WHERE competence_id = {$this->competence_id} AND competence_set_id = {$this->stream_obj->get_competence_set_id()}
		");
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->data = stream_admin_longterm_marks_helper::get_user_marks($this->stream_obj, $this->start_time, $this->finish_time, $this->user_id, $this->competence_id, $this->activity_id);
	}

	protected function postprocess()
	{
		news_ctrl_loader_helper::load_xml_ctrls($this->data, $this->xml_loader);
	}

}

?>