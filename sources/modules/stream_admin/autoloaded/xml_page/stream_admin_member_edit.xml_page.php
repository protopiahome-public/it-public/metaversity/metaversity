<?php

class stream_admin_member_edit_xml_page extends base_dt_edit_xml_ctrl
{

	protected $dependencies_settings = array(
		array(
			"column" => "user_id",
			"ctrl" => "user_short",
		),
	);
	// Settings
	/**
	 * @var stream_user_link_dt
	 */
	protected $dt;
	protected $dt_name = "stream_user_link";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $user_id;

	/**
	 * @var stream_access
	 */
	protected $stream_access;
	protected $page_type;

	public function __construct(stream_obj $stream_obj, $id, $page_type)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->user_id = $id["user_id"];
		$this->stream_access = new stream_access($this->stream_obj, $this->user_id);
		$this->page_type = $page_type;
		parent::__construct($id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_id($this->stream_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		if (!$this->stream_access->is_member() and !$this->stream_access->is_pretender_strict())
		{
			$this->set_error_404();
		}
	}
	
	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("page_type", $this->page_type);
	}

}

?>