<?php

class stream_admin_activity_edit_general_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $dt_name = "activity";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj, $id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct($id);
	}

	public function init()
	{
		if (!activity_edit_helper::can_edit_format($this->id))
		{
			$this->axis_name = "edit_without_format";
		}
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->create_child_node("groups_info", levels_and_groups_helper::get_groups_info_json($this->stream_id));
	}

}

?>