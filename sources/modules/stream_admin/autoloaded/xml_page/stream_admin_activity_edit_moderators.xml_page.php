<?php

class stream_admin_activity_edit_moderators_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $dt_name = "activity";
	protected $axis_name = "edit_moderators";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj, $id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct($id);
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
	}

}

?>