<?php

class stream_admin_metaactivities_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "metaactivity_short",
			"param2" => "stream_id",
		),
	);
	protected $xml_row_name = "metaactivity";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $page;

	public function __construct(stream_obj $stream_obj, $page)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("ma.id");
		$select_sql->add_from("`metaactivity` ma");
		$select_sql->add_where("ma.stream_id = {$this->stream_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>