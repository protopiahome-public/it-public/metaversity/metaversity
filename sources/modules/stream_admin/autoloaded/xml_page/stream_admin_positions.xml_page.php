<?php

class stream_admin_positions_xml_page extends base_easy_xml_ctrl
{

	// Settings (base class)
	protected $xml_row_name = "position";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	
	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function init()
	{
		$processor = new sort_easy_processor();
		$processor->add_order("title", "title ASC", "title DESC");
		$processor->add_order("competences", "competence_count_calc DESC", "competence_count_calc ASC");
		$this->add_easy_processor($processor);
		
		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new multi_link_or_empty_sql_filter("level", trans("Study level"), "id", "position_study_level_link", "position_id", "study_level_id", "study_level", "title", "id", "stream_id = {$this->stream_id}"));
		$this->add_easy_processor($processor);
		
		$this->xml_loader->add_xml(new stream_study_level_restrictions_xml_ctrl($this->stream_id, "position", "position_study_level_link"));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id, title, competence_count_calc");
		$select_sql->add_from("position");
		$select_sql->add_where("stream_id = {$this->stream_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>