<?php

class stream_admin_import_users_xml_page extends base_xml_ctrl
{

	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct($stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function get_xml()
	{
		if (!$this->stream_obj->users_import_allowed())
		{
			$this->set_error_404();
			return;
		}
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		return $this->get_node_string();
	}

}

?>