<?php

class stream_admin_metaactivity_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	/**
	 * @var metaactivity_dt
	 */
	protected $dt;
	protected $dt_name = "metaactivity";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}
	
}

?>