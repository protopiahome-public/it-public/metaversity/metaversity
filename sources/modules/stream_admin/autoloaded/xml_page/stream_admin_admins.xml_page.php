<?php

class stream_admin_admins_xml_page extends base_easy_xml_ctrl
{

	protected $dependencies_settings = array(
		array(
			"column" => "user_id",
			"ctrl" => "user_short",
		),
	);
	protected $stream_id;
	protected $xml_row_name = "admin";
	protected $page;

	public function __construct($stream_id, $page)
	{
		$this->stream_id = $stream_id;
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("da.*");
		$select_sql->add_from("stream_admin da");
		$select_sql->add_join("LEFT JOIN stream_user_link l ON da.user_id = l.user_id AND da.stream_id = l.stream_id");
		$select_sql->add_where("da.stream_id = {$this->stream_id} AND da.is_admin = 1 AND l.status = 'admin'");
		$select_sql->add_order("da.add_time DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>