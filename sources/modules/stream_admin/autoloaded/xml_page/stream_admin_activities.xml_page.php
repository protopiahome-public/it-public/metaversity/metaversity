<?php

class stream_admin_activities_xml_page extends base_activities_xml_ctrl
{

	protected function fetch_data(select_sql $select_sql)
	{
		return activities_fetch_helper::load_data($select_sql, 0, false, $this->filters, $this->stream_id, true);
	}

}

?>