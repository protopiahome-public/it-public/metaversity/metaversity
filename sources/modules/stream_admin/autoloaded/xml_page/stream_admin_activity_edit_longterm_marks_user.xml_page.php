<?php

class stream_admin_activity_edit_longterm_marks_user_xml_page extends base_easy_xml_ctrl
{

	protected $dependencies_settings = array(
		array(
			"column" => "changer_user_id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	// Settings
	protected $xml_attrs = array("stream_id", "activity_id", "user_id", "start_time", "finish_time");
	protected $xml_row_name = "competence";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $user_id;
	protected $start_time = null;
	protected $finish_time = null;

	/**
	 * @var select_sql_filter
	 */
	protected $sort_mode_sql_filter;

	public function __construct(stream_obj $stream_obj, $activity_id, $user_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->activity_id = $activity_id;
		$this->user_id = $user_id;
		parent::__construct();
	}

	public function init()
	{
		$row = activity_helper::get_row($this->activity_id);
		if (!$row || $row["stream_id"] != $this->stream_id || !activity_helper::is_longterm($row))
		{
			$this->set_error_404();
			return;
		}

		if (!stream_admin_longterm_marks_helper::set_times($this->activity_id, $this->start_time, $this->finish_time))
		{
			$this->set_error_404();
			return;
		}

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter($this->sort_mode_sql_filter = new select_sql_filter("sort-mode", trans("Sort mode"), array(
			"tree" => trans("Tree mode"),
		)));
		$this->sort_mode_sql_filter->set_default_value("weight");
		$this->sort_mode_sql_filter->set_inactive_title(trans("Sort by total weight"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		stream_admin_longterm_marks_helper::build_raw_table($this->stream_obj, $this->start_time, $this->finish_time, $this->user_id);
		stream_admin_longterm_marks_helper::build_user_competence_stat_table();

		$select_sql->add_select_fields("competence_id AS id, total_weight");
		$select_sql->add_from("longterm_user_competence_stat");
		$select_sql->add_where("user_id = {$this->user_id}");
		$select_sql->add_order("total_weight DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql(), "id");

		$marks = stream_admin_longterm_marks_helper::get_user_competence_marks($this->stream_obj, $this->user_id, $this->activity_id);
		foreach ($marks as $competence_id => $mark_data)
		{
			array_key_set($this->data, $competence_id, array(
				"id" => $competence_id,
			));
			$this->data[$competence_id]["mark"] = $mark_data["mark"];
			$this->data[$competence_id]["weight"] = $mark_data["weight"];
			$this->data[$competence_id]["comment"] = $mark_data["comment"];
			$this->data[$competence_id]["changer_user_id"] = $mark_data["changer_user_id"];
		}
	}

}

?>