<?php

class stream_admin_formats_xml_page extends base_xml_ctrl
{

	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function get_xml()
	{
		return $this->get_node_string($this->name, array(
				"stream_id" => $this->stream_id,
		));
	}

}

?>