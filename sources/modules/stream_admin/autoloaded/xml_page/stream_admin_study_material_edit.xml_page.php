<?php

class stream_admin_study_material_edit_xml_page extends base_dt_edit_xml_ctrl
{

	// Settings
	/**
	 * @var study_material_dt
	 */
	protected $dt;
	protected $dt_name = "study_material";
	protected $axis_name = "edit";
	// Internal
	protected $options = null;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;

	public function __construct(stream_obj $stream_obj, $id)
	{
		$this->options = array(
			0 => "—",
			1 => trans("Improves indirectly", "COMPETENCE_IMPORTANCE"),
			2 => trans("Improves", "COMPETENCE_IMPORTANCE"),
		);
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		parent::__construct($id);
		$is_outer = $this->db->get_value("SELECT is_outer FROM study_material WHERE id = {$id}");
		if ($is_outer)
		{
			$this->axis_name = "edit_outer";
		}
	}

	public function init()
	{
		$this->xml_loader->add_xml(new competence_set_competences_xml_ctrl($this->competence_set_id, false, true, $this->stream_id));
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_select_fields("dt.rate_id, dt.is_outer, dt.outer_url");
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("is_outer", $this->data[0]["is_outer"]);
		$xdom->set_attr("outer_url", $this->data[0]["outer_url"]);
		parent::modify_xml($xdom);
	}

	public function postprocess()
	{
		$this->xml_loader->add_xml(new rate_xml_ctrl($this->data[0]["rate_id"], $this->competence_set_id, $this->options));
	}

}

?>