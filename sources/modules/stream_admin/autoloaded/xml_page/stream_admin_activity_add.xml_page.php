<?php

class stream_admin_activity_add_xml_page extends base_dt_add_xml_ctrl
{

	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $dt_name = "activity";
	protected $axis_name = "edit";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	protected function on_after_dt_init()
	{
		$this->dt->set_stream_obj($this->stream_obj);
		$this->dt->disable_archived_formats();
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->create_child_node("groups_info", levels_and_groups_helper::get_groups_info_json($this->stream_id));
	}

}

?>