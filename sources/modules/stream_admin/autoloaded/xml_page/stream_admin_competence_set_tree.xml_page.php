<?php

class stream_admin_competence_set_tree_xml_page extends base_xml_ctrl
{

	private $competence_set_id;
	private $stream_id = null;

	public function __construct($competence_set_id, $stream_id = null)
	{
		$this->competence_set_id = $competence_set_id;
		$this->stream_id = $stream_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		$xdom = xdom::create($this->name);
		$xdom->set_attr("competence_set_id", $this->competence_set_id);

		$multi_link_exists = $this->db->row_exists("
			SELECT c.id, c.competence_set_id, count(l.competence_group_id) as link_count
			FROM competence_full c
			LEFT JOIN competence_link l ON (c.id = l.competence_id)
			WHERE c.competence_set_id = {$this->competence_set_id}
			GROUP BY c.id
			HAVING link_count > 1
			ORDER BY link_count DESC
		");
		if ($multi_link_exists)
		{
			$xdom->set_attr("multi_link", true);
		}

		if (!$this->stream_id)
		{
			$this->stream_id = competence_set_helper::get_corresponding_stream_id($this->competence_set_id);
		}
		if ($this->stream_id)
		{
			//dd(competence_restrictions_helper::get_competence_groups_restrictions($this->competence_set_id, $this->stream_id));
			$xdom->set_attr("stream_id", $this->stream_id);
			$this->xml_loader->add_xml(new stream_short_xml_ctrl($this->stream_id));
			competence_restrictions_helper::build_study_levels_xml($xdom, $this->stream_id);
		}
		else
		{
			$xdom->set_attr("stream_count", competence_set_helper::get_stream_count($this->competence_set_id));
		}

		return $xdom->get_xml(true);
	}

}

?>