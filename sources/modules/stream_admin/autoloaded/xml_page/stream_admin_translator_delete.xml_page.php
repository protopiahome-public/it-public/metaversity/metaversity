<?php

class stream_admin_translator_delete_xml_page extends base_xml_ctrl
{

	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $competence_set_id;
	protected $eq_competence_set_id;

	public function __construct(stream_obj $stream_obj, $eq_competence_set_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->competence_set_id = $this->stream_obj->get_competence_set_id();
		$this->eq_competence_set_id = $eq_competence_set_id;
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new competence_set_short_xml_ctrl($this->eq_competence_set_id));
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		if (!competence_translator_helper::translator_exists($this->competence_set_id, $this->eq_competence_set_id))
		{
			$this->set_error_404();
		}
		
		$xdom = xdom::create($this->name);
		$xdom->set_attr("eq_competence_set_id", $this->eq_competence_set_id);
		return $xdom->get_xml(true);
	}

}

?>