<?php

class rate_multi_link_save_ctrl extends rate_save_ctrl
{

	public function start()
	{
		if ($this->object_id)
		{
			$this->rate_id = rate_data_math::get_rate_id_multi_link($this->type, $this->object_id, $this->competence_set_id);
		}
		else
		{
			$this->rate_id = 0;
		}
		return parent::start();
	}
	
	public function update_object_table()
	{
		$this->db->sql("
			INSERT INTO `{$this->type}_rate_link` ({$this->type}_id, rate_id, competence_set_id)
			VALUES ({$this->object_id}, {$this->real_rate_id}, {$this->competence_set_id})
		");
	}

	protected function update_object_table_count()
	{
		$this->db->sql("
			UPDATE `{$this->type}_rate_link`
			SET
				competence_count_calc = (
					SELECT COUNT(*)
					FROM rate_competence_link l
					JOIN competence_calc c ON c.competence_id = l.competence_id
					WHERE l.rate_id = {$this->real_rate_id}
				)
			WHERE {$this->type}_id = {$this->object_id} AND competence_set_id = {$this->competence_set_id}
		");
	}

}

?>
