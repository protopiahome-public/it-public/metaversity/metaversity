<?php

class rate_multi_link_xml_ctrl extends rate_xml_ctrl
{

	protected $type;
	protected $object_id;
	protected $competence_set_id;

	public function __construct($type, $object_id, $competence_set_id, $options)
	{
		$this->type = $type;
		$this->object_id = $object_id;
		parent::__construct(0, $competence_set_id, $options);

		$this->name = "rate";
	}

	public function start()
	{
		if (is_good_id($this->object_id))
		{
			$this->rate_id = rate_data_math::get_rate_id_multi_link($this->type, $this->object_id, $this->competence_set_id);
		}

		return true;
	}

	public function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("type", $this->type);
		$xdom->set_attr("object_id", $this->object_id);
		parent::fill_xml($xdom);
	}

}

?>