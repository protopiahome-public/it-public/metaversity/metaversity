<?php

class competence_set_competences_xml_ctrl extends base_easy_xml_ctrl
{

	protected $competence_set_id;
	protected $show_stat;
	protected $show_restrictions;
	protected $stream_id;
	protected $xml_attrs = array("competence_set_id");
	protected $restrictions = null;

	public function __construct($competence_set_id, $show_stat = false, $show_restrictions = false, $stream_id = null)
	{
		$this->competence_set_id = $competence_set_id;
		$this->show_stat = $show_stat;
		$this->show_restrictions = $show_restrictions;
		$this->stream_id = $stream_id;
		if ($this->show_restrictions and !$this->stream_id)
		{
			$this->stream_id = competence_set_helper::get_corresponding_stream_id($this->competence_set_id);
		}

		parent::__construct();
	}

	public function load_data(select_sql $select_sql = null)
	{
		if ($this->show_restrictions)
		{
			$this->restrictions = competence_restrictions_helper::get_restrictions($this->competence_set_id, $this->stream_id);
		}
		// Groups
		$data_raw_plain = $this->db->fetch_all("
			SELECT
				id, title, parent_id
			FROM competence_group
			WHERE competence_set_id = {$this->competence_set_id}
			ORDER BY position, id
		");
		$data_index = array();
		foreach ($data_raw_plain as $row)
		{
			$data_index[$row["id"]] = array(
				"id" => $row["id"],
				"title" => $row["title"],
				"competences" => array(),
				"children" => array(),
			);
			if ($this->show_restrictions)
			{
				$competence_group_key = "g" . $row["id"];
				if (isset($this->restrictions["g"][$competence_group_key]))
				{
					$data_index[$row["id"]]["restrictions"] = $this->restrictions["g"][$competence_group_key];
					if (isset($this->restrictions["g"][$competence_group_key]["prefix"]))
					{
						$data_index[$row["id"]]["title"] = $this->restrictions["g"][$competence_group_key]["prefix"] . $data_index[$row["id"]]["title"];
					}
				}
			}
		}
		$data = array();
		foreach ($data_raw_plain as $row)
		{
			if ($row["parent_id"])
			{
				$data_index[$row["parent_id"]]["children"][] = &$data_index[$row["id"]];
			}
			else
			{
				$data[] = &$data_index[$row["id"]];
			}
		}

		// Competences
		if (sizeof($data_index))
		{
			$db_result = $this->db->sql("
				SELECT
					l.competence_group_id,
					c.competence_id id, c.number, c.title
				FROM competence_link l
				JOIN competence_calc c ON c.competence_id = l.competence_id
				WHERE
					l.competence_group_id IN (" . join(",", array_keys($data_index)) . ")
				ORDER BY l.position, c.competence_id
			");
			while ($row = $this->db->fetch_array($db_result))
			{
				if ($this->show_restrictions)
				{
					$competence_key = "c" . $row["id"];
					if (isset($this->restrictions["c"][$competence_key]))
					{
						$row["restrictions"] = $this->restrictions["c"][$competence_key];
						if (isset($this->restrictions["c"][$competence_key]["prefix"]))
						{
							$row["title"] = $this->restrictions["c"][$competence_key]["prefix"] . $row["title"];
						}
					}
				}
				$data_index[$row["competence_group_id"]]["competences"][] = $row;
			}
		}

		$this->data = $data;
	}

	public function fill_xml(xdom $xdom)
	{
		$this->group_export($xdom, $this->data);

		return $xdom;
	}

	private function group_export(xnode $xnode, $data)
	{
		foreach ($data as $competence_group_data)
		{
			$group_node = $xnode->create_child_node("group")
				->set_attr("id", $competence_group_data["id"])
				->set_attr("title", $competence_group_data["title"]);
			if (isset($competence_group_data["restrictions"]))
			{
				$group_node->set_attr("restrictions", json_encode($competence_group_data["restrictions"]));
			}
			foreach ($competence_group_data["competences"] as $competence_data)
			{
				$competence_node = $group_node->create_child_node("competence")
					->set_attr("id", $competence_data["id"])
					->set_attr("number", $competence_data["number"])
					->set_attr("title", $competence_data["title"]);
				if (isset($competence_data["restrictions"]))
				{
					$competence_node->set_attr("restrictions", json_encode($competence_data["restrictions"]));
				}
			}
			if (sizeof($competence_group_data["children"]))
			{
				$this->group_export($group_node, $competence_group_data["children"]);
			}
		}
	}

	public function get_cache()
	{
		if (!$this->show_stat)
		{
			return competence_set_competences_cache::init($this->competence_set_id, $this->stream_id);
		}
	}

}

?>