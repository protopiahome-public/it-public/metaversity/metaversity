<?php

class competence_title_xml_ctrl extends base_easy_xml_ctrl
{

	protected $competence_id;

	public function __construct($competence_id)
	{
		$this->competence_id = $competence_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("id, title");
		$select_sql->add_from("competence_full");
		$select_sql->add_where("id = {$this->competence_id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>