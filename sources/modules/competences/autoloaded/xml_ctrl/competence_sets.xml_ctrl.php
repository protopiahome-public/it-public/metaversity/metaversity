<?php

class competence_sets_xml_ctrl extends base_easy_xml_ctrl
{

	protected $xml_row_name = "competence_set";

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("competence_set s");
		$select_sql->add_select_fields("s.id, s.title");
		$select_sql->add_order("s.title");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>