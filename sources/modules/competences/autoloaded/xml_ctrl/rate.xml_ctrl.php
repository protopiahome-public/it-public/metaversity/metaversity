<?php

class rate_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("competence_set_id");
	// Internal
	protected $rate_id; // Zero for rate addition
	protected $competence_set_id;

	/**
	 * Options keys and values
	 * Ex.: 1 => "one", 2 => "two", 3 => "three"
	 */
	protected $options = array();
	protected $focus_data;
	protected $potental_details;

	public function __construct($rate_id, $competence_set_id, $options, $data = null, $focus_data = array(), $potental_details = null)
	{
		$this->rate_id = $rate_id;
		$this->competence_set_id = $competence_set_id;
		$this->options = $options;
		$this->data = $data;
		$this->focus_data = $focus_data;
		$this->potental_details = $potental_details;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if (!$this->rate_id)
		{
			$this->data = array();
			return;
		}

		if (!$this->data) // Not set in constructor
		{
			$rate_data_math = rate_data_math::get_instance($this->rate_id, $this->competence_set_id);
			$this->data = $rate_data_math->get_data();
		}

		foreach ($this->data as $competence_id => &$competence_data)
		{
			if (isset($this->focus_data[$competence_id]))
			{
				$competence_data["focus"] = true;
			}
			if (!is_null($this->potental_details))
			{
				$competence_data["potential_impact"] = isset($this->potental_details[$competence_id]) ? $this->potental_details[$competence_id] : 0;
			}
		}
		unset($competence_data);

		if ($this->focus_data)
		{
			usort($this->data, array($this, "__sort"));
		}
	}

	private function __sort($a, $b)
	{
		if (!is_null($this->potental_details))
		{
			// potential_impact DESC
			if ($a["potential_impact"] > $b["potential_impact"])
			{
				return -1;
			}
			if ($a["potential_impact"] < $b["potential_impact"])
			{
				return 1;
			}
		}
		// mark DESC
		if ($a["mark"] > $b["mark"])
		{
			return -1;
		}
		if ($a["mark"] < $b["mark"])
		{
			return 1;
		}
		// focus DESC
		if (isset($a["focus"]) && !isset($b["focus"]))
		{
			return -1;
		}
		if (isset($b["focus"]) && !isset($a["focus"]))
		{
			return 1;
		}
		// id ASC
		if ($a["id"] > $b["id"])
		{
			return 1;
		}
		if ($a["id"] < $b["id"])
		{
			return -1;
		}
		return 0;
	}

	protected function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("id", $this->rate_id);

		$options_node = $xdom->create_child_node("options");
		foreach ($this->options as $mark => $title)
		{
			$options_node->create_child_node("option")
				->set_attr("mark", $mark)
				->set_attr("mark_title", $title);
		}

		if (!is_null($this->potental_details))
		{
			foreach ($this->data as &$competence_data)
			{
				$competence_data["potential_impact"] = potential_math_common_helper::potential_impact_number_format($competence_data["potential_impact"]);
			}
		}
		unset($competence_data);

		$this->db_xml_converter->build_xml($xdom, $this->data, false, "competence");
	}

}

?>