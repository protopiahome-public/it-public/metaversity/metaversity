<?php

class competence_set_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	protected $dt_name = "competence_set";
	protected $axis_name = "short";

	protected function get_cache()
	{
		return competence_set_short_cache::init($this->id);
	}

}

?>