<?php

class competence_set_before_start_mixin extends base_mixin
{

	protected $competence_set_id;

	/**
	 * @var competence_set_obj
	 */
	protected $competence_set_obj;

	public function on_before_start()
	{
		$this->competence_set_id = $this->get_competence_set_id();
		if (!is_good_id($this->competence_set_id))
		{
			return false;
		}
		
		$this->competence_set_obj = new competence_set_obj($this->competence_set_id, $lock = true);
		if (!$this->competence_set_obj->exists())
		{
			return false;
		}
		
		return true;
	}

	protected function get_competence_set_id()
	{
		return REQUEST("competence_set_id");
	}

}

?>