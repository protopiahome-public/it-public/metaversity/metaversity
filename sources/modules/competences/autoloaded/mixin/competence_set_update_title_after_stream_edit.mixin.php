<?php

class competence_set_update_title_after_stream_edit_mixin extends base_mixin
{

	protected $stream_id;
	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function on_after_commit()
	{
		$competence_set_id = $this->stream_obj->get_competence_set_id();
		if (1 == competence_set_stream_relation_helper::get_stream_count($competence_set_id))
		{
			$this->db->sql("
				UPDATE competence_set cs
				SET cs.title = (
					SELECT d.title
					FROM stream d
					WHERE d.id = {$this->stream_id}
				)
				WHERE cs.id = {$competence_set_id}
			");
		}
		return true;
	}

}

?>