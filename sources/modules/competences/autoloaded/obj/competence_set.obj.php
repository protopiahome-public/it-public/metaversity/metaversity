<?php

class competence_set_obj extends base_obj
{
	// Settings
	protected $fake_delete = false;
	// Internal
	private static $cache = array();

	/**
	 * @return competence_set_obj
	 */
	public static function instance($id, $lock = false)
	{
		if (!$lock && isset(self::$cache[$id]))
		{
			return self::$cache[$id];
		}
		$class = __CLASS__;
		$instance = new $class($id, $lock);
		if (!$lock)
		{
			self::$cache[$id] = $instance;
		}
		return $instance;
	}

	public function get_title()
	{
		return $this->get_param("title");
	}

	protected function fill_data($lock)
	{
		if ($lock)
		{
			$this->data = $this->fetch_data(true);
		}
		else
		{
			$this->data = $this->fetch_data();
			if (!$this->data)
			{
				$this->data = array();
			}
		}
	}

	private function fetch_data($lock = false)
	{
		$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
		$row = $this->db->get_row("
			SELECT *
			FROM competence_set
			WHERE id = {$this->id}
			{$lock_sql}
		");
		return $row ? $row : null;
	}

}

?>