<?php

class competence_set_dt extends base_dt
{

	protected function init()
	{
		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf);

		$dtf = new text_html_dtf("descr", trans("Description"));
		$dtf->set_editor_height(240);
		$this->add_field($dtf);

		$this->add_fields_in_axis("short", array("title"));
		$this->add_fields_in_axis("edit", array("title", "descr"));
	}

}

?>