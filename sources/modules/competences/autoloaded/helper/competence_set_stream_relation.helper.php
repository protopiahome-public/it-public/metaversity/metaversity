<?php

class competence_set_stream_relation_helper extends base_static_db_helper
{

	public static function get_stream_count($competence_set_id)
	{
		$count = self::db()->get_value("
			SELECT COUNT(*)
			FROM stream
			WHERE competence_set_id = {$competence_set_id}
		");
		return $count;
	}

	public static function get_corresponding_stream_ids($competence_set_id)
	{
		$stream_ids = self::db()->fetch_column_values("
			SELECT id
			FROM stream
			WHERE competence_set_id = {$competence_set_id}
		");
		return $stream_ids;
	}

	public static function get_corresponding_stream_id($competence_set_id, $get_any = false)
	{
		$stream_ids = self::get_corresponding_stream_ids($competence_set_id);
		if ($get_any)
		{
			return sizeof($stream_ids) > 0 ? $stream_ids[0] : null;
		}
		else
		{
			return sizeof($stream_ids) === 1 ? $stream_ids[0] : null;
		}
	}

}

?>