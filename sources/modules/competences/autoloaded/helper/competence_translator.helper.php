<?php

class competence_translator_helper extends base_static_db_helper
{

	const RE_FLOAT = "\d(\.\d)?";
	const RE_MAX = "(\s*\/\/\s*MAX\s*=\s*(\d(\.\d)?))?";

	public static function translator_exists($competence_set_id, $eq_competence_set_id)
	{
		return self::db()->row_exists("
			SELECT * 
			FROM competence_translator 
			WHERE competence_set_id = {$competence_set_id} 
				AND eq_competence_set_id = {$eq_competence_set_id}
		");
	}

	public static function parse_text($competence_set_id, $eq_competence_set_id, $text, $skip_left_aliens = false, $skip_direct_right_aliens = false, $auto_add_via_titles = false)
	{
		$direct = array();
		if ($auto_add_via_titles)
		{
			$direct = self::db()->fetch_all("
				SELECT 
					c1.competence_id AS competence_id, 
					c2.competence_id AS eq_competence_id,
					NULL AS eq_max_mark
				FROM competence_calc c1
				JOIN competence_calc c2 ON c1.title = c2.title
				WHERE c1.competence_set_id = {$competence_set_id} AND c2.competence_set_id = {$eq_competence_set_id}
			", "competence_id");
		}
		$formulae = array();
		$errors = array();
		$competence_ids = self::get_competences($competence_set_id);
		$eq_competence_ids = self::get_competences($eq_competence_set_id);
		$lines = explode("\n", $text);
		foreach ($lines as $line_idx => $line)
		{
			$line = trim($line);
			if (!strlen($line))
			{
				continue;
			}
			$matches = null;
			if (preg_match("/^(\d+)\s*=\s*(\d+)" . self::RE_MAX . "$/i", $line, $matches))
			{
				$competence_id = $matches[1];
				$eq_competence_id = $matches[2];
				$eq_max_mark = isset($matches[4]) && $matches[4] ? (float) $matches[4] : -1;
				if (!is_good_id($competence_id) || !is_good_id($eq_competence_id))
				{
					$errors[$line_idx + 1] = array("BAD_FORMAT");
				}
				elseif (!isset($competence_ids[$competence_id]))
				{
					if ($skip_left_aliens)
					{
						continue;
					}
					$errors[$line_idx + 1] = array("ALIEN_LEFT", $competence_id);
				}
				elseif (!isset($eq_competence_ids[$eq_competence_id]))
				{
					if ($skip_direct_right_aliens)
					{
						continue;
					}
					$errors[$line_idx + 1] = array("ALIEN_RIGHT", $eq_competence_id);
				}
				elseif ($eq_max_mark > 3.0)
				{
					$errors[$line_idx + 1] = array("BAD_MAX_FORMAT", self::number_format($eq_max_mark));
				}
				else
				{
					$direct[$competence_id] = array(
						"competence_id" => $competence_id,
						"eq_competence_id" => $eq_competence_id,
						"eq_max_mark" => $eq_max_mark >= 0 ? self::number_format($eq_max_mark) : null,
					);
				}
			}
			elseif (preg_match("/^(\d+)\s*=\s*(MIN|AVG)\s*\(([\d,\s]+)\)" . self::RE_MAX . "$/i", $line, $matches))
			{
				$competence_id = $matches[1];
				$eq_formula = strtoupper($matches[2]);
				$list = $matches[3];
				$eq_max_mark = isset($matches[5]) && $matches[5] ? (float) $matches[5] : -1;
				if (!is_good_id($competence_id))
				{
					$errors[$line_idx + 1] = array("BAD_FORMAT");
				}
				elseif (!isset($competence_ids[$competence_id]))
				{
					if ($skip_left_aliens)
					{
						continue;
					}
					$errors[$line_idx + 1] = array("ALIEN_LEFT", $competence_id);
				}
				elseif ($eq_max_mark > 3.0)
				{
					$errors[$line_idx + 1] = array("BAD_MAX_FORMAT", self::number_format($eq_max_mark));
				}
				else
				{
					$eq_competence_list = array();
					$ok = true;
					foreach (explode(",", $list) as $eq_part_competence_id)
					{
						$eq_part_competence_id = trim($eq_part_competence_id);
						if (!$eq_part_competence_id)
						{
							continue;
						}
						if (!is_good_id($eq_part_competence_id))
						{
							$errors[$line_idx + 1] = array("BAD_FORMAT");
							$ok = false;
							break;
						}
						elseif (!isset($eq_competence_ids[$eq_part_competence_id]))
						{
							$errors[$line_idx + 1] = array("ALIEN_RIGHT", $eq_part_competence_id);
							$ok = false;
							break;
						}
						else
						{
							$eq_competence_list[$eq_part_competence_id] = -1;
						}
					}
					if ($ok)
					{
						if (!sizeof($eq_competence_list))
						{
							$errors[$line_idx + 1] = array("BLANK_LIST");
						}
						else
						{
							$eq_competence_list_text_parts = array();
							foreach ($eq_competence_list as $eq_part_competence_id => $eq_part_competence_float)
							{
								$eq_competence_list_text_parts[] = $eq_part_competence_id . "|" . ($eq_part_competence_float >= 0 ? self::number_format($eq_part_competence_float) : "");
							}
							$formulae[$competence_id] = array(
								"competence_id" => $competence_id,
								"eq_formula" => $eq_formula,
								"eq_competence_list" => "," . join(",", $eq_competence_list_text_parts) . ",",
								"eq_max_mark" => $eq_max_mark >= 0 ? self::number_format($eq_max_mark) : null,
							);
						}
					}
				}
			}
			else
			{
				$errors[$line_idx + 1] = array("BAD_FORMAT");
			}
		}
		foreach ($formulae as $competence_id => $row)
		{
			if (isset($direct[$competence_id]))
			{
				unset($direct[$competence_id]);
			}
		}
		return array($direct, $formulae, $errors);
	}

	private static function get_competences($competence_set_id)
	{
		return self::db()->fetch_column_values("
			SELECT competence_id
			FROM competence_calc
			WHERE competence_set_id = {$competence_set_id} 
		", true, "competence_id");
	}

	public static function get_edit_text($competence_set_id, $eq_competence_set_id)
	{
		$data = self::fetch_translator($competence_set_id, $eq_competence_set_id);
		ksort($data);
		$lines = array();
		foreach ($data as $row)
		{
			$line = $row["competence_id"] . " = ";
			if ($row["eq_formula"] === "EQ")
			{
				$line .= $row["eq_competence_id"];
			}
			else
			{
				$line .= $row["eq_formula"] . "(";
				$competences = array();
				foreach ($row["eq_competence_list"] as $competence_id => $float)
				{
					$competences[] = $competence_id . ($float >= 0.0 ? "|" . self::number_format($float) : "");
				}
				$line .= join(", ", $competences);
				$line .= ")";
			}
			if ($row["eq_max_mark"] >= 0.0)
			{
				$line .= " // MAX=" . self::number_format($row["eq_max_mark"]);
			}
			$lines[] = $line;
		}
		return join("\n", $lines);
	}

	public static function fetch_translator($competence_set_id, $eq_competence_set_id)
	{
		$result = self::fetch_direct($competence_set_id, $eq_competence_set_id);
		array_merge_good($result, self::fetch_formulae($competence_set_id, $eq_competence_set_id));
		return $result;
	}

	private static function fetch_formulae($competence_set_id, $eq_competence_set_id)
	{
		$result = self::db()->fetch_all("
			SELECT competence_id, eq_competence_list, eq_formula, eq_max_mark
			FROM competence_translation_formulae
			WHERE competence_set_id = {$competence_set_id} 
				AND eq_competence_set_id = {$eq_competence_set_id}
		", "competence_id");
		foreach ($result as &$row)
		{
			$row["competence_id"] = (int) $row["competence_id"];
			$row["eq_competence_list"] = self::parse_competence_list($row["eq_competence_list"]);
			$row["eq_max_mark"] = $row["eq_max_mark"] ? (float) $row["eq_max_mark"] : -1;
		}
		unset($row);
		return $result;
	}

	private static function parse_competence_list($text)
	{
		$result = array();
		foreach (explode(",", $text) as $rec)
		{
			$matches = null;
			if ($rec and preg_match("/^(\d+)(\|(" . self::RE_FLOAT . ")?)?$/", $rec, $matches))
			{
				$competence_id = (int) $matches[1];
				$float = isset($matches[3]) && $matches[3] ? (float) $matches[3] : -1;
				$result[$competence_id] = $float;
			}
		}
		return $result;
	}

	private static function fetch_direct($competence_set_id, $eq_competence_set_id)
	{
		$result = self::db()->fetch_all("
			SELECT competence_id, eq_competence_id, 'EQ' AS eq_formula, eq_max_mark
			FROM competence_translation_direct
			WHERE competence_set_id = {$competence_set_id} 
				AND eq_competence_set_id = {$eq_competence_set_id}
		", "competence_id");
		foreach ($result as &$row)
		{
			$row["competence_id"] = (int) $row["competence_id"];
			$row["eq_competence_id"] = (int) $row["eq_competence_id"];
			$row["eq_max_mark"] = $row["eq_max_mark"] ? (float) $row["eq_max_mark"] : -1;
		}
		unset($row);
		return $result;
	}

	public static function clean_competences($competence_set_id, $eq_competence_set_id)
	{
		self::db()->sql("
			DELETE FROM competence_translation_direct
			WHERE competence_set_id = {$competence_set_id}
				AND eq_competence_set_id = {$eq_competence_set_id}
		");
		self::db()->sql("
			DELETE FROM competence_translation_formulae
			WHERE competence_set_id = {$competence_set_id}
				AND eq_competence_set_id = {$eq_competence_set_id}
		");
	}

	private static function number_format($float)
	{
		return number_format($float, 1, ".", "");
	}

}

?>