<?php

class stream_join_save_page extends base_save_ctrl
{

	protected $mixins = array(
		"stream_before_start",
	);
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	private $join;

	/**
	 * @var stream_access_save
	 */
	private $stream_access_save;

	public function set_up()
	{
		/* cancel button */
		if (POST("cancel") !== null)
		{
			if (($retpath = POST("retpath")))
			{
				$retpath .= strpos($retpath, "?") === false ? "?cancel=1" : "&cancel=1";
			}
			else
			{
				$retpath = $this->request->get_prefix() . "/";
			}
			$this->set_redirect_url($retpath);
			$this->save_loader->stop_load();
		}
		return true;
	}

	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function start()
	{
		/* join param check */
		$this->join = POST("join");
		if ($this->join !== "1" and $this->join !== "0")
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		/* access_save class initialization */
		$this->stream_access_save = new stream_access_save($this->stream_obj, $this->user->get_user_id());

		if ($this->join)
		{
			$this->stream_access_save->join();
		}
		else
		{
			$this->stream_access_save->unjoin();
		}
		return true;
	}

	public function clean_cache()
	{
		$this->stream_access_save->clean_cache();
	}

}

?>