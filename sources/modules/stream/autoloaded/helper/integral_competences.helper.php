<?php

class integral_competences_helper extends base_static_db_helper
{

	public static function get_all_competences_in_calc_order($competence_set_id)
	{
		$result = integral_competences_calc_order_cache::init($competence_set_id)->get();
		if ($result)
		{
			return $result;
		}
		$result = array();
		$pairs = self::get_all_pairs($competence_set_id);
		$integral_index = self::group_pairs_by_competence_id($pairs);
		$competences = self::get_all_competences($competence_set_id);
		foreach ($competences as $competence_id => $is_integral)
		{
			if ($is_integral == 0)
			{
				$result[$competence_id] = true;
				unset($competences[$competence_id]);
			}
		}
		foreach ($integral_index as $competence_id => $included_competences_index)
		{
			if (!isset($competences[$competence_id]))
			{
				unset($integral_index[$competence_id]);
			}
		}
		$integral_index_left = $integral_index;
		do
		{
			$start_size = sizeof($competences);
			foreach ($integral_index_left as $competence_id => &$included_competences_index)
			{
				foreach ($included_competences_index as $included_competence_id => $mark)
				{
					if (isset($result[$included_competence_id]))
					{
						unset($included_competences_index[$included_competence_id]);
					}
				}
			}
			unset($included_competences_index);
			foreach ($competences as $competence_id => $is_integral)
			{
				if (!isset($integral_index_left[$competence_id]) or ! sizeof($integral_index_left[$competence_id]))
				{
					$result[$competence_id] = isset($integral_index[$competence_id]) ? $integral_index[$competence_id] : true;
					unset($integral_index_left[$competence_id]);
					unset($competences[$competence_id]);
				}
			}
			$new_size = sizeof($competences);
		}
		while ($new_size !== $start_size);
		if (sizeof($integral_index_left))
		{
			trigger_error("Ring in integral competences (competence_set_id = {$competence_set_id}): " . join(", ", array_keys($integral_index_left)));
		}
		integral_competences_calc_order_cache::init($competence_set_id)->set($result);
		return $result;
	}

	public static function get_all_including_competences_index($competence_set_id, $competence_id)
	{
		$pairs = self::get_all_pairs($competence_set_id);
		$index = self::group_pairs_by_included_competence_id($pairs);
		$incuding_competences_index = array();
		self::get_all_including_competences_iterator($index, $competence_id, $incuding_competences_index);
		//unset($incuding_competences_index[$competence_id]);
		return $incuding_competences_index;
	}

	private static function get_all_including_competences_iterator($index, $competence_id, &$incuding_competences, $path = array())
	{
		if (isset($incuding_competences[$competence_id]))
		{
			return;
		}
		$new_path = $path;
		$new_path[] = $competence_id;
		$incuding_competences[$competence_id] = $new_path;
		if (isset($index[$competence_id]))
		{
			foreach ($index[$competence_id] as $including_competence_id)
			{
				self::get_all_including_competences_iterator($index, $including_competence_id, $incuding_competences, $new_path);
			}
		}
	}

	public static function group_pairs_by_competence_id($pairs)
	{
		$result = array();
		foreach ($pairs as $pair_data)
		{
			$competence_id = $pair_data["competence_id"];
			$included_competence_id = $pair_data["included_competence_id"];
			$mark = (int) $pair_data["mark"];
			if (!isset($result[$competence_id]))
			{
				$result[$competence_id] = array();
			}
			$result[$competence_id][$included_competence_id] = $mark;
		}
		return $result;
	}

	public static function group_pairs_by_included_competence_id($pairs)
	{
		$result = array();
		foreach ($pairs as $pair_data)
		{
			$competence_id = $pair_data["competence_id"];
			$included_competence_id = $pair_data["included_competence_id"];
			if (!isset($result[$included_competence_id]))
			{
				$result[$included_competence_id] = array();
			}
			$result[$included_competence_id][] = $competence_id;
		}
		return $result;
	}

	public static function get_all_competences($competence_set_id)
	{
		$competences = self::db()->fetch_column_values("
			SELECT c.id, c.is_integral
			FROM competence_full c
			WHERE c.competence_set_id = {$competence_set_id}
				AND c.is_deleted = 0
		", "is_integral", "id");
		return $competences;
	}

	public static function get_all_pairs($competence_set_id)
	{
		$pairs = self::db()->fetch_all("
			SELECT ic.competence_id, ic.included_competence_id, ic.mark
			FROM integral_competence ic
			INNER JOIN competence_full c ON c.id = ic.competence_id
			INNER JOIN competence_full c2 ON c2.id = ic.included_competence_id
			WHERE c.competence_set_id = {$competence_set_id}
				AND c.is_deleted = 0
				AND c2.is_deleted = 0
				
		");
		return $pairs;
	}

	public static function get_options()
	{
		return array(
			0 => "—",
			1 => trans("Useful", "COMPETENCE_IMPORTANCE"),
			2 => trans("Required", "COMPETENCE_IMPORTANCE"),
			3 => trans("Key competence", "COMPETENCE_IMPORTANCE"),
		);
	}

	public static function get_possible_marks()
	{
		return array_keys(self::get_options());
	}

	public static function fill_integral_competences_xml(xnode $parent_node, $competence_set_id, $competence_id)
	{
		$including_competences_index = integral_competences_helper::get_all_including_competences_index($competence_set_id, $competence_id);
		$including_competences_node = $parent_node->create_child_node("including_competences");
		foreach ($including_competences_index as $including_competence_id => $path)
		{
			$including_competences_node->create_child_node("competence")
				->set_attr("id", $including_competence_id)
				->set_attr("path", join("→", $path));
		}
	}

	public static function fill_options_xml(xnode $parent_node)
	{
		$options_node = $parent_node->create_child_node("options");
		foreach (self::get_options() as $mark => $title)
		{
			$options_node->create_child_node("option")
				->set_attr("mark", $mark)
				->set_attr("mark_title", $title);
		}
	}

}

?>