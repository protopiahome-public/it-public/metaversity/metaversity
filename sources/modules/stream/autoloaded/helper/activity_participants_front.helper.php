<?php

class activity_participants_front_helper extends base_static_db_helper
{

	const PARTCIPANT_ACTION_REQUEST = "request";
	const PARTCIPANT_ACTION_CANCEL = "cancel";

	public static function get_current_statuses_for_roles($activity_id, $user_id)
	{
		$status_deleted = activity_participants_helper::PARTCIPANT_STATUS_DELETED;
		$current_statuses = self::db()->fetch_column_values("
			SELECT role_id, status 
			FROM activity_participant
			WHERE activity_id = {$activity_id}
				AND user_id = {$user_id}
				AND status != '{$status_deleted}'
		", "status", "role_id");
		return $current_statuses;
	}

	public static function is_good_participant_action($action)
	{
		return $action === self::PARTCIPANT_ACTION_REQUEST || $action === self::PARTCIPANT_ACTION_CANCEL;
	}

	public static function participant_action_allowed($activity_id_or_row, $role_id, $action)
	{
		global $user;
		/* @var $user user */

		$user_id = $user->get_user_id();
		if (!$user_id)
		{
			return false;
		}

		$activity_row = activity_helper::get_row($activity_id_or_row);
		if (!$activity_row)
		{
			return false;
		}
		$activity_id = $activity_row["id"];

		$roles_data = activity_roles_helper::fetch_roles($activity_id);
		if (!isset($roles_data[$role_id]))
		{
			return false;
		}
		$role_data = $roles_data[$role_id];
		$auto_accept = $role_data["auto_accept"] == 1;

		$current_status = activity_participants_helper::get_current_status($activity_id, $role_id, $user_id);
		$taken_number = activity_participants_helper::get_taken_number($activity_id, $role_id);

		if ($taken_number != $role_data["taken_number"])
		{
			$log_message = "activity_id={$activity_id}, role_id={$role_id}, expected={$taken_number}, real={$role_data["taken_number"]}\n";
			file_put_contents_safe(PATH_LOG . "/taken_number_error.log", $log_message, true);
			activity_participants_helper::update_taken_number($activity_id, $role_id, $taken_number);
		}

		if ($action === self::PARTCIPANT_ACTION_CANCEL)
		{
			if ($current_status == activity_participants_helper::PARTCIPANT_STATUS_DELETED)
			{
				return true;
			}
			elseif ($auto_accept and $current_status === activity_participants_helper::PARTCIPANT_STATUS_ACCEPTED or $current_status == activity_participants_helper::PARTCIPANT_STATUS_PREMODERATION)
			{
				return activity_participants_helper::PARTCIPANT_STATUS_DELETED;
			}
			else
			{
				return false;
			}
		}
		elseif ($action === self::PARTCIPANT_ACTION_REQUEST)
		{
			if ($current_status == activity_participants_helper::PARTCIPANT_STATUS_ACCEPTED)
			{
				return true;
			}
			elseif ($current_status == activity_participants_helper::PARTCIPANT_STATUS_PREMODERATION)
			{
				return true;
			}
			elseif ($current_status == activity_participants_helper::PARTCIPANT_STATUS_DELETED and ($role_data["total_number"] == 0 or $role_data["total_number"] > $taken_number))
			{
				return $auto_accept ? activity_participants_helper::PARTCIPANT_STATUS_ACCEPTED : activity_participants_helper::PARTCIPANT_STATUS_PREMODERATION;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

}

?>