<?php

class activity_longterm_marks_helper extends base_static_db_helper
{

	public static function fetch_marks(stream_obj $stream_obj, $activity_id)
	{
		$data = self::db()->fetch_all("
			SELECT 
				acm.user_id AS id, 
				acm.competence_id, acm.mark, acm.comment,
				acm.changer_user_id,
				acm.change_time
			FROM activity_competence_mark acm
			INNER JOIN competence_calc c ON c.competence_id = acm.competence_id
			WHERE 
				acm.activity_id = {$activity_id}
				AND c.competence_set_id = {$stream_obj->get_competence_set_id()}
			ORDER BY change_time
		");
		return $data;
	}

}

?>