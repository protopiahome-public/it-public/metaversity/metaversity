<?php

class activity_helper extends base_static_db_helper
{

	const DATE_TYPE_EVENT = "event";
	const DATE_TYPE_DEADLINE = "deadline";
	const DATE_TYPE_LONGTERM = "longterm";
	const WEIGHT_HI = "hi";
	const WEIGHT_LOW = "low";

	public static function get_row($activity_id_or_row, $lock = false)
	{
		if (is_array($activity_id_or_row))
		{
			if ($lock)
			{
				$activity_id_or_row = $activity_id_or_row["id"];
			}
			else
			{
				return $activity_id_or_row;
			}
		}
		$activity_id = $activity_id_or_row;
		static $cache = array();
		if (!isset($cache[$activity_id]) or $lock)
		{
			$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
			$cache[$activity_id] = self::db()->get_row("
				SELECT * 
				FROM activity 
				WHERE id = {$activity_id}
				{$lock_sql}
			");
		}
		return $cache[$activity_id];
	}

	public static function is_longterm($activity_id_or_row)
	{
		$row = self::get_row($activity_id_or_row);
		return $row["date_type"] === self::DATE_TYPE_LONGTERM;
	}

}

?>