<?php

class competence_set_helper extends base_static_db_helper
{

	public static function get_stream_count($competence_set_id)
	{
		$count = self::db()->get_value("
			SELECT COUNT(*)
			FROM stream
			WHERE competence_set_id = {$competence_set_id}
		");
		return $count;
	}

	public static function get_corresponding_stream_ids($competence_set_id)
	{
		$stream_ids = self::db()->fetch_column_values("
			SELECT id
			FROM stream
			WHERE competence_set_id = {$competence_set_id}
		");
		return $stream_ids;
	}

	public static function get_corresponding_stream_id($competence_set_id)
	{
		$stream_ids = self::get_corresponding_stream_ids($competence_set_id);
		return sizeof($stream_ids) === 1 ? $stream_ids[0] : null;
	}

	public static function check_editable_stream_id_for_competence_set($stream_id, $competence_set_id)
	{
		if (!is_good_id($stream_id))
		{
			return null;
		}
		$ok = self::db()->row_exists("
			SELECT * 
			FROM stream
			WHERE id = {$stream_id} AND competence_set_id = {$competence_set_id}
		");
		return $ok ? $stream_id : null;
	}

}

?>