<?php

class competence_restrictions_helper extends base_static_db_helper
{

	private static $competence_groups_restrictions_cache = array();

	public static function get_study_levels($stream_id)
	{
		$study_levels = self::db()->fetch_all("
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$stream_id}
			ORDER BY title
		", "id");
		return $study_levels;
	}

	public static function build_study_levels_xml(xnode $parent_node, $stream_id)
	{
		if (!$stream_id)
		{
			return;
		}
		$study_levels = self::get_study_levels($stream_id);
		$study_levels_node = $parent_node->create_child_node("study_levels");
		$study_levels_node->set_attr("stream_id", $stream_id);
		foreach ($study_levels as $study_level_data)
		{
			$study_level_node = $study_levels_node->create_child_node("study_level");
			foreach ($study_level_data as $idx => $val)
			{
				$study_level_node->set_attr($idx, $val);
			}
		}
	}

	public static function rebuild_restrictions($competence_set_id, $stream_id)
	{
		if (!$stream_id)
		{
			return;
		}
		$restrictions = self::get_restrictions($competence_set_id, $stream_id);
		// competence groups
		self::db()->sql("
			DELETE gll
			FROM competence_group_study_level_link AS gll
			LEFT JOIN competence_group g ON g.id = gll.competence_group_id
			LEFT JOIN study_level l ON l.id = gll.study_level_id
			WHERE g.competence_set_id = {$competence_set_id} AND l.stream_id = {$stream_id}
		");
		foreach ($restrictions["g"] as $competence_group_data)
		{
			if (!isset($competence_group_data["study_levels"]) || isset($competence_group_data["inherited"]))
			{
				continue;
			}
			$competence_group_id = $competence_group_data["competence_group_id"];
			foreach ($competence_group_data["study_levels"] as $study_level_key => $study_level_id)
			{
				self::db()->sql("
					INSERT INTO competence_group_study_level_link (competence_group_id, study_level_id)
					VALUES ({$competence_group_id}, {$study_level_id})
				");
			}
		}
		// competences
		self::db()->sql("
			DELETE cll
			FROM competence_study_level_link AS cll
			LEFT JOIN competence_full c ON c.id = cll.competence_id
			LEFT JOIN study_level l ON l.id = cll.study_level_id
			WHERE c.competence_set_id = {$competence_set_id} AND l.stream_id = {$stream_id}
		");
		foreach ($restrictions["c"] as $competence_data)
		{
			if (!isset($competence_data["study_levels"]) || isset($competence_data["inherited"]))
			{
				continue;
			}
			$competence_id = $competence_data["competence_id"];
			foreach ($competence_data["study_levels"] as $study_level_key => $study_level_id)
			{
				self::db()->sql("
					INSERT INTO competence_study_level_link (competence_id, study_level_id)
					VALUES ({$competence_id}, {$study_level_id})
				");
			}
		}
	}

	public static function get_restrictions($competence_set_id, $stream_id)
	{
		if (!$stream_id)
		{
			return array();
		}

		require_once PATH_INTCMF . "/tree.php";

		$restrictions = self::get_raw_restrictions($competence_set_id, $stream_id);
		$result = array();
		$result["g"] = array();
		$result["c"] = array();
		$study_levels = self::db()->fetch_all("
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$stream_id}
		", "id");
		$competence_groups_data = self::db()->fetch_all("
			SELECT id, parent_id, title
			FROM competence_group
			WHERE competence_set_id = {$competence_set_id}
		");
		$tree = new tree($competence_groups_data);
		$tree_index = $tree->get_index();
		self::$competence_groups_restrictions_cache = array();
		foreach ($tree_index as $competence_group_id => $data)
		{
			$competence_group_key = "g" . $competence_group_id;
			$result["g"][$competence_group_key] = self::get_restrictions_for_group($tree_index, $restrictions, $competence_group_id);
		}
		$competence_data = self::db()->fetch_all("
			SELECT c.competence_id AS id, l.competence_group_id AS parent_id, c.title
			FROM competence_calc c
			JOIN competence_link l ON l.competence_id = c.competence_id
			WHERE c.competence_set_id = {$competence_set_id}
		");
		foreach ($competence_data as $data)
		{
			$competence_id = (int) $data["id"];
			$parent_competence_group_id = (int) $data["parent_id"];
			if (isset($tree_index[$parent_competence_group_id]))
			{
				$competence_key = "c" . $competence_id;
				$result["c"][$competence_key] = self::get_restrictions_for_competence($tree_index, $restrictions, $competence_id, $parent_competence_group_id);
			}
		}
		// Prefix setup
		foreach ($result as &$result_sub)
		{
			foreach ($result_sub as &$data)
			{
				$data["prefix"] = "";
				if (isset($data["inherited"]) || !isset($data["study_levels"]) || !sizeof($data["study_levels"]))
				{
					continue;
				}
				$prefix_parts = array();
				foreach ($data["study_levels"] as $study_level_key => $study_level_id)
				{
					if (isset($study_levels[$study_level_id]))
					{
						$prefix_parts[] = $study_levels[$study_level_id]["title"];
					}
				}
				$data["prefix"] = join(", ", $prefix_parts);
				if (strlen($data["prefix"]))
				{
					$data["prefix"] = "[{$data["prefix"]}] ";
				}
			}
			unset($data);
		}
		unset($result_sub);
		return $result;
	}

	private static function get_restrictions_for_competence($tree_index, $restrictions, $competence_id, $parent_competence_group_id)
	{
		$competence_key = "c" . $competence_id;
		$result = isset($restrictions["c"][$competence_key]) ? $restrictions["c"][$competence_key] : array();
		$parent_group_restictions = self::get_restrictions_for_group($tree_index, $restrictions, $parent_competence_group_id);
		if (!isset($result["study_levels"]))
		{
			$result = $parent_group_restictions;
			$result["competence_id"] = $competence_id;
			$result["inherited"] = true;
		}
		else
		{
			if (isset($parent_group_restictions["study_levels"]) and sizeof($parent_group_restictions["study_levels"]))
			{
				foreach ($result["study_levels"] as $study_level_key => $study_level_id)
				{
					if (!isset($parent_group_restictions["study_levels"][$study_level_key]))
					{
						unset($result["study_levels"][$study_level_key]);
						continue;
					}
				}
			}
			if (!sizeof($result["study_levels"]))
			{
				$result = $parent_group_restictions;
				$result["competence_id"] = $competence_id;
				$result["inherited"] = true;
			}
		}
		return $result;
	}

	private static function get_restrictions_for_group($tree_index, $restrictions, $competence_group_id)
	{
		if (isset(self::$competence_groups_restrictions_cache[$competence_group_id]))
		{
			return self::$competence_groups_restrictions_cache[$competence_group_id];
		}
		$group_data = $tree_index[$competence_group_id];
		$competence_group_key = "g" . $competence_group_id;
		$result = isset($restrictions["g"][$competence_group_key]) ? $restrictions["g"][$competence_group_key] : array();
		if ($group_data["parent_id"] !== null)
		{
			$parent_competence_group_id = (int) $group_data["parent_id"];
			$parent_group_restictions = self::get_restrictions_for_group($tree_index, $restrictions, $parent_competence_group_id);
			if (!isset($result["study_levels"]))
			{
				$result = $parent_group_restictions;
				$result["competence_group_id"] = $competence_group_id;
				$result["inherited"] = true;
			}
			else
			{
				if (isset($parent_group_restictions["study_levels"]) and sizeof($parent_group_restictions["study_levels"]))
				{
					foreach ($result["study_levels"] as $study_level_key => $study_level_id)
					{
						if (!isset($parent_group_restictions["study_levels"][$study_level_key]))
						{
							unset($result["study_levels"][$study_level_key]);
							continue;
						}
					}
				}
				if (!sizeof($result["study_levels"]))
				{
					$result = $parent_group_restictions;
					$result["competence_group_id"] = $competence_group_id;
					$result["inherited"] = true;
				}
			}
		}
		self::$competence_groups_restrictions_cache[$competence_group_id] = $result;
		return $result;
	}

	private static function get_raw_restrictions($competence_set_id, $stream_id)
	{
		$result = array();
		$result["g"] = array();
		$result["c"] = array();
		$rows = self::db()->fetch_all("
			SELECT gll.*
			FROM competence_group_study_level_link gll
			JOIN competence_group g ON g.id = gll.competence_group_id
			JOIN study_level l ON l.id = gll.study_level_id
			WHERE g.competence_set_id = {$competence_set_id} AND l.stream_id = {$stream_id}
			ORDER BY l.title
		");
		foreach ($rows as $data)
		{
			$competence_group_id = (int) $data["competence_group_id"];
			$competence_group_key = "g" . $competence_group_id;
			if (!isset($result["g"][$competence_group_key]))
			{
				$result["g"][$competence_group_key] = array(
					"competence_group_id" => $competence_group_id,
					"study_levels" => array(),
				);
			}
			$study_level_id = (int) $data["study_level_id"];
			$study_level_key = "l" . $study_level_id;
			$result["g"][$competence_group_key]["study_levels"][$study_level_key] = $study_level_id;
		}
		$rows = self::db()->fetch_all("
			SELECT cll.*
			FROM competence_study_level_link cll
			JOIN competence_calc c ON c.competence_id = cll.competence_id
			JOIN study_level l ON l.id = cll.study_level_id
			WHERE c.competence_set_id = {$competence_set_id} AND l.stream_id = {$stream_id}
			ORDER BY l.title
		");
		foreach ($rows as $data)
		{
			$competence_id = (int) $data["competence_id"];
			$competence_key = "c" . $competence_id;
			if (!isset($result["c"][$competence_key]))
			{
				$result["c"][$competence_key] = array(
					"competence_id" => $competence_id,
					"study_levels" => array(),
				);
			}
			$study_level_id = (int) $data["study_level_id"];
			$study_level_key = "l" . $study_level_id;
			$result["c"][$competence_key]["study_levels"][$study_level_key] = $study_level_id;
		}
		return $result;
	}

}

?>