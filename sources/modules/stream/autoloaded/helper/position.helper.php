<?php

class position_helper extends base_static_db_helper
{

	public static function positions_checked(stream_obj $stream_obj, $user_id)
	{
		return self::db()->row_exists("
			SELECT up.*
			FROM user_position up
			JOIN position p ON p.id = up.position_id
			WHERE up.user_id = {$user_id} AND p.stream_id = {$stream_obj->get_id()}
			LIMIT 1
		");
	}

	public static function change_position(stream_obj $stream_obj, $user_id, $position_id, $checked)
	{
		global $user;
		/* @var $user user */

		$data = array(
			"user_id" => $user_id,
			"position_id" => $position_id,
			"add_time" => "NOW()",
		);

		if (!$checked)
		{
			self::db()->sql("DELETE FROM user_position WHERE user_id = {$user_id} AND position_id = {$position_id}");
		}
		else
		{
			self::db()->insert_by_array("user_position", $data, true, array("add_time" => true));
		}
		$data["checked"] = $checked ? "1" : "0";
		$data["changer_user_id"] = $user->get_user_id();
		self::db()->insert_by_array("user_position_log", $data);
		stream_calc_helper::update_stream_user_link_position_count_calc($stream_obj->get_id(), $user_id);
		stream_calc_helper::update_user_position_count_calc($user_id);
		stream_user_positions_cache_tag::init($stream_obj->get_id(), $user_id)->update();
	}

}

?>