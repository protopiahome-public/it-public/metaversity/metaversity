<?php

class study_material_helper extends base_static_db_helper
{

	const STATUS_DEFAULT = "default";
	const STATUS_READ = "read";
	const STATUS_SKIP = "skip";

	public static function get_row($study_material_id_or_row, $lock = false)
	{
		if (is_array($study_material_id_or_row))
		{
			if ($lock)
			{
				$study_material_id_or_row = $study_material_id_or_row["id"];
			}
			else
			{
				return $study_material_id_or_row;
			}
		}
		$study_material_id = $study_material_id_or_row;
		static $cache = array();
		if (!isset($cache[$study_material_id]) or $lock)
		{
			$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
			$cache[$study_material_id] = self::db()->get_row("
				SELECT * 
				FROM study_material 
				WHERE id = {$study_material_id}
				{$lock_sql}
			");
		}
		return $cache[$study_material_id];
	}

	public static function fetch_users($study_material_id, $status = null)
	{
		$status_default = self::STATUS_DEFAULT;
		$status_where = $status ? "AND l.status = '{$status}'" : "AND p.status != '{$status_default}'";
		$data = self::db()->fetch_all("
			SELECT l.user_id AS id, l.status
			FROM study_material_user_link l
			WHERE l.study_material_id = {$study_material_id}
				{$status_where}
			ORDER BY l.change_time
		");
		return $data;
	}

}

?>