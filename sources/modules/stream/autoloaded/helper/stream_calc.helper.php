<?php

class stream_calc_helper extends base_static_db_helper
{

	public static function update_stream_group_count_calc($stream_id)
	{
		self::db()->sql("
			UPDATE stream d
			SET group_count_calc = (
				SELECT COUNT(*)
				FROM `group` dt
				WHERE dt.stream_id = d.id
			)
			WHERE id = '{$stream_id}'
		");
	}

	public static function update_study_level_group_count_calc($study_level_id)
	{
		self::db()->sql("
			UPDATE study_level sl
			SET group_count_calc = (
				SELECT COUNT(*)
				FROM `group` dt
				WHERE dt.study_level_id = sl.id
			)
			WHERE id = '{$study_level_id}'
		");
	}

	public static function update_city_group_count_calc($city_id)
	{
		self::db()->sql("
			UPDATE city c
			SET group_count_calc = (
				SELECT COUNT(*)
				FROM `group` g
				WHERE g.city_id = c.id
			)
			WHERE id = '{$city_id}'
		");
	}

	public static function update_stream_city_count_calc($stream_id = null)
	{
		$where_sql = $stream_id ? "WHERE id = {$stream_id}" : "";
		self::db()->sql("
			UPDATE stream d
			SET city_count_calc = (
				SELECT COUNT(*)
				FROM stream_city_link l
				WHERE l.stream_id = d.id
			)
			{$where_sql}
		");
	}

	public static function update_city_stream_count_calc()
	{
		self::db()->sql("
			UPDATE city c
			SET stream_count_calc = (
				SELECT COUNT(*)
				FROM stream_city_link l
				WHERE l.city_id = c.id
			)
		");
	}

	public static function update_stream_user_count_calc($stream_id)
	{
		self::db()->sql("
			UPDATE stream d
			SET user_count_calc = (
				SELECT COUNT(*)
				FROM stream_user_link l
				WHERE l.stream_id = d.id
					AND l.status <> 'pretender' AND l.status <> 'deleted'
			)
			WHERE id = {$stream_id}
		");
	}

	public static function update_format_role_count_calc($format_id)
	{
		self::db()->sql("
			UPDATE `format` f
			SET role_count_calc = (
				SELECT count(*)
				FROM role
				WHERE format_id = f.id
			)
			WHERE id = {$format_id}
		");
	}

	public static function update_study_material_status_user_count_calc($study_material_id, $calc_column, $status)
	{
		self::db()->sql("
			UPDATE study_material sm
			SET {$calc_column} = (
				SELECT COUNT(*) 
				FROM study_material_user_link l
				WHERE l.study_material_id = sm.id
					AND l.status = '{$status}'
			)
			WHERE sm.id = {$study_material_id}
		");
	}

	public static function update_group_user_count_calc($stream_id)
	{
		self::db()->sql("
			UPDATE `group` f
			SET user_count_calc = (
				SELECT count(*)
				FROM stream_user_link l
				WHERE l.group_id = f.id
					AND l.status <> 'pretender' AND l.status <> 'deleted'
			)
			WHERE stream_id = {$stream_id}
		");
	}

	public static function update_activity_group_count_calc($activity_id)
	{
		self::db()->sql("
			UPDATE activity a
			SET group_count_calc = (
				SELECT COUNT(*) 
				FROM activity_group_link l
				WHERE l.activity_id = a.id
			)
			WHERE a.id = {$activity_id}
		");
	}

	public static function update_stream_user_link_mark_count_calc($stream_id, $user_id)
	{
		/* Too complex for compatibility with stream_count_calc.sql */
		self::db()->sql("
			INSERT IGNORE INTO stream_user_link
				(stream_id, user_id, status)
			SELECT a.stream_id, apm.user_id, 'deleted'
			FROM activity_participant_mark apm
			JOIN activity a ON a.id = apm.activity_id
			WHERE a.stream_id = {$stream_id} AND apm.user_id = {$user_id}
			GROUP BY a.stream_id, apm.user_id
		");
		self::db()->sql("
			INSERT IGNORE INTO stream_user_link
				(stream_id, user_id, status)
			SELECT a.stream_id, acm.user_id, 'deleted'
			FROM activity_competence_mark acm
			JOIN activity a ON a.id = acm.activity_id
			JOIN stream d ON d.id = a.stream_id
			JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
			WHERE a.stream_id = {$stream_id} AND acm.user_id = {$user_id}
			GROUP BY a.stream_id, acm.user_id
		");
		self::db()->sql("
			UPDATE stream_user_link l
			SET l.mark_count_calc = (
				SELECT COUNT(*)
				FROM activity_participant_mark apm
				JOIN activity a ON a.id = apm.activity_id
				WHERE apm.user_id = l.user_id AND a.stream_id = l.stream_id
			) + (
				SELECT COUNT(*)
				FROM activity_competence_mark acm
				JOIN activity a ON a.id = acm.activity_id
				JOIN stream d ON d.id = a.stream_id
				JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
				WHERE acm.user_id = l.user_id AND a.stream_id = l.stream_id
			)
			WHERE l.stream_id = {$stream_id} AND l.user_id = {$user_id}
		");
	}

	// @todo rewrite code in stream_admin_competence_set_tree_ajax_page::update_mark_count_calc()
	// and remove this code.
	public static function bulk_update_stream_user_link_mark_count_calc($stream_id)
	{
		self::db()->sql("
			UPDATE stream_user_link l
			SET l.mark_count_calc = (
				SELECT COUNT(*)
				FROM activity_participant_mark apm
				JOIN activity a ON a.id = apm.activity_id
				WHERE apm.user_id = l.user_id AND a.stream_id = l.stream_id
			) + (
				SELECT COUNT(*)
				FROM activity_competence_mark acm
				JOIN activity a ON a.id = acm.activity_id
				JOIN stream d ON d.id = a.stream_id
				JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
				WHERE acm.user_id = l.user_id AND a.stream_id = l.stream_id
			)
			WHERE l.stream_id = {$stream_id}
		");
	}

	public static function update_user_mark_count_calc($user_id)
	{
		self::db()->sql("
			UPDATE user u
			SET u.mark_count_calc = (
				SELECT COUNT(*)
				FROM activity_participant_mark apm
				WHERE apm.user_id = u.id
			) + (
				SELECT COUNT(*)
				FROM activity_competence_mark acm
				JOIN activity a ON a.id = acm.activity_id
				JOIN stream d ON d.id = a.stream_id
				JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
				WHERE acm.user_id = u.id
			)
			WHERE u.id = {$user_id}
		");
	}

	// @todo rewrite code in stream_admin_competence_set_tree_ajax_page::update_mark_count_calc()
	// and remove this code.
	public static function bulk_update_user_mark_count_calc()
	{
		self::db()->sql("
			UPDATE user u
			SET u.mark_count_calc = (
				SELECT COUNT(*)
				FROM activity_participant_mark apm
				WHERE apm.user_id = u.id
			) + (
				SELECT COUNT(*)
				FROM activity_competence_mark acm
				JOIN activity a ON a.id = acm.activity_id
				JOIN stream d ON d.id = a.stream_id
				JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
				WHERE acm.user_id = u.id
			)
		");
	}

	public static function update_stream_user_link_position_count_calc($stream_id, $user_id)
	{
		/* Too complex for compatibility with stream_count_calc.sql */
		self::db()->sql("
			INSERT IGNORE INTO stream_user_link
				(stream_id, user_id, status)
			SELECT p.stream_id, up.user_id, 'deleted'
			FROM user_position up
			JOIN position p ON p.id = up.position_id
			WHERE p.stream_id = {$stream_id} AND up.user_id = {$user_id}
			GROUP BY p.stream_id, up.user_id
		");
		self::db()->sql("
			UPDATE stream_user_link l
			SET l.position_count_calc = (
				SELECT COUNT(*)
				FROM user_position up
				JOIN position p ON p.id = up.position_id
				WHERE up.user_id = l.user_id AND p.stream_id = l.stream_id
			)
			WHERE l.stream_id = {$stream_id} AND l.user_id = {$user_id}
		");
	}

	public static function update_user_position_count_calc($user_id)
	{
		self::db()->sql("
			UPDATE user u
			SET u.position_count_calc = (
				SELECT COUNT(*)
				FROM user_position up
				WHERE up.user_id = u.id
			)
			WHERE u.id = {$user_id}
		");
	}

	public static function update_activity_participant_count_calc($activity_id)
	{
		$status_accepted = activity_participants_helper::PARTCIPANT_STATUS_ACCEPTED;
		$status_premoderation = activity_participants_helper::PARTCIPANT_STATUS_PREMODERATION;
		self::db()->sql("
			UPDATE activity a
			SET accepted_participant_count_calc = (
				SELECT COUNT(*) 
				FROM activity_participant_status_calc 
				WHERE activity_id = a.id
					AND status = '{$status_accepted}'
			), premoderation_request_count_calc = (
				SELECT COUNT(*)
				FROM activity_participant
				WHERE activity_id = a.id
					AND status = '{$status_premoderation}'
			)
			WHERE a.id = {$activity_id}
		");
		self::db()->sql("
			UPDATE activity_calc ac
			LEFT JOIN activity a ON a.id = ac.activity_id
			SET ac.accepted_participant_count_calc = a.accepted_participant_count_calc
			WHERE a.id = {$activity_id}
		");
	}

	public static function update_activity_mark_count_calc($activity_id)
	{
		self::db()->sql("
			UPDATE activity a
			SET mark_count_calc = (
				SELECT COUNT(*)
				FROM activity_participant_mark apm
				WHERE apm.activity_id = a.id
			)
			WHERE a.id = {$activity_id}
		");
		self::db()->sql("
			UPDATE activity_calc ac
			LEFT JOIN activity a ON a.id = ac.activity_id
			SET ac.mark_count_calc = a.mark_count_calc
			WHERE a.id = {$activity_id}
		");
	}

	public static function update_activity_unset_mark_count_calc($activity_id)
	{
		$status_accepted = activity_participants_helper::PARTCIPANT_STATUS_ACCEPTED;
		self::db()->sql("
			UPDATE activity a
			SET unset_mark_count_calc = (
				SELECT COUNT(*)
				FROM activity_participant ap
				LEFT JOIN activity_participant_mark apm ON apm.activity_id = ap.activity_id
					AND apm.user_id = ap.user_id AND apm.role_id = ap.role_id
				WHERE ap.activity_id = a.id
					AND ap.status = '{$status_accepted}'
					AND apm.mark IS NULL
			)
			WHERE a.id = {$activity_id}
		");
	}
	
	public static function update_activity_longterm_count_calc($activity_id, $competence_set_id)
	{
		self::db()->sql("
			UPDATE activity a
			SET longterm_graded_user_count_calc = (
				SELECT COUNT(DISTINCT user_id)
				FROM activity_competence_mark acm
				JOIN competence_calc c ON c.competence_id = acm.competence_id
				WHERE acm.activity_id = a.id
					AND c.competence_set_id = {$competence_set_id}
			)
			WHERE a.id = {$activity_id}
		");
		self::db()->sql("
			UPDATE activity a
			SET longterm_mark_count_calc = (
				SELECT COUNT(*)
				FROM activity_competence_mark acm
				JOIN competence_calc c ON c.competence_id = acm.competence_id
				WHERE acm.activity_id = a.id
					AND c.competence_set_id = {$competence_set_id}
			)
			WHERE a.id = {$activity_id}
		");
		self::db()->sql("
			UPDATE activity_calc ac
			LEFT JOIN activity a ON a.id = ac.activity_id
			SET ac.longterm_graded_user_count_calc = a.longterm_graded_user_count_calc,
				ac.longterm_mark_count_calc = a.longterm_mark_count_calc
			WHERE a.id = {$activity_id}
		");
	}

	public static function update_competence_set_competence_count_calc($competence_set_id)
	{
		self::db()->sql("
			UPDATE competence_set s
			SET s.competence_count_calc = (
				SELECT COUNT(*)
				FROM competence_full c
				WHERE c.is_deleted = 0 AND c.competence_set_id = s.id
			)
			WHERE s.id = {$competence_set_id}
		");
	}

	public static function bulk_update_position_competence_count_calc($competence_set_id)
	{
		self::db()->sql("
			UPDATE position p
			LEFT JOIN stream d ON d.id = p.stream_id
			SET p.competence_count_calc = (
				SELECT COUNT(*)
				FROM rate_competence_link rcl 
				JOIN competence_calc c ON c.competence_id = rcl.competence_id
				WHERE rcl.rate_id = p.rate_id AND c.competence_set_id = d.competence_set_id
			)
			WHERE d.competence_set_id = {$competence_set_id}
		");
	}

	public static function bulk_update_study_material_competence_count_calc($competence_set_id)
	{
		self::db()->sql("
			UPDATE study_material sm
			LEFT JOIN stream d ON d.id = sm.stream_id
			SET sm.competence_count_calc = (
				SELECT COUNT(*)
				FROM rate_competence_link rcl 
				JOIN competence_calc c ON c.competence_id = rcl.competence_id
				WHERE rcl.rate_id = sm.rate_id AND c.competence_set_id = d.competence_set_id
			)
			WHERE d.competence_set_id = {$competence_set_id}
		");
	}

	public static function bulk_update_role_competence_count_calc($competence_set_id)
	{
		self::db()->sql("
			UPDATE role r
			LEFT JOIN stream d ON d.id = r.stream_id
			SET r.competence_count_calc = (
				SELECT COUNT(*)
				FROM rate_competence_link rcl 
				JOIN competence_calc c ON c.competence_id = rcl.competence_id
				WHERE rcl.rate_id = r.rate_id AND c.competence_set_id = d.competence_set_id
			)
			WHERE d.competence_set_id = {$competence_set_id}
		");
	}

}

?>