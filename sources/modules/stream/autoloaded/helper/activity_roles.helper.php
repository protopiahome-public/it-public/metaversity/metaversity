<?php

class activity_roles_helper extends base_static_db_helper
{

	public static function fetch_roles($activity_id, $edit_mode = false)
	{
		$row = activity_helper::get_row($activity_id);
		$descr_column = $edit_mode ? "descr_text" : "descr_html";

		$roles_data = self::db()->fetch_all("
			SELECT r.id, r.rate_id, r.title, r.total_number, 1 as enabled, r.auto_accept, r.{$descr_column}
			FROM role r
			WHERE r.format_id = {$row["format_id"]}
			ORDER BY r.title
		", "id");

		$redefined_data = self::db()->fetch_all("
			SELECT l.*
			FROM role r
			INNER JOIN activity_role_link l ON l.role_id = r.id AND l.activity_id = {$activity_id}
			WHERE r.format_id = {$row["format_id"]}
		", "role_id");

		$stat = self::db()->fetch_all("
			SELECT s.*
			FROM role r
			INNER JOIN activity_role_stat s ON s.role_id = r.id AND s.activity_id = {$activity_id}
			WHERE r.format_id = {$row["format_id"]}
		", "role_id");

		foreach ($roles_data as $idx => &$role_data)
		{
			$id = $role_data["id"];
			if (isset($redefined_data[$id]))
			{
				$role_data[$descr_column] = $redefined_data[$id][$descr_column];
				$role_data["enabled"] = $redefined_data[$id]["enabled"];
				$role_data["total_number"] = $redefined_data[$id]["total_number"];
				$role_data["auto_accept"] = $redefined_data[$id]["auto_accept"];
				if (!$edit_mode and $redefined_data[$id]["enabled"] == 0)
				{
					unset($roles_data[$idx]);
					continue;
				}
			}
			$role_data["taken_number"] = isset($stat[$id]) ? $stat[$id]["taken_number"] : 0;
		}
		unset($role_data);
		return $roles_data;
	}

	public static function fetch_roles_for_calculus(stream_obj $stream_obj, $activity_id_or_row)
	{
		$activity_row = activity_helper::get_row($activity_id_or_row);
		$activity_id = $activity_row["id"];
		$format_id = $activity_row["format_id"];
		$result = stream_activity_roles_cache::init($stream_obj, $activity_id, $format_id)->get();
		if ($result)
		{
			return $result;
		}
		$result = array(
			"format_id" => $format_id,
			"roles" => array(),
		);
		$roles_data = self::fetch_roles($activity_id);
		foreach ($roles_data as $role_data)
		{
			$result["roles"][$role_data["id"]] = true;
		}
		stream_activity_roles_cache::init($stream_obj, $activity_id, $format_id)->set($result);
		return $result;
	}

}

?>