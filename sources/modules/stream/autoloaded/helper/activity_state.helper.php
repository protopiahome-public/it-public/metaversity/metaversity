<?php

// @todo unused now but may be useful in future
class activity_state_helper extends base_static_db_helper
{

	const STATE_ACTUAL = "actual";
	const STATE_PAST = "past";
	const STATE_NO_DATE = "no-date";

	public static function get_state($activity_row)
	{
		$now = time();
		$start_time = ($activity_row["start_time"] != "" and substr($activity_row["start_time"], 0, 5) != "0000-") ? self::db()->parse_datetime($activity_row["start_time"]) : 0;
		$finish_time = ($activity_row["finish_time"] != "" and substr($activity_row["finish_time"], 0, 5) != "0000-") ? self::db()->parse_datetime($activity_row["finish_time"]) : 0;
		switch (true)
		{
			case $start_time == 0:
				return self::STATE_NO_DATE;
			case ($start_time >= $now or $finish_time >= $now):
				return self::STATE_ACTUAL;
			default:
				return self::STATE_PAST;
		}
	}

	static $stored_state_cache = array();

	public static function stored_state_set($activity_id, $state)
	{
		self::$stored_state_cache[$activity_id] = $state;
	}

	public static function stored_state_get($activity_id)
	{
		return isset(self::$stored_state_cache[$activity_id]) ? self::$stored_state_cache[$activity_id] : null;
	}

}

?>