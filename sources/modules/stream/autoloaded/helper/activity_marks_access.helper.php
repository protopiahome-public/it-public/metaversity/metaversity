<?php

class activity_marks_access_helper extends base_static_db_helper
{

	public static function filter_user_marks(&$data, $user_id)
	{
		$user_obj = user_obj::instance($user_id);
		foreach ($data as &$row)
		{
			if (isset($row["mark"]))
			{
				if (!user_access_helper::can_access_marks($user_obj, stream_obj::instance($row["stream_id"])))
				{
					$row["mark"] = "ACCESS_DENIED";
					unset($row["comment"]);
				}
			}
		}
		unset($row);
	}

	public static function filter_stream_marks(&$data, $stream_id, $user_id_key = "id")
	{
		$stream_obj = stream_obj::instance($stream_id);
		foreach ($data as &$row)
		{
			if (isset($row["mark"]))
			{
				if (!user_access_helper::can_access_marks(user_obj::instance($row[$user_id_key]), $stream_obj))
				{
					$row["mark"] = "ACCESS_DENIED";
					unset($row["comment"]);
				}
			}
		}
		unset($row);
	}

}

?>