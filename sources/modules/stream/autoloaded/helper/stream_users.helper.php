<?php

class stream_users_helper extends base_static_db_helper
{

	public static function get_admins_as_keys($stream_id)
	{
		static $cache = null;
		if (!is_null($cache))
		{
			return $cache;
		}
		$data = self::db()->fetch_column_values("
			SELECT a.user_id
			FROM stream_admin a
			JOIN stream_user_link l ON l.stream_id = a.stream_id AND l.user_id = a.user_id
			WHERE a.stream_id = {$stream_id} AND a.is_admin = 1 AND l.status = 'admin'
		", true, "user_id");
		$cache = $data;
		return $data;
	}

	public static function get_moderators_as_keys($stream_id)
	{
		static $cache = null;
		if (!is_null($cache))
		{
			return $cache;
		}
		$data = self::db()->fetch_column_values("
			SELECT a.user_id
			FROM stream_admin a
			JOIN stream_user_link l ON l.stream_id = a.stream_id AND l.user_id = a.user_id
			WHERE a.stream_id = {$stream_id} AND a.is_admin = 0 AND l.status = 'moderator'
		", true, "user_id");
		$cache = $data;
		return $data;
	}

}

?>