<?php

class activity_participants_subscriptions_helper extends base_static_db_helper
{

	public static function fetch_participants_subscriptions_to_edit($activity_id)
	{
		$activity_row = activity_helper::get_row($activity_id);
		$stream_id = $activity_row["stream_id"];
		$data = self::fetch_participants_subscriptions_common($activity_id);
		// $data contains all subscribed(!) people, let's show it explicitly:
		foreach ($data as &$row)
		{
			$row["is_subscribed"] = true;
		}
		unset($row);
		// it's time to add other possible users
		self::add_users_as_keys_to_participants_subscriptions($data, stream_users_helper::get_moderators_as_keys($stream_id), true);
		self::add_users_as_keys_to_participants_subscriptions($data, stream_users_helper::get_admins_as_keys($stream_id), true);

		global $user;
		/* @var $user user */
		$is_admin = $user->is_admin();
		if ($is_admin)
		{
			self::add_users_as_keys_to_participants_subscriptions($data, system_users_helper::get_system_admins_as_keys(), true);
		}

		// Me first
		if (isset($data[$user->get_user_id()]))
		{
			$new_data = array();
			$new_data[$user->get_user_id()] = $data[$user->get_user_id()];
			foreach ($data as $user_id => $user_data)
			{
				if ($user_id != $user->get_user_id())
				{
					$new_data[$user_id] = $user_data;
				}
			}
			$data = $new_data;
		}
		return $data;
	}

	private static function add_users_as_keys_to_participants_subscriptions(&$data, $users_as_keys, $mark_as_regular)
	{
		foreach ($users_as_keys as $user_id => $tmp)
		{
			if (!isset($data[$user_id]))
			{
				$data[$user_id] = array(
					"id" => $user_id,
				);
			}
			if ($mark_as_regular)
			{
				$data[$user_id]["is_regular"] = true;
			}
		}
	}

	public static function fetch_participants_subscriptions_to_send($activity_id)
	{
		static $cache = array();
		if (isset($cache[$activity_id]))
		{
			return $cache[$activity_id];
		}
		$data = self::fetch_participants_subscriptions_common($activity_id);
		$data = self::filter_participants_subscriptions($activity_id, $data);
		$cache[$activity_id] = $data;
		return $data;
	}

	private static function fetch_participants_subscriptions_common($activity_id)
	{
		$data = self::db()->fetch_all("
			SELECT user_id AS id
			FROM activity_participants_subscription
			WHERE activity_id = {$activity_id}
		", "id");
		return $data;
	}

	private static function filter_participants_subscriptions($activity_id, $data)
	{
		$activity_row = activity_helper::get_row($activity_id);
		$stream_id = $activity_row["stream_id"];

		$allowed = system_users_helper::get_system_admins_as_keys();
		$allowed2 = stream_users_helper::get_moderators_as_keys($stream_id);
		$allowed3 = stream_users_helper::get_admins_as_keys($stream_id);
		array_merge_good($allowed, $allowed2);
		array_merge_good($allowed, $allowed3);

		$result = array();
		foreach ($data as $id => $row)
		{
			$user_id = $row["id"];
			if (isset($allowed[$user_id]))
			{
				$result[$id] = $row;
			}
		}
		return $result;
	}

	public static function change_participants_subscription($activity_id, $user_id, $is_subscribed)
	{
		if ($is_subscribed)
		{
			self::db()->sql("
				INSERT IGNORE INTO activity_participants_subscription
				SET 
					activity_id = {$activity_id},
					user_id = {$user_id}
			");
		}
		else
		{
			self::db()->sql("
				DELETE FROM activity_participants_subscription
				WHERE 
					activity_id = {$activity_id}
					AND user_id = {$user_id}
			");
		}
	}

	public static function get_participants_subscriptions_count($activity_id)
	{
		$count = self::db()->get_value("
			SELECT COUNT(*) 
			FROM activity_participants_subscription
			WHERE activity_id = {$activity_id}
		");
		return $count;
	}

}

?>