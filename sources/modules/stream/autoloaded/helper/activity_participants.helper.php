<?php

class activity_participants_helper extends base_static_db_helper
{

	const PARTCIPANT_STATUS_ACCEPTED = "accepted";
	const PARTCIPANT_STATUS_DECLINED = "declined";
	const PARTCIPANT_STATUS_PREMODERATION = "premoderation";
	const PARTCIPANT_STATUS_DELETED = "deleted";

	public static function fetch_participants($activity_id)
	{
		$status_deleted = self::PARTCIPANT_STATUS_DELETED;
		// changer is not needed here
		$data = self::db()->fetch_all("
			SELECT 
				ap.user_id AS id, 
				ap.role_id, ap.status,
				ap.change_time,
				CONCAT(ap.user_id, '-', ap.role_id) AS `key`
			FROM activity_participant ap
			WHERE 
				ap.activity_id = {$activity_id}
				AND ap.status <> '{$status_deleted}'
		", "key");
		$marks = self::db()->fetch_all("
			SELECT 
				apm.user_id AS id, 
				apm.role_id, apm.mark, apm.comment,
				apm.changer_user_id,
				apm.change_time,
				CONCAT(apm.user_id, '-', apm.role_id) AS `key`
			FROM activity_participant_mark apm
			WHERE 
				apm.activity_id = {$activity_id}
		", "key");
		array_merge_deep($data, $marks);
		array_unset_column($data, "key");
		$data = array_sort($data, "change_time", SORT_ASC);
		return $data;
	}

	public static function is_good_participant_status($status)
	{
		return in_array($status, array(self::PARTCIPANT_STATUS_ACCEPTED, self::PARTCIPANT_STATUS_DECLINED, self::PARTCIPANT_STATUS_PREMODERATION, self::PARTCIPANT_STATUS_DELETED));
	}

	public static function fetch_participants_raw_stat($activity_id)
	{
		$status_accepted = self::PARTCIPANT_STATUS_ACCEPTED;
		$status_premoderation = self::PARTCIPANT_STATUS_PREMODERATION;
		$accepted_count = self::db()->get_value("
			SELECT COUNT(*)
			FROM activity_participant
			WHERE activity_id = {$activity_id}
				AND status = '{$status_accepted}'
		");
		$premoderation_count = self::db()->get_value("
			SELECT COUNT(*)
			FROM activity_participant
			WHERE activity_id = {$activity_id}
				AND status = '{$status_premoderation}'
		");
		return array(
			"accepted_count" => $accepted_count,
			"premoderation_count" => $premoderation_count,
		);
	}

	public static function change_participant_status($activity_id, $role_id, $user_id, $status, $can_add)
	{
		global $user;
		/* @var $user user */

		$current_status = self::get_current_status($activity_id, $role_id, $user_id);
		$current_is_for_news_status = self::get_current_is_for_news_status($activity_id, $role_id, $user_id);
		$is_for_news = ($current_status === self::PARTCIPANT_STATUS_PREMODERATION || $current_is_for_news_status) && in_array($status, array(self::PARTCIPANT_STATUS_ACCEPTED, self::PARTCIPANT_STATUS_DECLINED));
		if (!$can_add && $current_status === self::PARTCIPANT_STATUS_DELETED)
		{
			return false;
		}
		self::remove_status($activity_id, $role_id, $user_id);
		$data = array(
			"activity_id" => $activity_id,
			"role_id" => $role_id,
			"user_id" => $user_id,
			"status" => "'{$status}'",
			"change_time" => "NOW()",
			"is_for_news" => $is_for_news ? "1" : "0",
			"changer_user_id" => $user->get_user_id(),
		);
		self::db()->insert_by_array("activity_participant", $data);
		unset($data["change_time"]);
		unset($data["is_for_news"]);
		$data["add_time"] = "NOW()";
		self::db()->insert_by_array("activity_participant_log", $data);
		self::update_participant_status_calc($activity_id, $user_id);
		stream_calc_helper::update_activity_participant_count_calc($activity_id);
		stream_calc_helper::update_activity_unset_mark_count_calc($activity_id);
		self::update_taken_number($activity_id, $role_id);
		return true;
	}

	public static function remove_status($activity_id, $role_id, $user_id)
	{
		self::db()->sql("
			DELETE
			FROM activity_participant
			WHERE activity_id = {$activity_id}
				AND user_id = {$user_id}
				AND role_id = {$role_id}
		");
	}

	public static function update_participant_status_calc($activity_id, $user_id)
	{
		$statuses = self::db()->fetch_column_values("
			SELECT status 
			FROM activity_participant
			WHERE activity_id = {$activity_id}
				AND user_id = {$user_id}
		", "status");
		$numbers = array();
		foreach ($statuses as $status)
		{
			$numbers[] = self::status_to_number($status);
		}
		$number_max = max($numbers);
		if ($number_max)
		{
			$status_calc = self::number_to_status($number_max);
			self::db()->sql("
				INSERT INTO activity_participant_status_calc
				SET 
					activity_id = {$activity_id},
					user_id = {$user_id},
					status = '{$status_calc}'
				ON DUPLICATE KEY UPDATE
					status = VALUES(status)
			");
		}
		else
		{
			self::db()->sql("
				DELETE FROM activity_participant_status_calc
				WHERE activity_id = {$activity_id}
					AND user_id = {$user_id}
			");
		}
	}

	public static function status_to_number($status)
	{
		switch ($status)
		{
			case self::PARTCIPANT_STATUS_ACCEPTED:
				return 3;
			case self::PARTCIPANT_STATUS_PREMODERATION:
				return 2;
			case self::PARTCIPANT_STATUS_DECLINED:
				return 1;
			case self::PARTCIPANT_STATUS_DELETED:
				return 0;
			default:
				trigger_error("Unexpected status '{$status}'");
				return 0;
		}
	}

	public static function number_to_status($number)
	{
		switch ($number)
		{
			case 3:
				return self::PARTCIPANT_STATUS_ACCEPTED;
			case 2:
				return self::PARTCIPANT_STATUS_PREMODERATION;
			case 1:
				return self::PARTCIPANT_STATUS_DECLINED;
			case 0:
				return self::PARTCIPANT_STATUS_DELETED;
			default:
				trigger_error("Unexpected status number: '{$number}'");
				return self::PARTCIPANT_STATUS_DELETED;
		}
	}

	public static function update_taken_number($activity_id, $role_id, $taken_number = null)
	{
		$taken_number = !is_null($taken_number) ? $taken_number : self::get_taken_number($activity_id, $role_id);
		self::db()->sql("
			INSERT INTO activity_role_stat
			SET 
				taken_number = {$taken_number},
				activity_id = {$activity_id},
				role_id = '{$role_id}'
			ON DUPLICATE KEY UPDATE
				taken_number = VALUES(taken_number)
		");
	}

	public static function get_taken_number($activity_id, $role_id)
	{
		$status_accepted = self::PARTCIPANT_STATUS_ACCEPTED;
		$taken_number = self::db()->get_value("
			SELECT COUNT(*)
			FROM activity_participant ap
			WHERE ap.activity_id = {$activity_id}
				AND ap.role_id = {$role_id}
				AND ap.status = '{$status_accepted}'
		");
		return $taken_number;
	}

	public static function get_current_status($activity_id, $role_id, $user_id)
	{
		$current_status = self::db()->get_value("
			SELECT status 
			FROM activity_participant
			WHERE activity_id = {$activity_id}
				AND user_id = {$user_id}
				AND role_id = {$role_id}
		");
		if (!$current_status)
		{
			$current_status = self::PARTCIPANT_STATUS_DELETED;
		}
		return $current_status;
	}

	public static function get_current_is_for_news_status($activity_id, $role_id, $user_id)
	{
		$is_for_news = self::db()->get_value("
			SELECT is_for_news 
			FROM activity_participant
			WHERE activity_id = {$activity_id}
				AND user_id = {$user_id}
				AND role_id = {$role_id}
		");
		return !!$is_for_news;
	}

}

?>