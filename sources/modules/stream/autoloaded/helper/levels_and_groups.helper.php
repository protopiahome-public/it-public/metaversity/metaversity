<?php

class levels_and_groups_helper extends base_static_db_helper
{

	public static function get_groups_info_json($stream_id)
	{
		$data = self::db()->fetch_all("
			SELECT id, study_level_id, city_id
			FROM `group`
			WHERE stream_id = {$stream_id}
		");
		return json_encode($data);
	}

	/**
	 * @return array(
	 *   <stream_id> => array(
	 *     "l" => array(
	 *       <study_level_id> => true,
	 *       ...
	 *     ),
	 *     "g" => array(
	 *       <group_id> => array(<study_level_id>, <city_id>),
	 *       ...
	 *     ),
	 *   ),
	 *   ...
	 * ),
	 */
	public static function get_streams_check_list()
	{
		$result = streams_check_list_cache::init()->get();
		if ($result)
		{
			return $result;
		}
		$result = self::db()->fetch_column_values("SELECT id FROM stream", array(
			"l" => array(),
			"g" => array(),
			), "id");
		$study_levels = self::db()->fetch_all("SELECT id, stream_id FROM study_level");
		foreach ($study_levels as $l)
		{
			$result[$l["stream_id"]]["l"][$l["id"]] = true;
		}
		$groups = self::db()->fetch_all("SELECT id, stream_id, study_level_id, city_id FROM `group`");
		foreach ($groups as $g)
		{
			$result[$g["stream_id"]]["g"][$g["id"]] = array(
				(int) $g["study_level_id"] ? : null,
				(int) $g["city_id"] ? : null,
			);
		}
		streams_check_list_cache::init()->set($result);
		return $result;
	}

}

?>