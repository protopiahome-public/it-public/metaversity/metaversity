<?php

class activity_marks_helper extends base_static_db_helper
{

	public static function is_good_participant_mark($mark)
	{
		return $mark === "-" || is_good_num($mark) && $mark <= 3;
	}

	public static function change_participant_mark($activity_id, $role_id, $user_id, $mark, $comment)
	{
		global $user;
		/* @var $user user */

		$comment_escaped = self::db()->escape($comment);

		self::remove_participant_mark($activity_id, $role_id, $user_id);

		$data = array(
			"activity_id" => $activity_id,
			"role_id" => $role_id,
			"user_id" => $user_id,
			"mark" => "{$mark}",
			"comment" => "'{$comment_escaped}'",
			"change_time" => "NOW()",
			"changer_user_id" => $user->get_user_id(),
		);

		if ($mark === "-")
		{
			$data["mark"] = "NULL";
		}
		else
		{
			self::db()->insert_by_array("activity_participant_mark", $data);
		}
		self::db()->insert_by_array("activity_participant_mark_log", $data);
		$activity_row = activity_helper::get_row($activity_id);
		stream_calc_helper::update_activity_mark_count_calc($activity_id);
		stream_calc_helper::update_activity_unset_mark_count_calc($activity_id);
		stream_calc_helper::update_stream_user_link_mark_count_calc($activity_row["stream_id"], $user_id);
		stream_calc_helper::update_user_mark_count_calc($user_id);
	}

	public static function remove_participant_mark($activity_id, $role_id, $user_id)
	{
		self::db()->sql("
			DELETE
			FROM activity_participant_mark
			WHERE activity_id = {$activity_id}
				AND user_id = {$user_id}
				AND role_id = {$role_id}
		");
	}

	public static function fetch_marks_stat($activity_id)
	{
		$activity_row = self::db()->get_row("SELECT mark_count_calc, unset_mark_count_calc FROM activity WHERE id = {$activity_id}");
		$stat = array(
			"mark_count" => $activity_row ? $activity_row["mark_count_calc"] : 0,
			"unset_mark_count" => $activity_row ? $activity_row["unset_mark_count_calc"] : 0,
		);
		return $stat;
	}

	public static function is_good_competence_mark($mark)
	{
		return $mark === "-" || is_good_num($mark) && $mark <= 3;
	}

	public static function change_competence_mark($activity_id, $user_id, $competence_id, $competence_set_id, $mark, $comment)
	{
		global $user;
		/* @var $user user */

		self::db()->sql("
			DELETE
			FROM activity_competence_mark
			WHERE activity_id = {$activity_id}
				AND user_id = {$user_id}
				AND competence_id = {$competence_id}
		");

		$comment_escaped = self::db()->escape($comment);

		$data = array(
			"activity_id" => $activity_id,
			"user_id" => $user_id,
			"competence_id" => $competence_id,
			"mark" => "{$mark}",
			"comment" => "'{$comment_escaped}'",
			"change_time" => "NOW()",
			"changer_user_id" => $user->get_user_id(),
		);

		if ($mark === "-")
		{
			$data["mark"] = "NULL";
		}
		else
		{
			self::db()->insert_by_array("activity_competence_mark", $data);
		}
		self::db()->insert_by_array("activity_competence_mark_log", $data);
		$activity_row = activity_helper::get_row($activity_id);
		stream_calc_helper::update_stream_user_link_mark_count_calc($activity_row["stream_id"], $user_id);
		stream_calc_helper::update_user_mark_count_calc($user_id);
		stream_calc_helper::update_activity_longterm_count_calc($activity_id, $competence_set_id);
	}

}

?>