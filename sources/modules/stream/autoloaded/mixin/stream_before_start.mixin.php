<?php

class stream_before_start_mixin extends base_mixin
{

	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function on_before_start()
	{
		$this->stream_id = $this->get_stream_id();
		if (!is_good_id($this->stream_id))
		{
			return false;
		}
		$this->stream_obj = stream_obj::instance($this->stream_id, $lock = true);
		if (!$this->stream_obj->exists())
		{
			return false;
		}

		return true;
	}

	protected function get_stream_id()
	{
		return REQUEST("stream_id");
	}

}

?>