<?php

class position_before_start_mixin extends base_mixin
{

	protected $stream_id;
	protected $position_id;
	protected $position_row;

	public function on_before_start()
	{
		$this->position_id = POST("position_id");
		if (!is_good_id($this->position_id))
		{
			return false;
		}
		$this->position_row = $this->db->sql("
			SELECT * 
			FROM position 
			WHERE id = {$this->position_id} 
				AND stream_id = {$this->stream_id}
			LOCK IN SHARE MODE
		");
		if (!$this->position_row)
		{
			return false;
		}
		return true;
	}

}

?>