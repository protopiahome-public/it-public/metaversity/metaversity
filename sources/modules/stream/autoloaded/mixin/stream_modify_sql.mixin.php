<?php

class stream_modify_sql_mixin extends base_mixin
{

	protected $stream_id;

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("stream_id = {$this->stream_id}");
	}

}

?>