<?php

class activity_before_start_mixin extends base_mixin
{

	protected $stream_id;
	protected $activity_id;
	protected $activity_row;

	public function on_before_start()
	{
		$this->activity_id = POST("activity_id");
		if (!is_good_id($this->activity_id))
		{
			return false;
		}
		$this->activity_row = activity_helper::get_row($this->activity_id, true);
		if (!$this->activity_row || $this->activity_row["stream_id"] != $this->stream_id)
		{
			return false;
		}
		return true;
	}

}

?>