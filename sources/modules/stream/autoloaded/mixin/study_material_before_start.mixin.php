<?php

class study_material_before_start_mixin extends base_mixin
{

	protected $stream_id;
	protected $study_material_id;
	protected $study_material_row;

	public function on_before_start()
	{
		$this->study_material_id = POST("study_material_id");
		if (!is_good_id($this->study_material_id))
		{
			return false;
		}
		$this->study_material_row = study_material_helper::get_row($this->study_material_id, true);
		if (!$this->study_material_row || $this->study_material_row["stream_id"] != $this->stream_id)
		{
			return false;
		}
		return true;
	}

}

?>