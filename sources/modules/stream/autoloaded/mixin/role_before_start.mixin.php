<?php

class role_before_start_mixin extends base_mixin
{

	protected $stream_id;
	protected $activity_row;
	protected $role_id;
	protected $role_row;

	public function on_before_start()
	{
		$this->role_id = POST("role_id");
		if (!is_good_id($this->role_id))
		{
			return false;
		}
		$this->role_row = $this->db->sql("
			SELECT r.* 
			FROM role r
			WHERE r.id = {$this->role_id}
				AND r.format_id = {$this->activity_row["format_id"]} 
			LOCK IN SHARE MODE
		");
		if (!$this->role_row)
		{
			return false;
		}
		return true;
	}

}

?>