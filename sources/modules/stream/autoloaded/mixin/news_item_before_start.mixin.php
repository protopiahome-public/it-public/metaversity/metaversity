<?php

class news_item_before_start_mixin extends base_mixin
{

	protected $stream_id;
	protected $news_item_id;
	protected $news_item_row;

	public function on_before_start()
	{
		$this->news_item_id = POST("news_item_id");
		if (!is_good_id($this->news_item_id))
		{
			return false;
		}
		$this->news_item_row = $this->db->get_row("
			SELECT * 
			FROM news_item 
			WHERE id = {$this->news_item_id} 
				AND stream_id = {$this->stream_id}
			LOCK IN SHARE MODE
		");
		if (!$this->news_item_row)
		{
			return false;
		}
		return true;
	}

}

?>