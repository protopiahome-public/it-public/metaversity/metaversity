<?php

class stream_obj extends base_obj
{

	// Settings
	protected $fake_delete = false;
	// Internal
	protected $access_array = array();

	/**
	 * @var request
	 */
	protected $request;

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;
	protected $cities = null;
	protected $groups = null;
	protected $current_city_id = null;
	protected $current_city_title = "";
	protected $current_study_level_id = null;
	protected $current_study_level_title = "";
	protected $current_group_id = null;
	protected $current_group_title = "";

	public function __construct($id, $lock = false)
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		global $request;
		$this->request = $request;

		parent::__construct($id, $lock);
	}

	private static $cache = array();

	/**
	 * @return stream_obj
	 */
	public static function instance($id, $lock = false)
	{
		if (!$lock && isset(self::$cache[$id]))
		{
			return self::$cache[$id];
		}
		$class = __CLASS__;
		$instance = new $class($id, $lock);
		if (!$lock)
		{
			self::$cache[$id] = $instance;
		}
		return $instance;
	}

	public static function _test_clean_cache()
	{
		self::$cache = array();
	}

	/**
	 * @return stream_access
	 */
	public function get_access($user_id = null)
	{
		if (!$this->exists())
		{
			trigger_error("Can't return access for unexisted stream (id={$this->get_id()})");
			return null;
		}
		$key = $user_id ? $user_id : 0;
		if (!isset($this->access_array[$key]))
		{
			$this->access_array[$key] = new stream_access($this, $user_id);
		}
		return $this->access_array[$key];
	}

	public function get_name()
	{
		return $this->get_param("name");
	}

	public function get_title()
	{
		return $this->get_param($this->lang->get_current_lang_code() === "en" ? "title_en" : "title");
	}

	public function get_title_short()
	{
		return $this->get_param($this->lang->get_current_lang_code() === "en" ? "title_short_en" : "title_short");
	}

	public function get_competence_set_id()
	{
		return (int) $this->get_param("competence_set_id");
	}

	public function get_url($no_protocol = false)
	{
		if ($this->domain_logic->is_on())
		{
			$host_name = $this->get_param("name") . "." . $this->domain_logic->get_main_host_display_name();
			return ($no_protocol ? "//" : $this->request->get_protocol(true)) . $host_name . "/";
		}
		else
		{
			return $this->domain_logic->get_streams_prefix() . "/" . $this->get_param("name") . "/";
		}
	}
	
	public function get_position_credit_min_match()
	{
		return (int) $this->get_param("position_credit_min_match");
	}

	public function join_premoderation()
	{
		return $this->get_param("join_premoderation") == 1;
	}

	public function users_import_allowed()
	{
		return $this->get_param("users_import_allowed") == 1;
	}

	public function get_cities()
	{
		if (is_null($this->cities))
		{
			$this->fill_cities();
		}
		return $this->cities;
	}

	public function get_current_city_id()
	{
		if (is_null($this->current_city_id))
		{
			$this->fill_current_values();
		}
		return $this->current_city_id;
	}

	public function get_current_city_title()
	{
		if (is_null($this->current_city_id))
		{
			$this->fill_current_values();
		}
		return $this->current_city_title;
	}

	public function get_current_study_level_id()
	{
		if (is_null($this->current_study_level_id))
		{
			$this->fill_current_values();
		}
		return $this->current_study_level_id;
	}

	public function get_current_study_level_title()
	{
		if (is_null($this->current_study_level_id))
		{
			$this->fill_current_values();
		}
		return $this->current_study_level_title;
	}

	public function get_current_group_id()
	{
		if (is_null($this->current_group_id))
		{
			$this->fill_current_values();
		}
		return $this->current_group_id;
	}

	public function get_current_group_title()
	{
		if (is_null($this->current_group_id))
		{
			$this->fill_current_values();
		}
		return $this->current_group_title;
	}

	public function is_city_defined($city_id)
	{
		if (is_null($this->cities))
		{
			$this->fill_cities();
		}
		return is_good_id($city_id) and isset($this->cities[$city_id]);
	}

	protected function fill_current_values()
	{
		global $user;
		/* @var $user user */

		if (is_null($this->cities))
		{
			$this->fill_cities();
		}
		$city_id_raw = COOKIE("current_city_id");
		$this->current_city_id = isset($this->cities[$city_id_raw]) ? (int) $city_id_raw : 0;
		$this->current_study_level_id = 0;
		$this->current_group_id = 0;
		if ($this->current_city_id)
		{
			$group_id_raw = COOKIE("current_group_id");
			$this->current_group_id = isset($this->cities[$this->current_city_id]["groups"][$group_id_raw]) ? (int) $group_id_raw : 0;
		}
		if (!$this->current_city_id and $city_id_raw !== "0" and $user)
		{
			$this->current_group_id = (int) $this->db->get_value("SELECT group_id FROM stream_user_link WHERE stream_id = {$this->id} AND user_id = {$user->get_user_id()}");
			if ($this->current_group_id)
			{
				$this->current_city_id = (int) $this->groups[$this->current_group_id]["city_id"];
			}
			else
			{
				$user_city_id = $user->get_city_id();
				$this->current_city_id = isset($this->cities[$user_city_id]) ? (int) $user_city_id : 0;
			}
		}
		if ($this->current_city_id)
		{
			$this->current_city_title = $this->cities[$this->current_city_id]["title"];
		}
		if ($this->current_group_id)
		{
			$this->current_group_title = $this->groups[$this->current_group_id]["title"];
			$this->current_study_level_id = (int) $this->groups[$this->current_group_id]["study_level_id"];
			$this->current_study_level_title = $this->groups[$this->current_group_id]["study_level_title"];
		}
	}

	protected function fill_cities()
	{
		global $user;
		/* @var $user user */

		list($this->cities, $this->groups) = stream_cities_cache::init($this->id, $this->lang->get_current_lang_code())->get();
		if (!empty($this->cities))
		{
			return;
		}
		$city_id = $user ? $user->get_city_id() : 0;
		$city_title_sql = $this->lang->get_current_lang_code() === "en" ? "c.title_en AS title" : "c.title";
		$this->cities = $this->db->fetch_all("
			SELECT c.id, {$city_title_sql}
			FROM stream_city_link l
			JOIN city c ON c.id = l.city_id
			WHERE l.stream_id = {$this->id}
			ORDER BY c.id = {$city_id} DESC, c.title
		", "id");
		foreach ($this->cities as &$city_data)
		{
			$city_data["groups"] = array();
		}
		unset($city_data);
		$city_title_sql = $this->lang->get_current_lang_code() === "en" ? "c.title_en AS city_title" : "c.title AS city_title";
		$this->groups = $this->db->fetch_all("
			SELECT g.id, g.city_id, g.study_level_id, sl.title AS study_level_title, g.title, {$city_title_sql}
			FROM `group` g
			JOIN city c ON c.id = g.city_id
			LEFT JOIN study_level sl ON sl.id = g.study_level_id
			WHERE g.stream_id = {$this->id}
			ORDER BY g.title
		", "id");
		foreach ($this->groups as $group_id => $group_data)
		{
			$city_id = $group_data["city_id"];
			if (!isset($this->cities[$city_id]))
			{
				$this->cities[$city_id] = array(
					"id" => $city_id,
					"title" => $group_data["city_title"],
					"groups" => array(),
				);
			}
			$this->cities[$city_id]["groups"][$group_id] = array(
				"id" => $group_id,
				"title" => $group_data["title"],
			);
		}
		stream_cities_cache::init($this->id, $this->lang->get_current_lang_code())->set(array($this->cities, $this->groups));
	}

	protected function fill_data($lock)
	{
		if ($lock)
		{
			$this->data = $this->fetch_data(true);
		}
		else
		{
			$this->data = stream_cache::init($this->id)->get();
			if (empty($this->data))
			{
				$this->data = $this->fetch_data();
				if (!$this->data)
				{
					$this->data = array();
				}
				else
				{
					stream_cache::init($this->id)->set($this->data);
				}
			}
		}
	}

	private function fetch_data($lock = false)
	{
		$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
		$row = $this->db->get_row("
			SELECT *
			FROM stream
			WHERE id = {$this->id}
			{$lock_sql}
		");
		return $row ? $row : null;
	}

}

?>