<?php

class stream_user_short_cache extends base_cache
{

	public static function init($stream_id, $user_id, $lang_code)
	{
		$tag_keys = array();
		$tag_keys[] = user_cache_tag::init($user_id)->get_key();
		$tag_keys[] = cities_cache_tag::init()->get_key();
		$tag_keys[] = stream_user_cache_tag::init($stream_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $stream_id, $user_id, $lang_code, $tag_keys);
	}

}

?>