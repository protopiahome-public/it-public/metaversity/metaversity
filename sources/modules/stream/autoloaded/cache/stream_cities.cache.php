<?php

// @todo stream_cities_and_groups_cache?
class stream_cities_cache extends base_cache
{

	public static function init($stream_id, $lang_code)
	{
		$tag_keys = array();
		$tag_keys[] = stream_cache_tag::init($stream_id)->get_key();
		$tag_keys[] = cities_cache_tag::init()->get_key();
		$tag_keys[] = stream_study_levels_cache_tag::init($stream_id)->get_key();
		$tag_keys[] = stream_groups_cache_tag::init($stream_id)->get_key();
		return parent::get_cache(__CLASS__, $stream_id, $lang_code, $tag_keys);
	}

}

?>