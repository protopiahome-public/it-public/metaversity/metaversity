<?php

class stream_short_cache extends base_cache
{

	public static function init($stream_id, $lang_code)
	{
		$tag_keys = array();
		$tag_keys[] = stream_cache_tag::init($stream_id)->get_key();
		return parent::get_cache(__CLASS__, $stream_id, $lang_code, $tag_keys);
	}

}

?>