<?php

class stream_activity_roles_cache extends base_cache
{

	public static function init(stream_obj $stream_obj, $activity_id, $format_id)
	{
		$tag_keys = array();
		$tag_keys[] = activity_cache_tag::init($activity_id)->get_key();
		$tag_keys[] = format_cache_tag::init($format_id)->get_key();
		$tag_keys[] = stream_competence_set_cache_tag::init($stream_obj->get_id())->get_key();
		$tag_keys[] = competence_set_competences_cache_tag::init($stream_obj->get_competence_set_id())->get_key();
		return parent::get_cache(__CLASS__, $activity_id, $tag_keys);
	}

}

?>