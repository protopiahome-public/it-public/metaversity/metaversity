<?php

class stream_access_cache extends base_cache
{

	public static function init($stream_id, $user_id)
	{
		$tag_keys = array();
		$tag_keys[] = stream_user_cache_tag::init($stream_id, $user_id)->get_key();
		return parent::get_cache(__CLASS__, $stream_id, $user_id, $tag_keys);
	}

}

?>