<?php

class format_short_cache extends base_cache
{

	public static function init($format_id, $stream_id)
	{
		$tag_keys = array();
		$tag_keys[] = stream_formats_cache_tag::init($stream_id)->get_key();
		return parent::get_cache(__CLASS__, $format_id, $tag_keys);
	}

}

?>