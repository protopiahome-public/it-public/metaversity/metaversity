<?php

class activity_short_cache extends base_cache
{

	public static function init($activity_id, $stream_id, $lang_code)
	{
		$tag_keys = array();
		$tag_keys[] = activity_cache_tag::init($activity_id)->get_key();
		$tag_keys[] = stream_metaactivities_cache_tag::init($stream_id)->get_key();
		$tag_keys[] = stream_formats_cache_tag::init($stream_id)->get_key();
		$tag_keys[] = cities_cache_tag::init()->get_key();
		return parent::get_cache(__CLASS__, $activity_id, $stream_id, $lang_code, $tag_keys);
	}

}

?>