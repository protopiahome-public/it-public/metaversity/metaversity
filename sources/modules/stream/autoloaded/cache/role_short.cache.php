<?php

class role_short_cache extends base_cache
{

	public static function init($role_id)
	{
		$tag_keys = array();
		$tag_keys[] = role_cache_tag::init($role_id)->get_key();
		return parent::get_cache(__CLASS__, $role_id, $tag_keys);
	}

}

?>