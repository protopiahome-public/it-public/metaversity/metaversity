<?php

class stream_user_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "user";
	protected $axis_name = "short";
	// Internal
	protected $stream_id = null;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		$this->stream_obj = stream_obj::instance($this->stream_id);
		parent::__construct($id);
	}

	protected function get_cache()
	{
		return stream_user_short_cache::init($this->stream_id, $this->id, $this->lang->get_current_lang_code());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		if ($this->lang->get_current_lang_code() === "en")
		{
			$select_sql->add_select_fields("dt.first_name_en AS first_name, dt.last_name_en AS last_name");
		}
		else
		{
			$select_sql->add_select_fields("dt.first_name, dt.last_name");
		}
		$select_sql->add_join("LEFT JOIN `stream_user_link` l ON l.user_id = dt.id AND l.stream_id = {$this->stream_id}");
		$select_sql->add_join("LEFT JOIN `group` g ON g.id = l.group_id");
		$select_sql->add_select_fields("l.status");
		$select_sql->add_select_fields("g.id AS group_id, g.title AS group_title");
	}

	protected function process_data()
	{
		$data = &$this->data[0];
		$full_name = trim($data["first_name"] . " " . $data["last_name"]);
		$data["visible_name"] = $full_name ? : $data["login"];
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$data = &$this->data[0];
		$xdom->set_attr("stream_id", $this->stream_id);
		$xdom->set_attr("group_id", $data["group_id"]);
		$xdom->set_attr("group_title", $data["group_title"]);
		$xdom->set_attr("visible_name", $data["visible_name"]);
		$xdom->set_attr("status", $data["status"]);
	}

}

?>