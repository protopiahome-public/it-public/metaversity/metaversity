<?php

class stream_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array();
	protected $dt_name = "stream";
	protected $axis_name = "short";
	// Internal
	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct($stream_id)
	{
		$this->stream_obj = stream_obj::instance($stream_id);
		parent::__construct($stream_id);
	}

	protected function get_cache()
	{
		return stream_short_cache::init($this->id, $this->lang->get_current_lang_code());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		if ($this->lang->get_current_lang_code() === "en")
		{
			$select_sql->add_select_fields("dt.title_en, dt.title_short_en");
			$select_sql->add_select_fields("lang_code_foreign_table.title_en AS lang_title_en");
		}
		$select_sql->add_select_fields("lang_code_foreign_table.code AS lang_code");
	}

	protected function process_data()
	{
		$data = &$this->data[0];
		if ($this->lang->get_current_lang_code() === "en")
		{
			$data["title"] = $data["title_en"];
			unset($data["title_en"]);
			$data["title_short"] = $data["title_short_en"];
			unset($data["title_short_en"]);
			$data["lang_title"] = $data["lang_title_en"];
			unset($data["lang_title_en"]);
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("name", $this->stream_obj->get_name());
		$xdom->set_attr("position_credit_min_match", $this->stream_obj->get_param("position_credit_min_match"));
		$xdom->set_attr("url", "%STREAM_URL%");
		if ($this->data)
		{
			$xdom->set_attr("lang_code", $this->data[0]["lang_code"]);
		}
	}

	protected function postprocess()
	{
		$this->xml = preg_replace("/%STREAM_URL%/", $this->stream_obj->get_url(), $this->xml, 1);
	}

}

?>