<?php

class stream_study_level_restrictions_xml_ctrl extends base_xml_ctrl
{

	protected $stream_id;
	protected $object_type;
	protected $id_column;
	protected $study_level_link_table_name;
	protected $object_restriction;
	protected $current_study_level_id = 0;

	public function __construct($stream_id, $object_type, $study_level_link_table_name, $object_restriction = null, $current_study_level_id = 0)
	{
		$this->stream_id = $stream_id;
		$this->object_type = $object_type;
		$this->id_column = $this->object_type . "_id";
		$this->study_level_link_table_name = $study_level_link_table_name;
		$this->object_restriction = $object_restriction;
		$this->current_study_level_id = (int) $current_study_level_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_NO_CACHE;
		$xdom = xdom::create($this->name);

		$study_levels = $this->db->fetch_all("
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$this->stream_id}
			ORDER BY title
		", "id");

		$this->check_currents($study_levels);
		if ($this->current_study_level_id)
		{
			$xdom->set_attr("current_study_level_id", $this->current_study_level_id);
		}

		$object_restriction_sql = "
			INNER JOIN {$this->object_type} o ON o.id = l.{$this->id_column}
			WHERE o.stream_id = {$this->stream_id}
		";
		if ($this->object_restriction)
		{
			$object_restriction_sql .= " AND {$this->object_restriction}";
		}

		$study_level_links = $this->db->fetch_all("
			SELECT l.{$this->id_column} AS id, l.study_level_id
			FROM `{$this->study_level_link_table_name}` l
			{$object_restriction_sql}
		");

		$objects = array();

		foreach ($study_level_links as $study_level_link)
		{
			$id = $study_level_link["id"];
			$study_level_id = $study_level_link["study_level_id"];
			if (!isset($study_levels[$study_level_id]))
			{
				trigger_error("Data inconsistency: stream_id = {$this->stream_id}, unexpected study_level_id = '{$study_level_id}'");
			}
			array_key_set($objects, $id);
			array_key_set($objects[$id], $study_level_id);
		}

		foreach ($study_levels as $study_level_data)
		{
			$xdom->create_child_node("study_level")
				->set_attr("id", $study_level_data["id"])
				->set_attr("title", $study_level_data["title"]);
		}

		// current study level
		$objects_show = array();
		foreach ($objects as $id => $object_data)
		{
			$show = true;
			if ($this->current_study_level_id and sizeof($object_data))
			{
				if (!isset($object_data[$this->current_study_level_id]))
				{
					$show = false;
				}
			}
			if ($show)
			{
				$objects_show[$id] = true;
			}
		}

		// export
		foreach ($objects as $id => $object_data)
		{
			$object_node = $xdom->create_child_node("object")
				->set_attr("type", $this->object_type)
				->set_attr("id", $id);
			foreach ($object_data as $study_level_id => $study_level_data)
			{
				$object_node->create_child_node("study_level")
					->set_attr("id", $study_level_id)
					->set_attr("title", $study_levels[$study_level_id]["title"]);
			}
			if (isset($objects_show[$id]))
			{
				$object_node->set_attr("show", $objects_show[$id]);
			}
		}

		return $xdom->get_xml(true);
	}

	protected function check_currents($study_levels)
	{
		if ($this->current_study_level_id)
		{
			if (!isset($study_levels[$this->current_study_level_id]))
			{
				$this->current_study_level_id = 0;
				return;
			}
		}
	}

}

?>