<?php

class position_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "position";
	protected $axis_name = "full";
	// Internal
	protected $stream_id;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		parent::__construct($id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
	}

}

?>