<?php

class study_material_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	/**
	 * @var study_material_dt
	 */
	protected $dt;
	protected $dt_name = "study_material";
	protected $axis_name = "full";
	// Internal
	protected $stream_id;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		parent::__construct($id);
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_obj(stream_obj::instance($this->stream_id));
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
		$user_id = $this->user->get_user_id();
		if ($user_id)
		{
			$select_sql->add_join("LEFT JOIN study_material_user_link l ON l.study_material_id = dt.id AND l.user_id = {$user_id}");
			$status_default = study_material_helper::STATUS_DEFAULT;
			$select_sql->add_select_fields("IFNULL(l.status, '{$status_default}') AS status");
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		if (isset($this->data[0]["status"]))
		{
			$xdom->set_attr("status", $this->data[0]["status"]);
		}
	}

}

?>