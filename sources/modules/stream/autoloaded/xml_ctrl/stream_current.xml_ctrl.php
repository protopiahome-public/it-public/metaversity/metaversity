<?php

class stream_current_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct($stream_id)
	{
		$this->stream_obj = stream_obj::instance($stream_id);
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		if (!$this->stream_obj->exists())
		{
			$this->set_error_404();
			return;
		}

		$xdom = xdom::create($this->name);
		$xdom->set_attr("id", $this->stream_obj->get_id());
		$xdom->set_attr("name", $this->stream_obj->get_name());
		$xdom->set_attr("title", $this->stream_obj->get_title());
		$xdom->set_attr("title_short", $this->stream_obj->get_title_short());
		$xdom->set_attr("url", $this->stream_obj->get_url());
		$xdom->set_attr("join_premoderation", $this->stream_obj->join_premoderation());
		$xdom->set_attr("users_import_allowed", $this->stream_obj->users_import_allowed());
		$xdom->set_attr("position_credit_min_match", $this->stream_obj->get_param("position_credit_min_match"));

		$current_city_id = $this->stream_obj->get_current_city_id();
		$current_study_level_id = $this->stream_obj->get_current_study_level_id();
		$current_group_id = $this->stream_obj->get_current_group_id();
		$cities_node = $xdom->create_child_node("cities")
			->set_attr("current_city_id", $current_city_id)
			->set_attr("current_city_title", $this->stream_obj->get_current_city_title())
			->set_attr("current_study_level_id", $current_study_level_id)
			->set_attr("current_study_level_title", $this->stream_obj->get_current_study_level_title())
			->set_attr("current_group_id", $current_group_id)
			->set_attr("current_group_title", $this->stream_obj->get_current_group_title());
		foreach ($this->stream_obj->get_cities() as $city_id => $city_data)
		{
			$city_node = $cities_node->create_child_node("city")
				->set_attr("id", $city_id)
				->set_attr("title", $city_data["title"])
				->set_attr_if_not_empty("is_current", $city_id == $current_city_id);
			foreach ($city_data["groups"] as $group_id => $group_data)
			{
				$city_node->create_child_node("group")
					->set_attr("id", $group_id)
					->set_attr("title", $group_data["title"])
					->set_attr_if_not_empty("is_current", $group_id == $current_group_id);
			}
		}
		return $xdom->get_xml(true);
	}

}

?>