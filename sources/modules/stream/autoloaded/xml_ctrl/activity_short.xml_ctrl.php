<?php

class activity_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "activity";
	protected $axis_name = "short";
	// Internal
	protected $stream_id;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		parent::__construct($id);
	}

	protected function get_cache()
	{
		return activity_short_cache::init($this->id, $this->stream_id, $this->lang->get_current_lang_code());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
		if ($this->lang->get_current_lang_code() === "en")
		{
			$select_sql->add_select_fields("city_id_foreign_table.title_en AS city_title_en");
		}
	}

	protected function process_data()
	{
		$data = &$this->data[0];
		if ($this->lang->get_current_lang_code() === "en")
		{
			$data["city_title"] = $data["city_title_en"];
			unset($data["city_title_en"]);
		}
	}

}

?>