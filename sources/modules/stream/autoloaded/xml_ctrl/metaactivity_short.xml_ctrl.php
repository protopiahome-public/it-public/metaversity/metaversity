<?php

class metaactivity_short_xml_ctrl extends base_dt_show_xml_ctrl
{
	protected $dt_name = "metaactivity";
	protected $axis_name = "short";
	// Internal
	protected $stream_id;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		parent::__construct($id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
	}

}

?>