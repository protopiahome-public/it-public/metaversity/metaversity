<?php

class format_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "format";
	protected $axis_name = "short";
	// Internal
	protected $stream_id;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		parent::__construct($id);
	}

	protected function get_cache()
	{
		return format_short_cache::init($this->id, $this->stream_id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
	}

}

?>