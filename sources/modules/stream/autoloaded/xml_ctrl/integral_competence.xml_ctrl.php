<?php

class integral_competence_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("id", "title");
	protected $xml_row_name = "competence";
	// Internal
	protected $id;
	protected $title;
	protected $competence_set_id;

	public function __construct($competence_id, $competence_set_id)
	{
		$this->id = $competence_id;
		$this->competence_set_id = $competence_set_id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$competence_row = $this->db->get_row("
			SELECT * 
			FROM competence_full 
			WHERE id = {$this->id} AND is_deleted = 0
		");
		if (!$competence_row)
		{
			$this->set_error_404();
			return;
		}
		if ($competence_row["is_integral"] != 1)
		{
			$this->set_error_404();
			return;
		}
		if ($this->competence_set_id and $this->competence_set_id != $competence_row["competence_set_id"])
		{
			$this->set_error_404();
			return;
		}
		$this->title = $competence_row["title"];

		$select_sql->add_select_fields("ic.included_competence_id AS id, c.title, ic.mark");
		$select_sql->add_from("integral_competence ic");
		$select_sql->add_join("INNER JOIN competence_calc c ON c.competence_id = ic.included_competence_id");
		$select_sql->add_where("ic.competence_id = {$this->id}");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>