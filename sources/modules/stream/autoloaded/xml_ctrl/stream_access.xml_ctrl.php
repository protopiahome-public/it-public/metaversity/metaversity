<?php

class stream_access_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var stream_access
	 */
	private $stream_access;
	private $all_strict_values;

	public function __construct(stream_access $stream_access, $all_strict_values = false)
	{
		$this->stream_access = $stream_access;
		$this->all_strict_values = $all_strict_values;
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$attrs = array(
			"stream_id" => $this->stream_access->get_stream_id(),
			"status" => $this->stream_access->get_status(),
			"is_member" => $this->stream_access->is_member(),
			"is_pretender" => $this->stream_access->is_pretender_strict(),
			"has_member_rights" => $this->stream_access->has_member_rights(),
			"has_moderator_rights" => $this->stream_access->has_moderator_rights(),
			"has_admin_rights" => $this->stream_access->has_admin_rights(),
			"join_premoderation" => $this->stream_access->join_premoderation(),
		);
		if ($this->all_strict_values)
		{
			$attrs["is_moderator_strict"] = $this->stream_access->is_moderator_strict();
			$attrs["is_admin_strict"] = $this->stream_access->is_admin_strict();
		}
		foreach ($this->stream_access->get_required_statuses() as $access_name => $status)
		{
			$attrs["access_" . $access_name] = $status;
		}

		return $this->get_node_string("stream_access", $attrs);
	}

}

?>