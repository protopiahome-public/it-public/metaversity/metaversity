<?php

class activity_full_xml_ctrl extends activity_short_xml_ctrl
{

	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $axis_name = "full";
	// Internal
	protected $show_role_rates;
	protected $state;
	protected $roles_data = array();
	protected $roles_data_city_id = null;

	public function __construct($id, $stream_id, $show_role_rates = false)
	{
		$this->show_role_rates = $show_role_rates;
		parent::__construct($id, $stream_id);
	}

	protected function get_cache()
	{
		return false;
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_obj(stream_obj::instance($this->stream_id));
	}

	protected function process_data()
	{
		$this->store_data = $this->data[0];
		$this->roles_data = activity_roles_helper::fetch_roles($this->id);
		if ($this->show_role_rates)
		{
			foreach ($this->roles_data as $role_data)
			{
				$this->xml_loader->add_xml(new rate_xml_ctrl($role_data["rate_id"], $this->stream_obj->get_competence_set_id(), array(0 => "0", 1 => "1", 2 => "2", 3 => "3")));
			}
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		$this->db_xml_converter->build_xml($xdom->create_child_node("roles"), $this->roles_data, false, "role");
	}

}

?>