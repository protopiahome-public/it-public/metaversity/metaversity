<?php

class stream_access extends base_access
{

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	/**
	 * @var stream_access_fetcher
	 */
	protected $access_fetcher;

	/**
	 * @var stream_access_maper
	 */
	protected $access_maper;
	protected $admin_row = false;

	public function __construct(stream_obj $stream_obj, $user_id = null)
	{
		$this->stream_obj = $stream_obj;
		parent::__construct($user_id);
	}

	protected function get_new_access_maper()
	{
		return new stream_access_maper();
	}

	protected function fill_data()
	{
		if (!$this->user_id)
		{
			$this->level = ACCESS_LEVEL_GUEST;
			return;
		}
		$cache = stream_access_cache::init($this->stream_obj->get_id(), $this->user_id);
		$data = $cache->get();
		if ($data)
		{
			$this->level = $data["level"];
			$this->admin_row = $data["admin_row"];
			return;
		}
		$this->access_fetcher = new stream_access_fetcher($this->stream_obj, $this->access_maper, $this->user_id);
		$this->level = $this->access_fetcher->get_level();
		if (!$this->access_fetcher->error_occured())
		{
			$this->admin_row = $this->access_fetcher->get_admin_row();
			$data = array(
				"level" => $this->level,
				"admin_row" => $this->admin_row,
			);
			$cache->set($data);
		}
	}

	public function join_premoderation()
	{
		return $this->stream_obj->join_premoderation();
	}

	public function get_stream_id()
	{
		return $this->stream_obj->get_id();
	}
	
	public function get_link_param($param_name)
	{
		$link_row = $this->access_fetcher->get_link_row();
		return $link_row && isset($link_row[$param_name]) ? $link_row[$param_name] : null;
	}

}

?>