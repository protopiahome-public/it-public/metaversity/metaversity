<?php

require_once PATH_CORE . "/loader/xml_loader.php";

class stream_access_save extends base_access_save
{

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	/**
	 * @var stream_access_maper
	 */
	protected $access_maper;

	/**
	 * @var stream_access_fetcher
	 */
	protected $access_fetcher;
	protected $link_row;
	protected $admin_row;

	public function __construct($stream_obj, $user_id)
	{
		$this->stream_obj = $stream_obj;
		parent::__construct($user_id);
	}

	protected function fill_data()
	{
		$this->access_maper = new stream_access_maper();
		$this->access_fetcher = new stream_access_fetcher($this->stream_obj, $this->access_maper, $this->user_id, $lock = true);
		$this->link_row = $this->access_fetcher->get_link_row();
		$this->admin_row = $this->access_fetcher->get_admin_row();
		$this->level = $this->access_fetcher->get_level();
		return !$this->access_fetcher->error_occured();
	}

	public function join()
	{
		if ($this->stream_obj->join_premoderation())
		{
			$this->add_pretender();
		}
		else
		{
			$this->add_member();
		}
	}

	public function unjoin()
	{
		$this->delete_member();
	}

	public function add_pretender()
	{
		if ($this->is_pretender_strict() or $this->is_member())
		{
			return true;
		}
		$new_status = ACCESS_STATUS_PRETENDER;
		$this->db->sql("
			INSERT INTO stream_user_link (stream_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->stream_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->send_emails_to_admins_about_pretender();
		return true;
	}

	public function add_member()
	{
		if ($this->is_member())
		{
			return true;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			INSERT INTO stream_user_link (stream_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->stream_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->update_stat();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function add_moderator()
	{
		if ($this->level > ACCESS_LEVEL_MODERATOR)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_MODERATOR)
		{
			return true;
		}
		$new_status = ACCESS_STATUS_MODERATOR;
		$adder_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			INSERT INTO stream_user_link (stream_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->stream_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->db->sql("
			INSERT INTO stream_admin (stream_id, user_id, add_time, adder_admin_user_id, edit_time, editor_admin_user_id, is_admin)
			VALUES ('{$this->stream_obj->get_id()}', '{$this->user_id}', NOW(), '{$adder_user_id}', NOW(), '{$adder_user_id}', 0)
			ON DUPLICATE KEY UPDATE
				edit_time = NOW(),
				editor_admin_user_id = {$adder_user_id},
				is_admin = 0
		");
		$this->update_stat();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function add_admin()
	{
		if ($this->level > ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_ADMIN)
		{
			return true;
		}
		$new_status = ACCESS_STATUS_ADMIN;
		$adder_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			INSERT INTO stream_user_link (stream_id, user_id, add_time, edit_time, status)
			VALUES ('{$this->stream_obj->get_id()}', '{$this->user_id}', NOW(), NOW(), '{$new_status}')
			ON DUPLICATE KEY UPDATE
				status = '{$new_status}',
				edit_time = NOW()
		");
		$this->db->sql("
			INSERT INTO stream_admin (stream_id, user_id, add_time, adder_admin_user_id, edit_time, editor_admin_user_id, is_admin)
			VALUES ('{$this->stream_obj->get_id()}', '{$this->user_id}', NOW(), '{$adder_user_id}', NOW(), '{$adder_user_id}', 1)
			ON DUPLICATE KEY UPDATE
				edit_time = NOW(),
				editor_admin_user_id = {$adder_user_id},
				is_admin = 1
		");
		$this->update_stat();
		$this->send_email_to_pretender(true);
		return true;
	}

	public function delete_member()
	{
		$new_status = ACCESS_STATUS_DELETED;
		$this->db->sql("
			UPDATE stream_user_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				stream_id = '{$this->stream_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_stat();
		$this->send_email_to_pretender(false);
		return true;
	}

	public function delete_moderator()
	{
		if ($this->level < ACCESS_LEVEL_MODERATOR)
		{
			return false;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			UPDATE stream_user_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				stream_id = '{$this->stream_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			UPDATE stream_admin
			SET
				is_admin = 0,
				edit_time = NOW(),
				editor_admin_user_id = {$editor_user_id}
			WHERE
				stream_id = '{$this->stream_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_stat();
		return true;
	}

	public function delete_admin()
	{
		if ($this->level < ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		$new_status = ACCESS_STATUS_MEMBER;
		$this->db->sql("
			UPDATE stream_user_link
			SET
				status = '{$new_status}',
				edit_time = NOW()
			WHERE
				stream_id = '{$this->stream_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		$this->db->sql("
			UPDATE stream_admin
			SET
				is_admin = 0,
				edit_time = NOW(),
				editor_admin_user_id = {$editor_user_id}
			WHERE
				stream_id = '{$this->stream_obj->get_id()}' AND user_id = '{$this->user_id}'
		");
		$this->update_stat();
		return true;
	}

	public function set_moderator_rights($rights_array)
	{
		$editor_user_id = $this->user->get_user_id() ? $this->user->get_user_id() : "NULL";
		foreach ($rights_array as $idx => $val)
		{
			if ($idx == "is_admin")
			{
				trigger_error("Incorrect set_moderator_rights() function usage ('is_admin' check)");
				return false;
			}
			if (!preg_match("/^is_[a-z0-9_]+_moderator$/i", $idx))
			{
				trigger_error("Incorrect set_moderator_rights() function usage (regexp check)");
				return false;
			}
			if ((string) $val !== "0" and (string) $val !== "1")
			{
				trigger_error("Incorrect set_moderator_rights() function usage (value check)");
				return false;
			}
			$rights_array["edit_time"] = "NOW()";
			$rights_array["editor_admin_user_id"] = $editor_user_id;
		}
		if (!$this->is_moderator_strict())
		{
			return false;
		}
		if (sizeof($rights_array))
		{
			$this->db->update_by_array("stream_admin", $rights_array, "stream_id = {$this->stream_obj->get_id()} AND user_id = {$this->user_id}");
		}
		return true;
	}

	private function update_stat()
	{
		stream_calc_helper::update_stream_user_count_calc($this->stream_obj->get_id());
		stream_calc_helper::update_group_user_count_calc($this->stream_obj->get_id());
		user_stream_status_helper::update_is_moderator_anywhere_calc_status($this->user_id);
	}

	public function clean_cache()
	{
		stream_user_cache_tag::init($this->stream_obj->get_id(), $this->user_id)->update();
	}

	private function send_emails_to_admins_about_pretender()
	{
		$admin_emails = $this->db->fetch_column_values("
			SELECT u.id, u.email
			FROM stream_user_link l
			JOIN user u ON u.id = l.user_id
			WHERE
				l.stream_id = {$this->stream_obj->get_id()}
				AND l.status = 'admin'
				AND u.allow_email_on_premoderation = 1
		", "email", "id");

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user_id));
		$xml_loader->add_xml(new stream_current_xml_ctrl($this->stream_obj->get_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xslt("mail/stream_admin_new_member.mail", "stream_admin");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		foreach ($admin_emails as $admin_user_id => $admin_email)
		{
			mail_helper::add_letter(":AUTO:", $body, $admin_email, $admin_user_id);
		}
	}

	private function send_email_to_pretender($is_now_member)
	{
		if (!$this->is_pretender_strict() or $this->user_id == $this->user->get_user_id())
		{
			return;
		}

		$is_now_member = $is_now_member ? "1" : "0";

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user_id));
		$xml_loader->add_xml(new user_short_xml_ctrl($this->user->get_user_id()));
		$xml_loader->add_xml(new stream_current_xml_ctrl($this->stream_obj->get_id()));
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new sys_params_xml_ctrl());
		$xml_loader->add_xml(new string_to_xml_xml_ctrl("<is_now_member>{$is_now_member}</is_now_member>"));
		$xml_loader->add_xslt("mail/stream_pretender.mail", "stream");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		$pretender_user = new user($this->user_id);
		mail_helper::add_letter(":AUTO:", $body, $pretender_user->get_email(), $this->user_id);
	}

}

?>