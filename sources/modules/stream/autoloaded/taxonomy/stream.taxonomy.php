<?php

final class stream_taxonomy extends base_taxonomy
{

	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	/**
	 * @var stream_access
	 */
	protected $stream_access;
	protected $is_direct;

	public function __construct(xml_loader $xml_loader, base_taxonomy $parent_taxonomy, $parts_offset, $stream_id, $is_direct)
	{
		$this->stream_id = $stream_id;
		$this->stream_obj = stream_obj::instance($this->stream_id);
		$this->stream_access = $this->stream_obj->get_access();
		$this->is_direct = $is_direct;
		parent::__construct($xml_loader, $parent_taxonomy, $parts_offset);
	}

	public function run()
	{
		lang_helper::reinit_lang($this->stream_obj->get_param("lang_code"));
		$p = $this->get_parts_relative();

		if ($this->domain_logic->is_on() and ! $this->is_direct)
		{
			$folders = substr($this->request->get_all_folders(), strlen("streams/{$this->stream_obj->get_name()}/"));
			$url = $this->stream_obj->get_url() . $folders . $this->request->get_params_string();
			$this->xml_loader->set_redirect_url($url);
			$this->xml_loader->set_redirect_type_permanent();
			return;
		}

		$this->xml_loader->set_error_404_xslt("stream_404", "stream");
		$this->xml_loader->set_error_403_xslt("stream_403", "stream");

		$this->xml_loader->add_xml(new stream_current_xml_ctrl($this->stream_id));
		$this->xml_loader->add_xml(new stream_access_xml_ctrl($this->stream_access));

		if ($p[1] === "admin")
		{
			$stream_admin_taxonomy = new stream_admin_taxonomy($this->xml_loader, $this, 1, $this->stream_obj);
			$stream_admin_taxonomy->run();
		}
		elseif ($p[1] === null and $page = 1 or $p[1] === "news" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new stream_news_xml_page($this->stream_obj, $page));
		}
		elseif ($p[1] === "status" and $p[2] === null)
		{
			if ($this->user->get_user_id())
			{
				$this->xml_loader->add_xml_with_xslt(new stream_status_xml_page());
			}
			else
			{
				$this->xml_loader->set_error_403();
			}
		}
		elseif ($p[1] === "positions" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new stream_positions_xml_page($this->stream_obj));
		}
		elseif ($p[1] === "positions-map" and $p[2] === null and $this->xerror->is_debug_mode())
		{
			$this->xml_loader->add_xml_with_xslt(new stream_positions_map_xml_page($this->stream_obj));
		}
		elseif ($p[1] === "activities")
		{
			if (($params = activities_taxonomy_helper::parse_request_params($p, 2, activities_taxonomy_helper::MOD_MY)))
			{
				$this->xml_loader->add_xml(new activities_xml_page($params, $this->stream_id));
				$this->xml_loader->add_xslt("stream_activities", "stream");
			}
			elseif ($p[2] === "calendar" and $p[3] === null)
			{
				//activities/calendar/
				$this->set_redirect_url("{$p[1]}/");
			}
			elseif (is_good_id($activity_id = $p[2]) and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_activity_xml_page($this->stream_obj, $activity_id));
			}
		}
		elseif ($p[1] === "materials")
		{
			if ($p[2] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_study_materials_xml_page($this->stream_obj, $this->user->get_user_id() ? study_materials_helper::FILTER_RECOMMENDED : study_materials_helper::FILTER_ALL));
			}
			elseif (!$this->user->get_user_id() and ( $p[2] === study_materials_helper::FILTER_STUDIED or $p[2] === study_materials_helper::FILTER_ALL) and $p[3] == null)
			{
				$this->set_redirect_url("materials/");
			}
			elseif ($this->user->get_user_id() and $p[2] === study_materials_helper::FILTER_STUDIED and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_study_materials_xml_page($this->stream_obj, study_materials_helper::FILTER_STUDIED));
			}
			elseif ($this->user->get_user_id() and $p[2] === study_materials_helper::FILTER_ALL and $p[3] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_study_materials_xml_page($this->stream_obj, study_materials_helper::FILTER_ALL));
			}
			elseif (is_good_id($study_material_id = $p[2]) and $p[3] == null)
			{
				$this->xml_loader->add_xml(new study_material_full_xml_ctrl($study_material_id, $this->stream_id));
				if ($p[3] === null)
				{
					$this->xml_loader->add_xml_with_xslt(new stream_study_material_xml_page($this->stream_obj, $study_material_id));
				}
			}
		}
		elseif ($p[1] === "users" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new stream_users_xml_page($this->stream_obj, $page));
		}
		elseif ($p[1] === "users" and ( $user_id = $this->get_user_id_by_login($p[2])))
		{
			$url = $this->domain_logic->get_users_prefix() . "/{$p[2]}/";
			$this->xml_loader->set_redirect_url($url);
		}
		elseif ($p[1] === "ratings")
		{
			if ($p[2] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_ratings_positions_xml_page($this->stream_obj));
			}
			elseif (is_good_id($p[2]) and $page = $this->is_page_folder($p[3]) and $p[4] === null)
			{
				$this->xml_loader->add_xml_with_xslt(new stream_rating_xml_page());
				$this->xml_loader->add_xml(new position_full_xml_ctrl($p[2], $this->stream_id));
				$this->xml_loader->add_xml(new math_rating_xml_ctrl($p[2], $this->stream_obj, $page));
			}
		}
		elseif (($file_id = $this->is_type_folder($p[1], "file-a")) and $p[2] === null)
		{
			$this->xml_loader->add_xml(new stream_activity_file_download_xml_page($this->stream_obj, $file_id));
		}
		elseif (($file_id = $this->is_type_folder($p[1], "file-n")) and $p[2] === null)
		{
			$this->xml_loader->add_xml(new stream_news_item_file_download_xml_page($this->stream_obj, $file_id));
		}
		elseif (($file_id = $this->is_type_folder($p[1], "file-mt")) and $p[2] === null)
		{
			$this->xml_loader->add_xml(new stream_study_material_file_download_xml_page($this->stream_obj, $file_id));
		}
	}

	private function get_user_id_by_login($login)
	{
		return url_helper::get_user_id_by_login($login);
	}

}

?>