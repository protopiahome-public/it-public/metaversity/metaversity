<?php

class stream_study_materials_cache_tag extends base_cache_tag
{

	public static function init($stream_id)
	{
		return parent::get_tag(__CLASS__, $stream_id);
	}

}

?>