<?php

class stream_study_material_status_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"study_material_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $study_material_id;
	protected $study_material_row;
	protected $user_id;
	protected $new_status;

	public function start()
	{
		$this->user_id = $this->user->get_user_id();
		$this->new_status = POST("new_status");
		if (!study_material_status_helper::is_good_status($this->new_status))
		{
			return false;
		}
		return true;
	}

	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function commit()
	{
		study_material_status_helper::change_status($this->study_material_id, $this->user_id, $this->new_status);
		return true;
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"new_status" => $this->new_status,
		);
	}

}

?>