<?php

class stream_position_get_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"position_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $position_id;
	protected $position_row;

	public function get_data()
	{
		require_once PATH_CORE . "/loader/xml_loader.php";
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_xml_ctrl());
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new stream_short_xml_ctrl($this->stream_id));
		$xml_loader->add_xml(new position_full_xml_ctrl($this->position_id, $this->stream_id));
		if ($this->user->get_user_id())
		{
			$xml_loader->add_xml(new stream_user_short_xml_ctrl($this->user->get_user_id(), $this->stream_id));
		}
		$xml_loader->add_xml(new math_position_details_xml_ctrl($this->stream_obj, $this->position_id, $this->user->get_user_id() ? : null));
		$xml_loader->add_xslt("ajax/stream_position_get.ajax", "stream");
		$xml_loader->run();
		return array(
			"status" => "OK",
			"position_id" => $this->position_id,
			"html" => $xml_loader->get_content(),
		);
	}

}

?>