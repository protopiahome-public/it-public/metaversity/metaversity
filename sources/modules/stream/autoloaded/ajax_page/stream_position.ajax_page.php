<?php

class stream_position_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"position_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $position_id;
	protected $position_row;
	protected $user_id;
	protected $checked;

	public function start()
	{
		$this->user_id = $this->user->get_user_id();
		$this->checked = POST("checked") === "1";
		return true;
	}

	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function commit()
	{
		position_helper::change_position($this->stream_obj, $this->user_id, $this->position_id, $this->checked);
		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

}

?>