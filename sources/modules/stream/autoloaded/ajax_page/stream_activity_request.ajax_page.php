<?php

class stream_activity_request_ajax_page extends base_ajax_ctrl
{

	protected $mixins = array(
		"stream_before_start",
		"activity_before_start",
		"role_before_start",
	);
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $activity_row;
	protected $role_id;
	protected $role_row;
	protected $user_id;
	protected $action;
	protected $next_status;

	/**
	 * @var stream_access_save
	 */
	protected $stream_access_save = null;

	public function start()
	{
		$this->user_id = $this->user->get_user_id();
		$this->action = POST("action");
		if (!activity_participants_front_helper::is_good_participant_action($this->action))
		{
			return false;
		}
		$this->next_status = activity_participants_front_helper::participant_action_allowed($this->activity_row, $this->role_id, $this->action);
		return true;
	}

	public function check_rights()
	{
		return !!$this->next_status;
	}

	public function commit()
	{
		if ($this->next_status !== true)
		{
			activity_participants_helper::change_participant_status($this->activity_id, $this->role_id, $this->user_id, $this->next_status, true);
			if ($this->next_status == activity_participants_helper::PARTCIPANT_STATUS_PREMODERATION)
			{
				$this->store_email();
			}
			if ($this->next_status != activity_participants_helper::PARTCIPANT_STATUS_DELETED)
			{
				$this->stream_access_save = new stream_access_save($this->stream_obj, $this->user->get_user_id());
				$this->stream_access_save->join();
			}
		}
		return true;
	}

	public function clean_cache()
	{
		if ($this->stream_access_save)
		{
			$this->stream_access_save->clean_cache();
		}
	}

	private function store_email()
	{
		require_once PATH_CORE . "/loader/xml_loader.php";

		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new stream_current_xml_ctrl($this->stream_id));
		$xml_loader->add_xml(new stream_user_short_xml_ctrl($this->user_id, $this->stream_id));
		$xml_loader->add_xml(new activity_full_xml_ctrl($this->activity_id, $this->stream_id));
		$xml_loader->add_xml(new role_short_xml_ctrl($this->role_id, $this->stream_id));
		$xml_loader->add_xslt("mail/stream_admin_activity_request.mail", "stream_admin");
		$xml_loader->run();
		$body = $xml_loader->get_content();

		$body_escaped = $this->db->escape($body);
		$this->db->sql("
			UPDATE activity_participant
			SET email_to_send = '{$body_escaped}'
			WHERE activity_id = {$this->activity_id}
				AND role_id = {$this->role_id}
				AND user_id = {$this->user_id}
		");
	}

	public function get_data()
	{
		return array(
			"status" => "OK",
			"new_status" => $this->next_status,
		);
	}

}

?>