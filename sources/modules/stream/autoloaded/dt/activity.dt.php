<?php

class activity_dt extends base_dt
{

	/**
	 * @var lang
	 */
	protected $lang;

	/**
	 * @var user
	 */
	protected $user;

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * foreign_key_dtf
	 */
	protected $metaactivity_id_dtf;

	/**
	 * @var foreign_key_dtf 
	 */
	protected $city_id_dtf;

	/**
	 * @var foreign_key_dtf 
	 */
	protected $study_level_id_dtf;

	/**
	 * @var multi_link_dtf 
	 */
	protected $groups_dtf;

	/**
	 * @var multi_link_dtf 
	 */
	protected $crosspost_dtf;

	/**
	 * @var foreign_key_dtf
	 */
	protected $format_id_dtf;

	/**
	 * @var multi_link_dtf 
	 */
	protected $subjects_dtf;

	/**
	 * @var intmarkup_dtf 
	 */
	protected $descr_dtf;

	/**
	 * @var intmarkup_dtf 
	 */
	protected $moderators_dtf;

	public function __construct()
	{
		global $lang;
		$this->lang = $lang;

		global $user;
		$this->user = $user;

		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new foreign_key_dtf("metaactivity_id", trans("Activity group"), "metaactivity");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(false);
		$this->add_field($dtf, "main");
		$this->metaactivity_id_dtf = $dtf;

		$dtf = new foreign_key_dtf("city_id", trans("City"), "city");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(false);
		$dtf->set_unselected_title(trans("For all cities"));
		$this->add_field($dtf, "main");
		$this->city_id_dtf = $dtf;

		$dtf = new foreign_key_dtf("study_level_id", trans("Study level"), "study_level");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(false);
		$dtf->set_unselected_title(trans("For all levels"));
		$this->add_field($dtf, "main");
		$this->study_level_id_dtf = $dtf;

		$dtf = new multi_link_dtf("groups", trans("Groups"), "group", "activity_group_link", "activity_id", "group_id", "title");
		$dtf->set_restrict_sql("NOT INITIALIZED");
		$dtf->set_max_height("150");
		$dtf->set_comment(trans("Nothing is checked = Show for all groups."));
		$this->add_field($dtf, "main");
		$this->groups_dtf = $dtf;

		$dtf = new multi_link_dtf("crosspost", trans("Crosspost into streams"), "stream", "activity_stream_crosspost", "activity_id", "stream_id", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "", "id", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title");
		$dtf->set_restrict_sql("NOT INITIALIZED");
		$dtf->set_max_height("150");
		$dtf->set_comment(trans("The activity will be published for all groups of the selected streams."));
		$this->add_field($dtf, "main");
		$this->crosspost_dtf = $dtf;

		$dtf = new foreign_key_dtf("format_id", trans("Format"), "format");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");
		$this->format_id_dtf = $dtf;

		$dtf = new select_dtf("weight", trans("Weight"));
		$dtf->set_cases(array(
			"low" => trans("1.0 (low, 1-day events or shorter)"),
			"hi" => trans("2.0 (high, 2–3 days+)"),
		));
		$dtf->set_drop_down_view(true);
		$dtf->set_default_key("low");
		$this->add_field($dtf, "main");

		$dtf = new multi_link_dtf("subjects", trans("Subjects"), "subject", "activity_subject_link", "activity_id", "subject_id", "title");
		$dtf->set_restrict_sql("NOT INITIALIZED");
		$dtf->set_max_height("150");
		$this->add_field($dtf, "main");
		$this->subjects_dtf = $dtf;

		$dtf = new select_dtf("date_type", trans("Type"));
		$dtf->set_cases(array(
			"event" => trans("Event", "DATE_TYPE_LONG"),
			"deadline" => trans("Deadline (for assignments, etc.)", "DATE_TYPE_LONG"),
			"longterm" => trans("Long-term grading", "DATE_TYPE_LONG"),
		));
		$dtf->set_drop_down_view(true);
		$dtf->set_default_key("event");
		$this->add_field($dtf, "main");

		$dtf = new datetime_dtf("start_time", trans("Start time"));
		$dtf->set_importance(false);
		$dtf->set_edit_mode_years_count_for_select(3, 7);
		$dtf->set_format("Y-m-d H:i:s N");
		$this->add_field($dtf, "main");

		$dtf = new datetime_dtf("finish_time", trans("Finish time"));
		$dtf->set_importance(false);
		$dtf->set_edit_mode_years_count_for_select(3, 7);
		$dtf->set_format("Y-m-d H:i:s N");
		$dtf->set_must_be_later("start_time", false, true);
		$dtf->add_dependency("date_type", "event", INTCMF_DTF_DEP_ACTION_SHOW);
		$dtf->add_dependency("date_type", "longterm", INTCMF_DTF_DEP_ACTION_SHOW);
		$dtf->add_dependency("date_type", "deadline", INTCMF_DTF_DEP_ACTION_DO_NOT_STORE);
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("place", trans("Place"));
		$dtf->set_max_length(255);
		$this->add_field($dtf, "main");

		$dtf = new intmarkup_dtf("descr", trans("Description"));
		$dtf->set_editor_height(200);
		$this->add_field($dtf, "main");
		$this->descr_dtf = $dtf;

		$dtf = new intmarkup_dtf("moderators", trans("Text for moderators"));
		$dtf->set_editor_height(200);
		$this->add_field($dtf, "main");
		$this->moderators_dtf = $dtf;

		$dtf = new int_dtf("comment_count_calc", trans("Number of comments"));
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$this->add_field($dtf);

		$this->add_fields_in_axis("short", array(
			"title", "metaactivity_id", "city_id", "format_id", "weight",
			"date_type", "start_time", "finish_time",
		));

		$this->add_fields_in_axis("full", array(
			"title", "metaactivity_id", "city_id", "study_level_id", "groups", "format_id", "weight", "subjects",
			"date_type", "start_time", "finish_time",
			"place", "descr",
			"moderators",
		));

		$this->add_fields_in_axis("edit", array(
			"title", "metaactivity_id", "city_id", "study_level_id", "groups", "crosspost", "format_id", "weight", "subjects",
			"date_type", "start_time", "finish_time",
			"place", "descr",
		));

		$this->add_fields_in_axis("edit_without_format", array(
			"title", "metaactivity_id", "city_id", "study_level_id", "groups", "crosspost", "weight", "subjects",
			"date_type", "start_time", "finish_time",
			"place", "descr",
		));

		$this->add_fields_in_axis("edit_moderators", array(
			"moderators",
		));
	}

	public function set_stream_obj(stream_obj $stream_obj)
	{
		$this->metaactivity_id_dtf->set_restrict_items_sql("
			SELECT id, title
			FROM metaactivity
			WHERE stream_id = {$stream_obj->get_id()}
			ORDER BY title
		", "
			SELECT id, title
			FROM metaactivity
			WHERE stream_id = {$stream_obj->get_id()} AND id = %s
		");
		$this->city_id_dtf->set_restrict_items_sql("
			SELECT c.id, c.title
			FROM stream_city_link l
			JOIN city c ON c.id = l.city_id
			WHERE l.stream_id = {$stream_obj->get_id()}
			ORDER BY c.title
		", "
			SELECT c.id, c.title
			FROM stream_city_link l
			JOIN city c ON c.id = l.city_id
			WHERE l.stream_id = {$stream_obj->get_id()} AND l.city_id = %s
		");
		$this->city_id_dtf->set_default_value($stream_obj->get_current_city_id());
		$this->study_level_id_dtf->set_restrict_items_sql("
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$stream_obj->get_id()}
			ORDER BY title
		", "
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$stream_obj->get_id()} AND id = %s
		");
		$this->groups_dtf->set_restrict_sql("WHERE stream_id = {$stream_obj->get_id()}");
		$this->crosspost_dtf->set_restrict_sql("WHERE id != {$stream_obj->get_id()}");
		$this->format_id_dtf->set_restrict_items_sql("
			SELECT id, format_group_id AS group_id, title
			FROM format
			WHERE stream_id = {$stream_obj->get_id()}
			ORDER BY position
		", "
			SELECT id, title
			FROM format
			WHERE stream_id = {$stream_obj->get_id()} AND id = %s
		");
		$this->format_id_dtf->set_groups_data("format_group", "format_group_id", "id", "title", "position", "parent_id", "stream_id = {$stream_obj->get_id()}");
		$this->subjects_dtf->set_restrict_sql("WHERE stream_id = {$stream_obj->get_id()}");
		$this->descr_dtf->setup_upload("activity", $stream_obj->get_url(true) . "file-a-");
		$this->moderators_dtf->setup_upload("activity_moderators", $stream_obj->get_url(true) . "admin/file-am-");
	}

	public function disable_archived_formats()
	{
		$this->format_id_dtf->set_where("dt.is_archived = 0");
	}

}

?>