<?php

class group_dt extends base_dt
{

	/**
	 * @var lang
	 */
	protected $lang;

	/**
	 * @var foreign_key_dtf 
	 */
	protected $city_id_dtf;

	/**
	 * @var foreign_key_dtf 
	 */
	protected $study_level_id_dtf;

	public function __construct()
	{
		global $lang;
		$this->lang = $lang;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(30);
		$this->add_field($dtf, "main");

		$dtf = new foreign_key_dtf("city_id", trans("City"), "city");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");
		$this->city_id_dtf = $dtf;

		$dtf = new foreign_key_dtf("study_level_id", trans("Study level"), "study_level");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(false);
		$this->add_field($dtf, "main");
		$this->study_level_id_dtf = $dtf;

		$this->add_fields_in_axis("full", array("title", "city_id", "study_level_id"));
		$this->add_fields_in_axis("edit", array("title", "city_id", "study_level_id"));
	}

	public function set_stream_id($stream_id)
	{
		$this->study_level_id_dtf->set_restrict_items_sql("
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$stream_id}
			ORDER BY title
		", "
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$stream_id} AND id = %s
		");
		$fetch_city_sql = $this->lang->get_current_lang_code() === "en" ? "c.title_en AS title" : "c.title";
		$order_sql = $this->lang->get_current_lang_code() === "en" ? "c.title_en" : "c.title";
		$this->city_id_dtf->set_restrict_items_sql("
			SELECT c.id, {$fetch_city_sql}
			FROM stream_city_link l
			JOIN city c ON c.id = l.city_id
			WHERE l.stream_id = {$stream_id}
			ORDER BY {$order_sql}
		", "
			SELECT c.id, c.title
			FROM stream_city_link l
			JOIN city c ON c.id = l.city_id
			WHERE l.stream_id = {$stream_id} AND l.city_id = %s
		");
	}

}

?>