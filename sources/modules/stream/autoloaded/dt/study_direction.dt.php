<?php

class study_direction_dt extends base_dt
{

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(30);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("full", array("title"));
		$this->add_fields_in_axis("edit", array("title"));
	}

}

?>