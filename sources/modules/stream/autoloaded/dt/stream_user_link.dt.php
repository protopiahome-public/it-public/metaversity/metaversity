<?php

class stream_user_link_dt extends base_dt
{

	protected $pk_column = array("stream_id", "user_id");

	/**
	 * @var foreign_key_dtf 
	 */
	protected $group_id_dtf;

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new foreign_key_dtf("group_id", trans("Group"), "group");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(false);
		$this->add_field($dtf, "main");
		$this->group_id_dtf = $dtf;

		$this->add_fields_in_axis("edit", array("group_id"));
	}

	public function set_stream_id($stream_id)
	{
		$this->group_id_dtf->set_restrict_items_sql("
			SELECT id, title
			FROM `group`
			WHERE stream_id = {$stream_id}
			ORDER BY title
		", "
			SELECT id, title
			FROM `group`
			WHERE stream_id = {$stream_id} AND id = %s
		");
	}

}

?>