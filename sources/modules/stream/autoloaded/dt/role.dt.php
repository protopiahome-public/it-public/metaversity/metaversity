<?php

class role_dt extends base_dt
{

	/**
	 * @var multi_link_dtf 
	 */
	protected $study_level_dtf;

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("auto_accept", trans("Accept all requests without premoderation"));
		$dtf->set_default_value(false);
		$dtf->set_comment(trans("You will be able to change it in activitiy settings.", "ROLE_AUTO_ACCEPT"));
		$this->add_field($dtf, "main");

		$dtf = new int_dtf("total_number", trans("Recommended number of vacancies"), INTCMF_INT_DTF_TYPE_BYTE);
		$dtf->set_importance(true);
		$dtf->set_default_value(0);
		$dtf->set_comment(trans("Zero = unlimited. You will be able to change it in activitiy settings."));
		$this->add_field($dtf, "main");

		$dtf = new intmarkup_dtf("descr", trans("Description"));
		$dtf->set_editor_height(200);
		$dtf->set_comment(trans("You will be able to change it in activitiy settings.", "ROLE_DESCRIPTION"));
		$this->add_field($dtf, "main");

		$dtf = new multi_link_dtf("study_level", trans("Study levels"), "study_level", "role_study_level_link", "role_id", "study_level_id", "title");
		$dtf->set_restrict_sql("NOT INITIALIZED");
		$dtf->set_comment(trans("Nothing is checked = Every level is checked. The tree is always automatically expanded to show actual competences. Non-actual competences become gray."));
		$this->add_field($dtf, "main");
		$this->study_level_dtf = $dtf;

		$this->add_fields_in_axis("full", array("title", "total_number", "auto_accept", "descr"));
		$this->add_fields_in_axis("short", array("title", "descr"));
		$this->add_fields_in_axis("edit", array("title", "total_number", "auto_accept", "descr", "study_level"));
	}

	public function set_stream_id($stream_id)
	{
		$this->study_level_dtf->set_restrict_sql("WHERE stream_id = {$stream_id}");
	}

}

?>