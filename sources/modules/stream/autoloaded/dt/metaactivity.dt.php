<?php

class metaactivity_dt extends base_dt
{
	/**
	 * @var user
	 */
	protected $user;

	public function __construct()
	{
		global $user;
		$this->user = $user;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("edit", array(
			"title",
		));

		$this->add_fields_in_axis("short", array(
			"title",
		));

		$this->add_fields_in_axis("show", array(
			"title",
		));
	}

}

?>