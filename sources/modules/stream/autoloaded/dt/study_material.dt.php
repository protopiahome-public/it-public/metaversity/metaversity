<?php

class study_material_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * @var multi_link_dtf
	 */
	protected $subjects_dtf;

	/**
	 * @var intmarkup_dtf 
	 */
	protected $descr_dtf;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new intmarkup_dtf("descr", trans("Description"));
		$dtf->set_editor_height(300);
		$this->add_field($dtf, "main");
		$this->descr_dtf = $dtf;

		$dtf = new multi_link_dtf("subjects", trans("Subjects"), "subject", "study_material_subject_link", "study_material_id", "subject_id", "title");
		$dtf->set_restrict_sql("NOT INITIALIZED");
		$dtf->set_max_height("200");
		$this->add_field($dtf, "main");
		$this->subjects_dtf = $dtf;

		$dtf = new bool_dtf("is_outer", trans("External material"));
		$this->add_field($dtf, "outer");

		$dtf = new url_string_dtf("outer_url", trans("External material link"));
		$dtf->set_importance(true);
		$dtf->set_comment(trans("Start with http:// or https://"));
		$this->add_field($dtf, "outer");

		$this->add_fields_in_axis("full", array("title", "descr", "subjects", "is_outer", "outer_url"));
		$this->add_fields_in_axis("edit", array("title", "descr", "subjects"));
		$this->add_fields_in_axis("edit_outer", array("title", "outer_url", "subjects"));
	}

	public function set_stream_obj(stream_obj $stream_obj)
	{
		$this->subjects_dtf->set_restrict_sql("WHERE stream_id = {$stream_obj->get_id()}");
		$this->descr_dtf->setup_upload("material", $stream_obj->get_url(true) . "file-mt-");
	}

}

?>