<?php

class position_dt extends base_dt
{
	/**
	 * @var domain_logic
	 */
	protected $domain_logic;
	/**
	 * @var multi_link_dtf 
	 */
	protected $study_level_dtf;

	/**
	 * @var multi_link_dtf 
	 */
	protected $study_direction_dtf;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new intmarkup_dtf("descr", trans("Description"));
		$dtf->set_editor_height(180);
		$this->add_field($dtf, "main");

		$dtf = new multi_link_dtf("study_level", trans("Study levels"), "study_level", "position_study_level_link", "position_id", "study_level_id", "title");
		$dtf->set_restrict_sql("NOT INITIALIZED");
		$dtf->set_comment(trans("Nothing is checked = Every level is checked. The tree is always automatically expanded to show actual competences. Non-actual competences become gray."));
		$this->add_field($dtf, "main");
		$this->study_level_dtf = $dtf;
		
		$dtf = new multi_link_dtf("study_direction", trans("Study directions"), "study_direction", "position_study_direction_link", "position_id", "study_direction_id", "title");
		$this->add_field($dtf, "main");
		$this->study_direction_dtf = $dtf;
		
		$this->add_fields_in_axis("full", array("title", "descr", "study_level", "study_direction"));
		$this->add_fields_in_axis("edit", array("title", "descr", "study_level", "study_direction"));
	}
	
	public function set_stream_id($stream_id)
	{
		$this->study_level_dtf->set_restrict_sql("WHERE stream_id = {$stream_id}");
		$this->study_direction_dtf->set_restrict_sql("WHERE stream_id = {$stream_id}");
	}

}

?>