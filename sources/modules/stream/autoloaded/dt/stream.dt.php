<?php

class stream_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;
	
	/**
	 * @var lang
	 */
	protected $lang;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;
		
		global $lang;
		$this->lang = $lang;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Full title (usually including a word “Stream”)"));
		$dtf->set_comment(trans("Example: “Stream of Happiness”."));
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$dtf->set_unique(true, trans("This stream has been already added."));
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("title_en", trans("Full title (English)"));
		$dtf->set_comment("Start Words with Capital Letters Please (it's not Russian!)");
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$dtf->set_unique(true, trans("This stream has been already added."));
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("title_short", trans("Short title, up to 10 characters"));
		$dtf->set_comment(trans("Example: “SH”."));
		$dtf->set_max_length(10);
		$dtf->set_importance(true);
		$dtf->set_unique(true, trans("This stream has been already added."));
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("title_short_en", trans("Short title (English)"));
		$dtf->set_max_length(10);
		$dtf->set_importance(true);
		$dtf->set_unique(true, trans("This stream has been already added."));
		$this->add_field($dtf, "main");

		$dtf = new foreign_key_dtf("lang_code", trans("Content language"), "lang", "title", "code");
		$dtf->set_importance(true);
		$dtf->set_comment(trans("It's also a UI language for search engines."));
		$this->add_field($dtf, "main");

		$dtf = new url_name_dtf("name", trans("URL name"));
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_url_prefix($this->domain_logic->get_streams_prefix(true) . "/");
		$dtf->set_deprecated_values(array(
			"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
			"members", "users", "admins", "moderators", "settings", "search", "company", "join", "students", "tests", "streams", // std. URLs
			"roles", "id" // URLs
		));
		$dtf->set_comment(trans("Allowed: english letters, digits and hyphen. It is not recommended to change it."));
		$this->add_field($dtf, "main");

		$dtf = new int_dtf("position_credit_min_match", trans("Minimal match for crediting positions based on match (0–100)"));
		$dtf->set_importance(true);
		$dtf->set_default_value(0);
		$dtf->set_type(INTCMF_INT_DTF_TYPE_BYTE);
		$dtf->set_min_value(0);
		$dtf->set_max_value(100);
		$dtf->set_comment(trans("Zero disables this feature."));
		$this->add_field($dtf, "main");

		$dtf = new multi_link_dtf("cities", trans("Cities where this stream works"), "city", "stream_city_link", "stream_id", "city_id", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "", "id", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title");
		$dtf->set_design(INTCMF_MULTI_LINK_DTF_DESIGN_CHOSEN);
		$dtf->set_main_table_count_column("city_count_calc");
		$dtf->set_max_height("200");
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("join_premoderation", trans("Premoderate students"));
		$dtf->set_default_value(false);
		$this->add_field($dtf, "main");
		
		$dtf = new bool_dtf("users_import_allowed", trans("Allow importing users by email"));
		$dtf->set_default_value(false);
		$dtf->set_comment(trans("A special 'User import' page will be available for the stream's administrators."));
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("list_show", trans("Show on the Metaversity streams page"));
		$dtf->set_default_value(false);
		$dtf->set_comment(trans("System administrators will also be able to add students into these streams using user administration interface."));
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("list_descr", trans("Description for the streams page"));
		$dtf->set_comment(trans("Up to 600 characters."));
		$dtf->set_max_length(600);
		$dtf->set_importance(false);
		$this->add_field($dtf, "main");
		
		$this->add_fields_in_axis("short", array(
			"title", "title_short", "lang_code", "join_premoderation",
		));
		$this->add_fields_in_axis("edit_admin", array(
			"title", "title_en", "title_short", "title_short_en", "lang_code", "name", "join_premoderation", "users_import_allowed", "list_show", "list_descr",
		));
		$this->add_fields_in_axis("edit_general", array(
			"title", "title_en", "title_short", "title_short_en", "position_credit_min_match", "cities",
		));
	}

}

?>