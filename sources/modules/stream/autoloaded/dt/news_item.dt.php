<?php

class news_item_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * @var lang
	 */
	protected $lang;

	/**
	 * @var foreign_key_dtf 
	 */
	protected $city_id_dtf;

	/**
	 * @var intmarkup_dtf 
	 */
	protected $descr_dtf;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		global $lang;
		$this->lang = $lang;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new foreign_key_dtf("city_id", trans("City"), "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title");
		$dtf->set_restrict_items_sql("NOT INITIALIZED", "NOT INITIALIZED");
		$dtf->set_importance(false);
		$dtf->set_unselected_title(trans("For all cities"));
		$this->add_field($dtf, "main");
		$this->city_id_dtf = $dtf;

		$dtf = new intmarkup_dtf("announce", trans("Announce"));
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "main");

		$dtf = new intmarkup_dtf("descr", trans("News text"));
		$dtf->set_editor_height(200);
		$this->add_field($dtf, "main");
		$this->descr_dtf = $dtf;

		$dtf = new foreign_key_dtf("adder_user_id", trans("Author"), "user");
		$dtf->set_importance(false);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("edit", array(
			"title", "city_id", "announce", "descr",
		));

		$this->add_fields_in_axis("short", array(
			"title", "city_id",
		));

		$this->add_fields_in_axis("show", array(
			"title", "city_id", "announce", "descr",
		));

		$this->add_fields_in_axis("full", array(
			"title", "city_id", "announce", "descr",
		));
	}

	public function set_stream_obj(stream_obj $stream_obj)
	{
		$title_field = $this->lang->get_current_lang_code() === "en" ? "c.title_en AS title" : "c.title";
		$order_field = $this->lang->get_current_lang_code() === "en" ? "c.title_en" : "c.title";
		$this->city_id_dtf->set_restrict_items_sql("
			SELECT c.id, {$title_field}
			FROM stream_city_link l
			JOIN city c ON c.id = l.city_id
			WHERE l.stream_id = {$stream_obj->get_id()}
			ORDER BY {$order_field}
		", "
			SELECT c.id, {$title_field}
			FROM stream_city_link l
			JOIN city c ON c.id = l.city_id
			WHERE l.stream_id = {$stream_obj->get_id()} AND l.city_id = %s
		");
		$this->city_id_dtf->set_default_value($stream_obj->get_current_city_id());
		$this->descr_dtf->setup_upload("news_item", $stream_obj->get_url(true) . "file-n-");
	}

}

?>