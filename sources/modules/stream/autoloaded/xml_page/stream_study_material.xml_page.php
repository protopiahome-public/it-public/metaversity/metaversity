<?php

class stream_study_material_xml_page extends base_xml_ctrl
{

	// Internal
	protected $rate_options;
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $study_material_id;
	protected $required_results = array();

	public function __construct(stream_obj $stream_obj, $study_material_id)
	{
		$this->rate_options = array(
			0 => "—",
			1 => trans("Improves indirectly", "COMPETENCE_IMPORTANCE"),
			2 => trans("Improves", "COMPETENCE_IMPORTANCE"),
		);
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->study_material_id = $study_material_id;
		parent::__construct();
	}

	public function get_xml()
	{
		$study_material_row = study_material_helper::get_row($this->study_material_id);
		$rate_id = $study_material_row["rate_id"];
		
		$xdom = xdom::create($this->name);
		$xdom->set_attr("study_material_id", $this->study_material_id);
		$xdom->set_attr("rate_id", $rate_id);
		$this->fill_users($xdom->create_child_node("read_users"));

		$potential_details = null;
		if (($current_user_id = $this->user->get_user_id()))
		{
			$this->required_results = stream_user_positions_helper::get_stream_user_required_competence_results($this->stream_id, $current_user_id, $this->stream_obj->get_competence_set_id());
			list($potential, $potential_details) = potential_math_study_material_helper::get_stream_study_material_potential($this->stream_obj, $this->study_material_id, $this->required_results, true, true);
			$xdom->set_attr("potential", $potential);
		}

		$study_material_row = study_material_helper::get_row($this->study_material_id);
		$this->xml_loader->add_xml(new rate_xml_ctrl($rate_id, $this->stream_obj->get_competence_set_id(), $this->rate_options, null, $this->required_results, $potential_details));

		return $xdom->get_xml(true);
	}

	protected function fill_users(xnode $parent_node)
	{
		$data = study_material_helper::fetch_users($this->study_material_id, study_material_helper::STATUS_READ);
		foreach ($data as $row)
		{
			$this->xml_loader->add_xml_by_class_name("stream_user_short_xml_ctrl", $row["id"], $this->stream_id);
		}
		$this->db_xml_converter->build_xml($parent_node, $data, false, "user");
	}

}

?>