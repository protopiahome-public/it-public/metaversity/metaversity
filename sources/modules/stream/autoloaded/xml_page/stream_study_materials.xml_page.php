<?php

class stream_study_materials_xml_page extends base_easy_xml_ctrl
{
	// Settings
	protected $xml_attrs = array("filter", "positions_checked");
	protected $xml_row_name = "study_material";
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $filter;
	protected $positions_checked = false;

	public function __construct(stream_obj $stream_obj, $filter)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->filter = $filter;
		parent::__construct();
	}

	public function init()
	{
		$processor = new sql_filters_easy_processor();

		$processor->add_sql_filter(new multi_link_sql_filter("subject", trans("Subject"), "id", "study_material_subject_link", "study_material_id", "subject_id", "subject", "title", "id", "stream_id = {$this->stream_id}"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("sm.id, sm.title, sm.read_user_count_calc, sm.is_outer, sm.outer_url");
		$select_sql->add_from("study_material sm");
		$select_sql->add_where("sm.stream_id = {$this->stream_id}");
		$select_sql->add_order("read_user_count_calc DESC");

		$current_user_id = $this->user->get_user_id();
		if ($current_user_id)
		{
			$select_sql->add_join("LEFT JOIN study_material_user_link l ON l.study_material_id = sm.id AND l.user_id = {$current_user_id}");
			$status_default = study_material_helper::STATUS_DEFAULT;
			$select_sql->add_select_fields("IFNULL(l.status, '{$status_default}') AS status");
			if ($this->filter != study_materials_helper::FILTER_ALL)
			{
				$status_read = study_material_helper::STATUS_READ;
				$status_skip = study_material_helper::STATUS_SKIP;
				$select_sql->add_where("l.status IS NULL OR l.status <> '{$status_skip}'");
				if ($this->filter == study_materials_helper::FILTER_STUDIED)
				{
					$select_sql->add_where("l.status = '{$status_read}'");
				}
				elseif ($this->filter == study_materials_helper::FILTER_RECOMMENDED)
				{
					$select_sql->add_where("l.status IS NULL OR l.status <> '{$status_read}'");
				}
			}
		}

		$select_sql->add_order("sm.id DESC");
		$this->data = $this->db->fetch_all($select_sql->get_sql());

		if ($current_user_id)
		{
			$required_results = stream_user_positions_helper::get_stream_user_required_competence_results($this->stream_id, $current_user_id, $this->stream_obj->get_competence_set_id());
			potential_math_study_material_helper::fill_stream_study_materials_potential($this->stream_obj, $this->data, $required_results, false);
			$this->positions_checked = position_helper::positions_checked($this->stream_obj, $current_user_id);
			if ($this->positions_checked)
			{
				$this->data = array_sort($this->data, "potential", SORT_DESC);
				foreach ($this->data as &$row)
				{
					$row["potential"] = potential_math_common_helper::potential_number_format($row["potential"]);
				}
				unset($row);
			}
		}
	}

}

?>