<?php

require_once PATH_INTCMF . "/array_manipulator.php";

class stream_news_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "news_item";
	protected $xml_attrs = array("stream_id");
	protected $dependencies_settings = array();
	// Internal
	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $stream_id;
	protected $page;

	/**
	 * @var foreign_key_sql_filter
	 */
	protected $city_sql_filter;

	/**
	 * @var select_sql_filter
	 */
	protected $news_type_sql_filter;

	public function __construct(stream_obj $stream_obj, $page)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_full_fetch_easy_processor($this->page, 25));

		$processor = new sql_filters_easy_processor();

		if ($this->user->get_user_id())
		{
			$options = array(
				news_helper::TYPE_STREAM_TEXT => trans("Stream's news"),
				news_helper::TYPE_ACTIVITY_STATUS => trans("Status changes"),
				news_helper::TYPE_MARK => trans("New marks"),
				news_helper::TYPE_CREDIT => trans("New credits"),
			);
			if ($this->stream_obj->get_access()->has_moderator_rights())
			{
				$options[news_helper::TYPE_ACTIVITY_MODERATION] = trans("Moderation");
			}
			$processor->add_sql_filter($this->news_type_sql_filter = new select_sql_filter("news_type", trans("Type"), $options));
			$this->news_type_sql_filter->set_inactive_title(trans("Show all"));
		}

		if (!$this->user->get_user_id() or ! $this->news_type_sql_filter->get_value() or $this->news_type_sql_filter->get_value() === news_helper::TYPE_STREAM_TEXT)
		{
			$this->city_sql_filter = new foreign_key_sql_filter("city", trans("City"), "a.city_id", "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0");
			$this->city_sql_filter->set_always_select_unkeyed(true);
			$user_city_id = $this->user->get_city_id();
			if ($user_city_id and $this->db->row_exists("SELECT id FROM city WHERE id = {$user_city_id} AND is_fake = 0"))
			{
				$this->city_sql_filter->set_default_value($user_city_id);
			}
			$this->city_sql_filter->set_inactive_title(trans("Any city"));
			$processor->add_sql_filter($this->city_sql_filter);
		}

		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$news_type = $this->user->get_user_id() ? $this->news_type_sql_filter->get_value() : news_helper::TYPE_STREAM_TEXT;

		$this->data = array();

		// 1,2. Roles and marks
		if (!$news_type or $news_type === news_helper::TYPE_ACTIVITY_STATUS or $news_type === news_helper::TYPE_MARK)
		{
			$data2 = user_log_helper::get_news_feed($this->user->get_user_id(), $this->stream_id, true, $news_type);
			$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
		}

		// 3. Credits
		if (!$news_type or $news_type === news_helper::TYPE_CREDIT)
		{
			$data2 = user_credits_helper::get_news_feed($this->user->get_user_id(), $this->stream_id);
			$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
		}

		// 3. Stream news (text)
		if (!$news_type or $news_type === news_helper::TYPE_STREAM_TEXT)
		{
			$data2 = stream_news_helper::get_news_feed($this->stream_id);
			if ($this->city_sql_filter->get_value())
			{
				$data2 = array_manipulator::filter($data2, array(
						array(array_manipulator::FILTER_MODE_EQ_IF_NOT_EMPTY, "city_id", $this->city_sql_filter->get_value()),
				));
			}
			$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
		}

		// 4. Moderator's news
		if ($this->stream_obj->get_access()->is_moderator())
		{
			if (!$news_type or $news_type === news_helper::TYPE_ACTIVITY_MODERATION)
			{
				$data2 = stream_activity_participants_moderation_helper::get_news_feed($this->stream_id);
				$this->data = array_manipulator::merge_sorted($this->data, $data2, "time", SORT_DESC);
			}
		}
	}

	protected function postprocess()
	{
		news_ctrl_loader_helper::load_xml_ctrls($this->data, $this->xml_loader);
	}

}

?>