<?php

class stream_ratings_positions_xml_page extends base_xml_ctrl
{

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $stream_id;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function get_xml()
	{
		math_rating_cache_helper::check_and_request_build($this->stream_id);

		$this->xml_loader->add_xml(new stream_positions_xml_page($this->stream_obj, $this->user->get_user_id()));

		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		return $this->get_node_string();
	}

}

?>