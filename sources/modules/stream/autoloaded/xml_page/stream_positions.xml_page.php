<?php

class stream_positions_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "position";
	protected $xml_attrs = array("user_id");
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $current_user_id;

	public function __construct(stream_obj $stream_obj, $current_user_id = null)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
		$this->current_user_id = !is_null($current_user_id) ? $current_user_id : $this->user->get_user_id();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new stream_study_level_restrictions_xml_ctrl($this->stream_id, "position", "position_study_level_link", null, user_stream_status_helper::get_user_study_level_id_cached($this->current_user_id, $this->stream_id)));
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("p.id, p.title, p.descr_html, IF(up.user_id, 1, 0) AS checked");
		$select_sql->add_from("position p");
		$select_sql->add_where("p.stream_id = {$this->stream_id}");
		$select_sql->add_join("LEFT JOIN user_position up ON up.position_id = p.id AND up.user_id = {$this->current_user_id}");
		$select_sql->add_order("p.title");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>