<?php

class stream_positions_map_xml_page extends base_xml_ctrl
{

	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct();
	}

	public function get_xml()
	{
		if (!$this->xerror->is_debug_mode())
		{
			$this->set_error_404();
		}

		// Levels
		$levels = $this->db->fetch_all("
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$this->stream_id}
			ORDER BY title
		", "id");
		foreach ($levels as &$row)
		{
			$row["id"] = (int) $row["id"];
		}
		unset($row);

		// Positions
		$data = $this->db->fetch_all("
			SELECT id, title, descr_html AS description
			FROM position
			WHERE stream_id = {$this->stream_id}
		", "id");
		foreach ($data as &$row)
		{
			$row["id"] = (int) $row["id"];
//			$row["levelId"] = 3;
//			$row["level"] = array("id" => 3, "title" => "title");
		}
		unset($row);

		// Level links
		foreach ($data as &$row)
		{
			$row["levels"] = array();
		}
		unset($row);
		$levels_links_data = $this->db->fetch_all("
			SELECT l.position_id, l.study_level_id
			FROM position p
			JOIN position_study_level_link l ON l.position_id = p.id
			WHERE p.stream_id = {$this->stream_id}
		");
		foreach ($levels_links_data as $row)
		{
			$position_id = (int) $row["position_id"];
			$study_level_id = (int) $row["study_level_id"];
			if ($levels[$study_level_id])
			{
				$data[$position_id]["levels"][] = $levels[$study_level_id];
			}
		}

		// Competence links
		foreach ($data as &$row)
		{
			$row["competencies"] = array();
		}
		unset($row);
		$levels_links_data = $this->db->fetch_all("
			SELECT p.id AS position_id, c.competence_id, c.title, rcl.mark
			FROM position p
			JOIN rate ON rate.id = p.rate_id
			JOIN rate_competence_link rcl ON rcl.rate_id = rate.id
			JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE p.stream_id = {$this->stream_id} AND c.competence_set_id = {$this->stream_obj->get_competence_set_id()}
		");
		foreach ($levels_links_data as $row)
		{
			$position_id = (int) $row["position_id"];
			$competence_data = array(
				"id" => (int) $row["competence_id"],
				"title" => $row["title"],
				"mark" => (int) $row["mark"],
			);
			$data[$position_id]["competencies"][] = $competence_data;
		}

		// Export
//		foreach ($data as &$row)
//		{
//			$row["levelsIds"] = array_column($row["levels"], "id");
//			$row["competenceIds"] = array_column($row["competencies"], "id");
//		}
//		unset($row);
		$data = array_values($data);
		$json = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
//		file_put_contents_safe(PATH_TMP . "/database.json", $json);
		file_put_contents_safe("C:/httpdocs/tmp/ishipo-chart/public/database.json", $json);
		$levels = array_values($levels);
		$level_json = json_encode($levels, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
		file_put_contents_safe("C:/httpdocs/tmp/ishipo-chart/public/levels.json", $level_json);
		response::set_content_text("Rendered");
		output_buffer::finalize();
		die();
		$xdom = xdom::create($this->name);
		return $xdom->get_xml(true);
	}

}

?>