<?php

class stream_users_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	protected $xml_row_name = "user";
	// Internal
	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $stream_id;
	protected $page;

	public function __construct(stream_obj $stream_obj, $page)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", trans("Name/Login"), array("u.login", "u.first_name", "u.last_name", "u.first_name_en", "u.last_name_en"), trans("Search by name or login")));
		$processor->add_sql_filter(new foreign_key_sql_filter("city", trans("City"), "u.city_id", "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0"));
		$processor->add_sql_filter(new foreign_key_sql_filter("group", trans("Group"), "l.group_id", "group", "title", "id", "stream_id = {$this->stream_id}"));
		$this->add_easy_processor($processor);

		$processor = new sort_easy_processor();
		$processor->add_order("mark-count", "l.mark_count_calc DESC", "l.mark_count_calc ASC");
		if ($this->lang->get_current_lang_code() === "en")
		{
			$processor->add_order("name", "u.first_name_en ASC, u.last_name_en ASC", "u.first_name_en DESC, u.last_name_en DESC");
		}
		else
		{
			$processor->add_order("name", "u.first_name ASC, u.last_name ASC", "u.first_name DESC, u.last_name DESC");
		}
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("u.id");
		$select_sql->add_select_fields("l.position_count_calc, l.mark_count_calc");
		$select_sql->add_from("stream_user_link l");
		$select_sql->add_join("JOIN user u ON u.id = l.user_id");
		$select_sql->add_where("l.stream_id = {$this->stream_id}");
		$select_sql->add_where("l.status <> 'pretender' AND l.status <> 'deleted'");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>