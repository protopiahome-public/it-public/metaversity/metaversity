<?php

class stream_activity_xml_page extends activity_short_xml_ctrl
{

	// Settings
	/**
	 * @var activity_dt
	 */
	protected $dt;
	protected $axis_name = "full";
	// Internal
	protected $rate_options;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $roles_data = array();
	protected $competence_data;
	protected $required_results = array();

	public function __construct(stream_obj $stream_obj, $activity_id)
	{
		$this->rate_options = array(
			0 => "—",
			1 => trans("Improves indirectly", "COMPETENCE_IMPORTANCE"),
			2 => trans("Improves", "COMPETENCE_IMPORTANCE"),
		);
		$this->stream_obj = $stream_obj;
		parent::__construct($activity_id, $this->stream_obj->get_id());
	}

	protected function get_cache()
	{
		return false;
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_obj(stream_obj::instance($this->stream_id));
	}

	protected function modify_sql(select_sql $select_sql)
	{
		parent::modify_sql($select_sql);
		$select_sql->add_select_fields("dt.mark_count_calc");
		$select_sql->add_select_fields("dt.accepted_participant_count_calc");
		$select_sql->add_select_fields("dt.premoderation_request_count_calc");
		$select_sql->add_select_fields("dt.longterm_graded_user_count_calc");
		$select_sql->add_select_fields("dt.longterm_mark_count_calc");
	}

	protected function postprocess_data()
	{
		$this->fill_roles_data();
	}

	protected function modify_xml(xdom $xdom)
	{
		$row = reset($this->data);
		if ($row)
		{
			$xdom->set_attr("mark_count_calc", $row["mark_count_calc"]);
			$xdom->set_attr("accepted_participant_count_calc", $row["accepted_participant_count_calc"]);
			$xdom->set_attr("premoderation_request_count_calc", $row["premoderation_request_count_calc"]);
			$xdom->set_attr("longterm_graded_user_count_calc", $row["longterm_graded_user_count_calc"]);
			$xdom->set_attr("longterm_mark_count_calc", $row["longterm_mark_count_calc"]);
		}
		$this->fill_roles_xml($xdom);
		if (activity_helper::is_longterm($this->id))
		{
			$this->xml_loader->add_xml(new activity_longterm_marks_xml_ctrl($this->stream_obj, $this->id));
		}
		else
		{
			$this->xml_loader->add_xml(new activity_participants_xml_ctrl($this->stream_obj, $this->id));
		}

		if ($this->competence_data)
		{
			$xdom->set_attr("competence_id", $this->competence_data["id"]);
			$this->db_xml_converter->build_xml($xdom->create_child_node("competence"), array($this->competence_data), true);
		}
	}

	private function fill_roles_data()
	{
		$this->roles_data = activity_roles_helper::fetch_roles($this->id);

		$user_id = $this->user->get_user_id();

		$potentials = null;
		$this->competence_data = potential_math_competence_helper::get_competence_data(GET("competence"));
		if ($this->competence_data)
		{
			$this->required_results = array($this->competence_data["id"] => 3);
			$potential_stream_obj = stream_obj::instance($this->competence_data["stream_id"]);
			$potentials = potential_math_activity_helper::get_activity_roles_potential($potential_stream_obj, $this->id, $this->required_results, false, true);
		}
		elseif ($this->user->get_user_id())
		{
			$potential_math_user_streams_xml_ctrl = new potential_math_user_streams_xml_ctrl($this->user->get_user_id(), array(GET("potential-stream")), $this->stream_id);
			$this->xml_loader->add_xml($potential_math_user_streams_xml_ctrl);
			$potential_stream_id = $potential_math_user_streams_xml_ctrl->get_potential_stream_id();
			if ($potential_stream_id)
			{
				$potential_stream_obj = stream_obj::instance($potential_stream_id);
				$this->required_results = stream_user_positions_helper::get_stream_user_required_competence_results($potential_stream_id, $this->user->get_user_id(), $potential_stream_obj->get_competence_set_id());
				$potentials = potential_math_activity_helper::get_activity_roles_potential($potential_stream_obj, $this->id, $this->required_results, false, true);
			}
		}
		if ($potentials)
		{
			foreach ($this->roles_data as $role_id => &$role_data)
			{
				if (isset($potentials[$role_id]))
				{
					$role_data["potential"] = $potentials[$role_id][0];
					$role_data["potential_details"] = $potentials[$role_id][1];
				}
			}
			unset($role_data);

			$this->roles_data = array_sort($this->roles_data, "potential", SORT_DESC);

			foreach ($this->roles_data as $role_id => &$role_data)
			{
				if (isset($role_data["potential"]))
				{
					$role_data["potential"] = potential_math_common_helper::potential_number_format($role_data["potential"]);
				}
			}
			unset($role_data);
		}

		$statuses = activity_participants_front_helper::get_current_statuses_for_roles($this->id, $user_id);
		foreach ($statuses as $role_id => $status)
		{
			$this->roles_data[$role_id]["status"] = $status;
			array_move_element_up($this->roles_data, $role_id);
		}

		$this->load_rates();

		$format_id = $this->data[0]["format_id"];
		$this->xml_loader->add_xml(new stream_study_level_restrictions_xml_ctrl($this->stream_id, "role", "role_study_level_link", "o.format_id = {$format_id}", user_stream_status_helper::get_user_study_level_id_cached($user_id, $this->stream_id)));
	}

	private function fill_roles_xml(xdom $xdom)
	{
		$roles_node = $xdom->create_child_node("roles");
		foreach ($this->roles_data as $role_data)
		{
			$role_node = $roles_node->create_child_node("role");
			unset($role_data["potential_details"]);
			$this->db_xml_converter->build_xml($role_node, array($role_data), true);
		}
	}

	private function load_rates()
	{
		foreach ($this->roles_data as $role_data)
		{
			$this->xml_loader->add_xml(new rate_xml_ctrl($role_data["rate_id"], $this->stream_obj->get_competence_set_id(), $this->rate_options, null, $this->required_results, isset($role_data["potential_details"]) ? $role_data["potential_details"] : null));
		}
	}

}

?>