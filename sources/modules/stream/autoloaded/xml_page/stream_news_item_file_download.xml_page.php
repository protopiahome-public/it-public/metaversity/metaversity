<?php

class stream_news_item_file_download_xml_page extends base_intmarkup_file_download_xml_ctrl
{

	// Settings
	protected $object_type = array("news_item");
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;

	public function __construct(stream_obj $stream_obj, $file_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		parent::__construct($file_id);
	}

	protected function check_file_rights()
	{
		return true;
	}

}

?>