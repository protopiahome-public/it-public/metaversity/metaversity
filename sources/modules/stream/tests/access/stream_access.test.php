<?php

class stream_access_test extends base_test
{

	private $log_path;

	public function set_up()
	{
		$this->log_path = PATH_LOG . "/stream_access.log";
		if (!$this->db->row_exists("SELECT * FROM user WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO user (id, login) VALUES (1, 'test_user')");
		}
		if (!$this->db->row_exists("SELECT * FROM competence_set WHERE id = 1"))
		{
			$this->db->sql("INSERT INTO competence_set (id, title) VALUES (1, 'test')");
		}
		$this->db->sql("REPLACE INTO stream (id, name, title, title_short, competence_set_id) VALUES (1, 'test', 'test', 'test', 1)");
		$this->db->sql("REPLACE INTO stream_user_link (stream_id, user_id, status) VALUES (1, 1, 'member')");
		$this->db->sql("DELETE FROM stream_admin WHERE stream_id = 1 AND user_id = 1");
	}

	public function basic_test()
	{
		$this->mcache->flush();
		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
	}

	public function basic_cache_test()
	{
		$this->mcache->flush();
		$query_count = $this->db->debug_get_query_count();

		stream_obj::_test_clean_cache();
		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 2);

		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 2);
	}

	public function moderator_test()
	{
		$this->db->sql("REPLACE INTO stream_user_link (stream_id, user_id, status) VALUES (1, 1, 'moderator')");
		$this->db->sql("REPLACE INTO stream_admin (stream_id, user_id) VALUES (1, 1)");
		$this->mcache->flush();

		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_false($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
	}

	public function admin_test()
	{
		$this->db->sql("REPLACE INTO stream_user_link (stream_id, user_id, status) VALUES (1, 1, 'admin')");
		$this->db->sql("REPLACE INTO stream_admin (stream_id, user_id, is_admin) VALUES (1, 1, 1)");
		$this->mcache->flush();

		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
	}

	public function admin_cache_test()
	{
		$this->db->sql("REPLACE INTO stream_user_link (stream_id, user_id, status) VALUES (1, 1, 'admin')");
		$this->db->sql("REPLACE INTO stream_admin (stream_id, user_id, is_admin) VALUES (1, 1, 1)");
		$this->mcache->flush();

		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());

		$query_count = $this->db->debug_get_query_count();
		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->assert_identical($this->db->debug_get_query_count() - $query_count, 0);
	}

	public function super_admin_test()
	{
		$this->db->sql("DELETE FROM stream_user_link WHERE stream_id = 1 AND user_id = 1");
		$this->db->sql("DELETE FROM stream_admin WHERE stream_id = 1 AND user_id = 1");
		$this->mcache->flush();

		$this->user->set_user_data(array("is_admin" => "1"));
		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_true($access->has_member_rights());
		$this->assert_true($access->has_admin_rights());
		$this->assert_true($access->has_moderator_rights());
		$this->user->set_user_data(null);
	}

	public function not_admin_test()
	{
		$this->db->sql("REPLACE INTO stream_user_link (stream_id, user_id, status) VALUES (1, 1, 'moderator')");
		$this->db->sql("REPLACE INTO stream_admin (stream_id, user_id, is_admin) VALUES (1, 1, 1 /* !!! */)");
		$this->mcache->flush();

		$access = new stream_access(stream_obj::instance(1), 1);
		$this->assert_false($access->has_admin_rights());
	}

	public function log_test()
	{
		if (file_exists($this->log_path))
		{
			$this->assert_fail("Log already exists. We fail here to save the log");
			return;
		}
		$this->db->sql("REPLACE INTO stream_user_link (stream_id, user_id, status) VALUES (1, 1, 'moderator')");
		$this->db->sql("DELETE FROM stream_admin WHERE stream_id = 1 AND user_id = 1");
		$this->mcache->flush();

		new stream_access(stream_obj::instance(1), 1);
		$this->assert_true(file_exists($this->log_path));
		unlink_safe($this->log_path);
	}

}

?>