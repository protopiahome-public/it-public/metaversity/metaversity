<?php

abstract class base_api_ajax_ctrl extends base_ajax_ctrl
{

	public function init()
	{
		response::set_content_type_json();
	}

	protected function get_request_ids($name)
	{
		return implode(", ", array_filter(array_map("trim", explode(",", REQUEST($name))), "is_good_id"));
	}

	protected function check_input_id($table, $exists_callback = null)
	{
		$param_name = $table . "_id";
		$this->$param_name = REQUEST($param_name);
		
		if (!is_good_id($this->$param_name))
		{
			$this->ajax_loader->write_error("WRONG_" . strtoupper($param_name));
			return false;
		}

		$row_exists = $exists_callback ? $exists_callback($this->db, $this->$param_name) : $this->db->row_exists("SELECT id FROM {$table} WHERE id = {$this->$param_name}");
		if (!$row_exists)
		{
			$this->ajax_loader->write_error("WRONG_" . strtoupper($param_name));
			return false;
		}

		return true;
	}

	public function on_before_check()
	{
//		if (!$this->xerror->is_debug_mode())
//		{
//			$this->ajax_loader->write_error("TOO_EARLY");
//			return false;
//		}

		if (REQUEST("api_login") != "admin" && REQUEST("api_password") != "4nrefkjn3ew34")
		{
			$this->ajax_loader->write_error("WRONG_ACCESS");
			return false;
		}

		return true;
	}

}

?>