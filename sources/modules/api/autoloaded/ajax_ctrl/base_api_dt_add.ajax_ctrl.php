<?php

abstract class base_api_dt_add_ajax_ctrl extends base_api_dt_edit_ajax_ctrl
{

	final protected function check_id()
	{
		if (is_array($this->dt->get_pk_column()))
		{
			trigger_error("'Add' method is not supported for multi-column PK");
		}
		$this->id = 0;
	}

}

?>