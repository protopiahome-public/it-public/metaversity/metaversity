<?php

abstract class base_api_dt_edit_ajax_ctrl extends base_api_ajax_ctrl
{
// Settings
	protected $dt_name;
	protected $axis_name;
	// Internal
	protected $id;
	protected $action;
	protected $last_id;
	protected $old_db_row;
	protected $updated_db_row;
	protected $update_array = array();

	/**
	 * @var base_dt
	 */
	protected $dt;
	protected $dtfs = array();
	protected $save_dtfs = array();

	/**
	 * @var pass_info_plain
	 */
	protected $pass_info;

	public function __construct()
	{
		$_POST = $_REQUEST;

		parent::__construct();
	}

	final public function start()
	{
		global $pass_info;
		$this->pass_info = $pass_info;

		$this->dt_init();
		$this->check_id();
		if ($this->id < 0)
		{
			return false;
		}
		if (!$this->mixin_call_method_with_mixins("on_after_dt_init", array(), "chain"))
		{
			return false;
		}

		if ($this->id)
		{
			$this->old_db_row = $this->get_db_row();
			if (!$this->old_db_row and ! is_array($this->id))
			{
				return false;
			}
			$this->old_db_row = $this->old_db_row ? : null;
			$this->updated_db_row = $this->old_db_row ? : array();
		}
		else
		{
			$this->old_db_row = null;
			$this->updated_db_row = array();
		}
		return true;
	}

	protected function dt_init()
	{
		$class_reflection = new ReflectionClass($this->dt_name . "_dt");
		$this->dt = $class_reflection->newInstance();
	}

	protected function check_id()
	{
		if (is_array($this->dt->get_pk_column()))
		{
			$this->id = array();
			foreach ($this->dt->get_pk_column() as $pk_column_name)
			{
				$id_value = REQUEST($pk_column_name);
				if (!is_good_id($id_value))
				{
					$this->id = -1;
					return;
				}
				$this->id[$pk_column_name] = $id_value;
			}
		}
		else
		{
			$this->id = REQUEST($this->dt->get_pk_column());
			if (!is_good_id($this->id))
			{
				$this->id = -1;
			}
		}
		$this->action = $this->id ? "dt_edit" : "dt_add";
	}

	protected function on_after_dt_init()
	{
		return true;
	}

	final public function check()
	{
		$this->dtfs = $this->dt->get_fields_by_axis($this->axis_name, $remove_read_only = true);
		$this->fill_save_dtfs();
		$ok = true;
		foreach ($this->save_dtfs as $save_dtf_idx => $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$deps = $save_dtf->get_dtf()->get_dependencies();
			foreach ($deps as $dependency)
			{
				/* @var $dependency dtf_dependency */
				if ($this->updated_db_row[$dependency->get_inspected_column()] == $dependency->get_activation_value())
				{
					if ($dependency->get_actions() & INTCMF_DTF_DEP_ACTION_IMPORTANT and method_exists($save_dtf->get_dtf(), "set_importance"))
					{
						$save_dtf->get_dtf()->set_importance(true);
					}
					if ($dependency->get_actions() & INTCMF_DTF_DEP_ACTION_DO_NOT_STORE)
					{
						unset($this->save_dtfs[$save_dtf_idx]);
						continue 2;
					}
				}
			}
			$check_ok = $save_dtf->check();
			if ($check_ok)
			{
				$save_dtf->update_db_row($this->updated_db_row);
			}
			$ok = $check_ok && $ok;
		}
		return $ok;
	}

	final public function commit()
	{
		foreach ($this->save_dtfs as $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$save_dtf->get_fields_to_write($this->update_array);
		}
		if (is_array($this->id))
		{
			foreach ($this->id as $column_name => $column_value)
			{
				$this->update_array[$column_name] = $column_value;
			}
		}
		elseif ($this->id)
		{
			$this->update_array[$this->dt->get_pk_column()] = $this->id;
		}
		if (!$this->old_db_row and $this->dt->get_add_timestamp_column())
		{
			$this->update_array[$this->dt->get_add_timestamp_column()] = $this->db->get_now(true);
		}
		if ($this->dt->get_edit_timestamp_column())
		{
			$this->update_array[$this->dt->get_edit_timestamp_column()] = $this->db->get_now(true);
		}
		if (!$this->old_db_row)
		{
			$this->db->insert_by_array($this->dt->get_db_table(), $this->update_array);
			$this->last_id = !is_array($this->id) ? $this->db->get_last_id() : $this->id;
		}
		else
		{
			$where = "";
			if (is_array($this->id))
			{
				foreach ($this->id as $column_name => $column_value)
				{
					$where .= $where ? " AND " : "";
					$where .= "{$column_name} = {$column_value}";
				}
			}
			else
			{
				$where .= "{$this->dt->get_pk_column()} = {$this->id}";
			}
			$this->db->update_by_array($this->dt->get_db_table(), $this->update_array, $where);
			$this->last_id = $this->id;
		}
		foreach ($this->save_dtfs as $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$save_dtf->commit($this->last_id);
		}
		$this->pass_info->write_info('SAVED', $this->last_id);
		$this->pass_info->write_info('action', $this->action);
		return true;
	}

	final public function rollback()
	{
		foreach ($this->save_dtfs as $save_dtf)
		{
			/* @var $save_dtf base_save_dtf */
			$save_dtf->rollback();
		}
		//$this->pass_info->dump_vars();
	}

	private function fill_save_dtfs()
	{
		foreach ($this->dtfs as $dtf)
		{
			/* @var $dtf base_dtf */
			$field_name = $dtf->get_field_name();
			$field_type = $dtf->get_field_type();
			$class_reflection = new ReflectionClass($field_type . "_save_dtf");
			$this->save_dtfs[$field_name] = $class_reflection->newInstance($dtf, $this->pass_info, $this->dt->get_db_table(), $this->id, $this->old_db_row);
		}
	}

	private function get_db_row($lock = true)
	{
		$id = $this->id ? $this->id : $this->last_id;
		if (!$id)
		{
			trigger_error("Incorrect call: \$this->id must be > 0");
		}
		$select_sql = new select_sql();
		$select_sql->add_from("`" . $this->dt->get_db_table() . "`", "dt");
		$select_sql->add_select_fields("dt.*");
		if (is_array($id))
		{
			foreach ($id as $column_name => $column_value)
			{
				$select_sql->add_where("dt.{$column_name} = {$column_value}");
			}
		}
		else
		{
			$select_sql->add_where("dt.{$this->dt->get_pk_column()} = {$id}");
		}
		if ($lock)
		{
			$select_sql->set_FOR_UPDATE_modifier();
		}
		$this->mixin_call_method_with_mixins("modify_sql", array($select_sql));
		return $this->db->get_row($select_sql->get_sql());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

	public function get_data()
	{
		$data = $this->pass_info->get_data();
		if (!isset($data["ERRORS"]) && !isset($data["FIELDS_ERRORS"]))
		{
			$data["status"] = "OK";
		}
		return $data;
	}

}

?>