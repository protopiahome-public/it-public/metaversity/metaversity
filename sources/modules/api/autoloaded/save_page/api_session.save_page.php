<?php

class api_session_save_page extends base_save_ctrl
{

	protected $redirect_url;
	protected $parsed_redirect_url;
	protected $app_title;
	protected $token;

	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function start()
	{
		$this->redirect_url = POST("redirect_url");
		$this->app_title = POST("app_title");

		$this->parsed_redirect_url = parse_url(POST("redirect_url"));

		if ($this->parsed_redirect_url == null || !isset($this->parsed_redirect_url["scheme"]) || !in_array($this->parsed_redirect_url["scheme"], array("http", "https")) || strlen($this->app_title) == 0)
		{
			return false;
		}
		$host_escaped = $this->db->escape($this->parsed_redirect_url["host"]);
		$domain_allowed = $this->db->row_exists("
			SELECT id 
			FROM app_allowed_domain
			WHERE domain = '{$host_escaped}'
		");
		if (!$domain_allowed)
		{
			return false;
		}

		return true;
	}

	public function commit()
	{
		$app_title_escaped = $this->db->escape($this->app_title);
		$redirect_url_escaped = $this->db->escape($this->redirect_url);
		$this->token = md5(microtime());
		$token_escaped = $this->db->escape($this->token);
		$this->db->sql("
			INSERT INTO api_session (user_id, app_title, redirect_url, token, start_time)
			VALUES ({$this->user->get_user_id()}, '{$app_title_escaped}', '{$redirect_url_escaped}', '{$token_escaped}', NOW())
		");
		return true;
	}

	public function on_after_commit()
	{
		$this->set_redirect_url($this->parsed_redirect_url["scheme"] . "://" . $this->parsed_redirect_url["host"] . $this->parsed_redirect_url["path"] . "?token=" . $this->token . "&user_id=" . $this->user->get_user_id());
	}

}

?>