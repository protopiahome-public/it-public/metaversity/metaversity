<?php

class api_session_xml_page extends base_easy_xml_ctrl
{

	protected function load_data(\select_sql $select_sql = null)
	{
		$redirect_url = GET("redirect_url");
		$parsed_redirect_url = parse_url(GET("redirect_url"));
		$app_title = GET("app_title");

		if ($parsed_redirect_url == null || !isset($parsed_redirect_url["scheme"]) || !in_array($parsed_redirect_url["scheme"], array("http", "https")) || strlen($app_title) == 0)
		{
			$this->set_error_404();
			return;
		}
		$host_escaped = $this->db->escape($parsed_redirect_url["host"]);
		$domain_allowed = $this->db->row_exists("
			SELECT id 
			FROM app_allowed_domain
			WHERE domain = '{$host_escaped}'
		");
		if (!$domain_allowed)
		{
			$this->set_error_404();
			return;
		}

		$redirect_url_escaped = $this->db->escape($redirect_url);
		$token = $this->db->get_value("
			SELECT token 
			FROM api_session
			WHERE user_id = {$this->user->get_user_id()} AND redirect_url = '{$redirect_url_escaped}'
		");
		if ($token)
		{
			$this->set_redirect_url($parsed_redirect_url["scheme"] . "://" . $parsed_redirect_url["host"] . $parsed_redirect_url["path"] . "?token=" . $token . "&user_id=" . $this->user->get_user_id());
		}

		$this->data[0] = array(
			"app_title" => $app_title,
			"redirect_url" => $redirect_url,
			"app_domain" => $parsed_redirect_url["scheme"] . "://" . $parsed_redirect_url["host"],
		);
	}

}

?>