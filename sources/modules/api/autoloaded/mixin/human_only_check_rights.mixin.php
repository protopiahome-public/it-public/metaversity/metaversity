<?php

class human_only_check_rights_mixin extends base_mixin
{

	/**
	 * @var user
	 */
	protected $user;

	public function check_rights()
	{
		return !$this->user->get_api_mode();
	}

}

?>