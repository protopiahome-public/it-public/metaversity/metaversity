<?php

class api_get_format_ajax_page extends base_api_ajax_ctrl
{
	protected $format_id;
	protected $format_data;

	public function check()
	{
		$this->format_id = REQUEST("id");
		if (!is_good_id($this->format_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->format_data = $this->db->get_row("SELECT id, title FROM format WHERE id = {$this->format_id}");
		if (!$this->format_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}
		
		return true;
	}

	public function get_data()
	{
		return $this->format_data;
	}

}

?>