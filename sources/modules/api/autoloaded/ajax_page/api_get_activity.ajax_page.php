<?php

class api_get_activity_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $activity_data;

	public function check()
	{
		$this->activity_id = REQUEST("id");
		if (!is_good_id($this->activity_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->activity_data = $this->db->get_row("
			SELECT a.id, a.title, start_time, finish_time, c.id AS city_id, c.title AS city_title, f.id as format_id, f.title as format_title, date_type, descr_html, is_date_common
			FROM activity a
			LEFT JOIN format f ON f.id = a.format_id
			LEFT JOIN city c ON c.id = a.city_id
			WHERE a.id = {$this->activity_id}
		");
		if (!$this->activity_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}
		
		if (!$this->activity_data["is_date_common"])
		{
			$this->activity_data["cities"] = $this->db->fetch_all("
				SELECT acl.city_id, c.title as city_title, start_time, finish_time, place, descr_html
					FROM activity_city_link acl
					LEFT JOIN city c ON c.id = acl.city_id
					WHERE acl.activity_id = {$this->activity_id}
				");
		}
		
		return true;
	}

	public function get_data()
	{
		return $this->activity_data;
	}

}

?>