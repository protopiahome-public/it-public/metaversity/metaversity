<?php

class api_get_position_ajax_page extends base_api_ajax_ctrl
{
	protected $position_id;
	protected $position_data;

	public function check()
	{
		$this->position_id = REQUEST("id");
		if (!is_good_id($this->position_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->position_data = $this->db->get_row("SELECT id, title, stream_id, descr_html, rate_id FROM position WHERE id = {$this->position_id}");
		if (!$this->position_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}
		
		return true;
	}

	public function get_data()
	{
		$this->position_data["competences"] = $this->db->fetch_all("
			SELECT c.id, c.title
			FROM rate_competence_link rcl
			LEFT JOIN competence_full c ON c.id = rcl.competence_id
			WHERE rcl.rate_id = {$this->position_data["rate_id"]} AND c.is_deleted = 0
		");
			
		unset($this->position_data["rate_id"]);
		
		return $this->position_data;
	}

}

?>