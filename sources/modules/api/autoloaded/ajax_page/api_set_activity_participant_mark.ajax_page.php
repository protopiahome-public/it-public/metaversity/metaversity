<?php

class api_set_activity_participant_mark_ajax_page extends base_api_ajax_ctrl
{
	protected $user_id;
	protected $activity_id;
	protected $city_id;
	protected $role_id;
	protected $mark;
	protected $comment;

	public function check()
	{
		if (!$this->check_input_id("activity"))
		{
			return false;
		}
		if (!$this->check_input_id("city"))
		{
			return false;
		}
		if (!$this->check_input_id("role"))
		{
			return false;
		}
		if (!$this->check_input_id("user"))
		{
			return false;
		}

		$this->mark = REQUEST("mark");
		if (!in_array($this->mark, array("0", "1", "2", "3", "-")))
		{
			$this->ajax_loader->write_error("WRONG_STATUS");
			return false;
		}

		$this->comment = REQUEST("comment");

		return true;
	}

	public function commit()
	{
		$stream_admin_user_id = $this->db->get_value("SELECT dul.user_id FROM stream_user_link dul LEFT JOIN activity a ON a.stream_id = dul.stream_id WHERE dul.status = 'admin' AND a.id = {$this->activity_id}");
		$this->user->login_by_id($stream_admin_user_id);
		activity_marks_helper::change_participant_mark($this->activity_id, $this->role_id, $this->user_id, $this->city_id, $this->mark, $this->comment);

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

}

?>