<?php

class api_find_users_ajax_page extends base_api_ajax_ctrl
{
	protected $streams_ids;
	protected $cities_ids;
	protected $study_levels_ids;
	protected $groups_ids;
	protected $users_data;


	public function check()
	{
		$this->streams_ids = $this->get_request_ids("streams_ids");
		$this->cities_ids = $this->get_request_ids("cities_ids");
		$this->study_levels_ids = $this->get_request_ids("study_levels_ids");
		$this->groups_ids = $this->get_request_ids("groups_ids");

		return true;
	}

	public function get_data()
	{
		$select_sql = new select_sql();
		$select_sql->add_select_fields("id");
		$select_sql->add_from("user");
		if (strlen($this->cities_ids))
		{
			$select_sql->add_where("city_id IN ({$this->cities_ids})");
		}
		if (strlen($this->streams_ids))
		{
			$select_sql->add_where("stream_id IN ({$this->streams_ids})");
		}
		if (strlen($this->groups_ids))
		{
			$select_sql->add_where("group_id IN ({$this->groups_ids})");
		}

		$this->users_data = $this->db->fetch_all($select_sql->get_sql());

		return $this->users_data;
	}

}

?>