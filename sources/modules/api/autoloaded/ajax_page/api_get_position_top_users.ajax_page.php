<?php

class api_get_position_top_users_ajax_page extends base_api_ajax_ctrl
{
	protected $stream_id;
	protected $position_id;
	protected $limit;

	public function check()
	{
		if (!$this->check_input_id("stream"))
		{
			return false;
		}
		if (!$this->check_input_id("position"))
		{
			return false;
		}
		$this->limit = REQUEST("limit");
		if ($this->limit && !is_good_id($this->limit))
		{
			$this->ajax_loader->write_error("BAD_LIMIT");
			return false;
		}

		return true;
	}

	public function get_data()
	{
		$stream_obj = stream_obj::instance($this->stream_id);
		$data = stream_results_helper::get_positions_results($stream_obj);

		$result = isset($data[$this->position_id]) ? $data[$this->position_id] : array();
		if ($this->limit)
		{
			$result = array_slice($result, 0, $this->limit, true);
		}
		
		foreach ($result as $user_id => $value)
		{
			$result[$user_id] = array("user_id" => $user_id, "result" => $value);
		}
		
		return $result;
	}

}

?>