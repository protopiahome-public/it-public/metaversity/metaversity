<?php

class api_activity_delete_stream_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $stream_id;
	protected $activity_data;

	public function check()
	{
		$this->activity_id = REQUEST("activity_id");
		if (!is_good_id($this->activity_id))
		{
			$this->ajax_loader->write_error("WRONG_ACTIVITY_ID");
			return false;
		}

		$this->stream_id = REQUEST("stream_id");
		if (!is_good_id($this->stream_id))
		{
			$this->ajax_loader->write_error("WRONG_STREAM_ID");
			return false;
		}

		$activity_exists = $this->db->row_exists("
			SELECT id
			FROM activity
			WHERE id = {$this->activity_id}
		");
		if (!$activity_exists)
		{
			$this->ajax_loader->write_error("WRONG_ACTIVITY_ID");
			return false;
		}

		$stream_exists = $this->db->row_exists("
			SELECT id
			FROM `stream`
			WHERE id = {$this->stream_id}
		");
		if (!$stream_exists)
		{
			$this->ajax_loader->write_error("WRONG_STREAM_ID");
			return false;
		}

		return true;
	}

	public function commit()
	{
		$this->db->sql("
			DELETE FROM activity_stream_link
			WHERE activity_id = {$this->activity_id} AND stream_id = {$this->stream_id}
		");

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

	public function clean_cache()
	{
		activity_cache_tag::init($this->activity_id)->update();
	}

}

?>