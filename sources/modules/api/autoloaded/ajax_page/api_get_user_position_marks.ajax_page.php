<?php

class api_get_user_position_marks_ajax_page extends base_api_ajax_ctrl
{
	protected $position_id;
	protected $user_id;

	public function check()
	{
		if (!$this->check_input_id("position"))
		{
			return false;
		}
		if (!$this->check_input_id("user"))
		{
			return false;
		}

		return true;
	}

	public function get_data()
	{
		$data = $this->db->fetch_all("
			SELECT rcl.competence_id, MAX(rcl.mark) AS mark
			FROM user_position up
			INNER JOIN position p ON p.id = up.position_id
			INNER JOIN rate r ON r.id = p.rate_id
			INNER JOIN rate_competence_link rcl ON rcl.rate_id = r.id
			INNER JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE up.user_id = {$this->user_id} AND up.position_id = {$this->position_id}
			GROUP BY rcl.competence_id
		");

		return $data;
	}

}

?>