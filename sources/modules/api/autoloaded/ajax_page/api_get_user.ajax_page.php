<?php

class api_get_user_ajax_page extends base_api_ajax_ctrl
{
	protected $user_id;
	protected $user_data;

	public function check()
	{
		$this->user_id = REQUEST("id");
		if (!is_good_id($this->user_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->user_data = $this->db->get_row("
			SELECT u.id, u.login, u.first_name, u.mid_name, u.last_name, d.id as stream_id, d.title as stream_title, g.id as group_id, g.title as group_title, c.id as city_id, c.title as city_title, u.is_admin
			FROM user u
			LEFT JOIN stream d ON d.id = u.stream_id
			LEFT JOIN `group` g ON g.id = u.group_id
			LEFT JOIN city c ON c.id = u.city_id
			WHERE u.id = {$this->user_id}
		");
		if (!$this->user_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		return true;
	}

	public function get_data()
	{
		return $this->user_data;
	}

}

?>