<?php

class api_get_activity_roles_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $activity_data;

	public function check()
	{
		$this->activity_id = REQUEST("id");
		if (!is_good_id($this->activity_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->activity_data = $this->db->get_row("SELECT id, title FROM activity WHERE id = {$this->activity_id}");
		if (!$this->activity_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}
		
		return true;
	}

	public function get_data()
	{
		$this->activity_data["roles"] = $this->db->fetch_all("
			SELECT r.id, r.title
			FROM activity_role_link arl
			LEFT JOIN role r ON r.id = arl.role_id
			WHERE arl.activity_id = {$this->activity_data["id"]} AND arl.enabled = 1
		");
			
		return $this->activity_data;
	}

}

?>