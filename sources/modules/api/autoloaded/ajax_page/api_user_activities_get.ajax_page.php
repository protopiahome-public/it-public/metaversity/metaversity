<?php

class api_user_activities_get_ajax_page extends base_api_ajax_ctrl
{
	protected $user_id;
	protected $user_activities;

	public function check()
	{
		$this->user_id = REQUEST("id");
		if (!is_good_id($this->user_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$user_exists = $this->db->get_row("SELECT id FROM user WHERE id = {$this->user_id}");
		if (!$user_exists)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		return true;
	}

	public function get_data()
	{
		$this->user_activities = $this->db->fetch_all("
			SELECT a.id, a.title, ap.status
			FROM activity_participant ap
			LEFT JOIN activity a ON ap.activity_id = a.id
			WHERE ap.user_id = {$this->user_id} AND ap.status != 'deleted'
		");
		return $this->user_activities;
	}

}

?>