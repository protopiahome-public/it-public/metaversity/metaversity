<?php

class api_find_positions_ajax_page extends base_api_ajax_ctrl
{
	protected $stream_id;
	protected $stream_data;

	public function check()
	{
		$this->stream_id = REQUEST("stream_id");
		if (!is_good_id($this->stream_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->stream_data = $this->db->get_row("SELECT id FROM stream WHERE id = {$this->stream_id}");
		if (!$this->stream_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		return true;
	}

	public function get_data()
	{
		$positions = $this->db->fetch_all("
			SELECT position_id
			FROM position_stream_link
			WHERE stream_id = {$this->stream_id}
		");

		return $positions;
	}

}

?>