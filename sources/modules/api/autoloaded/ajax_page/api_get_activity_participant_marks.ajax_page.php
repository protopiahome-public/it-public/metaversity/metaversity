<?php

class api_get_activity_participant_marks_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $user_id;

	public function check()
	{
		if (!$this->check_input_id("activity"))
		{
			return false;
		}
		if (!$this->check_input_id("user"))
		{
			return false;
		}

		return true;
	}

	public function get_data()
	{
		$data = $this->db->fetch_all("
			SELECT u.id as user_id, u.login as user_login, apm.city_id, r.id as role_id, r.title as role_title, apm.mark, apm.comment, apm.change_time
			FROM activity_participant_mark apm
			LEFT JOIN user u ON u.id = apm.user_id
			LEFT JOIN role r ON r.id = apm.role_id
			WHERE apm.activity_id = {$this->activity_id} AND apm.user_id = {$this->user_id}
		");

		return $data;
	}

}

?>