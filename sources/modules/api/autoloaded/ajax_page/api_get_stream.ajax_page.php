<?php

class api_get_stream_ajax_page extends base_api_ajax_ctrl
{
	protected $stream_id;
	protected $stream_data;

	public function check()
	{
		$this->stream_id = REQUEST("id");
		if (!is_good_id($this->stream_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->stream_data = $this->db->get_row("SELECT id, title FROM stream WHERE id = {$this->stream_id}");
		if (!$this->stream_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}
		
		return true;
	}

	public function get_data()
	{
		$this->stream_data["cities"] = $this->db->fetch_all("
			SELECT c.id, c.title
			FROM city c
			LEFT JOIN stream_city_link dc ON dc.city_id = c.id
			WHERE dc.stream_id = {$this->stream_data["id"]}
		");
		
		$this->stream_data["groups"] = $this->db->fetch_all("
			SELECT id, title
			FROM `group`
			WHERE stream_id = {$this->stream_data["id"]}
		");
		
		$this->stream_data["study_levels"] = $this->db->fetch_all("
			SELECT id, title
			FROM study_level
			WHERE stream_id = {$this->stream_data["id"]}
		");
		
		return $this->stream_data;
	}

}

?>