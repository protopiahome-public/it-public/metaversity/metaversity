<?php

class api_activity_add_group_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $group_id;
	protected $activity_data;

	public function check()
	{
		$this->activity_id = REQUEST("activity_id");
		if (!is_good_id($this->activity_id))
		{
			$this->ajax_loader->write_error("WRONG_ACTIVITY_ID");
			return false;
		}

		$this->group_id = REQUEST("group_id");
		if (!is_good_id($this->group_id))
		{
			$this->ajax_loader->write_error("WRONG_GROUP_ID");
			return false;
		}

		$activity_exists = $this->db->row_exists("
			SELECT id
			FROM activity
			WHERE id = {$this->activity_id}
		");
		if (!$activity_exists)
		{
			$this->ajax_loader->write_error("WRONG_ACTIVITY_ID");
			return false;
		}

		$group_exists = $this->db->row_exists("
			SELECT id
			FROM `group`
			WHERE id = {$this->group_id}
		");
		if (!$group_exists)
		{
			$this->ajax_loader->write_error("WRONG_GROUP_ID");
			return false;
		}

		return true;
	}

	public function commit()
	{
		$this->db->sql("
			INSERT IGNORE activity_group_link (activity_id, group_id)
			VALUES ({$this->activity_id}, {$this->group_id})
		");

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

	public function clean_cache()
	{
		activity_cache_tag::init($this->activity_id)->update();
	}

}

?>