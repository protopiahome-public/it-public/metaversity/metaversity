<?php

class api_user_add_position_ajax_page extends base_api_ajax_ctrl
{
	protected $user_id;
	protected $position_id;
	protected $user_data;

	public function check()
	{
		$this->user_id = REQUEST("user_id");
		if (!is_good_id($this->user_id))
		{
			$this->ajax_loader->write_error("WRONG_USER_ID");
			return false;
		}

		$this->position_id = REQUEST("position_id");
		if (!is_good_id($this->position_id))
		{
			$this->ajax_loader->write_error("WRONG_POSITION_ID");
			return false;
		}

		$user_exists = $this->db->row_exists("
			SELECT id
			FROM user
			WHERE id = {$this->user_id}
		");
		if (!$user_exists)
		{
			$this->ajax_loader->write_error("WRONG_USER_ID");
			return false;
		}

		$position_exists = $this->db->row_exists("
			SELECT id
			FROM `position`
			WHERE id = {$this->position_id}
		");
		if (!$position_exists)
		{
			$this->ajax_loader->write_error("WRONG_POSITION_ID");
			return false;
		}

		return true;
	}

	public function commit()
	{
		position_helper::change_position($this->user_id, $this->position_id, 1);

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

	public function clean_cache()
	{
		user_cache_tag::init($this->user_id)->update();
	}

}

?>