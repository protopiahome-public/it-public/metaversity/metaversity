<?php

class api_add_user_ajax_page extends base_api_dt_add_ajax_ctrl
{
	protected $dt_name = "user";
	protected $axis_name = "register_api";

	public function clean_cache()
	{
		user_list_cache_tag::init()->update();
	}

}

?>