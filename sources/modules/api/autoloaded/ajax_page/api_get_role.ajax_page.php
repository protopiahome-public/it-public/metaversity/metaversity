<?php

class api_get_role_ajax_page extends base_api_ajax_ctrl
{
	protected $role_id;
	protected $role_data;

	public function check()
	{
		$this->role_id = REQUEST("id");
		if (!is_good_id($this->role_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->role_data = $this->db->get_row("SELECT id, title, rate_id FROM role WHERE id = {$this->role_id}");
		if (!$this->role_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}
		
		return true;
	}

	public function get_data()
	{
		$this->role_data["competences"] = $this->db->fetch_all("
			SELECT c.id, c.title, rcl.mark
			FROM rate_competence_link rcl
			LEFT JOIN competence_full c ON c.id = rcl.competence_id
			WHERE rcl.rate_id = {$this->role_data["rate_id"]} AND c.is_deleted = 0
		");
			
		unset($this->role_data["rate_id"]);
		
		return $this->role_data;
	}

}

?>