<?php

class api_get_activity_participants_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $activity_data;

	public function check()
	{
		$this->activity_id = REQUEST("id");
		if (!is_good_id($this->activity_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->activity_data = $this->db->get_row("SELECT id, title FROM activity WHERE id = {$this->activity_id}");
		if (!$this->activity_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}
		
		return true;
	}

	public function get_data()
	{
		$data = $this->db->fetch_all("
			SELECT u.id as user_id, u.login as user_login, ap.city_id, r.id as role_id, r.title as role_title, ap.status, ap.change_time
			FROM activity_participant ap
			LEFT JOIN user u ON u.id = ap.user_id
			LEFT JOIN role r ON r.id = ap.role_id
			WHERE ap.activity_id = {$this->activity_data["id"]} AND ap.status != 'deleted'
		");
			
		$this->activity_data["roles"] = array();
		foreach ($data as $paticipant)
		{
			$role_id = $paticipant["role_id"];
			$this->activity_data["participant"][$role_id] = array();
			$this->activity_data["participant"][$role_id]["id"] = $paticipant["role_id"];
			$this->activity_data["participant"][$role_id]["title"] = $paticipant["role_title"];
			unset($paticipant["role_id"]);
			unset($paticipant["role_title"]);
			$this->activity_data["participant"][$role_id]["users"][] = $paticipant;
		}
		
		return $this->activity_data;
	}

}

?>