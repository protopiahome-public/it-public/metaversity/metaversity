<?php

class api_get_activity_user_potential_ajax_page extends base_api_ajax_ctrl
{
	protected $user_id;
	protected $activity_id;

	public function check()
	{
		if (!$this->check_input_id("activity"))
		{
			return false;
		}
		if (!$this->check_input_id("user"))
		{
			return false;
		}
		return true;
	}

	public function get_data()
	{
		$user_data = $this->db->get_row("SELECT id, city_id FROM user WHERE id = {$this->user_id}");

		$result = position_helper::get_activity_user_potential($this->activity_id, $this->user_id, $user_data["city_id"]);
		return $result;
	}

}

?>