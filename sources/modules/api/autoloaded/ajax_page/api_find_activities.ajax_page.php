<?php

class api_find_activities_ajax_page extends base_api_ajax_ctrl
{
	protected $streams_ids;
	protected $cities_ids;
	protected $study_levels_ids;
	protected $groups_ids;
	protected $formats_ids;
	protected $date_from;
	protected $date_to;
	protected $activities_data;

	public function check()
	{
		$this->streams_ids = $this->get_request_ids("streams_ids");
		$this->cities_ids = $this->get_request_ids("cities_ids");
		$this->study_levels_ids = $this->get_request_ids("study_levels_ids");
		$this->groups_ids = $this->get_request_ids("groups_ids");
		$this->formats_ids = $this->get_request_ids("formats_ids");

		$this->date_from = strtotime(REQUEST("date_from"));
		$this->date_to = strtotime(REQUEST("date_to"));

		return true;
	}

	public function get_data()
	{
		$select_sql = new select_sql();
		$select_sql->set_DISTINCT_modifier();
		$select_sql->add_select_fields("a.id");
		$select_sql->add_from("activity a");

		if (strlen($this->cities_ids) || $this->date_from || $this->date_to)
		{
			$select_sql->add_join("LEFT JOIN activity_city_link acl ON acl.activity_id = a.id");
		}
		if (strlen($this->cities_ids))
		{
			$select_sql->add_where("city_id IN ({$this->cities_ids})");
		}
		if ($this->date_from)
		{
			$select_sql->add_select_fields("IF(a.is_date_common = 1, a.start_time, acl.start_time) as composite_start_time");
			$select_sql->add_having("composite_start_time >= FROM_UNIXTIME({$this->date_from})");
		}
		if ($this->date_to)
		{
			$select_sql->add_select_fields("IF(a.is_date_common = 1, a.finish_time, acl.finish_time) as composite_finish_time");
			$select_sql->add_having("composite_finish_time < FROM_UNIXTIME({$this->date_to})");
		}
		if (strlen($this->streams_ids))
		{
			$select_sql->add_join("LEFT JOIN activity_stream_link adl ON adl.activity_id = a.id");
			$select_sql->add_where("stream_id IN ({$this->streams_ids})");
		}
		if (strlen($this->groups_ids))
		{
			$select_sql->add_join("LEFT JOIN activity_group_link agl ON agl.activity_id = a.id");
			$select_sql->add_where("group_id IN ({$this->groups_ids})");
		}
		if (strlen($this->formats_ids))
		{
			$select_sql->add_join("LEFT JOIN activity_city_link acl ON acl.activity_id = a.id");
			$select_sql->add_where("city_id IN ({$this->cities_ids})");
		}

		$this->activities_data = array_unique($this->db->fetch_column_values($select_sql->get_sql(), "id"));

		return $this->activities_data;
	}

}

?>