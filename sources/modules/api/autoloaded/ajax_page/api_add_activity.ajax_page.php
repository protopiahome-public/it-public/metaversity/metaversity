<?php

class api_add_activity_ajax_page extends base_api_dt_add_ajax_ctrl
{
	protected $stream_id;
	protected $dt_name = "activity";
	protected $axis_name = "edit";

	/**
	 * @var activity_dt
	 */
	protected $dt;

	public function on_before_start()
	{
		if (!$this->check_input_id("stream"))
		{
			return false;
		}

		return parent::on_before_start();
	}

	protected function on_after_dt_init()
	{
		$stream_obj = stream_obj::instance($this->stream_id);
		$this->dt->set_stream_obj($stream_obj);
		parent::on_after_dt_init();

		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["stream_id"] = $this->stream_id;
		return true;
	}

}

?>