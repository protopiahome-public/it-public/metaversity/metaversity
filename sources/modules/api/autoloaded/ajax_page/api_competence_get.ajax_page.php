<?php

// @todo rename other like this
class api_competence_get_ajax_page extends base_api_ajax_ctrl
{
	protected $competence_id;
	protected $competence_data;

	public function check()
	{
		$this->competence_id = REQUEST("id");
		if (!is_good_id($this->competence_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->competence_data = $this->db->get_row("SELECT id, title FROM competence_full WHERE is_deleted = 0 AND id = {$this->competence_id}");
		if (!$this->competence_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		return true;
	}

	public function get_data()
	{
		return $this->competence_data;
	}

}

?>