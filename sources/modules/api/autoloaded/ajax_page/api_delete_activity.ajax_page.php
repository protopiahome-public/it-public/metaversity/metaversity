<?php

class api_delete_activity_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $activity_data;

	public function check()
	{
		$this->activity_id = REQUEST("id");
		if (!is_good_id($this->activity_id))
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		$this->activity_data = $this->db->get_row("
			SELECT id
			FROM activity
			WHERE id = {$this->activity_id}
		");
		if (!$this->activity_data)
		{
			$this->ajax_loader->write_error("WRONG_ID");
			return false;
		}

		return true;
	}

	public function commit()
	{
		$this->db->sql("
			DELETE FROM activity
			WHERE id = {$this->activity_id}
		");

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

	public function clean_cache()
	{
		activity_cache_tag::init($this->activity_id)->update();
		activities_cache_tag::init()->update();
	}

}

?>