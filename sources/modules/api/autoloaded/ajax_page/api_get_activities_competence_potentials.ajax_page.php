<?php

class api_get_activities_competence_potentials_ajax_page extends base_api_ajax_ctrl
{
	protected $stream_id;
	protected $competence_id;

	public function check()
	{
		if (!$this->check_input_id("stream"))
		{
			return false;
		}
		if (!$this->check_input_id("competence", function(db $db, $id)
			{
				return $db->row_exists("SELECT id FROM competence_full WHERE id = {$id} AND is_deleted = 0");
			}))
		{
			return false;
		}
		return true;
	}

	public function get_data()
	{
		$activities_data = $this->db->fetch_all("
			SELECT a.id, a.format_id
			FROM activity a
			LEFT JOIN activity_stream_link adl ON adl.activity_id = a.id
			WHERE adl.stream_id = {$this->stream_id}
		");

		activity_helper::fill_potential_by_competence($activities_data, $this->competence_id);
		$activities_data = array_filter($activities_data, function($value)
		{
			return $value["potential"] > 0;
		});

		return $activities_data;
	}

}

?>