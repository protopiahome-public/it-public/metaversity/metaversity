<?php

class api_activity_add_stream_ajax_page extends base_api_ajax_ctrl
{
	protected $activity_id;
	protected $stream_id;
	protected $study_level_id;
	protected $activity_data;

	public function check()
	{
		$this->activity_id = REQUEST("activity_id");
		if (!is_good_id($this->activity_id))
		{
			$this->ajax_loader->write_error("WRONG_ACTIVITY_ID");
			return false;
		}

		$this->stream_id = REQUEST("stream_id");
		if (!is_good_id($this->stream_id))
		{
			$this->ajax_loader->write_error("WRONG_STREAM_ID");
			return false;
		}

		$this->study_level_id = REQUEST("study_level_id");
		if ($this->study_level_id)
		{
			if (!is_good_id($this->study_level_id))
			{
				$this->ajax_loader->write_error("WRONG_STUDY_LEVEL_ID");
				return false;
			}
		}

		$activity_exists = $this->db->row_exists("
			SELECT id
			FROM activity
			WHERE id = {$this->activity_id}
		");
		if (!$activity_exists)
		{
			$this->ajax_loader->write_error("WRONG_ACTIVITY_ID");
			return false;
		}

		$stream_exists = $this->db->row_exists("
			SELECT id
			FROM `stream`
			WHERE id = {$this->stream_id}
		");
		if (!$stream_exists)
		{
			$this->ajax_loader->write_error("WRONG_STREAM_ID");
			return false;
		}

		if ($this->study_level_id)
		{
			$study_level_exists = $this->db->row_exists("
				SELECT id
				FROM `study_level`
				WHERE id = {$this->study_level_id} AND stream_id = {$this->stream_id}
			");
			if (!$study_level_exists)
			{
				$this->ajax_loader->write_error("WRONG_STUDY_LEVEL_ID");
				return false;
			}
		}

		return true;
	}

	public function commit()
	{

		if ($this->study_level_id)
		{
			$this->db->sql("
				INSERT IGNORE activity_stream_link (activity_id, stream_id, study_level_id)
				VALUES ({$this->activity_id}, {$this->stream_id}, {$this->study_level_id})
			");
		}
		else
		{
			$this->db->sql("
				INSERT IGNORE activity_stream_link (activity_id, stream_id)
				VALUES ({$this->activity_id}, {$this->stream_id})
			");
		}

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

	public function clean_cache()
	{
		activity_cache_tag::init($this->activity_id)->update();
	}

}

?>