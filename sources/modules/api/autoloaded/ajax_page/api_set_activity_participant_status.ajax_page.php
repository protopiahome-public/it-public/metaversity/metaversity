<?php

class api_set_activity_participant_status_ajax_page extends base_api_ajax_ctrl
{
	protected $user_id;
	protected $activity_id;
	protected $city_id;
	protected $role_id;
	protected $status;

	

	public function check()
	{
		if (!$this->check_input_id("activity"))
		{
			return false;
		}
		if (!$this->check_input_id("city"))
		{
			return false;
		}
		if (!$this->check_input_id("role"))
		{
			return false;
		}
		if (!$this->check_input_id("user"))
		{
			return false;
		}

		$this->status = REQUEST("status");
		if (!in_array($this->status, array("declined", "premoderation", "accepted", "deleted")))
		{
			$this->ajax_loader->write_error("WRONG_STATUS");
			return false;
		}

		return true;
	}

	public function commit()
	{
		activity_participants_helper::change_participant_status($this->activity_id, $this->role_id, $this->user_id, $this->city_id, $this->status, true);

		return true;
	}

	public function get_data()
	{
		return array("status" => "OK");
	}

}

?>