<?php

class api_get_user_top_positions_ajax_page extends base_api_ajax_ctrl
{
	protected $stream_id;
	protected $user_id;
	protected $limit;

	public function check()
	{
		if (!$this->check_input_id("stream"))
		{
			return false;
		}
		if (!$this->check_input_id("user"))
		{
			return false;
		}
		$this->limit = REQUEST("limit");
		if ($this->limit && !is_good_id($this->limit))
		{
			$this->ajax_loader->write_error("BAD_LIMIT");
			return false;
		}

		return true;
	}

	public function get_data()
	{
		$stream_obj = stream_obj::instance($this->stream_id);
		$data = stream_results_helper::get_user_results($stream_obj, $this->user_id);
		if ($this->limit)
		{
			$data = array_slice($data, 0, $this->limit);
		}
		$data = array("results" => $data);

		return $data;
	}

}

?>