<?php

class activity_participants_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
		array(
			"column" => "changer_user_id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	protected $xml_row_name = "user";
	protected $xml_attrs = array("stream_id", "activity_id");
	// Internal
	protected $stream_id;

	/**
	 * @var stream_obj
	 */
	protected $stream_obj;
	protected $activity_id;
	protected $activity_row;

	public function __construct(stream_obj $stream_obj, $activity_id)
	{
		$this->stream_obj = $stream_obj;
		$this->stream_id = $this->stream_obj->get_id();
		$this->activity_id = $activity_id;
		$this->activity_row = activity_helper::get_row($this->activity_id);
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if (!$this->activity_row || $this->activity_row["stream_id"] != $this->stream_id)
		{
			$this->set_error_404();
			return;
		}
		$this->data = activity_participants_helper::fetch_participants($this->activity_id);
		activity_marks_access_helper::filter_stream_marks($this->data, $this->stream_id);
	}

}

?>