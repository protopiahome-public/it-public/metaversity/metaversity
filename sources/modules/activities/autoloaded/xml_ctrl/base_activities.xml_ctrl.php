<?php

class base_activities_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_attrs = array("stream_id", "my", "is_archive", "is_actual", "is_no_date", "year", "month", "day", "date");
	protected $xml_row_name = "activity";
	// Internal
	protected $stream_id = null;
	protected $admin_mode = null;
	protected $my = false;
	protected $is_archive = false;
	protected $is_no_date = false;
	protected $year = null;
	protected $month = null;
	protected $day = null;
	protected $date = null;
	protected $filters;

	public function __construct($params, $stream_id = null, $admin_mode = false)
	{
		$this->page = isset($params["page"]) ? $params["page"] : 1;
		parent::__construct();

		$this->stream_id = $stream_id;
		$this->admin_mode = $admin_mode;

		$this->my = isset($params["my"]) && $params["my"];
		if ($this->my and ! $this->user->get_user_id())
		{
			$this->set_error_404();
		}

		if (isset($params["is_archive"]) && $params["is_archive"])
		{
			$this->is_archive = true;
		}
		elseif (isset($params["is_no_date"]) && $params["is_no_date"])
		{
			$this->is_no_date = true;
		}
		elseif (isset($params["year"]))
		{
			$this->year = $params["year"];
			if (isset($params["month"]))
			{
				$this->month = $params["month"];
				if (isset($params["day"]))
				{
					$this->day = $params["day"];
					$this->date = $this->year . "-" . str_pad($this->month, 2, "0", STR_PAD_LEFT) . "-" . str_pad($this->day, 2, "0", STR_PAD_LEFT);
				}
			}
		}
	}

	public function init()
	{
		$this->add_easy_processor(new pager_full_fetch_nobr_easy_processor($this->page, 25, "metaactivity_id"));

		$processor = new sql_filters_easy_processor();
		$this->filters = activities_fetch_helper::init_sql_filters($processor, $this->lang, $this->my, $this->stream_id, $this->user->get_user_id(), $this->user->get_city_id());
		if ($this->is_archive and ! $this->admin_mode)
		{
			$processor->add_sql_filter(new show_flag_sql_filter("is-archive", "__CALLBACK__", "__CALLBACK__"));
		}
		elseif ($this->year)
		{
			$processor->add_sql_filter(new show_flag_sql_filter("date-interval", "__CALLBACK__", "__CALLBACK__"));
		}
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$this->prepare_sql($select_sql);
		$this->data = $this->fetch_data($select_sql);
	}

	protected function prepare_sql(select_sql $select_sql)
	{
		if ($this->is_archive)
		{
			$select_sql->add_where("a.finish_time < NOW()");
			$select_sql->add_order("a.start_time DESC");
		}
		elseif ($this->year)
		{
			if (!$this->month)
			{
				$min_time = mktime(0, 0, 0, 1, 1, $this->year);
				$max_time = mktime(23, 59, 59, 12, 31, $this->year);
			}
			elseif (!$this->day)
			{
				$min_time = mktime(0, 0, 0, $this->month, 1, $this->year);
				$max_time = mktime(23, 59, 59, $this->month, date("t", $min_time), $this->year);
			}
			else
			{
				$min_time = mktime(0, 0, 0, $this->month, $this->day, $this->year);
				$max_time = mktime(23, 59, 59, $this->month, $this->day, $this->year);
			}
			$select_sql->add_where("a.start_time >= '" . $this->db->get_datetime($min_time) . "' AND a.start_time <= '" . $this->db->get_datetime($max_time) . "'");
			$select_sql->add_order("a.start_time");
		}
		elseif ($this->is_no_date)
		{
			$select_sql->add_where("a.start_time > '2038-01-01'");
			$select_sql->add_order("a.activity_id DESC");
		}
		else
		{
			$select_sql->add_where("a.finish_time > NOW()");
			if ($this->admin_mode || !$this->stream_id && !$this->my)
			{
				$select_sql->add_where("a.finish_time < '2038-01-01'");
			}
			$select_sql->add_order("a.start_time");
		}
	}

	protected function fetch_data(select_sql $select_sql)
	{
		return activities_fetch_helper::load_data($select_sql, $this->user->get_user_id(), $this->my, $this->filters, $this->stream_id);
	}

	protected function postprocess_data()
	{
		activities_fetch_helper::load_xml_ctrls($this->data, $this->xml_loader);
		$this->load_calendar_xml_ctrl();
	}

	protected function load_calendar_xml_ctrl()
	{
		$year = false;
		$month = false;
		if ($this->month)
		{
			$year = $this->year;
			$month = $this->month;
		}
		elseif (sizeof($this->data))
		{
			$row = reset($this->data);
			$start_time = $this->db->parse_datetime($row["start_time"]);
			$year = (int) date("Y", $start_time);
			$month = (int) date("n", $start_time);
			if (!$this->is_archive and ! $this->year and ! $this->request->get_params_string())
			{
				$current_year = (int) date("Y");
				$current_month = (int) date("n");
				if ($year < $current_year or $year === $current_year and $month < $current_month)
				{
					$year = false;
					$month = false;
				}
			}
		}
		if ($year >= 2038)
		{
			$year = false;
			$month = false;
		}
		$this->xml_loader->add_xml(new activities_calendar_xml_ctrl($this->my, $this->stream_id, $year, $month));
	}

}

?>