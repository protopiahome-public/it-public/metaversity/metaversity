<?php

class activities_calendar_xml_ctrl extends base_calendar_xml_ctrl
{

	protected $my;
	protected $stream_id;
	protected $filters;

	public function __construct($my, $stream_id, $year = false, $month = false)
	{
		$this->my = $my;
		$this->stream_id = $stream_id;
		parent::__construct($year, $month);
	}

	public function init()
	{
		$processor = new sql_filters_easy_processor();
		$this->filters = activities_fetch_helper::init_sql_filters($processor, $this->lang, $this->my, $this->stream_id, $this->user->get_user_id(), $this->user->get_city_id());
		$this->add_easy_processor($processor);
	}

	protected function get_cache()
	{
		return false;
	}

	protected function get_calendar_data(select_sql $select_sql)
	{
		$prev_date_sql = $this->prev_year . "-" . str_pad($this->prev_month, 2, "0", STR_PAD_LEFT) . "-23 00:00:00";
		$next_date_sql = $this->next_year . "-" . str_pad($this->next_month, 2, "0", STR_PAD_LEFT) . "-07 00:00:00";

		$select_sql->add_where("a.start_time >= '{$prev_date_sql}'");
		$select_sql->add_where("a.start_time < '{$next_date_sql}'");

		$activities = activities_fetch_helper::load_data($select_sql, $this->user->get_user_id(), $this->my, $this->filters, $this->stream_id);
		$this->competence_data = potential_math_competence_helper::get_competence_data(GET("competence"));
		if ($this->competence_data)
		{
			$required_results = array($this->competence_data["id"] => 3);
			$potential_stream_obj = stream_obj::instance($this->competence_data["stream_id"]);
			potential_math_activity_helper::fill_activities_potential($potential_stream_obj, $activities, $required_results, true);
			activities_fetch_helper::remove_zero_potential_activities($activities);
		}
		return activities_fetch_helper::get_data_for_calendar($activities);
	}

}

?>