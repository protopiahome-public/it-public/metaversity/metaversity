<?php

class activity_calc_helper extends base_static_db_helper
{

	public static function rebuild_activity_calc($activity_id)
	{
		self::db()->sql("
			DELETE FROM activity_calc
			WHERE activity_id = {$activity_id}
		");

		self::db()->sql("
			REPLACE INTO activity_calc
				(activity_id, stream_id, format_id, city_id, metaactivity_id, date_type, start_time, finish_time, study_level_id, groups, streams,
				accepted_participant_count_calc, mark_count_calc, longterm_graded_user_count_calc, longterm_mark_count_calc)
			SELECT
				id, stream_id, format_id, city_id, metaactivity_id,
				date_type, 
				IF(start_time = '0000-00-00', '2038-01-15 12:00:00', start_time),
				IF(finish_time = '0000-00-00', IF(start_time = '0000-00-00', '2038-01-15 12:00:00', start_time), finish_time),
				study_level_id, NULL, NULL,
				accepted_participant_count_calc, mark_count_calc, longterm_graded_user_count_calc, longterm_mark_count_calc
			FROM activity
			WHERE id = {$activity_id}
		");

		self::db()->sql("SET group_concat_max_len = 512 * 1024");

		self::db()->sql("
			UPDATE activity_calc a
			LEFT JOIN (
				SELECT l.activity_id, GROUP_CONCAT(l.group_id ORDER BY l.group_id SEPARATOR ',') AS groups
				FROM activity_group_link l
				GROUP BY l.activity_id
			) x ON a.activity_id = x.activity_id
			SET a.groups = x.groups
			WHERE a.activity_id = {$activity_id}
		");

		self::db()->sql("
			UPDATE activity_calc a
			LEFT JOIN (
				SELECT l.activity_id, GROUP_CONCAT(l.stream_id ORDER BY l.stream_id SEPARATOR ',') AS streams
				FROM activity_stream_crosspost l
				GROUP BY l.activity_id
			) x ON a.activity_id = x.activity_id
			SET a.streams = x.streams
			WHERE a.activity_id = {$activity_id}
		");
	}

}

?>