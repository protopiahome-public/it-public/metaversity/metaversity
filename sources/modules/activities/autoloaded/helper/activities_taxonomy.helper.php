<?php

class activities_taxonomy_helper
{

	const MOD_MY = 1;
	const MOD_NO_DATE = 2;

	public static function parse_request_params($p, $idx, $mods)
	{
		$ok = false;

		$result = array(
			"page" => 1,
		);

		if (self::MOD_MY & $mods and $p[$idx] === "my")
		{
			$result["my"] = true;
			$idx += 1;
		}

		if ($result["page"] = self::is_page_folder($p[$idx]) and $p[$idx + 1] === null)
		{
			//activities/[page-<page>/]
			$ok = true;
		}
		elseif ($p[$idx] === "archive" and $result["page"] = self::is_page_folder($p[$idx + 1]) and $p[$idx + 2] === null)
		{
			//activities/archive/[page-<page>/]
			$ok = true;
			$result["is_archive"] = true;
		}
		elseif (self::MOD_NO_DATE & $mods and $p[$idx] === "no-date" and $result["page"] = self::is_page_folder($p[$idx + 1]) and $p[$idx + 2] === null)
		{
			//activities/no-date/[page-<page>/]
			$ok = true;
			$result["is_no_date"] = true;
		}
		elseif ($p[$idx] === "calendar" and is_good_id($p[$idx + 1]) and $result["page"] = self::is_page_folder($p[$idx + 2]) and $p[$idx + 3] === null)
		{
			//activities/calendar/<year>/[page-<page>/]
			$ok = true;
			$result["year"] = $p[$idx + 1];
		}
		elseif ($p[$idx] === "calendar" and is_good_id($p[$idx + 1]) and is_good_id($p[$idx + 2]) and $result["page"] = self::is_page_folder($p[$idx + 3]) and $p[$idx + 4] === null)
		{
			//activities/calendar/<year>/<month>/[page-<page>/]
			$ok = true;
			$result["year"] = $p[$idx + 1];
			$result["month"] = $p[$idx + 2];
		}
		elseif ($p[$idx] === "calendar" and is_good_id($p[$idx + 1]) and is_good_id($p[$idx + 2]) and is_good_id($p[$idx + 3]) and $result["page"] = self::is_page_folder($p[$idx + 4]) and $p[$idx + 5] === null)
		{
			//activities/calendar/<year>/<month>/<day>/[page-<page>/]
			$ok = true;
			$result["year"] = $p[$idx + 1];
			$result["month"] = $p[$idx + 2];
			$result["day"] = $p[$idx + 3];
		}

		$ok = $ok && self::check_date_restrictions($result);

		return $ok ? $result : false;
	}

	private static function check_date_restrictions($result)
	{
		if (isset($result["year"]))
		{
			if (!is_good_id($result["year"]) || $result["year"] <= 1970 || $result["year"] >= 2038)
			{
				return false;
			}
			if (isset($result["month"]))
			{
				if (!is_good_id($result["month"]) || $result["month"] > 12)
				{
					return false;
				}
				if (isset($result["day"]))
				{
					if (!is_good_id($result["day"]) || $result["day"] > 31)
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	private static function is_page_folder($folder_param)
	{
		if ($folder_param === null)
		{
			return 1;
		}
		elseif (substr($folder_param, 0, 5) == "page-" && is_good_id($page = substr($folder_param, 5)))
		{
			return $page;
		}
		else
		{
			return false;
		}
	}

}

?>