<?php

class activities_fetch_helper extends base_static_db_helper
{

	public static function init_sql_filters(sql_filters_easy_processor $processor, lang $lang, $my, $stream_id, $user_id, $default_city_id)
	{
		$filters = array();

		$filters["search"] = new text_sql_filter("search", trans("Title"), array("activity.title"), trans("Search by title"));
		$processor->add_sql_filter($filters["search"]);

		if ($default_city_id and ! self::db()->row_exists("SELECT id FROM city WHERE id = {$default_city_id} AND is_fake = 0"))
		{
			$default_city_id = null;
		}
		$filters["city"] = new foreign_key_sql_filter("city", trans("City"), "a.city_id", "city", $lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0");
		$filters["city"]->set_always_select_unkeyed(true);
		if ($default_city_id && !$my)
		{
			$filters["city"]->set_default_value($default_city_id);
		}
		$filters["city"]->set_inactive_title(trans("Any city"));
		$processor->add_sql_filter($filters["city"]);

		if ($stream_id)
		{
			if ($filters["city"]->get_value())
			{
				$filters["groups_data"] = self::db()->fetch_all("
					SELECT id, study_level_id, title
					FROM `group`
					WHERE stream_id = {$stream_id} and city_id = {$filters["city"]->get_value()}
				", "id");
				$options = array();
				foreach ($filters["groups_data"] as $group_id => $group_data)
				{
					$options[$group_id] = $group_data["title"];
				}
				if ($options)
				{
					$filters["group"] = new select_sql_filter("group", trans("Group"), $options);
					$filters["group"]->set_inactive_title(trans("Any", "FEMALE"));
					if ($user_id)
					{
						$user_group_info = user_stream_status_helper::get_user_group_info($user_id, $stream_id);
						if ($user_group_info["group_id"])
						{
							$filters["group"]->set_default_value($user_group_info["group_id"]);
						}
					}
					$processor->add_sql_filter($filters["group"]);
				}
			}

			$filters["format"] = new foreign_key_sql_filter("format", trans("Format"), "a.format_id", "format", "title", "id", "stream_id = {$stream_id} AND is_archived = 0", "position");
			$filters["format"]->set_groups_data("format_group", "format_group_id", "id", "title", "position", "parent_id", "stream_id = {$stream_id}");
			$processor->add_sql_filter($filters["format"]);

			$filters["subject"] = new multi_link_sql_filter("subject", trans("Subject"), "a.activity_id", "activity_subject_link", "activity_id", "subject_id", "subject", "title", "id", "stream_id = {$stream_id}");
			$processor->add_sql_filter($filters["subject"]);
		}

		return $filters;
	}

	public static function load_data(select_sql $select_sql, $user_id, $my, $filters, $stream_id, $load_admin_stat = false)
	{
		$select_sql->add_select_fields("a.*");
		$select_sql->add_from("activity_calc a");

		if ($user_id)
		{
			$select_sql->add_join("LEFT JOIN activity_participant_status_calc aps ON aps.activity_id = a.activity_id AND aps.user_id = {$user_id}");
			$select_sql->add_select_fields("aps.status");
			if ($my)
			{
				$select_sql->add_where("aps.status IS NOT NULL");
			}
		}

		$search_filter = $filters["search"];
		/* @var $search_filter text_sql_filter */
		if ($load_admin_stat || $search_filter->is_active())
		{
			$select_sql->add_join("JOIN activity ON activity.id = a.activity_id");
		}

		if ($load_admin_stat)
		{
			$select_sql->add_select_fields("activity.premoderation_request_count_calc");
			$select_sql->add_select_fields("activity.unset_mark_count_calc");
		}

		$data = self::db()->fetch_all($select_sql->get_sql());

		self::prepare($data);
		self::clean_data($data);
		if ($stream_id)
		{

			$group_id = null;
			$study_level_id = null;
			if (isset($filters["group"]))
			{
				/* @var $filters["group"] select_sql_filter */
				$group_id = $filters["group"]->get_value();
				if (isset($filters["groups_data"][$group_id]))
				{
					$study_level_id = $filters["groups_data"][$group_id]["study_level_id"] ? : null;
				}
				else
				{
					$group_id = null;
				}
			}

			$user_streams = array(
				$stream_id => array(
					"stream_id" => $stream_id,
					"study_level_id" => $study_level_id,
					"group_id" => $group_id,
				),
			);
		}
		else
		{
			$user_streams = user_stream_status_helper::get_user_streams($user_id);
		}
		self::filter_by_user_stream_statuses($data, $user_streams);
		$data = self::get_data_for_xml($data);
		$data = self::group_metaactivities($data);
		return $data;
	}

	public static function get_data_for_calendar($activities)
	{
		$data = array();
		foreach ($activities as $activity)
		{
			$start_time = self::db()->parse_datetime($activity["start_time"]);
			$date = date("n|j", $start_time);
			if (!isset($data[$date]))
			{
				$data[$date] = array(
					"day" => date("j", $start_time),
					"date" => $date,
					"count" => 0,
				);
			}
			++$data[$date]["count"];
		}
		return $data;
	}

	public static function remove_zero_potential_activities(&$activities)
	{
		foreach ($activities as $idx => &$activity)
		{
			if ($activity["potential"] == "0")
			{
				unset($activities[$idx]);
			}
		}
		unset($activity);
	}

	public static function load_xml_ctrls($activities, xml_loader $xml_loader)
	{
		$stream_ids = array();
		foreach ($activities as $activity)
		{
			$stream_ids[$activity["stream_id"]] = true;
			$xml_loader->add_xml_by_class_name("activity_short_xml_ctrl", $activity["id"], $activity["stream_id"]);
		}
		foreach ($stream_ids as $stream_id => $true)
		{
			$xml_loader->add_xml_by_class_name("stream_short_xml_ctrl", $stream_id);
			$xml_loader->add_xml(new stream_access_xml_ctrl(stream_obj::instance($stream_id)->get_access()));
		}
	}

	private static function prepare(&$activities)
	{
		foreach ($activities as &$activity)
		{
			$activity["groups"] = explode_to_ints(",", $activity["groups"]);
			$activity["streams"] = explode_to_ints(",", $activity["streams"]);
		}
		unset($activity);
	}

	private static function clean_data(&$activities)
	{
		$cl = levels_and_groups_helper::get_streams_check_list();
		foreach ($activities as &$activity)
		{
			foreach ($activity["streams"] as $idx => $stream_id)
			{
				if (!isset($cl[$stream_id]))
				{
					unset($activity["streams"][$idx]);
				}
			}
			$stream_id = (int) $activity["stream_id"];
			$study_level_id = (int) $activity["study_level_id"];
			$city_id = (int) $activity["city_id"];
			foreach ($activity["groups"] as $idx => $group_id)
			{
				if (!isset($cl[$stream_id]["g"][$group_id])
					or $cl[$stream_id]["g"][$group_id][0] and $study_level_id and $cl[$stream_id]["g"][$group_id][0] !== $study_level_id
					or $cl[$stream_id]["g"][$group_id][1] and $city_id and $cl[$stream_id]["g"][$group_id][1] !== $city_id
				)
				{
					unset($activity["groups"][$idx]);
				}
			}
		}
		unset($activity);
	}

	private static function filter_by_user_stream_statuses(&$activities, $user_streams)
	{
		foreach ($activities as $idx => $activity)
		{
			$stream_id = (int) $activity["stream_id"];
			if (isset($user_streams[$stream_id]))
			{
				$activity_study_level_id = (int) $activity["study_level_id"];
				$user_study_level_id = (int) $user_streams[$stream_id]["study_level_id"];
				if (!$activity_study_level_id or ! $user_study_level_id or $activity_study_level_id === $user_study_level_id)
				{
					$activity_group_ids = $activity["groups"];
					$user_group_id = (int) $user_streams[$stream_id]["group_id"];
					if (!sizeof($activity_group_ids) or ! $user_group_id or in_array($user_group_id, $activity_group_ids))
					{
						continue;
					}
				}
			}
			elseif (any_in_keys($activity["streams"], $user_streams))
			{
				continue;
			}
			unset($activities[$idx]);
		}
	}

	private static function get_data_for_xml($activities)
	{
		$result = array();
		foreach ($activities as $activity)
		{
			$row1 = array(
				"id" => $activity["activity_id"],
				"start_time" => $activity["start_time"],
				"stream_id" => $activity["stream_id"],
				"format_id" => $activity["format_id"],
				"metaactivity_id" => $activity["metaactivity_id"],
				"date_type" => $activity["date_type"],
				"accepted_participant_count_calc" => $activity["accepted_participant_count_calc"],
				"mark_count_calc" => $activity["mark_count_calc"],
				"longterm_graded_user_count_calc" => $activity["longterm_graded_user_count_calc"],
				"longterm_mark_count_calc" => $activity["longterm_mark_count_calc"],
			);
			if (isset($activity["status"]))
			{
				$row1["status"] = $activity["status"];
			}
			if (isset($activity["premoderation_request_count_calc"]))
			{
				$row1["premoderation_request_count_calc"] = $activity["premoderation_request_count_calc"];
			}
			if (isset($activity["unset_mark_count_calc"]))
			{
				$row1["unset_mark_count_calc"] = $activity["unset_mark_count_calc"];
			}
			$result[] = $row1;
		}
		return $result;
	}

	private static function group_metaactivities($activities)
	{
		$result = array();
		$index = array();
		$i = 0;
		foreach ($activities as $row)
		{
			$metaactivity_id = $row["metaactivity_id"];
			if (!$metaactivity_id)
			{
				$result[$i] = $row;
			}
			elseif (!isset($index[$metaactivity_id]))
			{
				$result[$i] = $row;
				$index[$metaactivity_id] = $i + 1;
			}
			else
			{
				$insert_i = $index[$metaactivity_id];
				array_splice($result, $insert_i, 0, array($row));
				foreach ($index as &$_i)
				{
					if ($_i >= $insert_i)
					{
						++$_i;
					}
				}
				unset($_i);
				++$i;
			}
			++$i;
		}
		return $result;
	}

}

?>