<?php

class activities_calendar_ajax_page extends base_ajax_ctrl
{

	public function get_data()
	{
		$year = POST("year");
		$month = POST("month");
		$stream_id = is_good_id(POST("stream_id")) ? (int) POST("stream_id") : null;
		$stream_id_str = $stream_id ? : "";
		$my = "1" == POST("my");
		$my_str = $my ? "1" : "0";
		if (!is_good_id($year) || !is_good_id($month))
		{
			$this->set_error_404();
			return array(
				"status" => "error",
				"error" => "input"
			);
		}
		require_once PATH_CORE . "/loader/xml_loader.php";
		$xml_loader = new xml_loader();
		$xml_loader->add_xml(new user_xml_ctrl());
		$xml_loader->add_xml(new request_xml_ctrl());
		if ($stream_id)
		{
			$xml_loader->add_xml(new stream_current_xml_ctrl($stream_id));
		}

		$xdom = xdom::create("activities");
		$xdom->set_attr("stream_id", $stream_id_str);
		$xdom->set_attr("my", $my_str);
		$xdom->set_attr("base_url", POST("activities_base_url"));

		if (is_good_id(POST("selected_year")) and is_good_id(POST("selected_month")) and is_good_id(POST("selected_day")))
		{
			$selected_year = (int) POST("selected_year");
			$selected_month = (int) POST("selected_month");
			$selected_day = (int) POST("selected_day");
			$selected_date = $selected_year . "-" . str_pad($selected_month, 2, "0", STR_PAD_LEFT) . "-" . str_pad($selected_day, 2, "0", STR_PAD_LEFT);
			$xdom->set_attr("year", $selected_year);
			$xdom->set_attr("month", $selected_month);
			$xdom->set_attr("day", $selected_day);
			$xdom->set_attr("date", $selected_date);
			$xml_loader->add_xml(new string_to_xml_xml_ctrl($xdom->get_xml(true)));
		}
		else
		{
			$xml_loader->add_xml(new string_to_xml_xml_ctrl($xdom->get_xml(true)));
		}

		$xml_loader->add_xml(new activities_calendar_xml_ctrl($my, $stream_id, $year, $month));
		$xml_loader->add_xslt("ajax/activities_calendar.ajax", "activities");
		$xml_loader->run();
		return array(
			"status" => "OK",
			"month" => $month,
			"year" => $year,
			"html" => $xml_loader->get_content(),
		);
	}

}

?>