<?php

class activities_xml_page extends base_activities_xml_ctrl
{

	protected $competence_data;
	protected $potential_stream_id;

	protected function load_data(select_sql $select_sql = null)
	{
		parent::load_data($select_sql);

		$this->competence_data = potential_math_competence_helper::get_competence_data(GET("competence"));
		if ($this->competence_data)
		{
			$required_results = array($this->competence_data["id"] => 3);
			$potential_stream_obj = stream_obj::instance($this->competence_data["stream_id"]);
			potential_math_activity_helper::fill_activities_potential($potential_stream_obj, $this->data, $required_results, true);
			activities_fetch_helper::remove_zero_potential_activities($this->data);
		}
	}

	protected function postprocess_data()
	{
		parent::postprocess_data();

		if (!$this->competence_data and $this->user->get_user_id())
		{
			$potential_math_user_streams_xml_ctrl = new potential_math_user_streams_xml_ctrl($this->user->get_user_id(), array(
				$this->stream_id ? GET("potential-stream") : $this->user->get_user_param("potential_stream_id"),
				), $this->stream_id);
			$this->xml_loader->add_xml($potential_math_user_streams_xml_ctrl);
			$this->potential_stream_id = $potential_math_user_streams_xml_ctrl->get_potential_stream_id();
			if ($this->potential_stream_id)
			{
				$potential_stream_obj = stream_obj::instance($this->potential_stream_id);
				$required_results = stream_user_positions_helper::get_stream_user_required_competence_results($this->potential_stream_id, $this->user->get_user_id(), $potential_stream_obj->get_competence_set_id());
				potential_math_activity_helper::fill_activities_potential($potential_stream_obj, $this->data, $required_results, true);
			}
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		parent::modify_xml($xdom);

		if ($this->competence_data)
		{
			$xdom->set_attr("competence_id", $this->competence_data["id"]);
			$this->db_xml_converter->build_xml($xdom->create_child_node("competence"), array($this->competence_data), true);
		}
		if ($this->potential_stream_id)
		{
			$xdom->set_attr("potential_stream_id", $this->potential_stream_id);
		}
	}

}

?>