<?php

/**
 * MUST be called after groups are rebuilt (i. e. after stream_admin_activity_save_groups_mixin)
 */
class activity_calc_mixin extends base_mixin
{

	protected $last_id;

	public function on_after_commit()
	{
		activity_calc_helper::rebuild_activity_calc($this->last_id);
	}

}

?>