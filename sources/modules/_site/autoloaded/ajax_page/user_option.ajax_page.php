<?php

require_once PATH_MODULE_SITE_LIB . "/text_tools.php";

class user_option_ajax_page extends base_ajax_ctrl
{
	
	protected $mixins = array(
		"user_option",
	);

	public function get_data()
	{
		return array("status" => "OK");
	}
	
}

?>