<?php

class city_dt extends base_dt
{

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(40);
		$dtf->set_importance(true);
		$dtf->set_unique(true, trans("This city has already been added."));
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("title_en", "Title (English)");
		$dtf->set_max_length(40);
		$dtf->set_importance(true);
		$dtf->set_unique(true, trans("This city has already been added."));
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("is_fake", trans("Not a city"));
		$dtf->set_default_value(false);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("full", array("title", "title_en", "is_fake"));
		$this->add_fields_in_axis("edit", array("title", "title_en", "is_fake"));
	}

}

?>