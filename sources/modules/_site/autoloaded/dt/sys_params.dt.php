<?php

class sys_params_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Installation Title"));
		$dtf->set_max_length(60);
		$dtf->set_importance(true);
		$dtf->set_comment(trans("The title should be short."));
		$this->add_field($dtf, "main");

//		$dtf = new image_pack_dtf("logo", trans("Logo"), PATH_TMP);
//		$dtf->add_image("main", "Main", 0, 0, "/data/logo/", null, PATH_PUBLIC_DATA . "/logo");
//		$dtf->get_image_by_name("main")->add_processor(new image_pack_crop_processor(150, 44));
//		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix());
//		$dtf->set_comment(trans("No bigger than 150x44 pixels."));
//		$this->add_field($dtf);

		$dtf = new email_dtf("info_email", trans("Email"));
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$dtf->set_comment(trans("Used in footer and in some other places."));
		$this->add_field($dtf, "main");

		$dtf = new int_dtf("copyright_start_year", trans("Installation launch year"));
		$dtf->set_importance(true);
		$dtf->set_default_value(date("Y"));
		$dtf->set_type(INTCMF_INT_DTF_TYPE_DWORD);
		$dtf->set_min_value(2007);
		$dtf->set_max_value(2030);
		$dtf->set_comment(trans("Shown in the footer."));
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("ga_tracking_id", trans("Google Analytics code"));
		$dtf->set_max_length(60);
		$dtf->set_importance(false);
		$dtf->set_regexp("/^UA-[1-9][0-9]*-[1-9][0-9]*$/", trans("Incorrect value. Something like UA-1234567-123 is expected."));
		$dtf->set_comment(trans("Format: UA-1234567-123. Leave blank if you do not want to use Google Analytics."));
		$this->add_field($dtf, "main");

		$dtf = new foreign_key_dtf("central_competence_set_id", trans("Central competence tree"), "competence_set");
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");
		
		$dtf = new foreign_key_dtf("default_import_city_id", trans("Default city for user import"), "city");
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("full", array("title", /* "logo", */ "info_email", "copyright_start_year", "ga_tracking_id"));
		$this->add_fields_in_axis("short", array("title", /* "logo", */ "info_email", "copyright_start_year", "ga_tracking_id"));
		$this->add_fields_in_axis("edit", array("title", /* "logo", */ "info_email", "copyright_start_year", "ga_tracking_id", "central_competence_set_id", "default_import_city_id"));
	}

}

?>