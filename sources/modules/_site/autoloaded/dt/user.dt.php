<?php

class user_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * @var lang
	 */
	protected $lang;

	/**
	 * @var user
	 */
	protected $user;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		global $lang;
		$this->lang = $lang;

		global $user;
		$this->user = $user;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));
		$this->add_block("contacts", trans("Contacts"));
		$this->add_block("other", trans("Other"));
		$this->add_block("notifications", trans("Notifications"));
		$this->add_block("access", trans("Access Control"));
		$this->add_block("password", trans("Password"));

		$dtf = new url_name_dtf("login", trans("Login"));
		$dtf->set_max_length(40);
		$dtf->set_url_attribute("url");
		$dtf->set_url_prefix($this->domain_logic->get_users_prefix(true) . "/");
		$dtf->set_deprecated_values(array(
			"www", "mail", "ns", "ns1", "ns2", "ns3", "ns4", // std. subdomains
			"admin", "god", "moderator", "editor", "user", "all", // roles
			"cp", "add", "create", "edit", "delete", "remove", // actions
		));
		$dtf->set_comment(trans("Allowed: english letters, digits and hyphen."));
		$this->add_field($dtf, "main");

		$dtf = new password_dtf("password", trans("Password"));
		$dtf->set_max_length(64);
		$dtf->set_min_length(5);
		$this->add_field($dtf, "password");

		$dtf = new string_dtf("first_name", trans("First name"));
		$dtf->set_max_length(60);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("mid_name", trans("Middle name / Patronimic"));
		$dtf->set_max_length(60);
		$this->add_field($dtf, "main");

		$dtf = new string_dtf("last_name", trans("Last name"));
		$dtf->set_max_length(60);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new foreign_key_dtf("city_id", trans("Your city", "STUDY CITY"), "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title");
		$dtf->set_importance(true);
		$dtf->set_comment(trans("Affects what the site shows you by default. Also shown under your name on many pages."));
		$this->add_field($dtf, "main");

		$dtf = new select_dtf("sex", trans("Sex"));
		$dtf->set_cases(array(
			"?" => trans("Not selected"),
			"m" => trans("Male"),
			"f" => trans("Female"),
		));
		$dtf->set_default_key("?");
		$this->add_field($dtf, "main");

		$dtf = new image_pack_dtf("photo", trans("Photo"), PATH_TMP);
		$dtf->add_image("large", "Large", 800, 600, "/data/user/large/", "/img/defaults/user-large-default.jpg", PATH_PUBLIC_DATA . "/user/large");
		$dtf->add_image("big", "Big", 80, 80, "/data/user/big/", "/img/defaults/user-big-default.jpg", PATH_PUBLIC_DATA . "/user/big");
		$dtf->add_image("mid", "Mid", 48, 48, "/data/user/mid/", "/img/defaults/user-mid-default.jpg", PATH_PUBLIC_DATA . "/user/mid");
		$dtf->add_image("small", "Small", 24, 24, "/data/user/small/", "/img/defaults/user-small-default.jpg", PATH_PUBLIC_DATA . "/user/small");
		$dtf->set_add_prefix_to_url($this->domain_logic->get_main_prefix(true));
		$dtf->set_comment(trans("Please upload your real photo."));
		$this->add_field($dtf, "main");

		$dtf = new email_dtf("email", trans("Email"));
		$dtf->set_max_length(100);
		$dtf->set_importance(true);
		$this->add_field($dtf, "contacts");

		$dtf = new url_string_dtf("www", trans("Website"));
		$dtf->set_human_view_column();
		$dtf->set_comment(trans("Start with http:// or https://"));
		$this->add_field($dtf, "contacts");

		$dtf = new string_dtf("phone", trans("Phone"));
		$dtf->set_max_length(40);
		$this->add_field($dtf, "contacts");

		$dtf = new string_dtf("skype", trans("Skype"));
		$dtf->set_max_length(100);
		$this->add_field($dtf, "contacts");

		$dtf = new string_dtf("icq", trans("ICQ"));
		$dtf->set_max_length(12);
		$this->add_field($dtf, "contacts");

		$dtf = new intmarkup_dtf("about", trans("About me"));
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "other");

		$dtf = new calc_dtf("mark_count_calc", trans("Mark count"), "mark_count_calc");
		$this->add_field($dtf);

		$dtf = new string_dtf("register_ip", "Register IP");
		$dtf->set_max_length(15);
		$this->add_field($dtf);

		$dtf = new bool_dtf("allow_email_on_reply", trans("Receive replies to my comments by email"));
		$dtf->set_default_value(true);
		$this->add_field($dtf, "notifications");

		$dtf = new bool_dtf("allow_email_on_premoderation", trans("Receive requests for membership in my streams by email"));
		$dtf->set_default_value(true);
		$dtf->set_comment(trans("Emails will be sent if you are an administrator and premoderation is activated."));
		$this->add_field($dtf, "notifications");

		$dtf = new select_dtf("results_access_level", trans("Who can access your results"));
		$dtf->set_drop_down_view(true);
		$dtf->set_cases(array(
			"all" => trans("Everybody"),
			"registered" => trans("Registered users"),
			"stream_students" => trans("Classmates + Moderators"),
			"any_moderator" => trans("Moderators"),
		));
		$dtf->set_default_key("all");
		if (is_object($this->user))
		{
			$profile_url = htmlspecialchars($this->domain_logic->get_users_prefix() . "/" . $this->user->get_login() . "/");
			$dtf->set_comment_html(trans_html("This option manages access to your <a href=\"%1%\">Results</a> and <a href=\"%2%\">Competences</a> (but not Ratings now – see help).", $profile_url . "results/", $profile_url . "competences/"));
		}
		$this->add_field($dtf, "access");
		
		$dtf = new select_dtf("marks_access_level", trans("Who can access your marks"));
		$dtf->set_drop_down_view(true);
		$dtf->set_cases(array(
			"all" => trans("Everybody"),
			"registered" => trans("Registered users"),
			"stream_students" => trans("Classmates + Moderators"),
			"any_moderator" => trans("Moderators"),
		));
		$dtf->set_default_key("all");
		if (is_object($this->user))
		{
			$url = $this->domain_logic->get_users_prefix() . "/" . $this->user->get_login() . "/log/";
			$dtf->set_comment_html(trans_html("This option manages access to your marks shown on the <a href=\"%1%\">Roles and Marks</a> page and inside activities.", $url));
		}
		$this->add_field($dtf, "access");

		$this->add_fields_in_axis("short", array(
			"login",
			"sex",
			//"profile_url",
			"photo",
			"city_id",
			//"personal_mark_count_calc", "group_mark_count_calc", "total_mark_count_calc",
		));

		$this->add_fields_in_axis("profile", array(
			"about",
			"email", "www", "phone", "skype",
			"mark_count_calc",
		));

		$this->add_fields_in_axis("show", array(
			"login",
			"email",
			// names are calculated in the "user_short" ctrl
			"city_id",
			"sex", "photo",
			"email", "www", "phone", "skype", //"icq",
			"about",
		));

		$this->add_fields_in_axis("register", array(
			"login",
			"password",
			"email",
			"first_name", "mid_name", "last_name",
			"city_id",
			"sex",
			"photo",
		));

		$this->add_fields_in_axis("register_api", array(
			"login",
			"password",
			"email",
			"first_name", "mid_name", "last_name",
			"city_id",
			"sex",
		));

		$this->add_fields_in_axis("settings", array(
			"first_name", "mid_name", "last_name",
			"city_id",
			"sex", "photo",
			"email", "www", "phone", "skype", //"icq",
			"about",
			"password",
			"allow_email_on_reply",
			"allow_email_on_premoderation",
			"results_access_level", "marks_access_level",
		));

		$this->add_fields_in_axis("edit_by_admin", array(
			"first_name", "mid_name", "last_name",
			"city_id",
			"sex", "photo",
			"email", "www", "phone", "skype", //"icq",
			"about",
			"password",
			"allow_email_on_reply",
			"allow_email_on_premoderation",
		));
	}

}

?>