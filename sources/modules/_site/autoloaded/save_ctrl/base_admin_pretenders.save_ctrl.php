<?php

abstract class base_admin_pretenders_save_ctrl extends base_save_ctrl
{
	/* Internal */

	protected $allow_nonstrict_members = false;
	protected $action;
	protected $user_id_array = array();
	protected $access_save_array = array();

	/**
	 * @return base_access_save
	 */
	abstract protected function get_access_save($user_id);

	public function start()
	{
		$users = array();
		if (!is_null(POST("accept-all")))
		{
			$this->action = "accept";
			$users = array_keys(POST_STARTING_WITH("user-"));
		}
		elseif (!is_null(POST("decline-all")))
		{
			$this->action = "decline";
			$users = array_keys(POST_STARTING_WITH("user-"));
		}
		else
		{
			$users = array_keys(POST_STARTING_WITH("accept-user-"));
			if (sizeof($users))
			{
				$this->action = "accept";
			}
			else
			{
				$users = array_keys(POST_STARTING_WITH("decline-user-"));
				$this->action = "decline";
			}
		}

		foreach ($users as $user_id)
		{
			if (is_good_id($user_id))
			{
				$this->user_id_array[] = $user_id;
			}
		}

		return true;
	}

	public function commit()
	{
		foreach ($this->user_id_array as $user_id)
		{
			$access_save = $this->get_access_save($user_id);
			/* @var $access_save base_access_save */
			if ($this->allow_nonstrict_members || $access_save->is_pretender_strict())
			{
				$this->access_save_array[] = $access_save;
				if ($this->action == "accept")
				{
					$access_save->add_member();
				}
				else
				{
					$access_save->delete_member();
				}
			}
		}
		return true;
	}

	public function clean_cache()
	{
		foreach ($this->access_save_array as $access_save)
		{
			/* @var $access_save base_access_save */
			$access_save->clean_cache();
		}
	}

}

?>