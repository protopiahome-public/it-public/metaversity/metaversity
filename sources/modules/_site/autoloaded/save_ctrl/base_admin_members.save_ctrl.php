<?php

abstract class base_admin_members_save_ctrl extends base_save_ctrl
{
	/* Internal */

	protected $user_id_array = array();
	protected $access_save_array = array();

	/**
	 * @return base_access_save
	 */
	abstract protected function get_access_save($user_id);

	public function start()
	{
		$user_id = POST("user_id");
		if (is_good_id($user_id))
		{
			$this->user_id_array = array($user_id);
		}
		else
		{
			$users = array_keys(POST_STARTING_WITH("user-"));
			foreach ($users as $user_id)
			{
				if (is_good_id($user_id))
				{
					$this->user_id_array[] = $user_id;
				}
			}
		}
		
		return true;
	}

	public function commit()
	{
		foreach ($this->user_id_array as $user_id)
		{
			$access_save = $this->get_access_save($user_id);
			/* @var $access_save base_access_save */
			$this->access_save_array[] = $access_save;
			$access_save->delete_member();
		}
		return true;
	}

	public function clean_cache()
	{
		foreach ($this->access_save_array as $access_save)
		{
			/* @var $access_save base_access_save */
			$access_save->clean_cache();
		}
	}

}

?>