<?php

class user_option_mixin extends base_mixin
{

	/**
	 * @var user
	 */
	protected $user;

	public function start()
	{
		return true;
	}

	public function check_rights()
	{
		return $this->user->get_user_id() > 0;
	}

	public function commit()
	{
		if (POST("option") === "potential_stream_id")
		{
			$value = POST("value");
			if ($this->db->row_exists("SELECT id FROM stream WHERE id = {$value} LOCK IN SHARE MODE"))
			{
				$this->db->sql("UPDATE user SET potential_stream_id = '{$value}' WHERE id = {$this->user->get_user_id()}");
			}
		}
		return true;
	}

	public function clean_cache()
	{
		user_cache_tag::init($this->user->get_user_id())->update();
	}

}

?>