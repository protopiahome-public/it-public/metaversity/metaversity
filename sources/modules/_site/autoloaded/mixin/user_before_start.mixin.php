<?php

class user_before_start_mixin extends base_mixin
{

	protected $user_id;
	protected $user_row;

	public function on_before_start()
	{
		$this->user_id = POST("user_id");
		if (!is_good_id($this->user_id))
		{
			return false;
		}
		$this->user_row = $this->db->get_row("SELECT * FROM user WHERE id = {$this->user_id}");
		if (!$this->user_row)
		{
			return false;
		}
		return true;
	}

}

?>