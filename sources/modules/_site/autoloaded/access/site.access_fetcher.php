<?php

class site_access_fetcher extends base_access_fetcher
{

	protected $user_row = false;

	public function __construct(site_access_maper $access_maper, $user_id = null, $lock = false)
	{
		parent::__construct($access_maper, $user_id, $lock);
	}

	protected function fetch_data()
	{
		$for_update_sql = $this->lock ? "FOR UPDATE" : "";

		$this->user_row = $this->db->get_row("
			SELECT *
			FROM user
			WHERE id = {$this->user_id}
			{$for_update_sql}
		");
		if (!$this->user_row)
		{
			$this->level = ACCESS_LEVEL_GUEST;
		}
		elseif ($this->user_row["is_admin"] == "1")
		{
			$this->level = ACCESS_LEVEL_ADMIN;
		}
		else
		{
			$this->level = ACCESS_LEVEL_USER;
		}
	}

	protected function error($error_msg)
	{
		$log_message = date("c") . "\t" . $error_msg . " (user_id = {$this->user_id})";
		file_put_contents(PATH_LOG . "/site_access.log", $log_message . "\n", FILE_APPEND);
		parent::error($error_msg);
	}

	public function get_user_row()
	{
		return $this->user_row;
	}

}

?>