<?php

require_once PATH_CORE . "/loader/xml_loader.php";

class site_access_save extends base_access_save
{

	/**
	 * @var site_access_maper
	 */
	protected $access_maper;

	/**
	 * @var site_access_fetcher
	 */
	protected $access_fetcher;
	protected $user_row;

	protected function fill_data()
	{
		$this->access_maper = new site_access_maper();
		$this->access_fetcher = new site_access_fetcher($this->access_maper, $this->user_id, $lock = true);
		$this->user_row = $this->access_fetcher->get_user_row();
		$this->level = $this->access_fetcher->get_level();
		return !$this->access_fetcher->error_occured();
	}
	
	public function add_member()
	{
		trigger_error("Unallowed action");
	}

	public function add_moderator()
	{
		trigger_error("Unallowed action");
	}

	public function add_admin()
	{
		if ($this->level > ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		if ($this->level == ACCESS_LEVEL_ADMIN)
		{
			return true;
		}
		$this->db->sql("
			UPDATE user
			SET is_admin = 1
			WHERE id = {$this->user_id}
		");
		return true;
	}

	public function delete_member()
	{
		trigger_error("Unallowed action");
	}

	public function delete_moderator()
	{
		trigger_error("Unallowed action");
	}

	public function delete_admin()
	{
		if ($this->level < ACCESS_LEVEL_ADMIN)
		{
			return false;
		}
		$this->db->sql("
			UPDATE user
			SET is_admin = 0
			WHERE id = {$this->user_id}
		");
		return true;
	}

	public function set_moderator_rights($rights_array)
	{
		trigger_error("Unallowed action");
	}

	public function clean_cache()
	{
		user_cache_tag::init($this->user_id)->update();
	}

}

?>