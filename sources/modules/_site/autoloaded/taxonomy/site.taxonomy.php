<?php

final class site_taxonomy extends base_taxonomy
{

	public function run()
	{
		global $config;

		$p = $this->get_parts_relative();

		//$this->xml_loader->set_main_xslt("base_html", "_site");
		$this->xml_loader->set_error_404_xslt("404", "_site");
		$this->xml_loader->add_xml(new pass_info_xml_ctrl());
		$this->xml_loader->add_xml(new user_xml_ctrl());
		$this->xml_loader->add_xml(new request_xml_ctrl());
		$this->xml_loader->add_xml(new sys_params_xml_ctrl());

		if ($this->domain_logic->is_on())
		{
			if ($this->domain_logic->is_www_subdomain())
			{
				$this->xml_loader->set_redirect_url($this->request->get_protocol(true) . $this->request->get_host_without_www() . "/" . $this->request->get_all_folders() . $this->request->get_params_string());
				$this->xml_loader->set_redirect_type_permanent();
			}
			elseif ($this->domain_logic->is_users_host())
			{
				$users_taxonomy = new users_taxonomy($this->xml_loader, $this);
				$users_taxonomy->run();
			}
			elseif ($this->domain_logic->is_students_host())
			{
				$this->xml_loader->set_redirect_url($config["main_url"]);
			}
			elseif ($this->domain_logic->is_main_host())
			{
				$main_host_taxonomy = new main_host_taxonomy($this->xml_loader, $this);
				$main_host_taxonomy->run();
			}
			elseif (($stream_id = $this->domain_logic->get_current_domain_stream_id()))
			{
				$stream_taxonomy = new stream_taxonomy($this->xml_loader, $this, 0, $stream_id, true);
				$stream_taxonomy->run();
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new unknown_host_xml_page(), "unknown_host");
				$this->xml_loader->set_error_404();
			}
		}
		else
		{
			$main_host_taxonomy = new main_host_taxonomy($this->xml_loader, $this);
			$main_host_taxonomy->run();
		}
		$this->check_404();
	}

	protected function check_404()
	{
		if (!$this->xml_loader->is_xml_page_loaded() && !$this->xml_loader->get_redirect_url())
		{
			if (!($this->xml_loader->is_error_404() || $this->xml_loader->is_error_403()))
			{
				$this->xml_loader->set_error_404();
				$this->xml_loader->add_xml(new error_404_xml_page());
			}
		}
	}

}

?>