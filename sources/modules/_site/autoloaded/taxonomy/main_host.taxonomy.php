<?php

final class main_host_taxonomy extends base_taxonomy
{

	public function run()
	{
		global $config;

		$p = $this->get_parts_relative();

		if ($p[1] === null)
		{
			// (Main page)
			if ($this->user->get_user_id())
			{
				// --> /news/
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/news/");
			}
			else
			{
				// --> /streams/
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/streams/");
			}
		}
		elseif ($p[1] === "streams" and $page = $this->is_page_folder($p[2]) and $p[3] === null)
		{
			//streams/[page-<page>/]
			$this->xml_loader->add_xml_with_xslt(new streams_xml_page($page));
		}
		elseif ($p[1] === "streams" and $stream_id = url_helper::get_stream_id_by_name($p[2]))
		{
			//streams/<stream>/...

			$stream_taxonomy = new stream_taxonomy($this->xml_loader, $this, 2, $stream_id, false);
			$stream_taxonomy->run();
		}
		elseif ($p[1] === "activities")
		{
			//activities/...
			if (($params = activities_taxonomy_helper::parse_request_params($p, 2, activities_taxonomy_helper::MOD_MY)))
			{
				$this->xml_loader->add_xml_with_xslt(new activities_xml_page($params));
			}
			elseif ($p[2] === "calendar" and $p[3] === null)
			{
				//activities/calendar/
				$this->set_redirect_url("{$p[1]}/");
			}
		}
		elseif ($p[1] === "news" and ( $page = $this->is_page_folder($p[2])) and $p[3] === null)
		{
			//news/[page-<page>/]
			if (!$this->user->get_user_id())
			{
				$this->xml_loader->add_xml(new user_news_xml_page(0, $page));
				$this->xml_loader->add_xslt("news_guest", "_site");
			}
			else
			{
				$user_id = $this->user->get_user_id();
				$this->xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $user_id);
				$this->xml_loader->add_xml_with_xslt(new user_news_xml_page($user_id, $page));
			}
		}
		elseif ($p[1] === "users" and ! $this->domain_logic->is_on())
		{
			//users/...
			$users_taxonomy = new users_taxonomy($this->xml_loader, $this, 1);
			$users_taxonomy->run();
		}
		elseif ($p[1] === "admin")
		{
			//admin/...
			$admin_taxonomy = new admin_taxonomy($this->xml_loader, $this, 1);
			$admin_taxonomy->run();
		}
		elseif ($p[1] === "lang" and $p[2] === null)
		{
			$this->xml_loader->add_xml_with_xslt(new lang_xml_page());
		}
		elseif ($p[1] === "codegen" && $this->xerror->is_debug_mode())
		{
			$codegen_taxonomy = new codegen_taxonomy($this->xml_loader, $this, 1);
			$codegen_taxonomy->run();
		}

		if (!$this->xml_loader->is_xml_page_loaded() and ! $this->xml_loader->get_redirect_url())
		{
			$auth_taxonomy = new auth_taxonomy($this->xml_loader, $this);
			$auth_taxonomy->run();
		}
	}

}

?>