<?php

class lang_xml_page extends base_easy_xml_ctrl
{

	protected $xml_row_name = "lang";

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("lang");
		$select_sql->add_select_fields("code");
		$select_sql->add_select_fields("title");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>