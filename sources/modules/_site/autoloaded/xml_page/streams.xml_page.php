<?php

class streams_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "id",
			"ctrl" => "stream_short"
		),
	);
	// Internal
	protected $page;

	public function __construct($page)
	{
		$this->page = $page;
		parent::__construct();
	}

	public function init()
	{
		$this->xml_loader->add_xml(new cities_xml_ctrl());

		$this->add_easy_processor(new pager_db_easy_processor($this->page, 50));

		$processor = new sql_filters_easy_processor();
		$processor->add_sql_filter(new text_sql_filter("search", trans("Title"), array("d.title", "d.title_short", "d.title_en", "d.title_short_en"), trans("Search by title")));
		$processor->add_sql_filter(new multi_link_sql_filter("city", trans("City"), "d.id", "stream_city_link", "stream_id", "city_id", "city", $this->lang->get_current_lang_code() === "en" ? "title_en" : "title", "id", "is_fake = 0"));
		$this->add_easy_processor($processor);
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("stream d");
		$select_sql->add_select_fields("d.id, d.list_descr, d.user_count_calc");
		$select_sql->add_where("d.list_show = 1");
		if ($this->user->get_user_id())
		{
			$select_sql->add_select_fields("l.status");
			$select_sql->add_join("LEFT JOIN stream_user_link l ON l.stream_id = d.id AND l.user_id = {$this->user->get_user_id()}");
			$select_sql->add_order("IF(l.status = 'admin', 1, 0) DESC");
			$select_sql->add_order("IF(l.status = 'moderator', 1, 0) DESC");
			$select_sql->add_order("IF(l.status = 'pretender', 1, 0) DESC");
			$select_sql->add_order("IF(l.status = 'member', 1, 0) DESC");
		}
		$select_sql->add_order("IF(d.lang_code = '{$this->lang->get_current_lang_code()}', 1, 0) DESC");
		$select_sql->add_order("d.list_position");
		$this->data = $this->db->fetch_all($select_sql->get_sql(), "id");
	}

	protected function postprocess_data()
	{
		foreach ($this->data as &$row)
		{
			$row["cities"] = array();
		}
		unset($row);

		$links = $this->db->fetch_all("SELECT * FROM stream_city_link");
		foreach ($links as $link)
		{
			$stream_id = $link["stream_id"];
			$city_id = $link["city_id"];
			if (isset($this->data[$stream_id]))
			{
				$this->data[$stream_id]["cities"][] = $city_id;
			}
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		foreach ($this->data as $row)
		{
			$stream_node = $xdom->create_child_node("stream");
			foreach ($row as $idx => $val)
			{
				if (!is_array($val))
				{
					$stream_node->set_attr($idx, $val);
				}
			}
			if (sizeof($row["cities"]))
			{
				foreach ($row["cities"] as $city_id)
				{
					$stream_node->create_child_node("city")->set_attr("id", $city_id);
				}
			}
		}
	}

}

?>