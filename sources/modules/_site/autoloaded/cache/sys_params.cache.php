<?php

class sys_params_cache extends base_cache
{

	public static function init()
	{
		$tag_keys = array();
		$tag_keys[] = sys_params_cache_tag::init()->get_key();
		return parent::get_cache(__CLASS__, $tag_keys);
	}

}

?>