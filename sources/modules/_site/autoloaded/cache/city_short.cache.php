<?php

class city_short_cache extends base_cache
{

	public static function init($city_id, $lang_code)
	{
		$tag_keys = array();
		$tag_keys[] = cities_cache_tag::init()->get_key();
		return parent::get_cache(__CLASS__, $city_id, $lang_code, $tag_keys);
	}

}

?>