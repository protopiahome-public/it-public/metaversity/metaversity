<?php

class user_short_cache extends base_cache
{

	public static function init($user_id, $lang_code)
	{
		$tag_keys = array();
		$tag_keys[] = user_cache_tag::init($user_id)->get_key();
		$tag_keys[] = cities_cache_tag::init()->get_key();
		return parent::get_cache(__CLASS__, $user_id, $lang_code, $tag_keys);
	}

}

?>