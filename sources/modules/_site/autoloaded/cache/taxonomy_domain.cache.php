<?php

class taxonomy_domain_cache extends base_cache
{

	public static function init($domain_name, $domain_info = null)
	{
		$tag_keys = array();
		if ($domain_info)
		{
			if ($domain_info["type"] === "stream")
			{
				$tag_keys[] = stream_cache_tag::init($domain_info["id"])->get_key();
			}
		}
		return parent::get_cache(__CLASS__, $domain_name, $tag_keys);
	}

}

?>