<?php

class lang_save_page extends base_save_ctrl
{

	public function commit()
	{
		if (in_array(REQUEST("lang"), array("ru", "en")))
		{
			lang_helper::set_lang_cookie(REQUEST("lang"), 1 == REQUEST("translate_content"));
		}
		return true;
	}

}

?>