<?php

class user_obj extends base_obj
{

	const ACCESS_LEVEL_ALL = "all";
	const ACCESS_LEVEL_REGISTERED = "registered";
	const ACCESS_LEVEL_STREAM_STUDENTS = "stream_students";
	const ACCESS_LEVEL_ANY_MODERATOR = "any_moderator";

	// Internal
	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	public function __construct($id, $lock = false)
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct($id, $lock);
	}

	private static $cache = array();

	/**
	 * @return user_obj
	 */
	public static function instance($id, $lock = false)
	{
		if (!$lock && isset(self::$cache[$id]))
		{
			return self::$cache[$id];
		}
		$class = __CLASS__;
		$instance = new $class($id, $lock);
		if (!$lock)
		{
			self::$cache[$id] = $instance;
		}
		return $instance;
	}

	public static function _test_clean_cache()
	{
		self::$cache = array();
	}

	public function get_login()
	{
		return $this->get_param("login");
	}

	public function get_results_access_level()
	{
		return $this->get_param("results_access_level");
	}

	public function get_marks_access_level()
	{
		return $this->get_param("marks_access_level");
	}

	public function get_url($no_protocol = false)
	{
		return $this->domain_logic->get_users_prefix($no_protocol) . "/{$this->get_login()}/";
	}

	protected function fill_data($lock)
	{
		$this->data = $this->fetch_data($lock);
	}

	private function fetch_data($lock = false)
	{
		$lock_sql = $lock ? "LOCK IN SHARE MODE" : "";
		$row = $this->db->get_row("
			SELECT *
			FROM user
			WHERE id = {$this->id}
			{$lock_sql}
		");
		return $row ? $row : null;
	}

}

?>