<?php

class cache_cleaner_xml_ctrl extends base_xml_ctrl
{

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		
		if (isset($_GET["user_id"]) and is_good_id($user_id = $_GET["user_id"]))
		{
			user_cache_tag::init($user_id)->update();
			response::set_content_text("OK");
			return "";
		}
		
		response::set_error_404();
		response::set_content_text("ERR");
		return "";
	}

}

?>
