<?php

class user_xml_ctrl extends base_xml_ctrl
{

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$xdom = xdom::create("user");

		if ($this->user->get_user_id())
		{
			$xdom->set_attr("id", $this->user->get_user_id());
			$xdom->set_attr("login", $this->user->get_login());
			$xdom->set_attr("email", $this->user->get_email());
			$xdom->set_attr("visible_name", $this->user->get_visible_name());
			$xdom->set_attr("no_name", $this->user->is_no_name());
			$xdom->set_attr("sex", $this->user->get_sex());
			$xdom->set_attr("city_id", $this->user->get_city_id());
			$xdom->set_attr("is_admin", $this->user->is_admin());
			if ($this->user->get_user_param("photo_big_width") == 0)
			{
				$xdom->set_attr("photo_big_width", $this->user->get_user_param("photo_big_width"));
				$xdom->set_attr("photo_big_height", $this->user->get_user_param("photo_big_height"));
				$xdom->set_attr("photo_big_url", $this->request->get_prefix() . "/img/defaults/user-big-default.jpg");
			}
			else
			{
				$xdom->set_attr("photo_big_width", $this->user->get_user_param("photo_big_width"));
				$xdom->set_attr("photo_big_height", $this->user->get_user_param("photo_big_height"));
				$xdom->set_attr("photo_big_url", $this->request->get_prefix() . "/data/user/big/{$this->user->get_user_id()}.jpeg?v" . $this->user->get_user_param("photo_version"));
			}
		}
		return $xdom->get_xml(true);
	}

}

?>