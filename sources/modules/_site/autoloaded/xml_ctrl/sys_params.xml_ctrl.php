<?php

class sys_params_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "sys_params";
	protected $axis_name = "short";

	public function __construct()
	{
		parent::__construct(1);
	}

	protected function get_cache()
	{
		return sys_params_cache::init();
	}
	
}

?>