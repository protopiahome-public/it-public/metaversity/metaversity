<?php

class city_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "city";
	protected $axis_name = "full";

	protected function get_cache()
	{
		return city_short_cache::init($this->id, $this->lang->get_current_lang_code());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		if ($this->lang->get_current_lang_code() === "en")
		{
			$select_sql->add_select_fields("dt.title_en");
		}
	}

	protected function process_data()
	{
		$data = &$this->data[0];
		if ($this->lang->get_current_lang_code() === "en")
		{
			$data["title"] = $data["title_en"];
			unset($data["title_en"]);
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		if ($this->data)
		{
			$xdom->set_attr("lang_code", $this->data[0]["lang_code"]);
		}
	}

}

?>