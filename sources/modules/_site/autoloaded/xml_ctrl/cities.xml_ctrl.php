<?php

class cities_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "city";

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_from("city");
		$select_sql->add_select_fields("id");
		$select_sql->add_select_fields($this->lang->get_current_lang_code() === "en" ? "title_en AS title" : "title");
		$select_sql->add_where("is_fake = 0");
		$select_sql->add_order("title");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>