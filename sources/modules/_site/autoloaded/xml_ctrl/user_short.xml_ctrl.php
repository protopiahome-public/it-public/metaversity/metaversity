<?php

class user_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dt_name = "user";
	protected $axis_name = "short";

	protected function get_cache()
	{
		return user_short_cache::init($this->id, $this->lang->get_current_lang_code());
	}

	protected function modify_sql(select_sql $select_sql)
	{
		if ($this->lang->get_current_lang_code() === "en")
		{
			$select_sql->add_select_fields("dt.first_name_en AS first_name, dt.last_name_en AS last_name");
			$select_sql->add_select_fields("city_id_foreign_table.title_en AS city_title_en");
		}
		else
		{
			$select_sql->add_select_fields("dt.first_name, dt.last_name");
		}
	}

	protected function process_data()
	{
		$data = &$this->data[0];
		$full_name = trim($data["first_name"] . " " . $data["last_name"]);
		$data["visible_name"] = $full_name ? : $data["login"];
		if ($this->lang->get_current_lang_code() === "en")
		{
			$data["city_title"] = $data["city_title_en"];
			unset($data["city_title_en"]);
		}
	}

	protected function fill_xml(xdom $xdom)
	{
		parent::fill_xml($xdom);
		$data = &$this->data[0];
		$xdom->set_attr("visible_name", $data["visible_name"]);
	}

}

?>