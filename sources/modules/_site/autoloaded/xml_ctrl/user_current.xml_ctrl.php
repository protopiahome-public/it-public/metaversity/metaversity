<?php

class user_current_xml_ctrl extends base_xml_ctrl
{

	/**
	 * @var user_obj
	 */
	protected $user_obj;

	public function __construct($user_id)
	{
		$this->user_obj = user_obj::instance($user_id);
		parent::__construct();
	}

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;
		if (!$this->user_obj->exists())
		{
			$this->set_error_404();
			return;
		}
		$xdom = xdom::create($this->name);
		$xdom->set_attr("id", $this->user_obj->get_id());
		$xdom->set_attr("login", $this->user_obj->get_login());
		$xdom->set_attr("results_access_level", $this->user_obj->get_results_access_level());
		$xdom->set_attr("marks_access_level", $this->user_obj->get_marks_access_level());
		$xdom->set_attr("url", $this->user_obj->get_url());
		return $xdom->get_xml(true);
	}

}

?>