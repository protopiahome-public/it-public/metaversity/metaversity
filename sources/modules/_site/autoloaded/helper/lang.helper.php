<?php

class lang_helper
{

	protected static $is_lang_cookie_changed = false;

	public static function init_lang()
	{
		global $config;
		global $lang;
		$lang_cookie_name = $config["lang_cookie_prefix"] . "lang";
		$lang_code = isset($_COOKIE[$lang_cookie_name]) && ($_COOKIE[$lang_cookie_name] === "en" || $_COOKIE[$lang_cookie_name] === "ru") ? $_COOKIE[$lang_cookie_name] : null;
		$lang = new lang($lang_code, array("en", "ru"));
		if (!isset($_COOKIE[$lang_cookie_name]))
		{
			self::set_lang_cookie($lang->get_current_lang_code());
		}
	}

	public static function reinit_lang($lang_code)
	{
		global $config;
		$lang_cookie_name = $config["lang_cookie_prefix"] . "lang";
		if (!isset($_COOKIE[$lang_cookie_name]) || self::get_is_lang_cookie_changed())
		{
			global $lang;
			/* @var $lang lang */
			if ($lang->get_current_lang_code() != $lang_code)
			{
				$lang = new lang($lang_code);
				self::set_lang_cookie($lang_code);
				header("Refresh:0");
			}
		}
	}

	public static function set_lang_cookie($lang_code, $translate_content = false)
	{
		global $config;
		global $domain_logic;
		output_buffer::set_cookie($config["lang_cookie_prefix"] . "lang", $lang_code, 1591905600 /* Year 2020 */, $domain_logic->get_cookie_path(), $domain_logic->get_cookie_domain());
		output_buffer::set_cookie($config["lang_cookie_prefix"] . "translate_content", $translate_content ? "1" : "0", 1591905600 /* Year 2020 */, $domain_logic->get_cookie_path(), $domain_logic->get_cookie_domain());
		$_COOKIE[$config["lang_cookie_prefix"] . "lang"] = $lang_code;
		self::$is_lang_cookie_changed = true;
	}

	public static function get_is_lang_cookie_changed()
	{
		return self::$is_lang_cookie_changed;
	}

}

?>