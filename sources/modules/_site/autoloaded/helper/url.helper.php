<?php

class url_helper
{

	public static function get_user_id_by_login($login)
	{
		/* @var $db db */
		global $db;

		$user_id = taxonomy_user_cache::init($login)->get();
		if (!$user_id)
		{
			$login_escaped = $db->escape($login);
			if (($user_id = $db->get_value("SELECT id FROM user WHERE BINARY login = '{$login_escaped}'")))
			{
				taxonomy_user_cache::init($login, $user_id)->set($user_id);
			}
		}
		return $user_id;
	}

	public static function get_stream_id_by_name($stream_name)
	{
		/* @var $db db */
		global $db;

		$stream_name_escaped = $db->escape($stream_name);
		return $db->get_value("SELECT id FROM stream WHERE BINARY name = '{$stream_name_escaped}'");
	}

	protected static function remove_prefix($url)
	{
		/* @var $request request */
		global $request;
		if (!str_begins($url, $request->get_full_prefix()))
		{
			return false;
		}

		return substr($url, strlen($request->get_full_prefix()));
	}

}

?>