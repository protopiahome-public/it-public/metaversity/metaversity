<?php

class domain_parser_helper extends base_static_db_helper
{

	public static function get_domain_info($domain_name, $main_host_name)
	{
		static $cache = array();
		if (isset($cache[$domain_name]))
		{
			return $cache[$domain_name];
		}

		if (($info = taxonomy_domain_cache::init($domain_name)->get()))
		{
			$cache[$domain_name] = $info;
			return $info;
		}

		$info = null;

		$domain_name_escaped = self::db()->escape($domain_name);
		$domain_parts = explode(".", $domain_name, 2);
		if (isset($domain_parts[1]) and $domain_parts[1] == $main_host_name)
		{
			$name = $domain_parts[0];
			$name_escaped = self::db()->escape($name);
			$stream_id = self::db()->get_value("
					SELECT id
					FROM stream
					WHERE BINARY name = '{$name_escaped}'
				");
			if ($stream_id)
			{
				$info = array(
					"type" => "stream",
					"id" => $stream_id,
				);
			}
		}

		if ($info)
		{
			taxonomy_domain_cache::init($domain_name, $info)->set($info);
		}

		$cache[$domain_name] = $info;
		return $info;
	}

}

?>