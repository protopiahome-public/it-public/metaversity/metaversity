<?php

class user_access_helper
{

	public static function can_access_results(user_obj $results_user_obj, stream_obj $results_stream_obj)
	{
		return self::check_access_level($results_user_obj->get_results_access_level(), $results_user_obj, $results_stream_obj);
	}

	public static function can_access_marks(user_obj $results_user_obj, stream_obj $results_stream_obj)
	{
		return self::check_access_level($results_user_obj->get_marks_access_level(), $results_user_obj, $results_stream_obj);
	}

	private static function check_access_level($access_level, user_obj $results_user_obj, stream_obj $results_stream_obj)
	{
		global $user;
		/* @var $user user */

		if ($user->is_moderator_anywhere())
		{
			return true;
		}
		if ($access_level === user_obj::ACCESS_LEVEL_ALL)
		{
			return true;
		}
		if ($access_level === user_obj::ACCESS_LEVEL_REGISTERED)
		{
			return !!$user->get_user_id();
		}

		// $stream_access = $results_stream_obj->get_access($results_user_obj->get_id());
		// Now we do not check that $stream_access->is_member() or $stream_access->is_pretender_strict()
		$current_user_stream_access = $results_stream_obj->get_access();
		switch ($access_level)
		{
			case user_obj::ACCESS_LEVEL_STREAM_STUDENTS:
				return $current_user_stream_access->has_member_rights();
		}

		return false;
	}

}

?>