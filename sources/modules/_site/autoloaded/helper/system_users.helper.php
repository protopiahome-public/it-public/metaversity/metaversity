<?php

class system_users_helper extends base_static_db_helper
{

	public static function get_system_admins_as_keys()
	{
		static $cache = null;
		if (!is_null($cache))
		{
			return $cache;
		}
		$data = self::db()->fetch_column_values("
			SELECT id
			FROM user
			WHERE is_admin = 1
		", true, "id");
		$cache = $data;
		return $data;
	}

}

?>