<?php

class sys_params_helper extends base_static_db_helper
{

	protected static $cache = null;

	public static function get_central_competence_set_id()
	{
		self::load_cache();
		return (int) self::$cache["central_competence_set_id"];
	}

	public static function clear_cache()
	{
		self::$cache = null;
	}
	
	protected static function load_cache()
	{
		if (is_null(self::$cache))
		{
			self::$cache = self::db()->get_row("SELECT * FROM sys_params LIMIT 1");
		}
	}

}

?>