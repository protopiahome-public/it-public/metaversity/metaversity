<?php

class mail_helper extends base_static_db_helper
{

	public static function add_letter($subject, $html, $to_email, $to_user_id)
	{
		if (!trim($to_email))
		{
			return;
		}
		
		$html_escaped = self::db()->escape($html);
		$subject_escaped = self::db()->escape($subject);

		self::db()->sql("
			INSERT DELAYED INTO email
			SET
				to_email = '{$to_email}',
				to_user_id = {$to_user_id},
				subject = '{$subject_escaped}',
				add_time = NOW(),
				html = '{$html_escaped}'
		");
	}

}

?>