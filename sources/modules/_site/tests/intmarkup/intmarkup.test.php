<?php

require_once PATH_MODULE_SITE_LIB . "/intmarkup/intmarkup.php";

class intmarkup_test extends base_test
{

	public function paragraph_test()
	{
		$src = "123";
		$exp = "<p>123</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function paragraphs_test()
	{
		$src = "123\n\n456";
		$exp = "<p>123</p><p>456</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function paragraphs_spaces_test()
	{
		$src = " \n 123\n\n\n456 \n ";
		$exp = "<p>123</p><p>456</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function br_test()
	{
		$src = "123\n456\n\n789";
		$exp = "<p>123<br/>456</p><p>789</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_test()
	{
		$src = "`123`";
		$exp = "<p><code>123</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_second_test()
	{
		$src = "`123` `456`\n\nqwe `asdf asdf` rt";
		$exp = "<p><code>123</code> <code>456</code></p><p>qwe <code>asdf asdf</code> rt</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_inside_text_test()
	{
		$src = "a`b`c";
		$exp = "<p>a`b`c</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_inside_text_2_test()
	{
		$src = "`b`c";
		$exp = "<p>`b`c</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_blank_test()
	{
		$src = "``";
		$exp = "<p>``</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_after_quot_test()
	{
		$src = "a ``b`";
		$exp = "<p>a ``b`</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_escape_test()
	{
		$src = "\`a`";
		$exp = "<p>`a`</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_escape_2_test()
	{
		$src = "`a\`";
		$exp = "<p>`a`</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_escape_3_test()
	{
		$src = "`a\``";
		$exp = "<p><code>a`</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_blanks_test()
	{
		$src = "  `  xxx  `  ";
		$exp = "<p><code>  xxx  </code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_test()
	{
		$src = "``123``";
		$exp = "<p><code>123</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_second_test()
	{
		$src = "``123`` ``456``\n\nqwe ``asdf asdf`` rt";
		$exp = "<p><code>123</code> <code>456</code></p><p>qwe <code>asdf asdf</code> rt</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_inside_text_test()
	{
		$src = "a``b``c";
		$exp = "<p>a``b``c</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_inside_text_2_test()
	{
		$src = "``b``c";
		$exp = "<p>``b``c</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_blank_test()
	{
		$src = "````";
		$exp = "<p>````</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_three_quotes_test()
	{
		$src = "```";
		$exp = "<p>```</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_after_quot_test()
	{
		$src = "a ```b``";
		$exp = "<p>a <code>`b</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_escape_test()
	{
		$src = "\``a``";
		$exp = "<p>``a``</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_double_escape_test()
	{
		$src = "\`\`a``";
		$exp = "<p>``a``</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_escape_2_test()
	{
		$src = "``a\``";
		$exp = "<p>``a``</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_escape_3_test()
	{
		$src = "``a\```";
		$exp = "<p><code>a`</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_escape_4_test()
	{
		$src = "``123\`456``";
		$exp = "<p><code>123`456</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_escape_5_test()
	{
		$src = "``123\``456``";
		$exp = "<p><code>123``456</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_blanks_test()
	{
		$src = "  ``  xxx  ``  ";
		$exp = "<p><code>  xxx  </code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_code_inside_test()
	{
		$src = "``123` `456``";
		$exp = "<p><code>123` `456</code></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code3_test()
	{
		$src = "123\n\n```  \n\n\tcode\tcode\n\n\n\tcode```  \n```\n456";
		$exp = "<p>123</p><p><pre><code>\n\tcode\tcode\n\n\n\tcode```  </code></pre></p><p>456</p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_test()
	{
		$src = "http://yandex.ru";
		$exp = '<p><a href="http://yandex.ru">yandex.ru</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_human_slash_test()
	{
		$src = "http://yandex.ru/";
		$exp = '<p><a href="http://yandex.ru/">yandex.ru</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_full_test()
	{
		$src = "http://yandex.ru///889/?i=9&t=2#hhju;a=5&d=18";
		$exp = '<p><a href="http://yandex.ru///889/?i=9&amp;t=2#hhju;a=5&amp;d=18">yandex.ru///889/?i=9&amp;t=2#hhju;a=5&amp;d=18</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_port_test()
	{
		$src = "http://yandex.ru:80/abc";
		$exp = '<p><a href="http://yandex.ru:80/abc">yandex.ru:80/abc</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_ip_test()
	{
		$src = "http://192.168.0.1:8080/abc";
		$exp = '<p><a href="http://192.168.0.1:8080/abc">192.168.0.1:8080/abc</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_localhost_test()
	{
		$src = "http://localhost";
		$exp = '<p><a href="http://localhost">localhost</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_internal_test()
	{
		$src = "Hello, I'm from http://yandex.ru/?x=y !!!";
		$exp = '<p>Hello, I\'m from <a href="http://yandex.ru/?x=y">yandex.ru/?x=y</a> !!!</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_not_link_test()
	{
		$src = "Hello, I'm from http://-\\\ http://ya.ru !!!";
		$exp = '<p>Hello, I\'m from http://-\\\ <a href="http://ya.ru">ya.ru</a> !!!</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_utf_test()
	{
		$src = "http://домен.рф/тратата/улюлю?люлю=1хряхря#уляля";
		$exp = '<p><a href="http://домен.рф/тратата/улюлю?люлю=1хряхря#уляля">домен.рф/тратата/улюлю?люлю=1хряхря#уляля</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_escaping_test()
	{
		$path_shit = "1234567890;$:@&=+_.!~*'`(),[]-/";
		$path_shit_safe_text = str_replace(array("&", "<", ">"), array("&amp;", "&lt;", "&gt;"), $path_shit);
		$path_shit_safe_attr = str_replace(array("&", "<", ">", '"'), array("&amp;", "&lt;", "&gt;", "&quot;"), $path_shit);
		$full_shit = ";$:@&=+_.!~*'`(),[]-{}|^\"<>??\\";
		$full_shit_safe_text = str_replace(array("&", "<", ">"), array("&amp;", "&lt;", "&gt;"), $full_shit);
		$full_shit_safe_attr = str_replace(array("&", "<", ">", '"'), array("&amp;", "&lt;", "&gt;", "&quot;"), $full_shit);
		$src = "http://суп-домен.со-сметаной.рф/{$path_shit}?{$full_shit}";
		$exp = "<p><a href=\"http://суп-домен.со-сметаной.рф/{$path_shit_safe_attr}?{$full_shit_safe_attr}\">суп-домен.со-сметаной.рф/{$path_shit_safe_text}?{$full_shit_safe_text}</a></p>";
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_end_sentence_test()
	{
		$src = "Hello from http://ya.ru.";
		$exp = '<p>Hello from <a href="http://ya.ru">ya.ru</a>.</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_end_sentence_2_test()
	{
		$src = "Hello from http://ya.ru/...";
		$exp = '<p>Hello from <a href="http://ya.ru/">ya.ru</a>...</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_end_sentence_3_test()
	{
		$src = "Hello from http://ya.ru/xxx/; http://r0.ru/xxx.";
		$exp = '<p>Hello from <a href="http://ya.ru/xxx/">ya.ru/xxx/</a>; <a href="http://r0.ru/xxx">r0.ru/xxx</a>.</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_end_sentence_4_test()
	{
		$src = "Visit my site (http://мойсайт.рф/?параметр=12&пораметер=78).";
		$exp = '<p>Visit my site (<a href="http://мойсайт.рф/?параметр=12&amp;пораметер=78">мойсайт.рф/?параметр=12&amp;пораметер=78</a>).</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_escaped_test()
	{
		$src = "http://yandex.ru,http://yandex.ru";
		$exp = '<p><a href="http://yandex.ru">yandex.ru</a>,<a href="http://yandex.ru">yandex.ru</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_1_test()
	{
		$src = "http://x.ru/1.jpg";
		$exp = '<p><img src="http://x.ru/1.jpg" alt="1.jpg"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_2_test()
	{
		$src = "http://x.ru/path/1.png";
		$exp = '<p><img src="http://x.ru/path/1.png" alt="1.png"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_3_test()
	{
		$src = "http://x.ru/path/##image";
		$exp = '<p><img src="http://x.ru/path/" alt="path"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_4_test()
	{
		$src = "http://x.ru/##image";
		$exp = '<p><img src="http://x.ru/" alt="x.ru"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_5_test()
	{
		$src = "http://x.ru/1.jpg##link";
		$exp = '<p><a href="http://x.ru/1.jpg">x.ru/1.jpg</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_6_test()
	{
		$src = "http://x.ru/justpath/##link";
		$exp = '<p><a href="http://x.ru/justpath/">x.ru/justpath/</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_7_test()
	{
		$src = "http://x.ru/justpath/##link,320x240";
		$exp = '<p><a href="http://x.ru/justpath/">x.ru/justpath/</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_8_test()
	{
		$src = "http://x.ru/path.bmp##image,320x240";
		$exp = '<p><img src="http://x.ru/path.bmp" width="320" height="240" alt="path.bmp"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_9_test()
	{
		$src = "http://x.ru/path.bmp##320x240";
		$exp = '<p><img src="http://x.ru/path.bmp" width="320" height="240" alt="path.bmp"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_10_test()
	{
		$src = "http://x.ru/path.bmp##link,320x240";
		$exp = '<p><a href="http://x.ru/path.bmp">x.ru/path.bmp</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_11_test()
	{
		$src = "http://x.ru/path.bmp##image,link,320x240";
		$exp = '<p><a href="http://x.ru/path.bmp">x.ru/path.bmp</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_12_test()
	{
		$src = "http://x.ru/1.JPG";
		$exp = '<p><img src="http://x.ru/1.JPG" alt="1.JPG"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}
	
	public function image_13_test()
	{
		$src = "http://x.ru/1.JPG?asd";
		$exp = '<p><img src="http://x.ru/1.JPG?asd" alt="1.JPG"/></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function html_special_chars_test()
	{
		$src = "&amp; & <b>";
		$exp = '<p>&amp;amp; &amp; &lt;b&gt;</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code_html_special_chars_test()
	{
		$src = "`&amp; & <b>`";
		$exp = '<p><code>&amp;amp; &amp; &lt;b&gt;</code></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code2_html_special_chars_test()
	{
		$src = "``&amp; & <b>``";
		$exp = '<p><code>&amp;amp; &amp; &lt;b&gt;</code></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function code3_html_special_chars_test()
	{
		$src = "```\n&amp; & <b>\n```";
		$exp = '<p><pre><code>&amp;amp; &amp; &lt;b&gt;</code></pre></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_block_youtube_test()
	{
		$src = "  http://www.youtube.com/watch?v=pXnSVRMme5U ";
		$exp = '<p><iframe width="480" height="270" src="//www.youtube.com/embed/pXnSVRMme5U" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_block_br_youtube_test()
	{
		$src = "See video:\nhttp://www.youtube.com/watch?v=pXnSVRMme5U";
		$exp = '<p>See video:<br/><iframe width="480" height="270" src="//www.youtube.com/embed/pXnSVRMme5U" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_block_youtu_be_test()
	{
		$src = "http://youtu.be/pXnSVRMme5U";
		$exp = '<p><iframe width="480" height="270" src="//www.youtube.com/embed/pXnSVRMme5U" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_block_vimeo_test()
	{
		$src = "http://vimeo.com/27246366";
		$exp = '<p><iframe src="//player.vimeo.com/video/27246366?color=ffffff" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function link_inline_youtube_test()
	{
		$src = "Please see http://www.youtube.com/watch?v=pXnSVRMme5U.";
		$exp = '<p>Please see <a href="http://www.youtube.com/watch?v=pXnSVRMme5U">www.youtube.com/watch?v=pXnSVRMme5U</a>.</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function succession_1_test()
	{
		$src = "http://link.ru `code`";
		$exp = '<p><a href="http://link.ru">link.ru</a> <code>code</code></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function succession_2_test()
	{
		$src = "`code` http://link.ru";
		$exp = '<p><code>code</code> <a href="http://link.ru">link.ru</a></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_test()
	{
		$src = "*123*";
		$exp = '<p><b>123</b></p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_2_test()
	{
		$src = "X *123* X";
		$exp = '<p>X <b>123</b> X</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_3_test()
	{
		$src = "-*123*-";
		$exp = '<p>-<b>123</b>-</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_4_test()
	{
		$src = "--*---*--";
		$exp = '<p>--<b>---</b>--</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_escape_test()
	{
		$src = "--\*---*--";
		$exp = '<p>--*---*--</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_escape_2_test()
	{
		$src = "--\*---\*--";
		$exp = '<p>--*---*--</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_no_bold_1_test()
	{
		$src = "1*123*1";
		$exp = '<p>1*123*1</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_no_bold_2_test()
	{
		$src = "* 123 *";
		$exp = '<p>* 123 *</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	public function bold_unclosed_test()
	{
		$src = "*123";
		$exp = '<p>*123</p>';
		$res = intmarkup::process($src);
		$this->assert_html_equal($res, $exp);
	}

	private function assert_html_equal($real_html, $correct_html)
	{
		$real_html = $this->normalize_html($real_html);
		$correct_html = $this->normalize_html($correct_html);
		if ($real_html !== $correct_html)
		{
			$l = min(mb_strlen($real_html), mb_strlen($correct_html));
			for ($i = 0; $i < $l; ++$i)
			{
				if ($real_html[$i] != $correct_html[$i])
				{
					break;
				}
			}
			$common_start_index = min(15, $i);
			$common_substr = substr($real_html, $i - $common_start_index, $common_start_index);
			$common_substr .= $common_substr ? "|" : "";
			$real_substr = substr($real_html, $i, 15);
			$correct_substr = substr($correct_html, $i, 15);
			$this->test_results->add_error("HTMLs are not equal: '{$common_substr}{$real_substr}' != '{$common_substr}{$correct_substr}' at offset {$i}");
		}
		$this->test_results->increment_assert_counter();
	}

	private function normalize_html($html)
	{
		$html = trim($html);
		$block_element_re = "p|div";
		$html = preg_replace("#(</({$block_element_re})>)\s+(<({$block_element_re})>)#", "\\1\\3", $html);
		$html = preg_replace("#\s+#", " ", $html);
		$html = preg_replace("#\s*<br/>\s*#", "<br/>", $html);
		$html = preg_replace("#<p>\s+#", "<p>", $html);
		$html = preg_replace("#\s+</p>#", "</p>", $html);
		return $html;
	}

}

?>