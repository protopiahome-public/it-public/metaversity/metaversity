<?php

require_once PATH_MODULE_SITE_LIB . "/mail/mail_base.php";
require_once PATH_MODULE_SITE_LIB . "/mail/mail_text.php";
require_once PATH_MODULE_SITE_LIB . "/mail/mail.int.php";

class mail_html extends mail_text implements mail_interface 
{
	protected $body;

	public function __construct($to, $from, $subject, $body, $params = array(), $files = array(), $encoding = "UTF-8")
	{
		if ($subject === null or $subject === "" or $subject === ":AUTO:")
		{
			$subject = $this->get_subject_from_body($body);
		}
		parent::__construct($to, $from, $subject, $body, $params, $files, $encoding);
	}
	
	protected function get_subject_from_body($body)
	{
		$dom = new DOMDocument("1.0", "UTF-8");
		$dom->loadHTML($body);
		$html_sxml = simplexml_import_dom($dom);
		return $html_sxml->head->title;
	}
	
	protected function set_content_type_header()
	{
		$this->headers[] = "Content-Type: text/html; charset={$this->encoding}";
		$this->headers[] = "Content-Transfer-Encoding: quoted-printable";
	}

}

?>