<?php

define("MAIL_OK", 0);
define("MAIL_ERROR_SYSTEM", 101);

abstract class mail_base
{

	// Internal
	protected $line_separator = "\r\n";
	protected $encoding = "";
	// User settings
	protected $to = "";
	protected $from = "";
	protected $subject = "";
	protected $body = "";
	protected $params = array();
	protected $headers = array();
	protected $files = array();
	// Data prepared for sending
	protected $subject_prepared = "";
	protected $headers_prepared = "";
	protected $body_prepared = "";

	public function __construct($encoding)
	{
		$this->encoding = $encoding;
	}

	public function get_to()
	{
		return $this->to;
	}

	public function get_from()
	{
		return $this->from;
	}

	public function get_subject()
	{
		return $this->subject;
	}

	public function get_body()
	{
		return $this->body;
	}

	protected function prepare()
	{
		$this->headers_prepared = join($this->line_separator, $this->headers);
		$this->subject_prepared = trim($this->subject);
		foreach ($this->params as $idx => $val)
		{
			$this->subject_prepared = str_replace($idx, $val, $this->subject_prepared);
		}
		$this->subject_prepared = $this->encode_string($this->subject_prepared);
		$this->body_prepared = str_replace("\r\n", "\n", $this->body_prepared);
		$this->body_prepared = str_replace("\n", $this->line_separator, $this->body_prepared);
		return MAIL_OK;
	}

	protected function do_send()
	{
		$ret = mail($this->to, $this->subject_prepared, $this->body_prepared, $this->headers_prepared, "-f {$this->from}");
		/* if (!$ret)
		  {
		  if (!file_exists("mail_errors"))
		  {
		  mkdir("mail_errors", 0777, true);
		  }
		  //file_put_contents("mail_errors/" . substr(md5(rand()), 0, 8) . ".txt", $this->to . "\n\n===\n\n" . $this->subject_prepared . "\n\n===\n\n" . $this->body_prepared . "\n\n===\n\n" . $this->headers_prepared);
		  file_put_contents("mail_errors/to", serialize($this->to));
		  file_put_contents("mail_errors/subject_prepared", serialize($this->subject_prepared));
		  file_put_contents("mail_errors/body_prepared", serialize($this->body_prepared));
		  file_put_contents("mail_errors/headers_prepared", serialize($this->headers_prepared));
		  die();
		  } */
		return $ret ? MAIL_OK : MAIL_ERROR_SYSTEM;
	}

	protected function log()
	{
		return MAIL_OK;
	}

	final public function send()
	{
		$ret = $this->prepare();
		if ($ret != MAIL_OK)
		{
			return $ret;
		}
		$ret = $this->do_send();
		if ($ret != MAIL_OK)
		{
			return $ret;
		}
		$ret = $this->log();
		if ($ret != MAIL_OK)
		{
			return $ret;
		}
		return MAIL_OK;
	}

	protected function encode_string($str)
	{
		$encoding = strtolower($this->encoding);
		return "=?{$encoding}?B?" . base64_encode($str) . "?=";
	}

}

?>