<?php

require_once PATH_MODULE_SITE_LIB . "/mail/mail_base.php";
require_once PATH_MODULE_SITE_LIB . "/mail/mail.int.php";

/**
 * Sends text mail
 */
abstract class mail_text_base extends mail_base implements mail_interface
{

	/**
	 * @var array
	 */
	protected $conditions = array();

	protected $template_path;

	/**
	 * Name of template file
	 *
	 * @var string
	 */
	protected $template_name;

	/**
	 * Specifed mail params will be setted through set methods.
	 *
	 * @param string $to
	 * @param string $from
	 * @param string $template_path
	 * @param string $encoding
	 */
	public function __construct($to, $from, $template_path, $encoding = "UTF-8")
	{
		parent::__construct($encoding);
		$this->to = $to;
		$this->from = $from;
		$this->template_path = $template_path;
		
		$this->headers[] = "From: {$from}";
		$this->headers[] = "Reply-To: {$from}";
		$this->headers[] = "Content-Type: text/plain; charset={$this->encoding}";
	}

	/**
	 * Loads template and sets body and title properties.
	 * 
	 * Title is first string of template file
	 */
	protected function load_template()
	{
		$template_path = $this->template_path . "/" . $this->template_name . ".tpl";
		$template = file_get_contents($template_path);
		$template = explode("\n", $template);
		
		$this->subject = array_shift($template);
		$this->body = implode("\n", $template);
	}

	protected function prepare()
	{
		$this->load_template();
		$this->body_prepared = $this->body;

		foreach ($this->params as $idx => $val)
		{
			$this->body_prepared = str_replace($idx, $val, $this->body_prepared);
		}
		
		foreach ($this->conditions as $condition)
		{
			$this->body_prepared = preg_replace("|\[if='{$condition}'\](.*?)\[\/if='{$condition}'\]|s", "\\1", $this->body_prepared);
		}
		//Clean not complied conditions
		
		$this->body_prepared = preg_replace("|\[if='(.*)'\](.*)\[\/if='(.*)'\]|s", "", $this->body_prepared);
		
		parent::prepare();

		return MAIL_OK;
	}

	/**
	 * @param string $condition
	 */
	protected function set_condtion($condition)
	{
		if (! in_array($condition, $this->conditions))
		{
			$this->conditions[] = $condition;
		}
	}
}

?>