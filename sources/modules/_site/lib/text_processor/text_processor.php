<?php

class text_processor
{

	public static function tidy($html, $safe = null, $flash_opaque = true, $enclose_text = true, $encoding = "UTF8")
	{
		global $config;

		$tidy_config = array(
			"output-xhtml" => true,
			"clean" => true,
			"drop-empty-paras" => true,
			"show-body-only" => true,
			"numeric-entities" => true,
			"newline" => "CRLF",
			"indent" => true,
			"vertical-space" => true,
			"enclose-text" => $enclose_text,
			"error-file" => PATH_LOG . "/tidy_errors.log",
		);
		$html = tidy_parse_string($html, $tidy_config, "UTF8");
		$changed = false;
		if ($safe === true or is_null($safe) and isset($config["safe_html"]) and $config["safe_html"])
		{
			$html = self::html_safe($html);
			$changed = true;
		}
		if ($flash_opaque)
		{
			$html = self::flash_opaque($html);
			$changed = true;
		}
		// Blank paragraphs
		$html = preg_replace("#(<p[^>]*>\s*(&\#160;|&nbsp;)?\s*</p>\s*)+$#i", "", $html);
		$html = preg_replace("#^(<p[^>]*>\s*(&\#160;|&nbsp;)?\s*</p>\s*)+#i", "", $html);
		$html = tidy_parse_string($html, $tidy_config, "UTF8");
		$html = str_replace("\0", "", $html);
		return $html;
	}

	public static function intmarkup_process($intmarkup_text, $safe = null)
	{
		require_once PATH_MODULE_SITE_LIB . "/intmarkup/intmarkup.php";

		global $config;

		$tidy_config = array(
			"output-xhtml" => true,
			"show-body-only" => true,
			"numeric-entities" => true,
			"newline" => "LF",
			"error-file" => PATH_LOG . "/tidy_errors.log",
		);
		$html = intmarkup::process($intmarkup_text);
		$url_regexp = "#(<[^<>]+\s(?:href|src)=\")https?:(//(?:[^/<>]+\.)?{$config["intmarkup_no_protocol_host"]}[/\"])#isu";
		$html = preg_replace($url_regexp, "\\1\\2", $html);
		$html = tidy_parse_string($html, $tidy_config, "UTF8");
		$changed = false;
		if ($safe === true or is_null($safe) and isset($config["safe_html"]) and $config["safe_html"])
		{
			$html = self::html_safe($html);
			$changed = true;
		}
		$html = tidy_parse_string($html, $tidy_config, "UTF8");
		$html = str_replace("\0", "", $html);
		return $html;
	}

	public static function process_cut($html_raw, &$html, &$html_more)
	{
		$html = $html_raw;
		$html_more = "";
		$cut_regexp = "/\<!--\s*cut\s*--\>/i";
		if (preg_match($cut_regexp, $html_raw, $matches))
		{
			$cut_pos = strpos($html_raw, $matches[0]);
			$html = substr($html_raw, 0, $cut_pos);
			$html_more = substr($html_raw, $cut_pos + strlen($matches[0]));
			if (!trim($html_more))
			{
				$html_more = "";
			}
			else
			{
				$html_more = str_replace(array("<P>", "<P ", "</P>"), array("<p>", "<p ", "</p>"), $html_more);
				$p_open_pos = strpos($html_more, "<p>");
				$p_close_pos = strpos($html_more, "</p>");
				if ($p_close_pos !== false and ( $p_open_pos === false or $p_close_pos < $p_open_pos))
				{
					$html_more = "<p>" . $html_more;
				}
				$html_more = preg_replace($cut_regexp, "", $html_more);
			}
		}
	}

	public static function get_preview($html, $char_count = 300)
	{
		$html_short = $html;
		$html_short = preg_replace("#</(p|div|ol|ul|li|blockquote|dl|fieldset|form|h1|h2|h3|h4|h5|h6)>#i", "\\0 ", $html_short);
		$html_short = preg_replace("#<(br|hr)#i", " \\0", $html_short);
		$html_short = strip_tags($html_short);
		$html_short = preg_replace("#[\x20\x09\x0A\x0D]+#", " ", $html_short);
		$len = mb_strlen($html_short);
		$html_short = mb_substr($html_short, 0, $len > $char_count ? $char_count - 3 : $char_count);
		$html_short = preg_replace("*&[^;]{0,}$*", "", $html_short);
		$html_short = preg_replace("#\s+$#", "", $html_short);
		$html_short = $len > $char_count ? $html_short . "…" : $html_short;
		$html_short = self::tidy($html_short, $safe = false, $flash_opaque = true, $enclose_text = false);
		$html_short = str_replace("\0", "", $html_short);
		if (mb_strlen($html_short) > $char_count)
		{
			$html_short = "…";
		}
		return $html_short;
	}

	public static function flash_opaque($html)
	{
		$html = preg_replace("'(<embed[^>]*)\swmode=\'[a-z]+\'\s([^>]*)>'ims", "$1 $2>", $html);
		$html = preg_replace("'(<embed[^>]*)\swmode=\"[a-z]+\"\s([^>]*)>'ims", "$1 $2>", $html);
		$html = preg_replace("'(<param [^>]*)wmode([^>]*>)'ims", "", $html);
		$html = preg_replace("'<embed\s'ims", "<embed wmode=\"opaque\" ", $html);
		$html = preg_replace("'(<object[^>]*>)'ims", "$1<param name=\"wmode\" value=\"opaque\" />", $html);

		return $html;
	}

	public static function html_safe($html)
	{
		$html = preg_replace("'(<embed[^>]*?)(/?)>'ims", "$1 $2>", $html);
		$html = preg_replace("'(<embed[^>]*)\sallowscriptaccess=\'[a-z]+\'\s([^>]*)>'ims", "$1 $2>", $html);
		$html = preg_replace("'(<embed[^>]*)\sallowscriptaccess=\"[a-z]+\"\s([^>]*)>'ims", "$1 $2>", $html);
		$html = preg_replace("'(<param [^>]*)allowscriptaccess([^>]*>)'ims", "", $html);

		// search for and replacement
		$patterns = array(
			"'</?script[^>]*>'ims" => "-script-",
			"'</?style[^>]*>'ims" => "-style-",
			"'</?applet[^>]*>'ims" => "-applet-",
			//"'</?iframe[^>]*>'ims" => "-iframe-", // it is safe (no XSS is possible)
			"'(<[^>]*)allowscriptaccess([^>]*>)'ims" => "$1noscripts$2",
			"'(<[^>]*)expression([^>]*>)'ims" => "$1noexpr$2",
			"'(<[^>]*)behaviou?r([^>]*>)'ims" => "$1nobeh$2",
			"'(<[^>]*)javascript:([^>]*>)'ims" => "$1/noscripts/?$2",
			"'(<[^>]*)vbscript:([^>]*>)'ims" => "$1/noscripts/?$2",
			"'(<[^>]*)livescript:([^>]*>)'ims" => "$1/noscripts/?$2",
			"'(<[^>]*)jscript:([^>]*>)'ims" => "$1/noscripts/?$2", // I'm not sure that this could work...
			"'(<[^>]*)data:([^>]*>)'ims" => "$1/noscripts/?$2", // <EMBED SRC="data:image/svg+xml;base64,PHN2ZyB4bWxuczpzdmc9Imh0dH A6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcv MjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hs aW5rIiB2ZXJzaW9uPSIxLjAiIHg9IjAiIHk9IjAiIHdpZHRoPSIxOTQiIGhlaWdodD0iMjAw IiBpZD0ieHNzIj48c2NyaXB0IHR5cGU9InRleHQvZWNtYXNjcmlwdCI+YWxlcnQoIlh TUyIpOzwvc2NyaXB0Pjwvc3ZnPg==" type="image/svg+xml" AllowScriptAccess="never"></EMBED>
			"'(<[^>]*)moz-([^>]*>)'ims" => "$1noz-$2",
			"'(<[^>]*)(seekSegmentTime|FSCommand)([^>]*>)'ims" => "$1badattr$3",
			"'(<[^>]*)text/x-scriptlet([^>]*>)'ims" => "$1text/plain$2",
			"'(<[^>]*)\son(\w+)\s*=([^>]*>)'ims" => "$1\sno$2=$3",
		);

		foreach ($patterns as $pattern => $replacement)
		{
			do
			{
				$count = 0;
				$html = preg_replace($pattern, $replacement, $html, -1, $count);
			}
			while (0 != $count);
		}

		$html = preg_replace("'<embed\s'ims", "<embed allowscriptaccess=\"never\" ", $html);
		$html = preg_replace("'(<object[^>]*>)'ims", "$1<param name=\"AllowScriptAccess\" value=\"never\" />", $html);

		return $html;
	}

	/**
	 * URL convesion:
	 * http://the-site.ru/ --> the-site.ru
	 * http://the-site.ru/bla/bla/bla/ --> the-site.ru/bla/bla/bla/
	 * http://the-site.ru/bla/bla/bla/bla/bla/ --> the-site.ru/bla/bla/bl...
	 */
	public static function get_url_human($url, $length = 25)
	{
		if (!$parsed = parse_url($url))
		{
			return "";
		}
		$result = (isset($parsed["host"]) && strlen($parsed["host"])) ? $parsed["host"] : "";
		$result .= ( isset($parsed["path"]) && strlen($parsed["path"]) && ($parsed["path"] != "/")) ? $parsed["path"] : "";
		return (mb_strlen($result) > $length) ? mb_substr($result, 0, $length - 3) . "..." : $result;
	}

	public static function clean_string($string)
	{
		return trim(preg_replace("/[\\x00-\\x20]+/", " ", $string));
	}

	// https://habrahabr.ru/post/187778/
	public static function translate_to_en($string, $gost = false)
	{
		if ($gost)
		{
			$replace = array("А" => "A", "а" => "a", "Б" => "B", "б" => "b", "В" => "V", "в" => "v", "Г" => "G", "г" => "g", "Д" => "D", "д" => "d",
				"Е" => "E", "е" => "e", "Ё" => "E", "ё" => "e", "Ж" => "Zh", "ж" => "zh", "З" => "Z", "з" => "z", "И" => "I", "и" => "i",
				"Й" => "I", "й" => "i", "К" => "K", "к" => "k", "Л" => "L", "л" => "l", "М" => "M", "м" => "m", "Н" => "N", "н" => "n", "О" => "O", "о" => "o",
				"П" => "P", "п" => "p", "Р" => "R", "р" => "r", "С" => "S", "с" => "s", "Т" => "T", "т" => "t", "У" => "U", "у" => "u", "Ф" => "F", "ф" => "f",
				"Х" => "Kh", "х" => "kh", "Ц" => "Tc", "ц" => "tc", "Ч" => "Ch", "ч" => "ch", "Ш" => "Sh", "ш" => "sh", "Щ" => "Shch", "щ" => "shch",
				"Ы" => "Y", "ы" => "y", "Э" => "E", "э" => "e", "Ю" => "Iu", "ю" => "iu", "Я" => "Ia", "я" => "ia", "ъ" => "", "ь" => "");
		}
		else
		{
			$specialCases = array("Елена" => "Elena");
			$arStrES = array("ае", "уе", "ое", "ые", "ие", "эе", "яе", "юе", "ёе", "ее", "ье", "ъе", "ый", "ий");
			$arStrOS = array("аё", "уё", "оё", "ыё", "иё", "эё", "яё", "юё", "ёё", "её", "ьё", "ъё", "ый", "ий");
			$arStrRS = array("а$", "у$", "о$", "ы$", "и$", "э$", "я$", "ю$", "ё$", "е$", "ь$", "ъ$", "@", "@");

			$replace = array("А" => "A", "а" => "a", "Б" => "B", "б" => "b", "В" => "V", "в" => "v", "Г" => "G", "г" => "g", "Д" => "D", "д" => "d",
				"Е" => "Ye", "е" => "e", "Ё" => "Ye", "ё" => "e", "Ж" => "Zh", "ж" => "zh", "З" => "Z", "з" => "z", "И" => "I", "и" => "i",
				"Й" => "Y", "й" => "y", "К" => "K", "к" => "k", "Л" => "L", "л" => "l", "М" => "M", "м" => "m", "Н" => "N", "н" => "n",
				"О" => "O", "о" => "o", "П" => "P", "п" => "p", "Р" => "R", "р" => "r", "С" => "S", "с" => "s", "Т" => "T", "т" => "t",
				"У" => "U", "у" => "u", "Ф" => "F", "ф" => "f", "Х" => "Kh", "х" => "kh", "Ц" => "Ts", "ц" => "ts", "Ч" => "Ch", "ч" => "ch",
				"Ш" => "Sh", "ш" => "sh", "Щ" => "Sh", "щ" => "sh", "Ъ" => "", "ъ" => "", "Ы" => "Y", "ы" => "y", "Ь" => "", "ь" => "",
				"Э" => "E", "э" => "e", "Ю" => "Yu", "ю" => "yu", "Я" => "Ya", "я" => "ya", "@" => "y", "$" => "ye");

			if (isset($specialCases[$string]))
			{
				$string = $specialCases[$string];
			}
			$string = str_replace($arStrES, $arStrRS, $string);
			$string = str_replace($arStrOS, $arStrRS, $string);
		}

		return iconv("UTF-8", "UTF-8//IGNORE", strtr($string, $replace));
	}

}

?>