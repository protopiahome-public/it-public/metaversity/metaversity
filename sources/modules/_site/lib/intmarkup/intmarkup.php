<?php

class intmarkup
{

	const OPEN = 1;
	const CLOSE = 0;
	const TOGGLE = -1;

	public static function process($text)
	{
		$result = self::split_paragraphs($text);
		return $result;
	}

	private static function split_paragraphs($text)
	{
		$text = trim(str_replace("\r\n", "\n", $text)) . "\n\n";
		$result = "";
		$offset = 0;
		$started = "";
		$lines = array();
		while (($line = self::get_line($text, $offset)) !== false)
		{
			$trimmed = trim($line);
			if ($started === "code")
			{
				if ($trimmed === "```")
				{
					$result .= self::process_lines_code($lines);
					$lines = array();
					$started = "";
				}
				else
				{
					$lines[] = $line;
				}
			}
			elseif ($started === "p")
			{
				if ($trimmed === "```" and self::code_end_exists($text, $offset) or $trimmed === "")
				{
					$result .= self::process_lines($lines);
					$lines = array();
					$started = $trimmed === "```" ? "code" : "";
				}
				else
				{
					$lines[] = $line;
				}
			}
			else
			{
				if ($trimmed === "```" and self::code_end_exists($text, $offset))
				{
					$started = "code";
				}
				elseif ($trimmed !== "")
				{
					$started = "p";
					$lines[] = $line;
				}
			}
		}
		return $result;
	}

	private static function code_end_exists($text, $offset)
	{
		return preg_match("/\n\s*```\s*\n/", $text, $matches, 0, $offset);
	}

	private static function process_lines($lines)
	{
		$result = "<p>\n";
		$l = sizeof($lines);
		for ($i = 0; $i < $l; ++$i)
		{
			$line = $lines[$i];
			$is_last_line = $i === $l - 1;
			$result .= "\t" . self::process_line($line);
			if (!$is_last_line)
			{
				$result .= "<br/>";
			}
			$result .= "\n";
		}
		$result .= "</p>\n";
		return $result;
	}

	private static function process_lines_code($lines)
	{
		$result = "";
		$result .= "<p><pre><code>";
		$l = sizeof($lines);
		for ($i = 0; $i < $l; ++$i)
		{
			$line = $lines[$i];
			$is_last_line = $i === $l - 1;
			$result .= self::htmlize($line);
			if (!$is_last_line)
			{
				$result .= "\n";
			}
		}
		$result .= "</code></pre></p>";
		return $result;
	}

	private static function get_line($text, &$offset)
	{
		$pos = strpos($text, "\n", $offset);
		if ($pos === false)
		{
			return false;
		}
		$line = substr($text, $offset, $pos - $offset);
		$offset = $pos + 1;
		return $line;
	}

	private static function process_line($text)
	{
		$markup = self::parse_line($text);
		return self::process_markup($text, $markup);
	}

	private static function parse_line($text)
	{
		$markup = array();
		$modifiers = self::get_all_modifiers($text);
		$opened_modifiers = array();
		reset($modifiers);
		$offset = 0;
		do
		{
			list($modifier_name, $modifier_type, $modifier_offset) = current($modifiers);
			if ($modifier_offset < $offset)
			{
				if (next($modifiers))
				{
					continue;
				}
				else
				{
					break;
				}
			}
			$offset = $modifier_offset;
			switch ($modifier_name)
			{
				case "``":
				case "`":
					$end_offset = self::find_ending_modifier($text, $offset + 1, $modifier_name);
					$modifier_length = strlen($modifier_name);
					if ($end_offset and $end_offset > $offset + $modifier_length)
					{
						$markup[$offset] = array($modifier_name, $end_offset);
						$offset = $end_offset + $modifier_length;
					}
					break;
				case "link":
					self::find_link_end($text, $offset, $length, $link_data);
					if ($length !== false)
					{
						$markup[$offset] = array("link", $length, $link_data);
						$offset = $modifier_offset + $length;
					}
					break;
				case "*":
					if ($modifier_type == self::TOGGLE)
					{
						$modifier_type = isset($opened_modifiers[$modifier_name]) ? self::CLOSE : self::OPEN;
					}
					elseif ($modifier_type == self::CLOSE xor isset($opened_modifiers[$modifier_name]))
					{
						break;
					}
					$modifier_length = strlen($modifier_name);
					if ($modifier_type == self::OPEN)
					{
						$opened_modifiers[$modifier_name] = $offset;
						$markup[$offset] = array($modifier_name, $modifier_type);
						$offset = $modifier_offset + $modifier_length;
					}
					elseif ($modifier_type == self::CLOSE)
					{
						unset($opened_modifiers[$modifier_name]);
						$markup[$offset] = array($modifier_name, $modifier_type);
						$offset = $modifier_offset + $modifier_length;
					}
					break;
				default:
					break 2;
			}
			next($modifiers);
		}
		while (true);
		foreach ($opened_modifiers as $modifier_name => $offset)
		{
			unset($markup[$offset]);
		}
		return $markup;
	}

	private static function process_markup($text, $markup)
	{
		$offset = 0;
		$result = "";
		$tags = array(
			"`" => array("<code>", "</code>"),
			"``" => array("<code>", "</code>"),
			"*" => array("<b>", "</b>"),
		);
		foreach ($markup as $start_offset => $data)
		{
			$result .= self::htmlize_and_clean_escaping(substr($text, $offset, $start_offset - $offset), "`*");
			$modifier = $data[0];
			switch ($modifier)
			{
				case "``":
				case "`":
					$end_offset = $data[1];
					$modifier_length = strlen($modifier);
					$result .= $tags[$modifier][0];
					$result .= self::htmlize_and_clean_escaping(substr($text, $start_offset + $modifier_length, $end_offset - $start_offset - $modifier_length), "`");
					$result .= $tags[$modifier][1];
					$offset = $end_offset + $modifier_length;
					break;
				case "*":
					$modifier_type = $data[1];
					$modifier_length = strlen($modifier);
					$result .= $tags[$modifier][$modifier_type == self::OPEN ? 0 : 1];
					$offset = $start_offset + $modifier_length;
					break;
				case "link":
					$link_length = $data[1];
					$link_data = $data[2];
					$specially_processed = false;
					if (sizeof($markup) === 1 and strlen(trim($text)) == $link_length)
					{
						self::process_block_link_specially($link_data, $result, $specially_processed);
					}
					if (!$specially_processed)
					{
						if (isset($link_data["image"]) && $link_data["image"])
						{
							$result .= "<img src=\"";
							$result .= self::htmlize_attr($link_data["link"]);
							$result .= "\"";
							if (isset($link_data["image_width"]))
							{
								$result .= " width=\"" . $link_data["image_width"] . "\"";
							}
							if (isset($link_data["image_height"]))
							{
								$result .= " height=\"" . $link_data["image_height"] . "\"";
							}
							$result .= " alt=\"";
							$result .= self::htmlize_attr($link_data["image_alt"]);
							$result .= "\"/>";
						}
						else
						{
							$result .= "<a href=\"";
							$result .= self::htmlize_attr($link_data["link"]);
							$result .= "\">";
							$result .= self::htmlize($link_data["human"]);
							$result .= "</a>";
						}
					}
					$offset = $start_offset + $link_length;
					break;
			}
		}
		$result .= self::htmlize_and_clean_escaping(substr($text, $offset), "`*");
		return $result;
	}

	private static function get_all_modifiers($text)
	{
		$modifiers = array();
		self::find_link_modifiers($text, $modifiers);
		self::find_code2_modifiers($text, $modifiers);
		self::find_code1_modifiers($text, $modifiers);
		self::find_inline_open_space_modifiers($text, $modifiers);
		self::find_inline_close_space_modifiers($text, $modifiers);
		self::find_inline_open_word_modifiers($text, $modifiers);
		self::find_inline_close_word_modifiers($text, $modifiers);
		self::find_inline_toggle_modifiers($text, $modifiers);
		$modifiers = array_sort($modifiers, 2);
		return $modifiers;
	}

	private static function find_link_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<![\w\\\\])((https?|ftp):\/\/[\w_-])/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array("link", self::OPEN, $matches[$i][1][1]);
		}
	}

	private static function find_code2_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<![\w\\\\`])(``)/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array($matches[$i][1][0], self::OPEN, $matches[$i][1][1]);
		}
	}

	private static function find_code1_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<![\w\\\\`])(`)(?!`)/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array($matches[$i][1][0], self::OPEN, $matches[$i][1][1]);
		}
	}

	private static function find_inline_open_space_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<![^\s])(\*\*|\*)(?![\s\*_])/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array($matches[$i][1][0], self::OPEN, $matches[$i][1][1]);
		}
	}

	private static function find_inline_close_space_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<![\s\*_])(\*\*|\*)(?![^\s])/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array($matches[$i][1][0], self::CLOSE, $matches[$i][1][1]);
		}
	}

	private static function find_inline_open_word_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<=[^\s\w\*\\\\])(\*\*|\*)(?=\w)/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array($matches[$i][1][0], self::OPEN, $matches[$i][1][1]);
		}
	}

	private static function find_inline_close_word_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<=\w)(\*\*|\*)(?=[^\s\w\*])/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array($matches[$i][1][0], self::CLOSE, $matches[$i][1][1]);
		}
	}

	private static function find_inline_toggle_modifiers($text, &$modifiers)
	{
		$count = preg_match_all("/(?<=[^\s\w\*\\\\])(\*\*|\*)(?=[^\s\w\*])/u", $text, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		for ($i = 0; $i < $count; ++$i)
		{
			$modifiers[] = array($matches[$i][1][0], self::TOGGLE, $matches[$i][1][1]);
		}
	}

	private static function find_ending_modifier($text, $offset, $modifier)
	{
		$modifier_quoted = preg_quote($modifier);
		if (!preg_match("/(?<!\\\\)({$modifier_quoted})(?![\w])/u", $text, $matches, PREG_OFFSET_CAPTURE, $offset))
		{
			return false;
		}
		$end_offset = $matches[1][1];
		return $end_offset;
	}

	private static function find_link_end($text, $link_start_offset, &$length, &$link_data)
	{
		$path_chars = "\w%;$:@&=+_.!~*'`(),\\[\\]\\-\\/";
		$GET_chars = $path_chars . "{}|^\"<>?\\\\";
		$protocol_re_part = "https?|ftp";
		$host_re_part = "([\w-]+\.)*[\w][\w-]*";
		$port_re_part = ":\d{1,5}";
		$url_re = "/({$protocol_re_part}):\/\/({$host_re_part})({$port_re_part})?(\/[{$path_chars}]*)?(\?[{$GET_chars}]*)?(#[{$GET_chars}#]*)?/u";
		if (preg_match($url_re, $text, $matches, PREG_OFFSET_CAPTURE, $link_start_offset))
		{
			if ($matches[0][1] != $link_start_offset)
			{
				$length = false;
				return;
			}
			$length = strlen($matches[0][0]);
			$link_data = array();
			$link_data["link"] = substr($text, $link_start_offset, $length);
			$link_data["protocol"] = $matches[1][0];
			$link_data["host"] = $matches[2][0];
			$link_data["port"] = isset($matches[4]) ? $matches[4][0] : "";
			$link_data["path"] = isset($matches[5]) ? $matches[5][0] : "";
			$link_data["params"] = isset($matches[6]) ? $matches[6][0] : "";
			$link_data["hash"] = isset($matches[7]) ? $matches[7][0] : "";
			$link_data["setup_params"] = array();
			$is_image = false;
			if (strpos($link_data["hash"], "##") !== false)
			{
				// @todo strrpos? what about "\##"?
				list($link_data["hash"], $link_data["setup"]) = explode("##", $link_data["hash"], 2);
				$link_data["setup_params"] = explode(",", $link_data["setup"]);
				$link_data["link"] = substr($link_data["link"], 0, strlen($link_data["link"]) - strlen($link_data["setup"]) - 2);
				foreach ($link_data["setup_params"] as $setup_param)
				{
					if (preg_match("/^(\d{1,5})(x|х)(\d{1,5})$/", $setup_param, $setup_param_matches))
					{
						$is_image = true;
						$link_data["image_width"] = (int) $setup_param_matches[1];
						$link_data["image_height"] = (int) $setup_param_matches[3];
						break;
					}
				}
			}
			self::fix_link_terminal_signs($link_data, $length);
			$protocol_length = strlen($matches[1][0]) + 3;
			$link_data["human"] = substr($link_data["link"], $protocol_length);
			if ($link_data["path"] === "/" and $link_data["params"] === "" and $link_data["hash"] === "")
			{
				$link_data["human"] = substr($link_data["human"], 0, -1);
			}
			if (in_array("link", $link_data["setup_params"]))
			{
				$is_image = false;
			}
			elseif (preg_match("/\.(gif|jpe?g|png)$/i", $link_data["path"]) or in_array("image", $link_data["setup_params"]))
			{
				$is_image = true;
			}
			if ($is_image)
			{
				$link_data["image"] = true;
				$path_parts = preg_split("#/#", $link_data["path"], -1, PREG_SPLIT_NO_EMPTY);
				$alt = end($path_parts);
				if (!strlen($alt))
				{
					$alt = $link_data["host"];
				}
				$link_data["image_alt"] = $alt;
			}
		}
		else
		{
			$length = false;
		}
	}

	private static function fix_link_terminal_signs(&$link_data, &$length)
	{
		if (preg_match("/[:;.,?!'\"\\)\\]]+$/", $link_data["link"], $matches))
		{
			$delta = strlen($matches[0]);
			$length -= $delta;
			$link_data["link"] = substr($link_data["link"], 0, -$delta);
			if ($link_data["hash"])
			{
				$link_data["hash"] = substr($link_data["hash"], 0, -$delta);
			}
			elseif ($link_data["params"])
			{
				$link_data["params"] = substr($link_data["params"], 0, -$delta);
			}
			elseif ($link_data["path"])
			{
				$link_data["path"] = substr($link_data["path"], 0, -$delta);
			}
		}
	}

	private static function process_block_link_specially($link_data, &$result, &$specially_processed)
	{
		self::process_block_link_specially_as_video($link_data, $result, $specially_processed);
	}

	private static function process_block_link_specially_as_video($link_data, &$result, &$specially_processed)
	{
		$services = array(
			"youtube.com" => array(
				"regexp" => "#v=([a-zA-Z0-9-\_]{4,40})#",
				"code" => '<iframe width="480" height="270" src="//www.youtube.com/embed/%KEY%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>',
			),
			"youtu.be" => array(
				"regexp" => "#^/([a-zA-Z0-9-]{4,40})#",
				"code" => '<iframe width="480" height="270" src="//www.youtube.com/embed/%KEY%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>',
			),
			"smotri.com" => array(
				"regexp" => "#id=(v[a-zA-Z0-9-]{4,40})#",
				"code" => '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="480" height="270"><param name="movie" value="http://pics.smotri.com/player.swf?file=%KEY%&amp;bufferTime=3&amp;autoStart=false&amp;str_lang=rus&amp;xmlsource=http%3A%2F%2Fpics.smotri.com%2Fcskins%2Fblue%2Fskin_color.xml&amp;xmldatasource=http%3A%2F%2Fpics.smotri.com%2Fskin_ng.xml" /><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="true" /><param name="bgcolor" value="#ffffff" /><embed src="http://pics.smotri.com/player.swf?file=%KEY%&amp;bufferTime=3&amp;autoStart=false&amp;str_lang=rus&amp;xmlsource=http%3A%2F%2Fpics.smotri.com%2Fcskins%2Fblue%2Fskin_color.xml&amp;xmldatasource=http%3A%2F%2Fpics.smotri.com%2Fskin_ng.xml" quality="high" allowscriptaccess="always" allowfullscreen="true" wmode="opaque" width="480" height="270" type="application/x-shockwave-flash"></embed></object>',
			),
			"video.google.com" => array(
				"regexp" => "#docid=([a-zA-Z0-9-]{4,40})#",
				"code" => '<embed src="http://video.google.com/googleplayer.swf?docid=%KEY%&amp;hl=ru&amp;fs=true" width="480" height="270" allowFullScreen="true" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>',
			),
			"vimeo.com" => array(
				"regexp" => "#^/(\d+)#",
				"code" => '<iframe src="//player.vimeo.com/video/%KEY%?color=ffffff" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>',
			),
		);

		$html = null;
		foreach ($services as $service_host => $data)
		{
			if ($link_data["host"] === $service_host or $link_data["host"] === "www." . $service_host)
			{
				if (preg_match($data["regexp"], $link_data["path"] . $link_data["params"], $matches))
				{
					$html = str_replace("%KEY%", $matches[1], $data["code"]);
					break;
				}
			}
		}

		if (!is_null($html))
		{
			$result .= $html;
			$specially_processed = true;
		}
	}

	private static function htmlize_and_clean_escaping($text, $chars = "")
	{
		return self::htmlize(self::clean_escaping($text, $chars));
	}

	private static function clean_escaping($text, $chars = "")
	{
		static $cache = array();
		if (!isset($cache[$chars]))
		{
			$ar = array(array(), array());
			for ($i = 0; $i < strlen($chars); ++$i)
			{
				$char = substr($chars, $i, 1);
				$ar[0][] = '\\' . $char;
				$ar[1][] = $char;
			}
			$cache[$chars] = $ar;
		}
		$ar = $cache[$chars];
		return str_replace($ar[0], $ar[1], $text);
	}

	private static function htmlize($text)
	{
		return htmlspecialchars($text, ENT_NOQUOTES);
	}

	private static function htmlize_attr($text)
	{
		return htmlspecialchars($text, ENT_COMPAT);
	}

}

?>