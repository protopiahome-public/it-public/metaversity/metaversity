<?php

class user_foreign_statistics_xml_page extends base_easy_xml_ctrl
{

	// Settings
	protected $xml_row_name = "statistic";
	protected $user_id;

	public function __construct($user_id)
	{
		die("Not implemented");
		$this->user_id = $user_id;

		parent::__construct();
	}

	public function init()
	{
		
	}

	protected function load_data(select_sql $select_sql = null)
	{
		$select_sql->add_select_fields("*");
		$select_sql->set_DISTINCT_modifier();
		$select_sql->add_from("user_foreign_statistic");
		$select_sql->add_where("user_id = {$this->user_id}");
		$select_sql->add_order("success_percent DESC");
		$select_sql->add_group_by("provider, test_url");
		$this->data = $this->db->fetch_all($select_sql->get_sql());
	}

}

?>