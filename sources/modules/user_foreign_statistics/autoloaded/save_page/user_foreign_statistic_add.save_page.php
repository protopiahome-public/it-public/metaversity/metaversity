<?php

class user_foreign_statistic_add_save_page extends base_save_ctrl
{
	protected $provider;
	protected $test_url;
	protected $success_percent;
	protected $allowed_providers = array("tests", "cardspuzzles");
	public $xss_allowed = true;

	public function start()
	{
		$this->provider = REQUEST("provider");
		if (!in_array($this->provider, $this->allowed_providers))
		{
			return false;
		}

		$this->test_url = REQUEST("test_url");
		if (parse_url($this->test_url) === false)
		{
			return false;
		}

		$this->test_title = REQUEST("test_title");
		if (!strlen($this->test_title))
		{
			return false;
		}

		$this->success_percent = REQUEST("success_percent");
		if (!is_good_num($this->success_percent) || $this->success_percent > 100)
		{
			return false;
		}

		return true;
	}

	public function check()
	{
		if (!$this->user->get_user_id())
		{
			if (POST("login"))
			{
				if (!$this->user->login(POST("login"), POST("password")))
				{
					$this->pass_info->write_error("BAD_LOGIN_OR_PASSWORD");
					$this->pass_info->dump_vars();
					return false;
				}

				// this cookie is no more used in the project
//				if ($this->domain_logic->is_on())
//				{
//					output_buffer::set_cookie("logged_in", 1, 1735689600, $this->domain_logic->get_cookie_path(), $this->domain_logic->get_cookie_domain());
//				}
			}
			else
			{
				$this->set_redirect_url($this->domain_logic->get_users_prefix() . "/foreign-statistic-add/");
				$this->pass_info->dump_vars();
				return false;
			}
		}
		else
		{
			$test_url_escaped = $this->db->escape($this->test_url);

			$last_time = $this->db->get_value("
				SELECT UNIX_TIMESTAMP(add_time)
				FROM user_foreign_statistic
				WHERE provider = '{$this->provider}' AND test_url = '{$test_url_escaped}' AND user_id = {$this->user->get_user_id()}
				ORDER BY add_time DESC
				LIMIT 1
			");

			if ($last_time && ((time() - $last_time) < 60 * 60 * 24 * 14))
			{
				$this->pass_info->write_error("TEST_TIME_LIMIT");
				if (!REQUEST("retpath"))
				{
					$this->set_redirect_url($this->domain_logic->get_students_prefix() . "/results/");
				}

				return false;
			}
		}

		return true;
	}

	public function commit()
	{
		$test_url_escaped = $this->db->escape($this->test_url);
		$test_title_escaped = $this->db->escape($this->test_title);

		$this->db->sql("
			INSERT INTO user_foreign_statistic (user_id, provider, test_url, success_percent, add_time, test_title)
			VALUES ({$this->user->get_user_id()}, '{$this->provider}', '{$test_url_escaped}', '{$this->success_percent}', NOW(), '{$test_title_escaped}')
		");

		return true;
	}

	public function on_after_commit()
	{
		$this->pass_info->write_info("STATISTIC_ADDED");
		if (!REQUEST("retpath"))
		{
			$this->set_redirect_url($this->domain_logic->get_students_prefix() . "/results/");
		}
	}

}

?>