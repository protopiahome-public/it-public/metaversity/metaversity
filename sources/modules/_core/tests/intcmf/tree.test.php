<?php

require_once PATH_INTCMF . "/tree.php";

class tree_test extends base_test
{

	public function set_up()
	{
		
	}

	public function blank_test()
	{
		$in = array();

		$tree = new tree($in);
		$tree->make_tree();
		$real_root = $tree->get_root();
		$this->assert_identical($real_root, array("children" => array()));
	}

	public function basic_test()
	{
		$in = array();
		$in[] = array("id" => "3", "parent_id" => "", "title" => "three");
		$in[] = array("id" => "2", "parent_id" => "", "title" => "two");
		$in[] = array("id" => "5", "parent_id" => "3", "title" => "five");

		$tree = new tree($in, false);
		$tree->make_tree();
		$real_root = $tree->get_root();
		$this->assert_identical(count($real_root["children"]), 2);

		$this->assert_identical(count($real_root["children"][0]["children"]), 1);
		$this->assert_identical(count($real_root["children"][1]["children"]), 0);

		$this->assert_identical($real_root["children"][1]["contents"], array("id" => "2", "title" => "two")
		);

		$this->assert_identical($real_root["children"][0]["parent_id"], "");
		$this->assert_identical($real_root["children"][0]["children"][0]["parent_id"], "3");

		$this->assert_false(isset($real_root["children"][0]["ancestors"]));
	}

	public function ancestors_test()
	{
		$in = array();
		$in[] = array("id" => "3", "parent_id" => "", "title" => "three");
		$in[] = array("id" => "2", "parent_id" => "5", "title" => "two");
		$in[] = array("id" => "5", "parent_id" => "3", "title" => "five");
		$in[] = array("id" => "10", "parent_id" => "", "title" => "three");

		$tree = new tree($in);
		$tree->make_tree();
		$real_root = $tree->get_root();
		$this->assert_identical(count($real_root["children"][0]["ancestors"]), 0);
		$this->assert_identical(count($real_root["children"][0]["children"][0]["ancestors"]), 1);
		$this->assert_identical(count($real_root["children"][0]["children"][0]["children"][0]["ancestors"]), 2);

		$this->assert_identical($real_root["children"][0]["children"][0]["children"][0]["id"], "2");
		$this->assert_identical($real_root["children"][0]["children"][0]["children"][0]["ancestors"][0]["id"], "5");
		$this->assert_identical($real_root["children"][0]["children"][0]["children"][0]["ancestors"][1]["id"], "3");

		$this->assert_identical(count($real_root["children"][1]["ancestors"]), 0);
	}

	public function id_test()
	{
		$in = array();
		$in[] = array("id" => "3", "parent_id" => "", "title" => "three");
		$in[] = array("id" => "2", "parent_id" => "", "title" => "two");
		$in[] = array("id" => "5", "parent_id" => "3", "title" => "five");

		$tree = new tree($in);
		$tree->make_tree();

		$real_node = $tree->get_by_id(5);

		$this->assert_identical($real_node["contents"], array("id" => "5", "title" => "five"));
		$this->assert_identical($real_node["ancestors"][0]["id"], "3");
	}

}

?>