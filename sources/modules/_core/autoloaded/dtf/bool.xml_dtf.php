<?php

class bool_xml_dtf extends base_xml_dtf
{

	/**
	 * @var bool_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		$dtf_node->set_attr("default_value", $this->dtf->get_default_value() ? "1" : "0");
		return $dtf_node;
	}

}

?>