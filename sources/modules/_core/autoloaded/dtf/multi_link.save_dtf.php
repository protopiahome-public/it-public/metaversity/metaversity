<?php

class multi_link_save_dtf extends base_save_dtf
{

	/**
	 * @var multi_link_dtf
	 */
	protected $dtf;
	protected $values = array();
	protected $options;
	protected $checked_count = 0;

	public function check_inner()
	{
		$foreign_table_name = $this->dtf->get_foreign_table_name();
		$foreign_table_id_column = $this->dtf->get_foreign_table_id_column();
		$restrict_sql = $this->dtf->get_restrict_sql();

		$this->options = $this->db->fetch_all("
			SELECT * 
			FROM `{$foreign_table_name}`
			{$restrict_sql}
		", $foreign_table_id_column);

		if ($this->dtf->is_tree_mode())
		{
			require_once PATH_INTCMF . "/tree.php";
			$tree = new tree($this->options);
			$tree->make_tree();
		}

		$values_input = POST_AS_ARRAY($this->field_name);
		if ($this->dtf->get_design() != INTCMF_MULTI_LINK_DTF_DESIGN_CHECKBOXES)
		{
			$values_input_new = array();
			foreach ($values_input as $val)
			{
				$values_input_new[$val] = "1";
			}
			$values_input = $values_input_new;
		}

		foreach ($this->options as $option_id => $option_data)
		{
			if (isset($values_input[$option_id]))
			{
				if (!in_array(intval($values_input[$option_id]), array(0, 1)))
				{
					return false;
				}

				$this->values[$option_id]["checked"] = (int) $values_input[$option_id];

				if ($this->dtf->is_tree_mode())
				{
					if ($this->values[$option_id]["checked"])
					{
						$tree_item = $tree->get_by_id($option_id);
						foreach ($tree_item["ancestors"] as $ancestor)
						{
							if (isset($values_input[$ancestor["id"]]) and $values_input[$ancestor["id"]] == 1)
							{
								$this->values[$ancestor["id"]]["checked"] = 0;
								unset($values_input[$ancestor["id"]]);
							}
						}
					}
				}
			}
			else
			{
				$this->values[$option_id]["checked"] = 0;
			}
		}
		
		foreach ($this->values as $option_id => $value)
		{
			$this->checked_count += $value["checked"] ? 1 : 0;
		}

		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		if ($this->dtf->get_main_table_count_column())
		{
			$update_array[$this->dtf->get_main_table_count_column()] = $this->checked_count;
		}
	}

	public function commit($last_id)
	{
		$link_table_name = $this->dtf->get_link_table_name();
		$link_table_main_table_id_column = $this->dtf->get_link_table_main_table_id_column();
		$link_table_foreign_table_id_column = $this->dtf->get_link_table_foreign_table_id_column();

		foreach ($this->values as $option_id => $value)
		{
			$checked = $value["checked"];
			if ($checked == 0)
			{
				$this->db->sql("
					DELETE FROM	`{$link_table_name}`
					WHERE `{$link_table_main_table_id_column}` = {$last_id}
						AND `{$link_table_foreign_table_id_column}` = {$option_id}
				");
			}
			else
			{
				$this->db->sql("
					INSERT IGNORE INTO `{$link_table_name}`
						(`{$link_table_main_table_id_column}`, `{$link_table_foreign_table_id_column}`)
					VALUES
						({$last_id}, {$option_id})
				");
			}
		}
		
		if ($this->dtf->get_foreign_table_count_column())
		{
			$this->db->sql("
				UPDATE `{$this->dtf->get_foreign_table_name()}` f
				SET f.`{$this->dtf->get_foreign_table_count_column()}` = (
					SELECT COUNT(*)
					FROM `{$this->dtf->get_link_table_name()}` l
					WHERE l.`{$this->dtf->get_link_table_foreign_table_id_column()}`
						= f.`{$this->dtf->get_foreign_table_id_column()}`
				)
			");
		}
	}

}

?>