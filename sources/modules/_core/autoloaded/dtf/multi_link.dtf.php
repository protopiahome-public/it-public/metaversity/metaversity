<?php

define("INTCMF_MULTI_LINK_DTF_DESIGN_CHECKBOXES", "CHECKBOXES");
define("INTCMF_MULTI_LINK_DTF_DESIGN_MULTISELECT", "MULTISELECT");
define("INTCMF_MULTI_LINK_DTF_DESIGN_CHOSEN", "CHOSEN");

class multi_link_dtf extends base_dtf
{

	protected $foreign_table_name = "";
	protected $link_table_name = "";
	protected $foreign_table_id_column = "";
	protected $foreign_table_title_column = "";
	protected $custom_foreign_table_title_column = false;
	protected $link_table_main_table_id_column = "";
	protected $link_table_foreign_table_id_column = "";
	protected $position_column = "";
	protected $restrict_sql = "";
	protected $tree_mode = false;
	protected $foreign_table_parent_id_column = "";
	protected $max_height = false;
	protected $design = INTCMF_MULTI_LINK_DTF_DESIGN_CHECKBOXES;
	protected $main_table_count_column = false;
	protected $foreign_table_count_column = false;
	protected $default_values = array();

	public function __construct($field_name, $field_title, $foreign_table_name, $link_table_name, $link_table_main_table_id_column, $link_table_foreign_table_id_column, $position_column = "position", $restrict_sql = "", $foreign_table_id_column = "id", $foreign_table_title_column = "title", $custom_foreign_table_title_column = false)
	{
		$this->foreign_table_name = $foreign_table_name;
		$this->link_table_name = $link_table_name;
		$this->foreign_table_id_column = $foreign_table_id_column;
		$this->foreign_table_title_column = $foreign_table_title_column;
		$this->custom_foreign_table_title_column = $custom_foreign_table_title_column;
		$this->link_table_main_table_id_column = $link_table_main_table_id_column;
		$this->link_table_foreign_table_id_column = $link_table_foreign_table_id_column;
		$this->position_column = $position_column;
		$this->restrict_sql = $restrict_sql;

		parent::__construct($field_name, $field_title);
	}

	public function set_restrict_sql($restrict_sql)
	{
		$this->restrict_sql = $restrict_sql;
	}

	public function set_tree_mode($foreign_table_parent_id_column = "parent_id")
	{
		$this->tree_mode = true;
		$this->foreign_table_parent_id_column = $foreign_table_parent_id_column;
	}

	public function set_max_height($height)
	{
		$this->max_height = $height;
	}

	public function set_design($design)
	{
		$this->design = $design;
	}

	public function set_main_table_count_column($count_column)
	{
		$this->main_table_count_column = $count_column;
	}

	public function set_foreign_table_count_column($count_column)
	{
		$this->foreign_table_count_column = $count_column;
	}
	
	public function set_default_values($default_values)
	{
		$this->default_values = (array) $default_values;
	}

	public function get_foreign_table_name()
	{
		return $this->foreign_table_name;
	}

	public function get_link_table_name()
	{
		return $this->link_table_name;
	}

	public function get_foreign_table_id_column()
	{
		return $this->foreign_table_id_column;
	}

	public function get_foreign_table_title_column()
	{
		return $this->foreign_table_title_column;
	}

	public function is_custom_foreign_table_title_column()
	{
		return $this->custom_foreign_table_title_column;
	}

	public function get_link_table_main_table_id_column()
	{
		return $this->link_table_main_table_id_column;
	}

	public function get_link_table_foreign_table_id_column()
	{
		return $this->link_table_foreign_table_id_column;
	}

	public function get_position_column()
	{
		return $this->position_column;
	}

	public function get_restrict_sql()
	{
		return $this->restrict_sql;
	}

	public function is_tree_mode()
	{
		return $this->tree_mode;
	}

	public function get_foreign_table_parent_id_column()
	{
		return $this->foreign_table_parent_id_column;
	}

	public function get_max_height()
	{
		return $this->max_height;
	}

	public function get_design()
	{
		return $this->design;
	}
	
	public function get_main_table_count_column()
	{
		return $this->main_table_count_column;
	}

	public function get_foreign_table_count_column()
	{
		return $this->foreign_table_count_column;
	}
	
	public function get_default_values()
	{
		return $this->default_values;
	}

}

?>