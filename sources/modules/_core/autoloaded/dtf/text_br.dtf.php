<?php

class text_br_dtf extends base_dtf
{

	private $is_important = false;
	// MySQL TEXT field type restriction is 65535 (2^16 - 1) chars.
	// If you want to use more chars, use MEDIUMTEXT (2^24 - 1) and LARGETEXT (2^32 - 1) types in MySQL
	// and redefine this value through set_max_length()
	private $max_length = 16535;
	private $editor_height = 200;

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_max_length($max_length)
	{
		if (is_good_num($max_length))
		{
			$this->max_length = $max_length;
		}
	}

	public function set_editor_height($editor_height)
	{
		if (is_good_num($editor_height))
		{
			$this->editor_height = $editor_height;
		}
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_max_length()
	{
		return $this->max_length;
	}

	public function get_editor_height()
	{
		return $this->editor_height;
	}

}

?>