<?php

class int_save_dtf extends base_save_dtf
{

	/**
	 * @var int_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$this->value = trim($this->value);
		if ($this->dtf->is_important())
		{
			if ($this->value === "")
			{
				$this->write_error("UNFILLED");
				return false;
			}
		}
		else
		{
			if ($this->value === "")
			{
				$this->value = null;
				return true;
			}
		}
		$positive_tmp = substr($this->value, 0, 1) == "-" ? substr($this->value, 1) : $this->value;
		if (!is_positive_number($positive_tmp))
		{
			$this->write_error("NOT_INT", $this->field_name);
			return false;
		}
		$this->value = (int) $this->value;
		if ($this->value < $this->dtf->get_min_value())
		{
			$this->write_error("TOO_SMALL");
			return false;
		}
		if ($this->value > $this->dtf->get_max_value())
		{
			$this->write_error("TOO_BIG");
			return false;
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		if (is_null($this->value))
		{
			$update_array[$this->field_name] = "NULL";
		}
		else
		{
			$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
		}
	}

}

?>