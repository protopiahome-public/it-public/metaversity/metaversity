<?php

define("INTCMF_DTF_TEXT_HTML_BUTTON_SET_FULL", "full");
define("INTCMF_DTF_TEXT_HTML_BUTTON_SET_SHORT", "short");
define("INTCMF_DTF_TEXT_HTML_BUTTON_SET_FULL_MOBILE", "full-mobile");

class text_html_dtf extends base_dtf
{

	private $is_important = false;
	// MySQL TEXT field type restriction is 65535 (2^16 - 1) chars.
	// If you want to use more chars, use MEDIUMTEXT (2^24 - 1) and LARGETEXT (2^32 - 1) types in MySQL
	// and redefine this value through set_max_length()
	private $max_length = 16535;
	private $editor_height = 200;
	private $ensure_xhtml = true;
	private $html_safe = null;
	private $button_set = INTCMF_DTF_TEXT_HTML_BUTTON_SET_FULL;
	private $cut_column = false;
	private $body_class = "";

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_max_length($max_length)
	{
		if (is_good_num($max_length))
		{
			$this->max_length = $max_length;
		}
	}

	public function set_editor_height($editor_height)
	{
		if (is_good_num($editor_height))
		{
			$this->editor_height = $editor_height;
		}
	}

	public function set_ensure_xhtml($ensure_xhtml)
	{
		$this->ensure_xhtml = $ensure_xhtml ? true : false;
	}

	public function set_html_safe($html_safe = null)
	{
		$this->html_safe = $html_safe;
	}

	public function set_button_set($button_set)
	{
		$this->button_set = $button_set;
	}

	public function set_cut($cut_column)
	{
		$this->cut_column = $cut_column;
	}
	
	public function set_body_class($body_class)
	{
		$this->body_class = $body_class;
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_max_length()
	{
		return $this->max_length;
	}

	public function get_editor_height()
	{
		return $this->editor_height;
	}

	public function get_ensure_xhtml()
	{
		return $this->ensure_xhtml;
	}

	public function is_html_safe()
	{
		return $this->html_safe;
	}

	public function get_button_set()
	{
		return $this->button_set;
	}

	public function get_cut_column()
	{
		return $this->cut_column;
	}
	
	public function get_body_class()
	{
		return $this->body_class;
	}

}

?>