<?php

class password_dtf extends base_dtf
{

	private $min_length = 0;
	private $max_length = 0;

	public function set_min_length($min_length)
	{
		if (is_good_num($min_length))
		{
			$this->min_length = $min_length;
		}
	}

	public function set_max_length($max_length)
	{
		if (is_good_num($max_length))
		{
			$this->max_length = $max_length;
		}
	}

	public function get_min_length()
	{
		return $this->min_length;
	}

	public function get_max_length()
	{
		return $this->max_length;
	}

}

?>