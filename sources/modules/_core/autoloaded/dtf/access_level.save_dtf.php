<?php

class access_level_save_dtf extends base_save_dtf
{

	/**
	 * @var access_level_dtf
	 */
	protected $dtf;
	protected $level;
	protected $status;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		
		$this->level = $_POST[$this->field_name];
		
		if (!$this->dtf->level_exists($this->level))
		{
			$this->write_error("UNEXISTED_VALUE");
			return false;
		}
		
		$this->status = $this->dtf->get_status_from_level($this->level);
		
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->status) . "'";
	}

}

?>