<?php

class multi_link_xml_dtf extends base_xml_dtf
{

	protected $additional_data = array();

	/**
	 * @var multi_link_dtf
	 */
	protected $dtf;

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		// Nothing is required from the main table
	}

	public function load_additional_data($db_row = null, $is_edit_mode = false)
	{
		$foreign_table_name = $this->dtf->get_foreign_table_name();
		$link_table_name = $this->dtf->get_link_table_name();
		$foreign_table_id_column = $this->dtf->get_foreign_table_id_column();
		$foreign_table_title_column = $this->dtf->is_custom_foreign_table_title_column() ? $this->dtf->get_foreign_table_title_column() : "f.`" . $this->dtf->get_foreign_table_title_column() . "`";
		$link_table_main_table_id_column = $this->dtf->get_link_table_main_table_id_column();
		$link_table_foreign_table_id_column = $this->dtf->get_link_table_foreign_table_id_column();
		$position_column = $this->dtf->get_position_column();

		if (isset($db_row["id"]))
		{
			$this->additional_data = $this->db->fetch_all("
				SELECT f.`{$foreign_table_id_column}` as id, {$foreign_table_title_column} as title
				FROM `{$link_table_name}` l 
				JOIN `{$foreign_table_name}` f
					ON f.`{$foreign_table_id_column}` = l.`{$link_table_foreign_table_id_column}`
				WHERE l.`{$link_table_main_table_id_column}` = {$db_row["id"]}
				ORDER BY f.`{$position_column}`
			");
		}
	}

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$foreign_table_name = $this->dtf->get_foreign_table_name();
		$foreign_table_id_column = $this->dtf->get_foreign_table_id_column();
		$foreign_table_title_column = $this->dtf->is_custom_foreign_table_title_column() ? $this->dtf->get_foreign_table_title_column() : "`" . $this->dtf->get_foreign_table_title_column() . "`";
		$position_column = $this->dtf->get_position_column();
		$restrict_sql = $this->dtf->get_restrict_sql();

		$tree_fields_sql = "";
		if ($this->dtf->is_tree_mode())
		{
			$foreign_table_parent_id_column = $this->dtf->get_foreign_table_parent_id_column();
			$tree_fields_sql = ", `{$foreign_table_parent_id_column}`";
		}

		$dtf_node = parent::fill_doctype_xml($parent_node);

		$data = $this->db->fetch_all("
			SELECT `{$foreign_table_id_column}`, {$foreign_table_title_column} title, 
				`{$position_column}`
				{$tree_fields_sql}
			FROM `{$foreign_table_name}`
			{$restrict_sql}
			ORDER BY `{$position_column}`
		");
		if ($this->dtf->is_tree_mode())
		{
			require_once PATH_INTCMF . "/tree.php";
			require_once PATH_INTCMF . "/xml_builder.php";

			$tree = new tree($data, $foreign_table_id_column, $foreign_table_parent_id_column);
			$tree->make_tree();
			xml_builder::tree($dtf_node, $tree, "item");

			// @todo support of default values for tree mode
			$dtf_node->set_attr("tree_mode", 1);
		}
		else
		{
			$default_values = array_flip($this->dtf->get_default_values());
			foreach ($data as $row)
			{
				$item_node = $dtf_node->create_child_node("item")
					->set_attr("id", $row[$foreign_table_id_column])
					->set_attr("title", $row["title"]);
				if (isset($default_values[$row[$foreign_table_id_column]]))
				{
					$item_node->set_attr("is_default", true);
				}
			}
		}

		if ($max_height = $this->dtf->get_max_height())
		{
			$dtf_node->set_attr("max_height", $max_height);
		}

		$dtf_node->set_attr("design", $this->dtf->get_design());

		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node($this->field_name);
		$this->fill_common_xml($dtf_node, $db_row);

		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node("field");
		$dtf_node->set_attr("name", $this->field_name);

		$this->fill_common_xml($dtf_node, $db_row);

		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	protected function fill_common_xml(xnode $dtf_node, $db_row)
	{
		foreach ($this->additional_data as $item_dbrow)
		{
			$item_node = $dtf_node->create_child_node("item");
			$item_node->set_attr("id", $item_dbrow["id"]);
			$item_node->set_attr("title", $item_dbrow["title"]);
		}
		return $dtf_node;
	}

}

?>