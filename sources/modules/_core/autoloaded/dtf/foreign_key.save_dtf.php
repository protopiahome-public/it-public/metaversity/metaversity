<?php

class foreign_key_save_dtf extends base_save_dtf
{

	/**
	 * @var foreign_key_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		$foreign_table_name = $this->dtf->get_foreign_table();
		$foreign_table_pk_column = $this->dtf->get_foreign_table_pk_column();

		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		if ($this->value === "")
		{
			if ($this->dtf->is_important())
			{
				$this->write_error("UNFILLED");
				return false;
			}
			else
			{
				$this->value = null;
				return true;
			}
		}
		else
		{
			$value_escaped = $this->db->escape($this->value);
			if (($restrict_items_check_sql = $this->dtf->get_restrict_items_check_sql()))
			{
				$allowed_value = $this->db->row_exists(str_replace("%s", "'" . $value_escaped . "'", $restrict_items_check_sql));
			}
			else
			{
				$where = $this->dtf->get_where();
				if ($where)
				{
					$where = "AND ({$where})";
				}
				$allowed_value = $this->db->row_exists("SELECT * FROM `{$foreign_table_name}` WHERE {$foreign_table_pk_column} = '{$value_escaped}' {$where}");
			}
			if (!$allowed_value)
			{
				$this->write_error("UNEXISTED_VALUE");
				return false;
			}
			$dependency_fail = false;
			foreach ($this->dtf->get_link_dependency_list() as $link_dependency)
			{
				/* @var $link_dependency foreign_key_dtf_link_dependency_data */
				$select_sql = new select_sql();
				$select_sql->add_select_fields("*");
				$select_sql->add_from($link_dependency->link_table);
				$select_sql->add_where("`{$link_dependency->link_table_main_id_column}` = '{$value_escaped}'");
				// @todo
				// The check is needed that this foreign field is being written.
				// Otherwise take the value from $this->old_db_row.
				$foreign_value = POST($link_dependency->link_table_foreign_id_column);
				$foreign_value_escaped = $this->db->escape($foreign_value);
				$select_sql->add_where("`{$link_dependency->link_table_foreign_id_column}` = '{$foreign_value_escaped}'");
				if (!$foreign_value or !$this->db->row_exists($select_sql->get_sql()))
				{
					$this->write_error("LINK_DEPENDENCY_FAIL", $link_dependency->depends_from_field_name);
					$dependency_fail = true;
				}
			}
			foreach ($this->dtf->get_parent_dependency_list() as $parent_dependency)
			{
				/* @var $parent_dependency foreign_key_dtf_parent_dependency_data */
				$select_sql = new select_sql();
				$select_sql->add_select_fields("*");
				$select_sql->add_from($this->dtf->get_foreign_table());
				$select_sql->add_where("`{$this->dtf->get_foreign_table_pk_column()}` = '{$value_escaped}'");
				// @todo
				// The check is needed that this foreign field is being written.
				// Otherwise take the value from $this->old_db_row.
				$foreign_value = POST($parent_dependency->depends_from_field_name);
				$foreign_value_escaped = $this->db->escape($foreign_value);
				$select_sql->add_where("`{$parent_dependency->foreign_table_foreign_key_column}` = '{$foreign_value_escaped}'");
				if (!$foreign_value or !$this->db->row_exists($select_sql->get_sql()))
				{
					$this->write_error("LINK_DEPENDENCY_FAIL", $parent_dependency->depends_from_field_name);
					$dependency_fail = true;
				}
			}
			return !$dependency_fail;
		}
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		if (is_null($this->value))
		{
			$update_array[$this->field_name] = "NULL";
		}
		else
		{
			$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
		}
	}

	public function commit($last_id)
	{
		if ($this->dtf->get_foreign_table_count_column())
		{
			$value_escaped = $this->db->escape($this->value);
			$old_value_escaped = $this->db->escape($this->old_db_row[$this->field_name]);
			$this->db->sql("
				UPDATE `{$this->dtf->get_foreign_table()}` f
				SET f.`{$this->dtf->get_foreign_table_count_column()}` = (
					SELECT COUNT(*)
					FROM `{$this->db_table}` dt
					WHERE dt.`{$this->dtf->get_field_name()}`
						= f.`{$this->dtf->get_foreign_table_pk_column()}`
				)
				WHERE f.`{$this->dtf->get_foreign_table_pk_column()}` = '{$value_escaped}'
					OR f.`{$this->dtf->get_foreign_table_pk_column()}` = '{$old_value_escaped}'
			");
		}
	}

}

?>