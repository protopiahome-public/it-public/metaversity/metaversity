<?php

class email_dtf extends base_dtf
{

	private $is_important = false;
	private $max_length = 255;

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_max_length($max_length)
	{
		if (is_good_num($max_length))
		{
			$this->max_length = $max_length;
		}
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_max_length()
	{
		return $this->max_length;
	}

}

?>