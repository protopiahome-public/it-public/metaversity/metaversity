<?php

class url_name_save_dtf extends base_save_dtf
{

	/**
	 * @var url_name_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$this->value = trim($this->value);
		if ($this->value === "")
		{
			$this->write_error("UNFILLED");
			return false;
		}
		if (in_array($this->value, $this->dtf->get_deprecated_values()))
		{
			$this->write_error("UNIQUE_CHECK_FAILED", $this->value);
			return false;
		}
		$value_escaped = $this->db->escape($this->value);
		$unique_restriction_sql = $this->dtf->get_unique_restriction() ? (" AND " . $this->dtf->get_unique_restriction() ) : "";
		if (($existing_row = $this->db->get_row("SELECT * FROM {$this->db_table} WHERE `{$this->field_name}` = '{$value_escaped}' {$unique_restriction_sql} AND id <> {$this->document_id}")))
		{
			$this->write_error("UNIQUE_CHECK_FAILED", $existing_row[$this->field_name]);
			return false;
		}
		if (($min_length = $this->dtf->get_min_length()))
		{
			$len = mb_strlen($this->value);
			if ($len < $min_length)
			{
				$this->write_error("TOO_SHORT", $len);
				return false;
			}
		}
		if (($max_length = $this->dtf->get_max_length()))
		{
			$len = mb_strlen($this->value);
			if ($len > $max_length)
			{
				$this->write_error("TOO_LONG", $len);
				return false;
			}
		}
		if (!preg_match("/^[a-zA-Z0-9\-]+$/", $this->value))
		{
			$this->write_error("UNALLOWED_CHARS");
			return false;
		}
		if (strpos($this->value, "--") !== false)
		{
			$this->write_error("TWO_HYPHENS");
			return false;
		}
		if (substr($this->value, 0, 1) == "-" or substr($this->value, -1, 1) == "-")
		{
			$this->write_error("ENDING_HYPHEN");
			return false;
		}
		return true;
	}

	public function update_db_row(&$db_row)
	{
		$db_row[$this->field_name] = $this->value;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
	}

}

?>