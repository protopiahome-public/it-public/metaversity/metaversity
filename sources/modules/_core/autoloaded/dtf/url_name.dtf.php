<?php

class url_name_dtf extends base_dtf
{

	private $url_attribute = false;
	private $url_prefix = false;
	private $min_length = 3;
	private $max_length = 255;
	private $unique_restriction;
	private $deprecated_values = array();

	public function set_url_attribute($url_attribute)
	{
		$this->url_attribute = $url_attribute;
	}

	public function set_url_prefix($url_prefix)
	{
		$this->url_prefix = $url_prefix;
	}

	public function set_min_length($min_length)
	{
		if (is_good_num($min_length))
		{
			$this->min_length = $min_length;
		}
	}

	public function set_max_length($max_length)
	{
		if (is_good_num($max_length))
		{
			$this->max_length = $max_length;
		}
	}
	
	public function set_unique_restriction($unique_restriction)
	{
		$this->unique_restriction = $unique_restriction;
	}

	public function set_deprecated_values($deprecated_values)
	{
		$this->deprecated_values = $deprecated_values;
	}

	public function get_url_attribute()
	{
		return $this->url_attribute;
	}

	public function get_url_prefix()
	{
		return $this->url_prefix;
	}

	public function get_min_length()
	{
		return $this->min_length;
	}

	public function get_max_length()
	{
		return $this->max_length;
	}

	public function get_unique_restriction()
	{
		return $this->unique_restriction;
	}

	public function get_deprecated_values()
	{
		return $this->deprecated_values;
	}

}

?>