<?php

class text_br_save_dtf extends base_save_dtf
{

	/**
	 * @var text_br_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$this->value = str_replace("\r\n", "\n", $this->value);
		$this->value = str_replace("\n\r", "\n", $this->value);
		$this->value = str_replace("\r", "\n", $this->value);
		$this->value = str_replace("\n", "\r\n", $this->value);
		$this->value = preg_replace("/[\\x20\t]+/", " ", $this->value);
		$this->value = preg_replace("/[\\x00-\\x09\\x0B-\\x1F]/", "", $this->value);
		$this->value = preg_replace("/\n\\x20/", "\n", $this->value);
		$this->value = preg_replace("/\n{3,}/", "\n\n", $this->value);
		$this->value = trim($this->value);
		if ($this->dtf->is_important())
		{
			if (trim($this->value) === "")
			{
				$this->write_error("UNFILLED");
				return false;
			}
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$len = mb_strlen($this->value);
			if ($len > $max_length)
			{
				$this->write_error("TOO_LONG", $len);
				return false;
			}
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$write_value = nl2br(htmlspecialchars($this->value));
		$update_array[$this->field_name] = "'" . $this->db->escape($write_value) . "'";
	}

}

?>