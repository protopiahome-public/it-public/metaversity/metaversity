<?php

define("INTCMF_DTF_DEP_ACTION_SHOW", 1);
define("INTCMF_DTF_DEP_ACTION_ENABLE", 2);
define("INTCMF_DTF_DEP_ACTION_IMPORTANT", 4);
define("INTCMF_DTF_DEP_ACTION_DO_NOT_STORE", 8);

abstract class base_dtf
{

	protected $field_type;
	protected $field_name;
	protected $field_title;
	protected $comment = "";
	protected $comment_html = "";
	protected $dependencies = array();
	protected $read_only = false;

	public function __construct($field_name, $field_title)
	{
		$this->field_type = substr(get_class($this), 0, -4);
		$this->field_name = $field_name;
		$this->field_title = $field_title;
	}

	public function set_field_name($field_name)
	{
		$this->field_name = $field_name;
	}

	public function set_field_title($field_title)
	{
		$this->field_title = $field_title;
	}

	public function set_comment($comment)
	{
		$this->comment = $comment;
	}
	
	public function set_comment_html($comment_html)
	{
		$this->comment_html = $comment_html;
	}

	public function set_read_only($read_only = true)
	{
		$this->read_only = $read_only;
	}

	public function get_comment()
	{
		return $this->comment;
	}
	
	public function get_comment_html()
	{
		return $this->comment_html;
	}

	public function get_field_name()
	{
		return $this->field_name;
	}

	public function get_field_title()
	{
		return $this->field_title;
	}

	public function get_field_type()
	{
		return $this->field_type;
	}

	// @todo #equal - see wordsfromtext
	public function add_dependency($inspected_column, $activation_value, $actions)
	{
		$this->dependencies[] = array("inspected_column" => $inspected_column, "activation_value" => $activation_value, "actions" => $actions);
	}

	public function get_dependencies()
	{
		require_once PATH_CORE . "/dt/dtf_dependency.php";

		foreach ($this->dependencies as &$dep)
		{
			if (is_array($dep))
			{
				$dep = new dtf_dependency($dep["inspected_column"], $dep["activation_value"], $dep["actions"]);
			}
		}
		return $this->dependencies;
	}

	public function is_read_only()
	{
		return $this->read_only;
	}

}

?>