<?php

class url_string_xml_dtf extends base_xml_dtf
{

	/**
	 * @var url_dtf
	 */
	protected $dtf;

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		parent::modify_sql($select_sql, $is_edit_mode);
		if (!is_null($human_view_column = $this->dtf->get_human_view_column()))
		{
			$select_sql->add_select_fields("dt." . $human_view_column);
		}
	}

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		if ($max_length = $this->dtf->get_max_length())
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		parent::fill_xml($parent_node, $db_row);
		if (!is_null($human_view_column = $this->dtf->get_human_view_column()))
		{
			$parent_node->set_attr($human_view_column, $db_row[$human_view_column]);
		}
		return $parent_node;
	}

}

?>