<?php

class string_save_dtf extends base_save_dtf
{

	/**
	 * @var string_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$this->value = preg_replace("/[\\x00-\\x1F]/", "", $this->value);
		if ($this->dtf->get_trim_before_save())
		{
			$this->value = trim($this->value);
		}
		if ($this->dtf->is_important())
		{
			if ($this->value === "")
			{
				$this->write_error("UNFILLED");
				return false;
			}
		}
		if ($this->dtf->get_unique())
		{
			$value_escaped = $this->db->escape($this->value);
			if ($this->db->row_exists("SELECT * FROM {$this->db_table} WHERE `{$this->field_name}` = '{$value_escaped}' AND id <> {$this->document_id}"))
			{
				$this->write_error("UNIQUE_CHECK_FAILED");
				return false;
			}
		}
		if ($min_length = $this->dtf->get_min_length())
		{
			$len = mb_strlen($this->value);
			if ($len < $min_length)
			{
				$this->write_error("TOO_SHORT", $len);
				return false;
			}
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$len = mb_strlen($this->value);
			if ($len > $max_length)
			{
				$this->write_error("TOO_LONG", $len);
				return false;
			}
		}
		if ($this->value and $regexp = $this->dtf->get_regexp())
		{
			if (!preg_match($regexp, $this->value))
			{
				$this->write_error("REGEXP_INCOMPATIBILITY");
				return false;
			}
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
	}

}

?>