<?php

class calc_dtf extends base_dtf
{

	private $sql;

	public function __construct($field_name, $field_title, $sql)
	{
		$this->sql = $sql;
		parent::__construct($field_name, $field_title);
	}

	public function get_sql()
	{
		return $this->sql;
	}

}

?>