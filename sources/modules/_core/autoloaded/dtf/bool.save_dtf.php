<?php

class bool_save_dtf extends base_save_dtf
{

	/**
	 * @var bool_dtf
	 */
	protected $dtf;

	protected $value;

	public function check_inner()
	{
		$this->value = isset($_POST[$this->field_name]) ? 1 : 0;
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
	}

}

?>