<?php

class email_save_dtf extends base_save_dtf
{

	const EMAIL_REGEXP = "/^[a-zA-Z0-9_+.-]+@[a-zA-Z0-9-.]+$/";
	/**
	 * @var email_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$this->value = preg_replace("/[\\x00-\\x1F]/", "", $this->value);
		$this->value = trim($this->value);
		if ($this->dtf->is_important())
		{
			if ($this->value === "")
			{
				$this->write_error("UNFILLED");
				return false;
			}
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$len = mb_strlen($this->value);
			if ($len > $max_length)
			{
				$this->write_error("TOO_LONG", $len);
				return false;
			}
		}
		if ($this->value)
		{
			if (!preg_match(self::EMAIL_REGEXP, $this->value))
			{
				$this->write_error("INCORRECT_EMAIL");
				return false;
			}
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
	}

}

?>