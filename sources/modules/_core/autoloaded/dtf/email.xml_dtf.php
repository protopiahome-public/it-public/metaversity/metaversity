<?php

class email_xml_dtf extends base_xml_dtf
{

	/**
	 * @var email_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		if ($max_length = $this->dtf->get_max_length())
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		return $dtf_node;
	}
	
	public function fill_xml(xnode $parent_node, $db_row)
	{
		parent::fill_xml($parent_node, $db_row);
		$parent_node->set_attr($this->field_name . "_obscured", str_replace(array("@", "."), array(" sobaka ", " tochka "), $db_row[$this->field_name]));
	}

}

?>