<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class text_html_save_dtf extends base_save_dtf
{

	/**
	 * @var text_html_dtf
	 */
	protected $dtf;
	protected $value;
	protected $value_more;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$this->value = str_replace("\r\n", "\n", $this->value);
		$this->value = str_replace("\n\r", "\n", $this->value);
		$this->value = str_replace("\r", "\n", $this->value);
		$this->value = str_replace("\n", "\r\n", $this->value);
		$this->value = preg_replace("/[\\x20\t]+/", " ", $this->value);
		$this->value = preg_replace("/[\\x00-\\x09\\x0B-\\x1F]/", "", $this->value);
		$this->value = trim($this->value);
		if ($cut_column = $this->dtf->get_cut_column())
		{
			text_processor::process_cut($this->value, $this->value, $this->value_more);
			$this->value = text_processor::tidy($this->value, $this->dtf->is_html_safe());
			$this->value_more = text_processor::tidy($this->value_more, $this->dtf->is_html_safe());
		}
		else
		{
			$this->value = text_processor::tidy($this->value, $this->dtf->is_html_safe());
		}
		if ($this->dtf->is_important())
		{
			if (preg_replace("/[\r\n\x20\xA0]|</?p[^>]>/", "", $this->value) === "")
			{
				$this->write_error("UNFILLED");
				return false;
			}
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$len = mb_strlen($this->value);
			if ($len > $max_length)
			{
				$this->write_error("TOO_LONG", $len);
				return false;
			}
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
		if ($cut_column = $this->dtf->get_cut_column())
		{
			$update_array[$cut_column] = "'" . $this->db->escape($this->value_more) . "'";
		}
	}

}

?>