<?php

class password_xml_dtf extends base_xml_dtf
{

	/**
	 * @var password_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		/* @var $dtf_node xnode */
		if ($min_length = $this->dtf->get_min_length())
		{
			$dtf_node->set_attr("min_length", $min_length);
		}
		if ($max_length = $this->dtf->get_max_length())
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = parent::fill_edit_xml($parent_node, $db_row);
		/* @var $dtf_node xnode */
		$dtf_node->set_text("");
		if (isset($db_row[$this->field_name]))
		{
			if ($db_row[$this->field_name] == "")
			{
				$dtf_node->set_attr("is_blank", "1");
			}
		}
		return $dtf_node;
	}
	
	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		return $parent_node;
	}

}

?>