<?php

define("INTCMF_FOREIGN_KEY_DTF_DEPENDENCY_STRATEGY_DISABLE", "DISABLE");
define("INTCMF_FOREIGN_KEY_DTF_DEPENDENCY_STRATEGY_HIDE", "HIDE");

class foreign_key_dtf_link_dependency_data
{

	public $depends_from_field_name;
	public $link_table;
	public $link_table_main_id_column;
	public $link_table_foreign_id_column;

}

class foreign_key_dtf_parent_dependency_data
{

	public $depends_from_field_name;
	public $foreign_table_foreign_key_column;

}

class foreign_key_dtf_groups_data
{

	public $groups_table;
	public $groups_foreign_table_fk_column;
	public $groups_table_id_column;
	public $groups_table_title_column;
	public $groups_table_order_by_sql;
	public $groups_table_tree_mode_parent_id_column;
	public $groups_table_where;

}

class foreign_key_dtf extends base_dtf
{

	protected $foreign_table = "";
	protected $foreign_table_title_column = "";
	protected $foreign_table_pk_column = "";
	protected $is_important = false;
	protected $where = "";
	protected $order = "";
	protected $restrict_items_read_sql = "";
	protected $restrict_items_check_sql = "";
	/**
	 * @var foreign_key_dtf_groups_data 
	 */
	protected $groups_data = null;
	protected $default_value = null;
	protected $unselected_title = false;
	protected $foreign_table_count_column = false;
	protected $dependency_strategy = INTCMF_FOREIGN_KEY_DTF_DEPENDENCY_STRATEGY_DISABLE;
	protected $link_dependency_list = array();
	protected $parent_dependency_list = array();

	public function __construct($field_name, $field_title, $foreign_table, $foreign_table_title_column = "title", $foreign_table_pk_column = "id")
	{
		$this->foreign_table = $foreign_table;
		$this->foreign_table_title_column = $foreign_table_title_column;
		$this->foreign_table_pk_column = $foreign_table_pk_column;
		parent::__construct($field_name, $field_title);
	}

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_where($where)
	{
		$this->where = $where;
	}
	
	public function set_order($order)
	{
		$this->order = $order;
	}

	public function set_restrict_items_sql($restrict_items_read_sql, $restrict_items_check_sql)
	{
		$this->restrict_items_read_sql = $restrict_items_read_sql;
		$this->restrict_items_check_sql = $restrict_items_check_sql;
	}

	public function set_groups_data($groups_table, $groups_foreign_table_fk_column, $groups_table_id_column = "id", $groups_table_title_column = "title", $groups_table_order_by_sql = "title", $groups_table_tree_mode_parent_id_column = null, $groups_table_where = "")
	{
		$this->groups_data = new foreign_key_dtf_groups_data();
		$this->groups_data->groups_table = $groups_table;
		$this->groups_data->groups_foreign_table_fk_column = $groups_foreign_table_fk_column;
		$this->groups_data->groups_table_id_column = $groups_table_id_column;
		$this->groups_data->groups_table_title_column = $groups_table_title_column;
		$this->groups_data->groups_table_order_by_sql = $groups_table_order_by_sql;
		$this->groups_data->groups_table_tree_mode_parent_id_column = $groups_table_tree_mode_parent_id_column;
		$this->groups_data->groups_table_where = $groups_table_where;
	}

	public function set_default_value($default_value)
	{
		$this->default_value = $default_value;
	}

	public function set_unselected_title($unselected_title)
	{
		$this->unselected_title = $unselected_title;
	}

	public function set_foreign_table_count_column($count_column)
	{
		$this->foreign_table_count_column = $count_column;
	}

	public function set_dependency_strategy($dependency_strategy)
	{
		$this->dependency_strategy = $dependency_strategy;
	}

	public function add_link_dependency($depends_from_field_name, $link_table, $link_table_main_id_column, $link_table_foreign_id_column)
	{
		$dependency = new foreign_key_dtf_link_dependency_data();
		$dependency->depends_from_field_name = $depends_from_field_name;
		$dependency->link_table = $link_table;
		$dependency->link_table_main_id_column = $link_table_main_id_column;
		$dependency->link_table_foreign_id_column = $link_table_foreign_id_column;
		$this->link_dependency_list[] = $dependency;
	}

	public function add_parent_dependency($depends_from_field_name, $foreign_table_foreign_key_column)
	{
		$dependency = new foreign_key_dtf_parent_dependency_data();
		$dependency->depends_from_field_name = $depends_from_field_name;
		$dependency->foreign_table_foreign_key_column = $foreign_table_foreign_key_column;
		$this->parent_dependency_list[] = $dependency;
	}

	public function get_foreign_table()
	{
		return $this->foreign_table;
	}

	public function get_foreign_table_pk_column()
	{
		return $this->foreign_table_pk_column;
	}

	public function get_foreign_table_title_column()
	{
		return $this->foreign_table_title_column;
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_where()
	{
		return $this->where;
	}
	
	public function get_order()
	{
		return $this->order;
	}

	public function get_restrict_items_read_sql()
	{
		return $this->restrict_items_read_sql;
	}

	public function get_restrict_items_check_sql()
	{
		return $this->restrict_items_check_sql;
	}
	
	public function get_groups_data()
	{
		return $this->groups_data;
	}

	public function get_default_value()
	{
		return $this->default_value;
	}

	public function get_unselected_title()
	{
		return $this->unselected_title;
	}

	public function get_foreign_table_count_column()
	{
		return $this->foreign_table_count_column;
	}

	public function get_dependency_strategy()
	{
		return $this->dependency_strategy;
	}

	public function get_link_dependency_list()
	{
		return $this->link_dependency_list;
	}

	public function get_parent_dependency_list()
	{
		return $this->parent_dependency_list;
	}

}

?>