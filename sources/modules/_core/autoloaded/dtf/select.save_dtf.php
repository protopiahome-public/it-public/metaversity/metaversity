<?php

class select_save_dtf extends base_save_dtf
{

	/**
	 * @var select_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		$this->value = isset($_POST[$this->field_name]) ? $_POST[$this->field_name] : "";

		if ($this->dtf->is_important() && $this->value === "")
		{
			$this->write_error("UNFILLED");
			return false;
		}

		if (!array_key_exists($this->value, $this->dtf->get_cases()))
		{
			$this->write_error("UNEXISTED_VALUE");
			return false;
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		$update_array[$this->field_name] = "'" . $this->db->escape($this->value) . "'";
	}

}

?>