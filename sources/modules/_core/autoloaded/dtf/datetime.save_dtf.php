<?php

class datetime_save_dtf extends base_save_dtf
{

	/**
	 * @var datetime_dtf
	 */
	protected $dtf;
	protected $hide;
	protected $year;
	protected $month;
	protected $day;
	protected $hours = 0;
	protected $minutes = 0;
	protected static $times = array();

	public function check_inner()
	{
		$this->hide = $this->dtf->get_hide_by_default() && POST($this->field_name . "_hide") == "1";
		if (!$this->hide)
		{
			/* Year, month, day */
			$this->year = (int) POST($this->field_name . "_year");
			$this->month = (int) POST($this->field_name . "_month");
			$this->day = (int) POST($this->field_name . "_day");
			if ($this->year == 0 or $this->month == 0 or $this->day == 0)
			{
				if ($this->dtf->is_important())
				{
					$this->write_error("INCORRECT_VALUE");
					return false;
				}
				elseif ($this->year != 0 or $this->month != 0 or $this->day != 0)
				{
					$this->write_error("PARTIALLY_FILLED");
					return false;
				}
			}

			if ($this->year)
			{
				if ($this->year < datetime_dtf::min_allowed_year)
				{
					$this->write_error("UNALLOWED_YEAR");
					return false;
				}
				if ($this->year > datetime_dtf::max_allowed_year)
				{
					$this->write_error("UNALLOWED_YEAR");
					return false;
				}
				if ($this->month < 1 or $this->month > 12)
				{
					$this->write_error("UNALLOWED_MONTH");
					return false;
				}
				if ($this->day < 1 or $this->day > 31)
				{
					$this->write_error("UNALLOWED_DAY");
					return false;
				}
				$max_allowed_day = (int) date("t", mktime(1, 0, 0, $this->month, 1, $this->year));
				if ($this->day > $max_allowed_day)
				{
					$this->day = $max_allowed_day;
				}
			}

			/* Hours, minutes */
			if ($this->dtf->is_date_only())
			{
				$this->hours = 12;
				$this->minutes = 0;
			}
			else
			{
				$this->hours = POST($this->field_name . "_hours");
				$this->minutes = POST($this->field_name . "_minutes");
				if ($this->dtf->is_important())
				{
					if ($this->hours === "" or $this->minutes === "")
					{
						$this->write_error("INCORRECT_VALUE");
						return false;
					}
				}
				else
				{
					if ($this->year xor ($this->hours !== "" and $this->minutes !== ""))
					{
						$this->write_error("PARTIALLY_FILLED");
						return false;
					}
				}
				$this->hours = (int) $this->hours;
				$this->minutes = (int) $this->minutes;
				if (!is_good_num($this->hours) or !is_good_num($this->minutes))
				{
					$this->write_error("INCORRECT_VALUE");
					return false;
				}
				if ($this->hours > 23)
				{
					$this->write_error("UNALLOWED_HOURS");
					return false;
				}
				if ($this->minutes > 59)
				{
					$this->write_error("UNALLOWED_MINUTES");
					return false;
				}
			}

			/*  */
			$result_time = $this->year ? mktime($this->hours, $this->minutes, 0, $this->month, $this->day, $this->year) : 0;
			self::$times[$this->field_name] = $result_time;
			if (($check_field_name = $this->dtf->get_must_be_later_than_field_name()) && isset(self::$times[$check_field_name]))
			{
				$can_be_equal = $this->dtf->get_can_be_equal();
				if (self::$times[$check_field_name] == 0)
				{
					$this->write_error("FAILED_DEPENDENCY_UNEXISTED_SOURCE", $check_field_name);
					return false;
				}
				elseif ($result_time < self::$times[$check_field_name] or !$can_be_equal and $result_time == self::$times[$check_field_name])
				{
					$this->write_error("FAILED_DEPENDENCY", $check_field_name);
					return false;
				}
			}
		}

		// OK
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		if ($this->hide or $this->year == 0)
		{
			$update_array[$this->field_name] = "'0000-00-00 00:00:00'";
		}
		else
		{
			$year = $this->year;
			$month = str_pad($this->month, 2, "0", STR_PAD_LEFT);
			$day = str_pad($this->day, 2, "0", STR_PAD_LEFT);
			$hours = str_pad($this->hours, 2, "0", STR_PAD_LEFT);
			$minutes = str_pad($this->minutes, 2, "0", STR_PAD_LEFT);
			$update_array[$this->field_name] = "'{$year}-{$month}-{$day} {$hours}:{$minutes}:00'";
		}
	}

}

?>