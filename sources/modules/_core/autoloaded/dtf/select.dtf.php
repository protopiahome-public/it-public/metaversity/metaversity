<?php

class select_dtf extends base_dtf
{

	private $cases = array();
	private $is_important = false;
	private $default_key = null;
	private $drop_down_view = false;

	public function __construct($field_name, $field_title, $cases = null)
	{
		parent::__construct($field_name, $field_title);
		$this->set_cases($cases);
	}

	public function set_cases($cases)
	{
		if (is_array($cases))
		{
			foreach ($cases as $idx => $val)
			{
				$this->cases[$idx] = strval($val);
			}
		}
	}

	public function set_importance($is_important)
	{
		$this->is_important = $is_important ? true : false;
	}

	public function set_default_key($default_key)
	{
		$this->default_key = $default_key;
	}

	public function set_drop_down_view($drop_down_view)
	{
		$this->drop_down_view = $drop_down_view;
	}

	public function is_important()
	{
		return $this->is_important;
	}

	public function get_cases()
	{
		return $this->cases;
	}

	public function get_default_key()
	{
		return $this->default_key;
	}

	public function get_drop_down_view()
	{
		return $this->drop_down_view;
	}

}

?>