<?php

class password_save_dtf extends base_save_dtf
{

	/**
	 * @var password_dtf
	 */
	protected $dtf;
	protected $value;

	public function check_inner()
	{
		if (!isset($_POST[$this->field_name]) or !isset($_POST[$this->field_name . "_check"]))
		{
			return false;
		}
		$this->value = $_POST[$this->field_name];
		$value_check = $_POST[$this->field_name . "_check"];
		$good_old_password = isset($this->old_db_row[$this->field_name]) && $this->old_db_row[$this->field_name];
		if ($this->value === "" and ($this->document_id == 0 or !$good_old_password)) /* @todo is_new_document method, + $document_id should be private */
		{
			$this->write_error("UNFILLED", $this->field_name);
			return false;
		}
		if ($this->value !== "" or $value_check !== "")
		{
			if ($max_length = $this->dtf->get_max_length())
			{
				$len = mb_strlen($this->value);
				if ($len > $max_length)
				{
					$this->write_error("TOO_LONG", $len);
					return false;
				}
			}
			if ($min_length = $this->dtf->get_min_length())
			{
				$len = mb_strlen($this->value);
				if ($len < $min_length)
				{
					$this->write_error("TOO_SHORT", $len);
					return false;
				}
			}
			if ($this->value != $value_check)
			{
				$this->write_error("PASSWORDS_NOT_EQUAL");
				return false;
			}
		}
		return true;
	}

	protected function get_fields_to_write_inner(&$update_array)
	{
		if ($this->value)
		{
			$password_hash_escaped = $this->db->escape($this->user->get_password_hash($this->value));
			$update_array[$this->field_name] = "'" . $password_hash_escaped . "'";
		}
	}

}

?>