<?php

abstract class base_datetime_xml_dtf extends base_xml_dtf
{

	/**
	 * @var base_datetime_dtf
	 */
	protected $dtf;

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		if (substr($db_row[$this->field_name], 0, 5) === "0000-")
		{
			$dtf_node = $parent_node->set_attr($this->field_name, "");
		}
		elseif (is_null($this->dtf->get_format()))
		{
			$dtf_node = parent::fill_xml($parent_node, $db_row);
		}
		else
		{
			$datetime = $this->db->parse_datetime($db_row[$this->field_name]);
			$formatted = date($this->dtf->get_format(), $datetime);
			$dtf_node = $parent_node->set_attr($this->field_name, $formatted);
		}
		return $dtf_node;
	}

}

?>