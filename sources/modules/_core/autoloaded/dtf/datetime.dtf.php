<?php

class datetime_dtf extends base_datetime_dtf
{
	const min_allowed_year = 1971;
	const max_allowed_year = 2037;

	protected $is_important = true;
	protected $is_blank_default_for_nonimportant = false;
	protected $date_only = false;
	protected $must_be_later_than_field_name = null;
	protected $can_be_equal = null;
	protected $hide_by_default = false;
	protected $edit_mode_past_years_count_for_select = 6;
	protected $edit_mode_future_years_count_for_select = 6;
	
	public function set_importance($is_important, $is_blank_default_for_nonimportant = false)
	{
		$this->is_important = $is_important ? true : false;
		$this->is_blank_default_for_nonimportant = $is_blank_default_for_nonimportant ? true : false;
	}

	public function set_edit_mode_years_count_for_select($past_years_count, $future_years_count)
	{
		if (is_good_num($past_years_count) and is_good_num($future_years_count))
		{
			$this->edit_mode_past_years_count_for_select = $past_years_count;
			$this->edit_mode_future_years_count_for_select = $future_years_count;
		}
	}

	public function set_date_only($date_only)
	{
		$this->date_only = $date_only ? true : false;
		if ($date_only && is_null($this->format))
		{
			$this->format = "Y-m-d";
		}
	}

	public function set_must_be_later($field_name, $can_be_equal = true, $hide_by_default = false)
	{
		$this->must_be_later_than_field_name = $field_name;
		$this->can_be_equal = $can_be_equal;
		$this->hide_by_default = $hide_by_default;
	}

	public function is_date_only()
	{
		return $this->date_only;
	}
	
	public function is_important()
	{
		return $this->is_important;
	}
	
	public function is_blank_default_for_nonimportant()
	{
		return $this->is_blank_default_for_nonimportant;
	}

	public function get_must_be_later_than_field_name()
	{
		return $this->must_be_later_than_field_name;
	}

	public function get_can_be_equal()
	{
		return $this->can_be_equal;
	}
	
	public function get_hide_by_default()
	{
		return $this->hide_by_default;
	}

	public function get_edit_mode_past_years_count_for_select()
	{
		return $this->edit_mode_past_years_count_for_select;
	}

	public function get_edit_mode_future_years_count_for_select()
	{
		return $this->edit_mode_future_years_count_for_select;
	}

}

?>