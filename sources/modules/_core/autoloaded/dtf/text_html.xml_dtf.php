<?php

class text_html_xml_dtf extends base_xml_dtf
{

	/**
	 * @var text_html_dtf
	 */
	protected $dtf;

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		parent::modify_sql($select_sql, $is_edit_mode);
		if ($cut_column = $this->dtf->get_cut_column())
		{
			$select_sql->add_select_fields("dt." . $cut_column);
		}
	}

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$dtf_node = parent::fill_doctype_xml($parent_node);
		if (($max_length = $this->dtf->get_max_length()))
		{
			$dtf_node->set_attr("max_length", $max_length);
		}
		if ($this->dtf->is_important())
		{
			$dtf_node->set_attr("is_important", "1");
		}
		if ($this->dtf->get_cut_column())
		{
			$dtf_node->set_attr("use_cut", "1");
		}
		if (($body_class = $this->dtf->get_body_class()))
		{
			$dtf_node->set_attr("body_class", $body_class);
		}
		$dtf_node->set_attr("button_set", $this->dtf->get_button_set());
		$dtf_node->set_attr("editor_height", $this->dtf->get_editor_height());
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_edit_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node("field");
		$dtf_node->set_attr("name", $this->field_name);
		$text = $db_row[$this->field_name];
		if ($cut_column = $this->dtf->get_cut_column())
		{
			if ($db_row[$cut_column])
			{
				$text .= "<!-- cut -->" . $db_row[$cut_column];
			}
		}
		$dtf_node->set_text($text);
		return $dtf_node;
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = $parent_node->create_child_node($this->field_name);
		$content_xdom = xdom::create_from_string("<root><div class=\"xhtml\">{$db_row[$this->field_name]}</div></root>");
		$dtf_node->import_xdom($content_xdom, true);
		if ($cut_column = $this->dtf->get_cut_column())
		{
			$dtf_cut_node = $parent_node->create_child_node($this->field_name . "_more");
			if ($db_row[$cut_column])
			{
				$content_xdom = xdom::create_from_string("<root><div class=\"xhtml\">{$db_row[$cut_column]}</div></root>");
				$dtf_cut_node->import_xdom($content_xdom, true);
			}
		}
		return $dtf_node;
	}

}

?>