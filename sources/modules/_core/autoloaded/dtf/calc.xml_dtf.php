<?php

class calc_xml_dtf extends base_xml_dtf
{

	/**
	 * @var calc_dtf
	 */
	protected $dtf;

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		$select_sql->add_select_fields($this->dtf->get_sql() . " as " . $this->field_name);
	}

}

?>