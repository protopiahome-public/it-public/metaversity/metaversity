<?php

require_once PATH_INTCMF . "/tree.php";
require_once PATH_INTCMF . "/xml_builder.php";

class foreign_key_xml_dtf extends base_xml_dtf
{

	/**
	 * @var foreign_key_dtf
	 */
	protected $dtf;
	protected $title_column = "";

	/**
	 * @return xnode
	 */
	public function fill_doctype_xml(xnode $parent_node)
	{
		$foreign_table_name = $this->dtf->get_foreign_table();
		$foreign_table_pk_column = $this->dtf->get_foreign_table_pk_column();
		$foreign_table_title_column = $this->dtf->get_foreign_table_title_column();

		$dtf_node = parent::fill_doctype_xml($parent_node);
		/* @var $dtf_node xnode */

		$dtf_node->set_attr_if_not_empty("is_important", $this->dtf->is_important());
		$dtf_node->set_attr_if_not_empty("default_value", $this->dtf->get_default_value());
		$dtf_node->set_attr_if_not_empty("unselected_title", $this->dtf->get_unselected_title());
		if (($restrict_items_read_sql = $this->dtf->get_restrict_items_read_sql()))
		{
			$options_sql = $this->db->sql($restrict_items_read_sql);
		}
		else
		{
			$select_sql = new select_sql();
			$select_sql->add_from($foreign_table_name, "dt");
			$select_sql->add_select_fields("{$foreign_table_pk_column} AS `id`");
			$select_sql->add_select_fields("{$foreign_table_title_column} AS `title`");
			if (($where = $this->dtf->get_where()))
			{
				$select_sql->add_where($where);
			}
			if ($this->dtf->get_order())
			{
				$select_sql->add_order($this->dtf->get_order());
			}
			else
			{
				$select_sql->add_order($foreign_table_title_column);
			}
			foreach ($this->dtf->get_parent_dependency_list() as $parent_dependency)
			{
				/* @var $parent_dependency foreign_key_dtf_parent_dependency_data */
				$select_sql->add_select_fields($parent_dependency->foreign_table_foreign_key_column . " AS DEP_" . $parent_dependency->foreign_table_foreign_key_column);
			}
			if ($this->dtf->get_groups_data())
			{
				$select_sql->add_select_fields("{$this->dtf->get_groups_data()->groups_foreign_table_fk_column} AS `group_id`");
			}
			$options_sql = $select_sql->get_sql();
		}
		$options = $this->db->fetch_all($options_sql);
		$dependency_data = array();
		foreach ($this->dtf->get_parent_dependency_list() as $parent_dependency)
		{
			/* @var $parent_dependency foreign_key_dtf_parent_dependency_data */
			$link_dependency_data = array();
			foreach ($options as $option)
			{
				$key = "DEP_" . $parent_dependency->foreign_table_foreign_key_column;
				if (!isset($link_dependency_data[$option[$key]]))
				{
					$link_dependency_data[$option[$key]] = array();
				}
				$link_dependency_data[$option[$key]][] = $option["id"];
			}
			$dependency_data[$parent_dependency->depends_from_field_name] = $link_dependency_data;
		}
		foreach ($this->dtf->get_link_dependency_list() as $link_dependency)
		{
			/* @var $link_dependency foreign_key_dtf_link_dependency_data */
			$select_sql = new select_sql();
			$select_sql->add_select_fields($link_dependency->link_table_foreign_id_column . " f");
			$select_sql->add_select_fields($link_dependency->link_table_main_id_column . " m");
			$select_sql->add_from($link_dependency->link_table);
			$link_dependency_rows = $this->db->fetch_all($select_sql->get_sql());
			$link_dependency_data = array();
			foreach ($link_dependency_rows as $link_dependency_row)
			{
				if (!isset($link_dependency_data[$link_dependency_row["f"]]))
				{
					$link_dependency_data[$link_dependency_row["f"]] = array();
				}
				$link_dependency_data[$link_dependency_row["f"]][] = $link_dependency_row["m"];
			}
			$dependency_data[$link_dependency->depends_from_field_name] = $link_dependency_data;
		}
		if (sizeof($dependency_data))
		{
			$dtf_node->set_attr("dependency_data", json_encode($dependency_data));
			$dtf_node->set_attr("dependency_strategy", $this->dtf->get_dependency_strategy());
		}
		if ($this->dtf->get_groups_data())
		{
			$groups_data = $this->dtf->get_groups_data();
			$select_sql = new select_sql();
			$select_sql->add_from($groups_data->groups_table);
			$select_sql->add_select_fields("`{$groups_data->groups_table_id_column}` AS id");
			$select_sql->add_select_fields("`{$groups_data->groups_table_title_column}` AS title");
			if ($groups_data->groups_table_tree_mode_parent_id_column)
			{
				$select_sql->add_select_fields("`{$groups_data->groups_table_tree_mode_parent_id_column}` AS parent_id");
			}
			if ($groups_data->groups_table_where)
			{
				$select_sql->add_where($groups_data->groups_table_where);
			}
			if ($groups_data->groups_table_order_by_sql)
			{
				$select_sql->add_order($groups_data->groups_table_order_by_sql);
			}
			else
			{
				$select_sql->add_order($groups_data->groups_table_title_column);
			}
			$groups = $this->db->fetch_all($select_sql->get_sql(), "id");
			foreach ($groups as &$group)
			{
				$group["options"] = array();
			}
			unset($group);
			foreach ($options as $option)
			{
				$groups[$option["group_id"]]["options"][] = $option;
			}
			if ($groups_data->groups_table_tree_mode_parent_id_column)
			{
				$tree = new tree($groups, false);
				xml_builder::tree($dtf_node, $tree, "group", null, array($this, "create_group_node_callback"));
			}
			else
			{
				foreach ($groups as $group_data)
				{
					$group_node = $dtf_node->create_child_node("group");
					// For compatibility with tree mode
					$group_node->set_attr("level", 1);
					$this->create_group_node_callback($group_node, $group_data);
				}
			}
		}
		else
		{
			foreach ($options as $option)
			{
				$dtf_node->create_child_node("item")
					->set_attr("value", $option["id"])
					->set_attr("title", $option["title"]);
			}
		}
		return $dtf_node;
	}

	public function create_group_node_callback(xnode $group_node, $group_data)
	{
		$group_node->set_attr("id", $group_data["id"]);
		$group_node->set_attr("title", $group_data["title"]);
		foreach ($group_data["options"] as $option)
		{
			$group_node->create_child_node("item")
				->set_attr("value", $option["id"])
				->set_attr("title", "    " . $option["title"]);
		}
	}

	public function modify_sql(select_sql $select_sql, $is_edit_mode = false)
	{
		parent::modify_sql($select_sql, $is_edit_mode);

		$foreign_table_name = $this->dtf->get_foreign_table();
		$foreign_table_alias = $this->dtf->get_field_name() . "_foreign_table";
		$foreign_table_pk_column = $this->dtf->get_foreign_table_pk_column();
		$foreign_table_title_column = $this->dtf->get_foreign_table_title_column();
		$field_name = $this->dtf->get_field_name();
		$select_sql->add_join("
			LEFT JOIN `{$foreign_table_name}` {$foreign_table_alias}
			ON {$foreign_table_alias}.{$foreign_table_pk_column} = dt.{$field_name}
		");
		$this->title_column = $this->get_title_column();
		$select_sql->add_select_fields("{$foreign_table_alias}.{$foreign_table_title_column} AS {$this->title_column}");
	}

	protected function get_title_column()
	{
		$default_suffix = "_" . $this->dtf->get_foreign_table_pk_column();
		$l = strlen($default_suffix);
		return substr($this->field_name, -$l, $l) === $default_suffix ? substr($this->field_name, 0, -$l) . "_title" : $this->field_name . "_title";
	}

	/**
	 * @return xnode
	 */
	public function fill_xml(xnode $parent_node, $db_row)
	{
		$dtf_node = parent::fill_xml($parent_node, $db_row);
		/* @var $dtf_node xnode */
		$parent_node->set_attr_if_not_empty($this->title_column, $db_row[$this->title_column]);
		return $dtf_node;
	}

}

?>