<?php

class image_pack_crop_processor extends base_image_pack_processor
{

	protected $width;
	protected $height;
	protected $crop_from_center;

	public function __construct($width, $height, $crop_from_center = false)
	{
		$this->width = $width;
		$this->height = $height;
		$this->crop_from_center = $crop_from_center;
	}

	public function process(ximage $ximage, $tmp_path)
	{
		return $ximage->crop($this->width, $this->height, $tmp_path, $this->crop_from_center);
	}

}

?>
