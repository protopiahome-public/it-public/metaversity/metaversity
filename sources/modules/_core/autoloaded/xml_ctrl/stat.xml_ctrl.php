<?php

class stat_xml_ctrl extends base_xml_ctrl
{

	private $cache_stat;

	public function get_xml()
	{
		$this->cache_stat = $this->xml_loader->get_cache_stat();
		$xml = '<stat>';
		
		// cache stat
		$xml .= '<cache>';
		$xml .= $this->get_cache_state_node(XML_CTRL_CACHE_STATE_NO_INFO, "no_info");
		$xml .= $this->get_cache_state_node(XML_CTRL_CACHE_STATE_CACHE_NO_NEED, "no_need");
		$xml .= $this->get_cache_state_node(XML_CTRL_CACHE_STATE_NO_CACHE, "no_cache");
		$xml .= $this->get_cache_state_node(XML_CTRL_CACHE_STATE_MISS, "miss");
		$xml .= $this->get_cache_state_node(XML_CTRL_CACHE_STATE_CACHED_PARTIALLY, "cached_partially");
		$xml .= $this->get_cache_state_node(XML_CTRL_CACHE_STATE_CACHED, "cached");
		$xml .= $this->get_cache_state_node(XML_CTRL_CACHE_STATE_CACHED_DIRECTLY, "cached_directly");
		$xml .= '</cache>';
		
		// db stat
		$xml .= '<db query_count="' . $this->db->debug_get_query_count() . '"/>';
		
		// time stat
		global $global_start_time;
		$xml .= '<time load="' . (microtime(true) - $global_start_time) . '"/>';
		
		$xml .= '</stat>';
		return $xml;
	}

	private function get_cache_state_node($state_index, $state_name)
	{
		$count = 0;
		$controls = "";
		if (isset($this->cache_stat[$state_index]))
		{
			foreach ($this->cache_stat[$state_index] as $control_name => $number)
			{
				$count += $number;
				if ($controls)
				{
					$controls .= ", ";
				}
				$controls .= $control_name;
				if ($number > 1)
				{
					$controls .= " (" . $number . ")";
				}
			}
		}
		return '<state name="' . $state_name . '" count="' . $count . '" controls="' . $controls . '"/>';
	}

}

?>