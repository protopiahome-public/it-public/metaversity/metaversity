<?php

abstract class base_menu_xml_ctrl extends base_easy_xml_ctrl
{
	/* Settings */
	protected $xml_row_name = "item";

	/* Internal */

	/**
	 * @return
	 * $this->domain_logic->get_main_prefix() . "/folder1"
	 */
	abstract protected function get_prefix();

	/**
	 * @return
	 * array(
	 *   "section_name1" => array("title" => "Title 1", path => "")
	 *   "section_name2" => array("title" => "Title 2", path => "folder1")
	 * )
	 */
	abstract protected function get_menu();

	protected function load_data(select_sql $select_sql = null)
	{
		$prefix = $this->get_prefix();
		$menu = $this->get_menu();

		$this->data = array();
		foreach ($menu as $section_name => $item)
		{
			$full_path = $prefix . "/" . $item["path"];
			if (strlen($item["path"]))
			{
				$full_path .= "/";
			}

			$item = array(
				"section_name" => $section_name,
				"title" => $item["title"],
				"path" => $full_path,
			);

			$this->data[] = $item;
		}
	}

}

?>