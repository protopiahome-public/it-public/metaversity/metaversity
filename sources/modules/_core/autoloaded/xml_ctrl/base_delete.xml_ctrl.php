<?php

abstract class base_delete_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $db_table;
	protected $pk_column = "id";
	protected $title_column = "title";
	// Internal
	protected $id;

	public function __construct($id)
	{
		$this->id = $id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if (!$this->id)
		{
			trigger_error("Incorrect call: id must be defined");
		}
		$select_sql = new select_sql();
		$select_sql->add_select_fields($this->pk_column);
		$select_sql->add_select_fields($this->title_column);
		$select_sql->add_from($this->db_table);
		$select_sql->add_where("id = " . $this->id);
		$this->modify_sql($select_sql);
		$this->data = $this->db->fetch_all($select_sql->get_sql());
		if (empty($this->data))
		{
			$this->set_error_404();
		}
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

	protected function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("action", "delete");
		foreach ($this->data[0] as $name => $value)
		{
			$xdom->set_attr($name, $value);
		}
	}

}

?>