<?php

abstract class base_calendar_xml_ctrl extends base_easy_xml_ctrl
{

	protected $xml_attrs = array("year", "month", "prev_month", "prev_year", "next_month", "next_year");
	protected $year;
	protected $month;
	protected $prev_month;
	protected $prev_year;
	protected $next_month;
	protected $next_year;

	public function __construct($year = false, $month = false)
	{
		parent::__construct();
		$this->year = $year ? $year : date("Y");
		$this->month = $month ? $month : date("n");
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if (!$this->check_date())
		{
			return false;
		}
		$this->data = $this->get_calendar_data($select_sql);
	}

	protected function check_date()
	{
		$date_valid = true;
		if ($this->year < 1970 || $this->year > 2037)
		{
			$date_valid = false;
		}
		if ($this->month > 12)
		{
			$date_valid = false;
		}

		if (!$date_valid)
		{
			$this->set_error_404();
			return false;
		}

		if ($this->month == 12)
		{
			$this->next_month = 1;
			$this->next_year = $this->year + 1;
		}
		else
		{
			$this->next_month = $this->month + 1;
			$this->next_year = $this->year;
		}

		if ($this->month == 1)
		{
			$this->prev_month = 12;
			$this->prev_year = $this->year - 1;
		}
		else
		{
			$this->prev_month = $this->month - 1;
			$this->prev_year = $this->year;
		}

		return true;
	}

	abstract protected function get_calendar_data(select_sql $select_sql);

	public function fill_xml(xdom $xdom)
	{
		$objects = $this->data;

		$days_in_month = date("t", mktime(0, 0, 0, $this->month, 1, $this->year));
		$days = range(1, $days_in_month);
		$days_data = array();
		foreach ($days as $day)
		{
			$timestamp = mktime(0, 0, 0, $this->month, $day, $this->year);
			$weekday = date("w", $timestamp);
			if ($weekday == 0)
			{
				$weekday = 7;
			}

			$object_count = isset($objects["{$this->month}|{$day}"]) ? $objects["{$this->month}|{$day}"]["count"] : 0;

			$days_data[] = array(
				"day" => $day,
				"object_count" => $object_count,
				"weekday" => $weekday
			);
		}

		$prev_month_timestamp = mktime(0, 0, 0, $this->prev_month, 1, $this->prev_year);
		$days_in_prev_month = date("t", $prev_month_timestamp);

		$empty_before = array();
		if ($days_data[0]["weekday"] > 1)
		{
			for ($i = 1; $i < $days_data[0]["weekday"]; $i++)
			{
				$day = $days_in_prev_month + 1 - ($days_data[0]["weekday"] - $i);

				$prev_object_count = isset($objects["{$this->prev_month}|{$day}"]) ? $objects["{$this->prev_month}|{$day}"]["count"] : 0;

				$empty_before[] = array(
					"weekday" => $i,
					"day" => $day,
					"object_count" => $prev_object_count,
					"prev" => true,
				);
			}
			$days_data = array_merge($empty_before, $days_data);
		}

		$empty_after = array();
		$last = end($days_data);
		if ($last["weekday"] < 7)
		{
			for ($i = $last["weekday"] + 1; $i <= 7; $i++)
			{
				$day = $i - $last["weekday"];
				$next_object_count = isset($objects["{$this->next_month}|{$day}"]) ? $objects["{$this->next_month}|{$day}"]["count"] : 0;

				$empty_after[] = array(
					"weekday" => $i,
					"day" => ($day),
					"object_count" => $next_object_count,
					"next" => true,
				);
			}
			$days_data = array_merge($days_data, $empty_after);
		}
		foreach ($days_data as $day_data)
		{
			if ($day_data["weekday"] == 1)
			{
				$week_node = $xdom->create_child_node("week");
			}
			$day_node = $week_node->create_child_node("day");
			if ($day_data["day"])
			{
				$day_node->set_attr("number", $day_data["day"]);

				if (isset($day_data["prev"]))
				{
					$day_node->set_attr("prev", 1);
					$day_node->set_attr("month", $this->prev_month);
					$day_node->set_attr("year", $this->prev_year);
				}
				elseif (isset($day_data["next"]))
				{
					$day_node->set_attr("next", 1);
					$day_node->set_attr("month", $this->next_month);
					$day_node->set_attr("year", $this->next_year);
				}
				else
				{
					$day_node->set_attr("month", $this->month);
					$day_node->set_attr("year", $this->year);
				}

				$day_node->set_attr("object_count", $day_data["object_count"]);
			}
			$day_node->set_attr("weekday", $day_data["weekday"]);
		}

		$months_node = $xdom->create_child_node("months");
		for ($i = 1; $i <= 12; ++$i)
		{
			$months_node->create_child_node("month")->set_attr("number", $i);
		}

		$years_node = $xdom->create_child_node("years");
		for ($i = $this->year - 6; $i <= $this->year + 6; ++$i)
		{
			$years_node->create_child_node("year")->set_attr("number", $i);
		}
	}

}

?>