<?php

class request_xml_ctrl extends base_xml_ctrl
{

	private $cache_stat;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_xml()
	{
		global $config;
		global $lang;
		/* @var $lang lang */

		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$this->cache_stat = $this->xml_loader->get_cache_stat();

		$xdom = xdom::create("request");
		//$xdom->set_attr("prefix", $this->domain_logic->get_main_prefix());
		$xdom->set_attr("lang", $lang->get_current_lang_code());
		$xdom->set_attr("translate_content", isset($_COOKIE[$config["lang_cookie_prefix"] . "translate_content"]) and "1" == $_COOKIE[$config["lang_cookie_prefix"] . "translate_content"]);
		$xdom->set_attr("is_domain_logic_on", $this->domain_logic->is_on());
		$xdom->set_attr("is_main_host_subdomain", $this->domain_logic->is_main_host_subdomain());
		$xdom->set_attr("main_host_display_name", $this->domain_logic->get_main_host_display_name());
		$xdom->set_attr("main_prefix", $this->domain_logic->get_main_prefix());
		$xdom->set_attr("local_prefix", $this->request->get_prefix()); // @todo hack for tinymce, uploadify, etc.
		$xdom->set_attr("users_prefix", $this->domain_logic->get_users_prefix());
		$xdom->set_attr("save_prefix", $this->request->get_prefix() . "/save");
		$xdom->set_attr("ajax_prefix", $this->request->get_prefix() . "/ajax");
		$xdom->set_attr("domain_prefix", $this->request->get_protocol(true) . $this->request->get_host());
		$xdom->set_attr("cookie_domain", $this->domain_logic->get_cookie_domain());
		$xdom->set_attr("cookie_path", $this->domain_logic->get_cookie_path());
		$xdom->set_attr("url", $this->request->get_request_uri());
		$xdom->set_attr("url_base", $this->request->get_prefix() . $this->request->get_folders_string() . "/");
		$xdom->set_attr("request_url_escaped", rawurlencode($this->request->get_full_request_uri()));
		$xdom->set_attr("query_string", isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");
		$xdom->set_attr("debug", $this->xerror->is_debug_mode());
		$xdom->set_attr("user_agent", $this->request->get_user_agent_name());
		$xdom->set_attr("user_agent_version", $this->request->get_user_agent_version());
		$xdom->set_attr("referrer", $this->request->get_referrer());
		$retpath = REQUEST("retpath") ? REQUEST("retpath") : $this->request->get_full_request_uri();
		$xdom->set_attr("retpath", $retpath);
		$xdom->set_attr("retpath_escaped", rawurlencode($retpath));
		$xdom->set_attr("session_name", $this->session->get_session_name());

		if (sizeof($_GET) > 0)
		{
			$params_node = $xdom->create_child_node("params");
			$get_node = $params_node->create_child_node("get");
			foreach ($_GET as $name => $value)
			{
				if (is_array($value))
				{
					foreach ($value as $param_item_key => $param_item_value)
					{
						if (!is_array($param_item_value))
						{
							$get_node->create_child_node("var", $param_item_value)
								->set_attr("name", "{$name}[{$param_item_key}]")
								->set_attr("value_escaped", rawurlencode($param_item_value));
						}
					}
				}
				else
				{
					$get_node->create_child_node("var", $value)
						->set_attr("name", $name)
						->set_attr("value_escaped", rawurlencode($value));
				}
			}
		}

		return $xdom->get_xml(true);
	}

}

?>