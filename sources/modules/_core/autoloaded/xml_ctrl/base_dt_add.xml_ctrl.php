<?php

abstract class base_dt_add_xml_ctrl extends base_dt_edit_xml_ctrl
{

	public function __construct()
	{
		parent::__construct(0);
	}

	protected function check_id()
	{
		if (is_array($this->dt->get_pk_column()))
		{
			trigger_error("'Add' method is not supported for multi-column PK");
		}
		$this->id = 0;
	}

}

?>