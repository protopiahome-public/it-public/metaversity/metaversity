<?php

abstract class base_dt_edit_xml_ctrl extends base_easy_xml_ctrl
{

	// Settings
	protected $dt_name;
	protected $axis_name;
	protected $enable_blocks = false;
	protected $xslt_subdir = "dtf";
	// Internal
	protected $id;
	protected $action;

	/**
	 * @var base_dt
	 */
	protected $dt;
	protected $dtfs = array();
	protected $xml_dtfs = array();

	public function __construct($id)
	{
		$this->id = $id;
		parent::__construct();
	}

	protected function load_data(select_sql $select_sql = null)
	{
		if ($this->is_error_404())
		{
			return;
		}
		$this->dt_init();
		$this->check_id();
		$this->on_after_dt_init();
		if ($this->id)
		{
			$select_sql = new select_sql();
			if (is_array($this->id))
			{
				foreach ($this->id as $column_name => $column_value)
				{
					$select_sql->add_select_fields("dt.{$column_name}");
					$select_sql->add_where("dt.{$column_name} = " . $column_value);
				}
			}
			else
			{
				$select_sql->add_select_fields("dt.{$this->dt->get_pk_column()}");
				$select_sql->add_where("dt.{$this->dt->get_pk_column()} = " . $this->id);
			}
			if ($this->dt->get_add_timestamp_column())
			{
				$select_sql->add_select_fields("dt." . $this->dt->get_add_timestamp_column());
			}
			if ($this->dt->get_edit_timestamp_column())
			{
				$select_sql->add_select_fields("dt." . $this->dt->get_edit_timestamp_column());
			}
			$select_sql->add_from("`" . $this->dt->get_db_table() . "`", "dt");
			foreach ($this->xml_dtfs as $xml_dtf)
			{
				/* @var $xml_dtf base_xml_dtf */
				$xml_dtf->modify_sql($select_sql, true);
			}
			$this->modify_sql($select_sql);
			$this->data = $this->db->fetch_all($select_sql->get_sql());
			if (!empty($this->data))
			{
				foreach ($this->xml_dtfs as $xml_dtf)
				{
					/* @var $xml_dtf base_xml_dtf */
					$xml_dtf->load_additional_data($this->data[0], true);
				}
			}
			elseif (!is_array($this->id))
			{
				$this->set_error_404();
			}
		}
		else
		{
			$this->data = "CREATE";
			foreach ($this->xml_dtfs as $xml_dtf)
			{
				/* @var $xml_dtf base_xml_dtf */
				$xml_dtf->load_additional_data(null, true);
			}
		}
	}

	protected function dt_init()
	{
		$class_reflection = new ReflectionClass($this->dt_name . "_dt");
		$this->dt = $class_reflection->newInstance();
		$this->dtfs = $this->dt->get_fields_by_axis($this->axis_name);
		$this->fill_xml_dtfs();
	}

	protected function check_id()
	{
		if (is_array($this->dt->get_pk_column()))
		{
			$id = $this->id;
			$this->id = array();
			foreach ($this->dt->get_pk_column() as $pk_column_name)
			{
				if (!isset($id[$pk_column_name]))
				{
					trigger_error("Unset id[{$pk_column_name}] in edit mode.");
				}
				elseif (!is_good_id($id[$pk_column_name]))
				{
					trigger_error("Incorrect id[{$pk_column_name}] in edit mode: '" . var_export($id[$pk_column_name], true) . "'. Probably you should use 'add' mode here.");
				}
				else
				{
					$this->id[$pk_column_name] = $id[$pk_column_name];
				}
			}
		}
		elseif (!is_good_id($this->id))
		{
			trigger_error("Incorrect id in edit mode: '" . var_export($this->id, true) . "'. Probably you should use 'add' mode here.");
		}
		$this->action = $this->id ? "dt_edit" : "dt_add";
	}

	protected function on_after_dt_init()
	{
		return;
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

	protected function fill_xml(xdom $xdom)
	{
		$xdom->set_attr("action", $this->action); // Deprecated use; left for compatibility only
		$xdom->set_attr("enable_blocks", $this->enable_blocks);
		$primary_key_node = $xdom->create_child_node("primary_key");
		if (is_array($this->id))
		{
			foreach ($this->id as $column_name => $column_value)
			{
				$primary_key_node->create_child_node("field", $column_value)
					->set_attr("name", $column_name);
			}
		}
		else
		{
			$primary_key_node->create_child_node("field", $this->id)
				->set_attr("name", $this->dt->get_pk_column());
		}
		$doctype_node = $xdom->create_child_node("doctype");
		$doctype_node->set_attr("name", $this->dt->get_name());
		if (is_array($this->data) and !empty($this->data))
		{
			$document_node = $xdom->create_child_node("document");
			if (is_array($this->id))
			{
				foreach ($this->id as $column_name => $column_value)
				{
					$document_node->set_attr($column_name, $column_value);
				}
			}
			else
			{
				$document_node->set_attr($this->dt->get_pk_column(), $this->id);
			}
			foreach ($this->xml_dtfs as $xml_dtf)
			{
				/* @var $xml_dtf base_xml_dtf */
				$xml_dtf->fill_edit_xml($document_node, $this->data[0]);
			}
			if ($column = $this->dt->get_add_timestamp_column())
			{
				$xdom->set_attr($column, $this->data[0][$column]);
			}
			if ($column = $this->dt->get_edit_timestamp_column())
			{
				$xdom->set_attr($column, $this->data[0][$column]);
			}
		}
		if ($this->enable_blocks)
		{
			$blocks = $this->dt->get_blocks($this->axis_name);
			foreach ($blocks as $block_name => $block_data)
			{
				$block_node = $doctype_node->create_child_node("block");
				$block_node->set_attr("name", $block_name);
				$block_node->set_attr("title", $block_data["title"]);
				foreach ($block_data["fields"] as $field_name)
				{
					$xml_dtf = $this->xml_dtfs[$field_name];
					/* @var $xml_dtf base_xml_dtf */
					$xml_dtf->fill_doctype_xml($block_node);
				}
			}
		}
		else
		{
			$block_node = $doctype_node->create_child_node("block")
				->set_attr("is_fake", "1");
			foreach ($this->xml_dtfs as $xml_dtf)
			{
				/* @var $xml_dtf base_xml_dtf */
				$xml_dtf->fill_doctype_xml($block_node);
			}
		}
		$this->add_xslt_files();
	}

	private function fill_xml_dtfs()
	{
		foreach ($this->dtfs as $dtf)
		{
			/* @var $dtf base_dtf */
			$field_name = $dtf->get_field_name();
			$field_type = $dtf->get_field_type();
			$class_reflection = new ReflectionClass($field_type . "_xml_dtf");
			$this->xml_dtfs[$field_name] = $class_reflection->newInstance($dtf, $this->dt->get_db_table(), $this->xml_loader);
		}
	}

	private function add_xslt_files()
	{
		$attached_types = array();
		foreach ($this->dtfs as $field_name => $dtf)
		{
			/* @var $dtf base_dtf */
			$field_type = $dtf->get_field_type();
			if (!isset($attached_types[$field_type]))
			{
				$xml_dtf = $this->xml_dtfs[$field_name];
				/* @var $xml_dtf base_xml_dtf */
				if ($xml_dtf->has_edit_xslt())
				{
					$this->add_xslt($this->xslt_subdir . "/{$field_type}.dtf", "_core");
					$this->add_xslt($this->xslt_subdir . "/{$field_type}.lang.dtf", "_core");
				}
				$attached_types[$field_type] = true;
			}
		}
	}

}

?>