<?php

class pass_info_xml_ctrl extends base_xml_ctrl
{

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		$session_id = $this->session->get_session_id();
		$cache = pass_info_cache::init($session_id);

		$xml = $cache->get($session_id);
		$cache->delete($session_id);
		return $xml ? $xml : "";
	}

}

?>