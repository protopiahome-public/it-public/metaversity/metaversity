<?php

class show_flag_sql_filter extends base_sql_filter
{

	protected $remove_url;
	protected $preserve_get_params;

	public function __construct($name, $title, $remove_url, $preserve_get_params = true)
	{
		$this->remove_url = $remove_url;
		$this->preserve_get_params = $preserve_get_params;
		parent::__construct($name, $title);
	}

	public function fill_is_active()
	{
		$this->is_active = true;
	}

	public function modify_sql(select_sql $select_sql)
	{
		
	}

	public function modify_xml(xnode $parent_node)
	{
		$filter_node = $this->get_filter_node($parent_node);
		$filter_node->set_attr("remove_url", $this->remove_url);
		$filter_node->set_attr("preserve_get_params", $this->preserve_get_params);
	}

}

?>