<?php

class multi_link_or_empty_sql_filter extends multi_link_sql_filter
{

	public function modify_sql(select_sql $select_sql)
	{
		if ($this->value)
		{
			$link_table_alias = "filter_multi_link_{$this->table2}";
			$select_sql->add_join("
				LEFT JOIN {$this->link_table} as {$link_table_alias} 
				ON {$link_table_alias}.{$this->link_table_fk1_column} = {$this->pk_column}
			");
			$select_sql->add_where("
				{$link_table_alias}.{$this->link_table_fk2_column} = '{$this->value}' 
				OR {$link_table_alias}.{$this->link_table_fk2_column} IS NULL
			");
			if ($this->additional_filter_where)
			{
				$select_sql->add_where($this->additional_filter_where);
			}
		}
	}

}

?>