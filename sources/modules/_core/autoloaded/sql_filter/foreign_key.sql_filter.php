<?php

require_once PATH_INTCMF . "/tree.php";
require_once PATH_INTCMF . "/xml_builder.php";

class foreign_key_sql_filter_groups_data
{

	public $groups_table;
	public $groups_foreign_table_fk_column;
	public $groups_table_id_column;
	public $groups_table_title_column;
	public $groups_table_order_by_sql;
	public $groups_table_tree_mode_parent_id_column;
	public $groups_table_where;
	public $groups = null;

	/**
	 * @var tree
	 */
	public $groups_tree = null;

}

class foreign_key_sql_filter extends base_sql_filter
{

	protected $fk_column;
	protected $table;
	protected $table_pk_column;
	protected $table_title_column;
	protected $table_where;
	protected $table_order;
	protected $default_value = null;
	protected $inactive_title = null;
	protected $always_select_unkeyed = false;

	/**
	 * @var foreign_key_sql_filter_groups_data 
	 */
	protected $groups_data = null;
	protected $value = null;
	protected $group_value = null;
	protected $options_grouped = array();

	public function __construct($name, $title, $fk_column, $table, $table_title_column = "title", $table_pk_column = "id", $table_where = "", $table_order = "")
	{
		$this->fk_column = $fk_column;
		$this->table = $table;
		$this->table_title_column = $table_title_column;
		$this->table_pk_column = $table_pk_column;
		$this->table_where = $table_where;
		$this->table_order = $table_order;
		parent::__construct($name, $title);
	}

	public function set_default_value($default_value)
	{
		$this->default_value = $default_value;
	}

	public function set_inactive_title($inactive_title)
	{
		$this->inactive_title = $inactive_title;
	}

	public function set_always_select_unkeyed($always_select_unkeyed = true)
	{
		$this->always_select_unkeyed = $always_select_unkeyed;
	}

	public function set_groups_data($groups_table, $groups_foreign_table_fk_column, $groups_table_id_column = "id", $groups_table_title_column = "title", $groups_table_order_by_sql = "title", $groups_table_tree_mode_parent_id_column = null, $groups_table_where = "")
	{
		$this->groups_data = new foreign_key_sql_filter_groups_data();
		$this->groups_data->groups_table = $groups_table;
		$this->groups_data->groups_foreign_table_fk_column = $groups_foreign_table_fk_column;
		$this->groups_data->groups_table_id_column = $groups_table_id_column;
		$this->groups_data->groups_table_title_column = $groups_table_title_column;
		$this->groups_data->groups_table_order_by_sql = $groups_table_order_by_sql;
		$this->groups_data->groups_table_tree_mode_parent_id_column = $groups_table_tree_mode_parent_id_column;
		$this->groups_data->groups_table_where = $groups_table_where;
		$this->fill_groups();
		$this->fill_groups_tree();
	}

	protected function fill_groups()
	{
		$select_sql = new select_sql();
		$select_sql->add_from($this->groups_data->groups_table);
		$select_sql->add_select_fields("`{$this->groups_data->groups_table_id_column}` AS id");
		$select_sql->add_select_fields("`{$this->groups_data->groups_table_title_column}` AS title");
		if ($this->groups_data->groups_table_tree_mode_parent_id_column)
		{
			$select_sql->add_select_fields("`{$this->groups_data->groups_table_tree_mode_parent_id_column}` AS parent_id");
		}
		if ($this->groups_data->groups_table_where)
		{
			$select_sql->add_where($this->groups_data->groups_table_where);
		}
		if ($this->groups_data->groups_table_order_by_sql)
		{
			$select_sql->add_order($this->groups_data->groups_table_order_by_sql);
		}
		else
		{
			$select_sql->add_order($this->groups_data->groups_table_title_column);
		}
		$this->groups_data->groups = $this->db->fetch_all($select_sql->get_sql(), "id");
		foreach ($this->groups_data->groups as &$group)
		{
			$group["options"] = array();
		}
		unset($group);
	}

	protected function fill_groups_tree()
	{
		if ($this->groups_data->groups_table_tree_mode_parent_id_column)
		{
			$this->groups_data->groups_tree = new tree($this->groups_data->groups, false);
		}
	}

	public function get_default_value()
	{
		return $this->default_value;
	}

	public function get_always_select_unkeyed()
	{
		return $this->always_select_unkeyed;
	}

	public function get_groups_data()
	{
		return $this->groups_data;
	}

	public function fill_is_active()
	{
		$this->value = null;
		$this->group_value = null;
		$value = $this->get_filter_param();
		if ($value === "(any)")
		{
			$this->is_active = !is_null($this->default_value);
		}
		else
		{
			if ($this->groups_data and substr($value, 0, 2) === "g-")
			{
				$group_value_raw = substr($value, 2);
				if (is_good_id($group_value_raw))
				{
					$this->group_value = (int) $group_value_raw;
					$this->is_active = true;
				}
			}
			else
			{
				if (is_good_id($value))
				{
					$this->value = (int) $value;
					$this->is_active = $this->value != $this->default_value;
				}
				if (!$this->value)
				{
					$this->value = $this->default_value;
				}
			}
		}
		//dd($this->value, $this->is_active);
	}

	public function get_value()
	{
		return $this->value;
	}

	public function get_group_value()
	{
		return $this->group_value;
	}

	public function modify_sql(select_sql $select_sql)
	{
		if ($this->value)
		{
			if ($this->always_select_unkeyed)
			{
				$select_sql->add_where("{$this->fk_column} = '{$this->value}' OR {$this->fk_column} IS NULL");
			}
			else
			{
				$select_sql->add_where("{$this->fk_column} = '{$this->value}'");
			}
		}
		if ($this->group_value)
		{
			$allowed_groups = array($this->group_value);
			if ($this->groups_data->groups_table_tree_mode_parent_id_column)
			{
				$this->add_child_groups($allowed_groups, $this->group_value);
			}
			$allowed_values = $this->db->fetch_column_values("
				SELECT {$this->table_pk_column}
				FROM {$this->table}
				WHERE {$this->groups_data->groups_foreign_table_fk_column} 
					IN (" . join(", ", $allowed_groups) . ")
			");
			if (!sizeof($allowed_values))
			{
				$allowed_values = array(0);
			}
			$select_sql->add_where("{$this->fk_column} IN (" . join(", ", $allowed_values) . ")");
		}
	}

	protected function add_child_groups(&$allowed_groups, $parent_group_id)
	{
		$node = $this->groups_data->groups_tree->get_by_id($parent_group_id);
		if (!$node)
		{
			return;
		}
		foreach ($node["children"] as $child_node)
		{
			$allowed_groups[] = $child_node["id"];
			$this->add_child_groups($allowed_groups, $child_node["id"]);
		}
	}

	public function modify_xml(xnode $parent_node)
	{
		$filter_node = $this->get_filter_node($parent_node);
		$filter_node->set_attr_if_not_empty("inactive_title", $this->inactive_title);
		$filter_node->set_attr("value", $this->value);
		$filter_node->set_attr("group_value", $this->group_value);
		$select_sql = new select_sql();
		$select_sql->add_from($this->table);
		$select_sql->add_select_fields("`{$this->table_pk_column}` AS id");
		$select_sql->add_select_fields("`{$this->table_title_column}` AS title");
		$select_sql->add_where($this->table_where);
		if ($this->table_order)
		{
			$select_sql->add_order($this->table_order);
		}
		else
		{
			$select_sql->add_order($this->table_title_column);
		}
		if ($this->groups_data)
		{
			$select_sql->add_select_fields("`{$this->groups_data->groups_foreign_table_fk_column}` AS group_id");
		}
		$options = $this->db->fetch_all($select_sql->get_sql(), $this->table_pk_column);
		$options_node = $filter_node->create_child_node("options");
		if ($this->groups_data)
		{
			$this->options_grouped = array();
			foreach ($options as $option)
			{
				$group_id = $option["group_id"];
				if (!isset($this->options_grouped[$group_id]))
				{
					$this->options_grouped[$group_id] = array();
				}
				$this->options_grouped[$group_id][] = $option;
			}
			if ($this->groups_data->groups_table_tree_mode_parent_id_column)
			{
				xml_builder::tree($options_node, $this->groups_data->groups_tree, "group", null, array($this, "create_group_node_callback"));
			}
			else
			{
				foreach ($this->groups_data->groups as $group_data)
				{
					$group_node = $options_node->create_child_node("group");
					// For compatibility with tree mode
					$group_node->set_attr("level", 1);
					$this->create_group_node_callback($group_node, $group_data);
				}
			}
		}
		else
		{
			foreach ($options as $option)
			{
				$options_node->create_child_node("option")
					->set_attr("id", $option["id"])
					->set_attr("title", $option["title"]);
			}
		}
	}

	public function create_group_node_callback(xnode $group_node, $group_data)
	{
		$group_node->set_attr("id", $group_data["id"]);
		$group_node->set_attr("title", $group_data["title"]);
		if (isset($this->options_grouped[$group_data["id"]]))
		{
			foreach ($this->options_grouped[$group_data["id"]] as $option)
			{
				$group_node->create_child_node("option")
					->set_attr("id", $option["id"])
					->set_attr("title", "    " . $option["title"]);
			}
		}
	}

}

?>