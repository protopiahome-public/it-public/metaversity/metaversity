<?php

abstract class base_sql_filter
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var request
	 */
	protected $request;
	protected $name;
	protected $title;
	protected $reset_on_change = array();
	protected $is_active;

	public function __construct($name, $title)
	{
		global $db;
		$this->db = $db;

		global $request;
		$this->request = $request;

		$this->name = $name;
		$this->title = $title;
	}

	abstract public function fill_is_active();

	abstract public function modify_sql(select_sql $select_sql);

	abstract public function modify_xml(xnode $parent_node);

	public function is_active()
	{
		return $this->is_active;
	}

	/**
	 * @return xdom
	 */
	protected function get_filter_node(xnode $parent_node)
	{
		$type = substr(get_class($this), 0, -11);
		$filter_node = $parent_node->create_child_node("filter");
		$filter_node->set_attr("type", $type);
		$filter_node->set_attr("is_active", $this->is_active());
		$filter_node->set_attr("name", $this->name);
		$filter_node->set_attr("title", $this->title);
		if ($this->reset_on_change)
		{
			$reset_on_change_node = $filter_node->create_child_node("reset_on_change");
			foreach ($this->reset_on_change as $filter_name)
			{
				$reset_on_change_node->create_child_node("filter")
					->set_attr("name", $filter_name);
			}
		}
		return $filter_node;
	}

	protected function get_filter_param($name = null)
	{
		if (is_null($name))
		{
			$name = $this->name;
		}
		$filter = GET_AS_ARRAY("filter");
		return isset($filter[$name]) && !is_array($filter[$name]) ? (string) $filter[$name] : null;
	}

	public static function is_filter_active()
	{
		global $request;
		return sizeof($request->get_get_param_as_array("filter")) ? true : false;
	}

	public function get_name()
	{
		return $this->name;
	}

	public function reset_on_change($filter_name)
	{
		$this->reset_on_change[] = $filter_name;
	}

}

?>