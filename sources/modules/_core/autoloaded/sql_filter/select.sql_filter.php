<?php

class select_sql_filter extends base_sql_filter
{

	protected $options;
	protected $default_value = null;
	protected $inactive_title = null;

	public function __construct($name, $title, $options)
	{
		$this->options = $options;
		parent::__construct($name, $title);
	}

	public function set_default_value($default_value)
	{
		$this->default_value = $default_value;
	}

	public function set_inactive_title($inactive_title)
	{
		$this->inactive_title = $inactive_title;
	}

	public function fill_is_active()
	{
		$this->value = null;
		$value = $this->get_filter_param();
		if ($value === "(any)")
		{
			$this->is_active = !is_null($this->default_value);
		}
		else
		{
			if (isset($this->options[$value]))
			{
				$this->value = $value;
				$this->is_active = $this->value != $this->default_value;
			}
			else
			{
				$this->value = $this->default_value;
			}
		}
	}

	public function get_value()
	{
		return $this->value;
	}

	public function modify_sql(select_sql $select_sql)
	{
		// Do nothing
		// May be, later it will be able to connect with select_dtf
	}

	public function modify_xml(xnode $parent_node)
	{
		$filter_node = $this->get_filter_node($parent_node);
		$filter_node->set_attr_if_not_empty("inactive_title", $this->inactive_title);
		$filter_node->set_attr("value", $this->value);
		$options_node = $filter_node->create_child_node("options");
		foreach ($this->options as $id => $title)
		{
			$option_node = $options_node->create_child_node("option");
			$option_node->set_attr("id", $id);
			$option_node->set_attr("title", $title);
		}
	}

}

?>