<?php

class text_sql_filter extends base_sql_filter
{

	protected $search_fields;
	protected $search;
	protected $placeholder;

	public function __construct($name, $title, $search_fields, $placeholder = "")
	{
		$this->search_fields = is_array($search_fields) ? $search_fields : array($search_fields);
		$this->placeholder = $placeholder;
		parent::__construct($name, $title);
	}

	public function fill_is_active()
	{
		$this->search = $this->get_filter_param();
		$this->is_active = strlen($this->search) ? true : false;
	}

	public function modify_sql(select_sql $select_sql)
	{
		if (strlen($this->search))
		{
			$search_escaped = $this->db->escape($this->search);
			$where = array();
			foreach ($this->search_fields as $field_name)
			{
				$where[] = "{$field_name} LIKE '%{$search_escaped}%'";
			}
			$where = join(" OR ", $where);
			$select_sql->add_where($where);
		}
	}

	public function modify_xml(xnode $parent_node)
	{
		$filter_node = $this->get_filter_node($parent_node);
		$filter_node->set_attr("value", $this->search);
		$filter_node->set_attr("placeholder", $this->placeholder);
	}

}

?>