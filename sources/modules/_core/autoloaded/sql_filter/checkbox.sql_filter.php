<?php

class checkbox_sql_filter extends base_sql_filter
{

	protected $checked_by_default;
	protected $checked_where;
	protected $unchecked_where;
	protected $checked;

	public function __construct($name, $title, $checked_by_default, $checked_where, $unchecked_where = "")
	{
		parent::__construct($name, $title);
		$this->checked_by_default = $checked_by_default;
		$this->checked_where = $checked_where;
		$this->unchecked_where = $unchecked_where;
	}

	public function fill_is_active()
	{
		$this->checked = !is_null($this->get_filter_param()) || !base_sql_filter::is_filter_active() && $this->checked_by_default;
		$this->is_active = $this->checked != $this->checked_by_default;
	}

	public function modify_sql(select_sql $select_sql)
	{
		if ($this->checked && $this->checked_where)
		{
			$select_sql->add_where($this->checked_where);
		}
		elseif (!$this->checked && $this->unchecked_where)
		{
			$select_sql->add_where($this->unchecked_where);
		}
	}

	public function modify_xml(xnode $parent_node)
	{
		$filter_node = $this->get_filter_node($parent_node);
		$filter_node->set_attr("checked", $this->checked);
		$filter_node->set_attr("checked_by_default", $this->checked);
	}

}

?>