<?php

abstract class base_cache
{

	/**
	 * @return cache
	 */
	protected static function get_cache()
	{
		$args = func_get_args();
		$tag_keys = array_pop($args);
		if (!is_array($tag_keys))
		{
			trigger_error("Incorrect usage of cache tags");
		}
		$class_name = array_shift($args);
		$cache_name = substr($class_name, 0, -6);
		array_unshift($args, $cache_name);
		$cache_key = $args;
		global $cache;
		/* @var $cache cache */
		$cache->init($cache_key, $tag_keys);
		return $cache;
	}

}

?>