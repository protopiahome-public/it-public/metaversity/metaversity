<?php

abstract class base_ctrl extends base_mixin
{

	/**
	 * @var xerror
	 */
	protected $xerror;

	/**
	 * @var request
	 */
	protected $request;

	/**
	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	/**
	 * @var db
	 */
	protected $db;
	
	/**
	 * @var x_session
	 */
	protected $session;

	/**
	 * @var user
	 */
	protected $user;
	
	/**
	 * @var lang
	 */
	protected $lang;

	public function __construct()
	{
		global $xerror;
		$this->xerror = $xerror;

		global $request;
		$this->request = $request;
		
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		global $db;
		$this->db = $db;
		
		global $session;
		$this->session = $session;

		global $user;
		$this->user = $user;
		
		global $lang;
		$this->lang = $lang;

		parent::__construct();
	}

	public function init()
	{
		return;
	}

}

?>