<?php

abstract class base_pager_easy_processor extends base_easy_processor
{

	protected $page;
	protected $rows_per_page;
	protected $neighbour_page_count;
	protected $row_count;
	protected $page_count;
	protected $from_row;
	protected $to_row;

	public function __construct($page, $rows_per_page, $neighbour_page_count = 2)
	{
		$this->page = $page;
		$this->rows_per_page = $rows_per_page;
		$this->neighbour_page_count = $neighbour_page_count;
		$this->row_count = 0;
		parent::__construct();
	}

	public function set_row_count($row_count)
	{
		$this->row_count = $row_count;
	}

	protected function calculate()
	{
		$this->from_row = $this->rows_per_page * ($this->page - 1) + 1;
		$this->to_row = $this->rows_per_page * $this->page;
		if ($this->row_count == -1)
		{
			$this->page_count = -1;
		}
		else
		{
			if ($this->to_row > $this->row_count)
			{
				$this->to_row = $this->row_count;
			}
			$this->page_count = ceil($this->row_count / $this->rows_per_page);
		}
	}

	public function modify_xml(xdom $xdom)
	{
		require_once PATH_INTCMF . "/paging.php";
		$paging = new paging($xdom);
		$pages_node = $paging->process($this->page_count, $this->page, $this->neighbour_page_count);
		$pages_node->set_attr("row_count", $this->row_count);
		$pages_node->set_attr("from_row", $this->from_row);
		$pages_node->set_attr("to_row", $this->to_row);

		$this->easy_xml_ctrl->add_xslt("auxil/paging.inc", "_core");
	}

}

?>