<?php

class pager_full_fetch_easy_processor extends base_pager_easy_processor
{
	
	public function process_data(&$data)
	{
		if (!is_array($data))
		{
			return;
		}
		if ($this->row_count == 0)
		{
			$this->row_count = sizeof($data);
		}
		$this->calculate();
		reset($data);
		$i = 1;
		$unset_keys = array ();
		while ($i <= $this->row_count)
		{
			if ($i < $this->from_row or $i > $this->to_row)
			{
				$unset_keys[] = key($data);
			}
			next($data);
			++$i;
		}
		foreach ($unset_keys as $key)
		{
			unset($data[$key]);
		}
		
		if (empty($data) and $this->page != 1)
		{
			$this->easy_xml_ctrl->set_error_404();
		}
	}

}

?>