<?php

class sql_filters_easy_processor extends base_easy_processor
{

	protected $sql_filters;

	public function add_sql_filter(base_sql_filter $sql_filter)
	{
		$name = $sql_filter->get_name();
		$this->sql_filters[$name] = $sql_filter;
		$sql_filter->fill_is_active();
	}

	/**
	 * @return base_sql_filter
	 */
	public function get_filter_by_name($name)
	{
		return $this->sql_filters[$name];
	}

	public function remove_filter($name)
	{
		unset($this->sql_filters[$name]);
	}

	public function modify_sql(select_sql $select_sql)
	{
		foreach ($this->sql_filters as $sql_filter)
		{
			/* @var $sql_filter base_sql_filter */
			$sql_filter->modify_sql($select_sql);
		}
	}

	public function modify_xml(xdom $xdom)
	{
		$filters_node = $xdom->create_child_node("filters");
		$filters_node->set_attr("is_active", base_sql_filter::is_filter_active());
		foreach ($this->sql_filters as $sql_filter)
		{
			/* @var $sql_filter base_sql_filter */
			$sql_filter->modify_xml($filters_node);
		}
	}

	public function disable_cache()
	{
		return base_sql_filter::is_filter_active();
	}

}

?>