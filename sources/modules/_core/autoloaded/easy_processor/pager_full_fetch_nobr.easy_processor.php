<?php

class pager_full_fetch_nobr_easy_processor extends base_pager_easy_processor
{

	protected $nobr_column;

	public function __construct($page, $rows_per_page, $nobr_column, $neighbour_page_count = 2)
	{
		$this->nobr_column = $nobr_column;
		parent::__construct($page, $rows_per_page, $neighbour_page_count);
	}

	public function process_data(&$data)
	{
		if (!is_array($data))
		{
			return;
		}
		$this->row_count = sizeof($data);
		$page_counter = 1;
		$row_counter = 0;
		$total_row_counter = 0;
		$last_value = null;
		$from_row = 0;
		$to_row = 0;
		foreach ($data as $idx => $row)
		{
			++$total_row_counter;
			++$row_counter;
			if ($row_counter > $this->rows_per_page and ( !$last_value or $row[$this->nobr_column] != $last_value))
			{
				++$page_counter;
				$row_counter = 1;
			}
			$last_value = $row[$this->nobr_column];
			if ($page_counter == $this->page)
			{
				if (!$from_row)
				{
					$from_row = $total_row_counter;
				}
				$to_row = $total_row_counter;
			}
			else
			{
				unset($data[$idx]);
			}
		}
		$this->from_row = $from_row;
		$this->to_row = $to_row;
		$this->page_count = $page_counter;

		if (empty($data) and $this->page != 1)
		{
			$this->easy_xml_ctrl->set_error_404();
		}
	}

}

?>