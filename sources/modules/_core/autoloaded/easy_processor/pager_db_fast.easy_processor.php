<?php

class pager_db_fast_easy_processor extends base_pager_easy_processor
{

	public function modify_sql(select_sql $select_sql)
	{
		$limit = ($this->page - 1) * $this->rows_per_page;
		$limit .= ", ";
		$limit .= $this->rows_per_page + 1;
		$select_sql->set_limit($limit);
	}

	public function process_data(&$data)
	{
		if (sizeof($data) > $this->rows_per_page)
		{
			$this->row_count = -1;
			end($data);
			$last_key = key($data);
			unset($data[$last_key]);
		}
		else
		{
			$this->row_count = ($this->page - 1) * $this->rows_per_page + sizeof($data);
		}

		if (empty($data) and $this->page != 1)
		{
			$this->easy_xml_ctrl->set_error_404();
		}
		$this->calculate();
	}

}

?>