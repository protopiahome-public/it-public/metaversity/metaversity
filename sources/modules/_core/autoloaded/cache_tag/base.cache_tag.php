<?php

class base_cache_tag
{

	/**
	 * @return cache_tag
	 */
	public static function get_tag()
	{
		$args = func_get_args();
		$class_name = array_shift($args);
		$tag_name = substr($class_name, 0, -10);
		array_unshift($args, $tag_name);
		$tag_key = $args;
		global $tag;
		/* @var $tag cache_tag */
		$tag->init($tag_key);
		return $tag;
	}

}

?>