<?php

abstract class base_access_save
{

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var user
	 */
	protected $user;
	protected $user_id;

	/**
	 * @var base_access_fetcher
	 */
	protected $access_fetcher;
	protected $level;

	public function __construct($user_id)
	{
		if (!$user_id)
		{
			trigger_error("This class must not be initialized with user_id = 0");
		}
		$this->user_id = $user_id;

		global $db;
		$this->db = $db;

		global $user;
		$this->user = $user;

		if (!$this->db->is_transaction_started())
		{
			trigger_error("Transaction must be already started beyond the *_access_save class");
		}
		if (!$this->fill_data())
		{
			trigger_error("fill_data() returned false");
		}
	}

	abstract protected function fill_data();

	public function is_member_strict()
	{
		return $this->level == ACCESS_LEVEL_MEMBER;
	}

	public function is_moderator_strict()
	{
		return $this->level == ACCESS_LEVEL_MODERATOR;
	}

	public function is_pretender_strict()
	{
		return $this->level == ACCESS_LEVEL_PRETENDER;
	}

	public function is_member()
	{
		return $this->level >= ACCESS_LEVEL_MEMBER;
	}

	public function is_moderator()
	{
		return $this->level >= ACCESS_LEVEL_MODERATOR;
	}

	public function is_admin()
	{
		return $this->level >= ACCESS_LEVEL_ADMIN;
	}

	abstract public function add_member();

	abstract public function add_moderator();

	abstract public function add_admin();

	abstract public function delete_member();

	abstract public function delete_moderator();

	abstract public function delete_admin();

	abstract public function set_moderator_rights($rights_array);

	abstract public function clean_cache();

	public function get_level()
	{
		return $this->level;
	}

}

?>