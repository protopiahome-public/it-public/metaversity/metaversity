<?php

define("ACCESS_LEVEL_ERROR", 0);
define("ACCESS_LEVEL_GUEST", 10);
define("ACCESS_LEVEL_USER", 20);
define("ACCESS_LEVEL_DELETED", 30);
define("ACCESS_LEVEL_PRETENDER", 40);
define("ACCESS_LEVEL_MEMBER", 50);
define("ACCESS_LEVEL_MODERATOR", 60);
define("ACCESS_LEVEL_SPECIAL_MODERATOR", 70);
define("ACCESS_LEVEL_ADMIN", 80);

define("ACCESS_STATUS_ERROR", "error");
define("ACCESS_STATUS_GUEST", "guest");
define("ACCESS_STATUS_USER", "user");
define("ACCESS_STATUS_DELETED", "deleted");
define("ACCESS_STATUS_PRETENDER", "pretender");
define("ACCESS_STATUS_MEMBER", "member");
define("ACCESS_STATUS_MODERATOR", "moderator");
define("ACCESS_STATUS_SPECIAL_MODERATOR", "special_moderator");
define("ACCESS_STATUS_ADMIN", "admin");

abstract class base_access_maper
{

	protected $status_to_level = array();
	protected $level_to_status = array();

	public function __construct()
	{
		$this->fill_status_to_level();
		$this->level_to_status = array_flip($this->status_to_level);
	}

	abstract protected function fill_status_to_level();

	public function status_exists($status)
	{
		return isset($this->status_to_level[$status]);
	}

	public function level_exists($level)
	{
		return isset($this->level_to_status[$level]);
	}

	public function get_status_from_level($level)
	{
		return $this->level_to_status[$level];
	}

	public function get_level_from_status($status)
	{
		return $this->status_to_level[$status];
	}

}

?>