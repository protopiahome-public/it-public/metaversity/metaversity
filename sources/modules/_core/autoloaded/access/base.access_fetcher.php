<?php

abstract class base_access_fetcher
{

	/**
	 * @var base_access_maper
	 */
	protected $access_maper;

	/**
	 * @var db
	 */
	protected $db;

	/**
	 * @var user
	 */
	protected $user;
	protected $user_id;
	protected $lock;
	protected $level;
	protected $error_occured = false;

	protected function __construct(base_access_maper $access_maper, $user_id = null, $lock = false)
	{
		global $db;
		$this->db = $db;

		global $user;
		$this->user = $user;

		$this->access_maper = $access_maper;
		$this->user_id = !is_null($user_id) ? $user_id : $this->user->get_user_id();
		$this->lock = $lock;

		$this->fetch_data();
	}

	abstract protected function fetch_data();

	protected function error($error_msg)
	{
		$this->error_occured = true;
	}

	public function error_occured()
	{
		return $this->error_occured;
	}

	public function get_level()
	{
		return $this->level;
	}

}

?>