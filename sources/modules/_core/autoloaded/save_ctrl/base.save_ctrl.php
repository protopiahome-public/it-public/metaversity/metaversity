<?php

abstract class base_save_ctrl extends base_ctrl
{
	/**
	 * @var save_loader
	 */
	protected $save_loader;

	/**
	 * @var pass_info
	 */
	protected $pass_info;
	protected $autostart_db_transaction = true;
	public $xss_allowed = false;

	public function __construct()
	{
		global $save_loader;
		$this->save_loader = $save_loader;

		parent::__construct();
	}

	public function set_pass_info(pass_info $pass_info)
	{
		$this->pass_info = $pass_info;
	}

	public function set_up()
	{
		return true;
	}

	public function on_before_start()
	{
		return true;
	}

	public function start()
	{
		return true;
	}

	public function on_after_start()
	{
		return true;
	}

	public function check_rights()
	{
		return true;
	}

	public function on_before_check()
	{
		return true;
	}

	public function check()
	{
		return true;
	}

	public function on_after_check()
	{
		return;
	}

	public function on_check_fail()
	{
		return;
	}

	public function on_before_commit()
	{
		return true;
	}

	public function commit()
	{
		return true;
	}

	public function on_after_commit()
	{
		return;
	}

	public function clean_cache()
	{
		return;
	}

	public function rollback()
	{
		return;
	}

	public function autostart_db_transaction()
	{
		return $this->autostart_db_transaction;
	}

}

?>