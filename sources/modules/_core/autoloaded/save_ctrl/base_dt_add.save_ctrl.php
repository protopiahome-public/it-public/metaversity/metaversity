<?php

abstract class base_dt_add_save_ctrl extends base_dt_edit_save_ctrl
{

	final protected function check_id()
	{
		if (is_array($this->dt->get_pk_column()))
		{
			trigger_error("'Add' method is not supported for multi-column PK");
		}
		$this->id = 0;
		$this->action = "dt_add";
	}

}

?>