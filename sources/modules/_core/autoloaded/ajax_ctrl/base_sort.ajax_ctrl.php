<?php

abstract class base_sort_ajax_ctrl extends base_ajax_ctrl
{
	/* Settings */

	protected $table_name;
	protected $position_column = "position";

	public function get_data()
	{
		/* New order */
		$new_order = $this->get_new_order();
		if (!$new_order)
		{
			return array("status" => "FAIL");
		}

		$this->set_new_order($new_order);
		return array("status" => "OK");
	}

	private function get_new_order()
	{
		$new_order_str = POST("order");
		if (!$new_order_str)
		{
			return false;
		}
		$params = array();
		parse_str($new_order_str, $params);
		if (!isset($params["i"]) or !is_array($params["i"]))
		{
			return false;
		}
		$new_order_raw = $params["i"];
		foreach ($new_order_raw as $key => $value)
		{
			if (!is_good_id($value))
			{
				return false;
			}
		}
		$new_order = array_values($new_order_raw);
		return $new_order;
	}

	private function set_new_order($new_order)
	{
		$select_sql = new select_sql();
		$select_sql->add_from($this->table_name);
		$select_sql->add_select_fields("id, `{$this->position_column}`");
		$select_sql->add_order("`{$this->position_column}`");
		$this->mixin_call_method_with_mixins("modify_sql", array($select_sql));
		$rows = $this->db->fetch_column_values($select_sql->get_sql(), $this->position_column, "id");
		$i = 1;
		foreach ($new_order as $id)
		{
			if (isset($rows[$id]))
			{
				$this->db->sql("
					UPDATE {$this->table_name}
					SET `{$this->position_column}` = {$i}
					WHERE id = {$id}
				");
				++$i;
				unset($rows[$id]);
			}
		}
		foreach ($rows as $id => $tmp)
		{
			$this->db->sql("
				UPDATE {$this->table_name}
				SET `{$this->position_column}` = {$i}
				WHERE id = {$id}
			");
			++$i;
		}
		$this->clean_cache();
	}

	protected function modify_sql(select_sql $select_sql)
	{
		return;
	}

}

?>