<?php

abstract class base_wait_for_cache_ajax_ctrl extends base_ajax_ctrl
{

	/**
	 * @return cache
	 */
	abstract protected function get_cache();

	public function get_data()
	{
		$cache = $this->get_cache();
		return array(
			"status" => "OK",
			"ready" => !!$cache && !!$cache->get(),
		);
	}

}

?>