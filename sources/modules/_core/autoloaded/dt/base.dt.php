<?php

abstract class base_dt
{
	
	/**
	 * @var request
	 */
	protected $request;

	protected $name;
	protected $db_table = null;
	protected $pk_column = "id";
	protected $add_timestamp_column = "add_time";
	protected $edit_timestamp_column = "edit_time";
	protected $blocks = array();
	protected $fields = array();
	protected $axes = array();

	public function __construct()
	{
		global $request;
		$this->request = $request;
		
		$this->name = substr(get_class($this), 0, -3);
		if (!$this->db_table)
		{
			$this->db_table = $this->name;
		}
		$this->init();
	}

	public function set_db_table($db_table)
	{
		$this->db_table = $db_table;
	}
	
	public function set_pk_column($pk_column)
	{
		$this->pk_column = $pk_column;
	}

	public function set_add_timestamp_column($column = "")
	{
		$this->add_timestamp_column = $column;
	}

	public function set_edit_timestamp_column($column = "")
	{
		$this->edit_timestamp_column = $column;
	}

	abstract protected function init();

	protected function add_field(base_dtf $dtf, $block_name = null, $axes = array())
	{
		$axes = get_array($axes);
		$field_name = $dtf->get_field_name();
		$this->fields[$field_name] = $dtf;
		if ($block_name)
		{
			$this->add_block($block_name);
			$this->blocks[$block_name]["fields"][$field_name] = $dtf;
		}
		foreach ($axes as $axis_name)
		{
			if (!isset($this->axes[$axis_name]))
			{
				$this->axes[$axis_name] = array();
			}
			$this->axes[$axis_name][$field_name] = $dtf;
		}
	}

	protected function add_block($block_name, $block_title = null)
	{
		if (!isset($this->blocks[$block_name]))
		{
			$this->blocks[$block_name] = array(
				"title" => $block_title,
				"fields" => array(),
			);
		}
		elseif ($block_title)
		{
			$this->blocks[$block_name]["title"] = $block_title;
		}
	}

	public function add_fields_in_axis($axis_name, $field_names = array())
	{
		if (!isset($this->axes[$axis_name]))
		{
			$this->axes[$axis_name] = array();
		}
		foreach ($field_names as $field_name)
		{
			$this->axes[$axis_name][$field_name] = $this->fields[$field_name];
		}
	}

	public function get_blocks($axis_name = null)
	{
		$blocks = array();
		$fields = is_null($axis_name) ? $this->fields : $this->axes[$axis_name];
		foreach ($this->blocks as $block_name => $block_data)
		{
			foreach ($block_data["fields"] as $field_name => $dtf)
			{
				/* @var $dtf base_dtf */
				if (isset($fields[$field_name]))
				{
					if (!isset($blocks[$block_name]))
					{
						$blocks[$block_name] = array(
							"title" => $block_data["title"],
							"fields" => array(),
						);
					}
					$blocks[$block_name]["fields"][] = $field_name;
					unset($fields[$field_name]);
				}
			}
		}
		if (sizeof($fields))
		{
			$blocks["__other__"] = array(
				"title" => "",
				"fields" => array(),
			);
			foreach ($fields as $field_name => $dtf)
			{
				$blocks["__other__"]["fields"][] = $field_name;
			}
		}
		return $blocks;
	}

	public function get_fields()
	{
		return $this->fields;
	}

	public function get_fields_by_axis($axis_name, $remove_read_only = false)
	{
		if (!isset($this->axes[$axis_name]))
		{
			trigger_error("Undefined axis '{$axis_name}' in doctype '{$this->name}'");
		}
		$dtfs = $this->axes[$axis_name];
		if ($remove_read_only)
		{
			foreach ($dtfs as $key => $dtf)
			{
				/* @var $dtf base_dtf */
				if ($dtf->is_read_only())
				{
					unset($dtfs[$key]);
				}
			}
		}
		return $dtfs;
	}

	public function get_fields_by_block($block_name)
	{
		return $this->blocks[$block_name]["fields"];
	}

	/**
	 * @return base_dtf
	 */
	public function get_field($field_name)
	{
		return isset($this->fields[$field_name]) ? $this->fields[$field_name] : null;
	}

	public function get_name()
	{
		return $this->name;
	}

	public function get_db_table()
	{
		return $this->db_table;
	}
	
	public function get_pk_column()
	{
		return $this->pk_column;
	}

	public function get_add_timestamp_column()
	{
		return $this->add_timestamp_column;
	}

	public function get_edit_timestamp_column()
	{
		return $this->edit_timestamp_column;
	}

}

?>