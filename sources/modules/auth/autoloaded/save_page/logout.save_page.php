<?php

class logout_save_page extends base_save_ctrl
{

	public function commit()
	{
		$this->user->logout();
		if (!REQUEST("retpath"))
		{
			$this->set_redirect_url($this->request->get_full_prefix() . "/");
		}

		output_buffer::delete_cookie("current_city_id", $this->domain_logic->get_cookie_path(), $this->domain_logic->get_cookie_domain());
		output_buffer::delete_cookie("current_stream_id", $this->domain_logic->get_cookie_path(), $this->domain_logic->get_cookie_domain());
		output_buffer::delete_cookie("current_group_id", $this->domain_logic->get_cookie_path(), $this->domain_logic->get_cookie_domain());

		$this->pass_info->write_info("LOGGED_OUT");

		return true;
	}

}

?>