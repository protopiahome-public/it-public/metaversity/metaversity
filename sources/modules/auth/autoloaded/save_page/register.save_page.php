<?php

require_once PATH_CORE . "/loader/xml_loader.php";

class register_save_page extends base_dt_add_save_ctrl
{

	protected $mixins = array(
		"translate_names",
	);
	protected $dt_name = "user";
	protected $axis_name = "register";

	public function on_after_dt_init()
	{
		if (POST("cancel") !== null and $retpath = POST("retpath"))
		{
			$retpath .= strpos($retpath, "?") === false ? "?cancel=1" : "&cancel=1";
			$this->set_redirect_url($retpath);
			return true;
		}
		return true;
	}

	public function on_before_check()
	{
		if (POST("ignore_similarity_check") !== "1")
		{
			$email = trim(POST("email"));
			$first_name = trim(POST("first_name"));
			$last_name = trim(POST("last_name"));
			$email_escaped = $this->db->escape($email);
			$first_name_escaped = $this->db->escape($first_name);
			$last_name_escaped = $this->db->escape($last_name);
			$email_sql = strlen($email) ? "OR email = '{$email_escaped}'" : "";
			$name_sql = (strlen($first_name) or strlen($last_name)) ? "OR first_name = '{$first_name_escaped}' AND last_name = '{$last_name_escaped}'" : "";
			if (!$email_sql and !$name_sql)
			{
				return true;
			}
			$similar_users = $this->db->fetch_column_values("
				SELECT id
				FROM user
				WHERE FALSE {$email_sql} {$name_sql}
				LIMIT 50
			");
			if (sizeof($similar_users))
			{
				$my_loader = new xml_loader();
				foreach ($similar_users as $user_id)
				{
					$my_loader->add_xml(new user_short_xml_ctrl($user_id));
				}
				$this->pass_info->get_xdom()->import_xdom(xdom::create_from_string($my_loader->get_xml()));
				$this->pass_info->dump_vars($post_only = true);
				return false;
			}
			else
			{
				return true;
			}
		}
		if (POST("submit_from_similarity") === "1")
		{
			$this->pass_info->write_info("CHECK_AGAIN");
			return false;
		}
		return true;
	}

	public function on_before_commit()
	{
		$this->update_array["register_ip"] = "'" . $this->request->get_remote_addr() . "'";
		return true;
	}

	public function on_after_commit()
	{
		$this->user->login_by_id($this->last_id);

		if (!POST("retpath"))
		{
			$retpath = $this->request->get_full_prefix() . "/";
			$this->set_redirect_url($retpath);
		}

		$this->pass_info->write_info("REGISTERED");
	}

	public function clean_cache()
	{
		user_list_cache_tag::init()->update();
	}

}

?>