<?php

class settings_save_page extends base_dt_edit_save_ctrl
{

	protected $mixins = array(
		"human_only_check_rights",
		"translate_names",
	);
	protected $dt_name = "user";
	protected $axis_name = "settings";

	protected function check_id()
	{
		$this->id = REQUEST("id");
		if (!is_good_id($this->id) or $this->id != $this->user->get_user_id())
		{
			$this->id = -1;
		}
	}

	public function clean_cache()
	{
		user_cache_tag::init($this->id)->update();
		user_list_cache_tag::init()->update();
	}

}

?>