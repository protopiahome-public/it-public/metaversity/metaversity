<?php

require_once(PATH_MODULE_SITE_LIB . "/mail/mail_html.php");

class pass_save_page extends base_save_ctrl
{

	private $user_row;
	private $key_expired;
	private $need_new_key;
	private $is_step1;

	/**
	 * @var password_dtf
	 */
	private $password_dtf;

	/**
	 * @var password_save_dtf
	 */
	private $password_save_dtf;

	public function check()
	{
		global $config;

		// if we have $_POST['login']
		if (!$login = POST("login"))
		{
			$this->pass_info->write_error("BLANK");
			$this->pass_info->dump_vars();
			return false;
		}

		// if we have such login | e-mail
		$login_escaped = $this->db->escape($login);
		if (strpos($login, "@") !== false)
		{
			$where = "email = '{$login_escaped}'";
			$type = "EMAIL";
		}
		else
		{
			$where = "login = '{$login_escaped}'";
			$type = "LOGIN";
		}

		$this->user_row = $this->db->get_row("
			SELECT 
				id, login, email, pass_recovery_key, 
				UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(pass_recovery_time) AS key_age
			FROM user
			WHERE {$where}
			ORDER BY last_login_time
			LIMIT 1
		");

		if (!$this->user_row)
		{
			$this->pass_info->write_error("BAD_" . $type);
			$this->pass_info->dump_vars();
			return false;
		}

		// Is there a key already?
		$key = $this->user_row["pass_recovery_key"];
		$key_age = $this->user_row["key_age"];
		$this->key_expired = $key_age < 0 || $key_age > $config["pass_recovery_key_life_time"];
		$this->need_new_key = $this->key_expired || strlen($key) != 40;

		// Is step2 ?
		$key_entered = POST("key");
		$this->is_step1 = !$key_entered;
		if ($this->is_step1)
		{
			// step 1
			return true;
		}
		else
		{
			// step 2
			// is key expired?
			if ($this->key_expired)
			{
				$this->pass_info->write_error("KEY_EXPIRED");
				$this->pass_info->dump_vars();
				return false;
			}

			// is key good?
			if ($this->need_new_key or $key_entered !== $key)
			{
				$this->pass_info->write_error("BAD_KEY");
				$this->pass_info->dump_vars();
				return false;
			}

			$this->password_dtf = new password_dtf("password", "Password");
			$this->password_dtf->set_max_length(64);
			$this->password_dtf->set_min_length(5);
			$this->password_save_dtf = new password_save_dtf($this->password_dtf, $this->pass_info, "user", $this->user_row["id"]);

			if (!$this->password_save_dtf->check())
			{
				$this->pass_info->dump_vars();
				return false;
			}

			return true;
		}
	}

	public function commit()
	{
		global $config;

		// We will need these variables...
		$user_id = $this->user_row["id"];
		$user_login = $this->user_row["login"];
		$user_email = $this->user_row["email"];
		$key = $this->user_row["pass_recovery_key"];

		if ($this->is_step1)
		{
			return $this->commit_step1($user_id, $user_login, $user_email, $key);
		}
		else
		{
			return $this->commit_step2($user_id, $user_login, $user_email, $key);
		}
	}

	private function commit_step1($user_id, $user_login, $user_email, $key)
	{
		if ($this->need_new_key)
		{
			// New key generation
			$key = sha1(mt_rand() . serialize($this->user_row));
			$this->db->sql("UPDATE user SET pass_recovery_key = '{$key}', pass_recovery_time = NOW() WHERE id = {$user_id}");
		}

		// Sending email
		$aux_data = ($retpath = POST("retpath2")) ? array("retpath_suffix" => "&retpath=" . urlencode($retpath)) : array();
		$this->send_email($user_id, $user_email, $aux_data);

		// Logging
		$this->log("SEND", $user_id, $user_login, $user_email);

		// Response
		$email_to_show = preg_replace("/^(.)[^@]*@/", "\\1...@", $user_email);
		$this->pass_info->write_info("OK", $email_to_show);
		return true;
	}

	private function send_email($user_id, $user_email, $aux_data)
	{
		global $config;

		require_once PATH_CORE . "/loader/xml_loader.php";

		$xml_loader = new xml_loader(true);
		$xml_loader->add_xml(new request_xml_ctrl());
		$xml_loader->add_xml(new user_pass_restore_mail_xml_ctrl($user_id, $aux_data));
		$xml_loader->add_xslt("mail/pass.mail", "auth");
		$xml_loader->run();

		$body = $xml_loader->get_content();
		$html_email = new mail_html($user_email, $config["from"], $this->get_subject($body), $body);
		$html_email->send();
	}

	private function commit_step2($user_id, $user_login, $user_email, $key)
	{
		$fields = array("pass_recovery_key" => "'used'");
		$this->password_save_dtf->get_fields_to_write($fields);
		$this->db->update_by_array("user", $fields, "id = {$user_id}");

		$this->pass_info->write_info("SAVED");

		$this->user->login_by_id($user_id);
		$this->set_redirect_url(($retpath = POST('retpath2')) ? $retpath : "{$this->domain_logic->get_main_prefix()}/");

		// Logging
		$this->log("PASS", $user_id, $user_login, $user_email);
		return true;
	}

	private function get_subject($body)
	{
		$dom = new DOMDocument("1.0", "UTF-8");
		$dom->loadHTML($body);
		$html_sxml = simplexml_import_dom($dom);
		return $html_sxml->head->title;
	}

	private function log($action, $user_id, $user_login, $user_email)
	{
		file_put_contents(PATH_LOG . "/pass.log", date("r") . "\t{$action}\t{$user_id}\t{$user_login}\t{$user_email}\t{$this->request->get_remote_addr()}\n", FILE_APPEND);
	}

}

?>