<?php

class login_save_page extends base_save_ctrl
{

	public function set_up()
	{
		if (POST("cancel") !== null and $retpath = POST("retpath"))
		{
			$retpath .= strpos($retpath, "?") === false ? "?cancel=1" : "&cancel=1";
			$this->set_redirect_url($retpath);
			return true;
		}
		if (!POST("retpath"))
		{
			$this->set_redirect_url($this->request->get_prefix() . "/");
		}
		return true;
	}

	public function check()
	{
		if (!$this->user->login(POST("login"), POST("password")))
		{
			$this->pass_info->write_error("BAD_LOGIN_OR_PASSWORD");
			$this->pass_info->dump_vars();
			return false;
		}
		
		output_buffer::delete_cookie("current_city_id", $this->domain_logic->get_cookie_path(), $this->domain_logic->get_cookie_domain());
		output_buffer::delete_cookie("current_stream_id", $this->domain_logic->get_cookie_path(), $this->domain_logic->get_cookie_domain());
		output_buffer::delete_cookie("current_group_id", $this->domain_logic->get_cookie_path(), $this->domain_logic->get_cookie_domain());
		
		$this->pass_info->write_info("LOGGED_IN");
		
		return true;
	}

}

?>