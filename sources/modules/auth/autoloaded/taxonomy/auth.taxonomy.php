<?php

final class auth_taxonomy extends base_taxonomy
{

	public function run()
	{
		$p = $this->get_parts_relative();

		if ($p[1] === "login" and $p[2] === null)
		{
			//login/
			if ($this->user->get_user_id())
			{
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new login_xml_page());
			}
		}
		elseif ($p[1] === "pass" and $p[2] === null)
		{
			//pass/
			$this->xml_loader->add_xml_with_xslt(new pass_xml_page());
		}
		elseif ($p[1] === "reg" and $p[2] === null)
		{
			//reg/
			if ($this->user->get_user_id())
			{
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new register_xml_page());
			}
		}
		elseif ($p[1] === "settings" and $p[2] === null)
		{
			//settings/
			if (!$user_id = $this->user->get_user_id())
			{
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/login/");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new settings_xml_page($user_id));
			}
		}
		elseif ($p[1] === "app-auth" and $p[2] === null)
		{
			//settings/
			if (!$user_id = $this->user->get_user_id())
			{
				$this->xml_loader->set_redirect_url($this->request->get_prefix() . "/login/");
			}
			else
			{
				$this->xml_loader->add_xml_with_xslt(new api_session_xml_page());
			}
		}
	}

}

?>