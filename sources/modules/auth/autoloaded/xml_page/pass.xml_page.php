<?php

class pass_xml_page extends base_xml_ctrl
{

	public function get_xml()
	{
		$this->cache_state = XML_CTRL_CACHE_STATE_CACHE_NO_NEED;

		if (!isset($_GET["login"]) or !isset($_GET["key"]))
		{
			// Step 1
			return '<pass step="1"/>';
		}
		else
		{
			// Step 2

			global $config;

			$login = $_GET["login"];
			$key = $_GET["key"];
			if (strlen($key) != 40)
			{
				return '<pass step="2" error="BAD_KEY"/>';
			}
			$login_escaped = $this->db->escape($login);
			$user_row = $this->db->get_row("
				SELECT id, pass_recovery_key, pass_recovery_time, UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(pass_recovery_time) AS key_age
				FROM user
				WHERE login = '{$login_escaped}'
			");
			if (!$user_row)
			{
				return '<pass step="2" error="NO_USER"/>';
			}
			if ($user_row["pass_recovery_key"] != $key)
			{
				return '<pass step="2" error="BAD_KEY"/>';
			}
			$key_age = $user_row["key_age"];
			if ($key_age < 0 || $key_age > $config["pass_recovery_key_life_time"])
			{
				return '<pass step="2" error="KEY_EXPIRED"/>';
			}

			return '<pass step="2" login="' . $login . '" key="' . $key . '"/>';
		}
	}

}

?>