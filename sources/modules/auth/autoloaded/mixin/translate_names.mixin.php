<?php

require_once PATH_MODULE_SITE_LIB . "/text_processor/text_processor.php";

class translate_names_mixin extends base_mixin
{

	protected $updated_db_row;
	protected $last_id;

	public function on_after_commit()
	{
		// @todo merge with register, bulk register, etc...
		$data = array();
		$data["first_name"] = mb_convert_case($this->updated_db_row["first_name"], MB_CASE_TITLE);
		$data["last_name"] = mb_convert_case($this->updated_db_row["last_name"], MB_CASE_TITLE);
		$data["mid_name"] = mb_convert_case($this->updated_db_row["mid_name"], MB_CASE_TITLE);
		$data["first_name_en"] = text_processor::translate_to_en($data["first_name"]);
		$data["last_name_en"] = text_processor::translate_to_en($data["last_name"]);
		foreach ($data as &$val)
		{
			$val = "'" . $this->db->escape($val) . "'";
		}
		unset($val);
		$this->db->update_by_array("user", $data, "id = {$this->last_id}");
	}

}

?>