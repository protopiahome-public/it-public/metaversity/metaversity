<?php

class position_credit_helper extends base_static_db_helper
{

	public static function get_credit($position_id, $user_id)
	{
		return self::db()->get_row("
			SELECT * 
			FROM position_credit
			WHERE position_id = {$position_id} AND user_id = {$user_id}
		");
	}

	public static function get_credits($position_id, $user_ids)
	{
		return self::db()->fetch_all("
			SELECT * 
			FROM position_credit
			WHERE position_id = {$position_id} AND user_id IN (" . join(", ", $user_ids) . ")
		", "user_id");
	}

	public static function change_credit($position_id, $user_id, $comment = "", $is_deleted = false)
	{
		global $user;
		/* @var $user user */

		$comment_escaped = self::db()->escape($comment);

		$data = array(
			"position_id" => $position_id,
			"user_id" => $user_id,
			"comment" => "'{$comment_escaped}'",
			"change_time" => "NOW()",
			"expert_user_id" => $user->get_user_id(),
		);

		if ($is_deleted)
		{
			self::db()->sql("
				DELETE FROM position_credit
				WHERE position_id = {$position_id} AND user_id = {$user_id}
			");
		}
		else
		{
			self::db()->insert_by_array("position_credit", $data, true);
		}

		$data["is_deleted"] = $is_deleted ? "1" : "0";
		self::db()->insert_by_array("position_credit_log", $data);
	}

}

?>