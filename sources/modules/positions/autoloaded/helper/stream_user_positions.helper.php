<?php

class stream_user_positions_helper extends base_static_db_helper
{

	public static function get_stream_user_required_competence_results($stream_id, $user_id, $competence_set_id)
	{
		$result = stream_user_required_competence_results_cache::init($stream_id, $user_id, $competence_set_id)->get();
		if ($result)
		{
			return $result;
		}
		$result = self::db()->fetch_column_values("
			SELECT rcl.competence_id, MAX(rcl.mark) AS mark
			FROM user_position up
			JOIN position p ON p.id = up.position_id
			JOIN rate_competence_link rcl ON rcl.rate_id = p.rate_id
			JOIN competence_calc c ON c.competence_id = rcl.competence_id
			WHERE up.user_id = {$user_id} AND p.stream_id = {$stream_id} AND c.competence_set_id = {$competence_set_id}
			GROUP BY rcl.competence_id
		", "mark", "competence_id");
		foreach ($result as &$mark)
		{
			$mark = (int) $mark;
		}
		unset($mark);
		stream_user_required_competence_results_cache::init($stream_id, $user_id, $competence_set_id)->set($result);
		return $result;
	}

	public static function get_user_streams_with_positions($user_id, $force_stream_id = null)
	{
		$force_stream_where = $force_stream_id ? " OR d.id = {$force_stream_id} " : "";
		$force_stream_order = $force_stream_id ? " d.id = {$force_stream_id} DESC, " : "";
		$result = self::db()->fetch_all("
			SELECT 
				d.id,
				IFNULL(l.position_count_calc, 0) AS position_count_calc
			FROM stream d
			LEFT JOIN stream_user_link l ON l.stream_id = d.id AND l.user_id = {$user_id}
			WHERE (position_count_calc > 0 {$force_stream_where})
			ORDER BY {$force_stream_order} d.title
		", "id");
		// l.status <> 'pretender' AND l.status <> 'deleted'
		return $result;
	}

}

?>