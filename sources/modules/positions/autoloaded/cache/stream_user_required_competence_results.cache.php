<?php

class stream_user_required_competence_results_cache extends base_cache
{

	public static function init($stream_id, $user_id, $competence_set_id)
	{
		$tag_keys = array();
		$tag_keys[] = stream_user_positions_cache_tag::init($stream_id, $user_id)->get_key();
		$tag_keys[] = stream_positions_cache_tag::init($stream_id)->get_key();
		$tag_keys[] = competence_set_competences_cache_tag::init($competence_set_id)->get_key();
		return parent::get_cache(__CLASS__, $stream_id, $user_id, $tag_keys);
	}

}

?>