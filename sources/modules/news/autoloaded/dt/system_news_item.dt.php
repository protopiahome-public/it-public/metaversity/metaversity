<?php

class system_news_item_dt extends base_dt
{

	/**
	 * @var domain_logic
	 */
	protected $domain_logic;

	public function __construct()
	{
		global $domain_logic;
		$this->domain_logic = $domain_logic;

		parent::__construct();
	}

	protected function init()
	{
		$this->add_block("main", trans("Main"));

		$dtf = new string_dtf("title", trans("Title"));
		$dtf->set_max_length(255);
		$dtf->set_importance(true);
		$this->add_field($dtf, "main");

		$dtf = new bool_dtf("is_for_moderators", trans("Only for stream's administrators and moderators"));
		$dtf->set_default_value(false);
		$this->add_field($dtf, "main");

		$dtf = new intmarkup_dtf("announce", trans("Announce"));
		$dtf->set_editor_height(100);
		$this->add_field($dtf, "main");

		$dtf = new intmarkup_dtf("descr", trans("News text"));
		$dtf->set_editor_height(200);
		$this->add_field($dtf, "main");

		$dtf = new foreign_key_dtf("adder_user_id", trans("Author"), "user");
		$dtf->set_importance(false);
		$this->add_field($dtf, "main");

		$this->add_fields_in_axis("edit", array(
			"title", "is_for_moderators", "announce", "descr",
		));

		$this->add_fields_in_axis("full", array(
			"title", "is_for_moderators", "announce", "descr",
		));
	}

}

?>