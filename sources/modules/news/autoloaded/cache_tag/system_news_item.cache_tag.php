<?php

class system_news_item_cache_tag extends base_cache_tag
{

	public static function init($system_news_item_id)
	{
		return parent::get_tag(__CLASS__, $system_news_item_id);
	}

}

?>