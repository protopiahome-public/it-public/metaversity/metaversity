<?php

class stream_news_item_cache_tag extends base_cache_tag
{

	public static function init($news_item_id)
	{
		return parent::get_tag(__CLASS__, $news_item_id);
	}

}

?>