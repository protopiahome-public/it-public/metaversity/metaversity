<?php

class news_ctrl_loader_helper
{

	public static function load_xml_ctrls($news, xml_loader $xml_loader)
	{
		foreach ($news as $row)
		{
			if (isset($row["stream_id"]))
			{
				$xml_loader->add_xml_by_class_name("stream_short_xml_ctrl", $row["stream_id"]);
				if (isset($row["activity_id"]))
				{
					$xml_loader->add_xml_by_class_name("activity_short_xml_ctrl", $row["activity_id"], $row["stream_id"]);
				}
				if (isset($row["role_id"]))
				{
					$xml_loader->add_xml_by_class_name("role_short_xml_ctrl", $row["role_id"], $row["stream_id"]);
				}
				if (isset($row["changer_user_id"]))
				{
					$xml_loader->add_xml_by_class_name("stream_user_short_xml_ctrl", $row["changer_user_id"], $row["stream_id"]);
				}
				if (isset($row["user_id"]))
				{
					$xml_loader->add_xml_by_class_name("stream_user_short_xml_ctrl", $row["user_id"], $row["stream_id"]);
				}
				if (isset($row["expert_user_id"]))
				{
					$xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $row["expert_user_id"]);
				}
				if (isset($row["news_item_id"]))
				{
					$xml_loader->add_xml_by_class_name("stream_news_item_full_xml_ctrl", $row["news_item_id"], $row["stream_id"]);
				}
				if (isset($row["position_id"]))
				{
					$xml_loader->add_xml_by_class_name("position_full_xml_ctrl", $row["position_id"], $row["stream_id"]);
				}
			}
			if (isset($row["system_news_item_id"]))
			{
				$xml_loader->add_xml_by_class_name("system_news_item_full_xml_ctrl", $row["system_news_item_id"]);
				if (isset($row["adder_user_id"]))
				{
					$xml_loader->add_xml_by_class_name("user_short_xml_ctrl", $row["adder_user_id"]);
				}
			}
		}
	}

}

?>