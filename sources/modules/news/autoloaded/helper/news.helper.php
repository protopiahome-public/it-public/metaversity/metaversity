<?php

class news_helper extends base_static_db_helper
{

	const TYPE_SYSTEM_TEXT = "system_text";
	const TYPE_STREAM_TEXT = "stream_text";
	const TYPE_ACTIVITY_STATUS = "activity_status";
	const TYPE_MARK = "mark";
	const TYPE_ACTIVITY_MODERATION = "activity_moderation";
	const TYPE_CREDIT = "credit";

}

?>