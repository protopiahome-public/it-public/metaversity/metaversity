<?php

class user_log_helper extends base_static_db_helper
{

	/**
	 * All get_news_feed() functions MUST generate fields 'news_type' and 'time'.
	 */
	public static function get_news_feed($user_id, $stream_id = null, $news_mode = false, $news_type = null)
	{
		$result = array();
		$stream_sql = $stream_id ? " AND a.stream_id = {$stream_id} " : "";
		$status_deleted = activity_participants_helper::PARTCIPANT_STATUS_DELETED;
		$news_type_mark = news_helper::TYPE_MARK;
		$news_type_activity_status = news_helper::TYPE_ACTIVITY_STATUS;

		/* Roles */
		if (!$news_type or $news_type === news_helper::TYPE_ACTIVITY_STATUS)
		{
			$is_for_news_sql = $news_mode ? " AND ap.is_for_news = 1 " : "";
			$data = self::db()->fetch_all("
				SELECT
					'{$news_type_activity_status}' AS 'news_type',
					ap.change_time AS time,
					'role' AS type, a.stream_id,
					ap.activity_id, ap.role_id,
					ap.status, ap.changer_user_id AS changer_user_id
				FROM activity_participant ap
				JOIN activity a ON a.id = ap.activity_id
				WHERE ap.user_id = {$user_id} AND status <> '{$status_deleted}'
					{$stream_sql}
					{$is_for_news_sql}
			");
			foreach ($data as $row)
			{
				$key = math_marks_helper::get_mark_key(math_competence_results_helper::TYPE_ROLE, $row["activity_id"], $row["role_id"]);
				if ($news_mode)
				{
					$key .= "#status";
				}
				$result[$key] = $row;
			}
		}

		/* Marks for roles */
		if (!$news_type or $news_type === news_helper::TYPE_MARK)
		{
			$data = self::db()->fetch_all("
				SELECT
					'{$news_type_mark}' AS 'news_type',
					apm.change_time AS time,
					'role' AS type, a.stream_id,
					apm.activity_id, apm.role_id,
					apm.mark, apm.comment, apm.changer_user_id
				FROM activity_participant_mark apm
				JOIN activity a ON a.id = apm.activity_id
				WHERE apm.user_id = {$user_id}
					{$stream_sql}
			");
			foreach ($data as $row)
			{
				$key = math_marks_helper::get_mark_key(math_competence_results_helper::TYPE_ROLE, $row["activity_id"], $row["role_id"]);
				if (isset($result[$key]))
				{
					$result[$key]["mark"] = $row["mark"];
					$result[$key]["comment"] = $row["comment"];
					$result[$key]["changer_user_id"] = $row["changer_user_id"];
					$result[$key]["time"] = $row["time"];
					$result[$key]["news_type"] = $row["news_type"];
				}
				else
				{
					$row["status"] = $status_deleted;
					$result[$key] = $row;
				}
			}
		}

		/* Marks for competences */
		if (!$news_type or $news_type === news_helper::TYPE_MARK)
		{
			$data = self::db()->fetch_all("
				SELECT 
					'{$news_type_mark}' AS 'news_type',
					acm.change_time AS time,
					'competence' AS type, a.stream_id,
					acm.activity_id, acm.competence_id, c.title AS competence_title,
					acm.mark, acm.comment, acm.changer_user_id
				FROM activity_competence_mark acm
				JOIN activity a ON a.id = acm.activity_id
				JOIN stream d ON d.id = a.stream_id
				JOIN competence_calc c ON c.competence_id = acm.competence_id
				WHERE acm.user_id = {$user_id} AND d.competence_set_id = c.competence_set_id
					{$stream_sql}
			");
			foreach ($data as $row)
			{
				$key = math_marks_helper::get_mark_key(math_competence_results_helper::TYPE_COMPETENCE, $row["activity_id"], $row["competence_id"]);
				$result[$key] = $row;
			}
		}

		$result = array_sort($result, "time", SORT_DESC);

		return $result;
	}

}

?>