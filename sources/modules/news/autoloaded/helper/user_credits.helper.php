<?php

class user_credits_helper extends base_static_db_helper
{

	/**
	 * All get_news_feed() functions MUST generate fields 'news_type' and 'time'.
	 */
	public static function get_news_feed($user_id, $stream_id = null)
	{
		$news_type_credit = news_helper::TYPE_CREDIT;
		$stream_sql = $stream_id ? " AND p.stream_id = {$stream_id} " : "";
		$result = self::db()->fetch_all("
			SELECT
				'{$news_type_credit}' AS 'news_type',
				pc.change_time AS time,
				p.stream_id,
				pc.position_id,
				pc.expert_user_id,
				pc.comment
			FROM position_credit pc
			JOIN position p ON p.id = pc.position_id
			WHERE pc.user_id = {$user_id}
				{$stream_sql}
			ORDER BY pc.change_time DESC
		");
		return $result;
	}

}

?>