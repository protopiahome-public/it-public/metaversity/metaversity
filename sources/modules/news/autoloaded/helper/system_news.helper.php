<?php

class system_news_helper extends base_static_db_helper
{

	/**
	 * All get_news_feed() functions MUST generate fields 'news_type' and 'time'.
	 */
	public static function get_news_feed($is_for_moderators = false)
	{
		$result = system_news_cache::init($is_for_moderators)->get();
		if (!$result)
		{
			$where_sql = $is_for_moderators ? "" : "WHERE is_for_moderators <> 1";
			$result = self::db()->fetch_all("
				SELECT 
					'system_text' AS 'news_type',
					add_time AS time,
					id AS system_news_item_id,
					adder_user_id
				FROM system_news_item
				{$where_sql}
				ORDER BY id DESC
			");
			system_news_cache::init($is_for_moderators)->set($result);
		}
		return $result;
	}
	
}

?>