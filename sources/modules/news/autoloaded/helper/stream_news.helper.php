<?php

class stream_news_helper extends base_static_db_helper
{

	/**
	 * All get_news_feed() functions MUST generate fields 'news_type' and 'time'.
	 */
	public static function get_news_feed($stream_id)
	{
		$result = stream_news_cache::init($stream_id)->get();
		if (!$result)
		{
			$result = self::db()->fetch_all("
				SELECT 
					'stream_text' AS 'news_type',
					add_time AS time,
					id AS news_item_id,
					stream_id,
					city_id,
					adder_user_id
				FROM news_item
				WHERE stream_id = {$stream_id}
				ORDER BY id DESC
			");
			stream_news_cache::init($stream_id)->set($result);
		}
		return $result;
	}

}

?>