<?php

class stream_activity_participants_moderation_helper extends base_static_db_helper
{

	/**
	 * All get_news_feed() functions MUST generate fields 'news_type' and 'time'.
	 */
	public static function get_news_feed($stream_id)
	{
		/* @var $user user */
		global $user;

		$status_premoderation = activity_participants_helper::PARTCIPANT_STATUS_PREMODERATION;
		$result = self::db()->fetch_all("
			SELECT 
				'activity_moderation' AS 'news_type',
				ap.change_time AS time,
				a.stream_id,
				ap.activity_id,
				ap.role_id,
				ap.user_id
			FROM activity_participants_subscription aps
			LEFT JOIN activity_participant ap ON ap.activity_id = aps.activity_id
			LEFT JOIN activity a ON a.id = ap.activity_id
			WHERE aps.user_id = {$user->get_user_id()}
				AND stream_id = {$stream_id}
				AND status = '{$status_premoderation}'
				AND ap.change_time > DATE_SUB(NOW(), INTERVAL 30 DAY)
			ORDER BY ap.change_time DESC
		");
		return $result;
	}

}

?>