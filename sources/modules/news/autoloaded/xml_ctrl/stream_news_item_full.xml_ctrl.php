<?php

class stream_news_item_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "adder_user_id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	protected $dt_name = "news_item";
	protected $axis_name = "full";

	/**
	 * @var news_item_dt
	 */
	protected $dt;
	// Internal
	protected $stream_id;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		parent::__construct($id);
	}

	protected function get_cache()
	{
		return stream_news_item_full_cache::init($this->id, $this->stream_id, $this->lang->get_current_lang_code());
	}

	public function on_after_dt_init()
	{
		$this->dt->set_stream_obj(stream_obj::instance($this->stream_id));
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
		$select_sql->add_select_fields("adder_user_id");
	}

//	protected function process_data()
//	{
//		if (isset($this->data[0]))
//		{
//			$add_time = $this->db->parse_datetime($this->data[0]["add_time"]);
//			$this->data[0]["add_time"] = date("Y-m-d H:i:s N", $add_time);
//		}
//	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("adder_user_id", $this->data[0]["adder_user_id"]);
	}

}

?>