<?php

class system_news_item_full_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array();
	protected $dt_name = "system_news_item";
	protected $axis_name = "full";

	protected function get_cache()
	{
		return system_news_item_full_cache::init($this->id);
	}

}

?>