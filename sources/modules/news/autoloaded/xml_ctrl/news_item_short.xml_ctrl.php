<?php

class news_item_short_xml_ctrl extends base_dt_show_xml_ctrl
{

	// Settings
	protected $dependencies_settings = array(
		array(
			"column" => "adder_user_id",
			"ctrl" => "stream_user_short",
			"param2" => "stream_id",
		),
	);
	protected $dt_name = "news_item";
	protected $axis_name = "short";
	// Internal
	protected $stream_id;

	public function __construct($id, $stream_id)
	{
		$this->stream_id = $stream_id;
		parent::__construct($id);
	}

	protected function modify_sql(select_sql $select_sql)
	{
		$select_sql->add_where("dt.stream_id = {$this->stream_id}");
		$select_sql->add_select_fields("adder_user_id");
		if ($this->lang->get_current_lang_code() === "en")
		{
			$select_sql->add_select_fields("city_id_foreign_table.title_en AS city_title_en");
		}
	}

	protected function process_data()
	{
		$data = &$this->data[0];
		$add_time = $this->db->parse_datetime($data["add_time"]);
		$data["add_time"] = date("Y-m-d H:i:s N", $add_time);
		if ($this->lang->get_current_lang_code() === "en")
		{
			$data["city_title"] = $data["city_title_en"];
			unset($data["city_title_en"]);
		}
	}

	protected function modify_xml(xdom $xdom)
	{
		$xdom->set_attr("adder_user_id", $this->data[0]["adder_user_id"]);
	}

}

?>