<?php

class stream_news_item_full_cache extends base_cache
{

	public static function init($news_item_id, $stream_id, $lang_code)
	{
		$tag_keys = array();
		$tag_keys[] = stream_news_item_cache_tag::init($news_item_id)->get_key();
		$tag_keys[] = cities_cache_tag::init()->get_key();
		return parent::get_cache(__CLASS__, $news_item_id, $stream_id, $lang_code, $tag_keys);
	}

}

?>