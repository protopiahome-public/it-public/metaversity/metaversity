<?php

class stream_news_cache extends base_cache
{

	public static function init($stream_id)
	{
		$tag_keys = array();
		$tag_keys[] = stream_news_cache_tag::init($stream_id)->get_key();
		return parent::get_cache(__CLASS__, $stream_id, $tag_keys);
	}

}

?>