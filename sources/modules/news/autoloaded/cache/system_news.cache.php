<?php

class system_news_cache extends base_cache
{

	public static function init($include_moderators_content)
	{
		$tag_keys = array();
		$tag_keys[] = system_news_cache_tag::init()->get_key();
		return parent::get_cache(__CLASS__, $include_moderators_content ? "1" : "0", $tag_keys);
	}

}

?>