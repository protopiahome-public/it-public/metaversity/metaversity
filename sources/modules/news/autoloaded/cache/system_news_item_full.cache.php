<?php

class system_news_item_full_cache extends base_cache
{

	public static function init($system_news_item_id)
	{
		$tag_keys = array();
		$tag_keys[] = system_news_item_cache_tag::init($system_news_item_id)->get_key();
		return parent::get_cache(__CLASS__, $system_news_item_id, $tag_keys);
	}

}

?>