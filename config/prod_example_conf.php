<?php

$config["db_host"] = "localhost";
$config["db_user"] = "";
$config["db_pass"] = "";
$config["db_name"] = "";
$config["db_cmp4_source"] = "";
$config["db_idsrv_source"] = "";
$config["test_db_name"] = "";
$config["db_mysql_max_allowed_packet"] = 64 * 1024 * 1024;

$config["memcache_host"] = "localhost";
$config["memcache_port"] = "11211";
$config["memcache_prefix"] = "mv";

$config["beanstalk_host"] = "localhost";
$config["beanstalk_port"] = "11300";

$config["main_host_name"] = "example.ru";
$config["main_host_prefix"] = "";
$config["main_url"] = "https://example.ru/";
$config["from"] = "support@example.ru";
$config["errors_email"] = "";
$config["comments_from"] = "null@example.ru";
$config["intmarkup_max_file_size"] = 8 * 1024 * 1024;
$config["intmarkup_no_protocol_host"] = "example.ru";
$config["lang_cookie_prefix"] = "";

$config["imagemagick_path"] = "";
$config["math_path"] = "";

$config["letsencrypt_cmd"] = "/usr/local/bin/certbot-auto";
$config["letsencrypt_main_host"] = "example.ru";
$config["letsencrypt_email"] = "";
$config["letsencrypt_restart"] = "/usr/sbin/httpd -k graceful";
$config["letsencrypt_cert_dir"] = "/etc/letsencrypt";

$config["subdomains_auth_paths"]["tests"] = "/tmp/";
$config["subdomains_auth_paths"]["vgroup"] = "/";

function config_session()
{
	global $session, $domain_logic, $config;
	/* @var $domain_logic domain_logic */

	$cookie_path = $domain_logic->get_cookie_path();
	$cookie_domain = $domain_logic->get_cookie_domain();
	$session = new x_session(PATH_SESSIONS, $config["x_session_private_key"], $cookie_path, $cookie_domain, true, "X_SESSION_ID4");
	$session->remove_old_cookie("X_SESSION_ID");
	$session->remove_old_cookie("X_SESSION_ID2");
	$session->remove_old_cookie("X_SESSION_ID3");
}

?>