<?php

$config["db_host"] = "localhost";
$config["db_user"] = "";
$config["db_pass"] = "";
$config["db_name"] = "";
$config["test_db_name"] = "";
$config["db_mysql_max_allowed_packet"] = 64 * 1024 * 1024;

$config["memcache_host"] = "localhost";
$config["memcache_port"] = "11211";
$config["memcache_prefix"] = "metaversity";

$config["beanstalk_host"] = false;
$config["beanstalk_port"] = "11300";

$config["main_host_name"] = "metaversity.dev";
$config["main_host_name"] = false;
//$config["main_host_prefix"] = "/www";
$config["main_host_prefix"] = "";
$config["main_url"] = "http://metaversity.dev/";
$config["from"] = "null@metaversity.dev";
$config["errors_email"] = null;
$config["comments_from"] = "null@metaversity.dev";
$config["intmarkup_max_file_size"] = 8 * 1024 * 1024;
$config["intmarkup_no_protocol_host"] = "metaversity.dev";
$config["lang_cookie_prefix"] = "";

$config["imagemagick_path"] = "";
$config["math_path"] = "";

$config["subdomains_auth_paths"]["tests"] = "/";

function config_session()
{
	global $session, $domain_logic, $config;
	/* @var $domain_logic domain_logic */

	$cookie_path = $domain_logic->get_cookie_path();
	$cookie_domain = $domain_logic->get_cookie_domain();
	$session = new x_session(PATH_SESSIONS, $config["x_session_private_key"], $cookie_path, $cookie_domain, true, "X_SESSION_ID4");
	$session->remove_old_cookie("X_SESSION_ID");
	$session->remove_old_cookie("X_SESSION_ID2");
	$session->remove_old_cookie("X_SESSION_ID3");
	
	if (strpos($_SERVER["HTTP_USER_AGENT"], "Opera") !== false)
	{
		//$_SESSION["member_id"] = 4;
	}
	elseif (strpos($_SERVER["HTTP_USER_AGENT"], "Chrome") !== false)
	{
		//$_SESSION["member_id"] = 2;
	}
	$_SESSION["member_id"] = 2;
}

?>