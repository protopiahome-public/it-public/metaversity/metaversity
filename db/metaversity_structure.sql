/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица activity
CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned DEFAULT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `format_id` int(10) unsigned NOT NULL DEFAULT '0',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL DEFAULT '',
  `city_id` int(10) unsigned DEFAULT NULL,
  `date_type` enum('event','deadline','longterm') NOT NULL DEFAULT 'event',
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  `moderators_text` mediumtext,
  `moderators_html` mediumtext,
  `accepted_participant_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `premoderation_request_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `weight` enum('hi','low') NOT NULL DEFAULT 'low',
  `metaactivity_id` int(10) unsigned DEFAULT NULL,
  `group_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `old_id` int(10) unsigned NOT NULL DEFAULT '0',
  `unset_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `longterm_graded_user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `longterm_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_activity_city_id` (`city_id`),
  KEY `FK_activity_metaactivity_id` (`metaactivity_id`),
  KEY `FK_activity_study_level_id` (`study_level_id`),
  KEY `FK_activity_format_id` (`format_id`),
  KEY `FK_activity_stream_id` (`stream_id`),
  CONSTRAINT `FK_activity_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_format_id` FOREIGN KEY (`format_id`) REFERENCES `format` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_metaactivity_id` FOREIGN KEY (`metaactivity_id`) REFERENCES `metaactivity` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_calc
CREATE TABLE IF NOT EXISTS `activity_calc` (
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `stream_id` int(10) unsigned NOT NULL DEFAULT '0',
  `format_id` int(10) unsigned NOT NULL DEFAULT '0',
  `city_id` int(10) unsigned DEFAULT NULL,
  `metaactivity_id` int(10) unsigned DEFAULT NULL,
  `date_type` enum('event','deadline','longterm') NOT NULL DEFAULT 'event',
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `study_level_id` int(10) unsigned DEFAULT NULL,
  `groups` text,
  `streams` text,
  `accepted_participant_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `longterm_graded_user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `longterm_mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`),
  KEY `FK_activity_calc_format_id` (`format_id`),
  KEY `FK_activity_calc_city_id` (`city_id`),
  KEY `FK_activity_calc_metaactivity_id` (`metaactivity_id`),
  KEY `FK_activity_calc_study_level_id` (`study_level_id`),
  KEY `FK_activity_calc_stream_id` (`stream_id`),
  CONSTRAINT `FK_activity_calc_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_format_id` FOREIGN KEY (`format_id`) REFERENCES `format` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_metaactivity_id` FOREIGN KEY (`metaactivity_id`) REFERENCES `metaactivity` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура activity_comment_add_count_calc
DELIMITER //
CREATE DEFINER=`metaversity`@`localhost` PROCEDURE `activity_comment_add_count_calc`(
		IN in_activity_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE activity
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_activity_id;
END//
DELIMITER ;

-- Дамп структуры для процедура activity_comment_delete_count_calc
DELIMITER //
CREATE DEFINER=`metaversity`@`localhost` PROCEDURE `activity_comment_delete_count_calc`(
		IN in_activity_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE activity
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_activity_id;
END//
DELIMITER ;

-- Дамп структуры для таблица activity_competence_mark
CREATE TABLE IF NOT EXISTS `activity_competence_mark` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` tinyint(3) unsigned NOT NULL,
  `comment` text,
  `change_time` datetime NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`,`competence_id`),
  KEY `FK_activity_competence_mark_user_id` (`user_id`),
  KEY `FK_activity_competence_mark_competence_id` (`competence_id`),
  KEY `FK_activity_competence_mark_changer_user_id` (`changer_user_id`),
  CONSTRAINT `FK_activity_competence_mark_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_competence_mark_changer_user_id` FOREIGN KEY (`changer_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_competence_mark_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_competence_mark_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_competence_mark_log
CREATE TABLE IF NOT EXISTS `activity_competence_mark_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` tinyint(3) unsigned DEFAULT NULL,
  `comment` text,
  `change_time` datetime NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_group_link
CREATE TABLE IF NOT EXISTS `activity_group_link` (
  `activity_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`group_id`),
  KEY `FK_activity_group_link_group_id` (`group_id`),
  CONSTRAINT `FK_activity_group_link_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_group_link_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_participant
CREATE TABLE IF NOT EXISTS `activity_participant` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `status` enum('declined','premoderation','accepted','deleted') NOT NULL DEFAULT 'premoderation',
  `change_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_to_send` mediumtext,
  `changer_user_id` int(10) unsigned DEFAULT NULL,
  `is_for_news` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`user_id`,`role_id`) USING BTREE,
  KEY `FK_activity_participant_user_id` (`user_id`),
  KEY `FK_activity_participant_role_id` (`role_id`),
  KEY `FK_activity_participant_changer_user_id` (`changer_user_id`),
  CONSTRAINT `FK_activity_participant_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_changer_user_id` FOREIGN KEY (`changer_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_participants_subscription
CREATE TABLE IF NOT EXISTS `activity_participants_subscription` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`) USING BTREE,
  KEY `FK_activity_participants_subscription_user_id` (`user_id`),
  CONSTRAINT `FK_activity_participants_subscription_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participants_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_participant_log
CREATE TABLE IF NOT EXISTS `activity_participant_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(10) unsigned DEFAULT NULL,
  `status` enum('declined','premoderation','accepted','deleted') NOT NULL,
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_participant_mark
CREATE TABLE IF NOT EXISTS `activity_participant_mark` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `mark` tinyint(3) DEFAULT NULL,
  `comment` text,
  `change_time` datetime NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`,`role_id`) USING BTREE,
  KEY `FK_activity_participant_mark_user_id` (`user_id`),
  KEY `FK_activity_participant_mark_role_id` (`role_id`),
  KEY `FK_activity_participant_mark_changer_user_id` (`changer_user_id`),
  CONSTRAINT `FK_activity_participant_mark_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_mark_changer_user_id` FOREIGN KEY (`changer_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_mark_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_mark_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_participant_mark_log
CREATE TABLE IF NOT EXISTS `activity_participant_mark_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(10) unsigned DEFAULT NULL,
  `mark` tinyint(3) DEFAULT NULL,
  `change_time` datetime NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_participant_status_calc
CREATE TABLE IF NOT EXISTS `activity_participant_status_calc` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('declined','premoderation','accepted') NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`),
  KEY `FK_activity_participant_status_calc_user_id` (`user_id`),
  CONSTRAINT `FK_activity_participant_status_calc_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_status_calc_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_role_link
CREATE TABLE IF NOT EXISTS `activity_role_link` (
  `activity_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `total_number` int(11) unsigned NOT NULL DEFAULT '0',
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `auto_accept` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`role_id`),
  KEY `FK_activity_role_link_role_id` (`role_id`),
  CONSTRAINT `FK_activity_role_link_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_link_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_role_stat
CREATE TABLE IF NOT EXISTS `activity_role_stat` (
  `activity_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `taken_number` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`role_id`) USING BTREE,
  KEY `FK_activity_role_stat_role_id` (`role_id`),
  CONSTRAINT `FK_activity_role_stat_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_stat_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_stream_crosspost
CREATE TABLE IF NOT EXISTS `activity_stream_crosspost` (
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `stream_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`stream_id`),
  KEY `FK_activity_stream_crosspost_stream_id` (`stream_id`),
  CONSTRAINT `FK_activity_stream_crosspost_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_stream_crosspost_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица activity_subject_link
CREATE TABLE IF NOT EXISTS `activity_subject_link` (
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`subject_id`),
  KEY `FK_activity_subject_link_subject_id` (`subject_id`),
  CONSTRAINT `FK_activity_subject_link_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_subject_link_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица api_session
CREATE TABLE IF NOT EXISTS `api_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `app_title` varchar(255) NOT NULL,
  `redirect_url` varchar(255) NOT NULL,
  `token` varchar(32) NOT NULL,
  `start_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_api_session_user_id` (`user_id`),
  CONSTRAINT `FK_api_session_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица app_allowed_domain
CREATE TABLE IF NOT EXISTS `app_allowed_domain` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица city
CREATE TABLE IF NOT EXISTS `city` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `title_en` varchar(40) NOT NULL DEFAULT '',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `stream_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `is_fake` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `author_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `old_id` int(10) unsigned DEFAULT NULL,
  `old_parent_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_comment_parent_id` (`parent_id`),
  KEY `FK_comment_type_id` (`type_id`),
  KEY `FK_comment_author_user_id` (`author_user_id`),
  CONSTRAINT `FK_comment_author_user_id` FOREIGN KEY (`author_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_type_id` FOREIGN KEY (`type_id`) REFERENCES `comment_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица comment_subscription
CREATE TABLE IF NOT EXISTS `comment_subscription` (
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `object_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`type_id`,`object_id`,`user_id`),
  KEY `FK_comment_subscription_user_id` (`user_id`),
  CONSTRAINT `FK_comment_subscription_type_id` FOREIGN KEY (`type_id`) REFERENCES `comment_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_comment_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица comment_type
CREATE TABLE IF NOT EXISTS `comment_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `is_tree` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_calc
CREATE TABLE IF NOT EXISTS `competence_calc` (
  `competence_id` int(10) unsigned NOT NULL,
  `title` varchar(1024) NOT NULL,
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_id`),
  KEY `FK_competence_calc_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_calc_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_calc_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_full
CREATE TABLE IF NOT EXISTS `competence_full` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_id` int(10) unsigned DEFAULT NULL,
  `number` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(1024) NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_integral` tinyint(1) NOT NULL DEFAULT '0',
  `integral_included_competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_full_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_full_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_group
CREATE TABLE IF NOT EXISTS `competence_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_competence_group_parent_id` (`parent_id`),
  KEY `FK_competence_group_competence_set_id` (`competence_set_id`),
  CONSTRAINT `FK_competence_group_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_group_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_group_study_level_link
CREATE TABLE IF NOT EXISTS `competence_group_study_level_link` (
  `competence_group_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_group_id`,`study_level_id`),
  KEY `FK_competence_group_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_competence_group_study_level_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_group_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_link
CREATE TABLE IF NOT EXISTS `competence_link` (
  `competence_id` int(10) unsigned NOT NULL,
  `competence_group_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`competence_id`,`competence_group_id`),
  KEY `FK_competence_link_competence_group_id` (`competence_group_id`),
  CONSTRAINT `FK_competence_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_set
CREATE TABLE IF NOT EXISTS `competence_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `descr` mediumtext,
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_competence_set_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_competence_set_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_study_level_link
CREATE TABLE IF NOT EXISTS `competence_study_level_link` (
  `competence_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_id`,`study_level_id`),
  KEY `FK_competence_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_competence_study_level_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_translation_direct
CREATE TABLE IF NOT EXISTS `competence_translation_direct` (
  `competence_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `eq_competence_id` int(10) unsigned NOT NULL,
  `eq_competence_set_id` int(10) unsigned NOT NULL,
  `eq_max_mark` float(2,1) DEFAULT NULL,
  PRIMARY KEY (`competence_id`,`eq_competence_id`),
  KEY `FK_competence_translation_direct_eq_competence_id` (`eq_competence_id`),
  KEY `FK_competence_translation_direct_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_translation_direct_eq_competence_set_id` (`eq_competence_set_id`),
  CONSTRAINT `FK_competence_translation_direct_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_direct_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_direct_eq_competence_id` FOREIGN KEY (`eq_competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_direct_eq_competence_set_id` FOREIGN KEY (`eq_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_translation_formulae
CREATE TABLE IF NOT EXISTS `competence_translation_formulae` (
  `competence_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `eq_competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `eq_competence_list` text,
  `old_eq_competence_list` text,
  `eq_formula` enum('MIN','AVG','RES') NOT NULL DEFAULT 'MIN',
  `eq_max_mark` float(2,1) DEFAULT NULL,
  PRIMARY KEY (`competence_id`,`eq_competence_set_id`),
  KEY `FK_competence_translation_formulae_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_translation_formulae_eq_competence_set_id` (`eq_competence_set_id`),
  CONSTRAINT `FK_competence_translation_formulae_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_formulae_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_formulae_eq_competence_set_id` FOREIGN KEY (`eq_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица competence_translator
CREATE TABLE IF NOT EXISTS `competence_translator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `eq_competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `formulae_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `direct_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_translator_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_translator_eq_competence_set_id` (`eq_competence_set_id`),
  CONSTRAINT `FK_competence_translator_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translator_eq_competence_set_id` FOREIGN KEY (`eq_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица email
CREATE TABLE IF NOT EXISTS `email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица email_log
CREATE TABLE IF NOT EXISTS `email_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `to_email` varchar(255) NOT NULL,
  `to_user_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` mediumtext,
  `sent_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица format
CREATE TABLE IF NOT EXISTS `format` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `title` varchar(40) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `format_group_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `is_archived` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_format_format_group_id` (`format_group_id`),
  KEY `FK_format_stream_id` (`stream_id`),
  CONSTRAINT `FK_format_format_group_id` FOREIGN KEY (`format_group_id`) REFERENCES `format_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_format_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица format_group
CREATE TABLE IF NOT EXISTS `format_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_format_group_parent_id` (`parent_id`),
  KEY `FK_format_group_steram_id` (`stream_id`),
  CONSTRAINT `FK_format_group_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `format_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_format_group_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица group
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_id` int(10) unsigned NOT NULL,
  `stream_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned DEFAULT NULL,
  `user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_group_city_id` (`city_id`),
  KEY `FK_group_study_level_id` (`study_level_id`),
  KEY `FK_group_stream_id` (`stream_id`),
  CONSTRAINT `FK_group_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_group_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_group_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для функция insert_rate
DELIMITER //
CREATE DEFINER=`metaversity`@`localhost` FUNCTION `insert_rate`(
	rate_type_id1 int unsigned,
	title1 varchar(255) CHARACTER SET utf8,
	competence_set_id1 int unsigned,
	add_time1 datetime,
	edit_time1 datetime
) RETURNS int(10) unsigned
BEGIN
	INSERT INTO rate
		(rate_type_id, title, competence_set_id, add_time, edit_time) 
	VALUES
		(rate_type_id1, title1, competence_set_id1, add_time1, edit_time1);

	RETURN LAST_INSERT_ID();
END//
DELIMITER ;

-- Дамп структуры для таблица integral_competence
CREATE TABLE IF NOT EXISTS `integral_competence` (
  `competence_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `included_competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_id`,`included_competence_id`),
  KEY `FK_integral_competence_included_competence_id` (`included_competence_id`),
  CONSTRAINT `FK_integral_competence_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_integral_competence_included_competence_id` FOREIGN KEY (`included_competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица intmarkup_file
CREATE TABLE IF NOT EXISTS `intmarkup_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `object_type` char(20) NOT NULL,
  `object_id` char(21) NOT NULL,
  `mime_type` varchar(255) NOT NULL,
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `ext` varchar(10) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `position` int(10) unsigned NOT NULL DEFAULT '99999',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `Unque` (`object_type`,`object_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица lang
CREATE TABLE IF NOT EXISTS `lang` (
  `code` char(2) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `title_en` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица metaactivity
CREATE TABLE IF NOT EXISTS `metaactivity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `city_id` int(10) unsigned DEFAULT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_metaactivity_stream_id` (`stream_id`),
  CONSTRAINT `FK_metaactivity_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица news_item
CREATE TABLE IF NOT EXISTS `news_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned DEFAULT NULL,
  `html` text,
  `html_more` mediumtext,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `announce_text` mediumtext,
  `announce_html` mediumtext,
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_news_item_city_id` (`city_id`),
  KEY `FK_news_item_adder_user_id` (`adder_user_id`),
  KEY `FK_news_item_stream_id` (`stream_id`),
  CONSTRAINT `FK_news_item_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_news_item_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_news_item_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для процедура news_item_comment_add_count_calc
DELIMITER //
CREATE DEFINER=`metaversity`@`localhost` PROCEDURE `news_item_comment_add_count_calc`(
		IN in_news_item_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE news_item
	SET comment_count_calc = comment_count_calc + 1
	WHERE id = in_news_item_id;
END//
DELIMITER ;

-- Дамп структуры для процедура news_item_comment_delete_count_calc
DELIMITER //
CREATE DEFINER=`metaversity`@`localhost` PROCEDURE `news_item_comment_delete_count_calc`(
		IN in_news_item_id INTEGER(11),
		IN in_author_user_id INTEGER(11)
	)
BEGIN
	UPDATE news_item
	SET comment_count_calc = comment_count_calc - 1
	WHERE id = in_news_item_id;
END//
DELIMITER ;

-- Дамп структуры для таблица position
CREATE TABLE IF NOT EXISTS `position` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `descr` mediumtext,
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned DEFAULT NULL,
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_position_rate_id` (`rate_id`),
  KEY `FK_position_stream_id` (`stream_id`),
  CONSTRAINT `FK_position_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_position_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица position_credit
CREATE TABLE IF NOT EXISTS `position_credit` (
  `position_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `change_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expert_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`position_id`,`user_id`),
  KEY `FK_position_credit_user_id` (`user_id`),
  KEY `FK_position_credit_expert_user_id` (`expert_user_id`),
  CONSTRAINT `FK_position_credit_expert_user_id` FOREIGN KEY (`expert_user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_position_credit_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_position_credit_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица position_credit_log
CREATE TABLE IF NOT EXISTS `position_credit_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `change_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expert_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица position_study_direction_link
CREATE TABLE IF NOT EXISTS `position_study_direction_link` (
  `position_id` int(10) unsigned NOT NULL,
  `study_direction_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`position_id`,`study_direction_id`),
  KEY `FK_position_study_direction_link_study_direction_id` (`study_direction_id`),
  CONSTRAINT `FK_position_study_direction_link_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_position_study_direction_link_study_direction_id` FOREIGN KEY (`study_direction_id`) REFERENCES `study_direction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица position_study_level_link
CREATE TABLE IF NOT EXISTS `position_study_level_link` (
  `position_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`position_id`,`study_level_id`),
  KEY `FK_position_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_position_study_level_link_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_position_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица rate
CREATE TABLE IF NOT EXISTS `rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate_type_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_rate_rate_type_id` (`rate_type_id`),
  CONSTRAINT `FK_rate_rate_type_id` FOREIGN KEY (`rate_type_id`) REFERENCES `rate_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица rate_competence_link
CREATE TABLE IF NOT EXISTS `rate_competence_link` (
  `rate_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rate_id`,`competence_id`),
  KEY `FK_rate_competence_link_competence_id` (`competence_id`),
  CONSTRAINT `FK_rate_competence_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rate_competence_link_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица rate_type
CREATE TABLE IF NOT EXISTS `rate_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `main_table` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `format_id` int(10) unsigned NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `total_number` int(3) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned DEFAULT NULL,
  `descr` mediumtext CHARACTER SET utf8,
  `auto_accept` tinyint(1) NOT NULL DEFAULT '0',
  `descr_text` mediumtext CHARACTER SET utf8,
  `descr_html` mediumtext CHARACTER SET utf8,
  `old_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_role_format_id` (`format_id`),
  KEY `FK_role_rate_id` (`rate_id`),
  KEY `role_old_id` (`old_id`),
  KEY `FK_role_stream_id` (`stream_id`),
  CONSTRAINT `FK_role_format_id` FOREIGN KEY (`format_id`) REFERENCES `format` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_role_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_role_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица role_study_level_link
CREATE TABLE IF NOT EXISTS `role_study_level_link` (
  `role_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`study_level_id`),
  KEY `FK_role_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_role_study_level_link_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_role_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица stream
CREATE TABLE IF NOT EXISTS `stream` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_en` varchar(100) NOT NULL DEFAULT '',
  `title_short` varchar(10) NOT NULL,
  `title_short_en` varchar(10) NOT NULL DEFAULT '',
  `join_premoderation` tinyint(1) NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `group_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `list_show` tinyint(1) NOT NULL DEFAULT '1',
  `list_position` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `list_descr` varchar(600) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT '',
  `position_credit_min_match` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `users_import_allowed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_stream_competence_set_id` (`competence_set_id`),
  KEY `FK_stream_lang_code` (`lang_code`),
  CONSTRAINT `FK_stream_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_stream_lang_code` FOREIGN KEY (`lang_code`) REFERENCES `lang` (`code`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица stream_admin
CREATE TABLE IF NOT EXISTS `stream_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `stream_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`stream_id`),
  KEY `FK_stream_admin_stream_id` (`stream_id`),
  KEY `FK_stream_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_stream_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_stream_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_stream_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_stream_admin_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_stream_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица stream_city_link
CREATE TABLE IF NOT EXISTS `stream_city_link` (
  `stream_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`stream_id`,`city_id`),
  KEY `FK_stream_city_link_city_id` (`city_id`),
  CONSTRAINT `FK_stream_city_link_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_stream_city_link_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица stream_user_link
CREATE TABLE IF NOT EXISTS `stream_user_link` (
  `stream_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('deleted','pretender','member','moderator','admin') NOT NULL DEFAULT 'deleted',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `group_id` int(10) unsigned DEFAULT NULL,
  `position_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stream_id`,`user_id`),
  KEY `FK_stream_user_link_user_id` (`user_id`),
  KEY `FK_stream_user_link_group_id` (`group_id`),
  CONSTRAINT `FK_stream_user_link_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_stream_user_link_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_stream_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица study_direction
CREATE TABLE IF NOT EXISTS `study_direction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `stream_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_study_direction_stream_id` (`stream_id`),
  CONSTRAINT `FK_study_direction_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица study_level
CREATE TABLE IF NOT EXISTS `study_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `stream_id` int(10) unsigned NOT NULL,
  `group_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_study_level_stream_id` (`stream_id`),
  CONSTRAINT `FK_study_level_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица study_material
CREATE TABLE IF NOT EXISTS `study_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned DEFAULT NULL,
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  `read_user_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `is_outer` tinyint(1) NOT NULL DEFAULT '0',
  `outer_url` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `FK_study_material_rate_id` (`rate_id`),
  KEY `FK_study_material_stream_id` (`stream_id`),
  CONSTRAINT `FK_study_material_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_study_material_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица study_material_subject_link
CREATE TABLE IF NOT EXISTS `study_material_subject_link` (
  `study_material_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`study_material_id`,`subject_id`),
  KEY `FK_study_material_subject_link_subject_id` (`subject_id`),
  CONSTRAINT `FK_study_material_subject_link_study_material_id` FOREIGN KEY (`study_material_id`) REFERENCES `study_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_study_material_subject_link_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица study_material_user_link
CREATE TABLE IF NOT EXISTS `study_material_user_link` (
  `study_material_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('default','read','skip') NOT NULL DEFAULT 'default',
  `change_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`study_material_id`,`user_id`),
  KEY `FK_study_material_user_link_user_id` (`user_id`),
  CONSTRAINT `FK_study_material_user_link_study_material_id` FOREIGN KEY (`study_material_id`) REFERENCES `study_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_study_material_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица subject
CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stream_id` int(10) unsigned NOT NULL,
  `title` varchar(40) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_subject_stream_id` (`stream_id`),
  CONSTRAINT `FK_subject_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица system_news_item
CREATE TABLE IF NOT EXISTS `system_news_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `is_for_moderators` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `announce_text` mediumtext,
  `announce_html` mediumtext,
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_system_news_item_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_system_news_item_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица sys_params
CREATE TABLE IF NOT EXISTS `sys_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL DEFAULT '',
  `info_email` varchar(100) NOT NULL,
  `copyright_start_year` char(4) NOT NULL DEFAULT '1900',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ga_tracking_id` varchar(40) NOT NULL DEFAULT '',
  `logo_main_width` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_main_height` int(10) unsigned NOT NULL DEFAULT '0',
  `logo_version` int(10) unsigned NOT NULL DEFAULT '0',
  `central_competence_set_id` int(10) unsigned NOT NULL DEFAULT '1',
  `default_import_city_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_sys_params_central_competence_set_id` (`central_competence_set_id`),
  KEY `FK_sys_params_default_import_city_id` (`default_import_city_id`),
  CONSTRAINT `FK_sys_params_central_competence_set_id` FOREIGN KEY (`central_competence_set_id`) REFERENCES `competence_set` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_sys_params_default_import_city_id` FOREIGN KEY (`default_import_city_id`) REFERENCES `city` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL DEFAULT '',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sex` enum('m','f','?') NOT NULL DEFAULT '?',
  `first_name` varchar(60) DEFAULT NULL,
  `mid_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `photo_large_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_large_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_big_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_big_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_mid_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_mid_height` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_small_width` int(10) unsigned NOT NULL DEFAULT '0',
  `photo_small_height` int(10) unsigned NOT NULL DEFAULT '0',
  `profile_url` varchar(255) NOT NULL DEFAULT '',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `photo_large_url` varchar(255) DEFAULT NULL,
  `photo_big_url` varchar(255) DEFAULT NULL,
  `photo_mid_url` varchar(255) DEFAULT NULL,
  `photo_small_url` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `www` varchar(255) DEFAULT NULL,
  `www_human_calc` varchar(255) DEFAULT NULL,
  `about` text,
  `profile_id_bak` int(10) unsigned DEFAULT NULL,
  `register_ip` varchar(15) DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pass_recovery_key` varchar(40) DEFAULT NULL,
  `pass_recovery_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `allow_email_on_reply` tinyint(1) NOT NULL DEFAULT '1',
  `allow_email_on_premoderation` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `photo_version` int(10) unsigned NOT NULL DEFAULT '0',
  `city_id` int(10) unsigned NOT NULL DEFAULT '1',
  `initial_city_id` int(10) unsigned NOT NULL DEFAULT '1',
  `about_text` text,
  `about_html` text,
  `position_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `mark_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `first_name_en` varchar(160) NOT NULL DEFAULT '',
  `last_name_en` varchar(160) NOT NULL DEFAULT '',
  `is_moderator_anywhere_calc` tinyint(1) NOT NULL DEFAULT '0',
  `potential_stream_id` int(10) unsigned DEFAULT NULL,
  `imported_by_stream_id` int(10) unsigned DEFAULT NULL,
  `imported_foreign_id` int(10) unsigned DEFAULT NULL,
  `results_access_level` enum('all','registered','dept_students','any_moderator') NOT NULL DEFAULT 'all',
  `marks_access_level` enum('all','registered','dept_students','any_moderator') NOT NULL DEFAULT 'all',
  `api_key` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `KEY_user_login` (`login`),
  KEY `FK_user_city_id` (`city_id`),
  KEY `FK_user_potential_stream_id` (`potential_stream_id`),
  KEY `FK_user_imported_by_stream_id` (`imported_by_stream_id`),
  CONSTRAINT `FK_user_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_user_imported_by_stream_id` FOREIGN KEY (`imported_by_stream_id`) REFERENCES `stream` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_user_potential_stream_id` FOREIGN KEY (`potential_stream_id`) REFERENCES `stream` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_foreign_statistic
CREATE TABLE IF NOT EXISTS `user_foreign_statistic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(45) NOT NULL,
  `test_url` varchar(255) NOT NULL,
  `success_percent` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL,
  `test_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_foreign_statistics_user_id` (`user_id`),
  CONSTRAINT `FK_user_foreign_statistics_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_merge_log
CREATE TABLE IF NOT EXISTS `user_merge_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `merge_time` datetime NOT NULL,
  `merger_user_id` int(10) unsigned NOT NULL,
  `remove_user_id` int(10) unsigned NOT NULL,
  `merge_into_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_merge_run_log
CREATE TABLE IF NOT EXISTS `user_merge_run_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sql_script_name` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `ok` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_position
CREATE TABLE IF NOT EXISTS `user_position` (
  `user_id` int(10) unsigned NOT NULL,
  `position_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`position_id`),
  KEY `FK_user_position_position_id` (`position_id`),
  CONSTRAINT `FK_user_position_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_position_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица user_position_log
CREATE TABLE IF NOT EXISTS `user_position_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `position_id` int(10) unsigned NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `checked` tinyint(3) unsigned NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица _src
CREATE TABLE IF NOT EXISTS `_src` (
  `mon` varchar(7) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `stream_id` int(10) unsigned
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица _src1
CREATE TABLE IF NOT EXISTS `_src1` (
  `user_id` int(10) unsigned NOT NULL,
  `mon` varchar(7) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `stream_id` int(10) unsigned
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица _stat
CREATE TABLE IF NOT EXISTS `_stat` (
  `mon` varchar(7) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`mon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
