UPDATE user u
LEFT JOIN (
  SELECT l.user_id
  FROM stream_user_link l
  WHERE status = 'admin' or status = 'moderator'
  GROUP BY user_id
) x ON x.user_id = u.id
SET u.is_moderator_anywhere_calc = u.is_admin OR x.user_id IS NOT NULL;