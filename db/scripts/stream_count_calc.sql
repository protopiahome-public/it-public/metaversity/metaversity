UPDATE stream s
SET group_count_calc = (
	SELECT COUNT(*)
	FROM `group` dt
	WHERE dt.stream_id = s.id
);

UPDATE study_level sl
SET group_count_calc = (
	SELECT COUNT(*)
	FROM `group` dt
	WHERE dt.study_level_id = sl.id
);

UPDATE city c
SET group_count_calc = (
	SELECT COUNT(*)
	FROM `group` g
	WHERE g.city_id = c.id
);

UPDATE stream s
SET city_count_calc = (
	SELECT COUNT(*)
	FROM stream_city_link l
	WHERE l.stream_id = s.id
);

UPDATE city c
SET stream_count_calc = (
	SELECT COUNT(*)
	FROM stream_city_link l
	WHERE l.city_id = c.id
);

UPDATE stream s
SET user_count_calc = (
	SELECT COUNT(*)
	FROM stream_user_link l
	WHERE l.stream_id = s.id
		AND l.status <> 'pretender' AND l.status <> 'deleted'
);

UPDATE `format` f
SET role_count_calc = (
	SELECT count(*)
	FROM role
	WHERE format_id = f.id
);

UPDATE study_material sm
SET read_user_count_calc = (
	SELECT COUNT(*) 
	FROM study_material_user_link l
	WHERE l.study_material_id = sm.id
		AND l.status = 'read'
);

UPDATE `group` f
SET user_count_calc = (
	SELECT count(*)
	FROM stream_user_link l
	WHERE l.group_id = f.id
		AND l.status <> 'pretender' AND l.status <> 'deleted'
);

UPDATE activity a
SET group_count_calc = (
	SELECT COUNT(*) 
	FROM activity_group_link l
	WHERE l.activity_id = a.id
);

INSERT IGNORE INTO stream_user_link
	(stream_id, user_id, status)
SELECT a.stream_id, apm.user_id, 'deleted'
FROM activity_participant_mark apm
JOIN activity a ON a.id = apm.activity_id
GROUP BY a.stream_id, apm.user_id;

INSERT IGNORE INTO stream_user_link
	(stream_id, user_id, status)
SELECT a.stream_id, acm.user_id, 'deleted'
FROM activity_competence_mark acm
JOIN activity a ON a.id = acm.activity_id
JOIN stream d ON d.id = a.stream_id
JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
GROUP BY a.stream_id, acm.user_id;

UPDATE stream_user_link l
SET l.mark_count_calc = (
	SELECT COUNT(*)
	FROM activity_participant_mark apm
	JOIN activity a ON a.id = apm.activity_id
	WHERE apm.user_id = l.user_id AND a.stream_id = l.stream_id
) + (
	SELECT COUNT(*)
	FROM activity_competence_mark acm
	JOIN activity a ON a.id = acm.activity_id
	JOIN stream d ON d.id = a.stream_id
	JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
	WHERE acm.user_id = l.user_id AND a.stream_id = l.stream_id
);

UPDATE user u
SET u.mark_count_calc = (
	SELECT COUNT(*)
	FROM activity_participant_mark apm
	WHERE apm.user_id = u.id
) + (
	SELECT COUNT(*)
	FROM activity_competence_mark acm
	JOIN activity a ON a.id = acm.activity_id
	JOIN stream d ON d.id = a.stream_id
	JOIN competence_calc c ON c.competence_id = acm.competence_id AND c.competence_set_id = d.competence_set_id
	WHERE acm.user_id = u.id
);

INSERT IGNORE INTO stream_user_link
	(stream_id, user_id, status)
SELECT p.stream_id, up.user_id, 'deleted'
FROM user_position up
JOIN position p ON p.id = up.position_id
GROUP BY p.stream_id, up.user_id;

UPDATE stream_user_link l
SET l.position_count_calc = (
	SELECT COUNT(*)
	FROM user_position up
	JOIN position p ON p.id = up.position_id
	WHERE up.user_id = l.user_id AND p.stream_id = l.stream_id
);

UPDATE user u
SET u.position_count_calc = (
	SELECT COUNT(*)
	FROM user_position up
	WHERE up.user_id = u.id
);

UPDATE activity a
SET accepted_participant_count_calc = (
	SELECT COUNT(*) 
	FROM activity_participant_status_calc 
	WHERE activity_id = a.id
		AND status = 'accepted'
), premoderation_request_count_calc = (
	SELECT COUNT(*)
	FROM activity_participant
	WHERE activity_id = a.id
		AND status = 'premoderation'
);

UPDATE activity a
SET mark_count_calc = (
	SELECT COUNT(*)
	FROM activity_participant_mark apm
	WHERE apm.activity_id = a.id
);

UPDATE activity a
SET unset_mark_count_calc = (
	SELECT COUNT(*)
	FROM activity_participant ap
	LEFT JOIN activity_participant_mark apm ON apm.activity_id = ap.activity_id
		AND apm.user_id = ap.user_id AND apm.role_id = ap.role_id
	WHERE ap.activity_id = a.id
		AND ap.status = 'accepted'
		AND apm.mark IS NULL
);

UPDATE competence_set s
SET s.competence_count_calc = (
	SELECT COUNT(*)
	FROM competence_full c
	WHERE c.is_deleted = 0 AND c.competence_set_id = s.id
);

UPDATE position p
LEFT JOIN stream d ON d.id = p.stream_id
SET p.competence_count_calc = (
	SELECT COUNT(*)
	FROM rate_competence_link rcl 
	JOIN competence_calc c ON c.competence_id = rcl.competence_id
	WHERE rcl.rate_id = p.rate_id AND c.competence_set_id = d.competence_set_id
);

UPDATE study_material sm
LEFT JOIN stream d ON d.id = sm.stream_id
SET sm.competence_count_calc = (
	SELECT COUNT(*)
	FROM rate_competence_link rcl 
	JOIN competence_calc c ON c.competence_id = rcl.competence_id
	WHERE rcl.rate_id = sm.rate_id AND c.competence_set_id = d.competence_set_id
);

UPDATE role r
LEFT JOIN stream d ON d.id = r.stream_id
SET r.competence_count_calc = (
	SELECT COUNT(*)
	FROM rate_competence_link rcl 
	JOIN competence_calc c ON c.competence_id = rcl.competence_id
	WHERE rcl.rate_id = r.rate_id AND c.competence_set_id = d.competence_set_id
);

UPDATE competence_translator t
SET t.direct_count_calc = (
  SELECT COUNT(*)
  FROM competence_translation_direct x
  WHERE x.competence_set_id = t.competence_set_id AND x.eq_competence_set_id = t.eq_competence_set_id
);