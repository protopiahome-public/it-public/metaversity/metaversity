UPDATE stream_user_link l
LEFT JOIN stream_admin da USING(user_id, stream_id)
SET l.status = IF(da.is_admin = 1, 'admin', IF(da.is_admin IS NOT NULL, 'moderator', l.status))
WHERE l.user_id IN (SELECT merge_into_user_id FROM user_merge_log);
