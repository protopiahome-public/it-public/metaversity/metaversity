DELETE FROM activity_participant_status_calc;

INSERT IGNORE INTO activity_participant_status_calc (activity_id, user_id, status)
SELECT ap.activity_id, ap.user_id, ap.status
FROM activity a
INNER JOIN activity_participant ap ON ap.activity_id = a.id
WHERE ap.status = 'accepted'
GROUP BY ap.activity_id, ap.user_id;

INSERT IGNORE INTO activity_participant_status_calc (activity_id, user_id, status)
SELECT ap.activity_id, ap.user_id, ap.status
FROM activity a
INNER JOIN activity_participant ap ON ap.activity_id = a.id
WHERE ap.status = 'premoderation'
GROUP BY ap.activity_id, ap.user_id;

INSERT IGNORE INTO activity_participant_status_calc (activity_id, user_id, status)
SELECT ap.activity_id, ap.user_id, ap.status
FROM activity a
INNER JOIN activity_participant ap ON ap.activity_id = a.id
WHERE ap.status = 'declined'
GROUP BY ap.activity_id, ap.user_id;

UPDATE activity a
SET accepted_participant_count_calc = (
	SELECT COUNT(*) 
	FROM activity_participant_status_calc 
	WHERE activity_id = a.id
		AND status = 'accepted'
), premoderation_request_count_calc = (
	SELECT COUNT(*) 
	FROM activity_participant 
	WHERE activity_id = a.id
		AND status = 'premoderation'
);

DELETE FROM activity_role_stat;

INSERT INTO activity_role_stat (activity_id, role_id, taken_number)
SELECT ap.activity_id, ap.role_id, COUNT(*)
FROM activity a
INNER JOIN activity_participant ap ON ap.activity_id = a.id
WHERE ap.status = 'accepted'
GROUP BY ap.activity_id, ap.role_id;
