RENAME TABLE `dept` TO `stream`;
ALTER TABLE `activity`
	DROP FOREIGN KEY `FK_activity_dept_id`;
ALTER TABLE `activity`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `activity`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	ADD CONSTRAINT `FK_activity_dept_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `activity`
	DROP FOREIGN KEY `FK_activity_dept_id`;
ALTER TABLE `activity`
	ADD CONSTRAINT `FK_activity_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `activity`
	DROP INDEX `FK_activity_dept_id`,
	ADD INDEX `FK_activity_stream_id` (`stream_id`);




ALTER TABLE `activity_calc`
	DROP FOREIGN KEY `FK_activity_calc_dept_id`;
ALTER TABLE `activity_calc`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `activity_id`,
	ADD CONSTRAINT `FK_activity_calc_dept_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `activity_calc`
	DROP FOREIGN KEY `FK_activity_calc_dept_id`;
ALTER TABLE `activity_calc`
	ADD CONSTRAINT `FK_activity_calc_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `activity_calc`
	DROP INDEX `FK_activity_calc_dept_id`,
	ADD INDEX `FK_activity_calc_stream_id` (`stream_id`);
ALTER TABLE `activity_calc`
	CHANGE COLUMN `depts` `streams` TEXT NULL AFTER `groups`;



ALTER TABLE `activity_dept_crosspost`
	DROP FOREIGN KEY `FK_activity_dept_crosspost_dept_id`;
ALTER TABLE `activity_dept_crosspost`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `activity_id`,
	ADD CONSTRAINT `FK_activity_dept_crosspost_dept_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `dept_admin`
	DROP FOREIGN KEY `FK_dept_admin_dept_id`;
ALTER TABLE `dept_admin`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `dept_admin`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `user_id`,
	ADD CONSTRAINT `FK_dept_admin_dept_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
RENAME TABLE `dept_admin` TO `stream_admin`;

ALTER TABLE `stream_admin`
	DROP FOREIGN KEY `FK_dept_admin_dept_id`,
	DROP FOREIGN KEY `FK_dept_admin_adder_admin_user_id`,
	DROP FOREIGN KEY `FK_dept_admin_editor_admin_user_id`,
	DROP FOREIGN KEY `FK_dept_admin_user_id`;
ALTER TABLE `stream_admin`
	ADD CONSTRAINT `FK_stream_admin_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	ADD CONSTRAINT `FK_stream_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	ADD CONSTRAINT `FK_stream_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	ADD CONSTRAINT `FK_stream_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `dept_city_link`
	DROP FOREIGN KEY `FK_dept_city_link_city_id`,
	DROP FOREIGN KEY `FK_dept_city_link_dept_id`;
ALTER TABLE `dept_city_link`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `dept_city_link`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL FIRST,
	ADD CONSTRAINT `FK_stream_city_link_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	ADD CONSTRAINT `FK_stream_city_link_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
RENAME TABLE `dept_city_link` TO `stream_city_link`;



RENAME TABLE `dept_user_link` TO `stream_user_link`;
ALTER TABLE `stream_user_link`
	DROP FOREIGN KEY `FK_dept_user_link_dept_id`,
	DROP FOREIGN KEY `FK_dept_user_link_group_id`,
	DROP FOREIGN KEY `FK_dept_user_link_user_id`;
ALTER TABLE `stream_user_link`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `stream_user_link`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL FIRST,
	ADD CONSTRAINT `FK_stream_user_link_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	ADD CONSTRAINT `FK_stream_user_link_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	ADD CONSTRAINT `FK_stream_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `activity_dept_crosspost`
	DROP FOREIGN KEY `FK_activity_dept_crosspost_dept_id`;
ALTER TABLE `activity_dept_crosspost`
	ADD CONSTRAINT `FK_activity_dept_crosspost_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `activity_dept_crosspost`
	DROP FOREIGN KEY `FK_activity_dept_crosspost_stream_id`,
	DROP FOREIGN KEY `FK_activity_dept_crosspost_activity_id`;
ALTER TABLE `activity_dept_crosspost`
	ADD CONSTRAINT `FK_activity_stream_crosspost_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	ADD CONSTRAINT `FK_activity_stream_crosspost_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
RENAME TABLE `activity_dept_crosspost` TO `activity_stream_crosspost`;
ALTER TABLE `activity_stream_crosspost`
	DROP INDEX `FK_activity_dept_crosspost_dept_id`,
	ADD INDEX `FK_activity_stream_crosspost_stream_id` (`stream_id`);
    
ALTER TABLE `city`
	CHANGE COLUMN `dept_count_calc` `stream_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `edit_time`;

ALTER TABLE `format`
	DROP FOREIGN KEY `FK_format_dept_id`;
ALTER TABLE `format`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `format`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	ADD CONSTRAINT `FK_format_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `format`
	DROP INDEX `FK_format_dept_id`,
	ADD INDEX `FK_format_stream_id` (`stream_id`);

    
ALTER TABLE `format_group`
	DROP FOREIGN KEY `FK_format_group_dept_id`;
ALTER TABLE `format_group`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `format_group`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	ADD CONSTRAINT `FK_format_group_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE `format_group`
	DROP INDEX `FK_format_group_dept_id`,
	ADD INDEX `FK_format_group_steram_id` (`stream_id`);

    
ALTER TABLE `group`
	DROP FOREIGN KEY `FK_group_dept_id`;
ALTER TABLE `group`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `group`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `city_id`,
	DROP INDEX `FK_group_dept_id`,
	ADD INDEX `FK_group_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_group_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `metaactivity`
	DROP FOREIGN KEY `FK_metaactivity_dept_id`;
ALTER TABLE `metaactivity`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `metaactivity`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	DROP INDEX `FK_metaactivity_dept_id`,
	ADD INDEX `FK_metaactivity_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_metaactivity_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
    
ALTER TABLE `news_item`
	DROP FOREIGN KEY `FK_news_item_dept_id`;
ALTER TABLE `news_item`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `news_item`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	DROP INDEX `FK_news_item_dept_id`,
	ADD INDEX `FK_news_item_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_news_item_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `position`
	DROP FOREIGN KEY `FK_position_dept_id`;
ALTER TABLE `position`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `position`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	DROP INDEX `FK_position_dept_id`,
	ADD INDEX `FK_position_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_position_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

    
ALTER TABLE `role`
	DROP FOREIGN KEY `FK_role_dept_id`;
ALTER TABLE `role`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `role`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	DROP INDEX `FK_role_dept_id`,
	ADD INDEX `FK_role_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_role_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `stream`
	DROP FOREIGN KEY `FK_dept_competence_set_id`,
	DROP FOREIGN KEY `FK_dept_lang_code`;
ALTER TABLE `stream`
	DROP INDEX `FK_dept_competence_set_id`,
	ADD INDEX `FK_stream_competence_set_id` (`competence_set_id`),
	DROP INDEX `FK_dept_lang_code`,
	ADD INDEX `FK_stream_lang_code` (`lang_code`),
	ADD CONSTRAINT `FK_stream_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON UPDATE CASCADE,
	ADD CONSTRAINT `FK_stream_lang_code` FOREIGN KEY (`lang_code`) REFERENCES `lang` (`code`) ON UPDATE CASCADE;

    
ALTER TABLE `stream_admin`
	DROP INDEX `FK_dept_admin_dept_id`,
	ADD INDEX `FK_stream_admin_stream_id` (`stream_id`),
	DROP INDEX `FK_dept_admin_adder_admin_user_id`,
	ADD INDEX `FK_stream_admin_adder_admin_user_id` (`adder_admin_user_id`),
	DROP INDEX `FK_dept_admin_editor_admin_user_id`,
	ADD INDEX `FK_stream_admin_editor_admin_user_id` (`editor_admin_user_id`);

    
ALTER TABLE `stream_city_link`
	DROP INDEX `FK_dept_city_link_city_id`,
	ADD INDEX `FK_stream_city_link_city_id` (`city_id`);

    
ALTER TABLE `stream_user_link`
	DROP INDEX `FK_dept_user_link_user_id`,
	ADD INDEX `FK_stream_user_link_user_id` (`user_id`),
	DROP INDEX `FK_dept_user_link_group_id`,
	ADD INDEX `FK_stream_user_link_group_id` (`group_id`);

ALTER TABLE `study_level`
	DROP FOREIGN KEY `FK_study_level_dept_id`;
ALTER TABLE `study_level`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `study_level`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `title`,
	DROP INDEX `FK_study_level_dept_id`,
	ADD INDEX `FK_study_level_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_study_level_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `study_material`
	DROP FOREIGN KEY `FK_study_material_dept_id`;
ALTER TABLE `study_material`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `study_material`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	DROP INDEX `FK_study_material_dept_id`,
	ADD INDEX `FK_study_material_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_study_material_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `subject`
	DROP FOREIGN KEY `FK_subject_dept_id`;
ALTER TABLE `subject`
	ALTER `dept_id` DROP DEFAULT;
ALTER TABLE `subject`
	CHANGE COLUMN `dept_id` `stream_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	DROP INDEX `FK_subject_dept_id`,
	ADD INDEX `FK_subject_stream_id` (`stream_id`),
	ADD CONSTRAINT `FK_subject_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

    
ALTER TABLE `user`
	DROP FOREIGN KEY `FK_user_imported_by_dept_id`,
	DROP FOREIGN KEY `FK_user_potential_dept_id`;
ALTER TABLE `user`
	CHANGE COLUMN `potential_dept_id` `potential_stream_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `is_moderator_anywhere_calc`,
	CHANGE COLUMN `imported_by_dept_id` `imported_by_stream_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `potential_stream_id`,
	DROP INDEX `FK_user_potential_dept_id`,
	ADD INDEX `FK_user_potential_stream_id` (`potential_stream_id`),
	DROP INDEX `FK_user_imported_by_dept_id`,
	ADD INDEX `FK_user_imported_by_stream_id` (`imported_by_stream_id`),
	ADD CONSTRAINT `FK_user_imported_by_stream_id` FOREIGN KEY (`imported_by_stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	ADD CONSTRAINT `FK_user_potential_stream_id` FOREIGN KEY (`potential_stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE SET NULL;
