INSERT INTO `rate_type` (id, title, main_table)
VALUES (7, 'Учебный материал', 'study_material');

CREATE TABLE `study_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `descr` mediumtext,
  `competence_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_id` int(10) unsigned DEFAULT NULL,
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_study_material_rate_id` (`rate_id`),
  CONSTRAINT `FK_study_material_rate_id` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
