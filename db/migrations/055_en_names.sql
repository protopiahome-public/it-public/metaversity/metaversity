ALTER TABLE `user` 
  ADD COLUMN `first_name_en` VARCHAR(160) NOT NULL DEFAULT '' AFTER `mark_count_calc`,
  ADD COLUMN `last_name_en` VARCHAR(160) NOT NULL DEFAULT '' AFTER `first_name_en`;
