ALTER TABLE `api_user_update`
	DROP FOREIGN KEY `FK_api_user_update_user_id`,
	DROP FOREIGN KEY `FK_api_user_update_project_id`;
ALTER TABLE `communities_menu_dd_item`
	DROP FOREIGN KEY `FK_communities_menu_dd_project_id`,
	DROP FOREIGN KEY `FK_communities_menu_dd_community_id`;
ALTER TABLE `communities_menu_item`
	DROP FOREIGN KEY `FK_communities_menu_project_id`,
	DROP FOREIGN KEY `FK_communities_menu_community_id`;
ALTER TABLE `community`
	DROP FOREIGN KEY `FK_community_project_id`,
	DROP FOREIGN KEY `FK_community_parent_id`;
ALTER TABLE `community_admin`
	DROP FOREIGN KEY `FK_community_admin_user_id`,
	DROP FOREIGN KEY `FK_community_admin_editor_admin_user_id`,
	DROP FOREIGN KEY `FK_community_admin_community_id`,
	DROP FOREIGN KEY `FK_community_admin_adder_admin_user_id`;
ALTER TABLE `community_custom_feed`
	DROP FOREIGN KEY `FK_community_custom_feed_community_id`;
ALTER TABLE `community_custom_feed_source`
	DROP FOREIGN KEY `FK_community_custom_feed_source_section_id`,
	DROP FOREIGN KEY `FK_community_custom_feed_source_custom_feed_id`,
	DROP FOREIGN KEY `FK_community_custom_feed_source_community_id`;
ALTER TABLE `community_user_link`
	DROP FOREIGN KEY `FK_community_user_link_user_id`,
	DROP FOREIGN KEY `FK_community_user_link_community_id`;
ALTER TABLE `community_widget`
	DROP FOREIGN KEY `FK_community_widget_type_id`,
	DROP FOREIGN KEY `FK_community_widget_community_id`;
ALTER TABLE `community_widget_communities`
	DROP FOREIGN KEY `FK_community_widget_communities_widget_id`,
	DROP FOREIGN KEY `FK_community_widget_communities_community_id`;
ALTER TABLE `community_widget_custom_feed`
	DROP FOREIGN KEY `FK_community_widget_custom_feed_widget_id`,
	DROP FOREIGN KEY `FK_community_widget_custom_feed_community_feed_id`;
ALTER TABLE `community_widget_default`
	DROP FOREIGN KEY `FK_community_widget_default_type_id`;
ALTER TABLE `community_widget_text`
	DROP FOREIGN KEY `FK_community_widget_text_widget_id`;
ALTER TABLE `competence_calc`
	DROP COLUMN `personal_mark_count_calc`,
	DROP COLUMN `group_mark_count_calc`,
	DROP COLUMN `group_mark_raw_count_calc`,
	DROP COLUMN `total_mark_count_calc`;
ALTER TABLE `competence_full`
	DROP COLUMN `personal_mark_count_calc`,
	DROP COLUMN `group_mark_count_calc`,
	DROP COLUMN `group_mark_raw_count_calc`,
	DROP COLUMN `total_mark_count_calc`;
ALTER TABLE `competence_set`
	DROP COLUMN `professiogram_count_calc`,
	DROP COLUMN `personal_mark_count_calc`,
	DROP COLUMN `group_mark_count_calc`,
	DROP COLUMN `group_mark_raw_count_calc`,
	DROP COLUMN `total_mark_count_calc`,
	DROP COLUMN `personal_mark_count_with_trans_calc`,
	DROP COLUMN `group_mark_count_with_trans_calc`,
	DROP COLUMN `group_mark_raw_count_with_trans_calc`,
	DROP COLUMN `total_mark_count_with_trans_calc`,
	DROP COLUMN `project_count_calc`,
	DROP COLUMN `project_count_with_trans_calc`;
ALTER TABLE `event`
	DROP FOREIGN KEY `FK_event_reflex_project_id`,
	DROP FOREIGN KEY `FK_event_reflex_id`,
	DROP FOREIGN KEY `FK_event_project_id`,
	DROP FOREIGN KEY `FK_event_adder_user_id`;
ALTER TABLE `event_category`
	DROP FOREIGN KEY `FK_event_category_project_id`,
	DROP FOREIGN KEY `FK_event_category_parent_id`;
ALTER TABLE `event_category_link`
	DROP FOREIGN KEY `FK_event_category_link_event_id`,
	DROP FOREIGN KEY `FK_event_category_link_event_category_id`;
ALTER TABLE `event_rate_link`
	DROP FOREIGN KEY `FK_event_rate_link_rate_id`,
	DROP FOREIGN KEY `FK_event_rate_link_event_id`,
	DROP FOREIGN KEY `FK_event_rate_link_competence_set_id`;
ALTER TABLE `event_user_link`
	DROP FOREIGN KEY `FK_event_user_link_user_id`,
	DROP FOREIGN KEY `FK_event_user_link_event_id`;
ALTER TABLE `mark_calc`
	DROP FOREIGN KEY `FK_mark_calc_rater_user_id`,
	DROP FOREIGN KEY `FK_mark_calc_ratee_user_id`,
	DROP FOREIGN KEY `FK_mark_calc_project_id`,
	DROP FOREIGN KEY `FK_mark_calc_precedent_id`,
	DROP FOREIGN KEY `FK_mark_calc_precedent_group_id`,
	DROP FOREIGN KEY `FK_mark_calc_precedent_flash_group_user_link_id`,
	DROP FOREIGN KEY `FK_mark_calc_precedent_flash_group_id`,
	DROP FOREIGN KEY `FK_mark_calc_competence_set_id`,
	DROP FOREIGN KEY `FK_mark_calc_competence_id`,
	DROP FOREIGN KEY `FK_mark_calc_adder_user_id`;
ALTER TABLE `mark_group`
	DROP FOREIGN KEY `FK_mark_group_rater_user_id`,
	DROP FOREIGN KEY `FK_mark_group_precedent_flash_group_id`,
	DROP FOREIGN KEY `FK_mark_group_competence_id`,
	DROP FOREIGN KEY `FK_mark_group_adder_user_id`;
ALTER TABLE `mark_personal`
	DROP FOREIGN KEY `FK_mark_personal_rater_user_id`,
	DROP FOREIGN KEY `FK_mark_personal_precedent_flash_group_user_link_id`,
	DROP FOREIGN KEY `FK_mark_personal_competence_id`,
	DROP FOREIGN KEY `FK_mark_personal_adder_user_id`;
ALTER TABLE `material`
	DROP FOREIGN KEY `FK_material_reflex_project_id`,
	DROP FOREIGN KEY `FK_material_reflex_id`,
	DROP FOREIGN KEY `FK_material_project_id`,
	DROP FOREIGN KEY `FK_material_block_set_id`,
	DROP FOREIGN KEY `FK_material_adder_user_id`;
ALTER TABLE `material_category`
	DROP FOREIGN KEY `FK_material_category_project_id`,
	DROP FOREIGN KEY `FK_material_category_parent_id`;
ALTER TABLE `material_category_link`
	DROP FOREIGN KEY `FK_material_category_link_material_id`,
	DROP FOREIGN KEY `FK_material_category_link_material_category_id`;
ALTER TABLE `material_rate_link`
	DROP FOREIGN KEY `FK_material_rate_link_rate_id`,
	DROP FOREIGN KEY `FK_material_rate_link_material_id`,
	DROP FOREIGN KEY `FK_material_rate_link_competence_set_id`;
ALTER TABLE `material_user_link`
	DROP FOREIGN KEY `FK_material_user_link_user_id`,
	DROP FOREIGN KEY `FK_material_user_link_material_id`;
ALTER TABLE `post`
	DROP FOREIGN KEY `FK_post_user_id`,
	DROP FOREIGN KEY `FK_post_section_id`,
	DROP FOREIGN KEY `FK_post_last_comment_id`,
	DROP FOREIGN KEY `FK_post_last_comment_author_user_id`,
	DROP FOREIGN KEY `FK_post_community_id`,
	DROP FOREIGN KEY `FK_post_block_set_id`;
ALTER TABLE `precedent`
	DROP FOREIGN KEY `FK_precedent_responsible_user_id`,
	DROP FOREIGN KEY `FK_precedent_project_id`,
	DROP FOREIGN KEY `FK_precedent_precedent_group_id`,
	DROP FOREIGN KEY `FK_precedent_last_editor_user_id`,
	DROP FOREIGN KEY `FK_precedent_default_flash_group_id`,
	DROP FOREIGN KEY `FK_precedent_adder_user_id`;
ALTER TABLE `precedent_comment`
	DROP FOREIGN KEY `FK_precedent_comment_precedent_id`,
	DROP FOREIGN KEY `FK_precedent_comment_parent_id`,
	DROP FOREIGN KEY `FK_precedent_comment_commenter_user_id`;
ALTER TABLE `precedent_competence_group_link`
	DROP FOREIGN KEY `FK_precedent_competence_group_link_precedent_id`,
	DROP FOREIGN KEY `FK_precedent_competence_group_link_competence_set_id`,
	DROP FOREIGN KEY `FK_precedent_competence_group_link_competence_group_id`;
ALTER TABLE `precedent_flash_group`
	DROP FOREIGN KEY `FK_precedent_flash_group_precedent_id`;
ALTER TABLE `precedent_flash_group_user_link`
	DROP FOREIGN KEY `FK_precedent_flash_group_user_link_user_id`,
	DROP FOREIGN KEY `FK_precedent_flash_group_user_link_precedent_flash_group_id`;
ALTER TABLE `precedent_group`
	DROP FOREIGN KEY `FK_precedent_group_project_id`,
	DROP FOREIGN KEY `FK_precedent_group_parent_id`;
ALTER TABLE `precedent_group_link`
	DROP FOREIGN KEY `FK_precedent_group_link_precedent_id`,
	DROP FOREIGN KEY `FK_precedent_group_link_precedent_group_id`;
ALTER TABLE `precedent_mark_adder`
	DROP FOREIGN KEY `FK_precedent_mark_adder_precedent_id`,
	DROP FOREIGN KEY `FK_precedent_mark_adder_adder_user_id`;
ALTER TABLE `precedent_ratee_calc`
	DROP FOREIGN KEY `FK_precedent_ratee_calc_ratee_user_id`,
	DROP FOREIGN KEY `FK_precedent_ratee_calc_precedent_id`;
ALTER TABLE `precedent_rater_link`
	DROP FOREIGN KEY `FK_precedent_rater_link_rater_user_id`,
	DROP FOREIGN KEY `FK_precedent_rater_link_precedent_id`;
ALTER TABLE `professiogram`
	DROP FOREIGN KEY `FK_professiogram_rate_id`,
	DROP FOREIGN KEY `FK_professiogram_competence_set_id`,
	DROP FOREIGN KEY `FK_professiogram_adder_user_id`;
ALTER TABLE `project`
	DROP FOREIGN KEY `FK_project_vector_competence_set_id`,
	DROP FOREIGN KEY `FK_project_marks_competence_set_id`,
	DROP FOREIGN KEY `FK_project_adder_user_id`;
ALTER TABLE `project_admin`
	DROP FOREIGN KEY `FK_project_admin_user_id`,
	DROP FOREIGN KEY `FK_project_admin_project_id`,
	DROP FOREIGN KEY `FK_project_admin_editor_admin_user_id`,
	DROP FOREIGN KEY `FK_project_admin_adder_admin_user_id`;
ALTER TABLE `project_competence_set_link_calc`
	DROP FOREIGN KEY `FK_project_competence_set_link_calc_project_id`,
	DROP FOREIGN KEY `FK_project_competence_set_link_calc_competence_set_id`;
ALTER TABLE `project_custom_feed`
	DROP FOREIGN KEY `FK_project_custom_feed_project_id`;
ALTER TABLE `project_custom_feed_source`
	DROP FOREIGN KEY `FK_project_custom_feed_source_section_id`,
	DROP FOREIGN KEY `FK_project_custom_feed_source_custom_feed_id`,
	DROP FOREIGN KEY `FK_project_custom_feed_source_community_id`;
ALTER TABLE `project_import`
	DROP FOREIGN KEY `FK_project_import_to_project_id`,
	DROP FOREIGN KEY `FK_project_import_from_project_id`;
ALTER TABLE `project_intmenu_item`
	DROP FOREIGN KEY `FK_project_intmenu_item_project_id`;
ALTER TABLE `project_role`
	DROP FOREIGN KEY `FK_project_role_project_id`,
	DROP FOREIGN KEY `FK_project_role_based_on_professiogram_id`,
	DROP FOREIGN KEY `FK_project_role_adder_user_id`;
ALTER TABLE `project_role_rate_link`
	DROP FOREIGN KEY `FK_project_role_rate_link_rate_id`,
	DROP FOREIGN KEY `FK_project_role_rate_link_project_role_id`,
	DROP FOREIGN KEY `FK_project_role_rate_link_competence_set_id`;
ALTER TABLE `project_taxonomy_item`
	DROP FOREIGN KEY `FK_project_taxonomy_item_project_id`,
	DROP FOREIGN KEY `FK_project_taxonomy_item_parent_id`,
	DROP FOREIGN KEY `FK_project_taxonomy_item_block_set_id`;
ALTER TABLE `project_widget`
	DROP FOREIGN KEY `FK_project_widget_type_id`,
	DROP FOREIGN KEY `FK_project_widget_project_id`;
ALTER TABLE `project_widget_communities`
	DROP FOREIGN KEY `FK_project_widget_community_id`,
	DROP FOREIGN KEY `FK_project_widget_communities_widget_id`;
ALTER TABLE `project_widget_custom_feed`
	DROP FOREIGN KEY `FK_project_widget_custom_feed_widget_id`,
	DROP FOREIGN KEY `FK_project_widget_custom_feed_project_feed_id`;
ALTER TABLE `project_widget_default`
	DROP FOREIGN KEY `FK_project_widget_default_type_id`;
ALTER TABLE `project_widget_text`
	DROP FOREIGN KEY `FK_project_widget_text_widget_id`;
ALTER TABLE `section`
	DROP FOREIGN KEY `FK_section_community_id`;
ALTER TABLE `user_competence_set_link_calc`
	DROP FOREIGN KEY `FK_user_competence_set_link_calc_user_id`,
	DROP FOREIGN KEY `FK_user_competence_set_link_calc_default_vector_id_calc`,
	DROP FOREIGN KEY `FK_user_competence_set_link_calc_competence_set_id`;
ALTER TABLE `user_competence_set_project_link`
	DROP FOREIGN KEY `FK_user_competence_set_project_link_user_id`,
	DROP FOREIGN KEY `FK_user_competence_set_project_link_project_id`,
	DROP FOREIGN KEY `FK_user_competence_set_project_link_competence_set_id`;
ALTER TABLE `user_project_link`
	DROP FOREIGN KEY `FK_user_project_link_user_id`,
	DROP FOREIGN KEY `FK_user_project_link_project_id`,
	DROP FOREIGN KEY `FK_user_project_link_last_vector_id_calc`;
ALTER TABLE `vector`
	DROP FOREIGN KEY `FK_vector_user_id`,
	DROP FOREIGN KEY `FK_vector_project_id`,
	DROP FOREIGN KEY `FK_vector_competence_set_id`;
ALTER TABLE `vector_focus`
	DROP FOREIGN KEY `FK_vector_focus_vector_id`,
	DROP FOREIGN KEY `FK_vector_focus_competence_id`;
ALTER TABLE `vector_focus_history`
	DROP FOREIGN KEY `FK_vector_focus_history_vector_id`;
ALTER TABLE `vector_future`
	DROP FOREIGN KEY `FK_vector_future_vector_id`,
	DROP FOREIGN KEY `FK_vector_future_professiogram_id`;
ALTER TABLE `vector_future_competences_calc`
	DROP FOREIGN KEY `FK_vector_future_competences_calc_vector_id`,
	DROP FOREIGN KEY `FK_vector_future_competences_calc_competence_id`;
ALTER TABLE `vector_future_history`
	DROP FOREIGN KEY `FK_vector_future_history_vector_id`;
ALTER TABLE `vector_self`
	DROP FOREIGN KEY `FK_vector_self_user_id`,
	DROP FOREIGN KEY `FK_vector_self_competence_set_id`,
	DROP FOREIGN KEY `FK_vector_self_competence_id`;
ALTER TABLE `vector_self_history`
	DROP FOREIGN KEY `FK_vector_self_history_user_id`,
	DROP FOREIGN KEY `FK_vector_self_history_competence_set_id`;
ALTER TABLE `vector_self_stat`
	DROP FOREIGN KEY `FK_vector_self_stat_user_id`,
	DROP FOREIGN KEY `FK_vector_self_stat_competence_set_id`;
ALTER TABLE `block_video`
	DROP FOREIGN KEY `FK_block_video_fake_for_id`,
	DROP FOREIGN KEY `FK_block_video_block_set_id`;
ALTER TABLE `block_file`
	DROP FOREIGN KEY `FK_block_file_fake_for_id`,
	DROP FOREIGN KEY `FK_block_file_block_set_id`;
ALTER TABLE `module`
	DROP FOREIGN KEY `FK_module_block_set_id`;
ALTER TABLE `module`
	DROP COLUMN `block_set_id`;
ALTER TABLE `user_user_group_link`
	DROP FOREIGN KEY `FK_user_user_group_link_user_id`,
	DROP FOREIGN KEY `FK_user_user_group_link_user_group_id`;
DROP TABLE `api_user_update`;
DROP TABLE `community_widget_custom_feed`;
DROP TABLE `community_widget_communities`;
DROP TABLE `community_widget`;
DROP TABLE `community_widget_type`;
DROP TABLE `community_widget_text`;
DROP TABLE `community_widget_default`;
DROP TABLE `community_user_link`;
DROP TABLE `community`;
DROP TABLE `communities_menu_item`;
DROP TABLE `communities_menu_dd_item`;
DROP TABLE `community_custom_feed_source`;
DROP TABLE `community_custom_feed`;
DROP TABLE `community_admin`;
DROP TABLE `event_rate_link`;
DROP TABLE `event_user_link`;
DROP TABLE `event_category_link`;
DROP TABLE `event`;
DROP TABLE `event_category`;
DROP TABLE `material_category_link`;
DROP TABLE `material_category`;
DROP TABLE `material_user_link`;
DROP TABLE `material_rate_link`;
DROP TABLE `mark_group`;
DROP TABLE `mark_calc`;
DROP TABLE `material`;
DROP TABLE `mark_personal`;
DROP TABLE `project_intmenu_item`;
DROP TABLE `project_role`;
DROP TABLE `project_role_rate_link`;
DROP TABLE `project_import`;
DROP TABLE `project_competence_set_link_calc`;
DROP TABLE `project_custom_feed`;
DROP TABLE `project_custom_feed_source`;
DROP TABLE `project_widget_default`;
DROP TABLE `project_widget_text`;
DROP TABLE `project_widget_type`;
DROP TABLE `project_widget_custom_feed`;
DROP TABLE `project_taxonomy_item`;
DROP TABLE `project_widget`;
DROP TABLE `project_widget_communities`;
DROP TABLE `precedent_flash_group`;
DROP TABLE `precedent_flash_group_user_link`;
DROP TABLE `precedent_group`;
DROP TABLE `precedent_competence_group_link`;
DROP TABLE `post_calc`;
DROP TABLE `precedent`;
DROP TABLE `precedent_comment`;
DROP TABLE `professiogram`;
DROP TABLE `project`;
DROP TABLE `project_admin`;
DROP TABLE `precedent_rater_link`;
DROP TABLE `precedent_group_link`;
DROP TABLE `precedent_mark_adder`;
DROP TABLE `precedent_ratee_calc`;
DROP TABLE `section`;
DROP TABLE `user_competence_set_project_link`;
DROP TABLE `user_competence_set_link_calc`;
DROP TABLE `user_project_link`;
DROP TABLE `vector_self`;
DROP TABLE `vector_future_history`;
DROP TABLE `vector_self_stat`;
DROP TABLE `vector_self_history`;
DROP TABLE `vector_future_competences_calc`;
DROP TABLE `vector_focus`;
DROP TABLE `vector`;
DROP TABLE `vector_future`;
DROP TABLE `vector_focus_history`;
DROP TABLE `post`;
drop table `block_file`;
drop table `block_set`;
drop table `block_video`;
drop table `user_group`;
drop table `user_user_group_link`;