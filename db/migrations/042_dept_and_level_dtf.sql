CREATE TABLE `position_study_level_link` (
  `position_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`position_id`,`study_level_id`),
  KEY `FK_position_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_position_study_level_link_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_position_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role_dept_link` (
  `role_id` int(10) unsigned NOT NULL,
  `dept_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`dept_id`),
  KEY `FK_role_dept_link_dept_id` (`dept_id`),
  CONSTRAINT `FK_role_dept_link_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_role_dept_link_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role_study_level_link` (
  `role_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`study_level_id`),
  KEY `FK_role_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_role_study_level_link_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_role_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
