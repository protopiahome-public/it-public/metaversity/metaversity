CREATE TABLE `study_material_subject_link` (
  `study_material_id` INTEGER UNSIGNED NOT NULL,
  `subject_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`study_material_id`, `subject_id`),
  CONSTRAINT `FK_study_material_subject_link_study_material_id` FOREIGN KEY `FK_study_material_subject_link_study_material_id` (`study_material_id`)
    REFERENCES `study_material` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_study_material_subject_link_subject_id` FOREIGN KEY `FK_study_material_subject_link_subject_id` (`subject_id`)
    REFERENCES `subject` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

CREATE TABLE `study_material_module_link` (
  `study_material_id` INTEGER UNSIGNED NOT NULL,
  `module_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`study_material_id`, `module_id`),
  CONSTRAINT `FK_study_material_module_link_study_material_id` FOREIGN KEY `FK_study_material_module_link_study_material_id` (`study_material_id`)
    REFERENCES `study_material` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_study_material_module_link_module_id` FOREIGN KEY `FK_study_material_module_link_module_id` (`module_id`)
    REFERENCES `module` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;
