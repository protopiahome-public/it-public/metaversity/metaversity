CREATE TABLE `competence_group_dept_link` (
  `competence_group_id` int(10) unsigned NOT NULL,
  `dept_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_group_id`,`dept_id`) USING BTREE,
  KEY `FK_competence_group_dept_link_dept_id` (`dept_id`),
  CONSTRAINT `FK_competence_group_dept_link_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_group_dept_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `competence_group_study_level_link` (
  `competence_group_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_group_id`,`study_level_id`),
  KEY `FK_competence_group_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_competence_group_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_group_study_level_link_competence_group_id` FOREIGN KEY (`competence_group_id`) REFERENCES `competence_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
