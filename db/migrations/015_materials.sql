CREATE TABLE `study_material_user_link` (
  `study_material_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('','read','skip') NOT NULL DEFAULT '',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`study_material_id`,`user_id`),
  KEY `FK_study_material_user_link_user_id` (`user_id`),
  CONSTRAINT `FK_study_material_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_study_material_user_link_study_material_id` FOREIGN KEY (`study_material_id`) REFERENCES `study_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `study_material_user_link`
  CHANGE COLUMN `edit_time` `change_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `study_material`
  ADD COLUMN `read_user_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `descr_html`;

ALTER TABLE `study_material_user_link` 
  MODIFY COLUMN `status` ENUM('','default','read','skip') NOT NULL;

UPDATE study_material_user_link
  SET status = 'default'
  WHERE status = '';

ALTER TABLE `study_material_user_link`
  MODIFY COLUMN `status` ENUM('default','read','skip') NOT NULL DEFAULT 'default';
