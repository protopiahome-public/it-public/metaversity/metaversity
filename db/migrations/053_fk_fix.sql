ALTER TABLE `activity_dept_crosspost` 
  MODIFY COLUMN `activity_id` INTEGER UNSIGNED NOT NULL DEFAULT 0, 
  DROP INDEX `FK_activity_dept_crosspost_dept_id`,
  DROP FOREIGN KEY `FK_activity_dept_crosspost_activity_id`,
  DROP FOREIGN KEY `FK_activity_dept_crosspost_dept_id`;

ALTER TABLE `activity_dept_crosspost` 
  ADD CONSTRAINT `FK_activity_dept_crosspost_activity_id` FOREIGN KEY `FK_activity_dept_crosspost_activity_id` (`activity_id`)
    REFERENCES `activity` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `activity_dept_crosspost` 
  MODIFY COLUMN `dept_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  ADD CONSTRAINT `FK_activity_dept_crosspost_dept_id` FOREIGN KEY `FK_activity_dept_crosspost_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `sys_params` 
  ADD CONSTRAINT `FK_sys_params_central_competence_set_id` FOREIGN KEY `FK_sys_params_central_competence_set_id` (`central_competence_set_id`)
    REFERENCES `competence_set` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;
