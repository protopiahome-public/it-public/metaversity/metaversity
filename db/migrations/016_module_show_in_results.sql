ALTER TABLE `module`
  ADD COLUMN `show_in_results` BOOLEAN NOT NULL DEFAULT 0 AFTER `descr_html`;
