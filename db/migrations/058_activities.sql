CREATE TABLE `activity_calc` (
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `dept_id` int(10) unsigned NOT NULL DEFAULT '0',
  `format_id` int(10) unsigned NOT NULL DEFAULT '0',
  `city_id` int(10) unsigned DEFAULT NULL,
  `metaactivity_id` int(10) unsigned DEFAULT NULL,
  `start_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finish_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `study_level_id` int(10) unsigned DEFAULT NULL,
  `groups` text,
  `depts` text,
  PRIMARY KEY (`activity_id`),
  KEY `FK_activity_calc_dept_id` (`dept_id`),
  KEY `FK_activity_calc_format_id` (`format_id`),
  KEY `FK_activity_calc_city_id` (`city_id`),
  KEY `FK_activity_calc_metaactivity_id` (`metaactivity_id`),
  KEY `FK_activity_calc_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_activity_calc_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_format_id` FOREIGN KEY (`format_id`) REFERENCES `format` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_metaactivity_id` FOREIGN KEY (`metaactivity_id`) REFERENCES `metaactivity` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_calc_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `activity_calc` 
  ADD COLUMN `accepted_participant_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `depts`,
  ADD COLUMN `mark_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `accepted_participant_count_calc`;

INSERT INTO activity_calc
  (activity_id, dept_id, format_id, city_id, metaactivity_id, start_time, finish_time, study_level_id, accepted_participant_count_calc, mark_count_calc)
SELECT
  id, dept_id, format_id, city_id, metaactivity_id,
  IF(start_time = '0000-00-00', '2038-01-15 12:00:00', start_time),
  IF(finish_time = '0000-00-00', IF(start_time = '0000-00-00', '2038-01-15 12:00:00', start_time), finish_time),
  study_level_id, accepted_participant_count_calc, mark_count_calc
FROM activity;

SET group_concat_max_len = 512 * 1024;

UPDATE activity_calc a
LEFT JOIN (
  SELECT l.activity_id, GROUP_CONCAT(l.group_id ORDER BY l.group_id SEPARATOR ',') AS groups
  FROM activity_group_link l
  GROUP BY l.activity_id
) x ON a.activity_id = x.activity_id
SET a.groups = x.groups;

UPDATE activity_calc a
LEFT JOIN (
  SELECT l.activity_id, GROUP_CONCAT(l.dept_id ORDER BY l.dept_id SEPARATOR ',') AS depts
  FROM activity_dept_crosspost l
  GROUP BY l.activity_id
) x ON a.activity_id = x.activity_id
SET a.depts = x.depts;

