ALTER TABLE `user`
  ADD COLUMN `position_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `about_html`,
  ADD COLUMN `mark_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `position_count_calc`;

UPDATE user u
SET u.position_count_calc = (
  SELECT COUNT(*)
  FROM user_position up
  WHERE up.user_id = u.id
);

UPDATE user u
SET u.mark_count_calc = (
  SELECT COUNT(*)
  FROM activity_participant_mark apm
  WHERE apm.user_id = u.id
);
