CREATE TABLE `study_material_dept_link` (
  `study_material_id` INTEGER UNSIGNED NOT NULL,
  `dept_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`study_material_id`, `dept_id`),
  CONSTRAINT `FK_study_material_dept_link_dept_id` FOREIGN KEY `FK_study_material_dept_link_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_study_material_dept_link_study_material_id` FOREIGN KEY `FK_study_material_dept_link_study_material_id` (`study_material_id`)
    REFERENCES `study_material` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

CREATE TABLE `news_item_dept_link` (
  `news_item_id` INTEGER UNSIGNED NOT NULL,
  `dept_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`news_item_id`, `dept_id`),
  CONSTRAINT `FK_news_item_dept_link_dept_id` FOREIGN KEY `FK_news_item_dept_link_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_news_item_dept_link_news_item_id` FOREIGN KEY `FK_news_item_dept_link_news_item_id` (`news_item_id`)
    REFERENCES `news_item` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

CREATE TABLE `module_dept_link` (
  `module_id` INTEGER UNSIGNED NOT NULL,
  `dept_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`module_id`, `dept_id`),
  CONSTRAINT `FK_module_dept_link_dept_id` FOREIGN KEY `FK_module_dept_link_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_module_dept_link_module_id` FOREIGN KEY `FK_module_dept_link_module_id` (`module_id`)
    REFERENCES `module` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;