ALTER TABLE `role`
  CHANGE COLUMN `number` `total_number` INT(3) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `role`
  ADD COLUMN `auto_accept` BOOLEAN NOT NULL DEFAULT 0;

CREATE TABLE  `activity_role_link` (
  `activity_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `total_number` int(11) unsigned NOT NULL DEFAULT '0',
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `auto_accept` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`role_id`),
  KEY `FK_activity_role_link_role_id` (`role_id`),
  CONSTRAINT `FK_activity_role_link_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_link_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `activity_role_city_link` (
  `activity_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL DEFAULT '0',
  `total_number` int(11) unsigned NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `auto_accept` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`role_id`,`city_id`),
  KEY `FK_activity_role_city_link_role_id` (`role_id`),
  KEY `FK_activity_role_city_link_city_id` (`city_id`),
  CONSTRAINT `FK_activity_role_city_link_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_city_link_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_city_link_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `activity_role_city_stat` (
  `activity_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL DEFAULT '0',
  `taken_number` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`role_id`,`city_id`),
  KEY `FK_activity_role_city_stat_role_id` (`role_id`),
  KEY `FK_activity_role_city_stat_city_id` (`city_id`),
  CONSTRAINT `FK_activity_role_city_stat_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_city_stat_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_city_stat_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `role`
  ADD COLUMN `descr_text` MEDIUMTEXT AFTER `auto_accept`,
  ADD COLUMN `descr_html` MEDIUMTEXT AFTER `descr_text`;
