UPDATE format_group SET title = 'ARCHIVE' WHERE title = 'АРХИВ';

DROP TABLE module_subject_link;
DROP TABLE activity_module_link;
DROP TABLE study_material_module_link;
ALTER TABLE `news_item` 
  DROP COLUMN `module_id`, 
  DROP INDEX `FK_news_item_module_id`,
  DROP FOREIGN KEY `FK_news_item_module_id`;
DROP TABLE module;

ALTER TABLE `user` 
  DROP COLUMN `university_id`,
  DROP COLUMN `personal_mark_count_calc`,
  DROP COLUMN `group_mark_count_calc`,
  DROP COLUMN `total_mark_count_calc`,
  DROP COLUMN `icq`;

ALTER TABLE `activity` 
  DROP COLUMN `announce`,
  DROP COLUMN `html`;

ALTER TABLE `study_material` 
  DROP COLUMN `descr`,
  DROP COLUMN `html`;
