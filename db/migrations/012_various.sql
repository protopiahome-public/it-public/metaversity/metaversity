UPDATE rate_competence_link l
LEFT JOIN rate r ON r.id = l.rate_id
SET l.mark = l.mark + 1
WHERE rate_type_id = 6;