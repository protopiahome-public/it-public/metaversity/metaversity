CREATE TABLE `metaactivity` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `city_id` INTEGER UNSIGNED,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_metaactivity_city_id` FOREIGN KEY `FK_metaactivity_city_id` (`city_id`)
    REFERENCES `city` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

ALTER TABLE `activity`
  ADD COLUMN `metaactivity_id` INTEGER UNSIGNED AFTER `weight`;

ALTER TABLE `activity`
  ADD CONSTRAINT `FK_activity_metaactivity_id` FOREIGN KEY `FK_activity_metaactivity_id` (`metaactivity_id`)
    REFERENCES `metaactivity` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `metaactivity`
  ADD COLUMN `add_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `city_id`,
  ADD COLUMN `edit_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `add_time`;

ALTER TABLE `metaactivity`
  MODIFY COLUMN `title` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
