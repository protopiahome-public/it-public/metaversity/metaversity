ALTER TABLE `user` 
  ADD COLUMN `results_access_level` ENUM('all','registered','dept_students','dept_moderators') NOT NULL DEFAULT 'all' AFTER `imported_foreign_id`,
  ADD COLUMN `marks_access_level` ENUM('all','registered','dept_students','dept_moderators') NOT NULL DEFAULT 'all' AFTER `results_access_level`;
