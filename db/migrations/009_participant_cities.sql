ALTER TABLE `activity_participant`
  ADD COLUMN `city_id` INTEGER UNSIGNED NOT NULL AFTER `role_id`;

UPDATE activity_participant ap
LEFT JOIN user u ON u.id = ap.user_id
SET ap.city_id = u.city_id;

ALTER TABLE `activity_participant_log`
  ADD COLUMN `city_id` INTEGER UNSIGNED NOT NULL AFTER `role_id`;

UPDATE activity_participant_log ap
LEFT JOIN user u ON u.id = ap.user_id
SET ap.city_id = u.city_id;

ALTER TABLE `activity_participant` DROP PRIMARY KEY,
  ADD PRIMARY KEY  USING BTREE(`activity_id`, `user_id`, `role_id`, `city_id`),
  ADD CONSTRAINT `FK_activity_participant_city_id` FOREIGN KEY `FK_activity_participant_city_id` (`city_id`)
    REFERENCES `city` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `activity_participant`
  DROP COLUMN `add_time`;

DROP TABLE `activity_participant_calc`;

CREATE TABLE `activity_participant_status_calc` (
  `activity_id` INTEGER UNSIGNED NOT NULL,
  `user_id` INTEGER UNSIGNED NOT NULL,
  `status` enum('declined','premoderation','accepted') NOT NULL,
  PRIMARY KEY (`activity_id`, `user_id`),
  CONSTRAINT `FK_activity_participant_status_calc_activity_id` FOREIGN KEY `FK_activity_participant_status_calc_activity_id` (`activity_id`)
    REFERENCES `activity` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_status_calc_user_id` FOREIGN KEY `FK_activity_participant_status_calc_user_id` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

CREATE TABLE `activity_city_participant_status_calc` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `status` enum('declined','premoderation','accepted') NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`,`city_id`) USING BTREE,
  KEY `FK_activity_city_participant_status_calc_user_id` (`user_id`),
  KEY `FK_activity_city_participant_status_calc_city_id` (`city_id`),
  CONSTRAINT `FK_activity_city_participant_status_calc_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_city_participant_status_calc_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_city_participant_status_calc_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `activity_city_link` ADD COLUMN `participant_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `edit_time`;

ALTER TABLE `activity_participant_mark`
  ADD COLUMN `city_id` INTEGER UNSIGNED NOT NULL AFTER `role_id`;

UPDATE activity_participant_mark x
LEFT JOIN user u ON u.id = x.user_id
SET x.city_id = u.city_id;

ALTER TABLE `activity_participant_mark_log`
  ADD COLUMN `city_id` INTEGER UNSIGNED NOT NULL AFTER `role_id`;

UPDATE activity_participant_mark_log x
LEFT JOIN user u ON u.id = x.user_id
SET x.city_id = u.city_id;

ALTER TABLE `activity_participant_mark` DROP PRIMARY KEY,
  ADD PRIMARY KEY  USING BTREE(`activity_id`, `user_id`, `role_id`, `city_id`),
  ADD CONSTRAINT `FK_activity_participant_mark_city_id` FOREIGN KEY `FK_activity_participant_mark_city_id` (`city_id`)
    REFERENCES `city` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

// activity_participants.sql
