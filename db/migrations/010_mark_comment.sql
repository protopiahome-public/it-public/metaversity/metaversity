ALTER TABLE `activity_participant_mark`
  ADD COLUMN `comment` TEXT AFTER `mark`;

ALTER TABLE `activity_participant_mark_log`
  ADD COLUMN `comment` TEXT AFTER `changer_user_id`;

