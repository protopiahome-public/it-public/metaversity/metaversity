ALTER TABLE `format_group`
  ADD COLUMN `is_archive` BOOLEAN NOT NULL DEFAULT 0 AFTER `edit_time`;

INSERT INTO `format_group` (title, is_archive, position) VALUES ('АРХИВ', 1, 99);

ALTER TABLE `format`
  ADD COLUMN `is_archived` BOOLEAN NOT NULL DEFAULT 0 AFTER `position`;
