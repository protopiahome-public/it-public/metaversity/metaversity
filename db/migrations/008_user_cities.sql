ALTER TABLE `user` 
  ADD COLUMN `initial_city_id` INT(10) UNSIGNED NOT NULL DEFAULT 1 AFTER `city_id`,
  ADD CONSTRAINT `FK_user_initial_city_id` FOREIGN KEY `FK_user_initial_city_id` (`initial_city_id`)
    REFERENCES `city` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

UPDATE user
SET initial_city_id = city_id;