CREATE TABLE `position` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `position`
  ADD COLUMN `add_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `title`,
  ADD COLUMN `edit_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `add_time`;

ALTER TABLE `position`
  ADD COLUMN `position` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `edit_time`;

ALTER TABLE `position` 
  ADD COLUMN `descr` MEDIUMTEXT AFTER `position`;

ALTER TABLE `role`
  ADD COLUMN `descr` MEDIUMTEXT AFTER `rate_id`;

ALTER TABLE `position`
  MODIFY COLUMN `descr` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;

ALTER TABLE `role`
  MODIFY COLUMN `descr` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;

ALTER TABLE `role`
  MODIFY COLUMN `title` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `position`
  ADD COLUMN `competence_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `descr`;

ALTER TABLE `position`
  ADD COLUMN `rate_id` INTEGER UNSIGNED AFTER `competence_count_calc`,
  ADD CONSTRAINT `FK_position_rate_id` FOREIGN KEY `FK_position_rate_id` (`rate_id`)
    REFERENCES `role` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `position`
  DROP FOREIGN KEY `FK_position_rate_id`;

ALTER TABLE `position`
  ADD CONSTRAINT `FK_position_rate_id` FOREIGN KEY `FK_position_rate_id` (`rate_id`)
    REFERENCES `rate` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

INSERT INTO rate_type (id, title, main_table) VALUES (6, 'Позиция', 'position');

