ALTER TABLE `dept` 
  ADD COLUMN `users_import_allowed` BOOLEAN NOT NULL DEFAULT 0 AFTER `position_credit_min_match`;

ALTER TABLE `user` 
  ADD COLUMN `imported_by_dept_id` INTEGER UNSIGNED AFTER `potential_dept_id`,
  ADD COLUMN `imported_foreign_id` INTEGER UNSIGNED AFTER `imported_by_dept_id`;

ALTER TABLE `user` 
  ADD CONSTRAINT `FK_user_imported_by_dept_id` FOREIGN KEY `FK_user_imported_by_dept_id` (`imported_by_dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `sys_params` 
  ADD COLUMN `default_import_city_id` INTEGER UNSIGNED NOT NULL DEFAULT 1 AFTER `central_competence_set_id`;

ALTER TABLE `sys_params` 
  ADD CONSTRAINT `FK_sys_params_default_import_city_id` FOREIGN KEY `FK_sys_params_default_import_city_id` (`default_import_city_id`)
    REFERENCES `city` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;
