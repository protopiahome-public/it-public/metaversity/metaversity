CREATE TABLE `user_position` (
  `user_id` int(10) unsigned NOT NULL,
  `position_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`,`position_id`),
  KEY `FK_user_position_position_id` (`position_id`),
  CONSTRAINT `FK_user_position_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_position_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_position_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `position_id` int(10) unsigned NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `checked` tinyint(3) unsigned NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
