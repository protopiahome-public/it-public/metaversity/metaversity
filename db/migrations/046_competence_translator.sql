CREATE TABLE `competence_translation_direct` (
  `competence_id` int(10) unsigned NOT NULL,
  `competence_set_id` int(10) unsigned NOT NULL,
  `eq_competence_id` int(10) unsigned NOT NULL,
  `eq_competence_set_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_id`,`eq_competence_id`),
  KEY `FK_competence_translation_direct_eq_competence_id` (`eq_competence_id`),
  KEY `FK_competence_translation_direct_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_translation_direct_eq_competence_set_id` (`eq_competence_set_id`),
  CONSTRAINT `FK_competence_translation_direct_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_direct_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_direct_eq_competence_id` FOREIGN KEY (`eq_competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_direct_eq_competence_set_id` FOREIGN KEY (`eq_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO competence_translation_direct
SELECT c1.competence_id, c1.competence_set_id, c2.competence_id, c2.competence_set_id
FROM competence_calc c1
JOIN competence_calc c2 ON c1.title = c2.title
WHERE c1.competence_set_id <> c2.competence_set_id;

ALTER TABLE `activity` 
  MODIFY COLUMN `old_id` INT(10) UNSIGNED NOT NULL DEFAULT 0;

