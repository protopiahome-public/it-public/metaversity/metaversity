ALTER TABLE `sys_params` 
  DROP COLUMN `color_scheme`,
  DROP COLUMN `students_competence_set_id`,
  DROP INDEX `FK_sys_params_students_competence_set_id`,
  DROP FOREIGN KEY `FK_sys_params_students_competence_set_id`;
