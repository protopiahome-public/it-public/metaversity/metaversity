CREATE TABLE `position_group` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `position` ADD COLUMN `position_group_id` INTEGER UNSIGNED AFTER `descr_html`,
 ADD CONSTRAINT `FK_position_position_group_id` FOREIGN KEY `FK_position_position_group_id` (`position_group_id`)
    REFERENCES `position_group` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;
