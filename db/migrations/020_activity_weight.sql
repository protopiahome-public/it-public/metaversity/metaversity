ALTER TABLE `activity` 
  ADD COLUMN `weight` ENUM('hi','low') NOT NULL DEFAULT 'hi' AFTER `participant_count_calc`;

ALTER TABLE `activity` 
  MODIFY COLUMN `weight` ENUM('hi','low') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'low';

