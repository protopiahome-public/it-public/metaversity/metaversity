ALTER TABLE `user`
  ADD COLUMN `potential_dept_id` INTEGER UNSIGNED AFTER `is_moderator_anywhere_calc`;

ALTER TABLE `user` 
  ADD CONSTRAINT `FK_user_potential_dept_id` FOREIGN KEY `FK_user_potential_dept_id` (`potential_dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;
