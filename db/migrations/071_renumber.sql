-- format

ALTER TABLE `activity` DROP INDEX `FK_activity_format_id`,
 DROP FOREIGN KEY `FK_activity_format_id`;

 ALTER TABLE `activity` MODIFY COLUMN `format_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
 ADD CONSTRAINT `FK_activity_format_id` FOREIGN KEY `FK_activity_format_id` (`format_id`)
    REFERENCES `format` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

SELECT * FROM `format` f ORDER BY id DESC LIMIT 100;

SET @i=0;
UPDATE format SET id=(@i:=@i+1);

-- rate
-- can be updates if needed (checked that all FKs exist)

-- cleanup needed for roles renumbering
DELETE l
FROM activity_role_link l
LEFT JOIN activity a ON a.id = l.activity_id
WHERE a.id IS NULL;

-- role
ALTER TABLE `role` ADD COLUMN `old_id` INTEGER UNSIGNED AFTER `descr_html`;
ALTER TABLE `role` ADD INDEX `role_old_id`(`old_id`);
ALTER TABLE `activity_participant_log` MODIFY COLUMN `role_id` INTEGER NOT NULL DEFAULT 0;
ALTER TABLE `activity_participant_mark_log` MODIFY COLUMN `role_id` INTEGER NOT NULL DEFAULT 0;
UPDATE role SET old_id = id;
SET @i=0;
UPDATE role SET id=(@i:=@i+1);

UPDATE activity_participant_log l
LEFT JOIN role r ON l.role_id = r.old_id
SET l.role_id = IF(r.id IS NULL, -ABS(l.role_id), r.id);

UPDATE activity_participant_mark_log l
LEFT JOIN role r ON l.role_id = r.old_id
SET l.role_id = IF(r.id IS NULL, -ABS(l.role_id), r.id);

-- format_group
-- @todo - does not work because of parent_id
-- @see https://stackoverflow.com/questions/35255362/mysql-cannot-update-parent-row-when-i-have-on-update-cascade
-- need to remove FK, update id, update parent_id manually, restore FK
