ALTER TABLE `user` 
  MODIFY COLUMN `results_access_level` ENUM('all','registered','dept_students','any_moderator') NOT NULL DEFAULT 'all',
  MODIFY COLUMN `marks_access_level` ENUM('all','registered','dept_students','any_moderator') NOT NULL DEFAULT 'all';
