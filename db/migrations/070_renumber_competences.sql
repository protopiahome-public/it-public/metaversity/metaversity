DELETE i FROM integral_competence i LEFT JOIN competence_full c ON c.id = i.included_competence_id
WHERE c.id IS NULL;
ALTER TABLE `integral_competence`
	DROP FOREIGN KEY `FK_integral_competence_included_competence_id`;
ALTER TABLE `integral_competence`
	ADD CONSTRAINT `FK_integral_competence_included_competence_id` FOREIGN KEY (`included_competence_id`) REFERENCES `competence_full` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;
/*ALTER TABLE `activity_competence_mark_log`
	ADD CONSTRAINT `FK_activity_competence_mark_log_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON UPDATE CASCADE ON DELETE NO ACTION;*/
ALTER TABLE `competence_full`
	ADD COLUMN `old_id` INT(10) UNSIGNED NULL AFTER `id`;
ALTER TABLE `competence_translation_formulae`
	ADD COLUMN `old_eq_competence_list` TEXT NULL AFTER `eq_competence_list`;