CREATE TABLE `study_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `dept_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_study_level_dept_id` (`dept_id`),
  CONSTRAINT `FK_study_level_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `group`
  ADD COLUMN `study_level_id` INTEGER UNSIGNED DEFAULT NULL AFTER `dept_id`;

ALTER TABLE `group` 
  ADD CONSTRAINT `FK_group_study_level_id` FOREIGN KEY `FK_group_study_level_id` (`study_level_id`)
    REFERENCES `study_level` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `study_level`
  ADD COLUMN `group_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `dept_id`;

ALTER TABLE `study_level`
  ADD COLUMN `add_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `group_count_calc`,
  ADD COLUMN `edit_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `add_time`;

ALTER TABLE `activity_dept_link`
  ADD COLUMN `study_level_id` INT(10) UNSIGNED AFTER `dept_id`,
  ADD CONSTRAINT `FK_activity_dept_link_study_level_id` FOREIGN KEY `FK_activity_dept_link_study_level_id` (`study_level_id`)
    REFERENCES `study_level` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;
