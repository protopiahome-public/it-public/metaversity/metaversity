CREATE TABLE `api_session` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT UNSIGNED NOT NULL,
	`app_title` VARCHAR(255) NOT NULL,
	`redirect_url` VARCHAR(255) NOT NULL,
	`token` VARCHAR(32) NOT NULL,
	`start_time` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_api_session_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;