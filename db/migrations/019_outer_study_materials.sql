ALTER TABLE `study_material` ADD COLUMN `is_outer` BOOLEAN NOT NULL DEFAULT 0 AFTER `read_user_count_calc`,
 ADD COLUMN `outer_url` VARCHAR(1024) NOT NULL AFTER `is_outer`;