CREATE TABLE `role` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `format_id` INTEGER UNSIGNED NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_role_format_id` FOREIGN KEY `FK_role_format_id` (`format_id`)
    REFERENCES `format` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

ALTER TABLE `role` 
  ADD COLUMN `number` INTEGER(3) UNSIGNED NOT NULL DEFAULT 0 AFTER `title`;

ALTER TABLE `role` 
  ADD COLUMN `add_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `number`,
  ADD COLUMN `edit_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `add_time`;

ALTER TABLE `format`
  ADD COLUMN `role_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `edit_time`;

ALTER TABLE `sys_params`
  ADD COLUMN `students_competence_set_id` INTEGER UNSIGNED NOT NULL DEFAULT 1 AFTER `color_scheme`,
  ADD CONSTRAINT `FK_sys_params_students_competence_set_id` FOREIGN KEY `FK_sys_params_students_competence_set_id` (`students_competence_set_id`)
    REFERENCES `competence_set` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

ALTER TABLE `role`
  ADD COLUMN `competence_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `edit_time`,
  ADD COLUMN `rate_id` INTEGER UNSIGNED AFTER `competence_count_calc`,
  ADD CONSTRAINT `FK_role_rate_id` FOREIGN KEY `FK_role_rate_id` (`rate_id`)
    REFERENCES `rate` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

INSERT INTO rate_type (id, title, main_table) VALUES (5, 'Роль', 'role');

