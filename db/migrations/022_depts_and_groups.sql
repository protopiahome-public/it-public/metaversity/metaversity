CREATE TABLE `dept` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `title_short` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `dept`
  ADD COLUMN `add_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `title_short`,
  ADD COLUMN `edit_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `add_time`;

CREATE TABLE `dept_city_link` (
  `dept_id` INTEGER UNSIGNED NOT NULL,
  `city_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`dept_id`, `city_id`)
)
ENGINE = InnoDB;

ALTER TABLE `dept_city_link`
  ADD CONSTRAINT `FK_dept_city_link_dept_id` FOREIGN KEY `FK_dept_city_link_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
 ADD CONSTRAINT `FK_dept_city_link_city_id` FOREIGN KEY `FK_dept_city_link_city_id` (`city_id`)
    REFERENCES `city` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `dept`
  ADD COLUMN `city_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `edit_time`;

ALTER TABLE `city`
  ADD COLUMN `dept_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `edit_time`;

CREATE TABLE `group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(28) NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_id` int(10) unsigned NOT NULL,
  `dept_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_group_city_id` (`city_id`),
  KEY `FK_group_dept_id` (`dept_id`),
  CONSTRAINT `FK_group_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_group_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `dept`
  ADD COLUMN `group_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `city_count_calc`;

ALTER TABLE `city`
  ADD COLUMN `group_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `dept_count_calc`;
