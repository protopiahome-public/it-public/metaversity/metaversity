ALTER TABLE `activity` 
  CHANGE COLUMN `participant_count_calc` `accepted_participant_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `activity` 
  ADD COLUMN `premoderation_participant_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `accepted_participant_count_calc`;

ALTER TABLE `activity_city_link` 
  CHANGE COLUMN `participant_count_calc` `accepted_participant_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `activity_city_link` 
  ADD COLUMN `premoderation_participant_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `accepted_participant_count_calc`;

ALTER TABLE `activity` 
  CHANGE COLUMN `premoderation_participant_count_calc` `premoderation_request_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0;

ALTER TABLE `activity_city_link` 
  CHANGE COLUMN `premoderation_participant_count_calc` `premoderation_request_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0;

/* Stat update from scripts/activity_participants.sql */

UPDATE activity a
SET accepted_participant_count_calc = (
	SELECT COUNT(*) 
	FROM activity_participant_status_calc 
	WHERE activity_id = a.id
		AND status = 'accepted'
), premoderation_request_count_calc = (
	SELECT COUNT(*) 
	FROM activity_participant 
	WHERE activity_id = a.id
		AND status = 'premoderation'
);

UPDATE activity_city_link SET accepted_participant_count_calc = 0;
UPDATE activity_city_link SET premoderation_request_count_calc = 0;

INSERT IGNORE INTO activity_city_link (activity_id, city_id)
SELECT activity_id, city_id
FROM activity_participant
WHERE status = 'accepted' OR status = 'premoderation'
GROUP BY activity_id, city_id;

UPDATE activity_city_link l
SET accepted_participant_count_calc = (
	SELECT COUNT(*) 
	FROM activity_city_participant_status_calc 
	WHERE activity_id = l.activity_id
		AND city_id = l.city_id
		AND status = 'accepted'
), premoderation_request_count_calc = (
	SELECT COUNT(*) 
	FROM activity_participant 
	WHERE activity_id = l.activity_id
		AND city_id = l.city_id
		AND status = 'premoderation'
);