CREATE TABLE `intmarkup_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `object_type` char(20) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `mime_type` varchar(255) NOT NULL,
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `ext` varchar(10) NOT NULL DEFAULT '',
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `position` int(10) unsigned NOT NULL DEFAULT '99999',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `Unque` (`object_type`,`object_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `news_item`
  ADD COLUMN `announce_text` MEDIUMTEXT AFTER `comment_count_calc`,
  ADD COLUMN `announce_html` MEDIUMTEXT AFTER `announce_text`,
  ADD COLUMN `descr_text` MEDIUMTEXT AFTER `announce_html`,
  ADD COLUMN `descr_html` MEDIUMTEXT AFTER `descr_text`;

CREATE TABLE `activity_participant` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `status` enum('declined','premoderation','accepted','deleted') NOT NULL DEFAULT 'premoderation',
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `change_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_id`,`user_id`,`role_id`),
  KEY `FK_activity_participant_user_id` (`user_id`),
  KEY `FK_activity_participant_role_id` (`role_id`),
  CONSTRAINT `FK_activity_participant_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `activity_participant_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `status` enum('declined','premoderation','accepted','deleted') NOT NULL,
  `add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_activity_participant_log_activity_id` (`activity_id`),
  KEY `FK_activity_participant_log_user_id` (`user_id`),
  KEY `FK_activity_participant_log_role_id` (`role_id`),
  KEY `FK_activity_participant_log_changer_user_id` (`changer_user_id`),
  CONSTRAINT `FK_activity_participant_log_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_activity_participant_log_changer_user_id` FOREIGN KEY (`changer_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_activity_participant_log_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_activity_participant_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `activity_participant_mark` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `mark` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`activity_id`,`user_id`,`role_id`),
  KEY `FK_activity_participant_mark_user_id` (`user_id`),
  KEY `FK_activity_participant_mark_role_id` (`role_id`),
  CONSTRAINT `FK_activity_participant_mark_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_mark_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_mark_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `activity_participant_mark_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `mark` tinyint(3) DEFAULT NULL,
  `add_time` datetime NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_activity_participant_mark_log_activity_id` (`activity_id`),
  KEY `FK_activity_participant_mark_log_user_id` (`user_id`),
  KEY `FK_activity_participant_mark_log_role_id` (`role_id`),
  KEY `FK_activity_participant_mark_log_changer_user_id` (`changer_user_id`),
  CONSTRAINT `FK_activity_participant_mark_log_changer_user_id` FOREIGN KEY (`changer_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_activity_participant_mark_log_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_activity_participant_mark_log_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_activity_participant_mark_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `position`
  ADD COLUMN `descr_text` MEDIUMTEXT AFTER `rate_id`,
  ADD COLUMN `descr_html` MEDIUMTEXT AFTER `descr_text`;

ALTER TABLE `module_city_link`
  ADD COLUMN `add_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `finish_date`,
  ADD COLUMN `edit_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `add_time`;

ALTER TABLE `module`
  ADD COLUMN `descr_text` MEDIUMTEXT AFTER `block_set_id`,
  ADD COLUMN `descr_html` MEDIUMTEXT AFTER `descr_text`;

ALTER TABLE `activity_participant_log` DROP INDEX `FK_activity_participant_log_activity_id`
, DROP INDEX `FK_activity_participant_log_user_id`
, DROP INDEX `FK_activity_participant_log_role_id`
, DROP INDEX `FK_activity_participant_log_changer_user_id`,
 DROP FOREIGN KEY `FK_activity_participant_log_activity_id`,
 DROP FOREIGN KEY `FK_activity_participant_log_changer_user_id`,
 DROP FOREIGN KEY `FK_activity_participant_log_role_id`,
 DROP FOREIGN KEY `FK_activity_participant_log_user_id`;

ALTER TABLE `activity_participant_mark_log` DROP INDEX `FK_activity_participant_mark_log_activity_id`
, DROP INDEX `FK_activity_participant_mark_log_user_id`
, DROP INDEX `FK_activity_participant_mark_log_role_id`
, DROP INDEX `FK_activity_participant_mark_log_changer_user_id`,
 DROP FOREIGN KEY `FK_activity_participant_mark_log_changer_user_id`,
 DROP FOREIGN KEY `FK_activity_participant_mark_log_activity_id`,
 DROP FOREIGN KEY `FK_activity_participant_mark_log_role_id`,
 DROP FOREIGN KEY `FK_activity_participant_mark_log_user_id`;
