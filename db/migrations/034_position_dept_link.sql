CREATE TABLE `position_dept_link` (
  `position_id` INTEGER UNSIGNED NOT NULL,
  `dept_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`position_id`, `dept_id`),
  CONSTRAINT `FK_position_dept_link_dept_id` FOREIGN KEY `FK_position_dept_link_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_position_dept_link_position_id` FOREIGN KEY `FK_position_dept_link_position_id` (`position_id`)
    REFERENCES `position` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;

INSERT INTO position_dept_link (position_id, dept_id) (SELECT id, dept_id FROM position WHERE NOT(dept_id IS NULL));

ALTER TABLE `position` DROP COLUMN `dept_id`
, DROP INDEX `FK_position_dept_id`,
 DROP FOREIGN KEY `FK_position_dept_id`;