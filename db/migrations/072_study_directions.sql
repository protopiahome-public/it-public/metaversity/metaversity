CREATE TABLE `study_direction` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(100) NOT NULL,
	`stream_id` INT(10) UNSIGNED NOT NULL,
	`add_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`edit_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	INDEX `FK_study_direction_stream_id` (`stream_id`),
	CONSTRAINT `FK_study_direction_stream_id` FOREIGN KEY (`stream_id`) REFERENCES `stream` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `position_study_direction_link` (
	`position_id` INT(10) UNSIGNED NOT NULL,
	`study_direction_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`position_id`, `study_direction_id`),
	INDEX `FK_position_study_direction_link_study_direction_id` (`study_direction_id`),
	CONSTRAINT `FK_position_study_direction_link_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_position_study_direction_link_study_direction_id` FOREIGN KEY (`study_direction_id`) REFERENCES `study_direction` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
