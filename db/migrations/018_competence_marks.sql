CREATE TABLE `activity_competence_mark` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` tinyint(3) unsigned NOT NULL,
  `comment` text,
  `change_time` datetime NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`,`competence_id`),
  KEY `FK_activity_competence_mark_user_id` (`user_id`),
  KEY `FK_activity_competence_mark_competence_id` (`competence_id`),
  KEY `FK_activity_competence_mark_changer_user_id` (`changer_user_id`),
  CONSTRAINT `FK_activity_competence_mark_changer_user_id` FOREIGN KEY (`changer_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_competence_mark_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_competence_mark_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_competence_mark_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `activity_competence_mark_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `competence_id` int(10) unsigned NOT NULL,
  `mark` tinyint(3) unsigned DEFAULT NULL,
  `comment` text,
  `change_time` datetime NOT NULL,
  `changer_user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `activity`
  MODIFY COLUMN `date_type` ENUM('event','deadline','longterm') NOT NULL DEFAULT 'event';

ALTER TABLE `activity_participant_mark`
  ADD COLUMN `change_time` DATETIME NOT NULL AFTER `comment`,
  ADD COLUMN `changer_user_id` INTEGER UNSIGNED NOT NULL AFTER `change_time`;

ALTER TABLE `activity_participant_mark_log`
  CHANGE COLUMN `add_time` `change_time` DATETIME NOT NULL;

/* TO CHECK: Must have zero rows */
/* TO CHECK: Without WHERE must have the same number of rows as 
             SELECT COUNT(*) FROM activity_participant_mark */
SELECT m.*
FROM activity_participant_mark m
LEFT JOIN (
  SELECT activity_id, user_id, role_id, city_id, MAX(id) AS id
  FROM activity_participant_mark_log ll
  GROUP BY activity_id, user_id, role_id, city_id
) x USING(activity_id, user_id, role_id, city_id)
INNER JOIN activity_participant_mark_log l ON l.id = x.id
WHERE m.mark <> l.mark;

UPDATE activity_participant_mark m
LEFT JOIN (
  SELECT activity_id, user_id, role_id, city_id, MAX(id) AS id
  FROM activity_participant_mark_log ll
  GROUP BY activity_id, user_id, role_id, city_id
) x USING(activity_id, user_id, role_id, city_id)
LEFT JOIN activity_participant_mark_log l ON l.id = x.id
SET
  m.change_time = l.change_time,
  m.changer_user_id = l.changer_user_id;
