CREATE TABLE `activity_dept_crosspost` (
  `activity_id` int(10) unsigned NOT NULL,
  `dept_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`dept_id`),
  KEY `FK_activity_dept_crosspost_dept_id` (`dept_id`),
  CONSTRAINT `FK_activity_dept_crosspost_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`),
  CONSTRAINT `FK_activity_dept_crosspost_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

