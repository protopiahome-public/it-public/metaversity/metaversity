ALTER TABLE `activity` 
  ADD COLUMN `unset_mark_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `old_id`;

UPDATE activity a
SET unset_mark_count_calc = (
  SELECT COUNT(*)
  FROM activity_participant ap
  LEFT JOIN activity_participant_mark apm ON apm.activity_id = ap.activity_id
    AND apm.user_id = ap.user_id AND apm.role_id = ap.role_id
  WHERE ap.activity_id = a.id
    AND ap.status = 'accepted'
    AND apm.mark IS NULL
);

ALTER TABLE `activity` 
  ADD COLUMN `mark_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `unset_mark_count_calc`;

UPDATE activity a
SET mark_count_calc = (
  SELECT COUNT(*)
  FROM activity_participant_mark apm
  WHERE apm.activity_id = a.id
);
