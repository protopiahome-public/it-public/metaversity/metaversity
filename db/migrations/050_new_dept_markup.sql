ALTER TABLE `city` 
  ADD COLUMN `is_fake` BOOLEAN NOT NULL DEFAULT 0 AFTER `group_count_calc`;

ALTER TABLE `dept` 
  ADD COLUMN `list_descr` VARCHAR(600) NOT NULL DEFAULT '' AFTER `competence_set_id`;

/* Copy list_descr_text --> list_descr manually */

ALTER TABLE `dept` 
  DROP COLUMN `list_descr_text`,
  DROP COLUMN `list_descr_html`;
