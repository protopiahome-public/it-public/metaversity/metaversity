CREATE TABLE `activity_dept_link` (
  `activity_id` INTEGER UNSIGNED NOT NULL,
  `dept_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`activity_id`, `dept_id`),
  CONSTRAINT `FK_activity_dept_link_activity_id` FOREIGN KEY `FK_activity_dept_link_activity_id` (`activity_id`)
    REFERENCES `activity` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_dept_link_dept_id` FOREIGN KEY `FK_activity_dept_link_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;
