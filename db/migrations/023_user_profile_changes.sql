ALTER TABLE `user`
  ADD COLUMN `dept_id` INTEGER UNSIGNED NOT NULL DEFAULT 1 AFTER `mark_count_calc`,
  ADD CONSTRAINT `FK_user_dept_id` FOREIGN KEY `FK_user_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

ALTER TABLE `user`
  ADD COLUMN `group_id` INTEGER UNSIGNED AFTER `dept_id`,
  ADD CONSTRAINT `FK_user_group_id` FOREIGN KEY `FK_user_group_id` (`group_id`)
    REFERENCES `group` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `group`
  ADD COLUMN `user_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `dept_id`;

ALTER TABLE `group`
  MODIFY COLUMN `title` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
