ALTER TABLE `activity` 
  ADD COLUMN `longterm_graded_user_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `mark_count_calc`;

UPDATE activity a
JOIN dept d ON d.id = a.dept_id
SET longterm_graded_user_count_calc = (
	SELECT COUNT(DISTINCT user_id)
	FROM activity_competence_mark acm
	JOIN competence_calc c ON c.competence_id = acm.competence_id
	WHERE acm.activity_id = a.id
		AND c.competence_set_id = d.competence_set_id
);

ALTER TABLE `activity` 
  ADD COLUMN `longterm_mark_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `longterm_graded_user_count_calc`;

UPDATE activity a
JOIN dept d ON d.id = a.dept_id
SET longterm_mark_count_calc = (
	SELECT COUNT(*)
	FROM activity_competence_mark acm
	JOIN competence_calc c ON c.competence_id = acm.competence_id
	WHERE acm.activity_id = a.id
		AND c.competence_set_id = d.competence_set_id
);

ALTER TABLE `activity_calc` 
  ADD COLUMN `longterm_graded_user_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `mark_count_calc`,
  ADD COLUMN `longterm_mark_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `longterm_graded_user_count_calc`;

ALTER TABLE `activity_calc`
  ADD COLUMN `date_type` ENUM('event','deadline','longterm') NOT NULL DEFAULT 'event' AFTER `metaactivity_id`;

UPDATE activity_calc ac
LEFT JOIN activity a ON a.id = ac.activity_id
SET ac.longterm_graded_user_count_calc = a.longterm_graded_user_count_calc,
	ac.longterm_mark_count_calc = a.longterm_mark_count_calc,
	ac.date_type = a.date_type;
