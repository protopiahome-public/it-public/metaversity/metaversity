ALTER TABLE `position` ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `descr_html`,
 ADD CONSTRAINT `FK_position_dept_id` FOREIGN KEY `FK_position_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;
