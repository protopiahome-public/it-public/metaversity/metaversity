CREATE TABLE `user_foreign_statistic` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER UNSIGNED NOT NULL,
  `provider` VARCHAR(45) NOT NULL,
  `test_url` VARCHAR(255) NOT NULL,
  `success_percent` INTEGER UNSIGNED NOT NULL,
  `add_time` DATETIME NOT NULL,
  `test_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_user_foreign_statistics_user_id` FOREIGN KEY `FK_user_foreign_statistics_user_id` (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;