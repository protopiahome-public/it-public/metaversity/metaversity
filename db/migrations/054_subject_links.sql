CREATE TABLE `activity_subject_link` (
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subject_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`subject_id`),
  KEY `FK_activity_subject_link_subject_id` (`subject_id`),
  CONSTRAINT `FK_activity_subject_link_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_subject_link_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
