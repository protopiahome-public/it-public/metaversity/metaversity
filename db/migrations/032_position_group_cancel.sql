ALTER TABLE `position` DROP COLUMN `position_group_id`
, DROP INDEX `FK_position_position_group_id`,
 DROP FOREIGN KEY `FK_position_position_group_id`;

DROP TABLE position_group