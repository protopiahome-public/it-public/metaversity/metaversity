CREATE TABLE `competence_study_level_link` (
  `competence_id` int(10) unsigned NOT NULL,
  `study_level_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_id`,`study_level_id`),
  KEY `FK_competence_study_level_link_study_level_id` (`study_level_id`),
  CONSTRAINT `FK_competence_study_level_link_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_study_level_link_study_level_id` FOREIGN KEY (`study_level_id`) REFERENCES `study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
