CREATE TABLE `format_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_format_group_parent_id` (`parent_id`),
  CONSTRAINT `FK_format_group_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `format_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `format_group` (id, title) VALUES (1, 'Все старые форматы');

ALTER TABLE `format`
  ADD COLUMN `format_group_id` INTEGER UNSIGNED NOT NULL DEFAULT 1 AFTER `role_count_calc`,
  ADD CONSTRAINT `FK_format_format_group_id` FOREIGN KEY `FK_format_format_group_id` (`format_group_id`)
    REFERENCES `format_group` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `format`
  ADD COLUMN `position` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `format_group_id`;
