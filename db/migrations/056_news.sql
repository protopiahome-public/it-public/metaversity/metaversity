ALTER TABLE `user` 
ADD COLUMN `is_moderator_anywhere_calc` BOOLEAN NOT NULL DEFAULT 0 AFTER `last_name_en`;

UPDATE user u
LEFT JOIN (
  SELECT l.user_id
  FROM dept_user_link l
  WHERE status = 'admin' or status = 'moderator'
  GROUP BY user_id
) x ON x.user_id = u.id
SET u.is_moderator_anywhere_calc = u.is_admin OR x.user_id IS NOT NULL;

CREATE TABLE `system_news_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `is_for_moderators` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `announce_text` mediumtext,
  `announce_html` mediumtext,
  `descr_text` mediumtext,
  `descr_html` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_system_news_item_adder_user_id` (`adder_user_id`),
  CONSTRAINT `FK_system_news_item_adder_user_id` FOREIGN KEY (`adder_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `activity_participant` 
  ADD COLUMN `changer_user_id` INTEGER UNSIGNED DEFAULT NULL AFTER `email_to_send`;

ALTER TABLE `activity_participant` 
  ADD CONSTRAINT `FK_activity_participant_changer_user_id` FOREIGN KEY `FK_activity_participant_changer_user_id` (`changer_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

/* !!! VERY LONG AND CPU-CONSUMING UPDATE !!! */
UPDATE activity_participant ap
LEFT JOIN activity_participant_log apl ON apl.activity_id = ap.activity_id
  AND apl.user_id = ap.user_id
  AND apl.role_id = ap.role_id
  AND apl.status = ap.status
  AND apl.add_time = ap.change_time
SET ap.changer_user_id = apl.changer_user_id;

ALTER TABLE `activity_participant` 
  ADD COLUMN `is_for_news` BOOLEAN NOT NULL DEFAULT 0 AFTER `changer_user_id`;

