ALTER TABLE `activity_participant_mark` 
  ADD CONSTRAINT `FK_activity_participant_mark_changer_user_id` FOREIGN KEY `FK_activity_participant_mark_changer_user_id` (`changer_user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

CREATE TABLE `user_merge_log` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `merge_time` DATETIME NOT NULL,
  `merger_user_id` INTEGER UNSIGNED NOT NULL,
  `remove_user_id` INTEGER UNSIGNED NOT NULL,
  `merge_into_user_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

CREATE TABLE `user_merge_run_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sql_script_name` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `ok` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
