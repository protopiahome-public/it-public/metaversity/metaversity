ALTER TABLE `city`
 ADD COLUMN `title_en` VARCHAR(40) NOT NULL DEFAULT '' AFTER `title`;

UPDATE city SET title_en = title;

ALTER TABLE `dept`
 ADD COLUMN `title_en` VARCHAR(100) NOT NULL DEFAULT '' AFTER `title`,
 ADD COLUMN `title_short_en` VARCHAR(10) NOT NULL DEFAULT '' AFTER `title_short`;

UPDATE dept
SET title_en = title, title_short_en = title_short;

CREATE TABLE `lang` (
  `code` char(2) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `title_en` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO lang VALUES ('en', 'English', 'English');
INSERT INTO lang VALUES ('ru', 'Русский', 'Russian');

ALTER TABLE `dept` 
  ADD COLUMN `lang_code` CHAR(2) NOT NULL DEFAULT '' AFTER `list_descr`;

UPDATE dept SET lang_code = 'ru';

ALTER TABLE `dept` 
  ADD CONSTRAINT `FK_dept_lang_code` FOREIGN KEY `FK_dept_lang_code` (`lang_code`)
    REFERENCES `lang` (`code`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;
