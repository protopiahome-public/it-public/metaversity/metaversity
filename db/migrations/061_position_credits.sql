ALTER TABLE `dept` 
  MODIFY COLUMN `competence_set_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  ADD COLUMN `position_credit_min_match` TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER `lang_code`;

CREATE TABLE `position_credit` (
  `position_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `change_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expert_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`position_id`,`user_id`),
  KEY `FK_position_credit_user_id` (`user_id`),
  KEY `FK_position_credit_expert_user_id` (`expert_user_id`),
  CONSTRAINT `FK_position_credit_expert_user_id` FOREIGN KEY (`expert_user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_position_credit_position_id` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_position_credit_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `position_credit_log` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `position_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `change_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expert_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `position_credit_log` ADD COLUMN `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER `expert_user_id`;

ALTER TABLE `position_credit_log` MODIFY COLUMN `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT;
