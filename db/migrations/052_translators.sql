DROP TABLE IF EXISTS `competence_translator`;

CREATE TABLE `competence_translator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `eq_competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `formulae_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  `direct_count_calc` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_competence_translator_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_translator_eq_competence_set_id` (`eq_competence_set_id`),
  CONSTRAINT `FK_competence_translator_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translator_eq_competence_set_id` FOREIGN KEY (`eq_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `competence_translation_formulae` (
  `competence_id` int(10) unsigned NOT NULL DEFAULT '0',
  `competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `eq_competence_set_id` int(10) unsigned NOT NULL DEFAULT '0',
  `eq_competence_list` text,
  `eq_formula` enum('MIN','AVG','RES') NOT NULL DEFAULT 'MIN',
  `eq_max_mark` float(2,1) NOT NULL DEFAULT '3.0',
  PRIMARY KEY (`competence_id`,`eq_competence_set_id`),
  KEY `FK_competence_translation_formulae_competence_set_id` (`competence_set_id`),
  KEY `FK_competence_translation_formulae_eq_competence_set_id` (`eq_competence_set_id`),
  CONSTRAINT `FK_competence_translation_formulae_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_formulae_competence_set_id` FOREIGN KEY (`competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_competence_translation_formulae_eq_competence_set_id` FOREIGN KEY (`eq_competence_set_id`) REFERENCES `competence_set` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `competence_translation_direct` 
  ADD COLUMN `eq_max_mark` FLOAT(2,1) NOT NULL DEFAULT 3.0 AFTER `eq_competence_set_id`;

ALTER TABLE `competence_translation_formulae` 
  MODIFY COLUMN `eq_max_mark` FLOAT(2,1);

UPDATE competence_translation_formulae SET eq_max_mark = NULL;

ALTER TABLE `competence_translation_direct` 
  MODIFY COLUMN `eq_max_mark` FLOAT(2,1);

UPDATE competence_translation_direct SET eq_max_mark = NULL;

INSERT INTO competence_translator
SELECT NULL, competence_set_id, eq_competence_set_id, 0, 0
FROM competence_translation_direct
GROUP BY competence_set_id, eq_competence_set_id;

UPDATE competence_translator t
SET t.direct_count_calc = (
  SELECT COUNT(*)
  FROM competence_translation_direct x
  WHERE x.competence_set_id = t.competence_set_id AND x.eq_competence_set_id = t.eq_competence_set_id
);