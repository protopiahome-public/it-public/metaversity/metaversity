CREATE TABLE `activity_participants_subscription` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`) USING BTREE,
  KEY `FK_activity_participants_subscription_user_id` (`user_id`),
  CONSTRAINT `FK_activity_participants_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participants_subscription_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `activity_participants_city_subscription` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`,`city_id`),
  KEY `FK_activity_participants_city_subscription_user_id` (`user_id`) USING BTREE,
  KEY `FK_activity_participants_city_subscription_city_id` (`city_id`) USING BTREE,
  CONSTRAINT `FK_activity_participants_city_subscription_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participants_city_subscription_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participants_city_subscription_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `activity_participant`
  ADD COLUMN `email_to_send` MEDIUMTEXT AFTER `change_time`;
