CREATE TABLE `activity_participant_calc` (
  `activity_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('declined','premoderation','accepted') NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`),
  KEY `FK_activity_participant_calc_user_id` (`user_id`),
  CONSTRAINT `FK_activity_participant_calc_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_participant_calc_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

!!!db_up.php!!!

CALL activity_participant_recalc_all();

ALTER TABLE `activity`
  ADD COLUMN `participant_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `moderators_html`;

UPDATE activity a
LEFT JOIN (
  SELECT activity_id, COUNT(*) cnt
  FROM activity_participant_calc
  WHERE status = 'accepted'
  GROUP BY activity_id
) x ON x.activity_id = a.id
SET a.participant_count_calc = IFNULL(cnt, 0);

DROP TABLE IF EXISTS `activity_role_city_stat`;

CREATE TABLE `activity_role_city_stat` (
  `activity_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `taken_number` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`role_id`,`city_id`) USING BTREE,
  KEY `FK_activity_role_city_stat_role_id` (`role_id`),
  KEY `FK_activity_role_city_stat_city_id` (`city_id`),
  CONSTRAINT `FK_activity_role_city_stat_city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_city_stat_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_city_stat_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `activity_role_stat` (
  `activity_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `taken_number` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`,`role_id`) USING BTREE,
  KEY `FK_activity_role_stat_role_id` (`role_id`),
  CONSTRAINT `FK_activity_role_stat_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_activity_role_stat_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
