ALTER TABLE `sys_params` 
  ADD COLUMN `central_competence_set_id` INTEGER UNSIGNED NOT NULL DEFAULT 1 AFTER `students_competence_set_id`;
