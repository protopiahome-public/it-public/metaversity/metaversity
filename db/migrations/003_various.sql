ALTER TABLE `activity`
  ADD COLUMN `descr_text` MEDIUMTEXT AFTER `moderator_content_block_set_id`,
  ADD COLUMN `descr_html` MEDIUMTEXT AFTER `descr_text`;

ALTER TABLE `activity`
  ADD COLUMN `moderators_text` MEDIUMTEXT AFTER `descr_html`,
  ADD COLUMN `moderators_html` MEDIUMTEXT AFTER `moderators_text`;

ALTER TABLE `activity_city_link`
  ADD COLUMN `descr_text` MEDIUMTEXT AFTER `enabled`,
  ADD COLUMN `descr_html` MEDIUMTEXT AFTER `descr_text`;

ALTER TABLE `activity_city_link`
  ADD COLUMN `add_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `descr_html`,
  ADD COLUMN `edit_time` DATETIME NOT NULL DEFAULT '0000-00-00' AFTER `add_time`;

