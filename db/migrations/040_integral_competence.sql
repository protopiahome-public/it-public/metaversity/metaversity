CREATE TABLE `integral_competence` (
  `competence_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `included_competence_id` int(10) unsigned NOT NULL,
  `mark` int(10) unsigned NOT NULL,
  PRIMARY KEY (`competence_id`,`included_competence_id`),
  CONSTRAINT `FK_integral_competence_included_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_integral_competence_competence_id` FOREIGN KEY (`competence_id`) REFERENCES `competence_full` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `competence_full`
  ADD COLUMN `is_integral` BOOLEAN NOT NULL DEFAULT 0 AFTER `total_mark_count_calc`;

ALTER TABLE `competence_full`
  ADD COLUMN `integral_included_competence_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `is_integral`;
