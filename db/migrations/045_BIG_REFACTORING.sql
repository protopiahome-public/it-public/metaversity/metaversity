ALTER TABLE `dept`
  ADD COLUMN `name` VARCHAR(40) NOT NULL AFTER `id`;

ALTER TABLE `format`
  MODIFY COLUMN `format_group_id` INT(10) UNSIGNED NOT NULL;

//set name:
wide, prep, caprus, etc.

CREATE TABLE `dept_admin` (
  `user_id` int(10) unsigned NOT NULL,
  `dept_id` int(10) unsigned NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adder_admin_user_id` int(10) unsigned DEFAULT NULL,
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_admin_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`,`dept_id`),
  KEY `FK_dept_admin_dept_id` (`dept_id`),
  KEY `FK_dept_admin_adder_admin_user_id` (`adder_admin_user_id`),
  KEY `FK_dept_admin_editor_admin_user_id` (`editor_admin_user_id`),
  CONSTRAINT `FK_dept_admin_adder_admin_user_id` FOREIGN KEY (`adder_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_dept_admin_editor_admin_user_id` FOREIGN KEY (`editor_admin_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_dept_admin_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dept_admin_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `dept_user_link` (
  `dept_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `add_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('deleted','pretender','member','moderator','admin') NOT NULL DEFAULT 'deleted',
  `edit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`dept_id`,`user_id`),
  KEY `FK_dept_user_link_user_id` (`user_id`),
  CONSTRAINT `FK_dept_user_link_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dept_user_link_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `user`
  DROP INDEX `FK_user_dept_id`,
  DROP FOREIGN KEY `FK_user_dept_id`;

ALTER TABLE `dept`
  ADD COLUMN `join_premoderation` BOOLEAN NOT NULL DEFAULT 0 AFTER `title_short`;

ALTER TABLE `dept`
  ADD COLUMN `list_show` BOOLEAN NOT NULL DEFAULT 1 AFTER `user_count_calc`,
  ADD COLUMN `list_descr_text` TEXT AFTER `list_show`,
  ADD COLUMN `list_descr_html` TEXT AFTER `list_descr_text`;

ALTER TABLE `dept`
  ADD COLUMN `list_position` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `list_descr_html`;

ALTER TABLE `subject`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_subject_dept_id` FOREIGN KEY `FK_subject_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE subject SET dept_id = 3;

ALTER TABLE `subject`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `dept`
  ADD COLUMN `competence_set_id` INTEGER UNSIGNED AFTER `list_position`,
  ADD CONSTRAINT `FK_dept_competence_set_id` FOREIGN KEY `FK_dept_competence_set_id` (`competence_set_id`)
    REFERENCES `competence_set` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;

UPDATE dept
SET competence_set_id = (SELECT students_competence_set_id FROM sys_params LIMIT 1);

ALTER TABLE `dept`
  MODIFY COLUMN `competence_set_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `position`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_position_dept_id` FOREIGN KEY `FK_position_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE position SET dept_id = 3;

ALTER TABLE `position`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `format`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_format_dept_id` FOREIGN KEY `FK_format_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE format SET dept_id = 3;

ALTER TABLE `format`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `format_group`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_format_group_dept_id` FOREIGN KEY `FK_format_group_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE format_group SET dept_id = 3;

ALTER TABLE `format_group`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `role`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_role_dept_id` FOREIGN KEY `FK_role_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE role SET dept_id = 3;

ALTER TABLE `role`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `module`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_module_dept_id` FOREIGN KEY `FK_module_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE module SET dept_id = 3;

ALTER TABLE `module`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `study_material`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_study_material_dept_id` FOREIGN KEY `FK_study_material_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE study_material SET dept_id = 3;

ALTER TABLE `study_material`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `dept_user_link`
  ADD COLUMN `group_id` INTEGER UNSIGNED AFTER `edit_time`,
  ADD CONSTRAINT `FK_dept_user_link_group_id` FOREIGN KEY `FK_dept_user_link_group_id` (`group_id`)
    REFERENCES `group` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `news_item`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_news_item_dept_id` FOREIGN KEY `FK_news_item_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE news_item SET dept_id = 3;

ALTER TABLE `news_item`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `module`
  ADD COLUMN `city_id` INTEGER UNSIGNED AFTER `show_in_results`,
  ADD COLUMN `start_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `city_id`,
  ADD COLUMN `finish_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `start_date`,
  ADD CONSTRAINT `FK_module_city_id` FOREIGN KEY `FK_module_city_id` (`city_id`)
    REFERENCES `city` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `module`
  MODIFY COLUMN `start_date` DATE DEFAULT '0000-00-00',
  MODIFY COLUMN `finish_date` DATE DEFAULT '0000-00-00';

ALTER TABLE `metaactivity`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_metaactivity_dept_id` FOREIGN KEY `FK_metaactivity_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE metaactivity SET dept_id = 3;

ALTER TABLE `metaactivity`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `activity`
  ADD COLUMN `dept_id` INTEGER UNSIGNED AFTER `id`,
  ADD CONSTRAINT `FK_activity_dept_id` FOREIGN KEY `FK_activity_dept_id` (`dept_id`)
    REFERENCES `dept` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

UPDATE activity SET dept_id = 3;

ALTER TABLE `activity`
  MODIFY COLUMN `dept_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `activity`
  ADD COLUMN `study_level_id` INTEGER UNSIGNED AFTER `dept_id`,
  ADD CONSTRAINT `FK_activity_study_level_id` FOREIGN KEY `FK_activity_study_level_id` (`study_level_id`)
    REFERENCES `study_level` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `activity`
  ADD COLUMN `group_count_calc` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `html`;

run SOME PARTICIPANTS AND MARKS CONVERSION LOGIC

ALTER TABLE `activity_participant` 
  DROP COLUMN `city_id`,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY  USING BTREE(`activity_id`, `user_id`, `role_id`),
  DROP FOREIGN KEY `FK_activity_participant_city_id`;

ALTER TABLE `activity_participant_mark` 
  DROP COLUMN `city_id`,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY  USING BTREE(`activity_id`, `user_id`, `role_id`),
  DROP FOREIGN KEY `FK_activity_participant_mark_city_id`;

ALTER TABLE `activity_participant_log`
  MODIFY COLUMN `city_id` INT(10) UNSIGNED DEFAULT NULL;

ALTER TABLE `activity_participant_mark_log` 
  MODIFY COLUMN `city_id` INT(10) UNSIGNED DEFAULT NULL;

ALTER TABLE `dept_user_link` 
  ADD COLUMN `mark_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `group_id`;

ALTER TABLE `dept_user_link` 
  ADD COLUMN `position_count_calc` INTEGER UNSIGNED NOT NULL DEFAULT 0 AFTER `group_id`;

# ############################ DATA MOVE ############################ #



# ############################ UPDATE ############################ #

run dept_count_calc.sql

# ############################ CLEANUP ############################ #

DROP TABLE city_moderator;
DROP TABLE competence_group_dept_link;
DROP TABLE position_dept_link;
DROP TABLE role_dept_link;
DROP TABLE news_item_dept_link;
DROP TABLE module_moderator;
DROP TABLE module_city_link;
DROP TABLE module_dept_link;
DROP TABLE activity_dept_link;
DROP TABLE activity_city_link;
DROP TABLE activity_role_city_link;
DROP TABLE activity_role_city_stat;
DROP TABLE activity_participants_city_subscription;
DROP TABLE activity_city_participant_status_calc;
DROP TABLE study_material_dept_link;

@todo remove some fields from `activity` (see activity_dt)

ALTER TABLE `user`
  DROP COLUMN `dept_id`,
  DROP COLUMN `group_id`;
