<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/pass/')"/>
	<xsl:variable name="top_section" select="'pass'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:template match="pass">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Password reset')"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Password reset')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:if test="$get_vars[@name='retpath']">
			<input type="hidden" name="retpath2" value="{$get_vars[@name='retpath']}"/>
		</xsl:if>
		<form action="{$save_prefix}/pass/" method="post">
			<xsl:choose>
				<xsl:when test="@step = 1 and $pass_info/info[@name = 'OK']">
					<xsl:call-template name="_draw_step1_ok"/>
				</xsl:when>
				<xsl:when test="@step = 1">
					<xsl:call-template name="_draw_step1"/>
				</xsl:when>
				<xsl:when test="@step = 2 and (@error or $pass_info/error[@name = 'KEY_EXPIRED'] or $pass_info/error[@name = 'BAD_KEY'])">
					<xsl:call-template name="_draw_step2_error"/>
				</xsl:when>
				<xsl:when test="@step = 2">
					<xsl:call-template name="_draw_step2"/>
				</xsl:when>
			</xsl:choose>
		</form>
	</xsl:template>
	<xsl:template name="_draw_step1">
		<div class="box p">
			<p>
				<b>
					<xsl:value-of select="php:function('trans', 'Step 1.')"/>
					<xsl:text> </xsl:text>
				</b>
				<xsl:value-of select="php:function('trans', 'Fill in your email')"/>
				<xsl:text> </xsl:text>
				<b>
					<xsl:value-of select="php:function('trans', 'or')"/>
				</b>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'login')"/>
				<xsl:text>. </xsl:text>
				<xsl:value-of select="php:function('trans', 'We will send a link to your email.')"/>
			</p>
		</div>
		<xsl:choose>
			<xsl:when test="$pass_info/error[@name = 'BLANK']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'Please type in email or login.')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'BAD_LOGIN']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'This login is not registered.')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'BAD_EMAIL']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'This email is not registered.')"/>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<div class="field2">
			<label for="f-login">
				<xsl:value-of select="php:function('trans', 'Email or login')"/>
				<xsl:text>:</xsl:text>
			</label>
			<input id="f-login" class="ctrl ctrl__text" type="text" name="login" maxlength="100" value="{$pass_info/vars/var[@name = 'login']}"/>
		</div>
		<div class="field2">
			<button class="ctrl ctrl__button">
				<xsl:value-of select="php:function('trans', 'Reset my password')"/>
			</button>
		</div>
		<div style="margin-top: 20px;" class="box p">
			<p>
				<b>
					<xsl:value-of select="php:function('trans', 'Step 2.')"/>
				</b>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'Follow the link within the email you received to set a new password')"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template name="_draw_step1_ok">
		<div class="box p">
			<p>
				<xsl:value-of select="php:function('trans', 'We have sent an email letter to')"/>
				<xsl:text> </xsl:text>
				<b>
					<xsl:value-of select="$pass_info/info[@name = 'OK']"/>
				</b>
				<xsl:text>.</xsl:text>
			</p>
			<p>
				<xsl:value-of select="php:function('trans', 'Please follow the link within the email you received to set a new password.')"/>
			</p>
			<p>
				<b>
					<xsl:value-of select="php:function('trans', 'If the letter is not received, please check the Spam folder.')"/>
				</b>
			</p>
		</div>
	</xsl:template>
	<xsl:template name="_draw_step2_error">
		<xsl:choose>
			<xsl:when test="@error = 'KEY_EXPIRED' or $pass_info/error[@name = 'KEY_EXPIRED']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'Password reset key is out of date.')"/>
						<xsl:text> </xsl:text>
						<a href="{$main_prefix}/pass/">
							<xsl:value-of select="php:function('trans', 'Please try again')"/>
						</a>
						<xsl:text>.</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'Password reset key is out of date.')"/>
						<xsl:text> </xsl:text>
						<a href="{$main_prefix}/pass/">
							<xsl:value-of select="php:function('trans', 'Please try again')"/>
						</a>
						<xsl:text>.</xsl:text>
					</span>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_step2">
		<p>
			<xsl:value-of select="php:function('trans', 'Please type in a new password.')"/>
		</p>
		<input type="hidden" name="login" value="{@login}"/>
		<input type="hidden" name="key" value="{@key}"/>
		<xsl:choose>
			<xsl:when test="$pass_info/error[@field = 'password' and @name = 'UNFILLED']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'Password can not be blank.', 'DTF')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@field = 'password' and @name = 'PASSWORDS_NOT_EQUAL']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'The passwords do not match.', 'DTF')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@field = 'password' and @name = 'TOO_SHORT']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'The password is too short. Please use at least 5 characters')"/>
						<xsl:text> (</xsl:text>
						<xsl:value-of select="php:function('trans', 'you used')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="$pass_info/error[@field = 'password' and @name = 'TOO_SHORT']"/>
						<xsl:text>).</xsl:text>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@field = 'password' and @name = 'PASSWORD_NOT_STRONG']">
				<div class="error">
					<span>The password is not safe enough. Please combine letters with digits or increase the length.</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<div class="field2">
			<label for="f-password">
				<xsl:value-of select="php:function('trans', 'Password')"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<input id="f-password" class="ctrl ctrl__text" type="password" name="password" maxlength="64" value="{$pass_info/vars/var[@name = 'password']}"/>
		</div>
		<div class="field2">
			<label for="f-password_check">
				<xsl:value-of select="php:function('trans', 'Confirm the password')"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<input id="f-password_check" class="ctrl ctrl__text" type="password" name="password_check" maxlength="64" value="{$pass_info/vars/var[@name = 'password_check']}"/>
		</div>
		<div class="field2">
			<button class="ctrl ctrl__button">
				<xsl:value-of select="php:function('trans', 'Set new password')"/>
			</button>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Password reset')"/>
	</xsl:template>
</xsl:stylesheet>
