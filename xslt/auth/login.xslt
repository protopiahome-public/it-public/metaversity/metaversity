<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/login.inc.xslt"/>
	<xsl:variable name="top_section" select="'login'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/login/')"/>
	<xsl:template match="login">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Login', 'PROCESS')"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Login', 'PROCESS')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/login/" method="post">
			<xsl:if test="$get_vars[@name='retpath']">
				<input type="hidden" name="retpath" value="{$get_vars[@name='retpath']}"/>
			</xsl:if>
			<xsl:call-template name="draw_login">
				<xsl:with-param name="can_cancel" select="$get_vars[@name='can_cancel']"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Login', 'PROCESS')"/>
	</xsl:template>
</xsl:stylesheet>
