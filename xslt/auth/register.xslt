<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'reg'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:template match="register">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Registration')"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Registration')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:choose>
			<xsl:when test="$pass_info//user_short">
				<p class="maxw500 bold">
					<b>
						<xsl:value-of select="php:function('trans', 'We found similar users.')"/>
					</b>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'Please, DO NOT regiter again if you are one of them')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'DASH')"/>
					<xsl:text> </xsl:text>
					<a href="{$login_url}">
						<xsl:value-of select="php:function('trans', 'just login')"/>
					</a>
					<xsl:text>!</xsl:text>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'Login is shown in the table in parentheses.')"/>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'If you do not remember your password')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'DASH')"/>
					<xsl:text> </xsl:text>
					<a href="{$main_prefix}/pass/?retpath={$request/@retpath}">
						<xsl:value-of select="php:function('trans', 'reset it')"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
				<table class="table table__no_th">
					<xsl:for-each select="$pass_info//user_short">
						<xsl:variable name="current_user" select="."/>
						<tr>
							<td class="cell_user">
								<div class="_img">
									<a href="{$users_prefix}/{@login}/">
										<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
									</a>
								</div>
								<div class="_title">
									<a href="{$users_prefix}/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
									<span class="_login">
										<xsl:text> (</xsl:text>
										<xsl:value-of select="@login"/>
										<xsl:text>)</xsl:text>
									</span>
								</div>
								<div class="_lines">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Registration date')"/>
										<xsl:with-param name="text">
											<xsl:call-template name="get_full_date">
												<xsl:with-param name="datetime" select="@add_time"/>
											</xsl:call-template>
										</xsl:with-param>
										<xsl:with-param name="class" select="'_line'"/>
									</xsl:call-template>
								</div>
							</td>
						</tr>
					</xsl:for-each>
				</table>
				<form action="{$save_prefix}/register/" method="post" enctype="multipart/form-data">
					<input type="hidden" name="ignore_similarity_check" value="1"/>
					<input type="hidden" name="submit_from_similarity" value="1"/>
					<xsl:for-each select="$pass_info/vars/var">
						<input type="hidden" name="{@name}" value="{.}"/>
					</xsl:for-each>
					<div class="field2">
						<br/>
						<button class="ctrl ctrl__button">
							<xsl:value-of select="php:function('trans', 'I am not in this list, I want to register!')"/>
						</button>
					</div>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<a href="{$login_url}">
						<xsl:value-of select="php:function('trans', 'Already registered?')"/>
					</a>
				</p>
				<form action="{$save_prefix}/register/" method="post" enctype="multipart/form-data">
					<xsl:if test="$get_vars[@name = 'retpath']">
						<input type="hidden" name="retpath" value="{$get_vars[@name = 'retpath']}"/>
					</xsl:if>
					<xsl:if test="$pass_info/vars/var[@name = 'ignore_similarity_check']">
						<input type="hidden" name="ignore_similarity_check" value="1"/>
					</xsl:if>
					<xsl:if test="$pass_info/info[@name = 'CHECK_AGAIN']">
						<div class="notification">
							<span>
								<xsl:value-of select="php:function('trans', 'Please check data once again and then register. If you chose a photo, please choose it once again.')"/>
							</span>
						</div>
					</xsl:if>
					<xsl:call-template name="draw_dt_edit">
						<xsl:with-param name="no_fieldset" select="true()"/>
						<xsl:with-param name="draw_button" select="false()"/>
						<xsl:with-param name="allow_error_message" select="not($pass_info/info[@name = 'CHECK_AGAIN'])"/>
					</xsl:call-template>
					<div class="field2">
						<button class="ctrl ctrl__button ctrl__button__big">
							<xsl:value-of select="php:function('trans', 'Register')"/>
						</button>
					</div>
				</form>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Registration')"/>
	</xsl:template>
</xsl:stylesheet>
