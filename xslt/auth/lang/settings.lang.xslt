<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="lang_settings_access">
		<xsl:choose>
			<xsl:when test="$lang = 'ru'">
				<p>Уровни доступа:</p>
				<ul>
					<li>
						<b>Все</b> — данные доступны всем.
					</li>
					<li>
						<b>Зарегистрированные пользователи</b> — данные доступны только зарегистрированным пользователям. Полезно, чтобы закрыть часть информации от поисковиков, кеширующих сайтов и случайных посетителей.
					</li>
					<li>
						<b>Сокурсники + Модераторы</b> — данные по конкретному стриму доступны студентам <b>этого</b> стрима, а также модераторам и администраторам любого стрима.
					</li>
					<li>
						<b>Модераторы</b> — данные доступны модераторам и администраторам любого стрима.
					</li>
				</ul>
				<p>Обратите внимание, что в открытых стримах можно стать студентом, просто зарегистрировавшись. Поэтому опция «Студенты стрима» не защищает доступ к вашим данным в открытых стримах.</p>
				<p>Независимо от настроек, ваши результаты (проценты соответствия позициям) в данный момент отображаются в рейтингах тех стримов, где вы имеете статус студента.</p>
				<p>Сам факт того, что вас оценили в событии, а также количество ваших оценок в данный момент всегда доступны всем.</p>
			</xsl:when>
			<xsl:otherwise>
				<p>Access levels:</p>
				<ul>
					<li>
						<b>Everybody</b> – everybody can access your data.
					</li>
					<li>
						<b>Registered users</b> – only registered users can access your data. Useful for protecting your data from search engines, caching websites and random visitors.
					</li>
					<li>
						<b>Classmates + Moderators</b> – stream's data is only accessible for students of <b>that</b> stream and also for moderators and administrators of any stream.
					</li>
					<li>
						<b>Moderators</b> – your data is accessible for moderators and administrators of any stream.
					</li>
				</ul>
				<p>Please note that everybody can become a student of an open stream just after a registration. Option “Stream's students” does not protect your data in open streams.</p>
				<p>Your results (position matches) are visible for everybody in ratings in streams where you are a student (regardless of the setting).</p>
				<p>The fact that you are graded in an event and a total number of your marks are now available for everybody.</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
