<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="lang/settings.lang.xslt"/>
	<xsl:variable name="top_section" select="'settings'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/settings/')"/>
	<xsl:template match="settings">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Personal settings')"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Personal settings')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/settings/" method="post" enctype="multipart/form-data">
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
	<xsl:template mode="dtf_fieldset_top" match="block[@name = 'access']">
		<p>
			<span data-toggle-for="block-access-text" class="js_toggle clickable">
				<i class="fa fa-question-circle"/>
				<xsl:text> </xsl:text>
				<span class="dashed">
					<xsl:value-of select="php:function('trans', 'Help')"/>
				</span>
			</span>
		</p>
		<div id="block-access-text" class="box p dn">
			<xsl:call-template name="lang_settings_access"/>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Personal settings')"/>
	</xsl:template>
</xsl:stylesheet>
