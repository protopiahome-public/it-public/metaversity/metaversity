<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_login">
		<xsl:param name="can_cancel" select="true()"/>
		<xsl:if test="$pass_info/error[@name = 'BAD_LOGIN_OR_PASSWORD']">
			<div class="error">
				<span>
					<xsl:value-of select="php:function('trans', 'Incorrect login or password. Please try again.')"/>
				</span>
			</div>
		</xsl:if>
		<div class="maxw300">
			<div id="field-{@name}" class="field2" jq-name="{@name}" jq-type="{@type}">
				<label for="f-{@name}">
					<xsl:value-of select="php:function('trans', 'Login')"/>
					<xsl:text>:</xsl:text>
				</label>
				<input class="ctrl ctrl__text" type="text" name="login" maxlength="40" value="{$pass_info/vars/var[@name = 'login']}"/>
			</div>
			<div id="field-{@name}" class="field2" jq-name="{@name}" jq-type="{@type}">
				<label for="f-{@name}">
					<xsl:value-of select="php:function('trans', 'Password')"/>
					<xsl:text>:</xsl:text>
				</label>
				<input class="ctrl ctrl__text" type="password" name="password" maxlength="255" value="{$pass_info/vars/var[@name = 'password']}"/>
			</div>
			<div class="field2">
				<button class="ctrl ctrl__button ctrl__button__big">
					<xsl:if test="$can_cancel or 1">
						<xsl:attribute name="class">ctrl ctrl__button ctrl__button__big ctrl__button__for_right</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="php:function('trans', 'Login', 'VERB')"/>
				</button>
				<xsl:if test="$can_cancel or 1">
					<button name="cancel" class="ctrl ctrl__button ctrl__button__big ctrl__button__cancel">
						<xsl:value-of select="php:function('trans', 'Cancel', 'VERB')"/>
					</button>
				</xsl:if>
			</div>
			<xsl:variable name="get_suffix">
				<xsl:if test="$get_vars[@name='retpath']">
					<xsl:value-of select="concat('?retpath=',$get_vars[@name='retpath']/@value_escaped)"/>
				</xsl:if>
			</xsl:variable>
		</div>
		<p style="margin-top:20px; margin-bottom: 20px;">
			<a href="{$reg_url}">
				<xsl:value-of select="php:function('trans', 'Register')"/>
			</a>
			<xsl:text> | </xsl:text>
			<a href="{$main_prefix}/pass/">
				<xsl:value-of select="php:function('trans', 'Forgot your password?')"/>
			</a>
		</p>
	</xsl:template>
</xsl:stylesheet>
