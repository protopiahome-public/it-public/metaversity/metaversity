<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_layout.xslt"/>
	<xsl:variable name="user_data" select="/root/user_pass_restore_mail"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:value-of select="php:function('trans', 'Password reset')"/>
					<xsl:text> (</xsl:text>
					<xsl:value-of select="$user_data/@login"/>
					<xsl:text>)</xsl:text>
				</title>
			</head>
			<body>
				<p>
					<xsl:value-of select="php:function('trans', 'Hello!')"/>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'It seems, you have requested a password reset for')"/>
					<xsl:text> </xsl:text>
					<a href="{$main_prefix}/">
						<xsl:value-of select="concat($main_prefix, '/')"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'Your login:')"/>
					<xsl:text> </xsl:text>
					<b>
						<xsl:value-of select="$user_data/@login"/>
					</b>
					<xsl:text>.</xsl:text>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'Please follow the link to set a new password:')"/>
				</p>
				<xsl:variable name="link">
					<xsl:value-of select="concat($main_prefix, '/pass/?login=', $user_data/@login, '&amp;key=', $user_data/@pass_recovery_key, $user_data/@retpath_suffix)"/>
				</xsl:variable>
				<p>
					<a href="{$link}">
						<xsl:value-of select="$link"/>
					</a>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', '-- The service team')"/>
				</p>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
