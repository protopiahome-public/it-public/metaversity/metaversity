<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_news">
		<xsl:param name="current_user_url" select="'-'"/>
		<xsl:if test="not(news_item)">
			<p>
				<xsl:value-of select="php:function('trans', 'No news were found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="news_item">
			<xsl:for-each select="news_item">
				<xsl:variable name="context" select="."/>
				<xsl:variable name="pos" select="position()"/>
				<div class="news_item clearfix">
					<div class="news_item_date">
						<span>
							<xsl:call-template name="get_time">
								<xsl:with-param name="datetime" select="@time"/>
							</xsl:call-template>
						</span>
					</div>
					<xsl:choose>
						<xsl:when test="@news_type = 'system_text'">
							<xsl:variable name="system_news_item" select="/root/system_news_item_full[@id = current()/@system_news_item_id][1]"/>
							<div class="news_item_body">
								<xsl:for-each select="/root/user_short[@id = current()/@adder_user_id][1]">
									<div class="news_item_photo">
										<a href="{@url}" class="inline_photo" title="{@visible_name}">
											<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.8)}" height="{round(photo_big/@height * 0.8)}"/>
										</a>
									</div>
								</xsl:for-each>
								<div class="news_item_title">
									<span class="smaller">
										<xsl:choose>
											<xsl:when test="$system_news_item/@is_for_moderators = 1">
												<xsl:value-of select="php:function('trans', 'Metaversity for moderators')"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="php:function('trans', 'Metaversity')"/>
											</xsl:otherwise>
										</xsl:choose>
									</span>
									<span class="arrow"> → </span>
									<xsl:choose>
										<xsl:when test="$user/@is_admin = 1">
											<span class="for_icon_right">
												<xsl:value-of select="$system_news_item/@title"/>
											</span>
											<a href="{$main_prefix}/admin/news/{$system_news_item/@id}/">
												<i class="red fa fa-gears"/>
											</a>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$system_news_item/@title"/>
										</xsl:otherwise>
									</xsl:choose>
								</div>
								<!--<xsl:for-each select="/root/user_short[@id = current()/@adder_user_id][1]">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key">
											<xsl:choose>
												<xsl:when test="@sex = 'f'">
													<xsl:value-of select="php:function('trans', 'Added by', 'FEMALE')"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="php:function('trans', 'Added by', 'MALE')"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:with-param>
										<xsl:with-param name="html">
											<a href="{@url}" class="inline_photo">
												<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
											</a>
											<xsl:text> </xsl:text>
											<a href="{@url}">
												<xsl:value-of select="@visible_name"/>
											</a>
										</xsl:with-param>
										<xsl:with-param name="class" select="'prop'"/>
									</xsl:call-template>
								</xsl:for-each>-->
								<xsl:call-template name="draw_intmarkup_with_files">
									<xsl:with-param name="field" select="$system_news_item/announce"/>
								</xsl:call-template>
								<xsl:if test="$system_news_item/descr/div/* or $system_news_item/descr/files/file">
									<p>
										<span class="js_toggle clickable" data-toggle-for="system-news-{$pos}">
											<span class="dashed">
												<xsl:value-of select="php:function('trans', 'Read more')"/>
											</span>
											<xsl:text> </xsl:text>
											<span class="details">...</span>
										</span>
									</p>
									<div id="system-news-{$pos}" class="js_toggle_freeze hide">
										<xsl:call-template name="draw_intmarkup_with_files">
											<xsl:with-param name="field" select="$system_news_item/descr"/>
										</xsl:call-template>
									</div>
								</xsl:if>
							</div>
						</xsl:when>
						<xsl:when test="@news_type = 'stream_text'">
							<xsl:variable name="stream" select="/root/stream_short[@id = current()/@stream_id][1]"/>
							<xsl:variable name="news_item" select="/root/stream_news_item_full[@id = current()/@news_item_id][1]"/>
							<div class="news_item_body">
								<xsl:for-each select="/root/stream_user_short[@id = current()/@adder_user_id][1]">
									<div class="news_item_photo">
										<a href="{@url}" class="inline_photo" title="{@visible_name}">
											<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.8)}" height="{round(photo_big/@height * 0.8)}"/>
										</a>
									</div>
								</xsl:for-each>
								<div class="news_item_title">
									<xsl:if test="not(../@stream_id)">
										<span class="smaller">
											<a href="{$stream/@url}">
												<xsl:value-of select="$stream/@title_short"/>
											</a>
										</span>
										<span class="arrow"> → </span>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="/root/stream_access[@stream_id = $stream/@id]/@has_moderator_rights = 1">
											<span class="for_icon_right">
												<xsl:value-of select="$news_item/@title"/>
											</span>
											<a href="{$stream/@url}admin/news/{$news_item/@id}/">
												<i class="red fa fa-gear"/>
											</a>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$news_item/@title"/>
										</xsl:otherwise>
									</xsl:choose>
								</div>
								<div class="props">
									<xsl:if test="$news_item/@city_title != ''">
										<div class="prop">
											<i class="fa fa-map-marker"/>
											<xsl:value-of select="$news_item/@city_title"/>
										</div>
									</xsl:if>
									<!--<xsl:for-each select="/root/stream_user_short[@id = current()/@adder_user_id][1]">
										<xsl:call-template name="draw_kv">
											<xsl:with-param name="key">
												<xsl:choose>
													<xsl:when test="@sex = 'f'">
														<xsl:value-of select="php:function('trans', 'Added by', 'FEMALE')"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="php:function('trans', 'Added by', 'MALE')"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:with-param>
											<xsl:with-param name="html">
												<a href="{@url}" class="inline_photo">
													<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
												</a>
												<xsl:text> </xsl:text>
												<a href="{@url}">
													<xsl:value-of select="@visible_name"/>
												</a>
											</xsl:with-param>
											<xsl:with-param name="class" select="'prop'"/>
										</xsl:call-template>
									</xsl:for-each>-->
								</div>
								<xsl:call-template name="draw_intmarkup_with_files">
									<xsl:with-param name="field" select="$news_item/announce"/>
								</xsl:call-template>
								<xsl:if test="$news_item/descr/div/* or $news_item/descr/files/file">
									<p>
										<span class="js_toggle clickable" data-toggle-for="news-{$pos}">
											<span class="dashed">
												<xsl:value-of select="php:function('trans', 'Read more')"/>
											</span>
											<xsl:text> </xsl:text>
											<span class="details">...</span>
										</span>
									</p>
									<div id="news-{$pos}" class="js_toggle_freeze hide">
										<xsl:call-template name="draw_intmarkup_with_files">
											<xsl:with-param name="field" select="$news_item/descr"/>
										</xsl:call-template>
									</div>
								</xsl:if>
							</div>
						</xsl:when>
						<xsl:when test="@news_type = 'activity_moderation' or @news_type = 'activity_status' or @news_type = 'mark'">
							<xsl:variable name="stream" select="/root/stream_short[@id = current()/@stream_id][1]"/>
							<xsl:variable name="activity" select="/root/activity_short[@id = current()/@activity_id][1]"/>
							<div class="news_item_body">
								<xsl:for-each select="/root/stream_user_short[@id = current()/@changer_user_id][1]">
									<div class="news_item_photo">
										<a href="{@url}" class="inline_photo" title="{@visible_name}">
											<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.8)}" height="{round(photo_big/@height * 0.8)}"/>
										</a>
									</div>
								</xsl:for-each>
								<xsl:if test="@news_type = 'activity_moderation'">
									<xsl:for-each select="/root/stream_user_short[@id = current()/@user_id][1]">
										<div class="news_item_photo">
											<a href="{@url}" class="inline_photo" title="{@visible_name}">
												<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.8)}" height="{round(photo_big/@height * 0.8)}"/>
											</a>
										</div>
									</xsl:for-each>
								</xsl:if>
								<div class="news_item_title">
									<xsl:if test="not(../@stream_id)">
										<span class="smaller">
											<a href="{$stream/@url}">
												<xsl:value-of select="$stream/@title_short"/>
											</a>
										</span>
										<span class="arrow"> → </span>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="@news_type = 'activity_moderation'">
											<span class="status_premoderation">
												<i class="fa fa-ellipsis-h"/>
												<xsl:text>&#160;</xsl:text>
												<xsl:value-of select="php:function('trans', 'New request')"/>
												<xsl:for-each select="/root/stream_user_short[@id = current()/@user_id][1]">
													<xsl:text> (</xsl:text>
													<a href="{@url}">
														<xsl:value-of select="@visible_name"/>
													</a>
													<xsl:text>)</xsl:text>
												</xsl:for-each>
											</span>
										</xsl:when>
										<xsl:when test="@news_type = 'activity_status'">
											<xsl:call-template name="draw_activity_participant_status">
												<xsl:with-param name="mode" select="'full'"/>
												<xsl:with-param name="no_default" select="false()"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:when test="@news_type = 'mark'">
											<xsl:value-of select="php:function('trans', 'New mark')"/>
											<xsl:text> </xsl:text>
											<xsl:call-template name="draw_mark">
												<xsl:with-param name="weight" select="$activity/@weight"/>
											</xsl:call-template>
										</xsl:when>
									</xsl:choose>
								</div>
								<div class="props">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Activity', 'WHERE')"/>
										<xsl:with-param name="html">
											<a href="{$stream/@url}activities/{$activity/@id}/" class="for_icon_right">
												<xsl:value-of select="$activity/@title"/>
											</a>
											<xsl:text> </xsl:text>
											<span class="gray nobr">
												<xsl:call-template name="draw_activity_date_short">
													<xsl:with-param name="context" select="$activity"/>
												</xsl:call-template>
											</span>
										</xsl:with-param>
										<xsl:with-param name="class" select="'prop'"/>
									</xsl:call-template>
									<xsl:if test="@competence_title != ''">
										<xsl:call-template name="draw_kv">
											<xsl:with-param name="key" select="php:function('trans', 'Competence')"/>
											<xsl:with-param name="text">
												<xsl:call-template name="draw_competence">
													<xsl:with-param name="id" select="@competence_id"/>
													<xsl:with-param name="title" select="@competence_title"/>
													<xsl:with-param name="dot" select="true()"/>
												</xsl:call-template>
											</xsl:with-param>
											<xsl:with-param name="class" select="'prop'"/>
										</xsl:call-template>
									</xsl:if>
									<xsl:for-each select="/root/role_short[@id = current()/@role_id][1]">
										<xsl:call-template name="draw_kv">
											<xsl:with-param name="key" select="php:function('trans', 'Role')"/>
											<xsl:with-param name="html">
												<xsl:choose>
													<xsl:when test="descr/div != ''">
														<xsl:value-of select="@title"/>
														<xsl:text> </xsl:text>
														<span class="js_toggle clickable" data-toggle-for="role-{$pos}">
															<span class="details">...</span>
														</span>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="@title"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:with-param>
											<xsl:with-param name="class" select="'prop'"/>
										</xsl:call-template>
									</xsl:for-each>
									<!--<xsl:for-each select="/root/stream_user_short[@id = current()/@changer_user_id][1]">
										<xsl:call-template name="draw_kv">
											<xsl:with-param name="key">
												<xsl:choose>
													<xsl:when test="$context/@news_type = 'mark'">
														<xsl:choose>
															<xsl:when test="@sex = 'f'">
																<xsl:value-of select="php:function('trans', 'Rated by', 'FEMALE')"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="php:function('trans', 'Rated by', 'MALE')"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:when>
													<xsl:otherwise>
														<xsl:choose>
															<xsl:when test="@sex = 'f'">
																<xsl:value-of select="php:function('trans', 'Status changed by', 'FEMALE')"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="php:function('trans', 'Status changed by', 'MALE')"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:with-param>
											<xsl:with-param name="html">
												<a href="{@url}" class="inline_photo">
													<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
												</a>
												<xsl:text> </xsl:text>
												<a href="{@url}">
													<xsl:value-of select="@visible_name"/>
												</a>
											</xsl:with-param>
											<xsl:with-param name="class" select="'prop'"/>
										</xsl:call-template>
									</xsl:for-each>-->
									<xsl:if test="@news_type = 'activity_moderation'">
										<!--<xsl:for-each select="/root/stream_user_short[@id = current()/@user_id][1]">
											<xsl:call-template name="draw_kv">
												<xsl:with-param name="key">
													<xsl:choose>
														<xsl:when test="@sex = 'f'">
															<xsl:value-of select="php:function('trans', 'Pretender', 'FEMALE')"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="php:function('trans', 'Pretender', 'MALE')"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:with-param>
												<xsl:with-param name="html">
													<a href="{@url}" class="inline_photo">
														<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
													</a>
													<xsl:text> </xsl:text>
													<a href="{@url}">
														<xsl:value-of select="@visible_name"/>
													</a>
												</xsl:with-param>
												<xsl:with-param name="class" select="'prop'"/>
											</xsl:call-template>
										</xsl:for-each>-->
										<xsl:if test="/root/stream_access[@stream_id = $stream/@id]/@has_moderator_rights = 1">
											<div class="prop">
												<span class="fa fa-gear"/>
												<xsl:text> </xsl:text>
												<a class="admin" href="{$stream/@url}admin/activities/{$activity/@id}/participants/">
													<xsl:value-of select="php:function('trans', 'Moderate')"/>
												</a>
											</div>
										</xsl:if>
									</xsl:if>
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Comment')"/>
										<xsl:with-param name="text" select="@comment"/>
										<xsl:with-param name="class" select="'prop'"/>
									</xsl:call-template>
									<xsl:if test="/root/role_short[@id = current()/@role_id]/descr/div != ''">
										<div id="role-{$pos}" class="hide">
											<xsl:copy-of select="/root/role_short[@id = current()/@role_id]/descr/div"/>
										</div>
									</xsl:if>
								</div>
							</div>
						</xsl:when>
						<xsl:when test="@news_type = 'credit'">
							<xsl:variable name="stream" select="/root/stream_short[@id = current()/@stream_id][1]"/>
							<xsl:variable name="position" select="/root/position_full[@id = current()/@position_id][1]"/>
							<div class="news_item_body">
								<xsl:for-each select="/root/user_short[@id = current()/@expert_user_id][1]">
									<div class="news_item_photo">
										<a href="{@url}" class="inline_photo" title="{@visible_name}">
											<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.8)}" height="{round(photo_big/@height * 0.8)}"/>
										</a>
									</div>
								</xsl:for-each>
								<div class="news_item_title">
									<xsl:if test="not(../@stream_id)">
										<span class="smaller">
											<a href="{$stream/@url}">
												<xsl:value-of select="$stream/@title_short"/>
											</a>
										</span>
										<span class="arrow"> → </span>
									</xsl:if>
									<span class="position_credit_expert">
										<i class="fa fa-certificate"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="php:function('trans', 'New credit from the expert')"/>
									</span>
								</div>
								<div class="props">
									<xsl:for-each select="/root/position_full[@id = current()/@position_id][1]">
										<xsl:call-template name="draw_kv">
											<xsl:with-param name="key" select="php:function('trans', 'Position')"/>
											<xsl:with-param name="html">
												<xsl:choose>
													<xsl:when test="descr/div != ''">
														<a href="{$current_user_url}results/{$stream/@name}/{$position/@id}/">
															<xsl:value-of select="$position/@title"/>
														</a>
														<xsl:text> </xsl:text>
														<span class="js_toggle clickable" data-toggle-for="position-{$pos}">
															<span class="details">...</span>
														</span>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="@title"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:with-param>
											<xsl:with-param name="class" select="'prop'"/>
										</xsl:call-template>
									</xsl:for-each>
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Comment')"/>
										<xsl:with-param name="text" select="@comment"/>
										<xsl:with-param name="class" select="'prop'"/>
									</xsl:call-template>
									<xsl:if test="/root/position_full[@id = current()/@position_id]/descr/div != ''">
										<div id="position-{$pos}" class="hide">
											<xsl:copy-of select="/root/position_full[@id = current()/@position_id]/descr/div"/>
										</div>
									</xsl:if>
								</div>
							</div>
						</xsl:when>
					</xsl:choose>
				</div>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
