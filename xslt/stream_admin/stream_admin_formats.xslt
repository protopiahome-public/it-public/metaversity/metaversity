<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'formats'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/formats/')"/>
	<xsl:template match="stream_admin_formats">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Formats')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Formats')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<span id="tree_button_add_group" class="link_btn link_btn__disabled link_btn__add_group link_btn__for_right">
				<i class="_icon _icon__1 fa fa-folder"/>
				<i class="_icon _icon__2 fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a group')"/>
				</span>
			</span>
			<span id="tree_button_add_item" class="link_btn link_btn__disabled link_btn__add link_btn__for_right">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a format')"/>
				</span>
			</span>
			<span id="tree_button_clone_item" class="link_btn link_btn__disabled link_btn__clone link_btn__for_right">
				<i class="_icon fa fa-clone"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Clone format')"/>
				</span>
			</span>
			<span id="tree_button_edit" class="link_btn link_btn__disabled link_btn__edit link_btn__for_right">
				<i class="_icon fa fa-pencil"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Edit')"/>
				</span>
			</span>
			<span id="tree_button_edit_roles" class="link_btn link_btn__disabled link_btn__edit link_btn__for_right">
				<i class="_icon fa fa-pencil"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Edit roles')"/>
				</span>
			</span>
			<span id="tree_button_delete" class="link_btn link_btn__disabled link_btn__delete">
				<i class="_icon fa fa-times"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Delete', 'VERB')"/>
				</span>
			</span>
		</div>
		<div id="format_tree" class="tree_wrap"/>
		<div class="box" style="margin-top: 20px;">
			<p>
				<ul>
					<li>
						<xsl:value-of select="php:function('trans', 'One can delete a format only if there are no activities of this format.')"/>
					</li>
					<li>
						<xsl:value-of select="php:function('trans', 'One can delete a group of formats only if there are no groups nor formats inside.')"/>
					</li>
					<li>
						<xsl:value-of select="php:function('trans', 'Move formats and groups into ARCHIVE to make them unavailable while creating a new activity.')"/>
					</li>
					<li>
						<xsl:value-of select="php:function('trans', 'One can not delete or rename ARCHIVE.')"/>
					</li>
				</ul>
			</p>
		</div>
		<div id="dialog-confirm-delete" data-title="{php:function('trans', 'Confirmation')}" data-button-title="{php:function('trans', 'Delete', 'VERB')}" class="hide">
			<p>
				<xsl:value-of select="php:function('trans', 'Please confirm that you want to delete this item.')"/>
			</p>
		</div>
		<div id="dialog-cannot-delete" data-title="{php:function('trans', 'Message')}" data-button-title="{php:function('trans', 'Close')}" class="hide">
			<p>
				<xsl:value-of select="php:function('trans', 'The format can not be deleted because there are activities of this format.')"/>
			</p>
		</div>
		<script type="text/javascript">
			var formats_ajax_ctrl = 'stream_admin_formats_tree';
			var formats_ajax_url = global.ajax_prefix + formats_ajax_ctrl + '/';
			var base_format_roles_url = '<xsl:value-of select="$current_stream/@url"/>admin/formats/';
			var stream_id = <xsl:value-of select="@stream_id"/>;
			var trans = {
				'Add a group': '<xsl:value-of select="php:function('trans', 'Add a group')"/>',
				'New group': '<xsl:value-of select="php:function('trans', 'New group')"/>',
				'Add a format': '<xsl:value-of select="php:function('trans', 'Add a format')"/>',
				'Clone format': '<xsl:value-of select="php:function('trans', 'Clone format')"/>',
				'New format': '<xsl:value-of select="php:function('trans', 'New format')"/>',
				'Edit': '<xsl:value-of select="php:function('trans', 'Edit')"/>',
				'Delete': '<xsl:value-of select="php:function('trans', 'Delete', 'VERB')"/>'
			};
		</script>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<link rel="stylesheet" href="{$main_prefix}/lib/jstree/themes/default/style.css?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.hotkeys.js?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/jstree.min.js?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.scrollTo.js?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_admin_formats_tree.js?v12"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Formats')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
