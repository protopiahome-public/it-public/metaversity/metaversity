<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_credits_common.inc.xslt"/>
	<xsl:include href="../stream/include/stream_users.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'credits'"/>
	<xsl:variable name="admin_section_main_page" select="/root/stream_users/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="credits_section" select="'users'"/>
	<xsl:variable name="credits_section_main_page" select="/root/stream_users/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/credits/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_users">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Position credits')"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Position credits')"/>
			</h1>
			<xsl:call-template name="draw_stream_admin_credits_submenu"/>
			<div class="widgets">
				<xsl:apply-templates select="filters" mode="filters">
					<xsl:with-param name="exclude_name" select="'search'"/>
				</xsl:apply-templates>
			</div>
			<xsl:apply-templates select="filters" mode="filter_search"/>
			<xsl:apply-templates select="filters" mode="filters_show">
				<xsl:with-param name="exclude_name" select="'search'"/>
			</xsl:apply-templates>
			<div class="maxw600">
				<xsl:call-template name="draw_stream_users">
					<xsl:with-param name="small" select="true()"/>
					<xsl:with-param name="draw_count" select="false()"/>
					<xsl:with-param name="base_url" select="concat($current_stream/@url, 'admin/credits/users/')"/>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Position credits')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
