<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="menu/stream_admin_activity.menu.xslt"/>
	<xsl:include href="include/stream_admin_activity_edit_longterm_marks_common.inc.xslt"/>
	<xsl:include href="../activities/include/activity_common.inc.xslt"/>
	<xsl:include href="../users/include/user_marks.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="admin_activity_section" select="'longterm-marks'"/>
	<xsl:variable name="admin_activity_section_main_page" select="false()"/>
	<xsl:variable name="longterm_marks_section" select="'user_competence'"/>
	<xsl:variable name="longterm_marks_section_main_page" select="/root/stream_admin_activity_edit_longterm_marks_user_competence/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="current_user" select="/root/stream_user_short[1]"/>
	<xsl:variable name="current_competence_id" select="/root/stream_admin_activity_edit_longterm_marks_user_competence[1]/@competence_id"/>
	<xsl:variable name="current_competence_title" select="/root/stream_admin_activity_edit_longterm_marks_user_competence[1]/@competence_title"/>
	<xsl:variable name="module_url" select="concat($current_activity_admin_url, $admin_activity_section, '/', $current_user/@login, '/', $current_competence_id, '/')"/>
	<xsl:template match="stream_admin_activity_edit_longterm_marks_user_competence">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$activities_admin_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span class="head_duplicate">
					<a href="{$module_url}">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</a>
					<xsl:text> / </xsl:text>
				</span>
				<a href="{$current_activity_admin_url}">
					<xsl:value-of select="$current_activity/@title"/>
				</a>
			</h1>
			<h2>
				<xsl:choose>
					<xsl:when test="$admin_activity_section_main_page">
						<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_activity_admin_url}longterm-marks/">
							<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h2>
			<div class="box p">
				<xsl:call-template name="draw_stream_admin_activity_edit_longterm_marks_grading_period"/>
			</div>
			<xsl:call-template name="draw_stream_admin_activity_edit_longterm_marks_user_header"/>
			<div class="prop p">
				<a class="underline_inner_span" href="{$users_prefix}/{$current_user/@login}/">
					<span>
						<xsl:value-of select="php:function('trans', 'User profile')"/>
					</span>
					<xsl:text> </xsl:text>
					<i class="fa fa-external-link"/>
				</a>
				<xsl:text> | </xsl:text>
				<a class="underline_inner_span" href="{$users_prefix}/{$current_user/@login}/competences/{$current_stream/@name}/{$current_competence_id}/">
					<span>
						<xsl:value-of select="php:function('trans', 'All marks for the competence')"/>
					</span>
					<xsl:text> </xsl:text>
					<i class="fa fa-external-link"/>
				</a>
			</div>
			<h3>
				<xsl:choose>
					<xsl:when test="$longterm_marks_section_main_page">
						<xsl:call-template name="draw_competence">
							<xsl:with-param name="id" select="$current_competence_id"/>
							<xsl:with-param name="title" select="$current_competence_title"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:call-template name="draw_competence">
								<xsl:with-param name="id" select="$current_competence_id"/>
								<xsl:with-param name="title" select="$current_competence_title"/>
							</xsl:call-template>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h3>
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
			<xsl:call-template name="draw_user_marks">
				<xsl:with-param name="marks" select="news_item"/>
				<xsl:with-param name="draw_status" select="false()"/>
				<xsl:with-param name="show_not_found_message" select="true()"/>
			</xsl:call-template>
			<xsl:apply-templates select="pages"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_activity_competence_mark/" method="post" class="p">
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<input type="hidden" name="activity_id" value="{$current_activity/@id}"/>
			<input type="hidden" name="user_id" value="{$current_user/@id}"/>
			<input type="hidden" name="competence_id" value="{$current_competence_id}"/>
			<xsl:choose>
				<xsl:when test="news_item[@type = 'competence']">
					<input type="hidden" name="is_deleted" value="1"/>
					<div class="field2 block_center center">
						<input type="hidden" name="mark" value="-"/>
						<input type="submit" class="ctrl ctrl__button ctrl__button__red" value="{php:function('trans', 'Delete mark')}"/>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<fieldset class="block_center">
						<legend>
							<xsl:value-of select="php:function('trans', 'Add mark “3” for this competence')"/>
						</legend>
						<div class="field2">
							<label>
								<xsl:value-of select="php:function('trans', 'Comment')"/>
								<xsl:text>:</xsl:text>
								<span class="star">
									<xsl:text>&#160;*</xsl:text>
								</span>
							</label>
							<textarea name="comment" class="ctrl ctrl__textarea" rows="3">
								<xsl:choose>
									<xsl:when test="$pass_info/vars/var">
										<xsl:value-of select="$pass_info/vars/var[@name = 'comment']"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="credit/@comment"/>
									</xsl:otherwise>
								</xsl:choose>
							</textarea>
							<xsl:if test="$pass_info/error[@name = 'EMPTY_COMMENT']">
								<div class="_error">
									<span>
										<xsl:value-of select="php:function('trans', 'Comment cannot be blank!')"/>
									</span>
								</div>
							</xsl:if>
							<!--<div class="_comment"/>-->
						</div>
						<div class="field2 center">
							<input type="hidden" name="mark" value="3"/>
							<input type="submit" class="ctrl ctrl__button" value="{php:function('trans', 'Add mark')}"/>
						</div>
					</fieldset>
				</xsl:otherwise>
			</xsl:choose>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_competence_id"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
