<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_site/include/admin_pretenders.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'pretenders'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars) and /root/stream_admin_pretenders/pages/@current_page = 1"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/pretenders/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_admin_pretenders">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Premoderation of students')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Premoderation of students')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Premoderation of students')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<div class="widgets">
				<xsl:apply-templates select="filters" mode="filters"/>
			</div>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_pretenders/" method="post">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<xsl:call-template name="draw_admin_pretenders"/>
		</form>
	</xsl:template>
	<xsl:template mode="draw_pretenders_aux_th" match="*">
		<xsl:call-template name="draw_th">
			<xsl:with-param name="title" select="php:function('trans', 'Group')"/>
			<xsl:with-param name="class" select="'show1000 p6'"/>
		</xsl:call-template>
		<th/>
	</xsl:template>
	<xsl:template mode="draw_pretenders_aux_lines" match="*">
		<xsl:param name="user_context"/>
		<xsl:call-template name="draw_kv">
			<xsl:with-param name="key" select="php:function('trans', 'Group')"/>
			<xsl:with-param name="text" select="$user_context/@group_title"/>
			<xsl:with-param name="class" select="'_line hide1000'"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="draw_pretenders_aux_td" match="*">
		<xsl:param name="user_context"/>
		<td class="cell_text show1000 maxw250 p6">
			<xsl:value-of select="$user_context/@group_title"/>
		</td>
		<td class="cell_btn">
			<a href="{$module_url}{@id}/" class="_btn" title="{php:function('trans', 'Edit')}">
				<i class="_btn_icon red fa fa-gear"/>
			</a>
		</td>
	</xsl:template>
	<xsl:template name="draw_pretenders_not_found">
		<xsl:if test="not(user)">
			<p>
				<xsl:value-of select="php:function('trans', 'Nobody is found.')"/>
			</p>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Premoderation of students')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
