<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'news'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars) and /root/stream_admin_news/pages/@current_page = 1"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/news/')"/>
	<xsl:template match="stream_admin_news">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'News')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'News')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'News')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<div class="head_dd_align_left p" style="padding-bottom: 6px;">
				<xsl:apply-templates select="filters" mode="filter_one">
					<xsl:with-param name="name" select="'city'"/>
					<xsl:with-param name="prefix_html">
						<div class="fa fa-map-marker"/>
						<xsl:text> </xsl:text>
					</xsl:with-param>
				</xsl:apply-templates>
			</div>
			<div class="maxw800">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$current_stream/@url}admin/news/add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a news item')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(news_item)">
			<p>
				<xsl:value-of select="php:function('trans', 'News were not found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="news_item">
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
						<xsl:with-param name="class" select="'p6'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="title" select="php:function('trans', 'Added')"/>
						<xsl:with-param name="class" select="'p6 show800'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="title" select="php:function('trans', 'City')"/>
						<xsl:with-param name="class" select="'p6 show800'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="news_item">
					<xsl:for-each select="/root/news_item_short[@id = current()/@id]">
						<tr>
							<td class="cell_text p6">
								<a href="{$current_stream/@url}admin/news/{@id}/" class="list_item">
									<xsl:value-of select="@title"/>
								</a>
								<div class="props">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Date')"/>
										<xsl:with-param name="text">
											<xsl:call-template name="get_half_full_datetime">
												<xsl:with-param name="datetime" select="@add_time"/>
												<xsl:with-param name="with_week_day" select="true()"/>
											</xsl:call-template>
										</xsl:with-param>
										<xsl:with-param name="class" select="'prop hide800'"/>
									</xsl:call-template>
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'City')"/>
										<xsl:with-param name="text">
											<xsl:choose>
												<xsl:when test="@city_id &gt; 0">
													<xsl:value-of select="@city_title"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="php:function('trans', 'Any city')"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:with-param>
										<xsl:with-param name="class" select="'prop hide800'"/>
									</xsl:call-template>
								</div>
							</td>
							<td class="cell_text cell_text__smaller p6 nobr show800">
								<xsl:call-template name="get_half_full_datetime">
									<xsl:with-param name="datetime" select="@add_time"/>
									<xsl:with-param name="with_week_day" select="true()"/>
								</xsl:call-template>
							</td>
							<td class="cell_text cell_text__smaller p6 show800 nobr">
								<xsl:choose>
									<xsl:when test="@city_id &gt; 0">
										<xsl:value-of select="@city_title"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="php:function('trans', 'Any city')"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="cell_btn">
								<a class="_btn" href="{$current_stream/@url}admin/news/{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
									<i class="_btn_icon red fa fa-times"/>
								</a>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'News')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
