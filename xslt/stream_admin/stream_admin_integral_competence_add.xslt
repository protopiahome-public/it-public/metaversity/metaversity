<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_integral_competence_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'integral-competences'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="integral_competence" select="/unexisted"/>
	<xsl:variable name="options" select="/root/stream_admin_integral_competence_add/options/option"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/integral-competences/')"/>
	<xsl:template match="stream_admin_integral_competence_add">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Integral competences')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Integral competences')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Adding an integral competence')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_integral_competence_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$module_url}%ID%/"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<div class="field2">
				<label for="f-competence_id">
					<xsl:value-of select="php:function('trans', 'Competence')"/>
					<xsl:text>:</xsl:text>
				</label>
				<select id="f-competence_id" class="js_ctrl_select_with_front ctrl ctrl__select" name="competence_id">
					<xsl:apply-templates mode="competence_select" select="/root/competence_set_competences"/>
				</select>
			</div>
			<p>
				<xsl:value-of select="php:function('trans', 'You will be able to setup included competences on the next step.')"/>
			</p>
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Save and continue')"/>
				<span class="bigger"> &#8594;</span>
			</button>
		</form>
	</xsl:template>
	<xsl:template mode="competence_select" match="group">
		<xsl:param name="tab" select="''"/>
		<optgroup label="{$tab}{@title}">
			<xsl:apply-templates mode="competence_select">
				<xsl:with-param name="tab" select="concat($tab, '&#160;&#160;')"/>
			</xsl:apply-templates>
		</optgroup>
	</xsl:template>
	<xsl:template mode="competence_select" match="competence">
		<xsl:param name="tab" select="''"/>
		<option value="{@id}">
			<xsl:if test="$pass_info/vars/var[@name = 'competence_id'] = @id">
				<xsl:attribute name="selected">selected</xsl:attribute>
			</xsl:if>
			<xsl:variable name="disabled" select="/root/stream_admin_integral_competences/competence[@id = current()/@id]"/>
			<xsl:if test="$disabled">
				<xsl:attribute name="disabled">disabled</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="$tab"/>
			<xsl:if test="$disabled">
				<xsl:text>[</xsl:text>
				<xsl:value-of select="php:function('trans', 'Already integral')"/>
				<xsl:text>] </xsl:text>
			</xsl:if>
			<xsl:call-template name="draw_competence_title"/>
		</option>
	</xsl:template>
	<xsl:template mode="competence_select" match="text()"/>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Adding an integral competence')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
