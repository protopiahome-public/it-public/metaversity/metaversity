<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="include/stream_admin_integral_competence_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'integral-competences'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="integral_competence" select="/root/integral_competence[1]"/>
	<xsl:variable name="competence_id" select="/root/stream_admin_integral_competence_edit/@competence_id"/>
	<xsl:variable name="title">
		<xsl:call-template name="draw_competence_title">
			<xsl:with-param name="competence" select="$integral_competence"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="options" select="/root/stream_admin_integral_competence_edit/options/option"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/integral-competences/')"/>
	<xsl:template match="stream_admin_integral_competence_edit">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Integral competences')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Integral competences')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="$title"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_integral_competence_edit/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<input type="hidden" name="competence_id" value="{$competence_id}"/>
			<xsl:call-template name="draw_integral_competence_edit_tree"/>
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Save', 'VERB')"/>
			</button>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Editing integral competence')"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
