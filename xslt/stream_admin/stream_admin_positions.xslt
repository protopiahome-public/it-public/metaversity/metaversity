<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_study_level.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'positions'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/positions/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_admin_positions">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Positions')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Positions')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Positions')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<div class="widgets">
				<xsl:apply-templates select="filters" mode="filters"/>
			</div>
			<div class="maxw800">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$module_url}add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a position')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(position)">
			<p>
				<xsl:value-of select="php:function('trans', 'Positions were not found.')"/>
			</p>
		</xsl:if>
		<xsl:call-template name="draw_form_info">
			<xsl:with-param name="added" select="php:function('trans', 'New position was added.')"/>
			<xsl:with-param name="edited" select="php:function('trans', 'The position was saved.')"/>
			<xsl:with-param name="deleted" select="php:function('trans', 'The position was deleted.')"/>
		</xsl:call-template>
		<xsl:if test="position">
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'competences'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Competences', 'COUNT')"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="title" select="php:function('trans', 'Study levels')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="position">
					<tr>
						<td class="cell_text">
							<a href="{$module_url}{@id}/">
								<xsl:value-of select="@title"/>
							</a>
							<xsl:if test="/root/stream_study_level_restrictions/object[@id = current()/@id]">
								<div class="props">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Study levels')"/>
										<xsl:with-param name="text">
											<xsl:call-template name="draw_stream_admin_study_level_short_value"/>
										</xsl:with-param>
										<xsl:with-param name="class" select="'prop hide600'"/>
									</xsl:call-template>
								</div>
							</xsl:if>
						</td>
						<td class="cell_num p6 center">
							<xsl:value-of select="@competence_count_calc"/>
						</td>
						<td class="cell_text p6 show600">
							<xsl:call-template name="draw_stream_admin_study_level_short_value"/>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$module_url}{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Positions')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
