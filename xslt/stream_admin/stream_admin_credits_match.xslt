<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_credits_common.inc.xslt"/>
	<xsl:include href="../positions/include/position_details.inc.xslt"/>
	<xsl:include href="../positions/include/position_credits.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'credits'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="credits_section" select="'match'"/>
	<xsl:variable name="credits_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="current_user" select="/root/stream_user_short[1]"/>
	<xsl:variable name="current_position" select="/root/position_full[1]"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/credits/users/', $current_user/@login, '/', $current_position/@id, '/')"/>
	<xsl:template match="math_position_details">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Position credits')"/>
			<xsl:with-param name="url">
				<xsl:value-of select="concat($current_stream/@url, 'admin/credits/')"/>
			</xsl:with-param>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate before_h1">
				<a href="{$current_stream/@url}admin/credits/">
					<xsl:value-of select="php:function('trans', 'Position credits')"/>
				</a>
			</h1>
			<xsl:call-template name="draw_stream_admin_credits_user_header"/>
			<h2 class="h2_main">
				<a href="{$current_stream/@url}admin/credits/positions/{$current_position/@id}/">
					<xsl:value-of select="$current_position/@title"/>
				</a>
			</h2>
			<div class="maxw600">
				<xsl:call-template name="draw_position_details_match"/>
				<form action="{$save_prefix}/stream_admin_credit/" method="post">
					<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
					<input type="hidden" name="position_id" value="{$current_position/@id}"/>
					<input type="hidden" name="user_id" value="{$current_user/@id}"/>
					<xsl:choose>
						<xsl:when test="credit/@expert_user_id != ''">
							<input type="hidden" name="is_deleted" value="1"/>
							<div class="field2 block_center center">
								<input type="submit" class="ctrl ctrl__button ctrl__button__red" value="{php:function('trans', 'Remove credit')}"/>
							</div>
						</xsl:when>
						<xsl:otherwise>
							<fieldset class="block_center">
								<legend>
									<xsl:value-of select="php:function('trans', 'Add credit')"/>
								</legend>
								<div class="field2">
									<label>
										<xsl:value-of select="php:function('trans', 'Comment')"/>
										<xsl:text>:</xsl:text>
										<span class="star">
											<xsl:text>&#160;*</xsl:text>
										</span>
									</label>
									<textarea name="comment" class="ctrl ctrl__textarea" rows="3">
										<xsl:choose>
											<xsl:when test="$pass_info/vars/var">
												<xsl:value-of select="$pass_info/vars/var[@name = 'comment']"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="credit/@comment"/>
											</xsl:otherwise>
										</xsl:choose>
									</textarea>
									<xsl:if test="$pass_info/error[@name = 'EMPTY_COMMENT']">
										<div class="_error">
											<span>
												<xsl:value-of select="php:function('trans', 'Comment cannot be blank!')"/>
											</span>
										</div>
									</xsl:if>
									<div class="_comment">
										<xsl:value-of select="php:function('trans', 'Which long-term activity of the student led to this credit?')"/>
									</div>
								</div>
								<div class="field2 center">
									<input type="submit" class="ctrl ctrl__button" value="{php:function('trans', 'Add credit')}"/>
								</div>
							</fieldset>
						</xsl:otherwise>
					</xsl:choose>
				</form>
				<xsl:call-template name="draw_position_details"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/rose.js?v5"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_user/@visible_name"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_position/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Position credits')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
