<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'news'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/news/')"/>
	<xsl:template match="stream_admin_news_item_add">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'News')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'News')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Adding a news item')"/>
			</h2>
			<div class="_block">
				<form action="{$save_prefix}/stream_admin_news_item_add/" method="post">
					<input type="hidden" name="retpath" value="{$module_url}"/>
					<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
					<xsl:call-template name="draw_dt_edit"/>
				</form>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/jquery-ui.js?v1"/>
		<script type="text/javascript">
			var FileAPI = {
				debug: global.debug,
				media: true,
				staticPath: global.main_prefix + 'js/FileAPI/'
			};
		</script>
		<script src="{$main_prefix}/js/FileAPI/FileAPI.min.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.fileapi.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Adding a news item')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
