<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'study_materials'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/materials/')"/>
	<xsl:template match="stream_admin_study_materials">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Materials')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Materials')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Materials')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add link_btn__for_right" href="{$module_url}add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a material')"/>
				</span>
			</a>
			<a class="link_btn link_btn__add" href="{$module_url}add/outer/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add an external material')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(study_material)">
			<p>
				<xsl:value-of select="php:function('trans', 'Materials were not found.')"/>
			</p>
		</xsl:if>
		<xsl:call-template name="draw_form_info">
			<xsl:with-param name="added" select="php:function('trans', 'New material was added.')"/>
			<xsl:with-param name="edited" select="php:function('trans', 'The material was saved.')"/>
			<xsl:with-param name="deleted" select="php:function('trans', 'The material was deleted.')"/>
		</xsl:call-template>
		<xsl:if test="study_material">
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
						<xsl:with-param name="class" select="'p6'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'add_time'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Added')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'competences'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Competences', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="study_material">
					<tr>
						<td class="cell_text p6">
							<a href="{$module_url}{@id}/">
								<xsl:value-of select="@title"/>
							</a>
							<xsl:if test="@is_outer = 1">
								<xsl:text> (</xsl:text>
								<a href="{@outer_url}">
									<i>
										<xsl:value-of select="php:function('trans', 'external')"/>
									</i>
								</a>
								<xsl:text>)</xsl:text>
							</xsl:if>
							<div class="props">
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Added')"/>
									<xsl:with-param name="text">
										<xsl:call-template name="get_half_full_datetime">
											<xsl:with-param name="datetime" select="@add_time"/>
											<xsl:with-param name="with_week_day" select="true()"/>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Competences')"/>
									<xsl:with-param name="text" select="@competence_count_calc"/>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="cell_text cell_text__smaller p6 nobr show600">
							<xsl:call-template name="get_half_full_datetime">
								<xsl:with-param name="datetime" select="@add_time"/>
								<xsl:with-param name="with_week_day" select="true()"/>
							</xsl:call-template>
						</td>
						<td class="cell_num center p6 show600">
							<xsl:value-of select="@competence_count_calc"/>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$module_url}{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Materials')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
