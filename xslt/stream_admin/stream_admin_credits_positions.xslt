<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_credits_common.inc.xslt"/>
	<xsl:include href="../stream/include/stream_ratings_positions.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'credits'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="credits_section" select="'positions'"/>
	<xsl:variable name="credits_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/credits/positions/')"/>
	<xsl:template match="stream_positions">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Position credits')"/>
			<xsl:with-param name="url">
				<xsl:value-of select="concat($current_stream/@url, 'admin/credits/')"/>
			</xsl:with-param>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Position credits')"/>
			</h1>
			<xsl:call-template name="draw_stream_admin_credits_submenu"/>
			<div class="maxw600">
				<xsl:call-template name="draw_stream_ratings_positions">
					<xsl:with-param name="draw_focus" select="false()"/>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/stream_positions.js?v6"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Positions')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Position credits')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
