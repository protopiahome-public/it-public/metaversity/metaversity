<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="menu/stream_admin_activity.menu.xslt"/>
	<xsl:include href="include/stream_admin_activity_edit_longterm_marks_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="admin_activity_section" select="'longterm-marks'"/>
	<xsl:variable name="admin_activity_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="longterm_marks_section" select="'users'"/>
	<xsl:variable name="longterm_marks_section_main_page" select="/root/stream_admin_activity_edit_longterm_marks_users/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_activity_admin_url, $admin_activity_section, '/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_admin_activity_edit_longterm_marks_users">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$activities_admin_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span class="head_duplicate">
					<a href="{$module_url}">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</a>
					<xsl:text> / </xsl:text>
				</span>
				<a href="{$current_activity_admin_url}">
					<xsl:value-of select="$current_activity/@title"/>
				</a>
			</h1>
			<div class="widgets">
				<xsl:apply-templates select="filters" mode="filters"/>
			</div>
			<h2>
				<xsl:choose>
					<xsl:when test="$admin_activity_section_main_page">
						<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_activity_admin_url}longterm-marks/">
							<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h2>
			<div class="maxw600">
				<xsl:choose>
					<xsl:when test="@dates_ok = 1">
						<div class="box p">
							<xsl:call-template name="draw_stream_admin_activity_edit_longterm_marks_grading_period"/>
							<p>
								<xsl:value-of select="php:function('trans', 'Right column shows how many competences have the required total weight of important marks “2” and “3” for roles in the selected interval. Total weight is a sum of weights of important marks.')"/>
							</p>
							<p>
								<xsl:value-of select="php:function('trans', 'Required total weight can be changed in filters.')"/>
							</p>
							<p>
								<xsl:value-of select="php:function('trans', 'Number in parentheses (if exists) shows how many marks were already added.')"/>
							</p>
						</div>
						<xsl:call-template name="_draw"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="draw_stream_admin_activity_edit_longterm_marks_dates_warning"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:apply-templates select="filters" mode="filters_show">
			<xsl:with-param name="exclude_name" select="'weight'"/>
		</xsl:apply-templates>
		<xsl:if test="not(user)">
			<p>
				<xsl:value-of select="php:function('trans', 'Nobody matches the selected search criteria.')"/>
			</p>
		</xsl:if>
		<xsl:if test="user">
			<table class="table w100p maxw600">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Name')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Competences (Marks)', 'COUNT')"/>
						<xsl:with-param name="class" select="'p10 right'"/>
					</xsl:call-template>
				</tr>
			</table>
			<table class="table w100p maxw600">
				<xsl:for-each select="user">
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="/root/stream_user_short[@id = current()/@id]">
						<tr>
							<td class="cell_user">
								<div class="_img">
									<a href="{$module_url}{@login}/">
										<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
									</a>
								</div>
								<div class="_title">
									<a href="{$module_url}{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
									<span class="_login">
										<xsl:text> (</xsl:text>
										<xsl:value-of select="@login"/>
										<xsl:text>)</xsl:text>
									</span>
								</div>
								<div class="_city">
									<xsl:value-of select="@city_title"/>
									<xsl:if test="@group_title != ''">
										<xsl:text>, </xsl:text>
										<xsl:value-of select="@group_title"/>
									</xsl:if>
								</div>
							</td>
							<td class="cell_num p10 nobr center">
								<xsl:value-of select="$context/@competence_count"/>
								<xsl:if test="$context/@competence_mark_count &gt; 0">
									<b>
										<xsl:text> (</xsl:text>
										<xsl:value-of select="$context/@competence_mark_count"/>
										<xsl:text>)</xsl:text>
									</b>
								</xsl:if>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
