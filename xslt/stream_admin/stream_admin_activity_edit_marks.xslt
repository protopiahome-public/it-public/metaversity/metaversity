<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="menu/stream_admin_activity.menu.xslt"/>
	<xsl:include href="include/stream_admin_activity_edit_participants_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="admin_activity_section" select="'marks'"/>
	<xsl:variable name="admin_activity_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/activities/')"/>
	<xsl:template match="stream_admin_activity_edit_marks">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span class="head_duplicate">
					<a href="{$module_url}">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</a>
					<xsl:text> / </xsl:text>
				</span>
				<a href="{$current_activity_admin_url}">
					<xsl:value-of select="$current_activity/@title"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Marks')"/>
			</h2>
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="box p">
			<h3>
				<xsl:value-of select="php:function('trans', 'Meaning of marks')"/>
			</h3>
			<xsl:call-template name="draw_marks_legend_digits_meanings"/>
		</div>
		<xsl:call-template name="draw_stream_admin_activity_edit_participants_roles">
			<xsl:with-param name="roles" select="$current_activity/roles/role[@enabled = 1]"/>
			<xsl:with-param name="users" select="/root/activity_participants/user[@status = 'accepted' or @mark]"/>
			<xsl:with-param name="show_marks" select="true()"/>
		</xsl:call-template>
		<xsl:call-template name="draw_stream_admin_activity_edit_participants_users"/>
		<div class="js_just_added_select select select__no_auto_init box_shadow w100 hide">
			<div class="_list">
				<div class="_item" data-value="-">
					<i class="_icon fa"/>
					<xsl:value-of select="php:function('trans', 'DASH')"/>
				</div>
				<div class="_item" data-value="0">
					<i class="_icon fa"/>
					<xsl:text>0</xsl:text>
				</div>
				<div class="_item" data-value="1">
					<i class="_icon fa"/>
					<xsl:text>1</xsl:text>
				</div>
				<div class="_item" data-value="2">
					<i class="_icon fa"/>
					<xsl:text>2</xsl:text>
				</div>
				<div class="_item" data-value="3">
					<i class="_icon fa"/>
					<xsl:text>3</xsl:text>
				</div>
			</div>
		</div>
		<div class="js_mark_comment_edit_blank _line hide">
			<div class="red bold p" style="padding-top: 10px;">
				<xsl:value-of select="php:function('trans', 'The data has not been saved yet!')"/>
				<span class="js_for_mark_3 hide">
					<br/>
					<xsl:value-of select="php:function('trans', 'Comment must be filled in for the mark 3.')"/>
				</span>
			</div>
			<div class="p">
				<textarea class="js_mark_comment_edit_text ctrl ctrl__textarea w100p" rows="3" cols="20" placeholder="{php:function('trans', 'Comment')}"/>
			</div>
			<div class="p">
				<input type="submit" class="js_mark_comment_edit_save ctrl ctrl__button ctrl__for_right" value="{php:function('trans', 'Save', 'VERB')}"/>
				<input type="submit" class="js_mark_comment_edit_cancel ctrl ctrl__button ctrl__button__cancel" name="cancel" value="{php:function('trans', 'Cancel', 'VERB')}"/>
			</div>
		</div>
		<div class="js_mark_comment_edit_error hide">
			<p>
				<xsl:value-of select="php:function('trans', 'Comment must be filled in for the mark 3.')"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template mode="user_ctrl" match="/root">
		<xsl:param name="user_info"/>
		<div class="js_mark_select select box_shadow w100" data-user_id="{$user_info/@id}" data-role_id="{$user_info/@role_id}">
			<div class="_list">
				<div class="_item" data-value="-">
					<i class="_icon fa"/>
					<xsl:value-of select="php:function('trans', 'DASH')"/>
				</div>
				<div class="_item" data-value="0">
					<xsl:if test="$user_info/@mark = '0'">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa"/>
					<xsl:text>0</xsl:text>
				</div>
				<div class="_item" data-value="1">
					<xsl:if test="$user_info/@mark = '1'">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa"/>
					<xsl:text>1</xsl:text>
				</div>
				<div class="_item" data-value="2">
					<xsl:if test="$user_info/@mark = '2'">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa"/>
					<xsl:text>2</xsl:text>
				</div>
				<div class="_item" data-value="3">
					<xsl:if test="$user_info/@mark = '3'">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa"/>
					<xsl:text>3</xsl:text>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			window.post_vars = {
				stream_id: <xsl:value-of select="$current_stream/@id"/>,
				activity_id: <xsl:value-of select="$current_activity/@id"/>
			}
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.xselect.js?v5"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_admin_edit_participants.js?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_admin_edit_marks.js?v3"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Marks')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
