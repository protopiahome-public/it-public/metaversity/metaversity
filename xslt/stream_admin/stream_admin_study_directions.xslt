<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'study-directions'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/study-directions/')"/>
	<xsl:template match="stream_admin_study_directions">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Study directions')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Study directions')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Study directions')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$module_url}add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a direction')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(study_direction)">
			<p>
				<xsl:value-of select="php:function('trans', 'Nothing was found.')"/>
			</p>
		</xsl:if>
		<xsl:call-template name="draw_form_info">
			<xsl:with-param name="added" select="php:function('trans', 'New study direction was added.')"/>
			<xsl:with-param name="edited" select="php:function('trans', 'The study direction was saved.')"/>
			<xsl:with-param name="deleted" select="php:function('trans', 'The study direction was deleted.')"/>
		</xsl:call-template>
		<xsl:if test="study_direction">
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
						<xsl:with-param name="class" select="'p6'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'groups'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Groups', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="study_direction">
					<tr>
						<td class="cell_text p6">
							<a href="{$module_url}{@id}/">
								<xsl:value-of select="@title"/>
							</a>
						</td>
						<td class="cell_num p6 center">
							<xsl:value-of select="@group_count_calc"/>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$module_url}{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Study directions')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
