<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_stream_admin_credits_submenu">
		<p>
			<xsl:value-of select="php:function('trans', 'Please start with either selecting a user or a position.')"/>
		</p>
		<div class="submenu p">
			<span class="_item">
				<xsl:if test="$credits_section = 'users'">
					<xsl:attribute name="class">_item _item__current</xsl:attribute>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$credits_section = 'users' and $credits_section_main_page">
						<xsl:value-of select="php:function('trans', 'Select a user', 'ADMIN_CREDITS_SUBMENU')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_stream/@url}admin/credits/">
							<xsl:value-of select="php:function('trans', 'Select a user', 'ADMIN_CREDITS_SUBMENU')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<xsl:text> </xsl:text>
			<span class="_item">
				<xsl:if test="$credits_section = 'positions'">
					<xsl:attribute name="class">_item _item__current</xsl:attribute>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$credits_section = 'positions' and $credits_section_main_page">
						<xsl:value-of select="php:function('trans', 'Select a position', 'ADMIN_CREDITS_SUBMENU')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_stream/@url}admin/credits/positions/">
							<xsl:value-of select="php:function('trans', 'Select a position', 'ADMIN_CREDITS_SUBMENU')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</span>
		</div>
	</xsl:template>
	<xsl:template name="draw_stream_admin_credits_user_header">
		<h1 class="after_h1">
			<span class="for_icon_right">
				<xsl:choose>
					<xsl:when test="$credits_section = 'user' and $credits_section_main_page">
						<xsl:value-of select="$current_user/@visible_name"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_stream/@url}admin/credits/users/{$current_user/@login}/">
							<xsl:value-of select="$current_user/@visible_name"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<span class="user_head_city">
				<span class="_icon fa fa-map-marker"/>
				<span class="_title">
					<xsl:value-of select="$current_user/@city_title"/>
				</span>
			</span>
		</h1>
		<div class="prop p">
			<a class="underline_inner_span" href="{$users_prefix}/{$current_user/@login}/">
				<span>
					<xsl:value-of select="php:function('trans', 'User profile')"/>
				</span>
				<xsl:text> </xsl:text>
				<i class="fa fa-external-link"/>
			</a>
		</div>
	</xsl:template>
</xsl:stylesheet>
