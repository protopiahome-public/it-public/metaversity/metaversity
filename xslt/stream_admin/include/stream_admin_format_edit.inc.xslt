<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="format_id" select="/root/format_short/@id"/>
	<xsl:variable name="format_title" select="/root/format_short/@title"/>
	<xsl:variable name="formats_url" select="concat($current_stream/@url, 'admin/formats/')"/>
	<xsl:variable name="format_url" select="concat($formats_url, $format_id, '/')"/>
</xsl:stylesheet>
