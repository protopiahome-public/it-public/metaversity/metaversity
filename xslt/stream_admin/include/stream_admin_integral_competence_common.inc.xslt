<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_integral_competence_edit_tree">
		<fieldset class="fieldset_text maxw800">
			<legend class="toggle">
				<xsl:value-of select="php:function('trans', 'Included competences')"/>
			</legend>
			<xsl:call-template name="draw_tree_head"/>
			<xsl:call-template name="draw_tree_competence_group">
				<xsl:with-param name="competence_is_clickable" select="false()"/>
				<xsl:with-param name="use_competence_title_template" select="true()"/>
			</xsl:call-template>
		</fieldset>
	</xsl:template>
	<xsl:template mode="tree_competence_title" match="competence">
		<span>
			<xsl:for-each select="/root/stream_admin_integral_competence_edit/including_competences/competence[@id = current()/@id]">
				<xsl:attribute name="class">note</xsl:attribute>
				<xsl:text>[</xsl:text>
				<xsl:value-of select="php:function('trans', 'Circle', 'COMPETENCE_TREE_EDIT')"/>
				<xsl:text>: </xsl:text>
				<xsl:value-of select="@id"/>
				<xsl:text>→</xsl:text>
				<xsl:value-of select="@path"/>
				<xsl:text>] </xsl:text>
			</xsl:for-each>
			<xsl:call-template name="draw_competence_title"/>
		</span>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="right show600">
			<xsl:value-of select="php:function('trans', 'Importance')"/>
		</th>
	</xsl:template>
	<xsl:template mode="tree_competence_td_top" match="competence">
		<xsl:attribute name="class">cell_competence cell_competence__2col</xsl:attribute>
	</xsl:template>
	<xsl:template mode="tree_competence_td" match="competence">
		<div class="_cell_right">
			<select name="marks[{current()/@id}]">
				<xsl:for-each select="/root/stream_admin_integral_competence_edit/including_competences/competence[@id = current()/@id]">
					<xsl:attribute name="disabled">disabled</xsl:attribute>
				</xsl:for-each>
				<xsl:variable name="current_competence" select="current()"/>
				<xsl:for-each select="$options">
					<option value="{@mark}">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = 'marks']/item[@index = $current_competence/@id] = @mark">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:when>
							<xsl:when test="$pass_info/vars/var[@name = 'marks']/item[@index = $current_competence/@id]"/>
							<xsl:when test="$integral_competence/competence[@id = $current_competence/@id]/@mark = current()/@mark">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:when>
						</xsl:choose>
						<xsl:value-of select="@mark_title"/>
					</option>
				</xsl:for-each>
			</select>
		</div>
	</xsl:template>
</xsl:stylesheet>
