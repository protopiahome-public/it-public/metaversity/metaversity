<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template mode="user_ctrl" match="text()"/>
	<xsl:template name="draw_stream_admin_activity_edit_participants_roles">
		<xsl:param name="roles"/>
		<xsl:param name="users"/>
		<xsl:param name="show_marks" select="false()"/>
		<xsl:for-each select="$roles">
			<xsl:variable name="role" select="."/>
			<div class="js_role" data-id="{@id}">
				<h3>
					<div class="js_search_btn float_right ctrl ctrl__button ctrl__button__white box_shadow clickable" style="margin-bottom: 10px;">
						<i class="fa fa-search"/>
					</div>
					<xsl:value-of select="@title"/>
				</h3>
				<div class="js_search p hide">
					<div class="p">
						<input class="js_search_text ctrl ctrl__text w100p" type="text" placeholder="{php:function('trans', 'Add user: search by city, name, login, group')}"/>
					</div>
					<div class="js_search_results_too_many p hide">
						<xsl:value-of select="php:function('trans', 'Too many search results (100+).')"/>
					</div>
					<div class="js_search_results_nothing_found p hide">
						<xsl:value-of select="php:function('trans', 'Nobody was found.')"/>
					</div>
					<div class="center p">
						<input class="js_search_close_btn ctrl ctrl__button ctrl__button__cancel" type="submit" value="{php:function('trans', 'Close search')}"/>
					</div>
					<table class="js_search_results table table__no_th maxw600 w100p p"/>
				</div>
				<div>
					<xsl:attribute name="class">
						<xsl:text>js_existing_users_nothing_found p</xsl:text>
						<xsl:if test="$users[@role_id = current()/@id][1]"> hide</xsl:if>
					</xsl:attribute>
					<xsl:value-of select="php:function('trans', 'Nobody was found.')"/>
				</div>
				<table class="js_existing_users table table__no_th maxw600 w100p p">
					<xsl:for-each select="$users[@role_id = current()/@id]">
						<xsl:variable name="user_info" select="."/>
						<xsl:for-each select="/root/stream_user_short[@id = current()/@id][1]">
							<tr class="js_user" data-id="{@id}">
								<td class="cell_user w100p">
									<div class="cell_float_ctrl pl6">
										<xsl:apply-templates mode="user_ctrl" select="/root">
											<xsl:with-param name="user_info" select="$user_info"/>
										</xsl:apply-templates>
									</div>
									<div class="_img">
										<a href="{$users_prefix}/{@login}/">
											<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
										</a>
									</div>
									<div class="_title">
										<a href="{$users_prefix}/{@login}/">
											<xsl:value-of select="@visible_name"/>
										</a>
										<span class="_login">
											<xsl:text> (</xsl:text>
											<xsl:value-of select="@login"/>
											<xsl:text>)</xsl:text>
										</span>
									</div>
									<div class="_city">
										<xsl:value-of select="@city_title"/>
										<xsl:if test="@group_title != ''">
											<xsl:text>, </xsl:text>
											<xsl:value-of select="@group_title"/>
										</xsl:if>
									</div>
									<div class="_lines">
										<div class="js_search_plus _line hide">
											<xsl:value-of select="concat('#', @id, '|', @login)"/>
										</div>
										<xsl:if test="$show_marks">
											<xsl:for-each select="/root/stream_user_short[@id = $user_info/@changer_user_id][1]">
												<xsl:call-template name="draw_kv">
													<xsl:with-param name="key">
														<xsl:choose>
															<xsl:when test="@id = $user/@id">
																<xsl:value-of select="php:function('trans', 'Rated by', 'YOU')"/>
															</xsl:when>
															<xsl:when test="@sex = 'f'">
																<xsl:value-of select="php:function('trans', 'Rated by', 'FEMALE')"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="php:function('trans', 'Rated by', 'MALE')"/>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:with-param>
													<xsl:with-param name="html">
														<!--<a href="{@url}" class="inline_photo">
															<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
														</a>
														<xsl:text> </xsl:text>-->
														<a href="{@url}">
															<xsl:choose>
																<xsl:when test="@id = $user/@id">
																	<xsl:value-of select="php:function('trans', 'You', 'RATED_BY')"/>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:value-of select="@visible_name"/>
																</xsl:otherwise>
															</xsl:choose>
														</a>
													</xsl:with-param>
													<xsl:with-param name="class" select="'js_changer _line'"/>
												</xsl:call-template>
											</xsl:for-each>
											<xsl:call-template name="draw_kv">
												<xsl:with-param name="key">
													<xsl:value-of select="php:function('trans', 'Rated by', 'YOU')"/>
												</xsl:with-param>
												<xsl:with-param name="html">
													<a href="{$users_prefix}/{$user/@login}/">
														<xsl:value-of select="php:function('trans', 'You', 'RATED_BY')"/>
													</a>
												</xsl:with-param>
												<xsl:with-param name="class" select="'js_changer_new _line hide'"/>
											</xsl:call-template>
											<div class="js_comment _line">
												<xsl:choose>
													<xsl:when test="$user_info/@comment != ''">
														<xsl:value-of select="$user_info/@comment"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:attribute name="class">js_comment _line</xsl:attribute>
													</xsl:otherwise>
												</xsl:choose>
											</div>
										</xsl:if>
									</div>
								</td>
								<!--<td class="cell_ctrl pl6">
									<xsl:apply-templates mode="user_ctrl" select="/root">
										<xsl:with-param name="user_info" select="$user_info"/>
									</xsl:apply-templates>
								</td>-->
							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_stream_admin_activity_edit_participants_users">
		<table class="js_users table table__no_th maxw600 w100p hide">
			<xsl:for-each select="/root/stream_admin_users/user">
				<xsl:for-each select="/root/stream_user_short[@id = current()/@id]">
					<tr class="js_user" data-id="{@id}">
						<td class="cell_user w100p">
							<div class="_img">
								<a href="{$users_prefix}/{@login}/">
									<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
								</a>
							</div>
							<div class="_title">
								<a class="js_user_name" href="{$users_prefix}/{@login}/">
									<xsl:value-of select="@visible_name"/>
								</a>
								<span class="_login">
									<xsl:text> (</xsl:text>
									<xsl:value-of select="@login"/>
									<xsl:text>)</xsl:text>
								</span>
							</div>
							<div class="js_user_city _city">
								<xsl:value-of select="@city_title"/>
								<xsl:if test="@group_title != ''">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="@group_title"/>
								</xsl:if>
							</div>
							<div class="_lines">
								<div class="js_search_plus _line hide">
									<xsl:value-of select="concat('#', @id, '|', @login)"/>
								</div>
								<div class="js_comment _line hide"/>
							</div>
						</td>
						<td class="cell_ctrl pl6">
							<span class="js_add_btn ctrl ctrl__button ctrl__button__white clickable box_shadow">
								<i class="_icon fa fa-plus green"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="php:function('trans', 'Add')"/>
							</span>
						</td>
					</tr>
				</xsl:for-each>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
