<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template mode="user_ctrl" match="text()"/>
	<xsl:template name="draw_stream_admin_activity_edit_longterm_marks_grading_period">
		<p>
			<b>
				<xsl:value-of select="php:function('trans', 'Grading date interval')"/>
				<xsl:text>: </xsl:text>
			</b>
			<xsl:call-template name="get_full_datetime">
				<xsl:with-param name="datetime" select="@start_time"/>
			</xsl:call-template>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:call-template name="get_full_datetime">
				<xsl:with-param name="datetime" select="@finish_time"/>
			</xsl:call-template>
			<xsl:text>.</xsl:text>
		</p>
	</xsl:template>
	<xsl:template name="draw_stream_admin_activity_edit_longterm_marks_dates_warning">
		<div class="box p">
			<p>
				<xsl:value-of select="php:function('trans', 'Grading date inverval is not set. Please set start time and finish time for this activity.')"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template name="draw_stream_admin_activity_edit_longterm_marks_user_header">
		<h1 class="after_h1">
			<span class="for_icon_right">
				<xsl:choose>
					<xsl:when test="$longterm_marks_section = 'user' and $longterm_marks_section_main_page">
						<xsl:value-of select="$current_user/@visible_name"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_activity_admin_url}longterm-marks/{$current_user/@login}/">
							<xsl:value-of select="$current_user/@visible_name"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<span class="user_head_city">
				<span class="_icon fa fa-map-marker"/>
				<span class="_title">
					<xsl:value-of select="$current_user/@city_title"/>
				</span>
			</span>
		</h1>
	</xsl:template>
</xsl:stylesheet>
