<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:variable name="current_activity" select="/root/activity_full[1]"/>
	<xsl:variable name="current_activity_url" select="concat($current_stream/@url, 'activities/', $current_activity/@id, '/')"/>
	<xsl:variable name="activities_admin_url" select="concat($current_stream/@url, 'admin/activities/')"/>
	<xsl:variable name="current_activity_admin_url" select="concat($activities_admin_url, $current_activity/@id, '/')"/>
	<xsl:template mode="menu_left_stream_admin_items" match="/root">
		<xsl:call-template name="_draw_item">
			<xsl:with-param name="section" select="''"/>
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url_right" select="'activities/'"/>
			<xsl:with-param name="icon" select="'long-arrow-left'"/>
			<xsl:with-param name="icon_modifier" select="'_icon__16'"/>
		</xsl:call-template>
		<div class="_space"/>
		<xsl:call-template name="_draw_activity_menu_header">
			<xsl:with-param name="title" select="php:function('trans', 'Edit activity', 'ACTIVITY_EDIT_MENU')"/>
		</xsl:call-template>
		<xsl:call-template name="_draw_activity_submenu_item">
			<xsl:with-param name="section" select="'edit'"/>
			<xsl:with-param name="title" select="php:function('trans', 'General', 'ACTIVITY_EDIT_MENU')"/>
			<xsl:with-param name="url_right" select="''"/>
		</xsl:call-template>
		<xsl:call-template name="_draw_activity_submenu_item">
			<xsl:with-param name="section" select="'moderators'"/>
			<xsl:with-param name="title" select="php:function('trans', 'Text for moderators', 'ACTIVITY_EDIT_MENU')"/>
		</xsl:call-template>
		<xsl:choose>
			<xsl:when test="$current_activity/@date_type != 'longterm'">
				<xsl:call-template name="_draw_activity_submenu_item">
					<xsl:with-param name="section" select="'roles'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Roles')"/>
				</xsl:call-template>
				<xsl:call-template name="_draw_activity_submenu_item">
					<xsl:with-param name="section" select="'participants-subscriptions'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Subscriptions')"/>
					<xsl:with-param name="count_html">
						<xsl:call-template name="_draw_participants_subscription_count"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="_draw_activity_submenu_item">
					<xsl:with-param name="section" select="'participants'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Participants')"/>
					<xsl:with-param name="count_html">
						<xsl:call-template name="_draw_participant_counts"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="_draw_activity_submenu_item">
					<xsl:with-param name="section" select="'marks'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Marks')"/>
					<xsl:with-param name="count_html">
						<xsl:call-template name="_draw_mark_count"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="_draw_activity_submenu_item">
					<xsl:with-param name="section" select="'longterm-marks'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Longterm marks', 'ACTIVITY_EDIT_MENU')"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:call-template name="_draw_activity_submenu_item">
			<xsl:with-param name="section" select="'delete'"/>
			<xsl:with-param name="title" select="php:function('trans', 'Delete', 'VERB')"/>
		</xsl:call-template>
		<a class="menu_item menu_item__sub" href="{$current_activity_url}">
			<span class="_icon fa fa-angle-right"/>
			<xsl:text> </xsl:text>
			<span class="_text">
				<xsl:value-of select="php:function('trans', 'Preview')"/>
				<xsl:text> </xsl:text>
				<i class="fa fa-external-link"/>
			</span>
		</a>
		<div class="_space"/>
	</xsl:template>
	<xsl:template name="_draw_participants_subscription_count">
		<xsl:param name="data_node" select="/root/stream_admin_activity_participants_subscriptions_count[1]"/>
		<span id="submenu_participants_subscription_count">
			<xsl:attribute name="class">
				<xsl:text>js_submenu_participants_subscription_count</xsl:text>
				<xsl:if test="not($data_node/@count &gt; 0)"> hide</xsl:if>
			</xsl:attribute>
			<xsl:text> </xsl:text>
			<span class="_count">
				<sup class="js_number">
					<xsl:value-of select="$data_node/@count"/>
				</sup>
			</span>
		</span>
	</xsl:template>
	<xsl:template name="_draw_participant_counts">
		<xsl:param name="data_node" select="/root/stream_admin_activity_participants_raw_stat[1]"/>
		<span id="submenu_participants_accepted_count">
			<xsl:attribute name="class">
				<xsl:text>js_submenu_participants_count</xsl:text>
				<xsl:if test="not($data_node/@accepted_count &gt; 0)"> hide</xsl:if>
			</xsl:attribute>
			<xsl:text> </xsl:text>
			<span class="_count">
				<sup class="js_number">
					<xsl:value-of select="$data_node/@accepted_count"/>
				</sup>
			</span>
		</span>
		<span id="submenu_participants_premoderation_count">
			<xsl:attribute name="class">
				<xsl:text>js_submenu_participants_count</xsl:text>
				<xsl:if test="not($data_node/@premoderation_count &gt; 0)"> hide</xsl:if>
			</xsl:attribute>
			<span class="_count_hl">
				<xsl:text>+</xsl:text>
				<span class="js_number">
					<xsl:value-of select="$data_node/@premoderation_count"/>
				</span>
			</span>
		</span>
	</xsl:template>
	<xsl:template name="_draw_mark_count">
		<xsl:param name="data_node" select="/root/stream_admin_activity_marks_stat[1]"/>
		<span id="submenu_mark_count">
			<xsl:attribute name="class">
				<xsl:text>js_submenu_mark_count</xsl:text>
				<xsl:if test="not($data_node/@mark_count &gt; 0)"> hide</xsl:if>
			</xsl:attribute>
			<xsl:text> </xsl:text>
			<span class="_count">
				<sup class="js_number">
					<xsl:value-of select="$data_node/@mark_count"/>
				</sup>
			</span>
		</span>
		<span id="submenu_unset_mark_count">
			<xsl:attribute name="class">
				<xsl:text>js_submenu_unset_mark_count</xsl:text>
				<xsl:if test="not($data_node/@unset_mark_count &gt; 0)"> hide</xsl:if>
			</xsl:attribute>
			<span class="_count_hl">
				<xsl:text>+</xsl:text>
				<span class="js_number">
					<xsl:value-of select="$data_node/@unset_mark_count"/>
				</span>
			</span>
		</span>
	</xsl:template>
	<xsl:template name="_draw_activity_menu_header">
		<xsl:param name="title"/>
		<xsl:param name="icon"/>
		<xsl:param name="icon_modifier"/>
		<xsl:variable name="html">
			<xsl:choose>
				<xsl:when test="$icon">
					<span class="_icon {$icon_modifier} fa fa-{$icon}"/>
					<xsl:text> </xsl:text>
				</xsl:when>
			</xsl:choose>
			<span class="_text">
				<xsl:value-of select="$title"/>
			</span>
		</xsl:variable>
		<span>
			<xsl:attribute name="class">
				<xsl:text>menu_item</xsl:text>
			</xsl:attribute>
			<xsl:copy-of select="$html"/>
		</span>
	</xsl:template>
	<xsl:template name="_draw_activity_submenu_item">
		<xsl:param name="section"/>
		<xsl:param name="title"/>
		<xsl:param name="count_html"/>
		<xsl:param name="icon"/>
		<xsl:param name="icon_modifier"/>
		<xsl:param name="url_right" select="concat($section, '/')"/>
		<xsl:variable name="html">
			<span class="_icon fa fa-angle-right"/>
			<xsl:text> </xsl:text>
			<span class="_text">
				<xsl:value-of select="$title"/>
			</span>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$admin_activity_section = $section and $admin_activity_section_main_page">
				<span>
					<xsl:attribute name="class">
						<xsl:text>menu_item menu_item__sub menu_item__current</xsl:text>
					</xsl:attribute>
					<xsl:copy-of select="$html"/>
					<xsl:copy-of select="$count_html"/>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$current_activity_admin_url}{$url_right}">
					<xsl:attribute name="class">
						<xsl:text>menu_item menu_item__sub</xsl:text>
						<xsl:if test="$admin_activity_section = $section"> menu_item__current</xsl:if>
					</xsl:attribute>
					<xsl:copy-of select="$html"/>
					<xsl:copy-of select="$count_html"/>
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
