<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:variable name="current_stream" select="/root/stream_current[1]"/>
	<xsl:variable name="current_stream_access" select="/root/stream_access[1]"/>
	<xsl:template mode="body_class" match="/root">with_menu_left with_menu_top</xsl:template>
	<xsl:template mode="menu_left" match="/root">
		<div class="menu_left">
			<div class="_submenu">
				<div class="js_btn_menu_left_open_main menu_item menu_item__clickable">
					<span class="_icon fa fa-home"/>
					<xsl:text> </xsl:text>
					<span class="_text _text__dashed">
						<xsl:value-of select="php:function('trans', 'Main menu')"/>
					</span>
					<xsl:text> </xsl:text>
					<span class="_down fa fa-caret-down"/>
				</div>
				<div class="js_menu_left_main _submenu_contents hide">
					<xsl:apply-templates mode="menu_items_main" select="/root"/>
				</div>
			</div>
			<div class="_submenu">
				<xsl:choose>
					<xsl:when test="$user/@id">
						<xsl:apply-templates mode="menu_items_user_title" select="/root">
							<xsl:with-param name="clickable" select="true()"/>
						</xsl:apply-templates>
						<div class="js_menu_left_user _submenu_contents hide">
							<xsl:apply-templates mode="menu_items_lang" select="/root"/>
							<xsl:apply-templates mode="menu_items_user" select="/root"/>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates mode="menu_items_lang" select="/root"/>
						<xsl:apply-templates mode="menu_items_guest" select="/root"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div class="_sep"/>
			<div class="_title">
				<xsl:choose>
					<xsl:when test="$stream_section = 'news' and $stream_section_main_page">
						<xsl:value-of select="$current_stream/@title"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_stream/@url}">
							<xsl:value-of select="$current_stream/@title"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div class="_undertitle">
				<i class="fa fa-gear"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'Administration')"/>
			</div>
			<xsl:apply-templates mode="menu_left_stream_admin_items" select="/root"/>
		</div>
	</xsl:template>
	<xsl:template mode="menu_left_stream_admin_items" match="/root">
		<xsl:choose>
			<xsl:when test="$current_stream_access/@has_admin_rights = 1">
				<xsl:call-template name="_draw_item">
					<xsl:with-param name="section" select="'general'"/>
					<xsl:with-param name="url_right" select="''"/>
					<xsl:with-param name="title" select="php:function('trans', 'General', 'Administration')"/>
					<xsl:with-param name="icon" select="'gear'"/>
				</xsl:call-template>
				<xsl:call-template name="_draw_item">
					<xsl:with-param name="section" select="'news'"/>
					<xsl:with-param name="title" select="php:function('trans', 'News')"/>
					<xsl:with-param name="icon" select="'bell-o'"/>
				</xsl:call-template>
				<div class="_group">
					<xsl:call-template name="_draw_group_item">
						<xsl:with-param name="title" select="php:function('trans', 'Competences')"/>
						<xsl:with-param name="icon" select="'list'"/>
						<xsl:with-param name="icon_modifier" select="'_icon__14'"/>
					</xsl:call-template>
					<div>
						<xsl:attribute name="class">
							<xsl:text>_group_contents</xsl:text>
							<xsl:if test="$admin_section != 'tree' and $admin_section != 'integral-competences' and $admin_section != 'translators' and $admin_section != 'tree-stat'"> hide</xsl:if>
						</xsl:attribute>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'tree'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Competence tree')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'integral-competences'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Integral', 'MENU')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'translators'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Translators')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'tree-stat'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Stats')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
					</div>
				</div>
				<div class="_group">
					<xsl:call-template name="_draw_group_item">
						<xsl:with-param name="title" select="php:function('trans', 'Positions')"/>
						<xsl:with-param name="icon" select="'crosshairs'"/>
					</xsl:call-template>
					<div>
						<xsl:attribute name="class">
							<xsl:text>_group_contents</xsl:text>
							<xsl:if test="$admin_section != 'positions' and $admin_section != 'credits'"> hide</xsl:if>
						</xsl:attribute>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'positions'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Positions')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'credits'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Position credits')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
					</div>
				</div>
				<div class="_group">
					<xsl:call-template name="_draw_group_item">
						<xsl:with-param name="title" select="php:function('trans', 'Schedule')"/>
						<xsl:with-param name="icon" select="'calendar-o'"/>
						<xsl:with-param name="icon_modifier" select="'_icon__16'"/>
					</xsl:call-template>
					<div>
						<xsl:attribute name="class">
							<xsl:text>_group_contents</xsl:text>
							<xsl:if test="$admin_section != 'formats' and $admin_section != 'metaactivities' and $admin_section != 'activities'"> hide</xsl:if>
						</xsl:attribute>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'activities'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'metaactivities'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Activity groups')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'formats'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Formats &amp; Roles')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
					</div>
				</div>
				<xsl:call-template name="_draw_item">
					<xsl:with-param name="section" select="'study_materials'"/>
					<xsl:with-param name="url_right" select="'materials'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Materials')"/>
					<xsl:with-param name="icon" select="'book'"/>
				</xsl:call-template>
				<xsl:call-template name="_draw_item">
					<xsl:with-param name="section" select="'subjects'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Subjects')"/>
					<xsl:with-param name="icon" select="'circle-o'"/>
				</xsl:call-template>
				<div class="_group">
					<xsl:call-template name="_draw_group_item">
						<xsl:with-param name="title" select="php:function('trans', 'Levels &amp; Groups')"/>
						<xsl:with-param name="icon" select="'sitemap'"/>
						<xsl:with-param name="icon_modifier" select="'_icon__14'"/>
					</xsl:call-template>
					<div>
						<xsl:attribute name="class">
							<xsl:text>_group_contents</xsl:text>
							<xsl:if test="$admin_section != 'study-levels' and $admin_section != 'groups'"> hide</xsl:if>
						</xsl:attribute>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'study-levels'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Study levels')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'groups'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Groups')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
					</div>
				</div>
				<div class="_group">
					<xsl:call-template name="_draw_group_item">
						<xsl:with-param name="title" select="php:function('trans', 'People')"/>
						<xsl:with-param name="icon" select="'users'"/>
					</xsl:call-template>
					<div>
						<xsl:attribute name="class">
							<xsl:text>_group_contents</xsl:text>
							<xsl:if test="$admin_section != 'members' and $admin_section != 'pretenders' and $admin_section != 'moderators' and $admin_section != 'admins'"> hide</xsl:if>
						</xsl:attribute>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'members'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Students')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:if test="$current_stream/@join_premoderation = 1">
							<xsl:call-template name="_draw_item">
								<xsl:with-param name="section" select="'pretenders'"/>
								<xsl:with-param name="title" select="php:function('trans', 'Premoderation')"/>
								<xsl:with-param name="sub" select="true()"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'moderators'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Moderators')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'admins'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Administrators')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:if test="$current_stream/@users_import_allowed = 1">
							<xsl:call-template name="_draw_item">
								<xsl:with-param name="section" select="'import-users'"/>
								<xsl:with-param name="title" select="php:function('trans', 'Import users', 'MENU')"/>
								<xsl:with-param name="sub" select="true()"/>
							</xsl:call-template>
						</xsl:if>
					</div>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="_draw_item">
					<xsl:with-param name="section" select="'news'"/>
					<xsl:with-param name="title" select="php:function('trans', 'News')"/>
					<xsl:with-param name="icon" select="'bell-o'"/>
				</xsl:call-template>
				<div class="_group">
					<xsl:call-template name="_draw_group_item">
						<xsl:with-param name="title" select="php:function('trans', 'Schedule')"/>
						<xsl:with-param name="icon" select="'calendar-o'"/>
						<xsl:with-param name="icon_modifier" select="'_icon__16'"/>
					</xsl:call-template>
					<div>
						<xsl:attribute name="class">
							<xsl:text>_group_contents</xsl:text>
							<xsl:if test="$admin_section != 'formats' and $admin_section != 'metaactivities' and $admin_section != 'activities'"> hide</xsl:if>
						</xsl:attribute>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'activities'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'metaactivities'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Activity groups')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
						<xsl:call-template name="_draw_item">
							<xsl:with-param name="section" select="'formats'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Formats &amp; Roles')"/>
							<xsl:with-param name="sub" select="true()"/>
						</xsl:call-template>
					</div>
				</div>
				<xsl:call-template name="_draw_item">
					<xsl:with-param name="section" select="'study_materials'"/>
					<xsl:with-param name="url_right" select="'materials'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Materials')"/>
					<xsl:with-param name="icon" select="'book'"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<div class="_space"/>
		<xsl:call-template name="_draw_item">
			<xsl:with-param name="url_right" select="'../'"/>
			<xsl:with-param name="title" select="php:function('trans', 'Back to front')"/>
			<xsl:with-param name="icon" select="'long-arrow-left'"/>
			<xsl:with-param name="icon_modifier" select="'_icon__16'"/>
			<xsl:with-param name="small" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="_draw_item">
		<xsl:param name="section"/>
		<xsl:param name="title"/>
		<xsl:param name="icon"/>
		<xsl:param name="icon_modifier"/>
		<xsl:param name="url_right" select="concat($section, '/')"/>
		<xsl:param name="sub" select="false()"/>
		<xsl:param name="small" select="false()"/>
		<xsl:variable name="html">
			<xsl:choose>
				<xsl:when test="$icon">
					<span class="_icon {$icon_modifier} fa fa-{$icon}"/>
					<xsl:text> </xsl:text>
				</xsl:when>
				<xsl:when test="$sub">
					<span class="_icon fa fa-angle-right"/>
					<xsl:text> </xsl:text>
				</xsl:when>
			</xsl:choose>
			<span class="_text">
				<xsl:value-of select="$title"/>
			</span>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$admin_section = $section and $admin_section_main_page">
				<span>
					<xsl:attribute name="class">
						<xsl:text>menu_item menu_item__current</xsl:text>
						<xsl:if test="$sub"> menu_item__sub</xsl:if>
						<xsl:if test="$small"> menu_item__small</xsl:if>
					</xsl:attribute>
					<xsl:copy-of select="$html"/>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$current_stream/@url}admin/{$url_right}">
					<xsl:attribute name="class">
						<xsl:text>menu_item</xsl:text>
						<xsl:if test="$admin_section = $section"> menu_item__current</xsl:if>
						<xsl:if test="$sub"> menu_item__sub</xsl:if>
						<xsl:if test="$small"> menu_item__small</xsl:if>
					</xsl:attribute>
					<xsl:copy-of select="$html"/>
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_group_item">
		<xsl:param name="sub" select="false()"/>
		<xsl:param name="section"/>
		<xsl:param name="title"/>
		<xsl:param name="icon"/>
		<xsl:param name="icon_modifier"/>
		<xsl:param name="url_right" select="$section"/>
		<div class="menu_item menu_item__clickable">
			<xsl:if test="$icon">
				<span class="_icon {$icon_modifier} fa fa-{$icon}"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<span class="_text _text__dashed">
				<xsl:value-of select="$title"/>
			</span>
			<xsl:text> </xsl:text>
			<span class="_down fa fa-caret-down"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
