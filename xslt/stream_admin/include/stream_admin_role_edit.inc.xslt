<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="role_id" select="/root/role_short/@id"/>
	<xsl:variable name="role_title" select="/root/role_short/@title"/>
	<xsl:variable name="roles_url" select="concat($format_url, 'roles/')"/>
	<xsl:variable name="role_url" select="concat($roles_url, $role_id, '/')"/>
</xsl:stylesheet>
