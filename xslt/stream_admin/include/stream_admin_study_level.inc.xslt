<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_stream_admin_study_level_short_value">
		<xsl:choose>
			<xsl:when test="/root/stream_study_level_restrictions/object[@id = current()/@id]">
				<xsl:for-each select="/root/stream_study_level_restrictions/object[@id = current()/@id]">
					<xsl:for-each select="study_level">
						<xsl:value-of select="@title"/>
						<xsl:if test="position() != last()">, </xsl:if>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>&#8211;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
