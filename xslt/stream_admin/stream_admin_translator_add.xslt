<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'translators'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="translator" select="/unexisted"/>
	<xsl:variable name="options" select="/root/stream_admin_translator_add/options/option"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/translators/')"/>
	<xsl:template match="stream_admin_translator_add">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Translators')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Translators')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Adding a translator')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_translator_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$module_url}%ID%/"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<div class="field field2">
				<label for="f-eq_competence_set_id">
					<xsl:value-of select="php:function('trans', 'Competence set FROM which we translate (the title is usually the same as the stream title)')"/>
					<xsl:text>:</xsl:text>
				</label>
				<div class="input-">
					<select id="f-eq_competence_set_id" name="eq_competence_set_id" class="wide-">
						<xsl:variable name="current_competence_set_id" select="@competence_set_id"/>
						<xsl:for-each select="/root/competence_sets/competence_set[@id != $current_competence_set_id]">
							<option value="{@id}">
								<xsl:if test="$pass_info/vars/var[@name = 'competence_set_id'] = @id">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:variable name="disabled" select="/root/stream_admin_translators/translator[@eq_competence_set_id = current()/@id]"/>
								<xsl:if test="$disabled">
									<xsl:attribute name="disabled">disabled</xsl:attribute>
								</xsl:if>
								<xsl:if test="$disabled">
									<xsl:text>[</xsl:text>
									<xsl:value-of select="php:function('trans', 'Already added')"/>
									<xsl:text>] </xsl:text>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
			</div>
			<p>
				<xsl:value-of select="php:function('trans', 'You will be able to setup translation rules on the next step.')"/>
			</p>
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Save and continue')"/>
				<span class="bigger"> &#8594;</span>
			</button>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Adding a translator')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
