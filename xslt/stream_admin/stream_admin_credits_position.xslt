<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../stream/include/stream_rating.inc.xslt"/>
	<xsl:include href="../positions/include/position_credits.inc.xslt"/>
	<xsl:include href="include/stream_admin_credits_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'credits'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="credits_section" select="'position'"/>
	<xsl:variable name="credits_section_main_page" select="/root/math_rating/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="current_position" select="/root/position_full[1]"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/credits/positions/', $current_position/@id, '/')"/>
	<xsl:template match="math_rating">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Position credits')"/>
			<xsl:with-param name="url">
				<xsl:value-of select="concat($current_stream/@url, 'admin/credits/')"/>
			</xsl:with-param>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate before_h1">
				<a href="{$current_stream/@url}admin/credits/">
					<xsl:value-of select="php:function('trans', 'Position credits')"/>
				</a>
			</h1>
			<h2 class="h2_main">
				<xsl:value-of select="$current_position/@title"/>
			</h2>
			<div class="maxw600">
				<xsl:call-template name="draw_stream_rating">
					<xsl:with-param name="base_url" select="concat($current_stream/@url, 'admin/credits/users/')"/>
					<xsl:with-param name="url_suffix" select="concat($current_position/@id, '/')"/>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<xsl:if test="/root/math_rating/@cache_miss = 1">
			<script type="text/javascript">
				var wait_for_cache_ajax_page = "math_rating_wait_for_cache";
				var post_vars = {stream_id: <xsl:value-of select="$current_stream/@id"/>};
			</script>
			<script type="text/javascript" src="{$main_prefix}/js/wait_for_cache.js?v1"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Positions')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Position credits')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
