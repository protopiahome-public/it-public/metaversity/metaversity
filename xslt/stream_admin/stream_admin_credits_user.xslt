<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_credits_common.inc.xslt"/>
	<xsl:include href="../users/include/user_position_results_positions.inc.xslt"/>
	<xsl:include href="../positions/include/position_credits.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'credits'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="credits_section" select="'user'"/>
	<xsl:variable name="credits_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="current_user" select="/root/stream_user_short[1]"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/credits/users/', $current_user/@login, '/')"/>
	<xsl:template match="math_user_position_results">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Position credits')"/>
			<xsl:with-param name="url">
				<xsl:value-of select="concat($current_stream/@url, 'admin/credits/')"/>
			</xsl:with-param>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate before_h1">
				<a href="{$current_stream/@url}admin/credits/">
					<xsl:value-of select="php:function('trans', 'Position credits')"/>
				</a>
			</h1>
			<xsl:call-template name="draw_stream_admin_credits_user_header"/>
			<div class="maxw600">
				<xsl:call-template name="draw_user_position_results_positions"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_user/@visible_name"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Position credits')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
