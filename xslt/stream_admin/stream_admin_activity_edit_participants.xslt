<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="menu/stream_admin_activity.menu.xslt"/>
	<xsl:include href="include/stream_admin_activity_edit_participants_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="admin_activity_section" select="'participants'"/>
	<xsl:variable name="admin_activity_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/activities/')"/>
	<xsl:template match="stream_admin_activity_edit_participants">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span class="head_duplicate">
					<a href="{$module_url}">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</a>
					<xsl:text> / </xsl:text>
				</span>
				<a href="{$current_activity_admin_url}">
					<xsl:value-of select="$current_activity/@title"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Participants')"/>
			</h2>
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:call-template name="draw_stream_admin_activity_edit_participants_roles">
			<xsl:with-param name="roles" select="$current_activity/roles/role[@enabled = 1 and @auto_accept = 0]"/>
			<xsl:with-param name="users" select="/root/activity_participants/user"/>
		</xsl:call-template>
		<xsl:if test="$current_activity/roles/role[@enabled = 1 and @auto_accept = 1]">
			<div class="box p">
				<p>
					<xsl:value-of select="php:function('trans', 'All following roles have automatic request approvals switched on (i. e. no-moderation mode).')"/>
				</p>
			</div>
		</xsl:if>
		<xsl:call-template name="draw_stream_admin_activity_edit_participants_roles">
			<xsl:with-param name="roles" select="$current_activity/roles/role[@enabled = 1 and @auto_accept = 1]"/>
			<xsl:with-param name="users" select="/root/activity_participants/user"/>
		</xsl:call-template>
		<xsl:call-template name="draw_stream_admin_activity_edit_participants_users"/>
		<div class="js_just_added_select js_status_select select select__no_auto_init box_shadow hide" data-change-ajax-ctrl="stream_admin_activity_participant_status" data-value-name="status">
			<div class="_list">
				<div class="_item" data-value="premoderation">
					<i class="_icon fa fa-ellipsis-h status_premoderation"/>
					<xsl:value-of select="php:function('trans', 'Moderation', 'ACTIVITY_STATUS')"/>
				</div>
				<div class="_item _item__selected" data-value="accepted">
					<i class="_icon fa fa-check status_accepted"/>
					<xsl:value-of select="php:function('trans', 'Approved', 'ACTIVITY_STATUS')"/>
				</div>
				<div class="_item" data-value="declined">
					<i class="_icon fa fa-times status_declined"/>
					<xsl:value-of select="php:function('trans', 'Rejected', 'ACTIVITY_STATUS')"/>
				</div>
				<div class="_item" data-value="deleted">
					<i class="_icon fa"/>
					<xsl:value-of select="php:function('trans', 'Delete', 'VERB')"/>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="user_ctrl" match="/root">
		<xsl:param name="user_info"/>
		<div class="js_status_select js_status_select_init select select__no_auto_init box_shadow" data-change-ajax-ctrl="stream_admin_activity_participant_status" data-value-name="status" data-user_id="{$user_info/@id}" data-role_id="{$user_info/@role_id}">
			<div class="_list">
				<div class="_item" data-value="premoderation">
					<xsl:if test="$user_info/@status = 'premoderation'">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa fa-ellipsis-h status_premoderation"/>
					<xsl:value-of select="php:function('trans', 'Moderation', 'ACTIVITY_STATUS')"/>
				</div>
				<div class="_item" data-value="accepted">
					<xsl:if test="$user_info/@status = 'accepted'">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa fa-check status_accepted"/>
					<xsl:value-of select="php:function('trans', 'Approved', 'ACTIVITY_STATUS')"/>
				</div>
				<div class="_item" data-value="declined">
					<xsl:if test="$user_info/@status = 'declined'">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa fa-times status_declined"/>
					<xsl:value-of select="php:function('trans', 'Rejected', 'ACTIVITY_STATUS')"/>
				</div>
				<div class="_item" data-value="deleted">
					<xsl:if test="not($user_info/@status)">
						<xsl:attribute name="class">_item _item__selected</xsl:attribute>
					</xsl:if>
					<i class="_icon fa"/>
					<xsl:value-of select="php:function('trans', 'Delete', 'VERB')"/>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			window.post_vars = {
				stream_id: <xsl:value-of select="$current_stream/@id"/>,
				activity_id: <xsl:value-of select="$current_activity/@id"/>
			}
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.xselect.js?v5"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_admin_edit_participants.js?v2"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Participants')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
