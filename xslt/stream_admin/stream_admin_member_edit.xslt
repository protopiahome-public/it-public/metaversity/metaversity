<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="page_type" select="/root/stream_admin_member_edit/@page_type"/>
	<xsl:variable name="admin_section" select="$page_type"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="pk" select="/root/stream_admin_member_edit/primary_key/field"/>
	<xsl:variable name="edited_user" select="/root/user_short[@id = $pk[@name = 'user_id']]"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/', $page_type, '/')"/>
	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="$page_type = 'members'">
				<xsl:value-of select="php:function('trans', 'Students')"/>
			</xsl:when>
			<xsl:when test="$page_type = 'pretenders'">
				<xsl:value-of select="php:function('trans', 'Premoderation of students')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="stream_admin_member_edit">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$title"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="$title"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="$edited_user/@visible_name"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_member_edit/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Editing stream profile')"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$edited_user/@visible_name"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
