<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_format_edit.inc.xslt"/>
	<xsl:include href="include/stream_admin_role_edit.inc.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'formats'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/formats/', $format_id, '/')"/>
	<xsl:template match="stream_admin_format_role_delete">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Formats')"/>
			<xsl:with-param name="url" select="$formats_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$formats_url}">
					<xsl:value-of select="php:function('trans', 'Formats')"/>
				</a>
			</h1>
			<h2>
				<a href="{$roles_url}">
					<xsl:value-of select="$format_title"/>
				</a>
			</h2>
			<h3>
				<xsl:value-of select="$role_title"/>
			</h3>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_format_role_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$roles_url}"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="php:function('trans', 'the role', 'AKK')"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Deleting')"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$role_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$format_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
