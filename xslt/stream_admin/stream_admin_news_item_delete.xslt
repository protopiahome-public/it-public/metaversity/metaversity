<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'news'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="news_item_id" select="/root/stream_admin_news_item_delete/@id"/>
	<xsl:variable name="news_item_title" select="/root/stream_admin_news_item_delete/@title"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/news/')"/>
	<xsl:template match="stream_admin_news_item_delete">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'News')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'News')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="$news_item_title"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_news_item_delete/" method="post">
			<input type="hidden" name="retpath" value="{$current_stream/@url}admin/news/"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<xsl:call-template name="draw_delete">
				<xsl:with-param name="title_akk" select="php:function('trans', 'the news item', 'AKK')"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Deleting')"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$news_item_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
