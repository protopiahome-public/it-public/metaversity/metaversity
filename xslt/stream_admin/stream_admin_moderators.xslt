<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_site/include/admin_moderators.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false"/>
	<xsl:variable name="admin_section" select="'moderators'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars) and /root/stream_admin_moderators/pages/@current_page = 1"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/moderators/')"/>
	<xsl:template match="stream_admin_moderators">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Moderators')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Moderators')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Moderators')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<xsl:call-template name="draw_admin_moderators">
				<xsl:with-param name="save_ctrl" select="'stream_admin_moderators'"/>
				<xsl:with-param name="aux_params">
					<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="draw_moderators_rights_headers"/>
	<xsl:template name="draw_moderators_rights_checkboxes"/>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/admin_moderators.js?v1"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Moderators')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
