<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_format_edit.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../_site/include/rate_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'formats'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="roles_url" select="concat($format_url, 'roles/')"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/formats/')"/>
	<xsl:template match="stream_admin_format_role_add">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Formats')"/>
			<xsl:with-param name="url" select="$formats_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$formats_url}">
					<xsl:value-of select="php:function('trans', 'Formats')"/>
				</a>
			</h1>
			<h2>
				<a href="{$roles_url}">
					<xsl:value-of select="$format_title"/>
				</a>
			</h2>
			<h3>
				<xsl:value-of select="php:function('trans', 'Adding a role')"/>
			</h3>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_format_role_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$roles_url}"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<input type="hidden" name="format_id" value="{$format_id}"/>
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="draw_button" select="false()"/>
			</xsl:call-template>
			<xsl:call-template name="draw_rate_edit">
				<xsl:with-param name="legend" select="php:function('trans', 'Competences to be improved by this role')"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_study_level.js?v3"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Adding a role')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$format_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
