<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'translators'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/translators/')"/>
	<xsl:template match="stream_admin_translators">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Translators')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Translators')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Translators')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$module_url}add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a translator')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(translator)">
			<p>
				<xsl:value-of select="php:function('trans', 'Translators were not found.')"/>
			</p>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$pass_info/info[@name = 'ADDED']">
				<div class="info">
					<span>
						<xsl:value-of select="php:function('trans', 'New translator was added.')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'SAVED']">
				<div class="info">
					<span>
						<xsl:value-of select="php:function('trans', 'The translator was saved.')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'DELETED']">
				<div class="info">
					<span>
						<xsl:value-of select="php:function('trans', 'The translator was deleted.')"/>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<xsl:if test="translator">
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'From competence set')"/>
						<xsl:with-param name="class" select="'p6'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'direct-count'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Direct translations', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'formulae-count'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Formulae', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="translator">
					<tr>
						<td class="cell_text p6">
							<a href="{$module_url}{@eq_competence_set_id}/">
								<xsl:value-of select="@eq_competence_set_title"/>
							</a>
							<div class="_lines">
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Direct translations', 'COUNT')"/>
									<xsl:with-param name="text" select="@direct_count_calc"/>
									<xsl:with-param name="class" select="'_line hide600'"/>
								</xsl:call-template>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Formulae', 'COUNT')"/>
									<xsl:with-param name="text" select="@formulae_count_calc"/>
									<xsl:with-param name="class" select="'_line hide600'"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="cell_num p6 center show600">
							<xsl:value-of select="@direct_count_calc"/>
						</td>
						<td class="cell_num p6 center show600">
							<xsl:value-of select="@formulae_count_calc"/>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$module_url}{@eq_competence_set_id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Translators')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
