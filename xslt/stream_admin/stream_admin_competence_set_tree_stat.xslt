<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../_site/include/lang.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'tree-stat'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/tree-stat/')"/>
	<xsl:variable name="stat_root" select="/root/stream_admin_competence_set_tree_stat"/>
	<xsl:template match="stream_admin_competence_set_tree_stat">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Competence usage statistics')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Competence usage statistics')"/>
			</h1>
			<script type="text/javascript">
				var base_stream_url = '<xsl:value-of select="$current_stream/@url"/>';
				var position_index = <xsl:value-of select="$stat_root/position_index"/>;
				var format_index = <xsl:value-of select="$stat_root/format_index"/>;
				var role_index = <xsl:value-of select="$stat_root/role_index"/>;
				var activity_index = <xsl:value-of select="$stat_root/activity_index"/>;
				global.trans['Positions'] = '<xsl:value-of select="php:function('trans', 'Positions')"/>';
				global.trans['Formats'] = '<xsl:value-of select="php:function('trans', 'Formats')"/>';
				global.trans['Schedule'] = '<xsl:value-of select="php:function('trans', 'Schedule')"/>';
				global.trans['Close'] = '<xsl:value-of select="php:function('trans', 'Close')"/>';
			</script>
			<xsl:call-template name="_draw_legend"/>
			<xsl:call-template name="_draw_tree"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw_tree">
		<xsl:call-template name="draw_tree_head"/>
		<xsl:call-template name="draw_tree_competence_group">
			<xsl:with-param name="competence_is_clickable" select="false()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="right">
			<xsl:value-of select="php:function('trans', 'Stats (clickable!)')"/>
		</th>
	</xsl:template>
	<xsl:template mode="tree_competence_td_top" match="competence">
		<xsl:attribute name="class">cell_competence cell_competence__2col</xsl:attribute>
	</xsl:template>
	<xsl:template mode="tree_competence_td" match="competence">
		<div class="_cell_right">
			<div class="tree_stat">
				<div class="_bar_wrap">
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="$stat_root/stat">
						<div>
							<xsl:attribute name="class">
								<xsl:text>js_line _line</xsl:text>
								<xsl:value-of select="concat(' _line__', @type)"/>
								<xsl:value-of select="concat(' _line__only_important_', @only_important)"/>
								<xsl:if test="@clickable = 1"> clickable</xsl:if>
							</xsl:attribute>
							<xsl:if test="not(competence[@id = $context/@id])">
								<span class="_bar"/>
							</xsl:if>
							<xsl:for-each select="competence[@id = $context/@id]">
								<span class="js_expand_stat _clickable" data-type="{../@type}" data-competence-id="{$context/@id}" data-stat="{@stat}">
									<xsl:attribute name="data-competence-title">
										<xsl:call-template name="draw_competence_title">
											<xsl:with-param name="competence" select="$context"/>
										</xsl:call-template>
									</xsl:attribute>
									<span class="_bar" style="width: {round(@rating * 100 div ../@max_rating)}px"/>
									<span class="_text">
										<xsl:value-of select="@rating"/>
										<xsl:text>&#160;(</xsl:text>
										<xsl:value-of select="@count"/>
										<xsl:text>)</xsl:text>
									</span>
								</span>
							</xsl:for-each>
						</div>
					</xsl:for-each>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw_legend">
		<div class="box p">
			<xsl:call-template name="draw_stream_admin_competence_stat_legend"/>
			<div class="field2">
				<input id="only-important" class="ctrl ctrl__checkbox" type="checkbox"/>
				<label for="only-important" class="_inline">
					<xsl:value-of select="php:function('trans', 'Fetch only weights ≥ 2 (decreases noise)')"/>
				</label>
			</div>
			<form action="{$module_url}" method="get">
				<div class="field_line p">
					<xsl:value-of select="php:function('trans', 'Date filter (affects events and marks; fill any or both dates):')"/>
				</div>
				<div class="field_line p">
					<xsl:value-of select="php:function('trans', 'From')"/>
					<xsl:text>&#160;</xsl:text>
					<input class="ctrl ctrl__text" type="text" name="from" size="10" maxlength="10" value="{@date_from}" placeholder="YYYY-MM-DD"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'to')"/>
					<xsl:text>&#160;</xsl:text>
					<input class="ctrl ctrl__text" type="text" name="to" size="10" maxlength="10" value="{@date_to}" placeholder="YYYY-MM-DD"/>
					<xsl:text> </xsl:text>
					<button class="ctrl ctrl__button ">
						<xsl:value-of select="php:function('trans', 'Recalc')"/>
					</button>
				</div>
			</form>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script src="{$main_prefix}/js/stream_admin_competence_set_tree_stat.js?v3"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Competence usage statistics')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
