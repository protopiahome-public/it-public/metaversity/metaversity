<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/activities/')"/>
	<xsl:template match="stream_admin_activity_add">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Activities')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Adding an activity')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_activity_add/" method="post">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<xsl:call-template name="draw_dt_edit"/>
		</form>
		<script type="text/javascript">
			var groups_info = <xsl:value-of select="groups_info"/>;
		</script>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<link rel="stylesheet" type="text/css" href="{$main_prefix}/css/jquery-ui.css?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery-ui.js?v1"/>
		<script type="text/javascript">
			var FileAPI = {
				debug: global.debug,
				media: true,
				staticPath: global.main_prefix + 'js/FileAPI/'
			};
		</script>
		<script src="{$main_prefix}/js/FileAPI/FileAPI.min.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.fileapi.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
		<script type="text/javascript">
			global.trans['Deadline'] = '<xsl:value-of select="php:function('trans', 'Deadline')"/>';
			global.trans['Start time'] = '<xsl:value-of select="php:function('trans', 'Start time')"/>';
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/stream_admin_activity_edit.js?v1"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Adding an activity')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
