<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'metaactivities'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars) and /root/stream_admin_metaactivities/pages/@current_page = 1"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/metaactivities/')"/>
	<xsl:template match="stream_admin_metaactivities">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activity groups')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Activity groups')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Activity groups')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<div class="maxw800">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$current_stream/@url}admin/metaactivities/add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add an activity group')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(metaactivity)">
			<p>
				<xsl:value-of select="php:function('trans', 'Nothing was found.')"/>
			</p>
		</xsl:if>
		<xsl:call-template name="draw_form_info">
			<xsl:with-param name="added" select="php:function('trans', 'New activity group was added.')"/>
			<xsl:with-param name="edited" select="php:function('trans', 'The activity group was saved.')"/>
			<xsl:with-param name="deleted" select="php:function('trans', 'The activity group was deleted.')"/>
		</xsl:call-template>
		<table class="table">
			<tr>
				<xsl:call-template name="draw_th">
					<xsl:with-param name="name" select="'title'"/>
					<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
					<xsl:with-param name="class" select="'p6'"/>
				</xsl:call-template>
				<th/>
			</tr>
			<xsl:for-each select="metaactivity">
				<xsl:for-each select="/root/metaactivity_short[@id = current()/@id]">
					<tr>
						<td class="cell_text p6">
							<a href="{$current_stream/@url}admin/metaactivities/{@id}/">
								<xsl:value-of select="@title"/>
							</a>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$current_stream/@url}admin/metaactivities/{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</xsl:for-each>
		</table>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Activity groups')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
