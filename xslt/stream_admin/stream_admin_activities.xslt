<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../activities/include/activity_common.inc.xslt"/>
	<xsl:include href="../activities/include/activities.inc.xslt"/>
	<xsl:include href="../activities/include/activity_calendar.inc.xslt"/>
	<xsl:variable name="activities" select="/root/stream_admin_activities"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="$activities/pages/@current_page = 1 and not($activities/@is_archive = 1) and not($activities/@is_no_date = 1) and not($activities/@year != '') and not($get_vars)"/>
	<xsl:variable name="activities_url" select="concat($current_stream/@url, 'admin/activities/')"/>
	<xsl:variable name="activities_base_url" select="$activities_url"/>
	<xsl:variable name="module_url">
		<xsl:value-of select="$activities_base_url"/>
		<xsl:if test="$activities/@is_archive = 1">archive/</xsl:if>
		<xsl:if test="$activities/@is_no_date = 1">no-date/</xsl:if>
		<xsl:if test="$activities/@year != ''">
			<xsl:text>calendar/</xsl:text>
			<xsl:value-of select="$activities/@year"/>
			<xsl:if test="$activities/@month != ''">
				<xsl:text>/</xsl:text>
				<xsl:value-of select="$activities/@month"/>
				<xsl:if test="$activities/@day != ''">
					<xsl:text>/</xsl:text>
					<xsl:value-of select="$activities/@day"/>
				</xsl:if>
			</xsl:if>
			<xsl:text>/</xsl:text>
		</xsl:if>
	</xsl:variable>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_admin_activities">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$activities_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:choose>
					<xsl:when test="$admin_section_main_page">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$activities_url}">
							<xsl:value-of select="php:function('trans', 'Activities')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h1>
			<div class="p">
				<a class="link_btn link_btn__add" href="{$current_stream/@url}admin/activities/add/">
					<i class="_icon fa fa-plus"/>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Add an activity')"/>
					</span>
				</a>
			</div>
			<xsl:call-template name="draw_activities_list">
				<xsl:with-param name="mode" select="'admin'"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template match="filter" mode="filters_show_draw_href_callback">
		<xsl:call-template name="draw_activities_filters_show_draw_href_callback"/>
	</xsl:template>
	<xsl:template match="filter" mode="filters_show_draw_title_callback">
		<xsl:call-template name="draw_activities_filters_show_draw_title_callback"/>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<xsl:call-template name="draw_activities_head"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Activities')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
