<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="../_site/include/rate_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'study_materials'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="study_material_id" select="/root/stream_admin_study_material_edit/document/@id"/>
	<xsl:variable name="study_material_title" select="/root/stream_admin_study_material_edit/document/field[@name = 'title']"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/materials/')"/>
	<xsl:template match="stream_admin_study_material_edit">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Materials')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Materials')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="$study_material_title"/>
				<xsl:if test="@is_outer = 1">
					<xsl:text> (</xsl:text>
					<a href="{@outer_url}">
						<i>
							<xsl:value-of select="php:function('trans', 'external')"/>
						</i>
					</a>
					<xsl:text>)</xsl:text>
				</xsl:if>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_study_material_edit/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="draw_button" select="false()"/>
			</xsl:call-template>
			<xsl:call-template name="draw_rate_edit">
				<xsl:with-param name="legend" select="php:function('trans', 'Competences to be improved by this material')"/>
			</xsl:call-template>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/jquery-ui.js?v1"/>
		<script type="text/javascript">
			var FileAPI = {
				debug: global.debug,
				media: true,
				staticPath: global.main_prefix + 'js/FileAPI/'
			};
		</script>
		<script src="{$main_prefix}/js/FileAPI/FileAPI.min.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.fileapi.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Editing')"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$study_material_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
