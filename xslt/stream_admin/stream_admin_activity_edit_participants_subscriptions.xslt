<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="menu/stream_admin_activity.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="admin_activity_section" select="'participants-subscriptions'"/>
	<xsl:variable name="admin_activity_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/activities/')"/>
	<xsl:template match="stream_admin_activity_edit_participants_subscriptions">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span class="head_duplicate">
					<a href="{$module_url}">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</a>
					<xsl:text> / </xsl:text>
				</span>
				<a href="{$current_activity_admin_url}">
					<xsl:value-of select="$current_activity/@title"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Subscriptions')"/>
			</h2>
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="box p">
			<p>
				<xsl:value-of select="php:function('trans', 'Here you can subscribe moderators on email notifications about required premoderation of new members. When new member is applied, email will be sent to all subscribed moderators (15–20 minutes after the request is sent).')"/>
			</p>
		</div>
		<table class="table table__no_th maxw600">
			<xsl:for-each select="user">
				<xsl:variable name="context" select="."/>
				<xsl:for-each select="/root/stream_user_short[@id = current()/@id]">
					<tr>
						<td class="cell_user">
							<div class="_img">
								<a href="{$users_prefix}/{@login}/">
									<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
								</a>
							</div>
							<div class="_title">
								<a href="{$users_prefix}/{@login}/">
									<xsl:value-of select="@visible_name"/>
								</a>
								<span class="_login">
									<xsl:text> (</xsl:text>
									<xsl:value-of select="@login"/>
									<xsl:text>)</xsl:text>
								</span>
							</div>
							<div class="_city">
								<xsl:value-of select="@city_title"/>
								<xsl:if test="@group_title != ''">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="@group_title"/>
								</xsl:if>
							</div>
						</td>
						<td class="cell_ctrl pl6">
							<div class="js_subscription_select select select__no_auto_init box_shadow" data-user_id="{@id}" data-is_regular="{$context/@is_regular}">
								<div class="_list">
									<div class="_item" data-value="1">
										<i class="_icon fa fa-check status_subscribed"/>
										<xsl:choose>
											<xsl:when test="@sex = 'f'">
												<xsl:value-of select="php:function('trans', 'Subscribed', 'FEMALE')"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="php:function('trans', 'Subscribed')"/>
											</xsl:otherwise>
										</xsl:choose>
									</div>
									<div class="_item" data-value="0">
										<xsl:if test="not($context/@is_subscribed = 1)">
											<xsl:attribute name="class">_item _item__selected</xsl:attribute>
										</xsl:if>
										<i class="_icon fa status_subscribed"/>
										<xsl:value-of select="php:function('trans', 'DASH')"/>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</xsl:for-each>
		</table>
		<div class="js_unsubscribe_warning hide">
			<p>
				<xsl:value-of select="php:function('trans', 'This user is not a moderator nor an administrator of this stream. If you unsubscribe him/her, you will NOT be able to subscribe him/her again. Do you want to continue?')"/>
			</p>
		</div>
		<div class="js_cannot_subscribe_info hide">
			<p>
				<xsl:value-of select="php:function('trans', 'You cannot subscribe this user because he/she is not a moderator nor an administrator of this stream.')"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			window.post_vars = {
				stream_id: <xsl:value-of select="$current_stream/@id"/>,
				activity_id: <xsl:value-of select="$current_activity/@id"/>
			}
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.xselect.js?v5"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_admin_edit_participants_subscriptions.js?v2"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Subscriptions')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
