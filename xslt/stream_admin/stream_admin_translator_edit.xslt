<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'translators'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="eq_competence_set" select="/root/competence_set_short[@id = /root/stream_admin_translator_edit/@eq_competence_set_id]"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/translators/')"/>
	<xsl:template match="stream_admin_translator_edit">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Translators')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Translators')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'From the competence set')"/>
				<xsl:text> &#171;</xsl:text>
				<xsl:value-of select="$eq_competence_set/@title"/>
				<xsl:text>&#187;</xsl:text>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:if test="$pass_info/info[@name = 'SAVED']">
			<div class="info">
				<span>
					<xsl:value-of select="php:function('trans', 'The translator was saved.')"/>
				</span>
			</div>
		</xsl:if>
		<div class="box intmarkup p">
			<p>
				<xsl:value-of select="php:function('trans', 'Format')"/>
				<xsl:text>:</xsl:text>
			</p>
			<p>
				<code style="background: #fdd;">123 = 456</code>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', '(direct translation; the competence 123 is from the current set, 456 is from the source set)')"/>
			</p>
			<p>
				<code style="background: #fdd;">123 = 456 // MAX=2.0</code>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', '(here we set a maximum value for the result obtained from this rule; the format allowes numbers from 0.0 to 3.0 with 1 decimal digit; dot-zero can be omitted; by default there is no limitation – i.e. 3.0 is used)')"/>
			</p>
			<p>
				<code style="background: #fdd;">123 = MIN(456, 567, 678)</code>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', '(formula; the result is equal to the minimum of the results calculated for included competences)')"/>
			</p>
			<p>
				<code style="background: #fdd;">123 = AVG(456, 567, 678)</code>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', '(formula; the result is equal to the average of the results calculated for included competences)')"/>
			</p>
			<p>
				<code style="background: #fdd;">123 = MIN(456, 567, 678) // MAX=2.5</code>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', '(formulae can also have result limitations)')"/>
			</p>
			<p>
				<xsl:value-of select="php:function('trans', 'Spaces and letter case are not important. It will be automatically checked that the used competences belong to the correct tree.')"/>
			</p>
		</div>
		<xsl:if test="$pass_info/error[@name = 'INVALID_FORMAT']">
			<div class="error">
				<span>
					<xsl:value-of select="php:function('trans', 'Uploaded rules have mistakes:')"/>
				</span>
			</div>
			<ul>
				<xsl:for-each select="$pass_info/error[@name = 'INVALID_FORMAT']">
					<li>
						<xsl:value-of select="php:function('trans', 'Line')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="@line_num"/>
						<xsl:text>: </xsl:text>
						<xsl:choose>
							<xsl:when test="@descr = 'BAD_FORMAT'">
								<xsl:value-of select="php:function('trans', 'incorrect format')"/>
							</xsl:when>
							<xsl:when test="@descr = 'ALIEN_LEFT'">
								<xsl:value-of select="php:function('trans', 'unexisted or deleted competence is used in LEFT part of the formula')"/>
								<xsl:text> (</xsl:text>
								<xsl:value-of select="."/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:when test="@descr = 'ALIEN_RIGHT'">
								<xsl:value-of select="php:function('trans', 'unexisted or deleted competence is used in RIGHT part of the formula')"/>
								<xsl:text> (</xsl:text>
								<xsl:value-of select="."/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:when test="@descr = 'BAD_MAX_FORMAT'">
								<xsl:value-of select="php:function('trans', 'incorrect maximum value; it cannot be more than 3.0')"/>
								<xsl:text> (</xsl:text>
								<xsl:value-of select="."/>
								<xsl:text>)</xsl:text>
							</xsl:when>
							<xsl:when test="@descr = 'BLANK_LIST'">
								<xsl:value-of select="php:function('trans', 'empty list of competences in the formula')"/>
							</xsl:when>
						</xsl:choose>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
		<form action="{$save_prefix}/stream_admin_translator_edit/" method="post" enctype="multipart/form-data">
			<!--<input type="hidden" name="retpath" value="{$module_url}"/>-->
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<input type="hidden" name="eq_competence_set_id" value="{$eq_competence_set/@id}"/>
			<div class="field2">
				<label for="f-text">
					<xsl:value-of select="php:function('trans', 'Translation rules')"/>
					<xsl:text>:</xsl:text>
				</label>
				<textarea id="f-text" name="text" class="ctrl ctrl__textarea" style="height: 400px;">
					<xsl:choose>
						<xsl:when test="$pass_info/vars/var[@name = 'text']">
							<xsl:value-of select="$pass_info/vars/var[@name = 'text']"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="edit_text"/>
						</xsl:otherwise>
					</xsl:choose>
				</textarea>
			</div>
			<div class="field2">
				<input id="f-skip_left_aliens" class="ctrl ctrl__checkbox" type="checkbox" name="skip_left_aliens" value="1">
					<xsl:choose>
						<xsl:when test="$pass_info/vars/var[@name = 'skip_left_aliens']">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</input>
				<label for="f-skip_left_aliens" class="_inline">
					<xsl:value-of select="php:function('trans', 'Automatically delete lines with unexisted or deleted competences in the &lt;b&gt;left&lt;/b&gt; parts of the formulae')" disable-output-escaping="yes"/>
				</label>
			</div>
			<div class="field2">
				<input id="f-skip_direct_right_aliens" class="ctrl ctrl__checkbox" type="checkbox" name="skip_direct_right_aliens" value="1">
					<xsl:choose>
						<xsl:when test="$pass_info/vars/var[@name = 'skip_direct_right_aliens']">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</input>
				<label for="f-skip_direct_right_aliens" class="_inline">
					<xsl:value-of select="php:function('trans', 'Automatically delete lines with unexisted or deleted competences in the &lt;b&gt;right&lt;/b&gt; parts of the formulae (&lt;b&gt;only for direct translations&lt;/b&gt;)')" disable-output-escaping="yes"/>
				</label>
			</div>
			<div class="field2">
				<input id="f-auto_add_via_titles" class="ctrl ctrl__checkbox" type="checkbox" name="auto_add_via_titles" value="1">
					<xsl:choose>
						<xsl:when test="$pass_info/vars/var[@name = 'auto_add_via_titles']">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:when>
					</xsl:choose>
				</input>
				<label for="f-auto_add_via_titles" class="_inline">
					<xsl:value-of select="php:function('trans', 'Automatically add direct translations (A = B) using competence titles')"/>
				</label>
			</div>
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Save', 'VERB')"/>
			</button>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Editing a translator from the competence set')"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="php:function('trans', 'LQ')"/>
		<xsl:value-of select="$eq_competence_set/@title"/>
		<xsl:value-of select="php:function('trans', 'RQ')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
