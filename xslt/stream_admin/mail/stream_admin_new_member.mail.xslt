<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_layout.xslt"/>
	<xsl:variable name="current_stream" select="/root/stream_current[1]"/>
	<xsl:variable name="current_user" select="/root/user_short[1]"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:value-of select="php:function('trans', 'New member', 'STREAM_ADMIN_NEW_MEMBER_MAIL')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'DASH')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$current_stream/@title"/>
				</title>
			</head>
			<body>
				<p>
					<xsl:value-of select="php:function('trans', 'New membership request was sent into the stream', 'STREAM_ADMIN_NEW_MEMBER_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="{$current_stream/@url}">
						<xsl:value-of select="$current_stream/@title"/>
					</a>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'from the user', 'STREAM_ADMIN_NEW_MEMBER_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="{$users_prefix}/{$current_user/@login}/">
						<xsl:value-of select="$current_user/@visible_name"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'The request can be accepted or rejected on the page', 'STREAM_ADMIN_NEW_MEMBER_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="{$current_stream/@url}admin/pretenders/">
						<xsl:value-of select="php:function('trans', 'LQ')"/>
						<xsl:value-of select="php:function('trans', 'Premoderation of students')"/>
						<xsl:value-of select="php:function('trans', 'RQ')"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
				<xsl:value-of select="php:function('trans', 'You received this message because you are an administrator of this stream.', 'STREAM_ADMIN_NEW_MEMBER_MAIL')"/>
				<p>
					<xsl:value-of select="php:function('trans', 'You can unsubscribe from these emails on the page', 'STREAM_ADMIN_NEW_MEMBER_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="{$main_prefix}/settings/#notifications">
						<xsl:value-of select="php:function('trans', 'LQ')"/>
						<xsl:value-of select="php:function('trans', 'Personal settings')"/>
						<xsl:value-of select="php:function('trans', 'RQ')"/>
					</a>
					<xsl:text> (</xsl:text>
					<xsl:value-of select="php:function('trans', 'see')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'LQ')"/>
					<xsl:value-of select="php:function('trans', 'Notifications')"/>
					<xsl:value-of select="php:function('trans', 'RQ')"/>
					<xsl:text>).</xsl:text>
				</p>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
