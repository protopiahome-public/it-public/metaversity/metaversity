<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_layout.xslt"/>
	<xsl:variable name="current_stream" select="/root/stream_current[1]"/>
	<xsl:variable name="participant" select="/root/stream_user_short[1]"/>
	<xsl:variable name="role" select="/root/role_short[1]"/>
	<xsl:variable name="activity" select="/root/activity_full[1]"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:value-of select="php:function('trans', 'Participation request', 'STREAM_ADMIN_ACTIVITY_REQUEST_MAIL')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'DASH')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$current_stream/@title"/>
				</title>
			</head>
			<body>
				<p>
					<xsl:value-of select="php:function('trans', 'User', 'STREAM_ADMIN_ACTIVITY_REQUEST_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="{$users_prefix}/{$participant/@login}/">
						<xsl:value-of select="$participant/@visible_name"/>
					</a>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'applied for the role', 'STREAM_ADMIN_ACTIVITY_REQUEST_MAIL')"/>
					<xsl:text> </xsl:text>
					<b>
						<xsl:value-of select="$role/@title"/>
					</b>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'at the event', 'STREAM_ADMIN_ACTIVITY_REQUEST_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="{$current_stream/@url}activities/{$activity/@id}/">
						<xsl:value-of select="$activity/@title"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
				<p>
					<a href="{$current_stream/@url}/admin/activities/{$activity/@id}/participants/">
						<xsl:value-of select="php:function('trans', 'Moderate!', 'STREAM_ADMIN_ACTIVITY_REQUEST_MAIL')"/>
					</a>
				</p>
				<p>
					<a style="color: #999;" href="{$current_stream/@url}admin/activities/{$activity/@id}/participants-subscriptions/">
						<xsl:value-of select="php:function('trans', 'Unsubscribe', 'STREAM_ADMIN_ACTIVITY_REQUEST_MAIL')"/>
					</a>
				</p>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
