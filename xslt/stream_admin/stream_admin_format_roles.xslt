<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_format_edit.inc.xslt"/>
	<xsl:include href="include/stream_admin_study_level.inc.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'formats'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/formats/', $format_id, '/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_admin_format_roles">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Formats')"/>
			<xsl:with-param name="url" select="$formats_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$formats_url}">
					<xsl:value-of select="php:function('trans', 'Formats')"/>
				</a>
			</h1>
			<h2>
				<xsl:choose>
					<xsl:when test="not($get_vars)">
						<xsl:value-of select="$format_title"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$format_url}">
							<xsl:value-of select="$format_title"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$format_url}roles/add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a role')"/>
				</span>
			</a>
		</div>
		<xsl:variable name="roles" select="role"/>
		<xsl:if test="not($roles)">
			<p>
				<xsl:value-of select="php:function('trans', 'Roles were not found.')"/>
			</p>
		</xsl:if>
		<xsl:call-template name="draw_form_info">
			<xsl:with-param name="added" select="php:function('trans', 'New role was added.')"/>
			<xsl:with-param name="edited" select="php:function('trans', 'The role was saved.')"/>
			<xsl:with-param name="deleted" select="php:function('trans', 'The role was deleted.')"/>
		</xsl:call-template>
		<xsl:if test="$roles">
			<div class="widgets">
				<xsl:apply-templates select="filters" mode="filters"/>
			</div>
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Role')"/>
						<xsl:with-param name="class" select="'p6'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'number'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Vacancies', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'competences'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Competences', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'auto-accept'"/>
						<xsl:with-param name="title" select="php:function('trans', 'No moderation')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="title" select="php:function('trans', 'Study levels')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="$roles">
					<tr>
						<td class="cell_text p6">
							<a href="{$format_url}roles/{@id}/">
								<xsl:value-of select="@title"/>
							</a>
							<div class="props">
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Vacancies', 'COUNT')"/>
									<xsl:with-param name="text" select="@total_number"/>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Competences', 'COUNT')"/>
									<xsl:with-param name="text" select="@competence_count_calc"/>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'No moderation')"/>
									<xsl:with-param name="text">
										<xsl:choose>
											<xsl:when test="@auto_accept = 1">
												<xsl:value-of select="php:function('trans', 'Yes')"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="php:function('trans', 'No')"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Study levels')"/>
									<xsl:with-param name="text">
										<xsl:call-template name="draw_stream_admin_study_level_short_value"/>
									</xsl:with-param>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="cell_num p6 center show600">
							<xsl:value-of select="@total_number"/>
						</td>
						<td class="cell_num p6 center show600">
							<xsl:value-of select="@competence_count_calc"/>
						</td>
						<td class="cell_num p6 center show600">
							<xsl:choose>
								<xsl:when test="@auto_accept = 1">
									<xsl:value-of select="php:function('trans', 'Yes')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="php:function('trans', 'No')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td class="cell_text p6 center show600">
							<xsl:call-template name="draw_stream_admin_study_level_short_value"/>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$format_url}roles/{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Format roles')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$format_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
