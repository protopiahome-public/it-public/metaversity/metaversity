<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'import-users'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/import-users/')"/>
	<xsl:template match="stream_admin_import_users">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Import users')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Import users')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:if test="$pass_info/info[@name = 'SAVED']">
			<div class="info">
				<span>
					<xsl:value-of select="php:function('trans', 'Processed records:', 'COUNT')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$pass_info/info[@name = 'SAVED']"/>
					<xsl:text>.</xsl:text>
				</span>
			</div>
		</xsl:if>
		<xsl:if test="$pass_info/info[@name = 'REGISTERED']">
			<div class="info">
				<span>
					<xsl:value-of select="php:function('trans', 'Registered:', 'USER_LIST')"/>
					<xsl:text> </xsl:text>
					<xsl:for-each select="$pass_info/info[@name = 'REGISTERED']">
						<a href="{$users_prefix}/{.}/">
							<xsl:value-of select="."/>
						</a>
						<xsl:choose>
							<xsl:when test="position() != last()">, </xsl:when>
							<xsl:otherwise>.</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</span>
			</div>
		</xsl:if>
		<xsl:if test="$pass_info/info[@name = 'UPDATED']">
			<div class="info">
				<span>
					<xsl:value-of select="php:function('trans', 'Profile updated:', 'USER_LIST')"/>
					<xsl:text> </xsl:text>
					<xsl:for-each select="$pass_info/info[@name = 'UPDATED']">
						<a href="{$users_prefix}/{.}/">
							<xsl:value-of select="."/>
						</a>
						<xsl:choose>
							<xsl:when test="position() != last()">, </xsl:when>
							<xsl:otherwise>.</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</span>
			</div>
		</xsl:if>
		<xsl:if test="$pass_info/info[@name = 'JOINED']">
			<div class="info">
				<span>
					<xsl:value-of select="php:function('trans', 'New stream members:', 'COUNT')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$pass_info/info[@name = 'JOINED']"/>
					<xsl:text>.</xsl:text>
				</span>
			</div>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$pass_info/info[@name = 'FORCE_TO_SUBMIT']">
				<h2>
					<xsl:value-of select="php:function('trans', 'Please review')"/>
				</h2>
				<p>
					<xsl:value-of select="php:function('trans', 'Total users:', 'COUNT')"/>
					<xsl:text> </xsl:text>
					<b>
						<xsl:value-of select="count($pass_info/user)"/>
					</b>
					<xsl:text>. </xsl:text>
					<xsl:value-of select="php:function('trans', 'Remember: only first 3000 will be processed.')"/>
				</p>
				<xsl:call-template name="_draw_status">
					<xsl:with-param name="status" select="'ERR_BAD_EMAIL'"/>
					<xsl:with-param name="title" select="php:function('trans', '[1/4] Bad emails (the records will be ignored)')"/>
				</xsl:call-template>
				<xsl:call-template name="_draw_status">
					<xsl:with-param name="status" select="'TO_JOIN'"/>
					<xsl:with-param name="title" select="php:function('trans', '[2/4] Users to be joined to the stream')"/>
				</xsl:call-template>
				<xsl:call-template name="_draw_status">
					<xsl:with-param name="status" select="'TO_REG'"/>
					<xsl:with-param name="title" select="php:function('trans', '[3/4] Users to be registered and joined')"/>
				</xsl:call-template>
				<xsl:call-template name="_draw_status">
					<xsl:with-param name="status" select="'PASS'"/>
					<xsl:with-param name="title" select="php:function('trans', '[4/4] Already in the stream')"/>
				</xsl:call-template>
				<form action="{$save_prefix}/stream_admin_import_users/" method="post">
					<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
					<input type="hidden" name="data" value="{$pass_info/vars/var[@name = 'data']}"/>
					<div class="field2">
						<input type="submit" class="ctrl ctrl__button ctrl__button__cancel ctrl__button__for_right" name="cancel" value="{php:function('trans', 'Go back')}"/>
						<input type="submit" class="ctrl ctrl__button" name="force" value="{php:function('trans', 'Confirm')}"/>
					</div>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<div class="box p">
					<p>
						<xsl:value-of select="php:function('trans', 'Here you can add users (either registered or not) into this stream.', 'USERS_IMPORT_MANUAL')"/>
					</p>
					<p>
						<xsl:value-of select="php:function('trans', 'Format: tab separated values (just copy a table from Excel).', 'USERS_IMPORT_MANUAL')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Columns order: 1) Email, 2) First name, 3) Last name, 4) Desired login, 5) External ID.', 'USERS_IMPORT_MANUAL')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Right columns may be omitted (be careful about the order).', 'USERS_IMPORT_MANUAL')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'External ID must be integer.', 'USERS_IMPORT_MANUAL')"/>
					</p>
					<p>
						<xsl:value-of select="php:function('trans', 'If an email is new for the system, a new user is registered.', 'USERS_IMPORT_MANUAL')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'We try to use desired login or left part of email address as a login if possible.', 'USERS_IMPORT_MANUAL')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Password is not set: new user must reset it to login.', 'USERS_IMPORT_MANUAL')"/>
					</p>
					<p>
						<xsl:value-of select="php:function('trans', 'Please note that Email is the first column.', 'USERS_IMPORT_MANUAL')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'This means that just one email or a list of emails (one per line) can be used as input data.', 'USERS_IMPORT_MANUAL')"/>
					</p>
				</div>
				<form action="{$save_prefix}/stream_admin_import_users/" method="post">
					<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
					<div class="field2">
						<textarea class="ctrl ctrl__textarea" rows="12" cols="80" name="data">
							<xsl:value-of select="$pass_info/vars/var[@name = 'data']"/>
						</textarea>
					</div>
					<div class="field2">
						<input type="submit" class="ctrl ctrl__button" value="{php:function('trans', 'Import', 'VERB')}"/>
					</div>
				</form>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_status">
		<xsl:param name="status"/>
		<xsl:param name="title"/>
		<h3>
			<xsl:value-of select="$title"/>
		</h3>
		<xsl:if test="not($pass_info/user[@status = $status])">
			<p>
				<xsl:value-of select="php:function('trans', 'Nobody here.')"/>
			</p>
		</xsl:if>
		<ul>
			<xsl:for-each select="$pass_info/user[@status = $status]">
				<li>
					<xsl:choose>
						<xsl:when test="@current_login != ''">
							<a target="_blank" href="{$users_prefix}/{@current_login}/">
								<xsl:value-of select="@email"/>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@email"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> (</xsl:text>
					<xsl:text>FirstName=</xsl:text>
					<xsl:value-of select="@first_name"/>
					<xsl:text>, LastName=</xsl:text>
					<xsl:value-of select="@last_name"/>
					<xsl:if test="@status = 'TO_REG'">
						<xsl:text>, LoginsToTest=</xsl:text>
						<xsl:value-of select="@login"/>
						<xsl:if test="@login2 != ''">
							<xsl:if test="@login != ''">,</xsl:if>
							<xsl:value-of select="@login2"/>
						</xsl:if>
					</xsl:if>
					<xsl:text>, ID=</xsl:text>
					<xsl:value-of select="@imported_foreign_id"/>
					<xsl:text>)</xsl:text>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Import users')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
