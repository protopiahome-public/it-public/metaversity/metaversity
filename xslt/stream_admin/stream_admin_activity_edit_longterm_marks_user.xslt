<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="menu/stream_admin_activity.menu.xslt"/>
	<xsl:include href="include/stream_admin_activity_edit_longterm_marks_common.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="admin_activity_section" select="'longterm-marks'"/>
	<xsl:variable name="admin_activity_section_main_page" select="false()"/>
	<xsl:variable name="longterm_marks_section" select="'user'"/>
	<xsl:variable name="longterm_marks_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="current_user" select="/root/stream_user_short[1]"/>
	<xsl:variable name="module_url" select="concat($current_activity_admin_url, $admin_activity_section, '/', $current_user/@login, '/')"/>
	<xsl:template match="stream_admin_activity_edit_longterm_marks_user">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$activities_admin_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span class="head_duplicate">
					<a href="{$module_url}">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</a>
					<xsl:text> / </xsl:text>
				</span>
				<a href="{$current_activity_admin_url}">
					<xsl:value-of select="$current_activity/@title"/>
				</a>
			</h1>
			<h2>
				<xsl:choose>
					<xsl:when test="$admin_activity_section_main_page">
						<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_activity_admin_url}longterm-marks/">
							<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</h2>
			<div class="box p">
				<xsl:call-template name="draw_stream_admin_activity_edit_longterm_marks_grading_period"/>
			</div>
			<xsl:call-template name="draw_stream_admin_activity_edit_longterm_marks_user_header"/>
			<div class="prop p">
				<a class="underline_inner_span" href="{$users_prefix}/{$current_user/@login}/">
					<span>
						<xsl:value-of select="php:function('trans', 'User profile')"/>
					</span>
					<xsl:text> </xsl:text>
					<i class="fa fa-external-link"/>
				</a>
			</div>
			<xsl:apply-templates mode="filter_form"/>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:choose>
			<xsl:when test="filters/filter[@name = 'sort-mode']/@value = 'tree'">
				<xsl:call-template name="draw_tree_head"/>
				<xsl:call-template name="draw_tree_competence_group">
					<xsl:with-param name="competence_is_clickable" select="true()"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<div class="maxw600">
					<xsl:if test="not(competence)">
						<p>
							<xsl:value-of select="php:function('trans', 'Nothing was found.')"/>
						</p>
					</xsl:if>
					<xsl:if test="competence">
						<table class="table w100p">
							<tr>
								<th colspan="2" class="right">
									<xsl:value-of select="php:function('trans', 'Total weight')"/>
								</th>
							</tr>
							<xsl:for-each select="competence">
								<tr>
									<td class="cell_text w100p">
										<a href="{concat($module_url, @id, '/')}">
											<xsl:call-template name="draw_competence"/>
										</a>
										<xsl:apply-templates mode="tree_competence_td" select="."/>
									</td>
									<td class="cell_num p10 center">
										<xsl:value-of select="@total_weight"/>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="right">
			<xsl:value-of select="php:function('trans', 'Total weight')"/>
		</th>
	</xsl:template>
	<xsl:template mode="tree_competence_custom_url" match="competence">
		<xsl:value-of select="concat($module_url, @id, '/')"/>
	</xsl:template>
	<xsl:template mode="tree_competence_td" match="competence">
		<xsl:for-each select="/root/stream_admin_activity_edit_longterm_marks_user/competence[@mark != '' and @id = current()/@id][1]">
			<div class="props">
				<div class="prop">
					<xsl:value-of select="php:function('trans', 'Mark')"/>
					<xsl:text>: </xsl:text>
					<xsl:call-template name="draw_mark">
						<xsl:with-param name="competence_id" select="@id"/>
						<xsl:with-param name="weight" select="@weight"/>
					</xsl:call-template>
				</div>
				<xsl:for-each select="/root/stream_user_short[@id = current()/@changer_user_id][1]">
					<xsl:call-template name="draw_kv">
						<xsl:with-param name="key">
							<xsl:choose>
								<xsl:when test="@sex = 'f'">
									<xsl:value-of select="php:function('trans', 'Rated by', 'FEMALE')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="php:function('trans', 'Rated by', 'MALE')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="html">
							<!--<a href="{@url}" class="inline_photo">
								<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
							</a>
							<xsl:text> </xsl:text>-->
							<a href="{@url}">
								<xsl:value-of select="@visible_name"/>
							</a>
						</xsl:with-param>
						<xsl:with-param name="class" select="'prop'"/>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:call-template name="draw_kv">
					<xsl:with-param name="key" select="php:function('trans', 'Comment')"/>
					<xsl:with-param name="text" select="@comment"/>
					<xsl:with-param name="class" select="'prop'"/>
				</xsl:call-template>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="tree_right_td" match="competence">
		<td class="cell_num p10 center">
			<xsl:value-of select="/root/stream_admin_activity_edit_longterm_marks_user/competence[@id = current()/@id]/@total_weight"/>
		</td>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_user/@visible_name"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Longterm marks')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
