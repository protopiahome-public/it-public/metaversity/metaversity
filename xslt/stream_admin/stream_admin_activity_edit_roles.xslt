<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="menu/stream_admin_activity.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'activities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="admin_activity_section" select="'roles'"/>
	<xsl:variable name="admin_activity_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/activities/')"/>
	<xsl:template match="stream_admin_activity_edit_roles">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Activities')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span class="head_duplicate">
					<a href="{$module_url}">
						<xsl:value-of select="php:function('trans', 'Activities')"/>
					</a>
					<xsl:text> / </xsl:text>
				</span>
				<a href="{$current_activity_admin_url}">
					<xsl:value-of select="$current_activity/@title"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Roles')"/>
			</h2>
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_activity_edit_roles/" method="post">
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<input type="hidden" name="activity_id" value="{@activity_id}"/>
			<input type="hidden" name="city_id" value="{@city_id}"/>
			<xsl:if test="$pass_info/info[@name = 'SAVED']">
				<div class="info">
					<span>
						<xsl:value-of select="php:function('trans', 'The form is saved.')"/>
					</span>
				</div>
			</xsl:if>
			<xsl:if test="not(role)">
				<p>
					<xsl:value-of select="php:function('trans', 'No roles were found.')"/>
				</p>
			</xsl:if>
			<xsl:if test="role">
				<xsl:variable name="obj" select="/root/stream_study_level_restrictions/object"/>
				<xsl:variable name="roles_actual" select="role[not($obj/@id = @id) or $obj[@show = 1]/@id = @id]"/>
				<xsl:variable name="roles_not_actual" select="role[not($roles_actual/@id = @id)]"/>
				<xsl:variable name="roles_are_filtered" select="/root/stream_study_level_restrictions/@current_study_level_id &gt; 0"/>
				<xsl:choose>
					<xsl:when test="$roles_are_filtered">
						<xsl:call-template name="_draw_set">
							<xsl:with-param name="roles" select="$roles_actual"/>
							<xsl:with-param name="title" select="php:function('trans', 'Actual roles')"/>
							<xsl:with-param name="hint_html">
								<div class="box p">
									<p>
										<xsl:value-of select="php:function('trans', 'Roles, suitable for the study level of the activity')"/>
										<xsl:text> (</xsl:text>
										<b>
											<xsl:value-of select="$current_activity/@study_level_title"/>
										</b>
										<xsl:text>).</xsl:text>
									</p>
								</div>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="_draw_set">
							<xsl:with-param name="roles" select="$roles_not_actual"/>
							<xsl:with-param name="title" select="php:function('trans', 'Non-actual roles')"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="_draw_set">
							<xsl:with-param name="roles" select="$roles_actual"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="role">
				<div class="field2">
					<button class="ctrl ctrl__button ctrl__button__big">
						<xsl:value-of select="php:function('trans', 'Save', 'VERB')"/>
					</button>
				</div>
			</xsl:if>
		</form>
	</xsl:template>
	<xsl:template name="_draw_set">
		<xsl:param name="roles"/>
		<xsl:param name="title" select="''"/>
		<xsl:param name="hint_html"/>
		<xsl:if test="$title != ''">
			<h3>
				<xsl:value-of select="$title"/>
			</h3>
		</xsl:if>
		<xsl:if test="$hint_html">
			<xsl:copy-of select="$hint_html"/>
		</xsl:if>
		<xsl:if test="not($roles[1])">
			<p>
				<xsl:value-of select="php:function('trans', 'Nothing was found.')"/>
			</p>
		</xsl:if>
		<xsl:for-each select="$roles">
			<fieldset>
			
			<h3>
				<xsl:value-of select="@title"/>
			</h3>
			<div class="field2">
				<input id="field_enabled_{@id}" name="enabled[{@id}]" type="checkbox" class="js_toggle ctrl ctrl__checkbox" data-toggle-for="js_fields_{@id}" value="1">
					<xsl:if test="@enabled = 1">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:if>
				</input>
				<label class="_inline" for="field_enabled_{@id}">
					<xsl:value-of select="php:function('trans', 'Use this role')"/>
				</label>
			</div>
			<div id="js_fields_{@id}">
				<xsl:if test="@enabled = 0">
					<xsl:attribute name="class">hide</xsl:attribute>
				</xsl:if>
				<div class="field_line">
					<label for="field_total_number_{@id}" class="ctrl ctrl__txt">
						<xsl:value-of select="php:function('trans', 'Number of vacancies')"/>
						<xsl:text>:</xsl:text>
					</label>
					<xsl:text> </xsl:text>
					<input id="field_total_number_{@id}" name="total_number[{@id}]" type="number" class="ctrl ctrl__text" value="{@total_number}" min="0" max="999" maxlength="3"/>
					<xsl:text> </xsl:text>
					<label for="field_total_number_{@id}" class="ctrl ctrl__txt">
						<xsl:value-of select="php:function('trans', '(zero = unlimited)')"/>
					</label>
				</div>
				<div class="field2">
					<input id="field_auto_accept_{@id}" name="auto_accept[{@id}]" type="checkbox" class="ctrl ctrl__checkbox" value="1">
						<xsl:if test="@auto_accept = 1">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<label class="_inline" for="field_auto_accept_{@id}">
						<xsl:value-of select="php:function('trans', 'Accept all requests (no moderation)')"/>
						
					</label>
				</div>
				<div class="field2">
					<label for="field_descr_text_{@id}">
						<xsl:value-of select="php:function('trans', 'Role description')"/>
						<xsl:text>:</xsl:text>
					</label>
					<textarea id="field_descr_text_{@id}" name="descr_text[{@id}]" class="ctrl ctrl__textarea" rows="6" cols="40">
						<xsl:value-of select="descr_text"/>
					</textarea>
				</div>
			</div></fieldset>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Roles')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
