<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:include href="include/stream_admin_competence_set_tree.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'tree'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/tree/')"/>
	<xsl:template match="stream_admin_competence_set_tree">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Competence tree')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Competence tree')"/>
			</h1>
			<xsl:choose>
				<xsl:when test="@multi_link">
					<p>
						<xsl:value-of select="php:function('trans', 'This competence tree is read-only.')"/>
					</p>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="_draw"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<span id="tree_button_add_group" class="link_btn link_btn__disabled link_btn__add_group link_btn__for_right">
				<i class="_icon _icon__1 fa fa-folder"/>
				<i class="_icon _icon__2 fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a group')"/>
				</span>
			</span>
			<span id="tree_button_add_item" class="link_btn link_btn__disabled link_btn__add link_btn__for_right">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a competence')"/>
				</span>
			</span>
			<span id="tree_button_edit" class="link_btn link_btn__disabled link_btn__edit link_btn__for_right">
				<i class="_icon fa fa-pencil"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Edit')"/>
				</span>
			</span>
			<span id="tree_button_delete" class="link_btn link_btn__disabled link_btn__delete">
				<i class="_icon fa fa-times"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Delete', 'VERB')"/>
				</span>
			</span>
		</div>
		<div id="competence_tree" class="tree_wrap"/>
		<div id="dialog-confirm-delete" data-title="{php:function('trans', 'Confirmation')}" data-button-title="{php:function('trans', 'Delete', 'VERB')}" class="hide">
			<p>
				<xsl:value-of select="php:function('trans', 'Please confirm that you want to delete this item.')"/>
			</p>
		</div>
		<div id="tree_edit_form" data-title="{php:function('trans', 'Editing')}" data-button-title="{php:function('trans', 'Save', 'VERB')}" class="hide">
			<div class="field2">
				<label for="tree_edit_form_title">
					<xsl:value-of select="php:function('trans', 'Title')"/>
					<xsl:text>:</xsl:text>
					<span class="star">&#160;*</span>
				</label>
				<input id="tree_edit_form_title" class="ctrl ctrl__text" type="text" value="" maxlength="1024"/>
			</div>
			<div id="tree_edit_form_study_levels" class="field2" data-stream-id="{study_levels/@stream_id}">
				<label>
					<xsl:value-of select="php:function('trans', 'Study levels')"/>
					<xsl:text>:</xsl:text>
				</label>
				<div class="_set" style="overflow: auto; max-height: 180px;">
					<xsl:for-each select="study_levels/study_level">
						<div class="_set_item">
							<input id="tree_edit_form_study_level_{@id}" class="jq-tree-edit-form-study-level ctrl ctrl__checkbox" type="checkbox" value="1" data-id="{@id}"/>
							<label for="tree_edit_form_study_level_{@id}" class="_inline">
								<xsl:value-of select="@title"/>
							</label>
						</div>
					</xsl:for-each>
				</div>
				<div class="_comment">
					<xsl:value-of select="php:function('trans', 'Nothing is checked = No restrictions based on study levels.')"/>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var competences_ajax_ctrl = 'stream_admin_competence_set_tree';
			var competences_ajax_url = global.ajax_prefix + competences_ajax_ctrl + '/';
			var competence_set_id = <xsl:value-of select="@competence_set_id"/>;
			var stream_id = '<xsl:value-of select="@stream_id"/>';
			var trans = {
				'Add a group': '<xsl:value-of select="php:function('trans', 'Add a group')"/>',
				'New group': '<xsl:value-of select="php:function('trans', 'New group')"/>',
				'Add a competence': '<xsl:value-of select="php:function('trans', 'Add a competence')"/>',
				'New competence': '<xsl:value-of select="php:function('trans', 'New competence')"/>',
				'Edit': '<xsl:value-of select="php:function('trans', 'Edit')"/>',
				'Delete': '<xsl:value-of select="php:function('trans', 'Delete', 'VERB')"/>'
			};
		</script>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<link rel="stylesheet" href="{$main_prefix}/lib/jstree/themes/default/style.css?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.hotkeys.js?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/jstree.min.js?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.scrollTo.js?v2"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_admin_competence_set_tree.js?v3"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Competence tree')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
