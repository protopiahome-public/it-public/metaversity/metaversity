<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream_admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'admin'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'translators'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="eq_competence_set" select="/root/competence_set_short[@id = /root/stream_admin_translator_delete/@eq_competence_set_id]"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'admin/translators/')"/>
	<xsl:template match="stream_admin_translator_delete">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Translators')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Translators')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'From the competence set')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'LQ')"/>
				<xsl:value-of select="$eq_competence_set/@title"/>
				<xsl:value-of select="php:function('trans', 'RQ')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/stream_admin_translator_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
			<input type="hidden" name="eq_competence_set_id" value="{$eq_competence_set/@id}"/>
			<div class="box p">
				<p>
					<xsl:value-of select="php:function('trans', 'Please comfirm that you want to delete the translator from the competence set')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'LQ')"/>
					<b>
						<xsl:value-of select="$eq_competence_set/@title"/>
					</b>
					<xsl:value-of select="php:function('trans', 'RQ')"/>
					<xsl:text>. </xsl:text>
				</p>
			</div>
			<div class="p">
				<input class="ctrl ctrl__button ctrl__button__big ctrl__button__red ctrl__button__for_right" type="submit" value="{php:function('trans', 'Delete', 'VERB')}"/>
				<input class="ctrl ctrl__button ctrl__button__big ctrl__button__cancel" type="submit" name="cancel" value="{php:function('trans', 'Cancel', 'VERB')}"/>
			</div>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Deleting a translator from the competence set')"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="php:function('trans', 'LQ')"/>
		<xsl:value-of select="$eq_competence_set/@title"/>
		<xsl:value-of select="php:function('trans', 'RQ')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
