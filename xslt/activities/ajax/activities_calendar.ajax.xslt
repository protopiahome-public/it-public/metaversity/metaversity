<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_layout.xslt"/>
	<xsl:include href="../include/activity_calendar.inc.xslt"/>
	<xsl:variable name="activities" select="/root/activities"/>
	<xsl:variable name="activities_base_url" select="$activities/@base_url"/>
	<xsl:template match="activities_calendar">
		<xsl:call-template name="draw_activities_calendar_table"/>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
