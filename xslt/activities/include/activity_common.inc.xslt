<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_activity_date_short">
		<xsl:param name="context" select="."/>
		<xsl:choose>
			<xsl:when test="$context/@date_type = 'event' and $context/@start_time = ''"/>
			<xsl:when test="($context/@date_type = 'deadline' or $context/@date_type = 'longterm') and $context/@start_time = ''"/>
			<xsl:when test="$context/@date_type = 'event' or $context/@date_type = 'longterm'">
				<xsl:call-template name="get_date_range">
					<xsl:with-param name="start_datetime" select="$context/@start_time"/>
					<xsl:with-param name="finish_datetime" select="$context/@finish_time"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$context/@date_type = 'deadline'">
				<xsl:call-template name="get_full_date">
					<xsl:with-param name="datetime" select="$context/@start_time"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_activity_prop_date">
		<span class="fa fa-calendar-o"/>
		<xsl:text> </xsl:text>
		<xsl:choose>
			<xsl:when test="@start_time = ''">
				<span class="note">
					<xsl:value-of select="php:function('trans', 'Date is not assigned')"/>
				</span>
			</xsl:when>
			<xsl:when test="@date_type = 'event'"/>
			<xsl:when test="@date_type = 'deadline'">
				<span class="hl">
					<xsl:value-of select="php:function('trans', 'Deadline')"/>
					<xsl:text>: </xsl:text>
				</span>
			</xsl:when>
			<xsl:when test="@date_type = 'longterm'">
				<span class="hl">
					<xsl:value-of select="php:function('trans', 'Grading date interval')"/>
					<xsl:text>: </xsl:text>
				</span>
			</xsl:when>
		</xsl:choose>
		<xsl:if test="@start_time != ''">
			<xsl:call-template name="get_time_range">
				<xsl:with-param name="start_datetime" select="@start_time"/>
				<xsl:with-param name="finish_datetime" select="@finish_time"/>
				<xsl:with-param name="with_week_day" select="true()"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_activity_prop_city">
		<span class="fa fa-map-marker"/>
		<xsl:text> </xsl:text>
		<xsl:choose>
			<xsl:when test="@city_id != ''">
				<xsl:value-of select="@city_title"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="php:function('trans', 'For all cities')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_activity_participant_status">
		<xsl:param name="status" select="@status"/>
		<xsl:param name="mode" select="'full'"/>
		<xsl:param name="no_default" select="false()"/>
		<xsl:param name="dot" select="false()"/>
		<xsl:param name="you" select="false()"/>
		<xsl:if test="$status">
			<span class="status_{$status}">
				<xsl:choose>
					<xsl:when test="$status = 'accepted'">
						<i class="fa fa-check"/>
						<xsl:if test="$mode != 'icon'">&#160;</xsl:if>
					</xsl:when>
					<xsl:when test="$status = 'premoderation'">
						<i class="fa fa-ellipsis-h"/>
						<xsl:if test="$mode != 'icon'">&#160;</xsl:if>
					</xsl:when>
					<xsl:when test="$status = 'declined'">
						<i class="fa fa-times"/>
						<xsl:if test="$mode != 'icon'">&#160;</xsl:if>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="$mode != 'icon'">
					<xsl:choose>
						<xsl:when test="$status = 'accepted' and $mode = 'shorter'">
							<xsl:value-of select="php:function('trans', 'Approved', 'ACTIVITY_STATUS')"/>
						</xsl:when>
						<xsl:when test="$status = 'accepted'">
							<xsl:value-of select="php:function('trans', 'Participation is approved', 'ACTIVITY_STATUS')"/>
						</xsl:when>
						<xsl:when test="$status = 'premoderation' and $mode = 'shorter'">
							<xsl:value-of select="php:function('trans', 'On moderation', 'ACTIVITY_STATUS')"/>
						</xsl:when>
						<xsl:when test="$status = 'premoderation'">
							<xsl:value-of select="php:function('trans', 'Request is on moderation', 'ACTIVITY_STATUS')"/>
						</xsl:when>
						<xsl:when test="$status = 'declined'">
							<xsl:value-of select="php:function('trans', 'Participation is rejected', 'ACTIVITY_STATUS')"/>
						</xsl:when>
						<xsl:when test="not($no_default) and $you">
							<xsl:value-of select="php:function('trans', 'You are not a participant', 'ACTIVITY_STATUS')"/>
						</xsl:when>
						<xsl:when test="not($no_default)">
							<xsl:value-of select="php:function('trans', 'Not a participant', 'ACTIVITY_STATUS')"/>
						</xsl:when>
					</xsl:choose>
					<xsl:if test="$dot">.</xsl:if>
				</xsl:if>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_activity_participant_potential">
		<xsl:param name="potential" select="@potential"/>
		<xsl:param name="class" select="''"/>
		<xsl:param name="regular" select="false()"/>
		<xsl:variable name="potential_cropped">
			<xsl:choose>
				<xsl:when test="$potential = '?' or $regular">10</xsl:when>
				<xsl:when test="number($potential) &gt; 4">10</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="round(number($potential) * 2.5)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="delta" select="(10 - $potential_cropped) * 15"/>
		<span class="potential {$class}" style="color: rgb(255, {127 + round($delta div 2)}, {$delta})">
			<i class="_icon fa fa-bolt"/>
			<span class="_count">
				<xsl:choose>
					<xsl:when test="$potential_cropped >= 6 and not($regular)">
						<b>
							<xsl:value-of select="$potential"/>
						</b>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$potential"/>
					</xsl:otherwise>
				</xsl:choose>
			</span>
		</span>
	</xsl:template>
	<xsl:template name="draw_activity_competence_potential">
		<xsl:param name="potential" select="@potential"/>
		<xsl:param name="class" select="''"/>
		<xsl:param name="regular" select="false()"/>
		<xsl:variable name="potential_cropped">
			<xsl:choose>
				<xsl:when test="$potential = '?' or $regular">10</xsl:when>
				<xsl:when test="number($potential) &gt; 2">10</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="round(number($potential) * 5)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="delta" select="10 - $potential_cropped"/>
		<span class="potential potential__competence {$class}" style="color: rgb({round(54 + 11.7 * $delta)}, {round(145 + 6.4 * $delta)}, {round(229 + 1.5 * $delta)})">
			<i class="_icon fa fa-bolt"/>
			<span class="_count">
				<xsl:choose>
					<xsl:when test="$potential_cropped >= 6 and not($regular)">
						<b>
							<xsl:value-of select="$potential"/>
						</b>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$potential"/>
					</xsl:otherwise>
				</xsl:choose>
			</span>
		</span>
	</xsl:template>
	<xsl:template name="draw_activity_potential_stream_selector">
		<xsl:param name="is_stream_context"/>
		<xsl:param name="field_class" select="''"/>
		<xsl:choose>
			<xsl:when test="/root/potential_math_user_streams/stream">
				<form class="filter">
					<xsl:choose>
						<xsl:when test="$is_stream_context">
							<xsl:attribute name="method">get</xsl:attribute>
							<xsl:attribute name="action">
								<xsl:value-of select="$module_url"/>
							</xsl:attribute>
							<xsl:for-each select="$get_vars[@name != 'potential-stream']">
								<xsl:call-template name="get_param_for_form"/>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="method">post</xsl:attribute>
							<xsl:attribute name="action">
								<xsl:value-of select="concat($save_prefix, '/user_option/')"/>
							</xsl:attribute>
							<input type="hidden" name="option" value="potential_stream_id"/>
						</xsl:otherwise>
					</xsl:choose>
					<div class="field2 {$field_class}">
						<label for="f-potential-stream">
							<xsl:value-of select="php:function('trans', 'Take positions for potential calculation from this stream:')"/>
						</label>
						<select id="f-potential-stream" class="js_ctrl_select_with_front js_potential_stream_select ctrl ctrl__select">
							<xsl:choose>
								<xsl:when test="$is_stream_context">
									<xsl:attribute name="name">potential-stream</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="name">value</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:for-each select="/root/potential_math_user_streams/stream">
								<option value="{@id}">
									<xsl:if test="@is_selected = 1">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:if>
									<xsl:for-each select="/root/stream_short[@id = current()/@id][1]">
										<xsl:text>(</xsl:text>
										<xsl:value-of select="@title_short"/>
										<xsl:text>) </xsl:text>
										<xsl:value-of select="@title"/>
									</xsl:for-each>
								</option>
							</xsl:for-each>
						</select>
					</div>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<i class="fa fa-exclamation-triangle"/>
					<xsl:text> </xsl:text>
					<xsl:choose>
						<xsl:when test="$is_stream_context">
							<xsl:choose>
								<xsl:when test="$user/@id">
									<xsl:value-of select="php:function('trans', 'Please take some positions into', 'PLEASE_FOCUS')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="php:function('trans', 'Please login and take some positions into', 'PLEASE_FOCUS')"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> </xsl:text>
							<a href="{$current_stream/@url}positions/">
								<xsl:value-of select="php:function('trans', 'your focus', 'PLEASE_FOCUS')"/>
							</a>
							<xsl:text>.</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'Please join a stream and take some positions into your focus.')"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'After that you will start receiving personal recommendations in activities, roles and materials.')"/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_activity_competence_potential_notice">
		<div class="box p">
			<p>
				<xsl:value-of select="php:function('trans', 'Shown is potential of improvement for the competence')"/>
				<xsl:text> </xsl:text>
				<b>
					<xsl:for-each select="competence[1]">
						<xsl:call-template name="draw_competence">
							<xsl:with-param name="dot" select="true()"/>
						</xsl:call-template>
					</xsl:for-each>
				</b>
			</p>
		</div>
	</xsl:template>
</xsl:stylesheet>
