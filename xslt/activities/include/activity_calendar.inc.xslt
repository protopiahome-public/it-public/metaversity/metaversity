<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_activities_calendar">
		<xsl:for-each select="/root/activities_calendar[1]">
			<div class="widget widget__avoid_header">
				<div class="widget_head">
					<i class="widget_head_icon fa fa-calendar"/>
					<span class="widget_head_title">
						<xsl:value-of select="php:function('trans', 'Calendar')"/>
					</span>
					<i class="widget_head_expand_icon fa"/>
				</div>
				<div class="widget_content">
					<div class="js_calendar calendar">
						<xsl:call-template name="draw_activities_calendar_table"/>
						<div class="_bottom"/>
						<!--<div class="_archive">
							<xsl:choose>
								<xsl:when test="/root/activities/@is_archive = 1">
									<a href="{$activities_base_url}{$request/@query_string}">
										<xsl:value-of select="php:function('trans', 'Show future events')"/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<a href="{$activities_base_url}archive/{$request/@query_string}">
										<xsl:value-of select="php:function('trans', 'Show archived events')"/>
									</a>
								</xsl:otherwise>
							</xsl:choose>
						</div>-->
					</div>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_activities_calendar_table">
		<table>
			<tr class="_title">
				<td colspan="7">
					<div class="_title_side _title_side__left">
						<span class="js_link_prev _link" data-year="{@prev_year}" data-month="{@prev_month}">
							<xsl:text>← </xsl:text>
							<span class="_link_dashed">
								<xsl:call-template name="get_month">
									<xsl:with-param name="number" select="@prev_month"/>
									<xsl:with-param name="mode" select="'lower'"/>
									<xsl:with-param name="abbr" select="true()"/>
								</xsl:call-template>
							</span>
						</span>
					</div>
					<div class="_title_side _title_side__right">
						<span class="js_link_next _link" data-year="{@next_year}" data-month="{@next_month}">
							<span class="_link_dashed">
								<xsl:call-template name="get_month">
									<xsl:with-param name="number" select="@next_month"/>
									<xsl:with-param name="mode" select="'lower'"/>
									<xsl:with-param name="abbr" select="true()"/>
								</xsl:call-template>
							</span>
							<xsl:text> →</xsl:text>
						</span>
					</div>
					<div class="_title_center">
						<a href="{$activities_base_url}calendar/{@year}/{@month}/{$request/@query_string}">
							<xsl:call-template name="get_month">
								<xsl:with-param name="number" select="@month"/>
							</xsl:call-template>
						</a>
						<xsl:text> </xsl:text>
						<a href="{$activities_base_url}calendar/{@year}/{$request/@query_string}">
							<xsl:value-of select="@year"/>
						</a>
					</div>
				</td>
			</tr>
			<tr class="_head">
				<xsl:for-each select="week[1]/day">
					<td class="_head">
						<xsl:call-template name="get_week_day">
							<xsl:with-param name="number" select="@weekday"/>
							<xsl:with-param name="mode" select="'lower'"/>
							<xsl:with-param name="abbr" select="true()"/>
						</xsl:call-template>
					</td>
				</xsl:for-each>
			</tr>
			<xsl:for-each select="week">
				<tr class="_week">
					<xsl:if test="position() = last()">
						<xsl:attribute name="class">_week _week__last</xsl:attribute>
					</xsl:if>
					<xsl:for-each select="day">
						<td>
							<xsl:attribute name="class">
								<xsl:text>_day</xsl:text>
								<xsl:if test="@prev = 1 or @next = 1"> _day__alien</xsl:if>
								<xsl:if test="@number = /root/@day and @month = /root/@month and @year = /root/@year"> _day__current</xsl:if>
								<xsl:if test="@number = /root/activities/@day and @month = /root/activities/@month and @year = /root/activities/@year"> _day__selected</xsl:if>
							</xsl:attribute>
							<xsl:choose>
								<xsl:when test="@object_count &gt; 0">
									<a href="{$activities_base_url}calendar/{@year}/{@month}/{@number}/{$request/@query_string}">
										<xsl:value-of select="@number"/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@number"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
