<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_activities_head">
		<script type="text/javascript">
			var calendar_ajax_page = 'activities_calendar';
			var calendar_query_string = '<xsl:value-of select="$request/@query_string"/>';
			var post_vars = {
				stream_id: '<xsl:value-of select="$activities/@stream_id"/>',
				my: '<xsl:value-of select="$activities/@my"/>',
				selected_year: '<xsl:value-of select="$activities/@year"/>',
				selected_month: '<xsl:value-of select="$activities/@month"/>',
				selected_day: '<xsl:value-of select="$activities/@day"/>',
				activities_base_url: '<xsl:value-of select="$activities_base_url"/>'
			};
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/calendar.js?v3"/>
	</xsl:template>
	<xsl:template name="draw_activities_filters_show_draw_href_callback">
		<xsl:choose>
			<xsl:when test="@name = 'is-archive'">
				<xsl:value-of select="$activities_base_url"/>
			</xsl:when>
			<xsl:when test="@name = 'date-interval'">
				<xsl:value-of select="$activities_base_url"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_activities_filters_show_draw_title_callback">
		<xsl:choose>
			<xsl:when test="@name = 'is-archive'">
				<xsl:value-of select="php:function('trans', 'Archived items are shown')"/>
			</xsl:when>
			<xsl:when test="@name = 'date-interval'">
				<xsl:choose>
					<xsl:when test="$activities/@day != ''">
						<xsl:value-of select="php:function('trans', 'Started on')"/>
						<xsl:text> </xsl:text>
						<xsl:call-template name="get_date">
							<xsl:with-param name="datetime" select="$activities/@date"/>
							<xsl:with-param name="no_tags" select="true()"/>
							<xsl:with-param name="force_year" select="true()"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Started in')"/>
						<xsl:text> </xsl:text>
						<xsl:if test="$activities/@month != ''">
							<xsl:call-template name="get_month">
								<xsl:with-param name="number" select="$activities/@month"/>
							</xsl:call-template>
							<xsl:text> </xsl:text>
						</xsl:if>
						<xsl:value-of select="$activities/@year"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_activities_list">
		<!-- 'user' | 'admin' -->
		<xsl:param name="mode" select="'user'"/>
		<xsl:choose>
			<xsl:when test="@competence_id">
				<xsl:call-template name="draw_activity_competence_potential_notice"/>
			</xsl:when>
			<xsl:when test="$mode = 'admin'">
				<xsl:if test="not(@year != '')">
					<div class="submenu">
						<span class="_item">
							<xsl:choose>
								<xsl:when test="not(@is_archive = 1) and not(@is_no_date = 1)">
									<xsl:attribute name="class">_item _item__current</xsl:attribute>
									<xsl:value-of select="php:function('trans', 'Actual', 'ACTIVITIES')"/>
								</xsl:when>
								<xsl:otherwise>
									<a href="{$activities_url}{$request/@query_string}">
										<xsl:value-of select="php:function('trans', 'Actual', 'ACTIVITIES')"/>
									</a>
								</xsl:otherwise>
							</xsl:choose>
						</span>
						<xsl:text> </xsl:text>
						<span class="_item">
							<xsl:choose>
								<xsl:when test="@is_archive = 1">
									<xsl:attribute name="class">_item _item__current</xsl:attribute>
									<xsl:value-of select="php:function('trans', 'Archive')"/>
								</xsl:when>
								<xsl:otherwise>
									<a href="{$activities_url}archive/{$request/@query_string}">
										<xsl:value-of select="php:function('trans', 'Archive')"/>
									</a>
								</xsl:otherwise>
							</xsl:choose>
						</span>
						<xsl:text> </xsl:text>
						<span class="_item">
							<xsl:choose>
								<xsl:when test="@is_no_date = 1">
									<xsl:attribute name="class">_item _item__current</xsl:attribute>
									<xsl:value-of select="php:function('trans', 'No date')"/>
								</xsl:when>
								<xsl:otherwise>
									<a href="{$activities_url}no-date/{$request/@query_string}">
										<xsl:value-of select="php:function('trans', 'No date')"/>
									</a>
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</div>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$user/@id">
				<div class="submenu">
					<span class="_item">
						<xsl:choose>
							<xsl:when test="not(@my = 1)">
								<xsl:attribute name="class">_item _item__current</xsl:attribute>
								<xsl:value-of select="php:function('trans', 'All activities')"/>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$activities_url}{$request/@query_string}">
									<xsl:value-of select="php:function('trans', 'All activities')"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
					<xsl:text> </xsl:text>
					<span class="_item">
						<xsl:choose>
							<xsl:when test="@my = 1">
								<xsl:attribute name="class">_item _item__current</xsl:attribute>
								<xsl:value-of select="php:function('trans', 'My activities')"/>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$activities_url}my/{$request/@query_string}">
									<xsl:value-of select="php:function('trans', 'My activities')"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<div class="widgets">
			<xsl:call-template name="draw_activities_calendar"/>
			<xsl:call-template name="draw_activities_potential_stream_selector_widget"/>
			<xsl:apply-templates select="filters" mode="filters">
				<xsl:with-param name="exclude_name" select="'city'"/>
				<xsl:with-param name="exclude_name_2" select="'group'"/>
			</xsl:apply-templates>
		</div>
		<div class="head_dd_align_left" style="padding-bottom: 6px;">
			<p>
				<xsl:apply-templates select="filters" mode="filter_one">
					<xsl:with-param name="name" select="'city'"/>
					<xsl:with-param name="prefix_html">
						<div class="fa fa-map-marker"/>
						<xsl:text> </xsl:text>
					</xsl:with-param>
					<xsl:with-param name="class" select="'head_dd_for_right'"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="filters" mode="filter_one">
					<xsl:with-param name="name" select="'group'"/>
					<xsl:with-param name="prefix_html">
						<xsl:value-of select="php:function('trans', 'Group')"/>
						<xsl:text>: </xsl:text>
					</xsl:with-param>
				</xsl:apply-templates>
				<xsl:if test="not(@is_archive = 1) and not($mode = 'admin')">
					<div class="inline_filter head_dd_for_left">
						<a href="{$activities_base_url}archive/{$request/@query_string}">
							<xsl:value-of select="php:function('trans', 'Show archive')"/>
						</a>
					</div>
				</xsl:if>
			</p>
		</div>
		<xsl:apply-templates select="filters" mode="filters_show">
			<xsl:with-param name="exclude_name" select="'city'"/>
			<xsl:with-param name="exclude_name_2" select="'group'"/>
		</xsl:apply-templates>
		<div class="maxw800">
			<xsl:if test="not(activity)">
				<p>
					<xsl:choose>
						<xsl:when test="@is_archive = 1 or @year != ''">
							<xsl:value-of select="php:function('trans', 'No activities were found.')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'No oncoming activities were found.')"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="(filters/filter[@name != 'city' and @name != 'group']/@is_active = 1 or @competence_id) and not(@is_archive = 1)">
						<xsl:text> </xsl:text>
						<a href="{$activities_base_url}archive/{$request/@query_string}">
							<xsl:value-of select="php:function('trans', 'Try searching in archive')"/>
						</a>
						<xsl:text>.</xsl:text>
					</xsl:if>
					<xsl:if test="not(@stream_id != '') and not(@my = 1) and not(filters/filter[@is_active = 1]) and not(@year != '') and not(@competence_id)">
						<xsl:text> </xsl:text>
						<a href="{$main_prefix}/streams/">
							<xsl:value-of select="php:function('trans', 'Become a student in some streams', 'ACTIVITIES_JOIN_PROPOSAL')"/>
						</a>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', '– and their events will appear in your schedule.', 'ACTIVITIES_JOIN_PROPOSAL')"/>
					</xsl:if>
				</p>
			</xsl:if>
			<xsl:for-each select="activity">
				<xsl:choose>
					<xsl:when test="@metaactivity_id != '' and not(preceding-sibling::activity[@metaactivity_id = current()/@metaactivity_id])">
						<fieldset class="fieldset_list_item">
							<xsl:variable name="nodes_to_draw" select=". | following-sibling::activity[@metaactivity_id = current()/@metaactivity_id]"/>
							<legend>
								<xsl:value-of select="/root/activity_short[@id = current()/@id]/@metaactivity_title"/>
								<xsl:text> (</xsl:text>
								<xsl:value-of select="php:function('trans', 'event group')"/>
								<xsl:text>)</xsl:text>
							</legend>
							<xsl:for-each select="$nodes_to_draw">
								<xsl:call-template name="draw_activities_item">
									<xsl:with-param name="mode" select="$mode"/>
								</xsl:call-template>
							</xsl:for-each>
						</fieldset>
					</xsl:when>
					<xsl:when test="@metaactivity_id != ''"/>
					<xsl:otherwise>
						<xsl:call-template name="draw_activities_item">
							<xsl:with-param name="mode" select="$mode"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:apply-templates select="pages"/>
		</div>
	</xsl:template>
	<xsl:template name="draw_activities_item">
		<xsl:param name="mode"/>
		<xsl:variable name="context" select="."/>
		<xsl:for-each select="/root/activity_short[@id = current()/@id]">
			<xsl:variable name="stream" select="/root/stream_short[@id = $context/@stream_id][1]"/>
			<xsl:variable name="access" select="/root/stream_access[@stream_id = $context/@stream_id][1]"/>
			<div class="list_item">
				<div class="list_item_title">
					<xsl:if test="not($context/../@stream_id = $context/@stream_id)">
						<span class="smaller">
							<a href="{$stream/@url}">
								<xsl:value-of select="$stream/@title_short"/>
							</a>
						</span>
						<span class="arrow"> → </span>
					</xsl:if>
					<span>
						<xsl:if test="$context/@potential != '' or $access/@has_moderator_rights = 1">
							<xsl:attribute name="class">for_icon_right</xsl:attribute>
						</xsl:if>
						<xsl:variable name="suffix">
							<xsl:choose>
								<xsl:when test="$context/../@competence_id">
									<xsl:value-of select="concat('?competence=', $context/../@competence_id)"/>
								</xsl:when>
								<xsl:when test="$context/../@potential_stream_id != $context/@stream_id">
									<xsl:value-of select="concat('?potential-stream=', $context/../@potential_stream_id)"/>
								</xsl:when>
							</xsl:choose>
						</xsl:variable>
						<a>
							<xsl:attribute name="href">
								<xsl:choose>
									<xsl:when test="$mode = 'admin'">
										<xsl:value-of select="concat($stream/@url, 'admin/activities/', @id, '/', $suffix)"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="concat($stream/@url, 'activities/', @id, '/', $suffix)"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
							<xsl:value-of select="@title"/>
						</a>
					</span>
					<xsl:if test="$context/@potential != ''">
						<xsl:choose>
							<xsl:when test="$context/../@competence_id">
								<xsl:call-template name="draw_activity_competence_potential">
									<xsl:with-param name="potential" select="$context/@potential"/>
									<xsl:with-param name="class">
										<xsl:text>list_item_title_regular_text</xsl:text>
										<xsl:if test="$access/@has_moderator_rights = 1"> for_icon_right</xsl:if>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="draw_activity_participant_potential">
									<xsl:with-param name="potential" select="$context/@potential"/>
									<xsl:with-param name="class">
										<xsl:text>list_item_title_regular_text</xsl:text>
										<xsl:if test="$access/@has_moderator_rights = 1"> for_icon_right</xsl:if>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$access/@has_moderator_rights = 1 and not($mode = 'admin')">
						<a href="{$stream/@url}admin/activities/{@id}/">
							<i class="red fa fa-gear"/>
						</a>
					</xsl:if>
				</div>
				<div class="props">
					<div class="prop">
						<span class="prop_for_right">
							<xsl:call-template name="draw_activity_prop_date"/>
							<xsl:text> </xsl:text>
						</span>
						<span class="nobr">
							<xsl:call-template name="draw_activity_prop_city"/>
						</span>
					</div>
					<div class="prop">
						<xsl:variable name="participant_count">
							<xsl:choose>
								<xsl:when test="$context/@date_type = 'longterm'">
									<xsl:value-of select="$context/@longterm_graded_user_count_calc"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$context/@accepted_participant_count_calc"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<span class="prop_for_right">
							<span class="fa fa-users"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$participant_count"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case_lang">
								<xsl:with-param name="number" select="$participant_count"/>
								<xsl:with-param name="word" select="'participant'"/>
							</xsl:call-template>
							<xsl:if test="$context/@premoderation_request_count_calc &gt; 0">
								<xsl:text> (</xsl:text>
								<span class="red nobr">
									<b>
										<xsl:value-of select="$context/@premoderation_request_count_calc"/>
									</b>
									<xsl:text> </xsl:text>
									<xsl:call-template name="count_case_lang">
										<xsl:with-param name="number" select="$context/@premoderation_request_count_calc"/>
										<xsl:with-param name="word" select="'request'"/>
									</xsl:call-template>
								</span>
								<xsl:text>)</xsl:text>
							</xsl:if>
						</span>
						<xsl:variable name="mark_count">
							<xsl:choose>
								<xsl:when test="$context/@date_type = 'longterm'">
									<xsl:value-of select="$context/@longterm_mark_count_calc"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$context/@mark_count_calc"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:if test="$mark_count &gt; 0">
							<xsl:text> </xsl:text>
							<span class="fa fa-check-circle-o"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$mark_count"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case_lang">
								<xsl:with-param name="number" select="$mark_count"/>
								<xsl:with-param name="word" select="'mark'"/>
							</xsl:call-template>
							<xsl:if test="$context/@unset_mark_count_calc &gt; 0">
								<xsl:text> (</xsl:text>
								<span class="red nobr">
									<b>
										<xsl:value-of select="$context/@unset_mark_count_calc"/>
									</b>
									<xsl:text> </xsl:text>
									<xsl:call-template name="count_case_lang">
										<xsl:with-param name="number" select="$context/@unset_mark_count_calc"/>
										<xsl:with-param name="word" select="'UNSET MARKS'"/>
									</xsl:call-template>
								</span>
								<xsl:text>)</xsl:text>
							</xsl:if>
						</xsl:if>
					</div>
					<xsl:if test="$context/@status != ''">
						<div class="prop">
							<xsl:call-template name="draw_activity_participant_status">
								<xsl:with-param name="status" select="$context/@status"/>
								<xsl:with-param name="mode" select="'full'"/>
								<xsl:with-param name="no_default" select="true()"/>
								<xsl:with-param name="you" select="true()"/>
							</xsl:call-template>
						</div>
					</xsl:if>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_activities_potential_stream_selector_widget">
		<xsl:if test="/root/potential_math_user_streams">
			<div class="widget">
				<div class="widget_head">
					<i class="widget_head_icon fa fa-bolt"/>
					<span class="widget_head_title">
						<xsl:value-of select="php:function('trans', 'Potential')"/>
					</span>
					<i class="widget_head_expand_icon fa"/>
				</div>
				<div class="widget_content widget_pad">
					<xsl:call-template name="draw_activity_potential_stream_selector">
						<xsl:with-param name="is_stream_context" select="@stream_id"/>
						<xsl:with-param name="field_class" select="'field2__padded'"/>
					</xsl:call-template>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
