<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_tree_head">
		<p class="note smaller right maxw800">
			<!--<span class="dashed clickable" id="tree-collapse">collapse all</span>
			<span> | </span>
			<span class="dashed clickable" id="tree-groups">show only groups</span>
			<span> | </span>
			<span class="dashed clickable" id="tree-expand">expand all</span>-->
			<span class="dashed clickable" id="tree-groups">
				<xsl:value-of select="php:function('trans', 'collapse tree')"/>
			</span>
			<span> | </span>
			<span class="dashed clickable" id="tree-expand">
				<xsl:value-of select="php:function('trans', 'expand')"/>
			</span>
		</p>
	</xsl:template>
	<xsl:template name="draw_tree_competence_group">
		<xsl:param name="competences" select="/root/competence_set_competences//competence"/>
		<xsl:param name="focus_competences" select="/"/>
		<xsl:param name="competence_is_clickable" select="true()"/>
		<xsl:param name="draw_checkboxes" select="false()"/>
		<xsl:param name="use_competence_title_template" select="false()"/>
		<xsl:param name="parent_node" select="/root/competence_set_competences"/>
		<xsl:param name="level" select="0"/>
		<div>
			<xsl:attribute name="class">
				<xsl:text>js_tree</xsl:text>
				<xsl:if test="$level = 0"> maxw800</xsl:if>
			</xsl:attribute>
			<xsl:for-each select="$parent_node/group[.//competence[@id = $competences/@id]]">
				<xsl:variable name="group_id" select="@id"/>
				<table class="table table__no_outline w100p">
					<tr>
						<td class="cell_h3 pr10" colspan="2">
							<h3>
								<span class="js_tree_group_head clickable dashed" data-group-id="{@id}">
									<xsl:value-of select="@title"/>
								</span>
							</h3>
						</td>
						<xsl:if test="$level = 0 and position() = 1">
							<xsl:apply-templates mode="tree_right_th" select="."/>
						</xsl:if>
					</tr>
				</table>
				<div class="js_tree_group_content js_tree_group_{$group_id}_content">
					<xsl:if test="competence[@id = $competences/@id]">
						<table class="table table__no_th p w100p">
							<xsl:for-each select="competence[@id = $competences/@id]">
								<xsl:variable name="mark_details_custom_url">
									<xsl:apply-templates mode="tree_competence_custom_url" select="."/>
								</xsl:variable>
								<xsl:variable name="mark_details_url">
									<xsl:choose>
										<xsl:when test="$mark_details_custom_url != ''">
											<xsl:value-of select="$mark_details_custom_url"/>
										</xsl:when>
										<xsl:when test="$competence_is_clickable">
											<xsl:value-of select="$current_user/@url"/>
											<xsl:text>competences/</xsl:text>
											<xsl:value-of select="$current_stream/@name"/>
											<xsl:text>/</xsl:text>
											<xsl:value-of select="@id"/>
											<xsl:text>/</xsl:text>
											<xsl:value-of select="$request/@query_string"/>
										</xsl:when>
									</xsl:choose>
								</xsl:variable>
								<tr class="js_tree_competence" data-competence-id="{@id}" data-group-id="{$group_id}">
									<xsl:if test="@restrictions != ''">
										<xsl:attribute name="data-restrictions">
											<xsl:value-of select="@restrictions"/>
										</xsl:attribute>
									</xsl:if>
									<td class="cell_competence w100p">
										<xsl:if test="$focus_competences[@id = current()/@id]">
											<!-- @future -->
										</xsl:if>
										<xsl:apply-templates mode="tree_competence_td_top" select="."/>
										<div class="_cell_left">
											<xsl:if test="$draw_checkboxes">
												<input type="checkbox" id="competence-{@id}" name="competence-{@id}" value="1">
													<xsl:choose>
														<xsl:when test="$pass_info/vars/var[@name = concat('competence-', current()/@id)]">
															<xsl:attribute name="checked">checked</xsl:attribute>
														</xsl:when>
														<xsl:when test="$pass_info"/>
														<xsl:when test="$focus_competences[@id = current()/@id]">
															<xsl:attribute name="checked">checked</xsl:attribute>
														</xsl:when>
													</xsl:choose>
												</input>
											</xsl:if>
											<xsl:variable name="tree_competence_clickable">
												<xsl:apply-templates mode="tree_competence_clickable" select="."/>
											</xsl:variable>
											<span class="_title">
												<xsl:choose>
													<xsl:when test="$competence_is_clickable and $tree_competence_clickable = 1">
														<a href="{$mark_details_url}">
															<xsl:call-template name="draw_competence"/>
														</a>
													</xsl:when>
													<xsl:when test="$draw_checkboxes">
														<label for="competence-{@id}">
															<xsl:call-template name="draw_competence"/>
														</label>
													</xsl:when>
													<xsl:when test="$use_competence_title_template">
														<xsl:apply-templates mode="tree_competence_title" select="."/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:call-template name="draw_competence"/>
													</xsl:otherwise>
												</xsl:choose>
											</span>
										</div>
										<xsl:apply-templates mode="tree_competence_td" select="."/>
									</td>
									<xsl:apply-templates mode="tree_right_td" select=".">
										<xsl:with-param name="mark_details_url" select="$mark_details_url"/>
									</xsl:apply-templates>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
					<div class="js_tree_group_children js_tree_group_{@id}_children" data-group-id="{@id}" style="padding-left: 21px;">
						<xsl:if test="@restrictions != ''">
							<xsl:attribute name="data-restrictions">
								<xsl:value-of select="@restrictions"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:call-template name="draw_tree_competence_group">
							<xsl:with-param name="competences" select="$competences"/>
							<xsl:with-param name="focus_competences" select="$focus_competences"/>
							<xsl:with-param name="competence_is_clickable" select="$competence_is_clickable"/>
							<xsl:with-param name="draw_checkboxes" select="$draw_checkboxes"/>
							<xsl:with-param name="parent_node" select="."/>
							<xsl:with-param name="level" select="$level + 1"/>
						</xsl:call-template>
					</div>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group"/>
	<xsl:template mode="tree_competence_clickable" match="competence">1</xsl:template>
	<xsl:template mode="tree_competence_td_top" match="competence"/>
	<xsl:template mode="tree_competence_td" match="competence"/>
	<xsl:template mode="tree_right_td" match="competence">
		<xsl:param name="mark_details_url"/>
	</xsl:template>
</xsl:stylesheet>
