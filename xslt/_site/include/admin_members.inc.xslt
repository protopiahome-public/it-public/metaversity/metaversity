<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_admin_members">
		<xsl:param name="save_ctrl"/>
		<xsl:param name="item_id"/>
		<xsl:param name="aux_params"/>
		<xsl:param name="bulk_operations_threshold" select="2"/>
		<xsl:variable name="user_count" select="count(user)"/>
		<xsl:call-template name="draw_members_not_found"/>
		<xsl:if test="user">
			<p>
				<xsl:value-of select="php:function('trans', 'Users shown:')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text>&#8211;</xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'of')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>
			<table class="table p">
				<tr>
					<xsl:if test="$user_count &gt;= $bulk_operations_threshold">
						<th>
							<input type="checkbox" class="js_check_all ctrl ctrl__checkbox" data-check-all-class="js_checkbox" name="all" value="1"/>
						</th>
					</xsl:if>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'name'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Name')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'time'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Joined', 'HEADER')"/>
						<xsl:with-param name="class" select="'p6 show1000 nobr'"/>
					</xsl:call-template>
					<xsl:apply-templates mode="draw_members_aux_th" select="."/>
					<th/>
				</tr>
				<xsl:for-each select="user">
					<xsl:variable name="user_context" select="."/>
					<tr>
						<xsl:if test="$user_count &gt;= $bulk_operations_threshold">
							<td class="pr6">
								<input type="checkbox" class="js_checkbox ctrl ctrl__checkbox" name="user-{@id}" value="1"/>
							</td>
						</xsl:if>
						<xsl:for-each select="/root/user_short[@id = current()/@id][1]">
							<td class="cell_user">
								<div class="_img">
									<a href="{$users_prefix}/{@login}/">
										<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
									</a>
								</div>
								<div class="_title">
									<a href="{$users_prefix}/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
									<span class="_login">
										<xsl:text> (</xsl:text>
										<xsl:value-of select="@login"/>
										<xsl:text>)</xsl:text>
									</span>
								</div>
								<div class="_city">
									<xsl:value-of select="@city_title"/>
								</div>
								<div class="_lines">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Joined', 'HEADER')"/>
										<xsl:with-param name="text">
											<xsl:call-template name="get_time">
												<xsl:with-param name="datetime" select="$user_context/@add_time"/>
											</xsl:call-template>
										</xsl:with-param>
										<xsl:with-param name="class" select="'_line hide1000'"/>
									</xsl:call-template>
									<xsl:apply-templates mode="draw_members_aux_lines" select=".">
										<xsl:with-param name="user_context" select="$user_context"/>
									</xsl:apply-templates>
								</div>
							</td>
						</xsl:for-each>
						<td class="cell_text cell_text__smaller p6 show1000 nobr">
							<xsl:call-template name="get_time">
								<xsl:with-param name="datetime" select="$user_context/@add_time"/>
							</xsl:call-template>
						</td>
						<xsl:apply-templates mode="draw_members_aux_td" select=".">
							<xsl:with-param name="user_context" select="$user_context"/>
						</xsl:apply-templates>
						<td class="cell_btn">
							<xsl:for-each select="/root/user_short[@id = current()/@id][1]">
								<a class="js_btn_delete _btn" data-user-id="{@id}" data-user-login="{@login}" href="javascript:void(0);" title="{php:function('trans', 'Unjoin')}">
									<i class="_btn_icon fa red fa-times"/>
								</a>
							</xsl:for-each>
						</td>
					</tr>
				</xsl:for-each>
			</table>
			<input class="js_input_user_id" type="hidden" name="user_id" value="0"/>
			<xsl:if test="$user_count &gt;= $bulk_operations_threshold">
				<div class="p">
					<button class="js_btn_bulk_delete ctrl ctrl__button ctrl__button__red" name="decline-all">
						<xsl:value-of select="php:function('trans', 'Unjoin all checked')"/>
					</button>
				</div>
			</xsl:if>
			<div class="js_dialog_confirm hide" data-title="{php:function('trans', 'Confirmation')}" data-button-title="{php:function('trans', 'Confirm')}">
				<p>
					<xsl:value-of select="php:function('trans', 'User', 'ADMIN_MEMBERS_REMOVE_CONFIRM')"/>
					<xsl:text> </xsl:text>
					<b class="js_dialog_confirm_login">???</b>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'will be removed from the list of students.', 'ADMIN_MEMBERS_REMOVE_CONFIRM')"/>
				</p>
			</div>
			<div class="js_dialog_bulk_confirm hide" data-title="{php:function('trans', 'Confirmation')}" data-button-title="{php:function('trans', 'Confirm')}">
				<p>
					<xsl:value-of select="php:function('trans', 'Users', 'ADMIN_MEMBERS_BULK_REMOVE_CONFIRM')"/>
					<xsl:text> (</xsl:text>
					<b class="js_dialog_bulk_confirm_count">???</b>
					<xsl:text>) </xsl:text>
					<xsl:value-of select="php:function('trans', 'will be removed from the list of students.', 'ADMIN_MEMBERS_BULK_REMOVE_CONFIRM')"/>
				</p>
			</div>
		</xsl:if>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
</xsl:stylesheet>
