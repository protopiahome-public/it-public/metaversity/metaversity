<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="draw_stream_admin_competence_stat_legend">
		<xsl:choose>
			<xsl:when test="$lang = 'ru'">
				<p>
					<strong>Значание чисел X (Y):</strong> X — сумма весов, Y — количество. Например, если компетенция использовалась в 3 позициях с весами 1, 2, 2, то отображаться будет 5 (3). Веса определяются при редактировании позиции или роли — например, &#171;Косвенно развивает&#187; — это 1, &#171;Развивает&#187; — 2, и проч. Размер графика пропорционален X.</p>
				<p>
					<span class="legend_bar legend_bar__stat_position"/> — позиции (сумма по всем позициям без учёта их использования).
				</p>
				<p>
					<span class="legend_bar legend_bar__stat_format"/> — форматы (сумма по всем форматам без учёта их использования).
				</p>
				<p>
					<span class="legend_bar legend_bar__stat_activity"/> — расписание (суммируем форматы пособытийно).
				</p>
				<p>
					<span class="legend_bar legend_bar__stat_mark"/> — оценки.
				</p>
				<p>
					<strong>Нажмите на график или число, чтобы посмотреть детальный список позиций или ролей.</strong> В открывшемся окне в скобках у каждого пункта будет указываться вес.
				</p>
				<p>
					<strong>Что можно улучшить.</strong> 1) Для статистики по форматам и событиям суммирование сейчас производится по ролям. В результате, если компетенция используется во многих ролях одного-единственного формата, её рейтинг всё равно будет большим, что не вполне честно: как правило, роли одного формата взаимоисключают друг друга, и реальных возможностей по развитию у студента становится меньше. 2) В статистике не учитываются реальные потребности согласно позициям, выбранным студентами — суммируются все имеющиеся позиции.</p>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<strong>Meaning of X (Y):</strong> X is a sum of weights, Y is count. For example: if a competence is used in 3 positions with weights 1, 2, 2 – 5 (3) will be shown. Weights are defined in the position or role editing form — i.e., “Improves indirectly” is 1, “Improves” is 2, etc. The bar size is proportional to X.</p>
				<p>
					<span class="legend_bar legend_bar__stat_position"/> — positions (sum over positions without taking into consideration their usage frequency).
				</p>
				<p>
					<span class="legend_bar legend_bar__stat_format"/> — formats (sum over formats without taking into consideration their usage frequency).
				</p>
				<p>
					<span class="legend_bar legend_bar__stat_activity"/> — schedule (formats are summed up for every activity).
				</p>
				<p>
					<span class="legend_bar legend_bar__stat_mark"/> — marks.
				</p>
				<p>
					<strong>Click on a bar or a number to see a detailed list of positions and formats.</strong> Weights will be shown in brackets in the opened window.
				</p>
				<p>
					<strong>What can be done better.</strong> 1) For stats over formats and activities the summation is made over roles. As a result, if a competence is used in many roles of only one format, its rating will be big – which is not correct because different roles of one format uaually exclude each other and a student receives less improvement possibilities. 2) The stats do not take into consideration how often the positions are chosen by students – summation is made over all available positions instead.</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
