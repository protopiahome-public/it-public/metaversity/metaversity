<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_admin_moderators">
		<xsl:param name="save_ctrl"/>
		<xsl:param name="aux_params"/>
		<xsl:choose>
			<xsl:when test="$pass_info/error[@name = 'USER_NOT_FOUND']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'User', 'ADMIN_ADMINS_USER_NOT_FOUND')"/>
						<xsl:text> </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/error[@name = 'USER_NOT_FOUND']"/>
						</strong>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'does not exist. Please check that the login is correct.', 'ADMIN_ADMINS_USER_NOT_FOUND')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'ALREADY_MODERATOR']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'User', 'ADMIN_MODERATORS_ALREADY_MODERATOR')"/>
						<xsl:text> </xsl:text>
						<xsl:if test="$pass_info/error[@name = 'ALREADY_MODERATOR'] != ''">
							<strong>
								<xsl:value-of select="$pass_info/error[@name = 'ALREADY_MODERATOR']"/>
							</strong>
							<xsl:text> </xsl:text>
						</xsl:if>
						<xsl:value-of select="php:function('trans', 'is already a moderator.', 'ADMIN_MODERATORS_ALREADY_MODERATOR')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'IS_ADMIN']">
				<div class="error">
					<span>
						<strong>
							<xsl:value-of select="$pass_info/error[@name = 'IS_ADMIN']"/>
						</strong>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'is already an administrator and has full rights for moderation.', 'ADMIN_MODERATORS_IS_ADMIN')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'MODERATOR_ADDED']">
				<div class="info">
					<span>
						<xsl:value-of select="php:function('trans', 'Moderator is added:', 'ADMIN_MODERATORS_MODERATOR_ADDED')"/>
						<xsl:text> </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/info[@name = 'MODERATOR_ADDED']"/>
						</strong>
						<xsl:text>.</xsl:text>
						<xsl:if test="$pass_info/info[@name = 'WAS_NOT_MEMBER']">
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'This user was not a student but the action was completed because you have rights for system administration.', 'ADMIN_ADMINS_ADMIN_ADDED')"/>
						</xsl:if>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'RIGHTS_CHANGED']">
				<div class="info">
					<span>
						<xsl:value-of select="php:function('trans', 'The rights of moderators are changed.', 'ADMIN_MODERATORS_RIGHTS_CHANGED')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'NOT_MEMBER']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'User', 'ADMIN_ADMINS_NOT_MEMBER')"/>
						<xsl:text> </xsl:text>
						<strong>
							<xsl:value-of select="$pass_info/error[@name = 'NOT_MEMBER']"/>
						</strong>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'is not a student. Please ask him to join this stream.', 'ADMIN_ADMINS_NOT_MEMBER')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'MODERATOR_DELETED']">
				<div class="info">
					<span>
						<strong>
							<xsl:value-of select="$pass_info/info[@name = 'MODERATOR_DELETED']"/>
						</strong>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'is no longer a moderator.', 'ADMIN_MODERATORS_MODERATOR_DELETED')"/>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<form class="p" action="{$save_prefix}/{$save_ctrl}/" method="post">
			<input type="text" class="ctrl ctrl__text ctrl__for_right" name="user_login" value="" placeholder="{php:function('trans', 'Login')}">
				<xsl:if test="$pass_info/error and $pass_info/vars/var[@name = 'action'] = 'add'">
					<xsl:attribute name="value">
						<xsl:value-of select="$pass_info/vars/var[@name = 'user_login']"/>
					</xsl:attribute>
				</xsl:if>
			</input>
			<button class="ctrl ctrl__button ctrl__button__green">
				<xsl:value-of select="php:function('trans', 'Add')"/>
			</button>
			<input type="hidden" name="action" value="add"/>
			<xsl:copy-of select="$aux_params"/>
		</form>
		<div class="js_dialog_confirm hide" data-title="{php:function('trans', 'Confirmation')}" data-button-title="{php:function('trans', 'Confirm')}">
			<p>
				<xsl:value-of select="php:function('trans', 'Moderator', 'ADMIN_MODERATORS_REMOVE_CONFIRM')"/>
				<xsl:text> </xsl:text>
				<b class="js_dialog_confirm_login">???</b>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'will be deleted.', 'ADMIN_MODERATORS_REMOVE_CONFIRM')"/>
			</p>
		</div>
		<xsl:variable name="module" select="."/>
		<xsl:if test="not(moderator)">
			<p>
				<xsl:value-of select="php:function('trans', 'Moderators were not found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="moderator">
			<form action="{$save_prefix}/{$save_ctrl}/" method="post">
				<input type="hidden" name="action" value="edit"/>
				<xsl:copy-of select="$aux_params"/>
				<table class="table table__no_th p">
					<xsl:call-template name="draw_moderators_rights_headers"/>
					<xsl:for-each select="moderator">
						<xsl:variable name="current_user_rights" select="current()"/>
						<tr>
							<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
								<xsl:if test="@login = $pass_info/info[@name = 'MODERATOR_ADDED'] and count($module/moderator) &gt; 1">
									<xsl:attribute name="class">hl-</xsl:attribute>
								</xsl:if>
								<td class="cell_user pr6">
									<div class="_img">
										<a href="{$users_prefix}/{@login}/">
											<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
										</a>
									</div>
									<div class="_title">
										<a href="{$users_prefix}/{@login}/">
											<xsl:value-of select="@visible_name"/>
										</a>
										<span class="_login">
											<xsl:text> (</xsl:text>
											<xsl:value-of select="@login"/>
											<xsl:text>)</xsl:text>
										</span>
									</div>
									<div class="_city">
										<xsl:value-of select="@city_title"/>
									</div>
								</td>
							</xsl:for-each>
							<xsl:call-template name="draw_moderators_rights_checkboxes"/>
							<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
								<td class="cell_btn">
									<form action="{$save_prefix}/{$save_ctrl}/" method="post">
										<a class="js_btn_delete _btn" href="javascript:void(0);" data-user-id="{@id}" data-user-login="{@login}">
											<i class="_btn_icon fa red fa-times"/>
										</a>
										<input type="hidden" name="retpath" value="{$module_url}"/>
										<input type="hidden" name="action" value="delete"/>
										<input type="hidden" name="user_id" value="{@id}"/>
										<xsl:copy-of select="$aux_params"/>
									</form>
								</td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
				<!--<p>
					<button class="ctrl ctrl__button ctrl__button__big">
						<xsl:value-of select="php:function('trans', 'Save changes')"/>
					</button>
				</p>-->
			</form>
		</xsl:if>
		<xsl:for-each select="moderator">
			<xsl:for-each select="/root/user_short[@id = current()/@user_id]">
				<form id="js_form_delete_{@id}" action="{$save_prefix}/{$save_ctrl}/" method="post">
					<input type="hidden" name="retpath" value="{$module_url}"/>
					<input type="hidden" name="action" value="delete"/>
					<input type="hidden" name="user_id" value="{@id}"/>
					<xsl:copy-of select="$aux_params"/>
				</form>
			</xsl:for-each>
		</xsl:for-each>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
</xsl:stylesheet>
