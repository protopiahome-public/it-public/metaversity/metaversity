<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_rate_edit">
		<xsl:param name="draw_button" select="true()"/>
		<xsl:param name="legend" select="''"/>
		<xsl:param name="descr" select="''"/>
		<fieldset class="fieldset_text maxw800">
			<xsl:if test="$legend != ''">
				<legend class="toggle">
					<xsl:value-of select="$legend"/>
				</legend>
			</xsl:if>
			<xsl:copy-of select="$descr"/>
			<xsl:call-template name="draw_tree_head"/>
			<xsl:call-template name="draw_tree_competence_group">
				<xsl:with-param name="competence_is_clickable" select="false()"/>
			</xsl:call-template>
		</fieldset>
		<xsl:if test="$draw_button">
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Save', 'VERB')"/>
			</button>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="right show600">
			<xsl:value-of select="php:function('trans', 'Importance')"/>
		</th>
	</xsl:template>
	<xsl:template mode="tree_competence_td_top" match="competence">
		<xsl:attribute name="class">cell_competence cell_competence__2col</xsl:attribute>
	</xsl:template>
	<xsl:variable name="rate_competences" select="/root/rate[1]/competence"/>
	<xsl:variable name="rate_options" select="/root/rate[1]/options/option"/>
	<xsl:template mode="tree_competence_td" match="competence">
		<div class="_cell_right">
			<select name="rates[{current()/@id}]">
				<xsl:variable name="current_rate" select="current()"/>
				<xsl:for-each select="$rate_options">
					<option value="{@mark}">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = 'rates']/item[@index = $current_rate/@id] = @mark">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:when>
							<xsl:when test="$pass_info/vars/var[@name = 'rates']/item[@index = $current_rate/@id]"/>
							<xsl:when test="$rate_competences[@id = $current_rate/@id]/@mark = current()/@mark">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:when>
						</xsl:choose>
						<xsl:value-of select="@mark_title"/>
					</option>
				</xsl:for-each>
			</select>
		</div>
	</xsl:template>
</xsl:stylesheet>
