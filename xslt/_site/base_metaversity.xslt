<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_competence_title">
		<xsl:param name="competence" select="."/>
		<xsl:param name="red_number" select="false()"/>
		<xsl:choose>
			<xsl:when test="$red_number">
				<span class="red">
					<xsl:value-of select="$competence/@id"/>
					<xsl:text>.</xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$competence/@id"/>
				<xsl:text>.</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:variable name="title">
			<xsl:choose>
				<xsl:when test="$competence/@title">
					<xsl:value-of select="$competence/@title"/>
				</xsl:when>
				<xsl:when test="/root/competence_title[@id = $competence/@id]/@title">
					<xsl:value-of select="/root/competence_title[@id = $competence/@id]/@title"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/root/competence_set_competences//competence[@id = $competence/@id]/@title"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$title"/>
		<xsl:if test="substring($title, string-length($title), 1) != '.'">.</xsl:if>
	</xsl:template>
	<xsl:template name="draw_competence">
		<xsl:param name="id" select="@id"/>
		<xsl:param name="title" select="@title"/>
		<xsl:param name="red_number" select="false()"/>
		<xsl:param name="dot" select="false()"/>
		<xsl:choose>
			<xsl:when test="$red_number">
				<span class="red">
					<xsl:value-of select="$id"/>
					<xsl:text>.</xsl:text>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$id"/>
				<xsl:text>.</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:variable name="_title">
			<xsl:choose>
				<xsl:when test="$title != ''">
					<xsl:value-of select="$title"/>
				</xsl:when>
				<xsl:when test="/root/competence_title[@id = $id]/@title">
					<xsl:value-of select="/root/competence_title[@id = $id]/@title"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/root/competence_set_competences//competence[@id = $id]/@title"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$_title"/>
		<xsl:if test="$dot">
			<xsl:if test="substring($_title, string-length($_title), 1) != '.'">.</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_mark">
		<xsl:param name="competence_id" select="@competence_id"/>
		<xsl:param name="weight" select="false()"/>
		<span class="nobr">
			<xsl:choose>
				<xsl:when test="@mark = 'ACCESS_DENIED'">
					<span class="mark mark__access_denied">
						<i class="fa fa-eye-slash"/>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="mark">
						<xsl:attribute name="class">
							<xsl:text>mark</xsl:text>
							<xsl:if test="@is_important = 0"> mark__unimportant</xsl:if>
							<xsl:value-of select="concat(' mark__', @mark)"/>
							<xsl:if test="$competence_id"> mark__competence</xsl:if>
						</xsl:attribute>
						<xsl:value-of select="@mark"/>
					</span>
					<xsl:if test="$weight = 'hi'">
						<span class="mark_x" title="{php:function('trans', 'Activity weight')}">×2.0</span>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</span>
	</xsl:template>
	<xsl:template name="draw_result_hl">
		<xsl:param name="result" select="@result"/>
		<xsl:param name="result_type" select="@result_type"/>
		<xsl:variable name="_result">
			<xsl:choose>
				<xsl:when test="$result != ''">
					<xsl:value-of select="$result"/>
				</xsl:when>
				<xsl:otherwise>0.0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<span class="result_hl">
			<xsl:if test="$_result = 0.0">
				<xsl:attribute name="class">result_hl result_hl__zero</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="style">
				<xsl:value-of select="concat('background-color: rgb(', round(255 - 240 * $_result div 3), ', ', round(255 - 72 * $_result div 3), ', ', round(255 - 80 * $_result div 3), ');')"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$result_type = 'integral'">
					<xsl:attribute name="title">
						<xsl:value-of select="php:function('trans', 'Integral result (calculated based on other competences)')"/>
					</xsl:attribute>
				</xsl:when>
			</xsl:choose>
			<xsl:if test="$result_type = 'integral'">∫&#160;</xsl:if>
			<xsl:value-of select="$result"/>
		</span>
	</xsl:template>
	<xsl:template name="draw_marks_legend_digits_meanings">
		<p>
			<b>0</b>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'applied but did not come or did not cope')"/>
			<xsl:text>.</xsl:text>
		</p>
		<p>
			<b>1</b>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'hardly coped / help was required')"/>
			<xsl:text>.</xsl:text>
		</p>
		<p>
			<b>2</b>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'coped well / everything was just normal')"/>
			<xsl:text>.</xsl:text>
		</p>
		<p>
			<b>3</b>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'better than expected / brilliantly coped with a diffucult situation')"/>
			<xsl:text>.</xsl:text>
		</p>
	</xsl:template>
	<xsl:template name="draw_stream_status_text">
		<xsl:param name="you" select="true()"/>
		<xsl:choose>
			<xsl:when test="$current_stream_access/@status = 'pretender'">
				<xsl:choose>
					<xsl:when test="$you">
						<xsl:value-of select="php:function('trans', 'You are on moderation')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'On moderation')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$current_stream_access/@status = 'member'">
				<xsl:choose>
					<xsl:when test="$you">
						<xsl:value-of select="php:function('trans', 'You are a student')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Student')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$current_stream_access/@status = 'admin'">
				<xsl:choose>
					<xsl:when test="$you">
						<xsl:value-of select="php:function('trans', 'You are an administrator')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Administrator')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$current_stream_access/@status = 'moderator'">
				<xsl:choose>
					<xsl:when test="$you">
						<xsl:value-of select="php:function('trans', 'You are a moderator')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Moderator')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$current_stream_access/@is_member = 1 or $current_stream_access/@is_pretender = 1"/>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$you">
						<xsl:value-of select="php:function('trans', 'You are not a student')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Not a student')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="draw_stream_status">
		<xsl:param name="status" select="@status"/>
		<xsl:param name="stream_url" select="@url"/>
		<xsl:param name="url" select="concat($stream_url, 'status/')"/>
		<xsl:param name="force_url" select="false()"/>
		<xsl:param name="you" select="true()"/>
		<xsl:param name="mode" select="'default'"/>
		<xsl:choose>
			<xsl:when test="$status = 'admin'">
				<span class="fa fa-key fa-key-admin"/>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="$you">
						<a href="{$url}">
							<xsl:value-of select="php:function('trans', 'You are an administrator')"/>
						</a>
					</xsl:when>
					<xsl:when test="$force_url">
						<a href="{$url}">
							<xsl:value-of select="php:function('trans', 'Administrator')"/>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Administrator')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$status = 'moderator'">
				<span class="fa fa-key fa-key-moderator"/>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="$you">
						<a href="{$url}">
							<xsl:value-of select="php:function('trans', 'You are a moderator')"/>
						</a>
					</xsl:when>
					<xsl:when test="$force_url">
						<a href="{$url}">
							<xsl:value-of select="php:function('trans', 'Moderator')"/>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Moderator')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$status = 'member'">
				<span class="fa fa-check"/>
				<xsl:text> </xsl:text>
				<span class="status_accepted">
					<xsl:choose>
						<xsl:when test="$you">
							<a href="{$url}">
								<xsl:value-of select="php:function('trans', 'You are a student')"/>
							</a>
						</xsl:when>
						<xsl:when test="$force_url">
							<a href="{$url}">
								<xsl:value-of select="php:function('trans', 'Student')"/>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'Student')"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</xsl:when>
			<xsl:when test="$status = 'pretender'">
				<span class="fa fa-ellipsis-h"/>
				<xsl:text> </xsl:text>
				<span class="status_premoderation">
					<xsl:choose>
						<xsl:when test="$you">
							<a href="{$url}">
								<xsl:value-of select="php:function('trans', 'You are on moderation')"/>
							</a>
						</xsl:when>
						<xsl:when test="$force_url">
							<a href="{$url}">
								<xsl:value-of select="php:function('trans', 'On moderation')"/>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'On moderation')"/>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$you">
						<a href="{$url}">
							<xsl:value-of select="php:function('trans', 'You are not a student')"/>
						</a>
					</xsl:when>
					<xsl:when test="$mode = 'silent'"/>
					<xsl:when test="$mode = 'short'">&#8211;</xsl:when>
					<xsl:when test="$force_url">
						<a href="{$url}">
							<xsl:value-of select="php:function('trans', 'Not a student')"/>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Not a student')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
