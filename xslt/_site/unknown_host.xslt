<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'bad_domain'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:template match="unknown_host">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Metaversity')"/>
			<xsl:with-param name="url" select="concat($main_prefix, '/')"/>
		</xsl:apply-templates>
		<div class="content list">
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<p style="font-size: 24px;">
			<xsl:value-of select="php:function('trans', 'Sorry, the requested domain is not registered on Metaversity.')"/>
		</p>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var referrer = '<xsl:value-of select="$request/@referrer"/>';
			_gaq.push(['_trackEvent', 'Error 404', location.href, referrer ? referrer : ':direct:', 0, true]);
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Unknown domain')"/>
	</xsl:template>
</xsl:stylesheet>
