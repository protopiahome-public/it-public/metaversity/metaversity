<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:apply-templates mode="title" select="/root"/>
				</title>
				<xsl:apply-templates mode="css" select="/root"/>
				<script type="text/javascript">
					var _gaq = _gaq || [];
				</script>
				<xsl:if test="$request/@main_host_display_name and not($debug) and $sys_params/@ga_tracking_id != ''">
					<xsl:variable name="ga_domain_name">
						<xsl:value-of select="$request/@main_host_display_name"/>
					</xsl:variable>
					<script type="text/javascript">
						_gaq.push(['_setAccount', '<xsl:value-of select="$sys_params/@ga_tracking_id"/>']);
						_gaq.push(['_setDomainName', '<xsl:value-of select="$ga_domain_name"/>']);
						_gaq.push(['_setAllowLinker', true]);
						_gaq.push(['_trackPageview']);
						
						(function() {
							var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
							ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
						})();
						
						window.onerror = function(msg, url, line) {
							var preventErrorAlert = true;
							_gaq.push(['_trackEvent', 'JS Error', msg, navigator.userAgent + ' -- ' + url + " : " + line + ' -- ' + location.href, 0, true]);
							return preventErrorAlert;
						};
					</script>
				</xsl:if>
				<xsl:if test="$debug">
					<script type="text/javascript" src="{$main_prefix}/js/debug.js?v3"/>
				</xsl:if>
				<xsl:apply-templates mode="head_priority" select="/root"/>
				<xsl:if test="$request/@is_domain_logic_on = 1">
					<script type="text/javascript" src="{$main_prefix}/js/ga_urls.js?v1"/>
				</xsl:if>
				<xsl:if test="not($debug)">
					<script type="text/javascript">
						jQuery.error = function(message) {
							_gaq.push(['_trackEvent', 'jQuery Error', message, navigator.userAgent, 0, true]);
						}
					</script>
				</xsl:if>
				<xsl:apply-templates mode="js" select="/root"/>
				<xsl:variable name="global_debug">
					<xsl:choose>
						<xsl:when test="$debug">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<script type="text/javascript">
					var global = {
						debug: <xsl:value-of select="$global_debug"/>,
						cookie_domain: '<xsl:value-of select="$request/@cookie_domain"/>',
						cookie_path: '<xsl:value-of select="$request/@cookie_path"/>',
						main_prefix: '<xsl:value-of select="$main_prefix"/>/',
						local_prefix: '<xsl:value-of select="$request/@local_prefix"/>/',
						main_host_display_name: '<xsl:value-of select="$request/@main_host_display_name"/>',
						ajax_prefix: '<xsl:value-of select="$ajax_prefix"/>/',
						session_cookie_name: '<xsl:value-of select="$request/@session_name"/>',
						trans: {}
					}
					global.trans['Error'] = '<xsl:value-of select="php:function('trans', 'Error')"/>';
					global.trans['Saving...'] = '<xsl:value-of select="php:function('trans', 'Saving...')"/>';
					global.trans['Loading...'] = '<xsl:value-of select="php:function('trans', 'Loading...')"/>';
					global.trans['Confirmation'] = '<xsl:value-of select="php:function('trans', 'Confirmation')"/>';
					global.trans['Confirm'] = '<xsl:value-of select="php:function('trans', 'Confirm')"/>';
					global.trans['Close'] = '<xsl:value-of select="php:function('trans', 'Close')"/>';
					var imGlobal = {
						cookieDomain: '<xsl:value-of select="$request/@cookie_domain"/>',
						cookiePath: '<xsl:value-of select="$request/@cookie_path"/>'
					}
				</script>
				<xsl:apply-templates mode="head" select="/root"/>
				<xsl:apply-templates mode="favicon" select="/root"/>
				<script type="text/javascript" src="{$main_prefix}/js/lang/{$lang}.js?v1"/>
			</head>
			<body>
				<xsl:attribute name="class">
					<xsl:text>no-touch </xsl:text>
					<xsl:apply-templates mode="body_class" select="/root"/>
					<xsl:text> </xsl:text>
					<xsl:apply-templates mode="body_class_2" select="/root"/>
				</xsl:attribute>
				<div class="lightbox_outer">
					<xsl:apply-templates select="/root" mode="top_level_markup"/>
					<div id="main" class="main">
						<xsl:if test="$debug">
							<div id="debug"/>
						</xsl:if>
						<xsl:apply-templates select="/root" mode="top_menu"/>
						<xsl:apply-templates/>
						<div class="clear"/>
						<xsl:apply-templates select="/root" mode="footer"/>
						<xsl:apply-templates select="/root" mode="debug_info"/>
					</div>
				</div>
				<xsl:apply-templates mode="errors" select="/root"/>
				<xsl:apply-templates mode="loading" select="/root"/>
				<xsl:apply-templates mode="lightbox" select="/root"/>
			</body>
		</html>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$sys_params/@title"/>
	</xsl:template>
	<xsl:template mode="css" match="/root"/>
	<xsl:template mode="head_priority" match="/root">
		<xsl:apply-templates mode="jquery" select="/root"/>
	</xsl:template>
	<xsl:template mode="jquery" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/jquery-1.12.0.min.js?v1"/>
	</xsl:template>
	<xsl:template mode="js" match="/root"/>
	<xsl:template mode="head" match="/root"/>
	<xsl:template mode="favicon" match="/root">
		<link rel="shortcut icon" type="image/gif" href="{$main_prefix}/favicon.png?v2"/>
		<meta property="og:image" content="{$main_prefix}/img/logo-152.gif?v1"/>
		<link rel="apple-touch-icon" href="{$main_prefix}/img/logo-152.gif?v1" sizes="152x152"/>
	</xsl:template>
	<xsl:template mode="body_class" match="/root"/>
	<xsl:template mode="body_class_2" match="/root"/>
	<xsl:template mode="top_level_markup" match="/root"/>
	<xsl:template mode="top_menu" match="/root"/>
	<xsl:template mode="footer" match="/root"/>
	<xsl:template mode="debug_info" match="/root"/>
	<xsl:template mode="errors" match="/root"/>
	<xsl:template mode="loading" match="/root"/>
	<xsl:template mode="lightbox" match="/root"/>
</xsl:stylesheet>
