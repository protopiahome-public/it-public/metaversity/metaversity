<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" exclude-result-prefixes="php">
	<xsl:decimal-format decimal-separator="." grouping-separator=" "/>
	<xsl:include href="../_core/auxil/datetime.inc.xslt"/>
	<xsl:include href="../_core/auxil/count_case.inc.xslt"/>
	<xsl:include href="../_core/auxil/trans.inc.xslt"/>
	<xsl:include href="base_vars.xslt"/>
	<xsl:include href="base_metaversity.xslt"/>
	<xsl:template match="text()"/>
	<xsl:template name="url_replace_param">
		<xsl:param name="url_base" select="$request/@url_base"/>
		<xsl:param name="param_name"/>
		<xsl:param name="param_value" select="1"/>
		<xsl:param name="param2_name" select="''"/>
		<xsl:param name="param2_value" select="1"/>
		<xsl:value-of select="$url_base"/>
		<xsl:for-each select="$get_vars[@name != $param_name and @name != $param2_name]">
			<xsl:call-template name="get_param_for_url">
				<xsl:with-param name="position" select="position()"/>
			</xsl:call-template>
		</xsl:for-each>
		<xsl:choose>
			<xsl:when test="$get_vars[@name != $param_name and @name != $param2_name]">&amp;</xsl:when>
			<xsl:otherwise>?</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$param_name"/>
		<xsl:text>=</xsl:text>
		<xsl:value-of select="$param_value"/>
		<xsl:if test="$param2_name != ''">
			<xsl:text>&amp;</xsl:text>
			<xsl:value-of select="$param2_name"/>
			<xsl:text>=</xsl:text>
			<xsl:value-of select="$param2_value"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="module_url_delete_param">
		<xsl:param name="url_base" select="$module_url"/>
		<xsl:param name="param_name"/>
		<xsl:value-of select="$url_base"/>
		<xsl:for-each select="$get_vars[@name != $param_name]">
			<xsl:call-template name="get_param_for_url">
				<xsl:with-param name="position" select="position()"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="url_delete_param">
		<xsl:param name="url_base" select="$request/@url_base"/>
		<xsl:param name="param_name"/>
		<xsl:value-of select="$url_base"/>
		<xsl:for-each select="$get_vars[@name != $param_name]">
			<xsl:call-template name="get_param_for_url">
				<xsl:with-param name="position" select="position()"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="get_param_for_form">
		<xsl:choose>
			<xsl:when test="item">
				<xsl:for-each select="item">
					<input type="hidden" name="{../@name}[]" value="{.}"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<input type="hidden" name="{@name}" value="{.}"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="get_param_for_url">
		<xsl:param name="position"/>
		<xsl:choose>
			<xsl:when test="item">
				<xsl:for-each select="item">
					<xsl:choose>
						<xsl:when test="$position = 1 and position() = 1">?</xsl:when>
						<xsl:otherwise>&amp;</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="../@name"/>
					<xsl:text>%5B%5D=</xsl:text>
					<xsl:value-of select="@value_escaped"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$position = 1">?</xsl:when>
					<xsl:otherwise>&amp;</xsl:otherwise>
				</xsl:choose>
				<xsl:value-of select="@name"/>
				<xsl:text>=</xsl:text>
				<xsl:value-of select="@value_escaped"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="dot">
		<xsl:param name="text"/>
		<xsl:value-of select="$text"/>
		<xsl:variable name="last" select="substring($text, string-length($text), 1)"/>
		<xsl:if test="$last != '.'">.</xsl:if>
	</xsl:template>
</xsl:stylesheet>
