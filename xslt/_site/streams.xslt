<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'streams'"/>
	<xsl:variable name="top_section_main_page" select="/root/streams/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/streams/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="streams">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Streams')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($top_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content list">
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="widgets">
			<xsl:apply-templates select="filters" mode="filters"/>
		</div>
		<xsl:apply-templates select="filters" mode="filters_show"/>
		<xsl:if test="not(stream)">
			<p>
				<xsl:value-of select="php:function('trans', 'No streams were found.')"/>
			</p>
		</xsl:if>
		<xsl:for-each select="stream">
			<xsl:variable name="context" select="."/>
			<xsl:for-each select="/root/stream_short[@id = current()/@id]">
				<div class="list_item">
					<div class="list_item_title list_item_title__bigger">
						<a href="{@url}">
							<xsl:value-of select="@title"/>
						</a>
						<xsl:text> (</xsl:text>
						<xsl:value-of select="@title_short"/>
						<xsl:text>)</xsl:text>
					</div>
					<div class="props">
						<xsl:if test="$context/@list_descr != ''">
							<div class="prop">
								<xsl:call-template name="dot">
									<xsl:with-param name="text" select="$context/@list_descr"/>
								</xsl:call-template>
							</div>
						</xsl:if>
						<div class="prop">
							<xsl:choose>
								<xsl:when test="@join_premoderation = 1">
									<span class="fa fa-lock"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="php:function('trans', 'Restricted membership', 'STREAM ACCESS')"/>
								</xsl:when>
								<xsl:otherwise>
									<span class="fa fa-globe"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="php:function('trans', 'Publicly opened', 'STREAM ACCESS')"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> </xsl:text>
							<xsl:if test="$user/@id">
								<span class="prop_sep"> </span>
								<xsl:call-template name="draw_stream_status">
									<xsl:with-param name="status" select="$context/@status"/>
								</xsl:call-template>
							</xsl:if>
						</div>
						<xsl:if test="$lang != @lang_code or $lang != 'ru'">
							<div class="prop">
								<span class="icon16 icon16__{@lang_code}"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="php:function('trans', 'Language')"/>
								<xsl:text>: </xsl:text>
								<xsl:value-of select="@lang_title"/>
							</div>
						</xsl:if>
						<xsl:if test="$context/city">
							<div class="prop">
								<span class="fa fa-map-marker"/>
								<xsl:text> </xsl:text>
								<xsl:for-each select="$context/city[@id = /root/cities/city/@id]">
									<xsl:value-of select="/root/cities/city[@id = current()/@id]/@title"/>
									<xsl:if test="position() != last()">, </xsl:if>
								</xsl:for-each>
							</div>
						</xsl:if>
						<div class="prop">
							<span class="fa fa-users"/>
							<xsl:text> </xsl:text>
							<a href="{@url}users/">
								<xsl:value-of select="$context/@user_count_calc"/>
								<xsl:text> </xsl:text>
								<xsl:call-template name="count_case_lang">
									<xsl:with-param name="number" select="$context/@user_count_calc"/>
									<xsl:with-param name="word" select="'student'"/>
								</xsl:call-template>
							</a>
							<!--<span class="prop_sep"> </span>
							<span class="fa fa-calendar"/>
							<a href="">7 actual events</a>, <a class="gray" href="">15 past</a>-->
						</div>
						<xsl:if test="$context/@status = 'admin' or $context/@status = 'moderator'">
							<div class="prop">
								<span class="fa fa-gear"/>
								<xsl:text> </xsl:text>
								<a class="admin" href="{@url}admin/">
									<xsl:value-of select="php:function('trans', 'Administration')"/>
								</a>
							</div>
						</xsl:if>
					</div>
				</div>
			</xsl:for-each>
			<xsl:copy-of select="list_descr_html/div"/>
		</xsl:for-each>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Metaversity streams')"/>
	</xsl:template>
</xsl:stylesheet>
