<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'page404'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/404/')"/>
	<xsl:template match="error_404">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Metaversity')"/>
			<xsl:with-param name="url" select="concat($main_prefix, '/')"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<p style="font-size: 40px;">
			<xsl:value-of select="php:function('trans', 'Error 404')"/>
			<xsl:text> :-(</xsl:text>
		</p>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var referrer = '<xsl:value-of select="$request/@referrer"/>';
			_gaq.push(['_trackEvent', 'Error 404', location.href, referrer ? referrer : ':direct:', 0, true]);
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Error 404')"/>
	</xsl:template>
</xsl:stylesheet>
