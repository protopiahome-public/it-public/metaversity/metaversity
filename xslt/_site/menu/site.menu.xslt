<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template mode="menu_top" match="/root">
		<div class="menu_top">
			<xsl:choose>
				<xsl:when test="$user/@id">
					<span class="js_btn_menu_top_user_dd menu_item_user">
						<span class="_img" style="background-image: url({$user/@photo_big_url});"/>
						<xsl:text> </xsl:text>
						<span class="js_menu_top_user_adjust _text">
							<xsl:value-of select="$user/@visible_name"/>
						</span>
						<xsl:text> </xsl:text>
						<span class="_down fa fa-caret-down"/>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span class="_right">
						<xsl:apply-templates mode="menu_items_guest" select="/root"/>
					</span>
				</xsl:otherwise>
			</xsl:choose>
			<span class="_right">
				<xsl:choose>
					<xsl:when test="$top_section = 'lang' and $top_section_main_page">
						<span class="menu_item menu_item__current">
							<span class="icon24 icon24__{$lang}"/>
						</span>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$main_prefix}/lang/?retpath={$request/@retpath_escaped}" class="menu_item">
							<xsl:if test="$top_section = 'lang'">
								<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
							</xsl:if>
							<span class="icon24 icon24__{$lang}"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<a class="_home" href="{$main_prefix}/">
				<span class="_home_icon fa fa-home"/>
			</a>
			<xsl:apply-templates mode="menu_items_main" select="/root"/>
		</div>
		<div class="menu_top_dd">
			<xsl:if test="$user/@id">
				<xsl:apply-templates mode="menu_items_user" select="/root"/>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template mode="menu_left" match="/root">
		<div class="menu_left menu_left__reserve">
			<div class="_block _block__first">
				<div class="_title_main">
					<xsl:value-of select="php:function('trans', 'Metaversity')"/>
				</div>
				<xsl:apply-templates mode="menu_items_main" select="/root"/>
			</div>
			<div class="_block _block__last">
				<xsl:choose>
					<xsl:when test="$user/@id">
						<xsl:apply-templates mode="menu_items_user_title" select="/root"/>
						<xsl:apply-templates mode="menu_items_lang" select="/root"/>
						<xsl:apply-templates mode="menu_items_user" select="/root"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates mode="menu_items_lang" select="/root"/>
						<xsl:apply-templates mode="menu_items_guest" select="/root"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template mode="menu_items_main" match="/root">
		<xsl:variable name="is_my_profile" select="$top_section = 'users' and not(/root/users) and $user/@id = /root/user_short[1]/@id"/>
		<xsl:if test="$user/@id">
			<xsl:choose>
				<xsl:when test="$is_my_profile and $user_section = 'news' and $user_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'My profile')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$main_prefix}/news/" class="menu_item">
						<xsl:if test="$is_my_profile">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'My profile')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$top_section = 'streams' and $top_section_main_page">
				<span class="menu_item menu_item__current">
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Streams')"/>
					</span>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$main_prefix}/streams/" class="menu_item">
					<xsl:if test="$top_section = 'streams'">
						<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
					</xsl:if>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Streams')"/>
					</span>
				</a>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$user/@id">
			<xsl:choose>
				<xsl:when test="$top_section = 'activities' and $top_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Schedule')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$main_prefix}/activities/" class="menu_item">
						<xsl:if test="$top_section = 'activities'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Schedule')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$top_section = 'users' and not($is_my_profile) and $top_section_main_page">
				<span class="menu_item menu_item__current">
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'People')"/>
					</span>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$users_prefix}/" class="menu_item">
					<xsl:if test="$top_section = 'users' and not($is_my_profile)">
						<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
					</xsl:if>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'People')"/>
					</span>
				</a>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="not($user/@id)">
			<xsl:choose>
				<xsl:when test="$top_section = 'news' and $top_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'News')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$main_prefix}/news/" class="menu_item">
						<xsl:if test="$top_section = 'news'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'News')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$user/@is_admin = 1">
			<xsl:choose>
				<xsl:when test="$top_section = 'admin' and $top_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-gears"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Administration')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$main_prefix}/admin/" class="menu_item">
						<xsl:if test="$top_section = 'admin'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-gears"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Administration')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="menu_items_user_title" match="/root">
		<xsl:param name="clickable" select="false()"/>
		<div>
			<xsl:attribute name="class">
				<xsl:text>menu_item menu_item__small</xsl:text>
				<xsl:if test="$clickable"> js_btn_menu_left_open_user menu_item__clickable</xsl:if>
			</xsl:attribute>
			<span class="_icon">
				<span class="_img" style="background-image: url({$user/@photo_big_url});"/>
			</span>
			<xsl:text> </xsl:text>
			<span>
				<xsl:attribute name="class">
					<xsl:text>js_menu_left_user_adjust _text</xsl:text>
					<xsl:if test="$clickable"> _text__dashed</xsl:if>
				</xsl:attribute>
				<xsl:value-of select="$user/@visible_name"/>
			</span>
			<xsl:if test="$clickable">
				<xsl:text> </xsl:text>
				<span class="_down fa fa-caret-down"/>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template mode="menu_items_lang" match="/root">
		<xsl:choose>
			<xsl:when test="$top_section = 'lang' and $top_section_main_page">
				<span class="menu_item menu_item__small menu_item__current">
					<span class="icon24 icon24__{$lang}"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Language')"/>
						<xsl:text>: </xsl:text>
						<xsl:value-of select="php:function('trans', 'lang')"/>
					</span>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$main_prefix}/lang/?retpath={$request/@retpath_escaped}" class="menu_item menu_item__small">
					<xsl:if test="$top_section = 'lang'">
						<xsl:attribute name="class">menu_item menu_item__small menu_item__current</xsl:attribute>
					</xsl:if>
					<span class="icon24 icon24__{$lang}"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Language')"/>
						<xsl:text>: </xsl:text>
						<xsl:value-of select="php:function('trans', 'lang')"/>
					</span>
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="menu_items_user" match="/root">
		<xsl:choose>
			<xsl:when test="$top_section = 'settings' and $top_section_main_page">
				<span class="menu_item menu_item__small menu_item__current">
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Personal settings')"/>
					</span>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$main_prefix}/settings/" class="menu_item menu_item__small">
					<xsl:if test="$top_section = 'settings'">
						<xsl:attribute name="class">menu_item menu_item__small menu_item__current</xsl:attribute>
					</xsl:if>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Personal settings')"/>
					</span>
				</a>
			</xsl:otherwise>
		</xsl:choose>
		<a class="menu_item menu_item__small" href="{$logout_url}">
			<span class="_text">
				<xsl:value-of select="php:function('trans', 'Logout')"/>
			</span>
		</a>
	</xsl:template>
	<xsl:template mode="menu_items_guest" match="/root">
		<xsl:choose>
			<xsl:when test="$top_section = 'login' and $top_section_main_page">
				<span class="menu_item menu_item__small menu_item__current">
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Login', 'PROCESS')"/>
					</span>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$login_url}" class="menu_item menu_item__small">
					<xsl:if test="$top_section = 'login'">
						<xsl:attribute name="class">menu_item menu_item__small menu_item__current</xsl:attribute>
					</xsl:if>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Login', 'PROCESS')"/>
					</span>
				</a>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$top_section = 'reg' and $top_section_main_page">
				<span class="menu_item menu_item__small menu_item__current">
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Registration')"/>
					</span>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{$reg_url}" class="menu_item menu_item__small">
					<xsl:if test="$top_section = 'reg'">
						<xsl:attribute name="class">menu_item menu_item__small menu_item__current</xsl:attribute>
					</xsl:if>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Registration')"/>
					</span>
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="menu_footer" match="/root">
		<!--<p>
			<xsl:if test="$user/@id">
				<a href="{$users_prefix}/{$user/@login}/">
					<xsl:value-of select="php:function('trans', 'My profile')"/>
				</a>
				<span class="_sep"> | </span>
			</xsl:if>
			<a href="{$main_prefix}/">
				<xsl:value-of select="php:function('trans', 'Streams')"/>
			</a>
			<span class="_sep"> | </span>
			<xsl:if test="$user/@id">
				<a href="{$main_prefix}/activities/">
					<xsl:value-of select="php:function('trans', 'Schedule')"/>
				</a>
				<span class="_sep"> | </span>
			</xsl:if>
			<a href="{$users_prefix}/">
				<xsl:value-of select="php:function('trans', 'People')"/>
			</a>
			<xsl:if test="not($user/@id)">
				<span class="_sep"> | </span>
				<a href="{$main_prefix}/">
					<xsl:value-of select="php:function('trans', 'News')"/>
				</a>
			</xsl:if>
			<xsl:if test="$user/@is_admin = 1">
				<span class="_sep"> | </span>
				<a class="admin" href="{$main_prefix}/admin/">
					<xsl:value-of select="php:function('trans', 'Administration')"/>
				</a>
			</xsl:if>
		</p>-->
		<p>
			<span class="nobr" style="padding: 0 10px;">
				<i class="fa fa-envelope-o"/>
				<xsl:text> </xsl:text>
				<a class="email" href="mailto:{$sys_params/@info_email_obscured}">
					<xsl:value-of select="$sys_params/@info_email_obscured"/>
				</a>
			</span>
			<xsl:text> </xsl:text>
			<span class="nobr" style="padding: 0 10px;">
				<i class="fa fa-vk"/>
				<xsl:text> </xsl:text>
				<a href="https://vk.com/metaversity">
					<xsl:text>vk.com/metaversity</xsl:text>
				</a>
				<xsl:text>&#160;</xsl:text>
				<xsl:value-of select="php:function('trans', 'DASH')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'News &amp; Support')"/>
			</span>
			<xsl:text> </xsl:text>
			<span class="nobr" style="padding: 0 10px;">
				<i class="fa fa-info-circle"/>
				<xsl:text> </xsl:text>
				<a href="//wiki.metaversity.ru/">
					<xsl:value-of select="php:function('trans', 'Metaversity Wiki')"/>
				</a>
			</span>
		</p>
		<p>
			<xsl:text>© </xsl:text>
			<a href="http://www.soling.su/">
				<xsl:value-of select="php:function('trans', 'Educational Bureau Soling', 'COPYRIGHT')"/>
			</a>
			<xsl:text>, </xsl:text>
			<xsl:if test="$sys_params/@copyright_start_year &lt; /root/@year">
				<xsl:value-of select="$sys_params/@copyright_start_year"/>
				<xsl:text>–</xsl:text>
			</xsl:if>
			<xsl:value-of select="/root/@year"/>
		</p>
	</xsl:template>
</xsl:stylesheet>
