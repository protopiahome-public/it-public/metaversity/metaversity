<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="../news/include/news.inc.xslt"/>
	<xsl:variable name="top_section" select="'news'"/>
	<xsl:variable name="top_section_main_page" select="/root/user_news/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/news/')"/>
	<xsl:template match="user_news">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'News')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($top_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content list">
			<div class="maxw800">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:apply-templates select="filters" mode="filters_show"/>
		<xsl:call-template name="draw_news"/>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'News')"/>
	</xsl:template>
</xsl:stylesheet>
