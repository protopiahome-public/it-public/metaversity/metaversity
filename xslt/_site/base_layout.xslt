<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="base.xslt"/>
	<xsl:template mode="header" match="/root">
		<xsl:param name="title"/>
		<xsl:param name="url" select="''"/>
		<xsl:param name="use_url" select="true()"/>
		<xsl:param name="show_filter" select="false()"/>
		<div class="head">
			<span class="js_btn_menu_left_open _btn_left _icon fa fa-bars"/>
			<xsl:if test="$show_filter">
				<span class="js_btn_filter _btn_right _icon fa fa-filter"/>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$use_url and $url != ''">
					<a href="{$url}">
						<xsl:value-of select="$title"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$title"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="draw_kv">
		<xsl:param name="key"/>
		<xsl:param name="text" select="''"/>
		<xsl:param name="html" select="''"/>
		<xsl:param name="class" select="''"/>
		<xsl:if test="$text != '' or $html != ''">
			<div class="{$class}">
				<span class="key">
					<xsl:value-of select="$key"/>
					<xsl:text>: </xsl:text>
				</span>
				<xsl:choose>
					<xsl:when test="$text != ''">
						<xsl:value-of select="$text"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="$html"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_th">
		<xsl:param name="name"/>
		<xsl:param name="title"/>
		<xsl:param name="class"/>
		<th class="{$class} nobr">
			<xsl:choose>
				<xsl:when test="$name != ''">
					<a>
						<xsl:attribute name="href">
							<xsl:call-template name="url_replace_param">
								<xsl:with-param name="url_base" select="$module_url"/>
								<xsl:with-param name="param_name" select="'sort'"/>
								<xsl:with-param name="param_value" select="$name"/>
								<xsl:with-param name="param2_name" select="'sort_back'"/>
								<xsl:with-param name="param2_value">
									<xsl:choose>
										<xsl:when test="sort/@order = $name and sort/@back = 0">1</xsl:when>
										<xsl:otherwise>0</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:attribute>
						<xsl:value-of select="$title"/>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$title"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="sort/@order = $name">
				<xsl:choose>
					<xsl:when test="sort/@back = 1">&#160;&#8593;</xsl:when>
					<xsl:otherwise>&#160;&#8595;</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</th>
	</xsl:template>
	<xsl:template name="draw_form_info">
		<xsl:param name="added" select="false()"/>
		<xsl:param name="edited" select="false()"/>
		<xsl:param name="deleted" select="false()"/>
		<xsl:choose>
			<xsl:when test="($pass_info/info[@name = 'ADDED'] or $pass_info/info[@name = 'action'] = 'dt_add' and $pass_info/info[@name = 'SAVED']) and $added">
				<div class="info">
					<span>
						<xsl:value-of select="$added"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'SAVED'] and $edited">
				<div class="info">
					<span>
						<xsl:value-of select="$edited"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/info[@name = 'DELETED'] and $deleted">
				<div class="info">
					<span>
						<xsl:value-of select="$deleted"/>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="filters_show" match="filters">
		<xsl:param name="exclude_name" select="false()"/>
		<xsl:param name="exclude_name_2" select="false()"/>
		<xsl:if test="filter[@is_active = 1 and @name != $exclude_name and @name != $exclude_name_2]">
			<div class="filtered p">
				<div class="_title">
					<xsl:value-of select="php:function('trans', 'Filters')"/>
					<xsl:text>:</xsl:text>
				</div>
				<xsl:for-each select="filter[@is_active = 1 and @name != $exclude_name and @name != $exclude_name_2]">
					<div class="_item">
						<xsl:choose>
							<xsl:when test="@type = 'foreign_key' or @type = 'multi_link' or @type = 'multi_link_or_empty' or @type = 'select'">
								<a class="_item_remove">
									<xsl:attribute name="href">
										<xsl:call-template name="module_url_delete_param">
											<xsl:with-param name="param_name" select="concat('filter[', @name, ']')"/>
										</xsl:call-template>
									</xsl:attribute>
									<i class="fa fa-times"/>
								</a>
								<xsl:variable name="filter_val">
									<xsl:choose>
										<xsl:when test="@value = '' and not(@group_value != '')">
											<xsl:choose>
												<xsl:when test="@inactive_title != ''">
													<xsl:value-of select="@inactive_title"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="php:function('trans', 'any', 'FILTER')"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:when test="options//option[@id = current()/@value]">
											<xsl:choose>
												<xsl:when test="options//option[@id = current()/@value]/@title_short">
													<xsl:value-of select="options//option[@id = current()/@value]/@title_short"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="options//option[@id = current()/@value]/@title"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:when test="options//group[@id = current()/@group_value]">
											<xsl:choose>
												<xsl:when test="options//group[@id = current()/@group_value]/@title_short">
													<xsl:value-of select="options//group[@id = current()/@group_value]/@title_short"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="options//group[@id = current()/@group_value]/@title"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
									</xsl:choose>
								</xsl:variable>
								<span class="_item_text" title="{@title}: {$filter_val}">
									<span class="_item_title">
										<xsl:value-of select="@title"/>
										<xsl:text>: </xsl:text>
									</span>
									<span class="_item_value">
										<xsl:value-of select="$filter_val"/>
									</span>
								</span>
							</xsl:when>
							<xsl:when test="@type = 'text'">
								<a class="_item_remove">
									<xsl:attribute name="href">
										<xsl:call-template name="module_url_delete_param">
											<xsl:with-param name="param_name" select="concat('filter[', @name, ']')"/>
										</xsl:call-template>
									</xsl:attribute>
									<i class="fa fa-times"/>
								</a>
								<span class="_item_text">
									<span class="_item_title">
										<xsl:value-of select="@title"/>
										<xsl:text>: </xsl:text>
									</span>
									<span class="_item_value" title="{@title}: {@value}">
										<xsl:choose>
											<xsl:when test="string-length(@value) &lt;= 10">
												<xsl:value-of select="@value"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="substring(@value, 1, 10)"/>
												<xsl:text>...</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</span>
								</span>
							</xsl:when>
							<xsl:when test="@type = 'show_flag'">
								<a class="_item_remove">
									<xsl:attribute name="href">
										<xsl:choose>
											<xsl:when test="@remove_url = '__CALLBACK__'">
												<xsl:apply-templates select="." mode="filters_show_draw_href_callback"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="@remove_url"/>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:if test="@preserve_get_params = 1">
											<xsl:value-of select="$request/@query_string"/>
										</xsl:if>
									</xsl:attribute>
									<i class="fa fa-times"/>
								</a>
								<span class="_item_text">
									<span class="_item_title">
										<xsl:choose>
											<xsl:when test="@title = '__CALLBACK__'">
												<xsl:apply-templates select="." mode="filters_show_draw_title_callback"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="@title"/>
											</xsl:otherwise>
										</xsl:choose>
									</span>
								</span>
							</xsl:when>
						</xsl:choose>
					</div>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="filter_one" match="filters">
		<xsl:param name="name"/>
		<xsl:param name="prefix_html"/>
		<xsl:param name="class" select="''"/>
		<xsl:if test="filter[@name = $name]">
			<form class="inline_filter" action="{$module_url}" method="get">
				<xsl:for-each select="$get_vars[@name != concat('filter[', $name, ']')]">
					<xsl:call-template name="get_param_for_form"/>
				</xsl:for-each>
				<xsl:for-each select="filter[@name = $name]">
					<span class="head_dd {$class}">
						<span class="_prefix_html dn">
							<xsl:copy-of select="$prefix_html"/>
						</span>
						<select name="filter[{@name}]">
							<option value="(any)">
								<xsl:value-of select="@inactive_title"/>
							</option>
							<xsl:for-each select="options//group | options//option">
								<option value="{@id}" data-title-short="{@title_short}" data-title-full="{@title}">
									<xsl:if test="@id = ancestor::filter/@value">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="@title_short"/>
								</option>
							</xsl:for-each>
						</select>
					</span>
				</xsl:for-each>
			</form>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="filter_search" match="filters">
		<xsl:param name="name" select="'search'"/>
		<xsl:if test="filter[@name = $name]">
			<form class="filter" action="{$module_url}" method="get">
				<xsl:for-each select="$get_vars[@name != concat('filter[', $name, ']')]">
					<xsl:call-template name="get_param_for_form"/>
				</xsl:for-each>
				<xsl:for-each select="filter[@name = $name]">
					<div class="field2">
						<div class="js_ctrl_search ctrl ctrl__search">
							<div class="_box_button">
								<button class="ctrl ctrl__button box_shadow">
									<i class="fa fa-arrow-right"/>
								</button>
							</div>
							<div class="_box_input box_shadow">
								<i class="_icon fa fa-search"/>
								<input class="js_ctrl_search_input ctrl ctrl__text" name="filter[{@name}]" type="text" value="{@value}" placeholder="{@placeholder}"/>
							</div>
						</div>
					</div>
				</xsl:for-each>
			</form>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="filter_form" match="filters">
		<xsl:param name="padded" select="false()"/>
		<xsl:param name="exclude_name" select="false()"/>
		<xsl:param name="exclude_name_2" select="false()"/>
		<form class="filter" action="{$module_url}" method="get">
			<xsl:for-each select="$get_vars[substring(@name, 1, 7) != 'filter[' or @name = concat('filter[', $exclude_name, ']') or @name = concat('filter[', $exclude_name_2, ']')]">
				<xsl:call-template name="get_param_for_form"/>
			</xsl:for-each>
			<xsl:for-each select="filter[@name != $exclude_name and @name != $exclude_name_2]">
				<xsl:choose>
					<xsl:when test="@type = 'foreign_key' or @type = 'multi_link' or @type = 'multi_link_or_empty' or @type = 'select'">
						<div class="field2">
							<xsl:if test="$padded">
								<xsl:attribute name="class">field2 field2__padded</xsl:attribute>
							</xsl:if>
							<label for="filter-ctrl-{@name}">
								<xsl:value-of select="@title"/>
								<xsl:text>:</xsl:text>
							</label>
							<xsl:variable name="spaces" select="'                                                                                                    '"/>
							<select id="filter-ctrl-{@name}" class="js_ctrl_select_with_front ctrl ctrl__select" name="filter[{@name}]">
								<xsl:attribute name="data-reset-on-change">
									<xsl:for-each select="reset_on_change/filter">
										<xsl:value-of select="@name"/>
										<xsl:text> </xsl:text>
									</xsl:for-each>
								</xsl:attribute>
								<option value="(any)">
									<xsl:choose>
										<xsl:when test="@inactive_title != ''">
											<xsl:value-of select="@inactive_title"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="php:function('trans', 'any', 'FILTER')"/>
										</xsl:otherwise>
									</xsl:choose>
								</option>
								<xsl:for-each select="options//group | options//option">
									<xsl:choose>
										<xsl:when test="name() = 'group'">
											<xsl:if test=".//option">
												<option value="g-{@id}">
													<xsl:if test="@id = ancestor::filter/@group_value">
														<xsl:attribute name="selected">selected</xsl:attribute>
													</xsl:if>
													<xsl:value-of select="substring($spaces, 1, (@level - 1) * 2)"/>
													<xsl:value-of select="@title"/>
												</option>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise>
											<option value="{@id}">
												<xsl:if test="@id = ancestor::filter/@value">
													<xsl:attribute name="selected">selected</xsl:attribute>
												</xsl:if>
												<xsl:if test="parent::group">
													<xsl:value-of select="substring($spaces, 1, parent::group/@level * 2)"/>
												</xsl:if>
												<xsl:value-of select="@title"/>
											</option>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</select>
						</div>
					</xsl:when>
					<xsl:when test="@type = 'text'">
						<div class="field2">
							<div class="js_ctrl_search ctrl ctrl__search">
								<div class="_box_button">
									<button class="ctrl ctrl__button box_shadow">
										<i class="fa fa-arrow-right"/>
									</button>
								</div>
								<div class="_box_input box_shadow">
									<i class="_icon fa fa-search"/>
									<input class="js_ctrl_search_input ctrl ctrl__text" name="filter[{@name}]" type="text" value="{@value}" placeholder="{@placeholder}"/>
								</div>
							</div>
						</div>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</form>
	</xsl:template>
	<xsl:template mode="filters" match="filters">
		<xsl:param name="exclude_name" select="false()"/>
		<xsl:param name="exclude_name_2" select="false()"/>
		<div class="widget">
			<div class="widget_head">
				<i class="widget_head_icon fa fa-filter"/>
				<span class="widget_head_title">
					<xsl:value-of select="php:function('trans', 'Filters')"/>
				</span>
				<i class="widget_head_expand_icon fa"/>
			</div>
			<div class="widget_content widget_pad">
				<xsl:apply-templates mode="filter_form" select=".">
					<xsl:with-param name="padded" select="true()"/>
					<xsl:with-param name="exclude_name" select="$exclude_name"/>
					<xsl:with-param name="exclude_name_2" select="$exclude_name_2"/>
				</xsl:apply-templates>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_intmarkup_with_files">
		<xsl:param name="field" select="descr"/>
		<xsl:param name="url_base" select="$field/files/@url_base"/>
		<xsl:copy-of select="$field/div"/>
		<xsl:for-each select="$field/files/file">
			<p class="intmarkup_download">
				<a href="{$url_base}{@id}/">
					<i class="fa fa-download"/>
					<xsl:value-of select="@file_name"/>
				</a>
				<span>
					<xsl:text> (</xsl:text>
					<xsl:value-of select="@size_human"/>
					<xsl:text>)</xsl:text>
				</span>
			</p>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
