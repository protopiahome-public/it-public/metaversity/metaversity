<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'lang'"/>
	<xsl:variable name="top_section_main_page" select="true()"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/lang/')"/>
	<xsl:template match="lang">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="'Language / Язык'"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">Language / Язык</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="maxw600">
			<form id="lang_form" action="{$save_prefix}/lang/" method="post">
				<div class="field2">
					<label for="input_lang_code">
						<xsl:text>Interface language / Язык интерфейса</xsl:text>
						<xsl:text>:</xsl:text>
					</label>
					<select id="input_lang_code" class="js_ctrl_select_with_front ctrl ctrl__select" name="lang">
						<xsl:for-each select="lang">
							<option value="{@code}">
								<xsl:if test="@code = $lang">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<div>
					<br/>
				</div>
				<div class="box">
					<p>
						<xsl:value-of select="php:function('trans', 'The above setting will translate user interface.')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'By default the &lt;b&gt;content&lt;/b&gt; of all streams will remain in their original languages.')" disable-output-escaping="yes"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Please check the following checkbox to translate content &lt;b&gt;automatically&lt;/b&gt; (the feature is provided by Google Website Translate).')" disable-output-escaping="yes"/>
					</p>
				</div>
				<div class="field2">
					<input id="input_translate_content" type="checkbox" class="ctrl ctrl__checkbox" name="translate_content" value="1">
						<xsl:if test="$request/@translate_content = 1">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input>
					<label for="input_translate_content" class="_inline">
						<xsl:value-of select="php:function('trans', 'Translate content using Google Website Translate')"/>
					</label>
				</div>
				<xsl:if test="$request/@translate_content = 1">
					<div class="notification">
						<span>
							<xsl:value-of select="php:function('trans', 'Please select the target language below:')"/>
						</span>
					</div>
					<div id="google_translate_element"/>
					<xsl:variable name="other_lang" select="lang[@code != $lang]/@code"/>
					<script type="text/javascript">
						function googleTranslateElementInit() {
							new google.translate.TranslateElement({pageLanguage: '<xsl:value-of select="$other_lang"/>', includedLanguages: '<xsl:value-of select="$lang"/>', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL, autoDisplay: true, multilanguagePage: true, gaTrack: true, gaId: '<xsl:value-of select="$sys_params/@ga_tracking_id"/>'}, 'google_translate_element');
						}
					</script>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"/>
				</xsl:if>
			</form>
			<xsl:if test="substring($request/@retpath, string-length($request/@retpath) - 5, 6) != '/lang/'">
				<div class="field2">
					<br/>
					<a class="ctrl ctrl__button" href="{$request/@retpath}">
						<span class="arrow">←</span>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Go to the previous page')"/>
					</a>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			$(function() {
				$('#input_lang_code').change(function() {
					$('#input_translate_content').prop('checked', false);
					$('#lang_form').submit();
				});
				$('#input_translate_content').click(function() {
					$('#lang_form').submit();
				});
			});
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:text>Language / Язык</xsl:text>
	</xsl:template>
</xsl:stylesheet>
