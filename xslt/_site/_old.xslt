<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="d1">
		<xsl:when test="not($user/@id)">
			<div class="_block">
				<div class="list_item list_item__hint">
					<p>
						<xsl:value-of select="php:function('trans', 'Please', 'AUTH_FOR_RECOMMENDATIONS')"/>
						<xsl:text> </xsl:text>
						<a href="{$login_url}">
							<xsl:value-of select="php:function('trans', 'login', 'AUTH_FOR_RECOMMENDATIONS')"/>
						</a>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'to see personal recommendations', 'AUTH_FOR_RECOMMENDATIONS')"/>
						<xsl:text>.</xsl:text>
					</p>
				</div>
			</div>
		</xsl:when>
		<xsl:when test="@positions_checked = 0">
			<div class="_block">
				<div class="list_item list_item__problem">
					<p>
						<xsl:value-of select="php:function('trans', 'Please add some positions to your', 'FOCUS_FOR_RECOMMENDATIONS')"/>
						<xsl:text> </xsl:text>
						<a href="{$current_stream/@url}positions/">
							<xsl:value-of select="php:function('trans', 'focus', 'FOCUS_FOR_RECOMMENDATIONS')"/>
						</a>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'to see personal recommendations', 'FOCUS_FOR_RECOMMENDATIONS')"/>
						<xsl:text>.</xsl:text>
					</p>
				</div>
			</div>
		</xsl:when>
		<div class="text match_legend dn">
			<p>
				<b>
					<xsl:value-of select="php:function('trans', 'Integral result')"/>
				</b>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'is calculated based on other other competences.')"/>
			</p>
			<p>
				<xsl:value-of select="php:function('trans', 'If the result is marked as integral – it means that the result which is calculated based on other competences is better than the result which is based on direct marks.')"/>
			</p>
			<xsl:call-template name="draw_stream_refine_common_legend"/>
		</div>
	</xsl:template>
	<xsl:template name="draw_marks_legend">
		<p>
			<xsl:value-of select="php:function('trans', 'The result is calculated based on the received marks.')"/>
		</p>
		<h4>
			<xsl:value-of select="php:function('trans', 'Received marks')"/>
		</h4>
		<p>
			<span class="mark">0</span>
			<xsl:text> </xsl:text>
			<span class="mark">1</span>
			<xsl:text> </xsl:text>
			<span class="mark">2</span>
			<xsl:text> </xsl:text>
			<span class="mark">3</span>
		</p>
		<xsl:call-template name="draw_marks_legend_digits_meanings"/>
		<p>
			<span class="mark mark__unimportant">0</span>
			<xsl:text> </xsl:text>
			<span class="mark mark__unimportant">1</span>
			<xsl:text> </xsl:text>
			<span class="mark mark__unimportant">2</span>
			<xsl:text> </xsl:text>
			<span class="mark mark__unimportant">3</span>
		</p>
		<p>
			<xsl:value-of select="php:function('trans', 'Gray colour is used for marks received for competences which were secondary for the role. These marks are usually downgraded to 1 in calculations.')"/>
		</p>
	</xsl:template>
	<xsl:template match="error_403">
		<div class="js_content content content__sub scrollable">
			<div class="_block">
				<div class="text">
					<xsl:choose>
						<xsl:when test="not($user/@id)">
							<p class="center">
								<xsl:value-of select="php:function('trans', 'You do not have permissions.')"/>
								<xsl:text> </xsl:text>
								<a href="{$login_url}">
									<xsl:value-of select="php:function('trans', 'Please login')"/>
								</a>
								<xsl:text>.</xsl:text>
							</p>
						</xsl:when>
						<xsl:otherwise>
							<p class="center">
								<b>
									<xsl:value-of select="$user/@login"/>
								</b>
								<xsl:text>, </xsl:text>
								<xsl:value-of select="php:function('trans', 'you do not have permissions')"/>
								<xsl:text>.</xsl:text>
							</p>
							<p class="center">
								<xsl:value-of select="php:function('trans', 'You can', 'LOGOUT_AND_LOGIN')"/>
								<xsl:text> </xsl:text>
								<a href="{$logout_url}">
									<xsl:value-of select="php:function('trans', 'logout', 'LOGOUT_AND_LOGIN')"/>
								</a>
								<xsl:text> </xsl:text>
								<xsl:value-of select="php:function('trans', 'and then use a different login', 'LOGOUT_AND_LOGIN')"/>
								<xsl:text>.</xsl:text>
							</p>
						</xsl:otherwise>
					</xsl:choose>
					<p class="center">
						<xsl:value-of select="php:function('trans', 'You can go back to the', 'BACK_TO_STREAM')"/>
						<xsl:text> </xsl:text>
						<a href="{$current_stream/@url}">
							<xsl:value-of select="php:function('trans', 'stream home page', 'BACK_TO_STREAM')"/>
						</a>
						<xsl:text>.</xsl:text>
					</p>
				</div>
			</div>
		</div>
		<xsl:apply-templates mode="menu" select="/root"/>
	</xsl:template>
</xsl:stylesheet>
