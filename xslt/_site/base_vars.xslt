<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:variable name="request" select="/root/request"/>
	<xsl:variable name="lang" select="$request/@lang"/>
	<xsl:variable name="sys_params" select="/root/sys_params"/>
	<xsl:variable name="get_vars" select="/root/request/params/get/var"/>
	<xsl:variable name="main_prefix" select="/root/request/@main_prefix"/>
	<xsl:variable name="users_prefix" select="/root/request/@users_prefix"/>
	<xsl:variable name="save_prefix" select="/root/request/@save_prefix"/>
	<xsl:variable name="ajax_prefix" select="/root/request/@ajax_prefix"/>
	<xsl:variable name="debug" select="/root/request/@debug = 1"/>
	<xsl:variable name="pass_info" select="/root/pass_info"/>
	<xsl:variable name="user" select="/root/user"/>
	<xsl:variable name="reg_url" select="concat($main_prefix, '/reg/', '?retpath=', $request/@retpath_escaped)"/>
	<xsl:variable name="login_url" select="concat($main_prefix, '/login/', '?retpath=', $request/@retpath_escaped)"/>
	<xsl:variable name="logout_url" select="concat($save_prefix, '/logout/')"/>
</xsl:stylesheet>
