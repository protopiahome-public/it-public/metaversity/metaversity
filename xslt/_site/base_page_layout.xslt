<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:output indent="no" method="html" encoding="UTF-8" doctype-public=""/>
	<xsl:include href="base_layout.xslt"/>
	<xsl:include href="base_page.xslt"/>
	<xsl:include href="menu/site.menu.xslt"/>
	<xsl:template mode="css" match="/root">
		<link rel="stylesheet" type="text/css" href="{$main_prefix}/fonts/FontAwesome/font-awesome.min.css?v2"/>
		<link rel="stylesheet" type="text/css" href="{$main_prefix}/css/intmarkup.css?v59"/>
		<link rel="stylesheet" type="text/css" href="{$main_prefix}/css/metaversity.css?v55"/>
		<meta name="viewport" content="width=device-width; initial-scale=1; user-scalable=0;"/>
		<!--<meta name="apple-mobile-web-app-capable" content="yes"/>-->
	</xsl:template>
	<xsl:template mode="js" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/jquery.cookie.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/touch-issues.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/intmarkup-functions.js?v17"/>
		<script type="text/javascript" src="{$main_prefix}/js/intmarkup-xpost.js?v4"/>
		<script type="text/javascript" src="{$main_prefix}/js/intmarkup-layout.js?v22"/>
		<script type="text/javascript" src="{$main_prefix}/js/xlightbox.js?v5"/>
		<script type="text/javascript" src="{$main_prefix}/js/init.js?v6"/>
	</xsl:template>
	<xsl:template mode="body_class" match="/root">with_menu_left with_menu_left__reserve with_menu_top</xsl:template>
	<xsl:template mode="top_level_markup" match="/root">
		<xsl:apply-templates mode="menu_top" select="/root"/>
		<xsl:apply-templates mode="menu_left" select="/root"/>
	</xsl:template>
	<xsl:template mode="footer" match="/root">
		<div class="footer">
			<hr/>
			<xsl:if test="$request/@translate_content = 1 and $top_section != 'lang'">
				<p>
					<div id="google_translate_element"/>
					<xsl:variable name="other_lang" select="lang[@code != $lang]/@code"/>
					<script type="text/javascript">
						function googleTranslateElementInit() {
							new google.translate.TranslateElement({pageLanguage: '<xsl:value-of select="$other_lang"/>', includedLanguages: '<xsl:value-of select="$lang"/>', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL, autoDisplay: true, multilanguagePage: true, gaTrack: true, gaId: '<xsl:value-of select="$sys_params/@ga_tracking_id"/>'}, 'google_translate_element');
						}
					</script>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"/>
				</p>
			</xsl:if>
			<xsl:variable name="is_my_profile" select="$top_section = 'users' and not(/root/users) and $user/@id = /root/user_short[1]/@id"/>
			<xsl:apply-templates mode="menu_footer" select="/root"/>
		</div>
	</xsl:template>
	<xsl:template match="/root" mode="debug_info">
		<xsl:if test="$debug">
			<div class="debug_info">
				<span class="_item">
					<xsl:text>cache: </xsl:text>
					<xsl:for-each select="/root/stat/cache/state[@count != 0]">
						<i class="{@name} nobr" title="{@controls}">
							<xsl:value-of select="@name"/>
							<xsl:text> (</xsl:text>
							<xsl:value-of select="@count"/>
							<xsl:text>)</xsl:text>
						</i>
						<xsl:if test="position() != last()">, </xsl:if>
					</xsl:for-each>
				</span>
				<xsl:text> </xsl:text>
				<span class="_item nobr">
					<xsl:text>db queries: </xsl:text>
					<b>
						<xsl:value-of select="/root/stat/db/@query_count"/>
					</b>
				</span>
				<xsl:text> </xsl:text>
				<span class="_item nobr">
					<xsl:text>time: </xsl:text>
					<b>
						<xsl:value-of select="substring(/root/stat/time/@load, 1, 5)"/>
					</b>
				</span>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="errors" match="/root">
		<div id="error_general" class="hide" data-title="{php:function('trans', 'Error')}">
			<p>
				<xsl:value-of select="php:function('trans', 'Error has happened. Please try again or write to support if it happens again:')"/>
				<xsl:text> </xsl:text>
				<a class="email" href="mailto:{$sys_params/@info_email_obscured}">
					<xsl:value-of select="$sys_params/@info_email_obscured"/>
				</a>
			</p>
		</div>
		<div id="error_ajax" class="hide" data-title="{php:function('trans', 'Error')}">
			<p>
				<xsl:value-of select="php:function('trans', 'Error has happened while connecting to the server. Please check your internet connection and try again.')"/>
			</p>
		</div>
		<div id="error_access_denied" class="hide" data-title="{php:function('trans', 'Access denied')}">
			<p>
				<xsl:value-of select="php:function('trans', 'You do not have permissions to complete the operation. The data was not saved.')"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template mode="loading" match="/root">
		<div class="xpost_loading hide">
			<div class="js_content _content"/>
		</div>
	</xsl:template>
	<xsl:template mode="lightbox" match="/root">
		<div id="js_lightbox" class="lightbox dn">
			<div class="lightbox_overlay"/>
			<div id="js_lightbox_dialog_box" class="lightbox_dialog_box">
				<div id="js_lightbox_dialog" class="lightbox_dialog">
					<div class="lightbox_head">
						<div id="js_lightbox_close" class="_close">
							<i class="_close_icon fa fa-times"/>
						</div>
						<div id="js_lightbox_title" class="_title"/>
					</div>
					<div class="lightbox_main">
						<div id="js_lightbox_content" class="lightbox_content"/>
						<div id="js_lightbox_button_box" class="p center">
							<button class="ctrl ctrl__button" id="js_lightbox_button"/>
							<button class="ctrl ctrl__button ctrl__button__cancel ctrl__button__right" id="js_lightbox_button_cancel">
								<xsl:value-of select="php:function('trans', 'Cancel', 'VERB')"/>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
