<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="../users/include/user_common.inc.xslt"/>
	<xsl:include href="include/user_results_access_denied.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/results/')"/>
	<xsl:template match="math_user_streams"/>
	<xsl:template match="user_position_results_streams">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<xsl:value-of select="php:function('trans', 'Results')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:for-each select="/root/math_user_streams">
			<xsl:if test="not(stream)">
				<p>
					<xsl:value-of select="php:function('trans', 'There are no results because the user has not received any marks yet.')"/>
				</p>
			</xsl:if>
			<xsl:if test="stream">
				<xsl:for-each select="stream">
					<xsl:variable name="current_stream" select="/root/stream_short[@id = current()/@id][1]"/>
					<div class="list_item">
						<xsl:if test="position() = 1">
							<xsl:attribute name="class">list_item list_item__first</xsl:attribute>
						</xsl:if>
						<div class="list_item_title">
							<a href="{$module_url}{@name}/">
								<xsl:value-of select="@title"/>
							</a>
							<xsl:text> (</xsl:text>
							<xsl:value-of select="@title_short"/>
							<xsl:text>)</xsl:text>
						</div>
						<div class="props">
							<xsl:if test="@is_central = 1">
								<div class="prop">
									<xsl:value-of select="php:function('trans', 'Contains competences common for all streams. Maintained by the Metaversity developers.')"/>
								</div>
							</xsl:if>
							<div class="prop">
								<span class="fa fa-check-circle-o"/>
								<xsl:text> </xsl:text>
								<span class="key">
									<xsl:value-of select="php:function('trans', 'Marks', 'COUNT')"/>
									<xsl:text>: </xsl:text>
								</span>
								<a href="{$current_user/@url}log/?filter[stream]={@id}">
									<xsl:value-of select="@mark_count_calc"/>
								</a>
							</div>
							<xsl:choose>
								<xsl:when test="@access_denied = 1">
									<div class="prop">
										<i class="fa fa-ban red"/>
										<xsl:text> </xsl:text>
										<xsl:call-template name="draw_user_results_access_denied">
											<xsl:with-param name="results_access_level" select="../@results_access_level"/>
										</xsl:call-template>
									</div>
								</xsl:when>
								<xsl:otherwise>
									<xsl:if test="/root/math_user_position_results[@stream_id = current()/@id]/position[position() &lt;= 3]">
										<div class="prop">
											<span class="fa fa-star"/>
											<xsl:text> </xsl:text>
											<span class="key">
												<xsl:value-of select="php:function('trans', 'Best positions')"/>
												<xsl:text>: </xsl:text>
											</span>
											<xsl:for-each select="/root/math_user_position_results[@stream_id = current()/@id]/position[position() &lt;= 3]">
												<a href="{$module_url}{$current_stream/@name}/{@id}/">
													<xsl:value-of select="@title"/>
												</a>
												<xsl:call-template name="trans">
													<xsl:with-param name="html">_DASH_</xsl:with-param>
												</xsl:call-template>
												<span class="nobr hl">
													<xsl:value-of select="@match"/>
													<xsl:text>%</xsl:text>
												</span>
												<xsl:choose>
													<xsl:when test="position() != last()">, </xsl:when>
													<xsl:otherwise>.</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</div>
									</xsl:if>
									<xsl:if test="/root/math_user_position_results[@stream_id = current()/@id]/position[@is_focus = 1]">
										<div class="prop">
											<span class="fa fa-crosshairs"/>
											<xsl:text> </xsl:text>
											<span class="key">
												<xsl:value-of select="php:function('trans', 'Focus positions')"/>
												<xsl:text>: </xsl:text>
											</span>
											<xsl:for-each select="/root/math_user_position_results[@stream_id = current()/@id]/position[@is_focus = 1]">
												<a href="{$module_url}{$current_stream/@name}/{@id}/">
													<xsl:value-of select="@title"/>
												</a>
												<xsl:call-template name="trans">
													<xsl:with-param name="html">_DASH_</xsl:with-param>
												</xsl:call-template>
												<span class="nobr hl">
													<xsl:value-of select="@match"/>
													<xsl:text>%</xsl:text>
												</span>
												<xsl:choose>
													<xsl:when test="position() != last()">, </xsl:when>
													<xsl:otherwise>.</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</div>
									</xsl:if>
									<div class="prop">
										<span class="fa fa-certificate"/>
										<xsl:text> </xsl:text>
										<a href="{$module_url}{@name}/">
											<xsl:value-of select="php:function('trans', 'All positions')"/>
										</a>
									</div>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</div>
				</xsl:for-each>
			</xsl:if>
			<div class="list_item">
				<div class="list_item_title list_item_title__smaller">
					<a href="{$module_url}all/">
						<xsl:value-of select="php:function('trans', 'All streams')"/>
					</a>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Results')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
