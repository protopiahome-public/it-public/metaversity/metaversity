<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="include/user_position_results_positions.inc.xslt"/>
	<xsl:include href="../positions/include/position_credits.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="false()"/>
	<xsl:variable name="current_stream" select="/root/stream_short[1]"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/results/', $current_stream/@name, '/')"/>
	<xsl:template match="user_position_results_positions">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<a href="{$module_url}../">
					<xsl:value-of select="php:function('trans', 'Results')"/>
				</a>
				<xsl:text> / </xsl:text>
				<xsl:value-of select="$current_stream/@title"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="$current_stream/@title_short"/>
				<xsl:text>)</xsl:text>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:call-template name="draw_user_position_results_links"/>
		<xsl:for-each select="/root/math_user_position_results">
			<xsl:call-template name="draw_user_position_results_positions"/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_stream/@title"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text>)</xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Results')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
