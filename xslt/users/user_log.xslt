<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="../activities/include/activity_common.inc.xslt"/>
	<xsl:include href="include/user_marks.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'log'"/>
	<xsl:variable name="user_section_main_page" select="/root/user_log/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/log/')"/>
	<xsl:template match="user_log">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<span class="for_head_dd">
					<xsl:value-of select="php:function('trans', 'Roles and Marks')"/>
					<xsl:text>:</xsl:text>
				</span>
				<xsl:apply-templates select="filters" mode="filter_one">
					<xsl:with-param name="name" select="'stream'"/>
				</xsl:apply-templates>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:apply-templates select="filters" mode="filters_show"/>
		<xsl:if test="pages/@row_count &gt; 0">
			<p>
				<xsl:call-template name="count_case_lang">
					<xsl:with-param name="number" select="pages/@row_count"/>
					<xsl:with-param name="word" select="'Found'"/>
				</xsl:call-template>
				<xsl:text> </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text> </xsl:text>
				<xsl:call-template name="count_case_lang">
					<xsl:with-param name="number" select="pages/@row_count"/>
					<xsl:with-param name="word" select="'record'"/>
				</xsl:call-template>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="php:function('trans', 'including', 'among')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="@mark_count"/>
				<xsl:text> </xsl:text>
				<xsl:call-template name="count_case_lang">
					<xsl:with-param name="number" select="@mark_count"/>
					<xsl:with-param name="word" select="'mark'"/>
				</xsl:call-template>
				<xsl:text>.</xsl:text>
				<xsl:if test="news_item">
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'Sorted by update time.')"/>
				</xsl:if>
			</p>
		</xsl:if>
		<xsl:call-template name="draw_user_marks">
			<xsl:with-param name="marks" select="news_item"/>
			<xsl:with-param name="draw_status" select="true()"/>
			<xsl:with-param name="show_not_found_message" select="true()"/>
		</xsl:call-template>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Roles and Marks')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
