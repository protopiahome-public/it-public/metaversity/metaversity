<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="include/user_competence_results.inc.xslt"/>
	<xsl:include href="include/user_results_access_denied.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'competences'"/>
	<xsl:variable name="user_section_main_page" select="false()"/>
	<xsl:variable name="current_stream" select="/root/stream_short[1]"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/competences/', $current_stream/@name, '/')"/>
	<xsl:template match="error_403">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<a href="{$module_url}../">
					<xsl:value-of select="php:function('trans', 'Competences')"/>
				</a>
				<xsl:text> / </xsl:text>
				<xsl:value-of select="$current_stream/@title"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="$current_stream/@title_short"/>
				<xsl:text>)</xsl:text>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:call-template name="draw_user_competence_results_links"/>
		<div class="access_denied">
			<i class="fa fa-ban red"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'Access denied')"/>
		</div>
		<p>
			<xsl:call-template name="draw_user_results_access_denied"/>
		</p>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_stream/@title"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text>)</xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Competences')"/>
		<xsl:text> </xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
