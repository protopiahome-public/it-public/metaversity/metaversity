<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_user_competence_results_links">
		<div class="prop p">
			<a class="underline_inner_span" href="{$current_stream/@url}">
				<span>
					<xsl:value-of select="php:function('trans', 'Stream page')"/>
				</span>
				<xsl:text> </xsl:text>
				<i class="fa fa-external-link"/>
			</a>
		</div>
	</xsl:template>
	<xsl:template name="draw_user_competence_results_iconic_stat">
		<xsl:param name="align_left" select="false()"/>
		<xsl:param name="draw_empty" select="false()"/>
		<xsl:if test="@mark_count and @rater_count or $draw_empty">
			<span title="{php:function('trans', 'Marks and raters', 'COUNT')}">
				<xsl:attribute name="class">
					<xsl:text>result_stat prop</xsl:text>
					<xsl:if test="$align_left"> result_stat__left</xsl:if>
				</xsl:attribute>
				<span class="_item _item__3digits">
					<xsl:if test="@mark_count and @rater_count">
						<xsl:value-of select="@mark_count"/>
						<i class="_icon fa fa-check-circle-o"/>
					</xsl:if>
				</span>
				<span class="_item _item__2digits _item__last">
					<xsl:if test="@mark_count and @rater_count">
						<xsl:value-of select="@rater_count"/>
						<i class="_icon fa fa-user"/>
					</xsl:if>
				</span>
			</span>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
