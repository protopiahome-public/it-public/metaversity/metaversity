<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template mode="users_th_left" match="users"/>
	<xsl:template mode="users_td_left" match="user"/>
	<xsl:template mode="users_th_right" match="users"/>
	<xsl:template mode="users_td_right" match="user"/>
	<xsl:template name="draw_users">
		<xsl:param name="small" select="false()"/>
		<xsl:param name="narrow_move_marks" select="false()"/>
		<xsl:variable name="narrow_move_marks_hide_class">
			<xsl:if test="$narrow_move_marks"> hide600</xsl:if>
		</xsl:variable>
		<xsl:variable name="narrow_move_marks_show_class">
			<xsl:if test="$narrow_move_marks"> show600</xsl:if>
		</xsl:variable>
		<xsl:apply-templates select="filters" mode="filters_show"/>
		<xsl:if test="not(user)">
			<p>
				<xsl:value-of select="php:function('trans', 'Nobody matches the selected search criteria.')"/>
			</p>
		</xsl:if>
		<xsl:if test="user">
			<!--<p>
				<xsl:value-of select="php:function('trans', 'Users shown:')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="pages/@from_row"/>
				<xsl:text>&#8211;</xsl:text>
				<xsl:value-of select="pages/@to_row"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'of')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>.</xsl:text>
			</p>-->
			<xsl:if test="filters/filter[@name = 'stream']/@is_active = 1">
				<div class="box p">
					<p>
						<xsl:value-of select="php:function('trans', 'The number of marks is shown for the selected stream.')"/>
					</p>
				</div>
			</xsl:if>
			<table class="table">
				<tr>
					<xsl:apply-templates mode="users_th_left" select="."/>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'name'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Name')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'mark-count'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Marks', 'COUNT')"/>
						<xsl:with-param name="class" select="concat('w60 center', $narrow_move_marks_show_class)"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Streams')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'reg-time'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Registration')"/>
						<xsl:with-param name="class" select="'p6 show1000'"/>
					</xsl:call-template>
					<xsl:apply-templates mode="users_th_right" select="."/>
				</tr>
				<xsl:variable name="cell_class">
					<xsl:choose>
						<xsl:when test="$small">cell_user</xsl:when>
						<xsl:otherwise>cell_user_big</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:for-each select="user">
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="/root/user_short[@id = current()/@id]">
						<xsl:variable name="streams">
							<xsl:for-each select="/root/stream_user_links/user[@id = current()/@id]/stream">
								<xsl:variable name="context2" select="."/>
								<xsl:for-each select="/root/stream_short[@id = current()/@id][1]">
									<a href="{@url}" title="{php:function('trans', 'Marks', 'COUNT')}: {$context2/@mark_count_calc}">
										<xsl:value-of select="@title_short"/>
									</a>
								</xsl:for-each>
								<xsl:if test="position() != last()">, </xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<tr>
							<xsl:apply-templates mode="users_td_left" select="$context"/>
							<td class="{$cell_class}">
								<div class="_img">
									<a href="{$users_prefix}/{@login}/">
										<xsl:choose>
											<xsl:when test="$small">
												<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
											</xsl:when>
											<xsl:otherwise>
												<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.6)}" height="{round(photo_big/@height * 0.6)}" alt="{@login}"/>
											</xsl:otherwise>
										</xsl:choose>
									</a>
								</div>
								<div class="_title">
									<a href="{$users_prefix}/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
									<span class="_login">
										<xsl:text> (</xsl:text>
										<xsl:value-of select="@login"/>
										<xsl:text>)</xsl:text>
									</span>
								</div>
								<div class="_city">
									<xsl:value-of select="@city_title"/>
								</div>
								<div class="_lines">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Streams')"/>
										<xsl:with-param name="html" select="$streams"/>
										<xsl:with-param name="class" select="'_line hide600'"/>
									</xsl:call-template>
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Registration')"/>
										<xsl:with-param name="text">
											<xsl:call-template name="get_date">
												<xsl:with-param name="datetime" select="@add_time"/>
											</xsl:call-template>
										</xsl:with-param>
										<xsl:with-param name="class" select="'_line hide1000'"/>
									</xsl:call-template>
									<xsl:apply-templates mode="users_line" select="$context"/>
								</div>
							</td>
							<td class="cell_num w60 p_sort center {$narrow_move_marks_show_class}">
								<xsl:value-of select="$context/@mark_count_calc"/>
							</td>
							<td class="cell_text maxw250 p6 show600">
								<xsl:copy-of select="$streams"/>
							</td>
							<td class="cell_text cell_text__smaller p6 show1000 nobr">
								<xsl:call-template name="get_date">
									<xsl:with-param name="datetime" select="@add_time"/>
								</xsl:call-template>
							</td>
							<xsl:apply-templates mode="users_td_right" select="$context"/>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
