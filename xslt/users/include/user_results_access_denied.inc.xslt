<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_user_results_access_denied">
		<xsl:param name="results_access_level" select="/root/user_current/@results_access_level"/>
		<xsl:choose>
			<xsl:when test="$results_access_level = 'any_moderator'">
				<xsl:value-of select="php:function('trans', 'This user provides access to results only for moderators.')"/>
			</xsl:when>
			<xsl:when test="$results_access_level = 'stream_students'">
				<xsl:value-of select="php:function('trans', 'This user provides access to results only for students of this stream and for moderators.')"/>
			</xsl:when>
			<xsl:when test="$results_access_level = 'registered'">
				<xsl:value-of select="php:function('trans', 'This user provides access to results only for registered users.')"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
