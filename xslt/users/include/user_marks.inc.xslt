<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_user_marks">
		<xsl:param name="marks" select="mark"/>
		<xsl:param name="draw_status" select="false()"/>
		<xsl:param name="show_not_found_message" select="true()"/>
		<xsl:if test="not($marks) and $show_not_found_message">
			<p>
				<xsl:value-of select="php:function('trans', 'No marks were found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="$marks">
			<table class="table w100p maxw600">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Role / Competence')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Mark')"/>
						<xsl:with-param name="class" select="'pl10 pr6'"/>
					</xsl:call-template>
				</tr>
				<xsl:for-each select="$marks">
					<xsl:variable name="context" select="."/>
					<xsl:variable name="stream" select="/root/stream_short[@id = current()/@stream_id][1]"/>
					<xsl:variable name="activity" select="/root/activity_short[@id = current()/@activity_id][1]"/>
					<tr>
						<td class="cell_expand pr10">
							<span class="cell_expand_icon fa"/>
							<span class="cell_expand_text for_icon_right_adjacent">
								<xsl:choose>
									<xsl:when test="@role_id">
										<xsl:value-of select="/root/role_short[@id = current()/@role_id]/@title"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>[</xsl:text>
										<xsl:value-of select="php:function('trans', 'Competence')"/>
										<xsl:text>] </xsl:text>
										<xsl:call-template name="draw_competence">
											<xsl:with-param name="id" select="@competence_id"/>
											<xsl:with-param name="title" select="@competence_title"/>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</span>
							<xsl:if test="$draw_status">
								<span class="cell_expand_right_icon expanded_hide">
									<xsl:call-template name="draw_activity_participant_status">
										<xsl:with-param name="mode" select="'icon'"/>
										<xsl:with-param name="no_default" select="true()"/>
										<xsl:with-param name="you" select="$user/@id = $current_user/@id"/>
									</xsl:call-template>
								</span>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="@comment != ''">
									<div class="props">
										<xsl:call-template name="draw_kv">
											<xsl:with-param name="key" select="php:function('trans', 'Comment')"/>
											<xsl:with-param name="text" select="@comment"/>
											<xsl:with-param name="class" select="'prop'"/>
										</xsl:call-template>
									</div>
								</xsl:when>
								<xsl:when test="@mark = 'ACCESS_DENIED'">
									<div class="props">
										<div class="prop">
											<xsl:value-of select="php:function('trans', 'The user restricted access to this mark.')"/>
										</div>
									</div>
								</xsl:when>
							</xsl:choose>
						</td>
						<td class="cell_mark w74 pl10 pr6">
							<xsl:if test="@mark != ''">
								<div class="rel">
									<span class="abs">
										<xsl:call-template name="draw_mark">
											<xsl:with-param name="weight" select="$activity/@weight"/>
										</xsl:call-template>
									</span>
								</div>
							</xsl:if>
						</td>
					</tr>
					<tr class="hide">
						<td class="cell_expandable" colspan="2">
							<div class="props">
								<xsl:if test="@competence_id">
									<div class="prop">
										<a href="{$current_user/@url}competences/{$stream/@name}/{@competence_id}/">
											<xsl:value-of select="php:function('trans', 'Final result')"/>
										</a>
									</div>
								</xsl:if>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Activity', 'WHERE')"/>
									<xsl:with-param name="html">
										<a href="{$stream/@url}">
											<xsl:value-of select="$stream/@title_short"/>
										</a>
										<span class="arrow"> → </span>
										<a href="{$stream/@url}activities/{$activity/@id}/" class="for_icon_right">
											<xsl:value-of select="$activity/@title"/>
										</a>
										<xsl:text> </xsl:text>
										<span class="gray nobr">
											<xsl:call-template name="draw_activity_date_short">
												<xsl:with-param name="context" select="$activity"/>
											</xsl:call-template>
										</span>
									</xsl:with-param>
									<xsl:with-param name="class" select="'prop'"/>
								</xsl:call-template>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key">
										<xsl:choose>
											<xsl:when test="@mark_time">
												<xsl:value-of select="php:function('trans', 'Mark date')"/>
											</xsl:when>
											<xsl:when test="@news_type = 'mark'">
												<xsl:value-of select="php:function('trans', 'Mark date')"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="php:function('trans', 'Status change date')"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="html">
										<xsl:call-template name="get_full_datetime">
											<xsl:with-param name="datetime">
												<xsl:choose>
													<xsl:when test="@mark_time != ''">
														<xsl:value-of select="@mark_time"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="@time"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:with-param>
											<xsl:with-param name="with_week_day" select="true()"/>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="class" select="'prop'"/>
								</xsl:call-template>
								<!-- @todo here should be only @rater_user_id, see also @todo at all_competence_results_loader -->
								<xsl:for-each select="/root/stream_user_short[@id = current()/@changer_user_id or @id = current()/@rater_user_id][1]">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key">
											<xsl:choose>
												<xsl:when test="$context/@news_type = 'activity_status'">
													<xsl:choose>
														<xsl:when test="@sex = 'f'">
															<xsl:value-of select="php:function('trans', 'Status changed by', 'FEMALE')"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="php:function('trans', 'Status changed by', 'MALE')"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:when>
												<xsl:otherwise>
													<xsl:choose>
														<xsl:when test="@sex = 'f'">
															<xsl:value-of select="php:function('trans', 'Rated by', 'FEMALE')"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="php:function('trans', 'Rated by', 'MALE')"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:with-param>
										<xsl:with-param name="html">
											<a href="{@url}" class="inline_photo">
												<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
											</a>
											<xsl:text> </xsl:text>
											<a href="{@url}">
												<xsl:value-of select="@visible_name"/>
											</a>
										</xsl:with-param>
										<xsl:with-param name="class" select="'prop'"/>
									</xsl:call-template>
								</xsl:for-each>
								<xsl:if test="$draw_status">
									<div class="prop">
										<xsl:call-template name="draw_activity_participant_status">
											<xsl:with-param name="mode" select="'full'"/>
											<xsl:with-param name="no_default" select="false()"/>
											<xsl:with-param name="you" select="$user/@id = $current_user/@id"/>
										</xsl:call-template>
									</div>
								</xsl:if>
								<xsl:if test="/root/role_short[@id = current()/@role_id]/descr/div">
									<div class="prop">
										<span class="js_toggle clickable" data-toggle-for="role-{position()}">
											<span class="dashed">
												<xsl:value-of select="php:function('trans', 'Role description')"/>
											</span>
											<xsl:text> </xsl:text>
											<span class="details">...</span>
										</span>
									</div>
									<div id="role-{position()}" class="hide">
										<xsl:copy-of select="/root/role_short[@id = current()/@role_id]/descr/div"/>
									</div>
								</xsl:if>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
