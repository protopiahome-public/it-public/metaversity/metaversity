<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:variable name="current_user" select="/root/user_short[1]"/>
	<xsl:variable name="initial_section">
		<xsl:choose>
			<xsl:when test="$current_user/@id = $user/@id">news</xsl:when>
			<xsl:otherwise>profile</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="initial_url">
		<xsl:choose>
			<xsl:when test="$current_user/@id = $user/@id">
				<xsl:value-of select="concat($main_prefix, '/news/')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$current_user/@url"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="header_title">
		<xsl:choose>
			<xsl:when test="$current_user/@id = $user/@id">
				<xsl:value-of select="php:function('trans', 'My profile')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="php:function('trans', 'Users')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="header_url">
		<xsl:choose>
			<xsl:when test="$current_user/@id = $user/@id and $user_section = $initial_section and $user_section_main_page"/>
			<xsl:when test="$current_user/@id = $user/@id">
				<xsl:value-of select="concat($main_prefix, '/news/')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($users_prefix, '/')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template mode="body_class" match="/root">with_menu_left with_menu_top</xsl:template>
	<xsl:template mode="menu_left" match="/root">
		<div class="menu_left menu_left__white">
			<div class="_submenu">
				<div class="js_btn_menu_left_open_main menu_item menu_item__clickable">
					<span class="_icon fa fa-home"/>
					<xsl:text> </xsl:text>
					<span class="_text _text__dashed">
						<xsl:value-of select="php:function('trans', 'Main menu')"/>
					</span>
					<xsl:text> </xsl:text>
					<span class="_down fa fa-caret-down"/>
				</div>
				<div class="js_menu_left_main _submenu_contents hide">
					<xsl:apply-templates mode="menu_items_main" select="/root"/>
				</div>
			</div>
			<div class="_submenu">
				<xsl:choose>
					<xsl:when test="$user/@id">
						<xsl:apply-templates mode="menu_items_user_title" select="/root">
							<xsl:with-param name="clickable" select="true()"/>
						</xsl:apply-templates>
						<div class="js_menu_left_user _submenu_contents hide">
							<xsl:apply-templates mode="menu_items_lang" select="/root"/>
							<xsl:apply-templates mode="menu_items_user" select="/root"/>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates mode="menu_items_lang" select="/root"/>
						<xsl:apply-templates mode="menu_items_guest" select="/root"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div class="menu_left_user_photo">
				<span class="_img">
					<xsl:for-each select="$current_user[1]">
						<xsl:variable name="ratio">
							<xsl:choose>
								<xsl:when test="photo_large/@width &gt; 186">
									<xsl:value-of select="round(186 div photo_large/@width * 1000)"/>
								</xsl:when>
								<xsl:otherwise>1000</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$user_section = $initial_section and $user_section_main_page">
								<img src="{photo_large/@url}" width="{round(photo_large/@width * $ratio div 1000)}" height="{round(photo_large/@height * $ratio div 1000)}" alt="{@login}"/>
							</xsl:when>
							<xsl:otherwise>
								<a href="{$initial_url}">
									<img src="{photo_large/@url}" width="{round(photo_large/@width * $ratio div 1000)}" height="{round(photo_large/@height * $ratio div 1000)}" alt="{@login}"/>
								</a>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</span>
			</div>
			<xsl:if test="$current_user/@id = $user/@id">
				<xsl:choose>
					<xsl:when test="$user_section = 'news' and $user_section_main_page">
						<span class="menu_item menu_item__current">
							<span class="_icon fa fa-bell-o"/>
							<xsl:text> </xsl:text>
							<span class="_text">
								<xsl:value-of select="php:function('trans', 'News')"/>
							</span>
						</span>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$main_prefix}/news/" class="menu_item">
							<xsl:if test="$user_section = 'news'">
								<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
							</xsl:if>
							<span class="_icon fa fa-bell-o"/>
							<xsl:text> </xsl:text>
							<span class="_text">
								<xsl:value-of select="php:function('trans', 'News')"/>
							</span>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$user_section = 'profile' and $user_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-info-circle"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Profile')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user/@url}" class="menu_item">
						<xsl:if test="$user_section = 'profile'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-info-circle"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Profile')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$user_section = 'results' and $user_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-certificate"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Results')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user/@url}results/" class="menu_item">
						<xsl:if test="$user_section = 'results'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-certificate"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Results')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$user_section = 'competences' and $user_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon _icon__14 fa fa-list"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Competences')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user/@url}competences/" class="menu_item">
						<xsl:if test="$user_section = 'competences'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon _icon__14 fa fa-list"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Competences')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$user_section = 'log' and $user_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-check"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Roles and Marks')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_user/@url}log/" class="menu_item">
						<xsl:if test="$user_section = 'log'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-check"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Roles and Marks')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="draw_user_head">
		<h1>
			<span class="for_icon_right">
				<xsl:choose>
					<xsl:when test="$user_section = $initial_section and $user_section_main_page">
						<xsl:value-of select="$current_user/@visible_name"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$initial_url}">
							<xsl:value-of select="$current_user/@visible_name"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<span class="user_head_city">
				<span class="_icon fa fa-map-marker"/>
				<span class="_title">
					<xsl:value-of select="$current_user/@city_title"/>
				</span>
			</span>
		</h1>
	</xsl:template>
</xsl:stylesheet>
