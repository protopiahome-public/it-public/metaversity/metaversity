<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_user_streams">
		<xsl:if test="not(stream)">
			<p>
				<xsl:value-of select="php:function('trans', 'This user has not joined any stream yet.')"/>
			</p>
		</xsl:if>
		<xsl:if test="stream">
			<xsl:for-each select="stream">
				<xsl:variable name="current_stream" select="/root/stream_short[@id = current()/@id][1]"/>
				<div class="list_item list_item__smaller">
					<xsl:if test="position() = 1">
						<xsl:attribute name="class">list_item list_item__smaller list_item__first</xsl:attribute>
					</xsl:if>
					<div class="list_item_title list_item_title__smaller">
						<a href="{$current_stream/@url}">
							<xsl:value-of select="$current_stream/@title"/>
						</a>
						<xsl:text> (</xsl:text>
						<xsl:value-of select="$current_stream/@title_short"/>
						<xsl:text>)</xsl:text>
					</div>
					<div class="props">
						<xsl:if test="substring(@add_time, 1, 5) != '0000-'">
							<div class="prop">
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key">
										<xsl:choose>
											<xsl:when test="$current_user/@sex = 'f'">
												<xsl:value-of select="php:function('trans', 'Joined on', 'FEMALE')"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="php:function('trans', 'Joined on', 'MALE')"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="html">
										<xsl:call-template name="get_full_date">
											<xsl:with-param name="datetime" select="@add_time"/>
											<xsl:with-param name="with_week_day" select="true()"/>
										</xsl:call-template>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</xsl:if>
						<div class="prop">
							<xsl:call-template name="draw_stream_status">
								<xsl:with-param name="status" select="@status"/>
								<xsl:with-param name="stream_url" select="$current_stream/@url"/>
								<xsl:with-param name="you" select="$current_user/@id = $user/@id"/>
							</xsl:call-template>
							<xsl:if test="@group_title != ''">
								<span class="prop_sep"> </span>
								<span class="prop_sep"> </span>
								<span class="dark_gray">
									<xsl:value-of select="php:function('trans', 'Group')"/>
									<xsl:text>: </xsl:text>
									<xsl:value-of select="@group_title"/>
								</span>
							</xsl:if>
						</div>
						<xsl:if test="positions/position">
							<div class="prop">
								<span class="fa fa-crosshairs"/>
								<xsl:text> </xsl:text>
								<span class="key">
									<xsl:value-of select="php:function('trans', 'Focus positions')"/>
									<xsl:text>: </xsl:text>
								</span>
								<xsl:for-each select="positions/position">
									<xsl:value-of select="@title"/>
									<xsl:choose>
										<xsl:when test="position() != last()">, </xsl:when>
										<xsl:otherwise>.</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</div>
						</xsl:if>
						<div class="prop prop__full_wrap">
							<span class="fa fa-check-circle-o"/>
							<xsl:text> </xsl:text>
							<span class="key">
								<xsl:value-of select="php:function('trans', 'Marks', 'COUNT')"/>
								<xsl:text>: </xsl:text>
							</span>
							<a href="{$current_user/@url}log/?filter[stream]={@id}">
								<xsl:value-of select="@mark_count_calc"/>
							</a>
							<span class="prop_sep"> </span>
							<span class="nobr">
								<span class="fa fa-certificate"/>
								<xsl:text> </xsl:text>
								<a href="{$current_user/@url}results/{$current_stream/@name}/">
									<xsl:value-of select="php:function('trans', 'Results')"/>
								</a>
								<span class="prop_sep"> </span>
							</span>
							<span class="nobr">
								<span class="fa fa-list"/>
								<xsl:text> </xsl:text>
								<a href="{$current_user/@url}competences/{$current_stream/@name}/">
									<xsl:value-of select="php:function('trans', 'Competences')"/>
								</a>
							</span>
						</div>
					</div>
				</div>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
