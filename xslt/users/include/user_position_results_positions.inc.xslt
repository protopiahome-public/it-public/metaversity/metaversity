<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_user_position_results_links">
		<div class="prop p">
			<a class="underline_inner_span" href="{$current_stream/@url}">
				<span>
					<xsl:value-of select="php:function('trans', 'Stream page')"/>
				</span>
				<xsl:text> </xsl:text>
				<i class="fa fa-external-link"/>
			</a>
		</div>
	</xsl:template>
	<xsl:template name="draw_user_position_results_positions">
		<xsl:if test="not(position)">
			<p>
				<xsl:value-of select="php:function('trans', 'No positions were found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="position">
			<table class="table maxw600">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Position')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'match'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Match')"/>
						<xsl:with-param name="class" select="'p6 center'"/>
					</xsl:call-template>
				</tr>
				<xsl:for-each select="position">
					<tr>
						<td class="cell_text">
							<a href="{$module_url}{@id}/">
								<xsl:if test="@is_focus = 1">
									<xsl:attribute name="class">for_icon_right</xsl:attribute>
								</xsl:if>
								<xsl:choose>
									<xsl:when test="@is_focus = 1">
										<b>
											<xsl:value-of select="@title"/>
										</b>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@title"/>
									</xsl:otherwise>
								</xsl:choose>
							</a>
							<xsl:if test="@is_focus = 1">
								<span class="smaller nobr hl">
									<i class="fa fa-crosshairs middle"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="php:function('trans', 'in the focus')"/>
								</span>
							</xsl:if>
							<xsl:call-template name="draw_position_credit">
								<xsl:with-param name="position_credit_min_match" select="$current_stream/@position_credit_min_match"/>
								<xsl:with-param name="class" select="'props'"/>
								<xsl:with-param name="line_class" select="'prop'"/>
							</xsl:call-template>
						</td>
						<td class="cell_num p6 center">
							<xsl:value-of select="@match"/>
							<xsl:text>%</xsl:text>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
