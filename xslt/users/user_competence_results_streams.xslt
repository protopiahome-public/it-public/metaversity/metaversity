<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="../users/include/user_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'competences'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/competences/')"/>
	<xsl:template match="math_user_streams"/>
	<xsl:template match="user_competence_results_streams">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<xsl:value-of select="php:function('trans', 'Competences')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:for-each select="/root/math_user_streams">
			<xsl:if test="not(stream)">
				<p>
					<xsl:value-of select="php:function('trans', 'Nothing was found.')"/>
				</p>
			</xsl:if>
			<div class="box p">
				<p>
					<xsl:value-of select="php:function('trans', 'Number of the marks shown below does not take competence translators into consideration and reflects only activity of a student in the stream.')"/>
				</p>
			</div>
			<xsl:if test="stream">
				<table class="table maxw600">
					<tr>
						<xsl:call-template name="draw_th">
							<xsl:with-param name="name" select="'title'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Stream')"/>
							<xsl:with-param name="class" select="''"/>
						</xsl:call-template>
						<xsl:call-template name="draw_th">
							<xsl:with-param name="name" select="'marks'"/>
							<xsl:with-param name="title" select="php:function('trans', 'Marks', 'COUNT')"/>
							<xsl:with-param name="class" select="'p6 center'"/>
						</xsl:call-template>
					</tr>
					<xsl:for-each select="stream">
						<tr>
							<td class="cell_text">
								<a href="{$module_url}{@name}/">
									<xsl:value-of select="@title"/>
								</a>
								<xsl:text> (</xsl:text>
								<xsl:value-of select="@title_short"/>
								<xsl:text>)</xsl:text>
								<xsl:choose>
									<xsl:when test="@is_central = 1">
										<div class="props">
											<div class="prop smaller hl">
												<xsl:value-of select="php:function('trans', 'Contains competences common for all streams. Maintained by the Metaversity developers.')"/>
											</div>
										</div>
									</xsl:when>
								</xsl:choose>
							</td>
							<td class="cell_num p6 center">
								<xsl:value-of select="@mark_count_calc"/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
				<xsl:apply-templates select="pages"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Competences')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
