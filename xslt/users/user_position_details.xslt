<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="../positions/include/position_details.inc.xslt"/>
	<xsl:include href="../positions/include/position_credits.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="false()"/>
	<xsl:variable name="current_stream" select="/root/stream_short[1]"/>
	<xsl:variable name="current_stream_access" select="/root/stream_access[1]"/>
	<xsl:variable name="current_position" select="/root/position_full[1]"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/results/', $current_stream/@name, '/', /root/math_position_details/@position_id, '/')"/>
	<xsl:template match="math_user_competence_details"/>
	<xsl:template match="user_position_details">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<a href="{$module_url}../../">
					<xsl:value-of select="php:function('trans', 'Results')"/>
				</a>
				<xsl:text> / </xsl:text>
				<a href="{$module_url}../">
					<xsl:value-of select="$current_stream/@title_short"/>
				</a>
				<xsl:text> / </xsl:text>
				<xsl:value-of select="$current_position/@title"/>
			</h2>
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:for-each select="/root/math_position_details">
			<xsl:call-template name="draw_position_details_match"/>
			<div class="prop p">
				<a class="underline_inner_span" href="{$current_stream/@url}">
					<span>
						<xsl:value-of select="php:function('trans', 'Stream page')"/>
					</span>
					<xsl:text> </xsl:text>
					<i class="fa fa-external-link"/>
				</a>
				<xsl:if test="$current_stream_access/@has_admin_rights = 1">
					<xsl:text> | </xsl:text>
					<a class="underline_inner_span" href="{$current_stream/@url}admin/credits/users/{$current_user/@login}/{$current_position/@id}/">
						<span>
							<xsl:value-of select="php:function('trans', 'Position credit')"/>
						</span>
						<xsl:text> </xsl:text>
						<i class="fa fa-external-link"/>
					</a>
				</xsl:if>
			</div>
			<xsl:call-template name="draw_position_details"/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/rose.js?v5"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_position/@title"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text>)</xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Results')"/>
		<xsl:text> </xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
