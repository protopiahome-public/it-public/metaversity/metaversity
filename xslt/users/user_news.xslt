<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="../activities/include/activity_common.inc.xslt"/>
	<xsl:include href="../news/include/news.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'news'"/>
	<xsl:variable name="user_section_main_page" select="/root/user_news/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url">
		<xsl:choose>
			<xsl:when test="/root/user_news/@via_profile = 1">
				<xsl:value-of select="concat($users_prefix, '/', $current_user/@login, '/news/')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($main_prefix, '/news/')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="user_news">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<span class="for_head_dd">
					<xsl:value-of select="php:function('trans', 'News')"/>
					<xsl:text>:</xsl:text>
				</span>
				<xsl:apply-templates select="filters" mode="filter_one">
					<xsl:with-param name="name" select="'news_type'"/>
				</xsl:apply-templates>
			</h2>
			<div class="maxw800">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<!--<xsl:apply-templates select="filters" mode="filters_show"/>-->
		<xsl:call-template name="draw_news">
			<xsl:with-param name="current_user_url" select="$current_user/@url"/>
		</xsl:call-template>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'News')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
