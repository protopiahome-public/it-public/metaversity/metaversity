<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="../_site/include/tree.inc.xslt"/>
	<xsl:include href="include/user_competence_results.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'competences'"/>
	<xsl:variable name="user_section_main_page" select="false()"/>
	<xsl:variable name="current_stream" select="/root/stream_short[1]"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/competences/', $current_stream/@name, '/')"/>
	<xsl:template match="user_competence_results_tree">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<a href="{$module_url}../">
					<xsl:value-of select="php:function('trans', 'Competences')"/>
				</a>
				<xsl:text> / </xsl:text>
				<xsl:value-of select="$current_stream/@title"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="$current_stream/@title_short"/>
				<xsl:text>)</xsl:text>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:call-template name="draw_user_competence_results_links"/>
		<div class="box p">
			<p>
				<xsl:choose>
					<xsl:when test="$current_user/@id = $user/@id">
						<xsl:value-of select="php:function('trans', 'Required levels are based on your positions.')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="php:function('trans', 'Required levels are based on positions selected by the user.')"/>
					</xsl:otherwise>
				</xsl:choose>
			</p>
		</div>
		<xsl:apply-templates select="/root/math_user_competence_results/filters" mode="filter_form"/>
		<xsl:choose>
			<xsl:when test="/root/math_user_competence_results/@show_linear = 1">
				<div class="maxw800">
					<table class="table w100p">
						<tr>
							<th colspan="3" class="right">
								<xsl:value-of select="php:function('trans', 'Result / Required')"/>
							</th>
						</tr>
						<xsl:for-each select="/root/math_user_competence_results/competence">
							<xsl:variable name="mark_details_url">
								<xsl:value-of select="$current_user/@url"/>
								<xsl:text>competences/</xsl:text>
								<xsl:value-of select="$current_stream/@name"/>
								<xsl:text>/</xsl:text>
								<xsl:value-of select="@id"/>
								<xsl:text>/</xsl:text>
								<xsl:value-of select="$request/@query_string"/>
							</xsl:variable>
							<tr>
								<td class="cell_text w100p">
									<xsl:choose>
										<!-- Showing link always -->
										<xsl:when test="@show_link = 1 or true()">
											<a href="{$mark_details_url}">
												<xsl:call-template name="draw_competence"/>
											</a>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="draw_competence"/>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:apply-templates mode="tree_competence_td" select="."/>
								</td>
								<xsl:apply-templates mode="tree_right_td" select=".">
									<xsl:with-param name="mark_details_url" select="$mark_details_url"/>
								</xsl:apply-templates>
							</tr>
						</xsl:for-each>
					</table>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="draw_tree_head"/>
				<xsl:call-template name="draw_tree_competence_group">
					<xsl:with-param name="competence_is_clickable" select="true()"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="tree_right_th" match="group">
		<th class="right">
			<xsl:value-of select="php:function('trans', 'Result / Required')"/>
		</th>
	</xsl:template>
	<xsl:template mode="tree_competence_clickable" match="competence">
		<xsl:text>1</xsl:text>
		<!--<xsl:value-of select="/root/math_user_competence_results/competence[@id = current()/@id][1]/@show_link"/>-->
	</xsl:template>
	<xsl:template mode="tree_competence_td" match="competence">
		<xsl:for-each select="/root/math_user_competence_results/competence[@id = current()/@id][1]">
			<div class="hide600">
				<div class="props">
					<div class="prop">
						<xsl:call-template name="draw_user_competence_results_iconic_stat">
							<xsl:with-param name="align_left" select="true()"/>
						</xsl:call-template>
					</div>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="tree_right_td" match="competence">
		<td class="cell_iconic_stat pr6 show600">
			<xsl:for-each select="/root/math_user_competence_results/competence[@id = current()/@id][1]">
				<xsl:call-template name="draw_user_competence_results_iconic_stat">
					<xsl:with-param name="draw_empty" select="true()"/>
				</xsl:call-template>
			</xsl:for-each>
			<xsl:if test="not(/root/math_user_competence_results/competence[@id = current()/@id][1])">
				<xsl:call-template name="draw_user_competence_results_iconic_stat">
					<xsl:with-param name="draw_empty" select="true()"/>
				</xsl:call-template>
			</xsl:if>
		</td>
		<td class="cell_result pr6 pl10 nobr">
			<xsl:for-each select="/root/math_user_competence_results/competence[@id = current()/@id][1][@result]">
				<xsl:call-template name="draw_result_hl"/>
			</xsl:for-each>
			<xsl:if test="not(/root/math_user_competence_results/competence[@id = current()/@id][1]/@result)">
				<xsl:call-template name="draw_result_hl">
					<xsl:with-param name="result" select="'0.00'"/>
				</xsl:call-template>
			</xsl:if>
			<span class="result_hl_of result_hl_of__fixed_one_digit">
				<xsl:choose>
					<xsl:when test="/root/math_user_competence_results/competence[@id = current()/@id][1]/@required_result != ''">
						<xsl:text> / </xsl:text>
						<xsl:value-of select="/root/math_user_competence_results/competence[@id = current()/@id][1]/@required_result"/>
					</xsl:when>
					<xsl:otherwise>&#160;</xsl:otherwise>
				</xsl:choose>
			</span>
		</td>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_stream/@title"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text>)</xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Competences')"/>
		<xsl:text> </xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
