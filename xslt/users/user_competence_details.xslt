<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="../activities/include/activity_common.inc.xslt"/>
	<xsl:include href="include/user_marks.inc.xslt"/>
	<xsl:include href="include/user_competence_results.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'competences'"/>
	<xsl:variable name="user_section_main_page" select="false()"/>
	<xsl:variable name="current_stream" select="/root/stream_short[1]"/>
	<xsl:variable name="competence_title">
		<xsl:call-template name="draw_competence">
			<xsl:with-param name="id" select="/root/math_user_competence_details/@competence_id"/>
			<xsl:with-param name="title" select="/root/math_user_competence_details/@competence_title"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/competences/', $current_stream/@name, '/', /root/math_user_competence_details/@competence_id, '/')"/>
	<xsl:template match="math_user_competence_details"/>
	<xsl:template match="user_competence_details">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<a href="{$module_url}../../">
					<xsl:value-of select="php:function('trans', 'Competences')"/>
				</a>
				<xsl:text> / </xsl:text>
				<a href="{$module_url}../">
					<xsl:value-of select="$current_stream/@title_short"/>
				</a>
				<xsl:text> / </xsl:text>
				<xsl:value-of select="$competence_title"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:for-each select="/root/math_user_competence_details">
			<div class="box box__result p">
				<p>
					<xsl:value-of select="php:function('trans', 'Final result for the competence')"/>
					<xsl:text>: </xsl:text>
					<b>
						<xsl:if test="@result_type = 'integral'">∫&#160;</xsl:if>
						<xsl:value-of select="@result"/>
					</b>
					<xsl:if test="@result_type = 'integral'">
						<br/>
						<xsl:text>(</xsl:text>
						<xsl:value-of select="php:function('trans', 'integral result — calculated based on other competences')"/>
						<xsl:text>)</xsl:text>
					</xsl:if>
					<xsl:if test="@competence_is_deleted = 1">
						<br/>
						<b>
							<xsl:text>(</xsl:text>
							<xsl:value-of select="php:function('trans', 'this competence was removed and it will vanish upon recalculation')"/>
							<xsl:text>)</xsl:text>
						</b>
					</xsl:if>
				</p>
			</div>
			<div class="prop p">
				<a class="underline_inner_span" href="{$current_stream/@url}">
					<span>
						<xsl:value-of select="php:function('trans', 'Stream page')"/>
					</span>
					<xsl:text> </xsl:text>
					<i class="fa fa-external-link"/>
				</a>
				<xsl:text> | </xsl:text>
				<a class="underline_inner_span" href="{$main_prefix}/activities/?competence={@competence_id}">
					<span>
						<xsl:value-of select="php:function('trans', 'Where to improve')"/>
					</span>
					<xsl:text> </xsl:text>
					<i class="fa fa-external-link"/>
				</a>
			</div>
			<xsl:if test="integral_data">
				<h3>
					<xsl:value-of select="php:function('trans', 'Competences used in calculations', 'RESULT')"/>
				</h3>
				<div class="rose maxw600">
					<i data-column='{{"name": "required_result", "color": "#818181", "fillColor": "rgba(114, 114, 114, 0.5)"}}'/>
					<i data-column='{{"name": "result", "color": "#5d935f", "fillColor": "rgba(11, 166, 17, 0.45)"}}'/>
					<xsl:for-each select="integral_data/competence">
						<xsl:variable name="competence_title">
							<xsl:call-template name="draw_competence"/>
						</xsl:variable>
						<i data-row='{{"id": {@id}, "title": {php:function("json_encode", $competence_title)}, "hint_line2": "{@result} / {@required_result}", "required_result": {@required_result}, "result": {@result}}}'/>
					</xsl:for-each>
				</div>
				<table class="table w100p maxw600">
					<tr>
						<th>
							<xsl:value-of select="php:function('trans', 'Competence')"/>
						</th>
						<th class="right pr6">
							<span class="nobr">
								<xsl:value-of select="php:function('trans', 'Result / Required')"/>
							</span>
						</th>
					</tr>
				</table>
				<table class="table p w100p maxw600">
					<xsl:for-each select="integral_data/competence">
						<tr>
							<td class="cell_text w100p">
								<xsl:choose>
									<xsl:when test="@show_link = 1">
										<a href="{$module_url}../{@id}/">
											<xsl:call-template name="draw_competence"/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="draw_competence"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="cell_result pr6 pl10 nobr">
								<xsl:call-template name="draw_result_hl"/>
								<span class="result_hl_of">
									<xsl:text> / </xsl:text>
									<xsl:value-of select="@required_result"/>
								</span>
							</td>
							<!--<td class="cell_iconic_stat pr6 show600">
								<xsl:call-template name="draw_user_competence_results_iconic_stat"/>
							</td>-->
						</tr>
					</xsl:for-each>
				</table>
				<h3>
					<xsl:value-of select="php:function('trans', 'Marks added directly for this competence')"/>
				</h3>
				<xsl:if test="not(mark)">
					<p>
						<xsl:value-of select="php:function('trans', 'No marks were found.')"/>
					</p>
				</xsl:if>
			</xsl:if>
			<xsl:if test="@mark_count &gt; 0">
				<div class="prop p">
					<i class="fa fa-check-circle-o"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="@mark_count"/>
					<xsl:text> </xsl:text>
					<xsl:call-template name="count_case_lang">
						<xsl:with-param name="number" select="@mark_count"/>
						<xsl:with-param name="word" select="'mark'"/>
					</xsl:call-template>
					<span class="prop_sep">
						<xsl:text> </xsl:text>
					</span>
					<i class="fa fa-user"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="@rater_count"/>
					<xsl:text> </xsl:text>
					<xsl:call-template name="count_case_lang">
						<xsl:with-param name="number" select="@rater_count"/>
						<xsl:with-param name="word" select="'rater'"/>
					</xsl:call-template>
				</div>
			</xsl:if>
			<xsl:if test="mark">
				<xsl:if test="not(integral_data)">
					<h3>
						<xsl:value-of select="php:function('trans', 'Marks used in calculations', 'RESULT')"/>
					</h3>
				</xsl:if>
				<xsl:call-template name="draw_user_marks"/>
				<xsl:apply-templates select="pages"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/rose.js?v5"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$competence_title"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text>)</xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Competences')"/>
		<xsl:text> </xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
