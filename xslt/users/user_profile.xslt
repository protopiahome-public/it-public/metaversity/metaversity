<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="include/user_common.inc.xslt"/>
	<xsl:include href="include/user_streams.inc.xslt"/>
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'profile'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/')"/>
	<xsl:template match="user_streams"/>
	<xsl:template match="user_profile">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$header_title"/>
			<xsl:with-param name="url" select="$header_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="draw_user_head"/>
			<h2 class="h2_main">
				<xsl:value-of select="php:function('trans', 'Profile')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="props">
			<xsl:call-template name="draw_kv">
				<xsl:with-param name="key" select="php:function('trans', 'Login')"/>
				<xsl:with-param name="text">
					<xsl:value-of select="$current_user/@login"/>
					<xsl:text> (#</xsl:text>
					<xsl:value-of select="$current_user/@id"/>
					<xsl:text>)</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="class" select="'prop'"/>
			</xsl:call-template>
			<xsl:call-template name="draw_kv">
				<xsl:with-param name="key">
					<xsl:choose>
						<xsl:when test="$current_user/@sex = 'f'">
							<xsl:value-of select="php:function('trans', 'Registered', 'FEMALE')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'Registered', 'MALE')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="html">
					<xsl:call-template name="get_full_date">
						<xsl:with-param name="datetime" select="$current_user/@add_time"/>
					</xsl:call-template>
				</xsl:with-param>
				<xsl:with-param name="class" select="'prop'"/>
			</xsl:call-template>
			<xsl:call-template name="draw_kv">
				<xsl:with-param name="key">
					<xsl:choose>
						<xsl:when test="$current_user/@sex = 'f'">
							<xsl:value-of select="php:function('trans', 'Marks received', 'FEMALE')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'Marks received', 'MALE')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="html">
					<a href="{$current_user/@url}log/">
						<xsl:value-of select="/root/user_profile/@mark_count_calc"/>
					</a>
				</xsl:with-param>
				<xsl:with-param name="class" select="'prop'"/>
			</xsl:call-template>
			<xsl:copy-of select="/root/user_profile/about/div"/>
		</div>
		<h2>
			<xsl:value-of select="php:function('trans', 'Contacts')"/>
		</h2>
		<div class="props">
			<xsl:call-template name="draw_kv">
				<xsl:with-param name="key" select="'Email'"/>
				<xsl:with-param name="html">
					<a href="mailto:{/root/user_profile/@email_obscured}" class="email">
						<xsl:value-of select="/root/user_profile/@email_obscured"/>
					</a>
				</xsl:with-param>
				<xsl:with-param name="class" select="'prop'"/>
			</xsl:call-template>
			<xsl:if test="/root/user_profile/@www">
				<xsl:call-template name="draw_kv">
					<xsl:with-param name="key" select="php:function('trans', 'Website')"/>
					<xsl:with-param name="html">
						<a href="{/root/user_profile/@www}">
							<xsl:value-of select="/root/user_profile/@www_human_calc"/>
						</a>
					</xsl:with-param>
					<xsl:with-param name="class" select="'prop'"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:call-template name="draw_kv">
				<xsl:with-param name="key" select="php:function('trans', 'Phone')"/>
				<xsl:with-param name="text" select="/root/user_profile/@phone"/>
				<xsl:with-param name="class" select="'prop'"/>
			</xsl:call-template>
			<xsl:call-template name="draw_kv">
				<xsl:with-param name="key" select="php:function('trans', 'Skype')"/>
				<xsl:with-param name="text" select="/root/user_profile/@skype"/>
				<xsl:with-param name="class" select="'prop'"/>
			</xsl:call-template>
		</div>
		<xsl:for-each select="/root/user_streams">
			<h2>
				<a href="{$module_url}streams/">
					<xsl:value-of select="php:function('trans', 'Streams')"/>
				</a>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="pages/@row_count"/>
				<xsl:text>)</xsl:text>
			</h2>
			<xsl:call-template name="draw_user_streams"/>
			<xsl:if test="pages/page[2]">
				<p>
					<a href="{$module_url}streams/page-2/">
						<xsl:value-of select="php:function('trans', 'All streams')"/>
					</a>
				</p>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_user/@visible_name"/>
	</xsl:template>
</xsl:stylesheet>
