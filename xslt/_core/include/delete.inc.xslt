<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_delete">
		<xsl:param name="title_akk" select="php:function('trans', 'an entity')"/>
		<xsl:param name="disable_saved_info" select="false()"/>
		<input type="hidden" name="id" value="{@id}"/>
		<div class="box p">
			<p>
				<xsl:value-of select="php:function('trans', 'You are going to delete')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$title_akk"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'LQ')"/>
				<strong>
					<xsl:value-of select="@title"/>
				</strong>
				<xsl:value-of select="php:function('trans', 'RQ')"/>
				<xsl:text>.</xsl:text>
			</p>
			<p>
				<xsl:value-of select="php:function('trans', 'This action cannot be rolled back.')"/>
			</p>
		</div>
		<div class="p">
			<input class="ctrl ctrl__button ctrl__button__big ctrl__button__red ctrl__button__for_right" type="submit" value="{php:function('trans', 'Delete', 'VERB')}"/>
			<input class="ctrl ctrl__button ctrl__button__big ctrl__button__cancel" type="submit" name="cancel" value="{php:function('trans', 'Cancel', 'VERB')}"/>
		</div>
	</xsl:template>
</xsl:stylesheet>
