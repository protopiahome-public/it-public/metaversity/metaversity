<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="text()" mode="dtf_error_extend"/>
	<xsl:template match="text()" mode="dtf_fieldset_top"/>
	<xsl:template name="draw_dt_edit">
		<xsl:param name="disable_saved_info" select="false()"/>
		<xsl:param name="draw_button" select="true()"/>
		<xsl:param name="button_title" select="false()"/>
		<xsl:param name="no_fieldset" select="false()"/>
		<xsl:param name="allow_error_message" select="true()"/>
		<xsl:param name="blocks" select="doctype/block"/>
		<xsl:if test="not($disable_saved_info) and $pass_info/info[@name = 'SAVED']">
			<div class="info">
				<span>
					<xsl:value-of select="php:function('trans', 'The form is saved.')"/>
				</span>
			</div>
		</xsl:if>
		<xsl:if test="($pass_info/error or $pass_info/vars) and $allow_error_message">
			<div class="error">
				<span>
					<xsl:value-of select="php:function('trans', 'Please fix errors. The form was not saved.', 'DTF')"/>
				</span>
			</div>
		</xsl:if>
		<xsl:for-each select="primary_key/field">
			<input type="hidden" name="{@name}" value="{.}"/>
		</xsl:for-each>
		<p>
			<span class="star">*</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'required field.')"/>
		</p>
		<xsl:choose>
			<xsl:when test="@enable_blocks = 1">
				<xsl:for-each select="$blocks">
					<fieldset>
						<xsl:if test="@title != ''">
							<legend>
								<a name="{@name}"/>
								<xsl:value-of select="@title"/>
							</legend>
						</xsl:if>
						<xsl:apply-templates select="." mode="dtf_fieldset_top"/>
						<xsl:apply-templates select="field" mode="dtf"/>
					</fieldset>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="$blocks[1]">
					<xsl:choose>
						<xsl:when test="$no_fieldset">
							<xsl:apply-templates select="field" mode="dtf"/>
						</xsl:when>
						<xsl:otherwise>
							<fieldset>
								<xsl:apply-templates select="field" mode="dtf"/>
							</fieldset>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$draw_button">
			<div class="field2">
				<button class="ctrl ctrl__button ctrl__button__big">
					<xsl:choose>
						<xsl:when test="$button_title">
							<xsl:value-of select="$button_title"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'Save', 'VERB')"/>
						</xsl:otherwise>
					</xsl:choose>
				</button>
			</div>
		</xsl:if>
		<script type="text/javascript">
			var dt_deps = [];
			<xsl:for-each select="$blocks/field/dependency">
				dt_deps.push({
					dependee: '<xsl:value-of select="parent::field/@name"/>',
					depends_from: '<xsl:value-of select="@inspected_column"/>',
					activation_value: '<xsl:value-of select="@activation_value"/>',
					actions: <xsl:value-of select="@actions"/>
				});
			</xsl:for-each>
		</script>
	</xsl:template>
</xsl:stylesheet>
