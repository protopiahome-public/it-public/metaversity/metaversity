<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="count_case_lang">
		<xsl:param name="number"/>
		<xsl:param name="word"/>
		<xsl:choose>
			<xsl:when test="$lang = 'ru'">
				<xsl:call-template name="count_case">
					<xsl:with-param name="number" select="$number"/>
					<xsl:with-param name="word_ns" select="php:function('trans', $word, 'COUNT_CASE_1')"/>
					<xsl:with-param name="word_gs" select="php:function('trans', $word, 'COUNT_CASE_2')"/>
					<xsl:with-param name="word_ap" select="php:function('trans', $word, 'COUNT_CASE_5')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="count_case_en">
					<xsl:with-param name="number" select="$number"/>
					<xsl:with-param name="word_1" select="php:function('trans', $word, 'COUNT_CASE_1')"/>
					<xsl:with-param name="word_n" select="php:function('trans', $word, 'COUNT_CASE_N')"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="count_case">
		<xsl:param name="number"/>
		<!-- nominative case, singular, именительный падеж, ед. число -->
		<xsl:param name="word_ns"/>
		<!-- genitive case, singular, родительный падеж, ед. число -->
		<xsl:param name="word_gs"/>
		<!-- accusative case, plural, винительный падеж, мн. число -->
		<xsl:param name="word_ap"/>
		<xsl:if test="string-length($number) &gt; 0">
			<xsl:variable name="last" select="substring($number, string-length($number), 1)"/>
			<xsl:variable name="last_but_one" select="substring($number, string-length($number)-1, 1)"/>
			<xsl:choose>
				<xsl:when test="$last = '0'">
					<xsl:value-of select="$word_ap"/>
				</xsl:when>
				<xsl:when test="$last_but_one = '1' or $last &gt;= '5' and $last &lt;= '9'">
					<xsl:value-of select="$word_ap"/>
				</xsl:when>
				<xsl:when test="$last &gt;= '2' and $last &lt;= '4'">
					<xsl:value-of select="$word_gs"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$word_ns"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="count_case_en">
		<xsl:param name="number"/>
		<xsl:param name="word_1"/>
		<xsl:param name="word_n"/>
		<xsl:if test="string-length($number) &gt; 0">
			<xsl:choose>
				<xsl:when test="$number = '1'">
					<xsl:value-of select="$word_1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$word_n"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
