<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="pages">
		<xsl:param name="url_base" select="$module_url"/>
		<xsl:param name="url_suffix" select="$request/@query_string"/>
		<xsl:if test="page">
			<div class="pages">
				<xsl:for-each select="page">
					<xsl:choose>
						<xsl:when test="@type = 'prev'">
							<span class="page page__arrow">
								<xsl:choose>
									<xsl:when test="@number = 1">
										<a href="{$url_base}{$url_suffix}">&#8592;</a>
									</xsl:when>
									<xsl:otherwise>
										<a href="{$url_base}page-{@number}/{$url_suffix}">&#8592;</a>
									</xsl:otherwise>
								</xsl:choose>
							</span>
						</xsl:when>
						<xsl:when test="@type = 'current'">
							<span class="page page__number page__current">
								<xsl:value-of select="@number"/>
							</span>
						</xsl:when>
						<xsl:when test="@type = 'delimeter'">
							<span class="page page__ellipsis">...</span>
						</xsl:when>
						<xsl:when test="@type = 'page'">
							<span class="page page__number">
								<a href="{$url_base}page-{@number}/{$url_suffix}">
									<xsl:if test="@number = 1">
										<xsl:attribute name="href">
											<xsl:value-of select="concat($url_base, $url_suffix)"/>
										</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="@number"/>
								</a>
							</span>
						</xsl:when>
						<xsl:when test="current()[@type='next']">
							<span class="page page__arrow">
								<a href="{$url_base}page-{@number}/{$url_suffix}">&#8594;</a>
							</span>
						</xsl:when>
					</xsl:choose>
					<xsl:if test="position() != last()">
						<td class="sep-"> </td>
					</xsl:if>
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
