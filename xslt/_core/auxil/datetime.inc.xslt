<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="get_date">
		<xsl:param name="datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:param name="abbr" select="false()"/>
		<xsl:param name="force_year" select="false()"/>
		<xsl:param name="no_tags" select="false()"/>
		<xsl:variable name="year" select="substring($datetime, 1, 4)"/>
		<xsl:if test="$year != '0000'">
			<xsl:variable name="month" select="substring($datetime, 6, 2)"/>
			<xsl:variable name="day" select="substring($datetime, 9, 2)"/>
			<xsl:variable name="day_human">
				<xsl:choose>
					<xsl:when test="substring($day, 1, 1) = '0'">
						<xsl:value-of select="substring($day, 2, 1)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$day"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="week_day_number" select="substring($datetime, 21, 1)"/>
			<xsl:choose>
				<xsl:when test="$lang = 'ru'">
					<xsl:value-of select="$day_human"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$day_human"/>
					<xsl:variable name="day_last_digit" select="substring($day, 2, 1)"/>
					<xsl:variable name="day_last_but_one_digit" select="substring($day, string-length($day) - 1, 1)"/>
					<xsl:choose>
						<xsl:when test="$no_tags">
							<xsl:choose>
								<xsl:when test="$day_last_but_one_digit = 1">th</xsl:when>
								<xsl:when test="$day_last_digit = 1">st</xsl:when>
								<xsl:when test="$day_last_digit = 2">nd</xsl:when>
								<xsl:when test="$day_last_digit = 3">rd</xsl:when>
								<xsl:otherwise>th</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<sup>
								<xsl:choose>
									<xsl:when test="$day_last_but_one_digit = 1">th</xsl:when>
									<xsl:when test="$day_last_digit = 1">st</xsl:when>
									<xsl:when test="$day_last_digit = 2">nd</xsl:when>
									<xsl:when test="$day_last_digit = 3">rd</xsl:when>
									<xsl:otherwise>th</xsl:otherwise>
								</xsl:choose>
							</sup>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>&#160;</xsl:text>
			<xsl:call-template name="get_month">
				<xsl:with-param name="number" select="$month"/>
				<xsl:with-param name="mode" select="'with-day'"/>
				<xsl:with-param name="abbr" select="$abbr"/>
			</xsl:call-template>
			<xsl:if test="$force_year or substring(/root/@time, 1, 4) != $year">
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="$abbr">
						<xsl:text>&#8217;</xsl:text>
						<xsl:value-of select="substring($year, 3, 2)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$year"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="$with_week_day and $week_day_number != ''">
				<xsl:text> (</xsl:text>
				<xsl:call-template name="get_week_day">
					<xsl:with-param name="number" select="$week_day_number"/>
					<xsl:with-param name="mode" select="'lower'"/>
					<xsl:with-param name="abbr" select="true()"/>
				</xsl:call-template>
				<xsl:text>)</xsl:text>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template name="get_date_range">
		<xsl:param name="start_datetime"/>
		<xsl:param name="finish_datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:param name="abbr" select="false()"/>
		<xsl:choose>
			<xsl:when test="not($finish_datetime != '')">
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="substring($start_datetime, 1, 10) = substring($finish_datetime, 1, 10)">
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="substring($start_datetime, 1, 8) = substring($finish_datetime, 1, 8)">
				<xsl:variable name="year" select="substring($start_datetime, 1, 4)"/>
				<xsl:variable name="month" select="substring($start_datetime, 6, 2)"/>
				<xsl:variable name="day1" select="substring($start_datetime, 9, 2)"/>
				<xsl:variable name="day1_human">
					<xsl:choose>
						<xsl:when test="substring($day1, 1, 1) = '0'">
							<xsl:value-of select="substring($day1, 2, 1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$day1"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="week_day1_number" select="substring($start_datetime, 21, 1)"/>
				<xsl:variable name="day2" select="substring($finish_datetime, 9, 2)"/>
				<xsl:variable name="day2_human">
					<xsl:choose>
						<xsl:when test="substring($day2, 1, 1) = '0'">
							<xsl:value-of select="substring($day2, 2, 1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$day2"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="week_day2_number" select="substring($finish_datetime, 21, 1)"/>
				<xsl:choose>
					<xsl:when test="$lang = 'ru'">
						<xsl:choose>
							<xsl:when test="$with_week_day">
								<xsl:value-of select="$day1_human"/>
								<xsl:if test="$with_week_day and $week_day1_number != ''">
									<xsl:text> (</xsl:text>
									<xsl:call-template name="get_week_day">
										<xsl:with-param name="number" select="$week_day1_number"/>
										<xsl:with-param name="mode" select="'lower'"/>
										<xsl:with-param name="abbr" select="true()"/>
									</xsl:call-template>
									<xsl:text>)</xsl:text>
								</xsl:if>
								<xsl:call-template name="trans">
									<xsl:with-param name="html">_DASH_</xsl:with-param>
								</xsl:call-template>
								<xsl:value-of select="$day2_human"/>
								<xsl:if test="$with_week_day and $week_day2_number != ''">
									<xsl:text> (</xsl:text>
									<xsl:call-template name="get_week_day">
										<xsl:with-param name="number" select="$week_day2_number"/>
										<xsl:with-param name="mode" select="'lower'"/>
										<xsl:with-param name="abbr" select="true()"/>
									</xsl:call-template>
									<xsl:text>)</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$day1_human"/>
								<xsl:text>&#8211;</xsl:text>
								<xsl:value-of select="$day2_human"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="day1_last_digit" select="substring($day1, 2, 1)"/>
						<xsl:variable name="day1_last_but_one_digit" select="substring($day1, string-length($day1) - 1, 1)"/>
						<xsl:variable name="day2_last_digit" select="substring($day2, 2, 1)"/>
						<xsl:variable name="day2_last_but_one_digit" select="substring($day2, string-length($day2) - 1, 1)"/>
						<xsl:choose>
							<xsl:when test="$with_week_day">
								<xsl:value-of select="$day1_human"/>
								<sup>
									<xsl:choose>
										<xsl:when test="$day1_last_but_one_digit = 1">th</xsl:when>
										<xsl:when test="$day1_last_digit = 1">st</xsl:when>
										<xsl:when test="$day1_last_digit = 2">nd</xsl:when>
										<xsl:when test="$day1_last_digit = 3">rd</xsl:when>
										<xsl:otherwise>th</xsl:otherwise>
									</xsl:choose>
								</sup>
								<xsl:if test="$with_week_day and $week_day1_number != ''">
									<xsl:text> (</xsl:text>
									<xsl:call-template name="get_week_day">
										<xsl:with-param name="number" select="$week_day1_number"/>
										<xsl:with-param name="mode" select="'lower'"/>
										<xsl:with-param name="abbr" select="true()"/>
									</xsl:call-template>
									<xsl:text>)</xsl:text>
								</xsl:if>
								<xsl:call-template name="trans">
									<xsl:with-param name="html">_DASH_</xsl:with-param>
								</xsl:call-template>
								<xsl:value-of select="$day2_human"/>
								<sup>
									<xsl:choose>
										<xsl:when test="$day2_last_but_one_digit = 1">th</xsl:when>
										<xsl:when test="$day2_last_digit = 1">st</xsl:when>
										<xsl:when test="$day2_last_digit = 2">nd</xsl:when>
										<xsl:when test="$day2_last_digit = 3">rd</xsl:when>
										<xsl:otherwise>th</xsl:otherwise>
									</xsl:choose>
								</sup>
								<xsl:if test="$with_week_day and $week_day2_number != ''">
									<xsl:text> (</xsl:text>
									<xsl:call-template name="get_week_day">
										<xsl:with-param name="number" select="$week_day2_number"/>
										<xsl:with-param name="mode" select="'lower'"/>
										<xsl:with-param name="abbr" select="true()"/>
									</xsl:call-template>
									<xsl:text>)</xsl:text>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$day1_human"/>
								<sup>
									<xsl:choose>
										<xsl:when test="$day1_last_but_one_digit = 1">th</xsl:when>
										<xsl:when test="$day1_last_digit = 1">st</xsl:when>
										<xsl:when test="$day1_last_digit = 2">nd</xsl:when>
										<xsl:when test="$day1_last_digit = 3">rd</xsl:when>
										<xsl:otherwise>th</xsl:otherwise>
									</xsl:choose>
								</sup>
								<xsl:text>&#8211;</xsl:text>
								<xsl:value-of select="$day2_human"/>
								<sup>
									<xsl:choose>
										<xsl:when test="$day2_last_but_one_digit = 1">th</xsl:when>
										<xsl:when test="$day2_last_digit = 1">st</xsl:when>
										<xsl:when test="$day2_last_digit = 2">nd</xsl:when>
										<xsl:when test="$day2_last_digit = 3">rd</xsl:when>
										<xsl:otherwise>th</xsl:otherwise>
									</xsl:choose>
								</sup>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>&#160;</xsl:text>
				<xsl:call-template name="get_month">
					<xsl:with-param name="number" select="$month"/>
					<xsl:with-param name="mode" select="'with-day'"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
				<xsl:if test="substring(/root/@time, 1, 4) != $year">
					<xsl:text> </xsl:text>
					<xsl:choose>
						<xsl:when test="$abbr">
							<xsl:text>&#8217;</xsl:text>
							<xsl:value-of select="substring($year, 3, 2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$year"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:when>
			<xsl:when test="substring($start_datetime, 1, 5) = substring($finish_datetime, 1, 5)">
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
				<xsl:call-template name="trans">
					<xsl:with-param name="html">_DASH_</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$finish_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
				<xsl:call-template name="trans">
					<xsl:with-param name="html">_DASH_</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$finish_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
					<xsl:with-param name="force_year" select="substring(/root/@time, 1, 4) != substring($start_datetime, 1, 4)"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="get_time">
		<xsl:param name="datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:param name="abbr" select="false()"/>
		<xsl:call-template name="get_date">
			<xsl:with-param name="datetime" select="$datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
			<xsl:with-param name="abbr" select="$abbr"/>
		</xsl:call-template>
		<xsl:variable name="hours" select="substring($datetime, 12, 2)"/>
		<xsl:variable name="minutes" select="substring($datetime, 15, 2)"/>
		<xsl:choose>
			<xsl:when test="$lang = 'ru'"> в&#160;</xsl:when>
			<xsl:otherwise> at&#160;</xsl:otherwise>
		</xsl:choose>
		<!--<xsl:if test="string-length($hours) = 1">0</xsl:if>-->
		<xsl:value-of select="$hours"/>
		<xsl:text>:</xsl:text>
		<!--<xsl:if test="string-length($minutes) = 1">0</xsl:if>-->
		<xsl:value-of select="$minutes"/>
	</xsl:template>
	<xsl:template name="get_time_range">
		<xsl:param name="start_datetime"/>
		<xsl:param name="finish_datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:param name="abbr" select="false()"/>
		<xsl:choose>
			<xsl:when test="not($finish_datetime != '') or $start_datetime = $finish_datetime">
				<xsl:call-template name="get_time">
					<xsl:with-param name="datetime" select="$start_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="substring($start_datetime, 1, 10) = substring($finish_datetime, 1, 10)">
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
				<xsl:choose>
					<xsl:when test="$lang = 'ru'">
						<xsl:text> с&#160;</xsl:text>
						<xsl:value-of select="substring($start_datetime, 12, 5)"/>
						<xsl:text> до&#160;</xsl:text>
						<xsl:value-of select="substring($finish_datetime, 12, 5)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>, </xsl:text>
						<xsl:value-of select="substring($start_datetime, 12, 5)"/>
						<xsl:call-template name="trans">
							<xsl:with-param name="html">_DASH_</xsl:with-param>
						</xsl:call-template>
						<xsl:value-of select="substring($finish_datetime, 12, 5)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$start_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
				</xsl:call-template>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="substring($start_datetime, 12, 5)"/>
				<xsl:call-template name="trans">
					<xsl:with-param name="html">_DASH_</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="get_date">
					<xsl:with-param name="datetime" select="$finish_datetime"/>
					<xsl:with-param name="with_week_day" select="$with_week_day"/>
					<xsl:with-param name="abbr" select="$abbr"/>
					<xsl:with-param name="force_year" select="substring(/root/@time, 1, 4) != substring($start_datetime, 1, 4)"/>
				</xsl:call-template>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="substring($finish_datetime, 12, 5)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="get_month">
		<xsl:param name="number"/>
		<!-- capital | lower | with-day -->
		<xsl:param name="mode" select="'capital'"/>
		<xsl:param name="abbr" select="false()"/>
		<xsl:choose>
			<xsl:when test="$lang = 'ru' and ($mode = 'lower' or $mode = 'with-day') and $abbr">
				<xsl:choose>
					<xsl:when test="$number = 1">янв</xsl:when>
					<xsl:when test="$number = 2">фев</xsl:when>
					<xsl:when test="$number = 3">мар</xsl:when>
					<xsl:when test="$number = 4">апр</xsl:when>
					<xsl:when test="$number = 5">май</xsl:when>
					<xsl:when test="$number = 6">июн</xsl:when>
					<xsl:when test="$number = 7">июл</xsl:when>
					<xsl:when test="$number = 8">авг</xsl:when>
					<xsl:when test="$number = 9">сен</xsl:when>
					<xsl:when test="$number = 10">окт</xsl:when>
					<xsl:when test="$number = 11">ноя</xsl:when>
					<xsl:when test="$number = 12">дек</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'ru' and $mode = 'with-day'">
				<xsl:choose>
					<xsl:when test="$number = 1">января</xsl:when>
					<xsl:when test="$number = 2">февраля</xsl:when>
					<xsl:when test="$number = 3">марта</xsl:when>
					<xsl:when test="$number = 4">апреля</xsl:when>
					<xsl:when test="$number = 5">мая</xsl:when>
					<xsl:when test="$number = 6">июня</xsl:when>
					<xsl:when test="$number = 7">июля</xsl:when>
					<xsl:when test="$number = 8">августа</xsl:when>
					<xsl:when test="$number = 9">сентября</xsl:when>
					<xsl:when test="$number = 10">октября</xsl:when>
					<xsl:when test="$number = 11">ноября</xsl:when>
					<xsl:when test="$number = 12">декабря</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'ru' and $mode = 'lower'">
				<xsl:choose>
					<xsl:when test="$number = 1">январь</xsl:when>
					<xsl:when test="$number = 2">февраль</xsl:when>
					<xsl:when test="$number = 3">март</xsl:when>
					<xsl:when test="$number = 4">апрель</xsl:when>
					<xsl:when test="$number = 5">май</xsl:when>
					<xsl:when test="$number = 6">июнь</xsl:when>
					<xsl:when test="$number = 7">июль</xsl:when>
					<xsl:when test="$number = 8">август</xsl:when>
					<xsl:when test="$number = 9">сентябрь</xsl:when>
					<xsl:when test="$number = 10">октябрь</xsl:when>
					<xsl:when test="$number = 11">ноябрь</xsl:when>
					<xsl:when test="$number = 12">декабрь</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'ru' and $abbr">
				<xsl:choose>
					<xsl:when test="$number = 1">Янв</xsl:when>
					<xsl:when test="$number = 2">Фев</xsl:when>
					<xsl:when test="$number = 3">Мар</xsl:when>
					<xsl:when test="$number = 4">Апр</xsl:when>
					<xsl:when test="$number = 5">Май</xsl:when>
					<xsl:when test="$number = 6">Июн</xsl:when>
					<xsl:when test="$number = 7">Июл</xsl:when>
					<xsl:when test="$number = 8">Авг</xsl:when>
					<xsl:when test="$number = 9">Сен</xsl:when>
					<xsl:when test="$number = 10">Окт</xsl:when>
					<xsl:when test="$number = 11">Ноя</xsl:when>
					<xsl:when test="$number = 12">Дек</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'ru'">
				<xsl:choose>
					<xsl:when test="$number = 1">Январь</xsl:when>
					<xsl:when test="$number = 2">Февраль</xsl:when>
					<xsl:when test="$number = 3">Март</xsl:when>
					<xsl:when test="$number = 4">Апрель</xsl:when>
					<xsl:when test="$number = 5">Май</xsl:when>
					<xsl:when test="$number = 6">Июнь</xsl:when>
					<xsl:when test="$number = 7">Июль</xsl:when>
					<xsl:when test="$number = 8">Август</xsl:when>
					<xsl:when test="$number = 9">Сентябрь</xsl:when>
					<xsl:when test="$number = 10">Октябрь</xsl:when>
					<xsl:when test="$number = 11">Ноябрь</xsl:when>
					<xsl:when test="$number = 12">Декабрь</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'en' and $abbr">
				<xsl:choose>
					<xsl:when test="$number = 1">Jan</xsl:when>
					<xsl:when test="$number = 2">Feb</xsl:when>
					<xsl:when test="$number = 3">Mar</xsl:when>
					<xsl:when test="$number = 4">Apr</xsl:when>
					<xsl:when test="$number = 5">May</xsl:when>
					<xsl:when test="$number = 6">Jun</xsl:when>
					<xsl:when test="$number = 7">Jul</xsl:when>
					<xsl:when test="$number = 8">Aug</xsl:when>
					<xsl:when test="$number = 9">Sep</xsl:when>
					<xsl:when test="$number = 10">Oct</xsl:when>
					<xsl:when test="$number = 11">Nov</xsl:when>
					<xsl:when test="$number = 12">Dec</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$number = 1">January</xsl:when>
					<xsl:when test="$number = 2">February</xsl:when>
					<xsl:when test="$number = 3">March</xsl:when>
					<xsl:when test="$number = 4">April</xsl:when>
					<xsl:when test="$number = 5">May</xsl:when>
					<xsl:when test="$number = 6">June</xsl:when>
					<xsl:when test="$number = 7">July</xsl:when>
					<xsl:when test="$number = 8">August</xsl:when>
					<xsl:when test="$number = 9">September</xsl:when>
					<xsl:when test="$number = 10">October</xsl:when>
					<xsl:when test="$number = 11">November</xsl:when>
					<xsl:when test="$number = 12">December</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="get_week_day">
		<xsl:param name="number"/>
		<!-- capital | lower -->
		<xsl:param name="mode" select="'capital'"/>
		<xsl:param name="abbr" select="false()"/>
		<xsl:choose>
			<xsl:when test="$lang = 'ru' and $mode = 'lower' and $abbr">
				<xsl:choose>
					<xsl:when test="$number = 1">пн</xsl:when>
					<xsl:when test="$number = 2">вт</xsl:when>
					<xsl:when test="$number = 3">ср</xsl:when>
					<xsl:when test="$number = 4">чт</xsl:when>
					<xsl:when test="$number = 5">пт</xsl:when>
					<xsl:when test="$number = 6">сб</xsl:when>
					<xsl:when test="$number = 7">вс</xsl:when>
					<xsl:when test="$number = 0">вс</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'ru' and $mode = 'lower'">
				<xsl:choose>
					<xsl:when test="$number = 1">понедельник</xsl:when>
					<xsl:when test="$number = 2">вторник</xsl:when>
					<xsl:when test="$number = 3">среда</xsl:when>
					<xsl:when test="$number = 4">четверг</xsl:when>
					<xsl:when test="$number = 5">пятница</xsl:when>
					<xsl:when test="$number = 6">суббота</xsl:when>
					<xsl:when test="$number = 7">воскресенье</xsl:when>
					<xsl:when test="$number = 0">воскресенье</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'ru' and $abbr">
				<xsl:choose>
					<xsl:when test="$number = 1">Пн</xsl:when>
					<xsl:when test="$number = 2">Вт</xsl:when>
					<xsl:when test="$number = 3">Ср</xsl:when>
					<xsl:when test="$number = 4">Чт</xsl:when>
					<xsl:when test="$number = 5">Пт</xsl:when>
					<xsl:when test="$number = 6">Сб</xsl:when>
					<xsl:when test="$number = 7">Вс</xsl:when>
					<xsl:when test="$number = 0">Вс</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'ru'">
				<xsl:choose>
					<xsl:when test="$number = 1">Понедельник</xsl:when>
					<xsl:when test="$number = 2">Вторник</xsl:when>
					<xsl:when test="$number = 3">Среда</xsl:when>
					<xsl:when test="$number = 4">Четверг</xsl:when>
					<xsl:when test="$number = 5">Пятница</xsl:when>
					<xsl:when test="$number = 6">Суббота</xsl:when>
					<xsl:when test="$number = 7">Воскресенье</xsl:when>
					<xsl:when test="$number = 0">Воскресенье</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$lang = 'en' and $abbr">
				<xsl:choose>
					<xsl:when test="$number = 1">Mon</xsl:when>
					<xsl:when test="$number = 2">Tue</xsl:when>
					<xsl:when test="$number = 3">Wed</xsl:when>
					<xsl:when test="$number = 4">Thu</xsl:when>
					<xsl:when test="$number = 5">Fri</xsl:when>
					<xsl:when test="$number = 6">Sat</xsl:when>
					<xsl:when test="$number = 7">Sun</xsl:when>
					<xsl:when test="$number = 0">Sun</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$number = 1">Monday</xsl:when>
					<xsl:when test="$number = 2">Tuesday</xsl:when>
					<xsl:when test="$number = 3">Wednesday</xsl:when>
					<xsl:when test="$number = 4">Thurthday</xsl:when>
					<xsl:when test="$number = 5">Friday</xsl:when>
					<xsl:when test="$number = 6">Saturday</xsl:when>
					<xsl:when test="$number = 7">Sunday</xsl:when>
					<xsl:when test="$number = 0">Sunday</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- obsolete, left for compatibility -->
	<xsl:template name="get_full_date">
		<xsl:param name="datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_date">
			<xsl:with-param name="datetime" select="$datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="get_half_full_date">
		<xsl:param name="datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_date">
			<xsl:with-param name="datetime" select="$datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
			<xsl:with-param name="abbr" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="get_full_datetime">
		<xsl:param name="datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_time">
			<xsl:with-param name="datetime" select="$datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="get_half_full_datetime">
		<xsl:param name="datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_time">
			<xsl:with-param name="datetime" select="$datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
			<xsl:with-param name="abbr" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="get_full_datetime_range">
		<xsl:param name="start_datetime"/>
		<xsl:param name="finish_datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_time_range">
			<xsl:with-param name="start_datetime" select="$start_datetime"/>
			<xsl:with-param name="finish_datetime" select="$finish_datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="get_half_full_datetime_range">
		<xsl:param name="start_datetime"/>
		<xsl:param name="finish_datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_time_range">
			<xsl:with-param name="start_datetime" select="$start_datetime"/>
			<xsl:with-param name="finish_datetime" select="$finish_datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
			<xsl:with-param name="abbr" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="get_full_date_range">
		<xsl:param name="start_datetime"/>
		<xsl:param name="finish_datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_date_range">
			<xsl:with-param name="start_datetime" select="$start_datetime"/>
			<xsl:with-param name="finish_datetime" select="$finish_datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="get_half_full_date_range">
		<xsl:param name="start_datetime"/>
		<xsl:param name="finish_datetime"/>
		<xsl:param name="with_week_day" select="false()"/>
		<xsl:call-template name="get_date_range">
			<xsl:with-param name="start_datetime" select="$start_datetime"/>
			<xsl:with-param name="finish_datetime" select="$finish_datetime"/>
			<xsl:with-param name="with_week_day" select="$with_week_day"/>
			<xsl:with-param name="abbr" select="true()"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>
