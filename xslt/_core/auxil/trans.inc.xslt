<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" xmlns:php="http://php.net/xsl" exclude-result-prefixes="exsl">
	<xsl:template name="trans">
		<xsl:param name="html"/>
		<xsl:param name="var1" select="false()"/>
		<xsl:param name="var2" select="false()"/>
		<xsl:param name="var3" select="false()"/>
		<xsl:param name="var4" select="false()"/>
		<xsl:param name="var5" select="false()"/>
		<xsl:param name="var6" select="false()"/>
		<xsl:param name="var7" select="false()"/>
		<xsl:param name="var8" select="false()"/>
		<xsl:param name="var9" select="false()"/>
		<xsl:param name="debug" select="false()"/>
		<xsl:variable name="html2">
			<xsl:apply-templates select="exsl:node-set($html)" mode="serialize"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$debug">
				<xsl:apply-templates select="php:function('trans_html_xslt_debug', exsl:node-set($html), $var1, $var2, $var3, $var4, $var5, $var6, $var7, $var8, $var9)" mode="trans"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="php:function('trans_html_xslt', exsl:node-set($html), $var1, $var2, $var3, $var4, $var5, $var6, $var7, $var8, $var9)" mode="trans"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="*" mode="trans">
		<xsl:apply-templates match="*" mode="trans1"/>
	</xsl:template>
	<xsl:template match="*" mode="trans1">
		<xsl:copy-of select="."/>
	</xsl:template>
</xsl:stylesheet>
