<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="file_size">
		<xsl:param name="size" select="@size"/>
		<xsl:param name="size_unit" select="@size_unit"/>
		<xsl:value-of select="@size"/>
		<xsl:text> </xsl:text>
		<xsl:variable name="size_unit_inner">
			<xsl:choose>
				<xsl:when test="$size_unit != ''">
					<xsl:value-of select="$size_unit"/>
				</xsl:when>
				<xsl:otherwise>B</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$size_unit_inner = 'B'">
				<xsl:call-template name="count_case">
					<xsl:with-param name="number" select="$size"/>
					<xsl:with-param name="word_ns" select="' байт'"/>
					<xsl:with-param name="word_gs" select="' байта'"/>
					<xsl:with-param name="word_ap" select="' байт'"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$size_unit_inner = 'KB'">КБ</xsl:when>
			<xsl:when test="$size_unit_inner = 'MB'">МБ</xsl:when>
			<xsl:when test="$size_unit_inner = 'GB'">ГБ</xsl:when>
			<xsl:when test="$size_unit_inner = 'TB'">ТБ</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
