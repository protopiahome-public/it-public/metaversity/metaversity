<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'bool']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
			</xsl:if>
			<input id="f-{@name}" class="ctrl ctrl__checkbox" type="checkbox" name="{@name}">
				<xsl:choose>
					<xsl:when test="$pass_info/vars/var[@name = current()/@name]">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:when>
					<xsl:when test="$pass_info/vars/var"/>
					<xsl:when test="$document_field = 1">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:when>
					<xsl:when test="$is_add and @default_value = 1">
						<xsl:attribute name="checked">checked</xsl:attribute>
					</xsl:when>
				</xsl:choose>
			</input>
			<label for="f-{@name}" class="_inline">
				<xsl:value-of select="@title"/>
			</label>
			<xsl:if test="@comment != ''">
				<div class="_comment">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
