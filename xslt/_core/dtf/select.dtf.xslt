<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'select']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
			</xsl:if>
			<label for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:if test="not(@is_important = 1)">
						<xsl:attribute name="class">star hide</xsl:attribute>
					</xsl:if>
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<xsl:choose>
				<xsl:when test="@drop_down_view = 1">
					<select id="f-{@name}" class="f-{@name} js_ctrl_select_with_front ctrl ctrl__select" name="{@name}">
						<xsl:if test="@is_read_only = 1">
							<xsl:attribute name="readonly">readonly</xsl:attribute>
						</xsl:if>
						<xsl:for-each select="item">
							<option value="{@key}">
								<xsl:choose>
									<xsl:when test="$pass_info/vars/var[@name = current()/../@name] = @key">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
									<xsl:when test="$pass_info/vars/var[@name = current()/../@name]"/>
									<xsl:when test="$document_field/@key = @key">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
									<xsl:when test="$is_add and @is_default = 1">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
								</xsl:choose>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</xsl:when>
				<xsl:otherwise>
					<div class="">
						<xsl:for-each select="item">
							<span class="nobr">
								<input id="f-{../@name}_{@key}" class="ctrl ctrl__radio" type="radio" name="{../@name}" value="{@key}">
									<xsl:if test="../@is_read_only = 1">
										<xsl:attribute name="readonly">readonly</xsl:attribute>
									</xsl:if>
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = current()/../@name] = @key">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var[@name = current()/../@name]"/>
										<xsl:when test="$document_field/@key = @key">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
										<xsl:when test="$is_add and @is_default = 1">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:when>
									</xsl:choose>
								</input>
							</span>
							<label for="f-{../@name}_{@key}" class="_inline">
								<xsl:if test="position() != last()">
									<xsl:attribute name="class">_inline _for_right</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</label>
						</xsl:for-each>
					</div>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:choose>
				<xsl:when test="comment_html/span">
					<div class="_comment">
						<xsl:copy-of select="comment_html/span"/>
					</div>
				</xsl:when>
				<xsl:when test="@comment != ''">
					<div class="_comment">
						<xsl:value-of select="@comment"/>
					</div>
				</xsl:when>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
