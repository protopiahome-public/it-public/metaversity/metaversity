<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'email' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Please fill in this field.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Too long. Please use', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@max_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case_lang">
								<xsl:with-param name="number" select="@max_length"/>
								<xsl:with-param name="word" select="'char'"/>
							</xsl:call-template>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'or less', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'you used', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']"/>
							<xsl:text>).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'INCORRECT_EMAIL']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Please use a correct email address.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
</xsl:stylesheet>
