<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'text_html']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2 field2__wide" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__wide field2__error</xsl:attribute>
			</xsl:if>
			<label class="_title" for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:if test="not(@is_important = 1)">
						<xsl:attribute name="class">star hide</xsl:attribute>
					</xsl:if>
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<textarea id="f-{@name}" name="{@name}" data-body-class="{@body_class}" style="height: {@editor_height}px">
				<xsl:attribute name="class">
					<xsl:text>ctrl ctrl__textarea </xsl:text>
					<xsl:value-of select="concat('f-', @name, ' ')"/>
					<xsl:text>xhtml-editor-</xsl:text>
					<xsl:value-of select="@button_set"/>
					<xsl:if test="@use_cut = 1">-cut</xsl:if>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="$pass_info/vars/var[@name = current()/@name]">
						<xsl:value-of select="$pass_info/vars/var[@name = current()/@name]"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$document_field"/>
					</xsl:otherwise>
				</xsl:choose>
			</textarea>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="_comment">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
