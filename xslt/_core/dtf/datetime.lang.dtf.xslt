<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'datetime' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'INCORRECT_VALUE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Incorrect value of one of the input fields.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'PARTIALLY_FILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'You have to fill in or leave blank all the fields.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_YEAR']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Incorrect year.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_MONTH']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Incorrect month.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_DAY']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Incorrect day.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_HOURS']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Incorrect value for hours.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_MINUTES']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Incorrect value for minutes.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'FAILED_DEPENDENCY']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'This date must be', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:choose>
								<xsl:when test="@can_be_equal = 1">
									<xsl:value-of select="php:function('trans', 'no less than', 'DTF')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="php:function('trans', 'greater than', 'DTF')"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'LQ')"/>
							<xsl:value-of select="../field[@name = current()/@must_be_later_than_field_name]/@title"/>
							<xsl:value-of select="php:function('trans', 'RQ')"/>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'FAILED_DEPENDENCY_UNEXISTED_SOURCE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'This date cannot be set until you set', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'LQ')"/>
							<xsl:value-of select="../field[@name = current()/@must_be_later_than_field_name]/@title"/>
							<xsl:value-of select="php:function('trans', 'RQ')"/>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'datetime' and ancestor::doctype]" mode="dtf_datetime_show_finish_time">
		<xsl:value-of select="php:function('trans', 'Set finish time', 'DTF')"/>
	</xsl:template>
	<xsl:template match="field[@type = 'datetime' and ancestor::doctype]" mode="dtf_datetime_do_not_fill">
		<xsl:value-of select="php:function('trans', 'Do not set this date', 'DTF')"/>
	</xsl:template>
</xsl:stylesheet>
