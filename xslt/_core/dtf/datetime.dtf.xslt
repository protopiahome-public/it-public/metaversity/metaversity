<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'datetime']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
			</xsl:if>
			<xsl:if test="@must_be_later_than_field_name != ''">
				<xsl:attribute name="jq-later-than">
					<xsl:value-of select="@must_be_later_than_field_name"/>
				</xsl:attribute>
				<xsl:attribute name="jq-can-be-equal">
					<xsl:value-of select="@can_be_equal"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:variable name="can_be_hidden" select="@hide_by_default = 1"/>
			<xsl:variable name="pass_info_hide" select="$pass_info/vars/var[@name = concat(current()/@name, '_hide')]"/>
			<xsl:variable name="hide" select="$can_be_hidden and ($is_add or $document_field/@unset = 1 or $pass_info_hide = 1) and not($pass_info_hide = 0)"/>
			<input type="hidden" id="f-{@name}_hide" name="{@name}_hide">
				<xsl:attribute name="value">
					<xsl:choose>
						<xsl:when test="$hide">1</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</input>
			<xsl:if test="$can_be_hidden">
				<div id="field-{@name}-enabler">
					<xsl:attribute name="class">
						<xsl:text>jq-datetime-enabler _enabler</xsl:text>
						<xsl:if test="not($hide)"> hide</xsl:if>
					</xsl:attribute>
					<span class="jq-datetime-enabler-link clickable dashed">
						<xsl:apply-templates mode="dtf_datetime_show_finish_time" select="."/>
					</span>
				</div>
			</xsl:if>
			<div id="field-{@name}-content">
				<xsl:attribute name="class">
					<xsl:text>jq-datetime-field-content _content </xsl:text>
					<xsl:if test="$hide">hide</xsl:if>
				</xsl:attribute>
				<label class="_title" for="f-{@name}_day">
					<xsl:value-of select="@title"/>
					<xsl:text>:</xsl:text>
				</label>
				<xsl:variable name="year">
					<xsl:choose>
						<xsl:when test="$is_add and @is_blank_default_for_nonimportant = 1">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:when test="$is_add">
							<xsl:value-of select="substring(/root/@time, 1, 4)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$document_field/@year"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="month">
					<xsl:choose>
						<xsl:when test="$is_add and @is_blank_default_for_nonimportant = 1">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:when test="$is_add">
							<xsl:value-of select="substring(/root/@time, 6, 2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$document_field/@month"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="day">
					<xsl:choose>
						<xsl:when test="$is_add and @is_blank_default_for_nonimportant = 1">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:when test="$is_add">
							<xsl:value-of select="substring(/root/@time, 9, 2)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$document_field/@day"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<select id="f-{@name}_day" name="{@name}_day" class="ctrl ctrl__select ctrl__for_right f-{@name}">
					<xsl:if test="@is_read_only = 1">
						<xsl:attribute name="readonly">readonly</xsl:attribute>
					</xsl:if>
					<xsl:if test="@is_important = 0">
						<option value=""/>
					</xsl:if>
					<xsl:for-each select="days_for_select/day">
						<option value="{@key}">
							<xsl:choose>
								<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_day')] = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
								<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_day')]"/>
								<xsl:when test="$day = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
							</xsl:choose>
							<xsl:value-of select="@title"/>
						</option>
					</xsl:for-each>
				</select>
				<select id="f-{@name}_month" name="{@name}_month" class="ctrl ctrl__select ctrl__for_right f-{@name}">
					<xsl:if test="@is_read_only = 1">
						<xsl:attribute name="readonly">readonly</xsl:attribute>
					</xsl:if>
					<xsl:if test="@is_important = 0">
						<option value=""/>
					</xsl:if>
					<xsl:for-each select="months_for_select/month">
						<option value="{@key}">
							<xsl:choose>
								<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_month')] = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
								<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_month')]"/>
								<xsl:when test="$month = @key">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
							</xsl:choose>
							<xsl:call-template name="get_month">
								<xsl:with-param name="number" select="@key"/>
								<xsl:with-param name="mode" select="'with-day'"/>
							</xsl:call-template>
						</option>
					</xsl:for-each>
				</select>
				<select id="f-{@name}_year" name="{@name}_year" class="ctrl ctrl__select ctrl__for_right f-{@name}">
					<xsl:if test="@is_read_only = 1">
						<xsl:attribute name="readonly">readonly</xsl:attribute>
					</xsl:if>
					<xsl:if test="@is_important = 0">
						<option value=""/>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$document_field/years_for_select">
							<xsl:for-each select="$document_field/years_for_select/year">
								<option value="{@key}">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')] = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')]"/>
										<xsl:when test="$year = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:for-each select="years_for_select/year">
								<option value="{@key}">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')] = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_year')]"/>
										<xsl:when test="$year = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
				</select>
				<xsl:if test="@date_only = 0">
					<xsl:variable name="hours">
						<xsl:choose>
							<xsl:when test="$is_add and @is_blank_default_for_nonimportant = 1">
								<xsl:value-of select="''"/>
							</xsl:when>
							<xsl:when test="$is_add">
								<xsl:value-of select="'12'"/>
								<!--<xsl:value-of select="substring(/root/@time, 12, 2)"/>-->
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$document_field/@hours"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="minutes">
						<xsl:choose>
							<xsl:when test="$is_add and @is_blank_default_for_nonimportant = 1">
								<xsl:value-of select="''"/>
							</xsl:when>
							<xsl:when test="$is_add">
								<xsl:value-of select="'00'"/>
								<!--<xsl:value-of select="substring(/root/@time, 15, 2)"/>-->
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$document_field/@minutes"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<span class="nobr">
						<select id="f-{@name}_hours" name="{@name}_hours" class="ctrl ctrl__select f-{@name}">
							<xsl:if test="@is_read_only = 1">
								<xsl:attribute name="readonly">readonly</xsl:attribute>
							</xsl:if>
							<xsl:if test="@is_important = 0">
								<option value=""/>
							</xsl:if>
							<xsl:for-each select="hours_for_select/hour">
								<option value="{@key}">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_hours')] = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_hours')]"/>
										<xsl:when test="$hours = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</select>
						<xsl:text> : </xsl:text>
						<select id="f-{@name}_minutes" name="{@name}_minutes" class="ctrl ctrl__select ctrl__for_right f-{@name}">
							<xsl:if test="@is_read_only = 1">
								<xsl:attribute name="readonly">readonly</xsl:attribute>
							</xsl:if>
							<xsl:if test="@is_important = 0">
								<option value=""/>
							</xsl:if>
							<xsl:for-each select="minutes_for_select/minute">
								<option value="{@key}">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_minutes')] = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var[@name = concat(current()/../../@name, '_minutes')]"/>
										<xsl:when test="$minutes = @key">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</select>
					</span>
				</xsl:if>
				<xsl:if test="not(@is_read_only = 1)">
					<input id="f-{@name}-datepicker" class="jq-datetime-datepicker-input _datepicker" type="text"/>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$can_be_hidden">
						<div class="jq-datetime-disabler _cleaner">
							<span class="jq-datetime-disabler-link clickable dashed nobr">
								<xsl:apply-templates mode="dtf_datetime_do_not_fill" select="."/>
							</span>
						</div>
					</xsl:when>
					<xsl:when test="@is_important = 0">
						<div>
							<xsl:attribute name="class">
								<xsl:text>jq-datetime-cleaner _cleaner</xsl:text>
								<xsl:if test="$document_field/@unset = 1 or $is_add and @is_blank_default_for_nonimportant = 1"> hide</xsl:if>
							</xsl:attribute>
							<span class="jq-datetime-cleaner-link clickable dashed nobr">
								<xsl:apply-templates mode="dtf_datetime_do_not_fill" select="."/>
							</span>
						</div>
					</xsl:when>
				</xsl:choose>
				<xsl:apply-templates mode="dtf_error" select="."/>
				<xsl:if test="@comment != ''">
					<div class="_comment">
						<xsl:value-of select="@comment"/>
					</div>
				</xsl:if>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
