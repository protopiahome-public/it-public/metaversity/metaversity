<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'select' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Please fill in this field.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNEXISTED_VALUE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The value you selected does not exist. Please try again.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
</xsl:stylesheet>
