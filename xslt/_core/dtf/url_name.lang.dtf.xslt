<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'url_name' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Please fill in this field.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNIQUE_CHECK_FAILED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'This name is already', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:choose>
								<xsl:when test="@url_prefix != ''">
									<a href="{@url_prefix}{$pass_info/error[@field = current()/@name and @name = 'UNIQUE_CHECK_FAILED']}/">
										<xsl:value-of select="php:function('trans', 'registered', 'DTF')"/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="php:function('trans', 'registered', 'DTF')"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>. </xsl:text>
							<xsl:value-of select="php:function('trans', 'Please, choose another one.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Too short. Please use at least', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@min_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case_lang">
								<xsl:with-param name="number" select="@min_length"/>
								<xsl:with-param name="word" select="'char'"/>
							</xsl:call-template>
							<xsl:text> (</xsl:text>
							<xsl:value-of select="php:function('trans', 'you used', 'DTF')"/>
							<xsl:text/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']"/>
							<xsl:text>).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Too long. Please use', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@max_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case_lang">
								<xsl:with-param name="number" select="@max_length"/>
								<xsl:with-param name="word" select="'char'"/>
							</xsl:call-template>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'or less', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'you used', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']"/>
							<xsl:text>).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNALLOWED_CHARS']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Sorry, you used restricted characters. Please use lowercase letters (a-z), digits and hyphen in this field.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TWO_HYPHENS']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Sorry, you can not use one hyphen after another.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'ENDING_HYPHEN']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Hyphen can not be used as a first or last character.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
</xsl:stylesheet>
