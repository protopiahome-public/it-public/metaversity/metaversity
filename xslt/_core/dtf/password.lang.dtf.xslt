<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Password can not be blank.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Too short. Please use at least', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@min_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case_lang">
								<xsl:with-param name="number" select="@min_length"/>
								<xsl:with-param name="word" select="'char'"/>
							</xsl:call-template>
							<xsl:text> (</xsl:text>
							<xsl:value-of select="php:function('trans', 'you used', 'DTF')"/>
							<xsl:text/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_SHORT']"/>
							<xsl:text>).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Too long. Please use', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@max_length"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="count_case_lang">
								<xsl:with-param name="number" select="@max_length"/>
								<xsl:with-param name="word" select="'char'"/>
							</xsl:call-template>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'or less', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'you used', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_LONG']"/>
							<xsl:text>).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'PASSWORD_NOT_STRONG']">
					<div class="_error">
						<span>The password is not safe enough. Please combine letters with digits or increase the length.</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_password_draw_warning">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<xsl:if test="not($is_add) or $pass_info and not($pass_info/error[@field = current()/@name])">
			<div class="_comment">
				<xsl:variable name="blank_allowed" select="not($is_add) and not($document_field/@is_blank = 1)"/>
				<xsl:if test="$blank_allowed">
					<xsl:value-of select="php:function('trans', 'Leave blank if you do not want to change the password.', 'DTF')"/>
				</xsl:if>
				<xsl:if test="not($pass_info/error[@field = current()/@name])">
					<xsl:if test="$blank_allowed">
						<xsl:text> </xsl:text>
					</xsl:if>
					<xsl:value-of select="php:function('trans', 'Minimal length', 'DTF')"/>
					<xsl:call-template name="trans">
						<xsl:with-param name="html">_DASH_</xsl:with-param>
					</xsl:call-template>
					<xsl:value-of select="@min_length"/>
					<xsl:text> </xsl:text>
					<xsl:call-template name="count_case_lang">
						<xsl:with-param name="number" select="@min_length"/>
						<xsl:with-param name="word" select="'char'"/>
					</xsl:call-template>
					<xsl:text>.</xsl:text>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_password_draw_warning_2">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:variable name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<xsl:if test="not($is_add) or $pass_info and not($pass_info/error[@field = current()/@name])">
			<div class="_comment">
				<xsl:variable name="blank_allowed" select="not($is_add) and not($document_field/@is_blank = 1)"/>
				<xsl:if test="$blank_allowed">
					<xsl:value-of select="php:function('trans', 'Leave blank if you do not want to change the password.', 'DTF')"/>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_error_2">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'PASSWORDS_NOT_EQUAL']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The passwords do not match.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template match="field[@type = 'password' and ancestor::doctype]" mode="dtf_password_confirm_title">
		<xsl:value-of select="php:function('trans', 'Confirm the password', 'DTF')"/>
	</xsl:template>
</xsl:stylesheet>
