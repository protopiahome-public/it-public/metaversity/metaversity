<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'access_level']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field-error</xsl:attribute>
			</xsl:if>
			<label class="title-" for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
			</label>
			<div class="input-">
				<select id="f-{@name}" class="f-{@name}" name="{@name}">
					<xsl:for-each select="item">
						<option value="{@level}">
							<xsl:choose>
								<xsl:when test="$pass_info/vars/var[@name = current()/../@name] = @level">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
								<xsl:when test="$pass_info/vars/var[@name = current()/../@name]"/>
								<xsl:when test="$document_field/@level = @level">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
								<xsl:when test="$is_add and @is_default = 1">
									<xsl:attribute name="selected">selected</xsl:attribute>
								</xsl:when>
							</xsl:choose>
							<xsl:value-of select="@title"/>
						</option>
					</xsl:for-each>
				</select>
			</div>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="comment-">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
			<xsl:if test="@required_level_field_name != ''">
				<script type="text/javascript">
					window.dt_access_deps = window.dt_access_deps || [];
					window.dt_access_deps.push({
						field: '<xsl:value-of select="@name"/>',
						depends_from: '<xsl:value-of select="@required_level_field_name"/>'
					});
				</script>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
