<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'foreign_key']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
			</xsl:if>
			<xsl:if test="@dependency_data">
				<xsl:attribute name="data-dependency-data">
					<xsl:value-of select="@dependency_data"/>
				</xsl:attribute>
				<xsl:attribute name="data-dependency-strategy">
					<xsl:value-of select="@dependency_strategy"/>
				</xsl:attribute>
			</xsl:if>
			<label for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:if test="not(@is_important = 1)">
						<xsl:attribute name="class">star hide</xsl:attribute>
					</xsl:if>
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<select id="f-{@name}" class="f-{@name} js_ctrl_select_with_front ctrl ctrl__select" name="{@name}">
				<xsl:if test="@is_read_only = 1">
					<xsl:attribute name="readonly">readonly</xsl:attribute>
				</xsl:if>
				<xsl:if test="$is_add or not(@is_important = 1)">
					<option value="">
						<xsl:choose>
							<xsl:when test="@unselected_title">
								<xsl:value-of select="@unselected_title"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates mode="dtf_foreign_key_not_selected" select="."/>
							</xsl:otherwise>
						</xsl:choose>
					</option>
				</xsl:if>
				<!-- Enough for 50 levels -->
				<xsl:variable name="spaces" select="'                                                                                                    '"/>
				<xsl:for-each select=".//group | .//item">
					<xsl:choose>
						<xsl:when test="name() = 'group'">
							<xsl:if test=".//item">
								<optgroup label="{substring($spaces, 1, (@level - 1) * 2)}{@title}"/>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<option value="{@value}">
								<xsl:choose>
									<xsl:when test="$pass_info/vars/var[@name = current()/../@name] = @value">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
									<xsl:when test="$pass_info/vars/var[@name = current()/../@name]"/>
									<xsl:when test="$is_add and @value = ../@default_value">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
									<xsl:when test="$document_field = @value">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
								</xsl:choose>
								<xsl:if test="parent::group">
									<xsl:value-of select="substring($spaces, 1, parent::group/@level * 2)"/>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</option>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</select>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="_comment">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
