<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'foreign_key' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Please fill in this field.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNEXISTED_VALUE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The value you selected does not exist. Please try again.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'LINK_DEPENDENCY_FAIL']">
					<xsl:variable name="errors" select="$pass_info/error[@field = current()/@name and @name = 'LINK_DEPENDENCY_FAIL']"/>
					<xsl:variable name="context" select="."/>
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The selected value is not allowed to be used with', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:choose>
								<xsl:when test="$errors[2]">
									<xsl:value-of select="php:function('trans', 'the selected values of the fields', 'DTF')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="php:function('trans', 'the selected value of the field', 'DTF')"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:for-each select="$errors">
								<xsl:choose>
									<xsl:when test="position() = 1"/>
									<xsl:when test="position() = last()">
										<xsl:text> </xsl:text>
										<xsl:value-of select="php:function('trans', 'and')"/>
									</xsl:when>
									<xsl:otherwise>,</xsl:otherwise>
								</xsl:choose>
								<xsl:text> </xsl:text>
								<xsl:value-of select="php:function('trans', 'LQ')"/>
								<xsl:value-of select="$context/../field[@name = current()]/@title"/>
								<xsl:value-of select="php:function('trans', 'RQ')"/>
							</xsl:for-each>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'foreign_key' and ancestor::doctype]" mode="dtf_foreign_key_not_selected">
		<xsl:value-of select="php:function('trans', 'Not selected', 'DTF')"/>
	</xsl:template>
</xsl:stylesheet>
