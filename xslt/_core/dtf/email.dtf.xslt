<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'email']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
			</xsl:if>
			<label for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:if test="not(@is_important = 1)">
						<xsl:attribute name="class">star hide</xsl:attribute>
					</xsl:if>
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<input id="f-{@name}" class="ctrl ctrl__text f-{@name}" type="text" name="{@name}" value="{$document_field}">
				<xsl:if test="@max_length != 0">
					<xsl:attribute name="maxlength">
						<xsl:value-of select="@max_length"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:if test="$pass_info/vars/var[@name = current()/@name]">
					<xsl:attribute name="value">
						<xsl:value-of select="$pass_info/vars/var[@name = current()/@name]"/>
					</xsl:attribute>
				</xsl:if>
			</input>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="_comment">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
