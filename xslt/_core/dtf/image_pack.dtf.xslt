<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'image_pack']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
			</xsl:if>
			<label for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:if test="not(@is_important = 1)">
						<xsl:attribute name="class">star hide</xsl:attribute>
					</xsl:if>
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<xsl:if test="$document_field/image/@is_uploaded = 1">
				<div class="_images">
					<xsl:choose>
						<xsl:when test="not($document_field/image[@is_uploaded = 1 and @width &lt;= 256 and @height &lt;= 100])">
							<a href="{$document_field/image[1]/@url}">
								<xsl:apply-templates mode="dtf_image_pack_see" select="."/>
							</a>
						</xsl:when>
						<xsl:otherwise>
							<div class="_image">
								<xsl:for-each select="$document_field/image[@is_uploaded = 1 and @width &lt;= 256 and @height &lt;= 100][1]">
									<xsl:choose>
										<xsl:when test="$document_field/image[@is_uploaded = 1 and @width &gt; 256 or @height &gt; 100]">
											<a href="{$document_field/image[@is_uploaded = 1 and @width &gt; 256 or @height &gt; 100]/@url}">
												<img class="inline_image" src="{@url}" alt="{@name}" width="{@width}" height="{@height}"/>
											</a>
										</xsl:when>
										<xsl:otherwise>
											<img class="inline_image" src="{@url}" alt="{@name}" width="{@width}" height="{@height}"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:if>
			<input id="f-{@name}" class="js_ctrl_file_with_front ctrl ctrl__file f-{@name}" type="file" name="{@name}" value="{$document_field}">
				<xsl:attribute name="placeholder">
					<xsl:apply-templates mode="dtf_image_pack_choose" select="."/>
				</xsl:attribute>
			</input>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="_comment">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
			<xsl:if test="$document_field/image/@is_uploaded = 1 and not(@is_important = 1)">
				<div class="_image_delete">
					<input id="f-{@name}_delete" class="f-{@name} ctrl ctrl__checkbox" type="checkbox" name="{@name}_delete">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = concat(current()/@name, '_delete')]">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:when>
						</xsl:choose>
					</input>
					<label for="f-{@name}_delete" class="_inline">
						<xsl:apply-templates mode="dtf_image_pack_delete_image" select="."/>
					</label>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
