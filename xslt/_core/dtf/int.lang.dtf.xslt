<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'int' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Please fill in this field.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'NOT_INT']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The number must be integer.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_SMALL']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The number is too small.', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'Minimal value', 'DTF')"/>
							<xsl:call-template name="trans">
								<xsl:with-param name="html">_DASH_</xsl:with-param>
							</xsl:call-template>
							<xsl:value-of select="@min_value"/>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_BIG']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The number is too big.', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'Maximum value', 'DTF')"/>
							<xsl:call-template name="trans">
								<xsl:with-param name="html">_DASH_</xsl:with-param>
							</xsl:call-template>
							<xsl:value-of select="@max_value"/>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
</xsl:stylesheet>
