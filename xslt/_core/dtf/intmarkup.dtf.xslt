<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'intmarkup']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
			<xsl:if test="$pass_info/error[@field = current()/@name]">
				<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
			</xsl:if>
			<label class="_title" for="f-{@name}">
				<xsl:value-of select="@title"/>
				<xsl:text> (</xsl:text>
				<a target="_blank" href="http://wiki.devyourself.ru/Intmarkup">
					<xsl:value-of select="php:function('trans', 'syntax', 'DTF')"/>
				</a>
				<xsl:text>)</xsl:text>
				<xsl:text>:</xsl:text>
				<span class="star">
					<xsl:if test="not(@is_important = 1)">
						<xsl:attribute name="class">star hide</xsl:attribute>
					</xsl:if>
					<xsl:text>&#160;*</xsl:text>
				</span>
			</label>
			<textarea id="f-{@name}" class="f-{@name} ctrl ctrl__textarea" name="{@name}" style="height: {@editor_height}px">
				<xsl:choose>
					<xsl:when test="$pass_info/vars/var[@name = current()/@name]">
						<xsl:value-of select="$pass_info/vars/var[@name = current()/@name]"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$document_field/text"/>
					</xsl:otherwise>
				</xsl:choose>
			</textarea>
			<xsl:apply-templates mode="dtf_error" select="."/>
			<xsl:if test="@comment != ''">
				<div class="_comment">
					<xsl:value-of select="@comment"/>
				</div>
			</xsl:if>
			<xsl:if test="upload">
				<xsl:variable name="doctype_field" select="."/>
				<div class="js_intmarkup_upload field_mobile_upload" data-url-base="{upload/@url_base}" data-max-file-size="{upload/@max_file_size}" data-max-file-size-human="{upload/@max_file_size_human}">
					<input class="js_intmarkup_upload_files_input" type="hidden" name="{@name}_files" value="-">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = concat(current()/@name, '_files')]">
								<xsl:attribute name="value">
									<xsl:value-of select="$pass_info/vars/var[@name = concat(current()/@name, '_files')]"/>
								</xsl:attribute>
							</xsl:when>
						</xsl:choose>
					</input>
					<div class="js_intmarkup_upload_files _list">
						<xsl:choose>
							<xsl:when test="$pass_info/field[@name = current()/@name]/files">
								<xsl:apply-templates mode="dtf_intmarkup_upload_files" select=".">
									<xsl:with-param name="files" select="$pass_info/field[@name = current()/@name]/files/file"/>
									<xsl:with-param name="url_base" select="$doctype_field/upload/@url_base"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates mode="dtf_intmarkup_upload_files" select=".">
									<xsl:with-param name="files" select="$document_field/files/file"/>
									<xsl:with-param name="url_base" select="$doctype_field/upload/@url_base"/>
								</xsl:apply-templates>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<div class="js-fileapi-wrapper _button">
						<div class="js_browse">
							<span class="_text">
								<xsl:apply-templates mode="dtf_intmarkup_upload_attach_files" select="."/>
							</span>
							<input type="file" name="filedata"/>
						</div>
						<div class="js_upload hide">
							<div class="_progress">
								<div class="js_progress _bar"/>
							</div>
							<span class="_text">
								<xsl:apply-templates mode="dtf_intmarkup_upload_loading" select="."/>
							</span>
						</div>
					</div>
					<xsl:apply-templates mode="dtf_intmarkup_upload_js_errors" select="."/>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template match="field[@type = 'intmarkup' and ancestor::doctype]" mode="dtf_intmarkup_upload_files">
		<xsl:param name="files"/>
		<xsl:param name="url_base"/>
		<div class="js_file_template js_file _file hide">
			<span class="js_file_remove _remove">
				<i class="_icon fa fa-times"/>
			</span>
			<a target="_blank" href="{$url_base}">
				<i class="_icon fa fa-download"/>
				<span class="js_file_template_file_name _file_name"/>
				<span class="_size">
					<xsl:text> (</xsl:text>
					<span class="js_file_template_size"/>
					<xsl:text>)</xsl:text>
				</span>
			</a>
		</div>
		<xsl:for-each select="$files">
			<div class="js_file _file" data-file-id="{@id}">
				<span class="js_file_remove _remove">
					<i class="_icon fa fa-times"/>
				</span>
				<a target="_blank" href="{$url_base}{@id}/">
					<i class="_icon fa fa-download"/>
					<span class="_file_name">
						<xsl:value-of select="@file_name"/>
					</span>
					<span class="_size">
						<xsl:text> (</xsl:text>
						<xsl:value-of select="@size_human"/>
						<xsl:text>)</xsl:text>
					</span>
				</a>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
