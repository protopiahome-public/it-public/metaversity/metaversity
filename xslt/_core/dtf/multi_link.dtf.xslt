<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="field[@type = 'multi_link']" mode="dtf">
		<xsl:param name="is_add" select="not(ancestor::doctype/../document)"/>
		<xsl:param name="document_field" select="ancestor::doctype/../document/field[@name = current()/@name]"/>
		<xsl:variable name="doctype_field" select="."/>
		<xsl:if test=".//item">
			<div id="field-{@name}" class="field field2" jq-name="{@name}" jq-type="{@type}">
				<xsl:if test="$pass_info/error[@field = current()/@name]">
					<xsl:attribute name="class">field field2 field2__error</xsl:attribute>
				</xsl:if>
				<label for="f-{@name}">
					<xsl:value-of select="@title"/>
					<xsl:text>:</xsl:text>
				</label>
				<xsl:choose>
					<xsl:when test="$doctype_field/@design = 'CHECKBOXES'">
						<div class="_set">
							<xsl:if test="@max_height">
								<xsl:attribute name="style">
									<xsl:text>overflow: auto; max-height: </xsl:text>
									<xsl:value-of select="@max_height"/>
									<xsl:text>px</xsl:text>
								</xsl:attribute>
							</xsl:if>
							<xsl:for-each select=".//item">
								<div class="_set_item">
									<xsl:if test="$doctype_field/@tree_mode">
										<xsl:attribute name="style">
											<xsl:value-of select="concat('margin-left: ', @level * 20 - 20, 'px;')"/>
										</xsl:attribute>
									</xsl:if>
									<input id="f-{$doctype_field/@name}-{@id}" class="jq-dtf-multi-link-item ctrl ctrl__checkbox" type="checkbox" name="{$doctype_field/@name}[{@id}]" value="1" jq-dtf-name="{$doctype_field/@name}" data-id="{@id}">
										<xsl:if test="$doctype_field/@is_read_only = 1">
											<xsl:attribute name="disabled">disabled</xsl:attribute>
										</xsl:if>
										<xsl:attribute name="jq-ancestors">
											<xsl:value-of select="@ancestors"/>
										</xsl:attribute>
										<xsl:attribute name="jq-descendants">
											<xsl:value-of select="@descendants"/>
										</xsl:attribute>
										<xsl:choose>
											<xsl:when test="$pass_info/vars/var[@name = $doctype_field/@name]/item[@index = current()/@id] = 1">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:when>
											<xsl:when test="$pass_info/vars/var"/>
											<xsl:when test="$is_add and @is_default = 1">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:when>
											<xsl:when test="$document_field/item[@id = current()/@id]">
												<xsl:attribute name="checked">checked</xsl:attribute>
											</xsl:when>
										</xsl:choose>
									</input>
									<label for="f-{$doctype_field/@name}-{@id}" class="_inline">
										<xsl:value-of select="@title"/>
									</label>
								</div>
							</xsl:for-each>
						</div>
					</xsl:when>
					<xsl:when test="$doctype_field/@design = 'MULTISELECT' or $doctype_field/@design = 'CHOSEN'">
						<select id="f-{@name}" name="{@name}[]" multiple="multiple">
							<xsl:if test="@is_read_only = 1">
								<xsl:attribute name="readonly">readonly</xsl:attribute>
							</xsl:if>
							<xsl:attribute name="class">
								<xsl:value-of select="concat('f-', @name)"/>
								<xsl:text> ctrl ctrl__multiselect</xsl:text>
								<xsl:if test="$doctype_field/@design = 'CHOSEN'"> jq-dtf-multi-link-chosen</xsl:if>
							</xsl:attribute>
							<xsl:for-each select=".//item">
								<option value="{@id}">
									<xsl:choose>
										<xsl:when test="$pass_info/vars/var[@name = $doctype_field/@name]/item[text() = current()/@id]">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$pass_info/vars/var"/>
										<xsl:when test="$is_add and @is_default = 1">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
										<xsl:when test="$document_field/item[@id = current()/@id]">
											<xsl:attribute name="selected">selected</xsl:attribute>
										</xsl:when>
									</xsl:choose>
									<xsl:value-of select="@title"/>
								</option>
							</xsl:for-each>
						</select>
					</xsl:when>
				</xsl:choose>
				<xsl:apply-templates mode="dtf_error" select="."/>
				<xsl:if test="@comment != ''">
					<div class="_comment">
						<xsl:value-of select="@comment"/>
					</div>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
