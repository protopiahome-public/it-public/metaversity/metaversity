<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template match="field[@type = 'image_pack' and ancestor::doctype]" mode="dtf_error">
		<xsl:if test="$pass_info/error[@field = current()/@name]">
			<xsl:choose>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNFILLED']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Please fill in this field.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_BIG_SIZE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The file is too big.', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'Maximum allowed size is', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@max_file_size_formatted"/>
							<xsl:text>, </xsl:text>
							<xsl:value-of select="php:function('trans', 'your image has', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_BIG_SIZE']"/>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_SMALL']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The image is too small.', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'Minimum allowed size is', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="@min_width"/>
							<xsl:text>x</xsl:text>
							<xsl:value-of select="@min_height"/>
							<xsl:text>, </xsl:text>
							<xsl:value-of select="php:function('trans', 'your image has size', 'DTF')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'TOO_SMALL']"/>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNKNOWN_TYPE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Unknown file type. Only jpeg, gif and png are supported.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'CANT_RESIZE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Error has occured while image transformation', 'DTF')"/>
							<xsl:text> (code = </xsl:text>
							<xsl:value-of select="$pass_info/error[@field = current()/@name and @name = 'CANT_RESIZE']"/>
							<xsl:text>)</xsl:text>
							<xsl:text>.</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'TOO_BIG_SIZE_INI']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The file is too big.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_FORM_SIZE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'The file is too big.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_PARTIAL']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Error has occured while file uploading', 'DTF')"/>
							<xsl:text> (code = 12).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_NO_TMP_DIR']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Error has occured while file uploading', 'DTF')"/>
							<xsl:text> (code = 11).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_CANT_WRITE']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Error has occured while file uploading', 'DTF')"/>
							<xsl:text> (code = 10).</xsl:text>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UPLOAD_ERR_EXTENSION']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Depreceted extension. Only jpeg, gif and png are supported.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
				<xsl:when test="$pass_info/error[@field = current()/@name and @name = 'UNKNOWN_UPLOAD_ERROR']">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Unknown error while uploading the file.', 'DTF')"/>
						</span>
					</div>
				</xsl:when>
			</xsl:choose>
		</xsl:if>
		<xsl:apply-templates select="." mode="dtf_error_extend"/>
	</xsl:template>
	<xsl:template match="field[@type = 'image_pack' and ancestor::doctype]" mode="dtf_image_pack_delete_image">
		<xsl:value-of select="php:function('trans', 'Delete image', 'DTF')"/>
	</xsl:template>
	<xsl:template match="field[@type = 'image_pack' and ancestor::doctype]" mode="dtf_image_pack_see">
		<xsl:value-of select="php:function('trans', 'View', 'DTF')"/>
	</xsl:template>
	<xsl:template match="field[@type = 'image_pack' and ancestor::doctype]" mode="dtf_image_pack_choose">
		<xsl:value-of select="php:function('trans', 'Choose file', 'DTF')"/>
	</xsl:template>
</xsl:stylesheet>
