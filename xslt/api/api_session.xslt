<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
    <xsl:include href="../_site/base_page_layout.xslt"/>
    <xsl:variable name="top_section" select="'settings'"/>
    <xsl:variable name="top_section_main_page" select="true()"/>
    <xsl:variable name="module_url" select="concat($main_prefix, '/api_session/')"/>
    <xsl:template match="api_session">
        <xsl:apply-templates mode="header" select="/root">
            <xsl:with-param name="title" select="php:function('trans', 'App authorize')"/>
        </xsl:apply-templates>
        <div class="content text">
            <h1 class="head_duplicate">
                <xsl:value-of select="php:function('trans', 'App authorize')"/>
            </h1>
            <xsl:call-template name="_draw"/>
        </div>
    </xsl:template>
    <xsl:template name="_draw">
        <form method="post" action="{$save_prefix}/api_session/">
            <input type="hidden" name="app_title" value="{@app_title}"/>
            <input type="hidden" name="redirect_url" value="{@redirect_url}"/>
            <p class="box">
                Приложение <b>
                    <xsl:value-of select="@app_title"/>
                </b> по адресу <b>
                    <xsl:value-of select="@redirect_url"/>
                </b> предлагает вам предоставить ему доступ к вашему аккаунту. Приложению будут доступны все действия, которые доступны лично вам. Распоряжайтесь своим доступом предусмотрительно!
            </p>
            <div class="field2">
                <button class="ctrl ctrl__button ctrl__button__big ctrl__button__red ctrl__button__for_right">Предоставить</button> 
                <a href="{@redirect_url}" class="ctrl ctrl__button ctrl__button__big">Отказаться</a>
            </div>
        </form>
    </xsl:template>
    <xsl:template mode="title" match="/root">
        <xsl:value-of select="php:function('trans', 'App authorize')"/>
    </xsl:template>
</xsl:stylesheet>
