<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_position_details_match">
		<xsl:if test="@user_id != ''">
			<div class="box box__result p">
				<p>
					<xsl:value-of select="php:function('trans', 'Position match')"/>
					<xsl:text>: </xsl:text>
					<b>
						<xsl:value-of select="@match"/>
						<xsl:text>%</xsl:text>
					</b>
				</p>
				<xsl:call-template name="draw_position_credit">
					<xsl:with-param name="position_credit_min_match" select="$current_stream/@position_credit_min_match"/>
					<xsl:with-param name="class" select="'_credit'"/>
					<xsl:with-param name="line_class" select="'p'"/>
				</xsl:call-template>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_position_details">
		<xsl:if test="$current_position/descr/div != ''">
			<h3>
				<xsl:value-of select="php:function('trans', 'Position description')"/>
			</h3>
			<xsl:copy-of select="$current_position/descr/div"/>
		</xsl:if>
		<h3>
			<xsl:value-of select="php:function('trans', 'Position competences', 'MATCH')"/>
		</h3>
		<xsl:if test="not(competence)">
			<p>
				<xsl:value-of select="php:function('trans', 'No competences were found. It seems, stream administratiors have not filled in necessary data yet.')"/>
			</p>
		</xsl:if>
		<xsl:if test="competence">
			<div class="rose">
				<i data-column='{{"name": "required_result", "color": "#818181", "fillColor": "rgba(114, 114, 114, 0.5)"}}'/>
				<xsl:if test="@user_id != ''">
					<i data-column='{{"name": "result", "color": "#5d935f", "fillColor": "rgba(11, 166, 17, 0.45)"}}'/>
				</xsl:if>
				<xsl:for-each select="competence">
					<xsl:variable name="competence_title">
						<xsl:call-template name="draw_competence"/>
					</xsl:variable>
					<i data-row='{{"id": {@id}, "title": {php:function("json_encode", $competence_title)}, "hint_line2": "{@result} / {@required_result}", "required_result": {@required_result}, "result": {@result}}}'/>
				</xsl:for-each>
			</div>
			<table class="table w100p maxw600">
				<tr>
					<th>
						<xsl:value-of select="php:function('trans', 'Competence')"/>
					</th>
					<th class="right pr6">
						<span class="nobr">
							<xsl:choose>
								<xsl:when test="@user_id != ''">
									<xsl:value-of select="php:function('trans', 'Result / Required')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="php:function('trans', 'Required result')"/>
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</th>
				</tr>
			</table>
			<table class="table p w100p maxw600">
				<xsl:for-each select="competence">
					<tr>
						<td class="cell_text w100p">
							<xsl:choose>
								<xsl:when test="@show_link = 1">
									<a href="{$users_prefix}/{$current_user/@login}/competences/{$current_stream/@name}/{@id}/">
										<xsl:call-template name="draw_competence"/>
									</a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="draw_competence"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
						<td class="cell_result pr6 pl10 nobr">
							<xsl:if test="../@user_id != ''">
								<xsl:call-template name="draw_result_hl"/>
							</xsl:if>
							<span class="result_hl_of">
								<xsl:if test="../@user_id != ''">
									<xsl:text> / </xsl:text>
								</xsl:if>
								<xsl:value-of select="@required_result"/>
							</span>
						</td>
						<!--<td class="cell_iconic_stat pr6 show600">
							<xsl:call-template name="draw_user_competence_results_iconic_stat"/>
						</td>-->
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
