<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_position_credit">
		<xsl:param name="credit" select="credit"/>
		<xsl:param name="credit_expert_user_id" select="@credit_expert_user_id"/>
		<xsl:param name="credit_comment" select="@credit_comment"/>
		<xsl:param name="match" select="@match"/>
		<xsl:param name="position_credit_min_match" select="101"/>
		<xsl:param name="class" select="''"/>
		<xsl:param name="line_class" select="''"/>
		<xsl:variable name="_credit_expert_user_id">
			<xsl:choose>
				<xsl:when test="$credit">
					<xsl:value-of select="$credit/@expert_user_id"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$credit_expert_user_id"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="_credit_comment">
			<xsl:choose>
				<xsl:when test="$credit">
					<xsl:value-of select="$credit/@comment"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$credit_comment"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$_credit_expert_user_id != ''">
				<div class="{$class} position_credit position_credit__expert">
					<div class="{$line_class} _credit_reason">
						<i class="_credit_icon fa fa-certificate"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Credited by the expert')"/>
						<span class="_expert">
							<xsl:text> (</xsl:text>
							<a href="{/root/user_short[@id = $_credit_expert_user_id]/@url}">
								<xsl:value-of select="/root/user_short[@id = $_credit_expert_user_id]/@visible_name"/>
							</a>
							<xsl:text>)</xsl:text>
						</span>
					</div>
					<xsl:call-template name="draw_kv">
						<xsl:with-param name="key" select="php:function('trans', 'Comment')"/>
						<xsl:with-param name="text" select="$_credit_comment"/>
						<xsl:with-param name="class" select="concat($line_class, ' _credit_comment')"/>
					</xsl:call-template>
				</div>
			</xsl:when>
			<xsl:when test="$current_stream/@position_credit_min_match &gt; 0 and $match &gt;= $current_stream/@position_credit_min_match">
				<div class="{$class} position_credit position_credit__auto">
					<div class="{$line_class} _credit_reason">
						<i class="_credit_icon fa fa-certificate"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Credited based on the position match')"/>
					</div>
				</div>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
