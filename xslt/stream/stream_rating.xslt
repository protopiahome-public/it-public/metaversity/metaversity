<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:include href="include/stream_rating.inc.xslt"/>
	<xsl:include href="../positions/include/position_credits.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'ratings'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="current_position" select="/root/position_full[1]"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'ratings/', $current_position/@id, '/'	)"/>
	<xsl:template match="math_rating">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Ratings')"/>
			<xsl:with-param name="url" select="concat($current_stream/@url, 'ratings/')"/>
		</xsl:apply-templates>
		<div class="content text">
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<h1>
			<span>
				<xsl:if test="$current_stream_access/@has_moderator_rights = 1">
					<xsl:attribute name="class">for_icon_right</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="$current_position/@title"/>
			</span>
			<xsl:if test="$current_stream_access/@has_admin_rights = 1">
				<a href="{$current_stream/@url}admin/positions/{$current_position/@id}/">
					<i class="red fa fa-gear"/>
				</a>
			</xsl:if>
		</h1>
		<xsl:call-template name="draw_stream_rating">
			<xsl:with-param name="base_url" select="concat($users_prefix, '/')"/>
			<xsl:with-param name="url_suffix" select="concat('results/', $current_stream/@name, '/', $current_position/@id, '/')"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<xsl:if test="/root/math_rating/@cache_miss = 1">
			<script type="text/javascript">
				var wait_for_cache_ajax_page = "math_rating_wait_for_cache";
				var post_vars = {stream_id: <xsl:value-of select="$current_stream/@id"/>};
			</script>
			<script type="text/javascript" src="{$main_prefix}/js/wait_for_cache.js?v1"/>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$current_position/@title"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="php:function('trans', 'Rating')"/>
		<xsl:text>)</xsl:text>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
