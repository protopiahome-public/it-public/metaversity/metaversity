<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'positions'"/>
	<xsl:variable name="stream_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'positions/')"/>
	<xsl:template match="stream_positions">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Positions')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($stream_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Positions')"/>
			</h1>
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<p>
			<xsl:value-of select="php:function('trans', '&lt;b&gt;Position&lt;/b&gt; is a direction of your personal growth.')" disable-output-escaping="yes"/>
		</p>
		<p>
			<xsl:value-of select="php:function('trans', 'We expect that you focus on (= work on) 1 or 2 positons at once. The choice can be changed at any time.')"/>
		</p>
		<p>
			<xsl:value-of select="php:function('trans', 'Recommendations of activities and materials will be based on your focus.')"/>
		</p>
		<xsl:if test="not($user/@id)">
			<p class="bold">
				<xsl:call-template name="trans">
					<xsl:with-param name="html">You need to <a href="%1%">login</a> to be able to choose positions!</xsl:with-param>
					<xsl:with-param name="var1" select="$login_url"/>
					<xsl:with-param name="debug" select="0"/>
				</xsl:call-template>
			</p>
		</xsl:if>
		<xsl:if test="$user/@id">
			<p class="bigger">
				<b>
					<xsl:attribute name="class">
						<xsl:choose>
							<xsl:when test="count(position[@checked = 1]) &gt;= 1 and count(position[@checked = 1]) &lt;= 2">js_position_count_text green</xsl:when>
							<xsl:otherwise>js_position_count_text red</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="php:function('trans', 'Positions in focus')"/>
					<xsl:text>: </xsl:text>
					<span class="js_position_count">
						<xsl:value-of select="count(position[@checked = 1])"/>
					</span>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', '(recommended 1 or 2)')"/>
				</b>
			</p>
		</xsl:if>
		<xsl:variable name="obj" select="/root/stream_study_level_restrictions/object"/>
		<xsl:variable name="main_positions" select="position[not($obj/@id = @id) or $obj[@show = 1]/@id = @id]"/>
		<xsl:variable name="other_positions" select="position[not($main_positions/@id = @id)]"/>
		<xsl:choose>
			<xsl:when test="$main_positions[1] and /root/stream_study_level_restrictions/@current_study_level_id &gt; 0">
				<h2>
					<xsl:value-of select="php:function('trans', 'Positions recommended for your study level')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'LQ')"/>
					<xsl:value-of select="$current_stream/cities/@current_study_level_title"/>
					<xsl:value-of select="php:function('trans', 'RQ')"/>
				</h2>
				<xsl:call-template name="_draw_list">
					<xsl:with-param name="positions" select="$main_positions"/>
					<xsl:with-param name="show_levels" select="true()"/>
				</xsl:call-template>
				<xsl:if test="$other_positions[1]">
					<div>
						<br/>
					</div>
					<h2>
						<xsl:value-of select="php:function('trans', 'Other positions')"/>
					</h2>
					<xsl:call-template name="_draw_filter"/>
					<div class="js_other_positions">
						<xsl:call-template name="_draw_list">
							<xsl:with-param name="positions" select="$other_positions"/>
							<xsl:with-param name="show_levels" select="true()"/>
							<xsl:with-param name="position_class" select="'js_other_position'"/>
						</xsl:call-template>
					</div>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="_draw_filter"/>
				<div class="js_other_positions">
					<xsl:call-template name="_draw_list">
						<xsl:with-param name="positions" select="position"/>
						<xsl:with-param name="position_class" select="'js_other_position'"/>
					</xsl:call-template>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="_draw_filter">
		<xsl:variable name="study_levels_to_draw" select="/root/stream_study_level_restrictions/study_level[not(../@current_study_level_id) or @id != ../@current_study_level_id]"/>
		<xsl:if test="$study_levels_to_draw[1]">
			<div class="head_dd_align_left p">
				<span class="head_dd">
					<select class="js_study_level_select ctrl ctrl__select">
						<option value="">
							<xsl:value-of select="php:function('trans', 'All study levels')"/>
						</option>
						<xsl:for-each select="$study_levels_to_draw">
							<option value="{@id}">
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</span>
			</div>
		</xsl:if>
		<div class="js_other_positions_not_found p hide">
			<xsl:value-of select="php:function('trans', 'Positions were not found.')"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw_list">
		<xsl:param name="positions"/>
		<xsl:param name="show_levels" select="false()"/>
		<xsl:param name="position_class" select="''"/>
		<xsl:if test="not($positions)">
			<p>
				<xsl:value-of select="php:function('trans', 'Positions were not found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="$positions">
			<table class="table w100p">
				<xsl:choose>
					<xsl:when test="not($user/@id)">
						<xsl:attribute name="class">table table__no_th w100p</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<tr>
							<th colspan="2" class="right">
								<xsl:value-of select="php:function('trans', 'take in your focus')"/>
							</th>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:for-each select="$positions">
					<xsl:variable name="levels_to_draw" select="/root/stream_study_level_restrictions/object[@id = current()/@id]/study_level"/>
					<tr class="js_position {$position_class}" data-id="{@id}">
						<xsl:attribute name="data-study-levels">
							<xsl:text>,</xsl:text>
							<xsl:for-each select="$levels_to_draw">
								<xsl:value-of select="@id"/>
								<xsl:text>,</xsl:text>
							</xsl:for-each>
						</xsl:attribute>
						<td class="cell_text pr4">
							<xsl:if test="$user/@id">
								<div class="cell_float_ctrl cell_float_switchery right p6">
									<input data-position-id="{@id}" class="js_position_checkbox" type="checkbox">
										<xsl:if test="@checked = 1">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:if>
									</input>
								</div>
							</xsl:if>
							<span id="js_position_clickable_{@id}" class="js_position_clickable clickable dashed">
								<xsl:value-of select="@title"/>
							</span>
							<xsl:if test="$show_levels and /root/stream_study_level_restrictions/@current_study_level_id or $levels_to_draw[1]">
								<div class="cell_text_note">
									<xsl:choose>
										<xsl:when test="$levels_to_draw">
											<xsl:for-each select="$levels_to_draw">
												<xsl:value-of select="@title"/>
												<xsl:if test="position() != last()">, </xsl:if>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="php:function('trans', 'All study levels')"/>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</xsl:if>
						</td>
						<!--<td class="cell_expand cell_expand__content pr4">
							<xsl:if test="$user/@id">
								<div class="cell_float_ctrl cell_float_switchery right p6">
									<input data-position-id="{@id}" class="js_position_checkbox" type="checkbox">
										<xsl:if test="@checked = 1">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:if>
									</input>
								</div>
							</xsl:if>
							<span class="cell_expand_icon fa"/>
							<span>
								<xsl:if test="descr_html/div != ''">
									<xsl:attribute name="class">cell_expand_text</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@title"/>
							</span>
							<xsl:if test="$show_levels and /root/stream_study_level_restrictions/@current_study_level_id or $levels_to_draw[1]">
								<div class="cell_text_note">
									<xsl:choose>
										<xsl:when test="$levels_to_draw">
											<xsl:for-each select="$levels_to_draw">
												<xsl:value-of select="@title"/>
												<xsl:if test="position() != last()">, </xsl:if>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="php:function('trans', 'All study levels')"/>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</xsl:if>
							<xsl:if test="descr_html/div">
								<div class="cell_expand_content hide">
									<xsl:copy-of select="descr_html/div"/>
								</div>
							</xsl:if>
						</td>-->
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			window.post_vars = {
				stream_id: <xsl:value-of select="$current_stream/@id"/>
			}
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/switchery.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/rose.js?v5"/>
		<script type="text/javascript" src="{$main_prefix}/js/dynurls.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_positions.js?v6"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Positions')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
