<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_layout.xslt"/>
	<xsl:variable name="current_stream" select="/root/stream_current[1]"/>
	<xsl:variable name="current_user" select="/root/user_short[1]"/>
	<xsl:variable name="admin" select="/root/user_short[2]"/>
	<xsl:template match="/root">
		<html>
			<head>
				<title>
					<xsl:choose>
						<xsl:when test="/root/is_now_member = 1">
							<xsl:value-of select="php:function('trans', 'Request accepted', 'STREAM_PRETENDER_MAIL')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'Request rejected', 'STREAM_PRETENDER_MAIL')"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'DASH')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$current_stream/@title"/>
				</title>
			</head>
			<body>
				<p>
					<xsl:value-of select="php:function('trans', 'Hello,')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$current_user/@visible_name"/>
					<xsl:text>.</xsl:text>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'Administrator', 'STREAM_PRETENDER_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="{$users_prefix}/{$admin/@login}/">
						<xsl:value-of select="$admin/@visible_name"/>
					</a>
					<xsl:text> </xsl:text>
					<xsl:choose>
						<xsl:when test="/root/is_now_member = 1">
							<xsl:value-of select="php:function('trans', 'accepted your request for membership into', 'STREAM_PRETENDER_MAIL')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="php:function('trans', 'rejected your request for membership into', 'STREAM_PRETENDER_MAIL')"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text> </xsl:text>
					<a href="{$current_stream/@url}">
						<xsl:value-of select="$current_stream/@title"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
				<p>
					<xsl:value-of select="php:function('trans', 'If you did not make a request – please write to administrators:', 'STREAM_PRETENDER_MAIL')"/>
					<xsl:text> </xsl:text>
					<a href="mailto:{$sys_params/@info_email}">
						<xsl:value-of select="$sys_params/@info_email"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
