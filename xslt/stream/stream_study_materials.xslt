<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:include href="include/stream_study_material_common.inc.xslt"/>
	<xsl:include href="include/stream_study_materials.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'study_materials'"/>
	<xsl:variable name="stream_section_main_page" select="/root/stream_study_materials/@filter = 'recommended'"/>
	<xsl:variable name="base_url" select="concat($current_stream/@url, 'materials/')"/>
	<xsl:variable name="study_material_list_url">
		<xsl:value-of select="$base_url"/>
		<xsl:if test="$user/@id &gt; 0 and /root/stream_study_materials/@filter != 'recommended'">
			<xsl:value-of select="concat(/root/stream_study_materials/@filter, '/')"/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="module_url" select="$study_material_list_url"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_study_materials">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Materials')"/>
			<xsl:with-param name="url" select="$base_url"/>
			<xsl:with-param name="use_url" select="not($stream_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Materials')"/>
			</h1>
			<xsl:call-template name="draw_stream_study_materials"/>
		</div>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Materials')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
