<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:include href="../activities/include/activity_common.inc.xslt"/>
	<xsl:include href="include/stream_activity.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'activities'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="activity" select="/root/stream_activity"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'activities/', /root/stream_activity/@id, '/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_activity">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Schedule')"/>
			<xsl:with-param name="url" select="concat($current_stream/@url, 'activities/')"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span>
					<xsl:if test="$current_stream_access/@has_moderator_rights = 1">
						<xsl:attribute name="class">for_icon_right</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="@title"/>
				</span>
				<xsl:if test="$current_stream_access/@has_moderator_rights = 1">
					<a href="{$current_stream/@url}admin/activities/{@id}/">
						<i class="red fa fa-gear"/>
					</a>
				</xsl:if>
			</h1>
			<xsl:call-template name="draw_activity"/>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			window.post_vars = {
				stream_id: <xsl:value-of select="$current_stream/@id"/>
			}
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/dynurls.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/stream_activity.js?v3"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="/root/stream_activity/@title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
	</xsl:template>
</xsl:stylesheet>
