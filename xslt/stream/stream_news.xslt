<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:include href="../activities/include/activity_common.inc.xslt"/>
	<xsl:include href="../news/include/news.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="/root/stream_news/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="stream_section" select="'news'"/>
	<xsl:variable name="stream_section_main_page" select="/root/stream_news/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="current_user" select="$user"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'news/')"/>
	<xsl:template match="stream_news">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'News')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($stream_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'News')"/>
			</h1>
			<div class="head_dd_align_left p" style="padding-bottom: 6px;">
				<xsl:apply-templates select="filters" mode="filter_one">
					<xsl:with-param name="name" select="'news_type'"/>
					<xsl:with-param name="prefix_html">
						<xsl:value-of select="php:function('trans', 'Type')"/>
						<xsl:text>: </xsl:text>
					</xsl:with-param>
					<xsl:with-param name="class" select="'head_dd_for_right'"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="filters" mode="filter_one">
					<xsl:with-param name="name" select="'city'"/>
					<xsl:with-param name="prefix_html">
						<div class="fa fa-map-marker"/>
						<xsl:text> </xsl:text>
					</xsl:with-param>
				</xsl:apply-templates>
			</div>
			<div class="maxw800">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<!--<xsl:apply-templates select="filters" mode="filters_show"/>-->
		<xsl:call-template name="draw_news">
			<xsl:with-param name="current_user_url" select="concat($users_prefix, '/', $user/@login, '/')"/>
		</xsl:call-template>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'News')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title"/>
		<xsl:text>)</xsl:text>
	</xsl:template>
</xsl:stylesheet>
