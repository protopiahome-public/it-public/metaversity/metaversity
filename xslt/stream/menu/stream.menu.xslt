<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:variable name="current_stream" select="/root/stream_current[1]"/>
	<xsl:variable name="current_stream_access" select="/root/stream_access[1]"/>
	<xsl:template mode="body_class" match="/root">with_menu_left with_menu_top</xsl:template>
	<xsl:template mode="menu_left" match="/root">
		<div class="menu_left">
			<div class="_submenu">
				<div class="js_btn_menu_left_open_main menu_item menu_item__clickable">
					<span class="_icon fa fa-home"/>
					<xsl:text> </xsl:text>
					<span class="_text _text__dashed">
						<xsl:value-of select="php:function('trans', 'Main menu')"/>
					</span>
					<xsl:text> </xsl:text>
					<span class="_down fa fa-caret-down"/>
				</div>
				<div class="js_menu_left_main _submenu_contents hide">
					<xsl:apply-templates mode="menu_items_main" select="/root"/>
				</div>
			</div>
			<div class="_submenu">
				<xsl:choose>
					<xsl:when test="$user/@id">
						<xsl:apply-templates mode="menu_items_user_title" select="/root">
							<xsl:with-param name="clickable" select="true()"/>
						</xsl:apply-templates>
						<div class="js_menu_left_user _submenu_contents hide">
							<xsl:apply-templates mode="menu_items_lang" select="/root"/>
							<xsl:apply-templates mode="menu_items_user" select="/root"/>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates mode="menu_items_lang" select="/root"/>
						<xsl:apply-templates mode="menu_items_guest" select="/root"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div class="_sep"/>
			<div class="_title">
				<xsl:choose>
					<xsl:when test="$stream_section = 'news' and $stream_section_main_page">
						<xsl:value-of select="$current_stream/@title"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$current_stream/@url}">
							<xsl:value-of select="$current_stream/@title"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<xsl:if test="$user/@id">
				<div class="_undertitle">
					<xsl:choose>
						<xsl:when test="/root/stream_status">
							<i class="fa fa-sign-in"/>
							<xsl:text> </xsl:text>
							<xsl:call-template name="draw_stream_status_text">
								<xsl:with-param name="you" select="true()"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<a href="{$current_stream/@url}status/" class="underline_inner_span">
								<i class="fa fa-sign-in"/>
								<xsl:text> </xsl:text>
								<span>
									<xsl:call-template name="draw_stream_status_text">
										<xsl:with-param name="you" select="true()"/>
									</xsl:call-template>
								</span>
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$stream_section = 'news' and $stream_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-bell-o"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'News')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_stream/@url}" class="menu_item">
						<xsl:if test="$stream_section = 'news'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-bell-o"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'News')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$stream_section = 'positions' and $stream_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-crosshairs"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Positions')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_stream/@url}positions/" class="menu_item">
						<xsl:if test="$stream_section = 'positions'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-crosshairs"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Positions')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$stream_section = 'activities' and $stream_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon _icon__16 fa fa-calendar-o"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Schedule')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_stream/@url}activities/" class="menu_item">
						<xsl:if test="$stream_section = 'activities'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon _icon__16 fa fa-calendar-o"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Schedule')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$stream_section = 'study_materials' and $stream_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-book"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Materials')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_stream/@url}materials/" class="menu_item">
						<xsl:if test="$stream_section = 'study_materials'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-book"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Materials')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$stream_section = 'users' and $stream_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-users"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Students')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_stream/@url}users/" class="menu_item">
						<xsl:if test="$stream_section = 'users'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-users"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Students')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$stream_section = 'ratings' and $stream_section_main_page">
					<span class="menu_item menu_item__current">
						<span class="_icon fa fa-bar-chart-o"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Ratings')"/>
						</span>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{$current_stream/@url}ratings/" class="menu_item">
						<xsl:if test="$stream_section = 'ratings'">
							<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
						</xsl:if>
						<span class="_icon fa fa-bar-chart-o"/>
						<xsl:text> </xsl:text>
						<span class="_text">
							<xsl:value-of select="php:function('trans', 'Ratings')"/>
						</span>
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$user/@id">
				<span class="menu_item">
					<span class="_icon fa fa-user"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'My profile')"/>
					</span>
				</span>
				<a href="{$users_prefix}/{$user/@login}/results/{$current_stream/@name}/" class="menu_item menu_item__sub">
					<span class="_icon fa fa-angle-right"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Results')"/>
						<xsl:text> </xsl:text>
						<i class="fa fa-external-link"/>
					</span>
				</a>
				<a href="{$users_prefix}/{$user/@login}/competences/{$current_stream/@name}/" class="menu_item menu_item__sub">
					<span class="_icon fa fa-angle-right"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Competences')"/>
						<xsl:text> </xsl:text>
						<i class="fa fa-external-link"/>
					</span>
				</a>
				<a href="{$users_prefix}/{$user/@login}/log/?filter[stream]={$current_stream/@id}" class="menu_item menu_item__sub">
					<span class="_icon fa fa-angle-right"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Roles and Marks')"/>
						<xsl:text> </xsl:text>
						<i class="fa fa-external-link"/>
					</span>
				</a>
			</xsl:if>
			<xsl:if test="$current_stream_access/@has_moderator_rights = 1">
				<a href="{$current_stream/@url}admin/" class="menu_item">
					<span class="_icon fa fa-gear"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'Administration')"/>
					</span>
				</a>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
