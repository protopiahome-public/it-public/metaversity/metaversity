<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'403'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:template match="error_403">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$current_stream/@title_short"/>
			<xsl:with-param name="url" select="$current_stream/@url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="access_denied">
			<i class="fa fa-ban red"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'Access denied')"/>
		</div>
		<xsl:if test="not($user/@id != '')">
			<p>
				<xsl:value-of select="php:function('trans', 'Please', 'PLEASE_LOGIN')"/>
				<xsl:text> </xsl:text>
				<a href="{$login_url}">
					<xsl:value-of select="php:function('trans', 'login', 'PLEASE_LOGIN')"/>
				</a>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'to access this page.', 'PLEASE_LOGIN')"/>
			</p>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Access denied')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title"/>
		<xsl:text>)</xsl:text>
	</xsl:template>
</xsl:stylesheet>
