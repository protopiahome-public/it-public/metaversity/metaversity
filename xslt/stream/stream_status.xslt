<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'status'"/>
	<xsl:variable name="stream_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'status/')"/>
	<xsl:template match="stream_status">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Status')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($stream_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<div class="maxw600">
				<xsl:call-template name="_draw"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<p>
			<span class="hl">
				<xsl:value-of select="php:function('trans', 'Your status')"/>
				<xsl:text>: </xsl:text>
			</span>
			<xsl:choose>
				<xsl:when test="$current_stream_access/@status = 'guest'"/>
				<xsl:when test="$current_stream_access/@status = 'pretender'">
					<xsl:value-of select="php:function('trans', 'You are on moderation')"/>
				</xsl:when>
				<xsl:when test="$current_stream_access/@status = 'member'">
					<xsl:value-of select="php:function('trans', 'You are a student of this stream')"/>
				</xsl:when>
				<xsl:when test="$current_stream_access/@status = 'admin'">
					<xsl:value-of select="php:function('trans', 'You are an administrator of this stream')"/>
				</xsl:when>
				<xsl:when test="$current_stream_access/@status = 'moderator'">
					<xsl:value-of select="php:function('trans', 'You are a moderator of this stream')"/>
				</xsl:when>
				<xsl:when test="$current_stream_access/@is_member = 1 or $current_stream_access/@is_pretender = 1"/>
				<xsl:otherwise>
					<xsl:value-of select="php:function('trans', 'You are a not a student of this stream')"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>.</xsl:text>
		</p>
		<xsl:choose>
			<xsl:when test="$current_stream_access/@status = 'guest'"/>
			<xsl:when test="$current_stream_access/@is_member = 1 or $current_stream_access/@is_pretender = 1">
				<form action="{$save_prefix}/stream_join/" method="post">
					<!--<input type="hidden" name="retpath" value="{$current_stream/@url}"/>-->
					<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
					<input type="hidden" name="join" value="0"/>
					<input class="ctrl ctrl__button ctrl__button__big" type="submit" value="{php:function('trans', 'Unjoin')}"/>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<form action="{$save_prefix}/stream_join/" method="post">
					<input type="hidden" name="stream_id" value="{$current_stream/@id}"/>
					<input type="hidden" name="join" value="1"/>
					<input class="ctrl ctrl__button ctrl__button__big ctrl__for_right" type="submit" value="{php:function('trans', 'Join')}"/>
					<xsl:if test="$current_stream/@join_premoderation = 1">
						<span class="nobr">
							<xsl:text>(</xsl:text>
							<xsl:value-of select="php:function('trans', 'Moderation will be requred')"/>
							<xsl:text>)</xsl:text>
						</span>
					</xsl:if>
				</form>
			</xsl:otherwise>
		</xsl:choose>
		<p>
			<br/>
		</p>
		<p>
			<xsl:value-of select="php:function('trans', 'What a student status gives')"/>
			<xsl:text>:</xsl:text>
		</p>
		<p>
			<span class="hl">1.</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'You are listed in the stream&amp;apos;s students section.')" disable-output-escaping="yes"/>
		</p>
		<p>
			<span class="hl">2.</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'Stream moderators can add you to the list of activity participants and grade you – even if you did not request participation via website by yourself.')"/>
		</p>
		<xsl:choose>
			<xsl:when test="$current_stream/@join_premoderation = 1">
				<p>
					<xsl:value-of select="php:function('trans', 'If you request participation in activity, request for student status is sent automatically.')"/>
				</p>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:value-of select="php:function('trans', 'If you request participation in activity, you receive a student status automatically.')"/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Status')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title"/>
		<xsl:text>)</xsl:text>
	</xsl:template>
</xsl:stylesheet>
