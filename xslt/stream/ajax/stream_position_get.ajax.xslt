<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:output indent="no" method="html" encoding="UTF-8"/>
	<xsl:include href="../../_site/base_layout.xslt"/>
	<xsl:include href="../../positions/include/position_details.inc.xslt"/>
	<xsl:include href="../../positions/include/position_credits.inc.xslt"/>
	<xsl:variable name="current_user" select="/root/stream_user_short[1]"/>
	<xsl:variable name="current_stream" select="/root/stream_short[1]"/>
	<xsl:variable name="current_position" select="/root/position_full[1]"/>
	<xsl:template match="math_position_details">
		<div class="js_lightbox_autoheight lightbox_autoheight" data-title="{php:function('trans', 'Position')}">
			<h1>
				<xsl:value-of select="$current_position/@title"/>
			</h1>
			<xsl:call-template name="draw_position_details_match"/>
			<xsl:call-template name="draw_position_details"/>
		</div>
	</xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>
