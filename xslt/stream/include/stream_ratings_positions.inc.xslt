<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_stream_ratings_positions">
		<xsl:param name="draw_focus" select="true()"/>
		<xsl:param name="base_url" select="$module_url"/>
		<xsl:call-template name="_draw_stream_ratings_positions_filter"/>
		<div class="js_other_positions">
			<xsl:call-template name="_draw_stream_ratings_positions_list">
				<xsl:with-param name="positions" select="position"/>
				<xsl:with-param name="position_class" select="'js_other_position'"/>
				<xsl:with-param name="draw_focus" select="$draw_focus"/>
				<xsl:with-param name="base_url" select="$base_url"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="_draw_stream_ratings_positions_filter">
		<xsl:variable name="study_levels_to_draw" select="/root/stream_study_level_restrictions/study_level"/>
		<xsl:if test="$study_levels_to_draw[1]">
			<div class="head_dd_align_left p">
				<span class="head_dd">
					<select class="js_study_level_select ctrl ctrl__select">
						<option value="">
							<xsl:value-of select="php:function('trans', 'All study levels')"/>
						</option>
						<xsl:for-each select="$study_levels_to_draw">
							<option value="{@id}">
								<xsl:value-of select="@title"/>
							</option>
						</xsl:for-each>
					</select>
				</span>
			</div>
		</xsl:if>
		<div class="js_other_positions_not_found p hide">
			<xsl:value-of select="php:function('trans', 'Positions were not found.')"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw_stream_ratings_positions_list">
		<xsl:param name="positions"/>
		<xsl:param name="position_class" select="''"/>
		<xsl:param name="draw_focus"/>
		<xsl:param name="base_url"/>
		<xsl:if test="not($positions)">
			<p>
				<xsl:value-of select="php:function('trans', 'Positions were not found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="$positions">
			<table class="table table__no_th">
				<xsl:for-each select="$positions">
					<xsl:variable name="levels_to_draw" select="/root/stream_study_level_restrictions/object[@id = current()/@id]/study_level"/>
					<tr class="{$position_class}" data-id="{@id}">
						<xsl:attribute name="data-study-levels">
							<xsl:text>,</xsl:text>
							<xsl:for-each select="$levels_to_draw">
								<xsl:value-of select="@id"/>
								<xsl:text>,</xsl:text>
							</xsl:for-each>
						</xsl:attribute>
						<td class="cell_text pr10">
							<span class="cell_expand_icon fa"/>
							<a href="{$base_url}{@id}/">
								<xsl:if test="$draw_focus and @checked = 1">
									<xsl:attribute name="class">for_icon_right</xsl:attribute>
								</xsl:if>
								<xsl:choose>
									<xsl:when test="$draw_focus and @checked = 1">
										<b>
											<xsl:value-of select="@title"/>
										</b>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@title"/>
									</xsl:otherwise>
								</xsl:choose>
							</a>
							<xsl:if test="$draw_focus and @checked = 1">
								<span class="smaller nobr hl">
									<i class="fa fa-crosshairs middle"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="php:function('trans', 'in your focus')"/>
								</span>
							</xsl:if>
							<xsl:if test="$levels_to_draw[1]">
								<div class="cell_text_note">
									<xsl:choose>
										<xsl:when test="$levels_to_draw">
											<xsl:for-each select="$levels_to_draw">
												<xsl:value-of select="@title"/>
												<xsl:if test="position() != last()">, </xsl:if>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="php:function('trans', 'All study levels')"/>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</xsl:if>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
