<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_activity">
		<div class="props p">
			<xsl:if test="@metaactivity_title != ''">
				<div class="prop">
					<xsl:value-of select="php:function('trans', 'Part of event group')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'LQ')"/>
					<xsl:value-of select="@metaactivity_title"/>
					<xsl:value-of select="php:function('trans', 'RQ')"/>
				</div>
			</xsl:if>
			<div class="prop">
				<xsl:call-template name="draw_activity_prop_date"/>
			</div>
			<div class="prop">
				<xsl:call-template name="draw_activity_prop_city"/>
			</div>
		</div>
		<div class="down12"/>
		<div class="submenu submenu__small">
			<span id="js_submenu_item_descr" class="js_submenu_item _item _item__current" data-block="descr">
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Description &amp; Roles')"/>
				</span>
			</span>
			<xsl:text> </xsl:text>
			<span id="js_submenu_item_users" class="js_submenu_item _item _item__clickable" data-block="users">
				<i class="fa fa-users"/>
				<xsl:text> </xsl:text>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Participants')"/>
				</span>
				<xsl:choose>
					<xsl:when test="@date_type = 'longterm'">
						<xsl:if test="@longterm_graded_user_count_calc &gt; 0">
							<sup>
								<xsl:value-of select="@longterm_graded_user_count_calc"/>
							</sup>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="@accepted_participant_count_calc &gt; 0">
							<sup>
								<xsl:value-of select="@accepted_participant_count_calc"/>
							</sup>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<xsl:if test="$current_stream_access/@has_moderator_rights = 1 and (moderators/div != '' or moderators/files/file)">
				<span id="js_submenu_item_moderators" class="js_submenu_item _item _item__clickable" data-block="moderators">
					<i class="fa fa-key"/>
					<xsl:text> </xsl:text>
					<span class="_text">
						<xsl:value-of select="php:function('trans', 'For moderators')"/>
					</span>
				</span>
			</xsl:if>
		</div>
		<div class="down10"/>
		<xsl:call-template name="draw_activity_block_descr"/>
		<xsl:call-template name="draw_activity_block_users"/>
		<xsl:call-template name="draw_activity_block_moderators"/>
	</xsl:template>
	<xsl:template name="draw_activity_block_descr">
		<div id="js_block_descr" class="js_block">
			<div class="maxw600">
				<div class="props p">
					<xsl:call-template name="draw_kv">
						<xsl:with-param name="key" select="php:function('trans', 'Format')"/>
						<xsl:with-param name="html">
							<span>
								<xsl:if test="$current_stream_access/@has_admin_rights = 1">
									<xsl:attribute name="class">for_icon_right_adjacent</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@format_title"/>
							</span>
							<xsl:if test="$current_stream_access/@has_admin_rights = 1">
								<a href="{$current_stream/@url}admin/formats/{@format_id}/">
									<i class="red fa fa-gear"/>
								</a>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="class" select="'prop'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_kv">
						<xsl:with-param name="key" select="php:function('trans', 'Weight of marks')"/>
						<xsl:with-param name="text">
							<xsl:choose>
								<xsl:when test="@weight = 'hi'">2.0</xsl:when>
								<xsl:when test="@weight = 'low'">1.0</xsl:when>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="class" select="'prop'"/>
					</xsl:call-template>
					<xsl:if test="subjects/item">
						<xsl:call-template name="draw_kv">
							<xsl:with-param name="key" select="php:function('trans', 'Subjects')"/>
							<xsl:with-param name="html">
								<xsl:for-each select="subjects/item">
									<xsl:value-of select="@title"/>
									<xsl:if test="position() != last()">, </xsl:if>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="class" select="'prop'"/>
						</xsl:call-template>
					</xsl:if>
					<xsl:call-template name="draw_kv">
						<xsl:with-param name="key" select="php:function('trans', 'Study level')"/>
						<xsl:with-param name="text" select="@study_level_title"/>
						<xsl:with-param name="class" select="'prop'"/>
					</xsl:call-template>
					<xsl:if test="groups/item">
						<xsl:call-template name="draw_kv">
							<xsl:with-param name="key" select="php:function('trans', 'For groups')"/>
							<xsl:with-param name="html">
								<xsl:for-each select="groups/item">
									<xsl:value-of select="@title"/>
									<xsl:if test="position() != last()">, </xsl:if>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="class" select="'prop'"/>
						</xsl:call-template>
					</xsl:if>
					<xsl:call-template name="draw_kv">
						<xsl:with-param name="key" select="php:function('trans', 'Place', 'WHERE')"/>
						<xsl:with-param name="text" select="@place"/>
						<xsl:with-param name="class" select="'prop'"/>
					</xsl:call-template>
				</div>
				<xsl:if test="descr/div != ''">
					<xsl:call-template name="draw_intmarkup_with_files"/>
				</xsl:if>
			</div>
			<div class="widgets">
				<xsl:if test="@competence_id or roles/role[@potential]">
					<div class="widget">
						<div class="widget_head">
							<i class="widget_head_icon fa fa-info-circle"/>
							<span class="widget_head_title">
								<xsl:value-of select="php:function('trans', 'Legend')"/>
							</span>
							<i class="widget_head_expand_icon fa"/>
						</div>
						<div class="widget_content widget_pad">
							<xsl:choose>
								<xsl:when test="@competence_id">
									<xsl:call-template name="draw_activity_competence_potential_legend"/>
								</xsl:when>
								<xsl:when test="/root/potential_math_user_streams/stream[2]">
									<xsl:call-template name="draw_activity_participant_potential_legend"/>
								</xsl:when>
							</xsl:choose>
						</div>
					</div>
				</xsl:if>
			</div>
			<div class="maxw600">
				<xsl:variable name="obj" select="/root/stream_study_level_restrictions/object"/>
				<xsl:variable name="rec" select="roles/role[not($obj/@id = @id) or $obj[@show = 1]/@id = @id]"/>
				<xsl:variable name="norec" select="roles/role[not($rec/@id = @id)]"/>
				<xsl:variable name="your_roles" select="roles/role[@status]"/>
				<xsl:variable name="main_roles" select="roles/role[$rec and $norec and not($your_roles/@id = @id) and (not($obj/@id = @id) or $obj[@show = 1]/@id = @id)]"/>
				<xsl:variable name="other_roles" select="roles/role[not($your_roles/@id = @id) and not($main_roles/@id = @id)]"/>
				<xsl:choose>
					<xsl:when test="@competence_id">
						<xsl:call-template name="draw_activity_competence_potential_notice"/>
					</xsl:when>
					<xsl:when test="roles/role[1] and /root/potential_math_user_streams/stream[2]">
						<xsl:call-template name="draw_activity_potential_stream_selector">
							<xsl:with-param name="is_stream_context" select="true()"/>
							<xsl:with-param name="field_class" select="''"/>
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="$your_roles[1]">
					<h2 class="for_table">
						<xsl:value-of select="php:function('trans', 'Your roles')"/>
					</h2>
					<xsl:call-template name="draw_activity_roles">
						<xsl:with-param name="roles" select="$your_roles"/>
					</xsl:call-template>
					<div>
						<br/>
					</div>
				</xsl:if>
				<xsl:if test="$main_roles[1]">
					<h2 class="for_table">
						<xsl:value-of select="php:function('trans', 'Roles recommended for your study level')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'LQ')"/>
						<xsl:value-of select="$current_stream/cities/@current_study_level_title"/>
						<xsl:value-of select="php:function('trans', 'RQ')"/>
					</h2>
					<xsl:if test="not($your_roles[1])">
						<div class="p invp">
							<xsl:value-of select="php:function('trans', 'Click on a role to read a description and apply.')"/>
						</div>
					</xsl:if>
					<xsl:call-template name="draw_activity_roles">
						<xsl:with-param name="roles" select="$main_roles"/>
					</xsl:call-template>
					<div>
						<br/>
					</div>
				</xsl:if>
				<xsl:if test="$other_roles[1]">
					<h2 class="for_table">
						<xsl:choose>
							<xsl:when test="$your_roles[1] or $main_roles[1]">
								<xsl:value-of select="php:function('trans', 'Other roles')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="php:function('trans', 'Roles')"/>
							</xsl:otherwise>
						</xsl:choose>
					</h2>
					<xsl:if test="not($your_roles[1]) and not($main_roles[1])">
						<div class="p invp">
							<xsl:value-of select="php:function('trans', 'Click on a role to read a description and apply.')"/>
						</div>
					</xsl:if>
					<xsl:call-template name="draw_activity_roles">
						<xsl:with-param name="roles" select="$other_roles"/>
					</xsl:call-template>
				</xsl:if>
				<div class="preload">
					<div class="js_status_accepted">
						<xsl:call-template name="draw_activity_status_controls">
							<xsl:with-param name="status" select="'accepted'"/>
						</xsl:call-template>
					</div>
					<div class="js_status_premoderation">
						<xsl:call-template name="draw_activity_status_controls">
							<xsl:with-param name="status" select="'premoderation'"/>
						</xsl:call-template>
					</div>
					<div class="js_status_declined">
						<xsl:call-template name="draw_activity_status_controls">
							<xsl:with-param name="status" select="'declined'"/>
						</xsl:call-template>
					</div>
					<div class="js_status_deleted">
						<xsl:call-template name="draw_activity_status_controls">
							<xsl:with-param name="status" select="'deleted'"/>
						</xsl:call-template>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_activity_roles">
		<xsl:param name="roles"/>
		<xsl:if test="not($roles)">
			<p>
				<xsl:value-of select="php:function('trans', 'Roles were not found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="$roles">
			<table class="table w100p">
				<tr>
					<th class="right">
						<xsl:value-of select="php:function('trans', 'Vacancies filled')"/>
					</th>
				</tr>
				<xsl:for-each select="$roles">
					<xsl:variable name="context" select="."/>
					<xsl:variable name="levels_to_draw" select="/root/stream_study_level_restrictions/object[@id = current()/@id]/study_level"/>
					<tr>
						<td class="cell_expand pr10">
							<div class="cell_float cell_num nobr pl10 pr6">
								<xsl:value-of select="@taken_number"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="php:function('trans', 'of')"/>
								<xsl:text> </xsl:text>
								<xsl:choose>
									<xsl:when test="@total_number != 0">
										<xsl:value-of select="@total_number"/>
									</xsl:when>
									<xsl:otherwise>∞</xsl:otherwise>
								</xsl:choose>
							</div>
							<span class="cell_expand_icon fa"/>
							<span class="cell_expand_text for_icon_right_adjacent">
								<xsl:value-of select="@title"/>
							</span>
							<xsl:if test="@potential != ''">
								<xsl:choose>
									<xsl:when test="../../@competence_id">
										<xsl:call-template name="draw_activity_competence_potential"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="draw_activity_participant_potential"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
							<xsl:if test="$levels_to_draw[1]">
								<div class="cell_text_note">
									<xsl:for-each select="$levels_to_draw">
										<xsl:value-of select="@title"/>
										<xsl:if test="position() != last()">, </xsl:if>
									</xsl:for-each>
								</div>
							</xsl:if>
							<xsl:if test="@status">
								<div class="props expanded_hide">
									<div class="prop">
										<xsl:call-template name="draw_activity_participant_status"/>
									</div>
								</div>
							</xsl:if>
						</td>
					</tr>
					<tr class="js_role hide" data-role_id="{@id}" data-activity_id="{$activity/@id}">
						<td class="cell_expandable pr6">
							<xsl:copy-of select="descr_html/div"/>
							<xsl:if test="/root/rate[@id = current()/@rate_id]/competence">
								<h3>
									<xsl:value-of select="php:function('trans', 'Role competences')"/>
								</h3>
								<xsl:for-each select="/root/rate[@id = current()/@rate_id]/competence">
									<p>
										<span class="for_icon_right_adjacent">
											<xsl:call-template name="draw_competence"/>
											<span class="note">
												<xsl:text>&#160;(</xsl:text>
												<xsl:value-of select="../options/option[@mark = current()/@mark]/@mark_title"/>
												<xsl:text>)</xsl:text>
											</span>
										</span>
										<xsl:if test="@potential_impact &gt; 0">
											<xsl:choose>
												<xsl:when test="$context/../../@competence_id">
													<xsl:call-template name="draw_activity_competence_potential">
														<xsl:with-param name="potential" select="@potential_impact"/>
														<xsl:with-param name="regular" select="true()"/>
														<xsl:with-param name="class" select="'potential__small'"/>
													</xsl:call-template>
												</xsl:when>
												<xsl:otherwise>
													<xsl:call-template name="draw_activity_participant_potential">
														<xsl:with-param name="potential" select="@potential_impact"/>
														<xsl:with-param name="regular" select="true()"/>
														<xsl:with-param name="class" select="'potential__small'"/>
													</xsl:call-template>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</p>
								</xsl:for-each>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="$user/@id">
									<xsl:call-template name="draw_activity_status_controls"/>
								</xsl:when>
								<xsl:otherwise>
									<p class="note">
										<xsl:value-of select="php:function('trans', 'Please login to apply for this role.')"/>
									</p>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_activity_status_controls">
		<xsl:param name="status" select="@status"/>
		<xsl:param name="comment" select="@comment"/>
		<div class="js_activity_status_controls activity_status_controls activity_status_controls__autoaccept_{@auto_accept}">
			<xsl:choose>
				<xsl:when test="$status = 'accepted'">
					<xsl:call-template name="draw_activity_participant_status">
						<xsl:with-param name="status" select="$status"/>
					</xsl:call-template>
					<div class="_paragraph _autoaccept_0">
						<xsl:value-of select="php:function('trans', 'Please write to a moderator to revoke your participation.')"/>
					</div>
					<span class="js_btn _cancel_link _autoaccept_1" data-btn-action="cancel">
						<xsl:value-of select="php:function('trans', 'Revoke')"/>
					</span>
				</xsl:when>
				<xsl:when test="$status = 'premoderation'">
					<xsl:call-template name="draw_activity_participant_status">
						<xsl:with-param name="status" select="$status"/>
					</xsl:call-template>
					<span class="js_btn _cancel_link" data-btn-action="cancel">
						<xsl:value-of select="php:function('trans', 'Revoke')"/>
					</span>
				</xsl:when>
				<xsl:when test="$status = 'declined'">
					<xsl:call-template name="draw_activity_participant_status">
						<xsl:with-param name="status" select="$status"/>
					</xsl:call-template>
					<xsl:if test="$comment != ''">
						<div class="_paragraph">
							<b>
								<xsl:value-of select="php:function('trans', 'Moderator&amp;apos;s comment')" disable-output-escaping="yes"/>
								<xsl:text>: </xsl:text>
							</b>
							<xsl:value-of select="$comment"/>
						</div>
					</xsl:if>
				</xsl:when>
				<xsl:when test="@taken_number != '' and @total_number != '' and @total_number != 0 and @taken_number &gt;= @total_number"/>
				<xsl:otherwise>
					<button type="submit" class="js_btn ctrl ctrl__button ctrl__button__for_right" data-btn-action="request">
						<xsl:value-of select="php:function('trans', 'Apply', 'ROLE')"/>
						<i class="_icon fa fa-chevron-right"/>
					</button>
					<span class="_moderation_hint _autoaccept_0">
						<xsl:value-of select="php:function('trans', '(premoderation will be required)')"/>
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="draw_activity_block_users">
		<div id="js_block_users" class="js_block hide">
			<div class="maxw600">
				<xsl:choose>
					<xsl:when test="@date_type = 'longterm'">
						<xsl:for-each select="/root/competence_title">
							<xsl:variable name="users" select="/root/activity_longterm_marks/user[@competence_id = current()/@id]"/>
							<xsl:if test="$users[1]">
								<h2>
									<xsl:call-template name="draw_competence"/>
								</h2>
								<table class="table p w100p">
									<xsl:choose>
										<xsl:when test="position() != 1">
											<xsl:attribute name="class">table table__no_th p w100p</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<tr>
												<xsl:call-template name="draw_th">
													<xsl:with-param name="name" select="''"/>
													<xsl:with-param name="title" select="php:function('trans', 'Name')"/>
													<xsl:with-param name="class" select="''"/>
												</xsl:call-template>
												<xsl:call-template name="draw_th">
													<xsl:with-param name="name" select="''"/>
													<xsl:with-param name="title" select="php:function('trans', 'Mark')"/>
													<xsl:with-param name="class" select="'pl10 pr6'"/>
												</xsl:call-template>
											</tr>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:call-template name="draw_activity_users">
										<xsl:with-param name="users" select="$users"/>
									</xsl:call-template>
								</table>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="roles/role">
							<xsl:variable name="role_position" select="position()"/>
							<h2>
								<xsl:value-of select="@title"/>
							</h2>
							<xsl:variable name="accepted_users" select="/root/activity_participants/user[@role_id = current()/@id and @status = 'accepted']"/>
							<xsl:variable name="premoderation_users" select="/root/activity_participants/user[@role_id = current()/@id and @status = 'premoderation']"/>
							<xsl:variable name="other_users" select="/root/activity_participants/user[@role_id = current()/@id and not(@status = 'accepted' or @status = 'premoderation')]"/>
							<xsl:if test="not($accepted_users or $premoderation_users or $other_users)">
								<p>
									<xsl:value-of select="php:function('trans', 'Nobody has applied yet.')"/>
								</p>
							</xsl:if>
							<xsl:if test="$accepted_users or $premoderation_users or $other_users">
								<table class="table p w100p">
									<xsl:choose>
										<xsl:when test="$role_position != 1">
											<xsl:attribute name="class">table table__no_th p w100p</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<tr>
												<xsl:call-template name="draw_th">
													<xsl:with-param name="name" select="''"/>
													<xsl:with-param name="title" select="php:function('trans', 'Name')"/>
													<xsl:with-param name="class" select="''"/>
												</xsl:call-template>
												<xsl:call-template name="draw_th">
													<xsl:with-param name="name" select="''"/>
													<xsl:with-param name="title" select="php:function('trans', 'Mark')"/>
													<xsl:with-param name="class" select="'pl10 pr6'"/>
												</xsl:call-template>
											</tr>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:call-template name="draw_activity_users">
										<xsl:with-param name="users" select="$accepted_users"/>
									</xsl:call-template>
									<xsl:call-template name="draw_activity_users">
										<xsl:with-param name="users" select="$premoderation_users"/>
										<xsl:with-param name="draw_status" select="true()"/>
									</xsl:call-template>
									<xsl:call-template name="draw_activity_users">
										<xsl:with-param name="users" select="$other_users"/>
									</xsl:call-template>
								</table>
							</xsl:if>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_activity_users">
		<xsl:param name="users"/>
		<xsl:param name="draw_status" select="false()"/>
		<xsl:for-each select="$users">
			<xsl:variable name="context" select="."/>
			<xsl:for-each select="/root/stream_user_short[@id = current()/@id]">
				<tr>
					<td class="cell_user">
						<div class="_img">
							<a href="{$users_prefix}/{@login}/">
								<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
							</a>
						</div>
						<div class="_title">
							<a href="{$users_prefix}/{@login}/">
								<xsl:value-of select="@visible_name"/>
							</a>
							<span class="_login">
								<xsl:text> (</xsl:text>
								<xsl:value-of select="@login"/>
								<xsl:text>)</xsl:text>
							</span>
						</div>
						<div class="_city">
							<xsl:value-of select="@city_title"/>
							<xsl:if test="@group_title != ''">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="@group_title"/>
							</xsl:if>
						</div>
						<div class="_lines">
							<xsl:for-each select="/root/stream_user_short[@id = $context/@changer_user_id][1]">
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key">
										<xsl:choose>
											<xsl:when test="@sex = 'f'">
												<xsl:value-of select="php:function('trans', 'Rated by', 'FEMALE')"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="php:function('trans', 'Rated by', 'MALE')"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="html">
										<!--<a href="{@url}" class="inline_photo">
											<img alt="" src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}"/>
										</a>
										<xsl:text> </xsl:text>-->
										<a href="{@url}">
											<xsl:value-of select="@visible_name"/>
										</a>
									</xsl:with-param>
									<xsl:with-param name="class" select="'_line'"/>
								</xsl:call-template>
							</xsl:for-each>
							<xsl:call-template name="draw_kv">
								<xsl:with-param name="key" select="php:function('trans', 'Comment to the mark')"/>
								<xsl:with-param name="text" select="$context/@comment"/>
								<xsl:with-param name="class" select="'_line'"/>
							</xsl:call-template>
							<xsl:if test="$context/@mark = 'ACCESS_DENIED'">
								<div class="_line">
									<xsl:value-of select="php:function('trans', 'The user restricted access to this mark.')"/>
								</div>
							</xsl:if>
							<xsl:if test="$draw_status">
								<div class="_line">
									<xsl:call-template name="draw_activity_participant_status">
										<xsl:with-param name="status" select="$context/@status"/>
									</xsl:call-template>
								</div>
							</xsl:if>
						</div>
					</td>
					<td class="cell_mark w74 pl10 pr6">
						<xsl:for-each select="$context[@mark != ''][1]">
							<div class="rel">
								<span class="abs">
									<xsl:call-template name="draw_mark">
										<xsl:with-param name="weight" select="$activity/@weight"/>
									</xsl:call-template>
								</span>
							</div>
						</xsl:for-each>
					</td>
				</tr>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="draw_activity_block_moderators">
		<xsl:if test="$current_stream_access/@has_moderator_rights = 1 and (moderators/div != '' or moderators/files/file)">
			<div id="js_block_moderators" class="js_block hide">
				<div class="maxw600">
					<xsl:call-template name="draw_intmarkup_with_files">
						<xsl:with-param name="field" select="moderators"/>
					</xsl:call-template>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_activity_participant_potential_legend">
		<p>
			<span class="potential">
				<i class="_icon fa fa-bolt"/>
				<span class="_count">
					<xsl:choose>
						<xsl:when test="roles/role[@potential &gt; 0]/@potential &gt; 0">
							<xsl:value-of select="roles/role[@potential &gt; 0]/@potential"/>
						</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'potential of the role (in accordance with your focus positions).')"/>
		</p>
		<p>
			<span class="potential potential__small">
				<i class="_icon fa fa-bolt"/>
				<span class="_count">
					<xsl:choose>
						<xsl:when test="/root/rate[@id = current()/roles/role[@potential &gt; 0]/@rate_id]/competence[1]/@potential_impact &gt; 0">
							<xsl:value-of select="/root/rate[@id = current()/roles/role[@potential &gt; 0]/@rate_id]/competence[1]/@potential_impact"/>
						</xsl:when>
						<xsl:otherwise>1.0</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', '(after a competence)')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'contribution of the competence to the role potential.')"/>
		</p>
	</xsl:template>
	<xsl:template name="draw_activity_competence_potential_legend">
		<p>
			<span class="potential potential__competence">
				<i class="_icon fa fa-bolt"/>
				<span class="_count">
					<xsl:choose>
						<xsl:when test="roles/role[@potential &gt; 0]/@potential &gt; 0">
							<xsl:value-of select="roles/role[@potential &gt; 0]/@potential"/>
						</xsl:when>
						<xsl:otherwise>1</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'potential of the role.')"/>
		</p>
		<p>
			<span class="potential potential__competence potential__small">
				<i class="_icon fa fa-bolt"/>
				<span class="_count">
					<xsl:choose>
						<xsl:when test="/root/rate[@id = current()/roles/role[@potential &gt; 0]/@rate_id]/competence[1]/@potential_impact &gt; 0">
							<xsl:value-of select="/root/rate[@id = current()/roles/role[@potential &gt; 0]/@rate_id]/competence[1]/@potential_impact"/>
						</xsl:when>
						<xsl:otherwise>1.0</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', '(after a competence)')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'contribution of the competence to the role potential.')"/>
		</p>
	</xsl:template>
</xsl:stylesheet>
