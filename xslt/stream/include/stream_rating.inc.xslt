<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_stream_rating">
		<xsl:param name="base_url" select="''"/>
		<xsl:param name="url_suffix" select="''"/>
		<xsl:if test="$current_position/descr/div != ''">
			<h3>
				<xsl:value-of select="php:function('trans', 'Position description')"/>
			</h3>
			<xsl:copy-of select="$current_position/descr/div"/>
		</xsl:if>
		<h3>
			<xsl:value-of select="php:function('trans', 'Rating')"/>
		</h3>
		<xsl:choose>
			<xsl:when test="@cache_miss = 1">
				<div class="loading">
					<i class="_icon fa fa-spinner fa-pulse"/>
					<div class="_text">
						<xsl:value-of select="php:function('trans', 'Calculating ratings...')"/>
					</div>
				</div>
			</xsl:when>
			<xsl:when test="not(user) and @position_no_competences = 1">
				<p>
					<xsl:value-of select="php:function('trans', 'No competences were found for this position. It seems, stream administratiors have not filled in necessary data yet.')"/>
				</p>
			</xsl:when>
			<xsl:when test="not(user) and @position_no_competences = 0">
				<p>
					<xsl:value-of select="php:function('trans', 'Nobody is here.')"/>
				</p>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="@cache_time != ''">
					<p>
						<xsl:value-of select="php:function('trans', 'This rating was calculated and cached')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="@cache_time"/>
						<xsl:text> </xsl:text>
						<xsl:call-template name="count_case_lang">
							<xsl:with-param name="number" select="@cache_time"/>
							<xsl:with-param name="word" select="concat(@cache_unit, ' ago')"/>
						</xsl:call-template>
						<xsl:text>.</xsl:text>
					</p>
				</xsl:if>
				<table class="table w100p">
					<tr>
						<xsl:call-template name="draw_th">
							<xsl:with-param name="name" select="''"/>
							<xsl:with-param name="title" select="php:function('trans', 'Name')"/>
							<xsl:with-param name="class" select="''"/>
						</xsl:call-template>
						<xsl:call-template name="draw_th">
							<xsl:with-param name="name" select="''"/>
							<xsl:with-param name="title" select="php:function('trans', 'Match')"/>
							<xsl:with-param name="class" select="'pl10 right'"/>
						</xsl:call-template>
					</tr>
				</table>
				<table class="table p w100p">
					<xsl:for-each select="user">
						<xsl:variable name="context" select="."/>
						<xsl:for-each select="/root/stream_user_short[@id = current()/@id]">
							<tr>
								<td class="cell_user">
									<div class="_img">
										<a href="{$base_url}{@login}/{$url_suffix}">
											<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
										</a>
									</div>
									<div class="_title">
										<a href="{$base_url}{@login}/{$url_suffix}">
											<xsl:value-of select="@visible_name"/>
										</a>
										<span class="_login">
											<xsl:text> (</xsl:text>
											<xsl:value-of select="@login"/>
											<xsl:text>)</xsl:text>
										</span>
									</div>
									<div class="_city">
										<xsl:value-of select="@city_title"/>
										<xsl:if test="@group_title != ''">
											<xsl:text>, </xsl:text>
											<xsl:value-of select="@group_title"/>
										</xsl:if>
									</div>
									<xsl:for-each select="$context[1]">
										<xsl:call-template name="draw_position_credit">
											<xsl:with-param name="position_credit_min_match" select="$current_stream/@position_credit_min_match"/>
											<xsl:with-param name="class" select="'_lines'"/>
											<xsl:with-param name="line_class" select="'_line'"/>
										</xsl:call-template>
									</xsl:for-each>
								</td>
								<td class="cell_num p10 center">
									<xsl:value-of select="$context/@match"/>
									<xsl:text>%</xsl:text>
								</td>
							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
</xsl:stylesheet>
