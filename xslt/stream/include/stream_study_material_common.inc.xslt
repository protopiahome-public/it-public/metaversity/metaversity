<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_study_material">
		<div class="widgets">
			<xsl:if test="@potential != ''">
				<div class="widget">
					<div class="widget_head">
						<i class="widget_head_icon fa fa-info-circle"/>
						<span class="widget_head_title">
							<xsl:value-of select="php:function('trans', 'Legend')"/>
						</span>
						<i class="widget_head_expand_icon fa"/>
					</div>
					<div class="widget_content widget_pad">
						<xsl:call-template name="draw_study_material_potential_legend_top"/>
					</div>
				</div>
			</xsl:if>
		</div>
		<xsl:call-template name="draw_study_material_content"/>
		<xsl:if test="$user/@id">
			<xsl:call-template name="draw_study_material_user_status"/>
		</xsl:if>
		<div class="widgets">
			<xsl:if test="@potential != '' and /root/rate[@id = current()/@rate_id]/competence">
				<div class="widget">
					<div class="widget_head">
						<i class="widget_head_icon fa fa-info-circle"/>
						<span class="widget_head_title">
							<xsl:value-of select="php:function('trans', 'Legend')"/>
						</span>
						<i class="widget_head_expand_icon fa"/>
					</div>
					<div class="widget_content widget_pad">
						<xsl:call-template name="draw_study_material_potential_legend_impact"/>
					</div>
				</div>
			</xsl:if>
		</div>
		<xsl:call-template name="draw_study_material_competences"/>
		<xsl:call-template name="draw_study_material_users"/>
	</xsl:template>
	<xsl:template name="draw_study_material_content">
		<div class="maxw600">
			<xsl:for-each select="$study_material[1]">
				<xsl:if test="@is_outer = 1">
					<a class="underline_inner_span" href="{@outer_url}">
						<span>
							<xsl:value-of select="php:function('trans', 'Proceed to the material')"/>
						</span>
						<xsl:text> </xsl:text>
						<i class="fa fa-external-link"/>
					</a>
				</xsl:if>
				<xsl:if test="html/div != ''">
					<xsl:copy-of select="html/div"/>
				</xsl:if>
				<xsl:call-template name="draw_intmarkup_with_files"/>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template name="draw_study_material_user_status">
		<div class="maxw600">
			<xsl:for-each select="$study_material[1]">
				<div class="center" style="padding: 20px 0;">
					<div class="js_change_status" data-study_material_id="{@id}">
						<xsl:call-template name="draw_study_material_status_controls"/>
					</div>
				</div>
			</xsl:for-each>
			<div class="preload">
				<div class="js_status_read">
					<xsl:call-template name="draw_study_material_status_controls">
						<xsl:with-param name="status" select="'read'"/>
					</xsl:call-template>
				</div>
				<div class="js_status_skip">
					<xsl:call-template name="draw_study_material_status_controls">
						<xsl:with-param name="status" select="'skip'"/>
					</xsl:call-template>
				</div>
				<div class="js_status_default">
					<xsl:call-template name="draw_study_material_status_controls">
						<xsl:with-param name="status" select="'default'"/>
					</xsl:call-template>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="draw_study_material_competences">
		<div class="maxw600">
			<xsl:if test="/root/rate[@id = current()/@rate_id]/competence">
				<h2>
					<xsl:value-of select="php:function('trans', 'Material competences')"/>
				</h2>
				<xsl:for-each select="/root/rate[@id = current()/@rate_id]/competence">
					<p>
						<span class="for_icon_right_adjacent">
							<xsl:call-template name="draw_competence"/>
							<span class="note">
								<xsl:text>&#160;(</xsl:text>
								<xsl:value-of select="../options/option[@mark = current()/@mark]/@mark_title"/>
								<xsl:text>)</xsl:text>
							</span>
						</span>
						<xsl:if test="@potential_impact &gt; 0">
							<xsl:call-template name="draw_study_material_potential">
								<xsl:with-param name="potential" select="@potential_impact"/>
								<xsl:with-param name="regular" select="true()"/>
								<xsl:with-param name="class" select="'potential__small'"/>
							</xsl:call-template>
						</xsl:if>
					</p>
				</xsl:for-each>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="draw_study_material_users">
		<div class="maxw600">
			<h2>
				<xsl:value-of select="php:function('trans', 'Students who studied this material')"/>
			</h2>
			<xsl:variable name="users" select="read_users/user"/>
			<xsl:if test="not($users)">
				<p>
					<xsl:value-of select="php:function('trans', 'Nobody is here.')"/>
				</p>
			</xsl:if>
			<table class="table table__no_th p w100p">
				<xsl:for-each select="read_users/user">
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="/root/stream_user_short[@id = current()/@id]">
						<tr>
							<td class="cell_user">
								<div class="_img">
									<a href="{$users_prefix}/{@login}/">
										<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
									</a>
								</div>
								<div class="_title">
									<a href="{$users_prefix}/{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
									<span class="_login">
										<xsl:text> (</xsl:text>
										<xsl:value-of select="@login"/>
										<xsl:text>)</xsl:text>
									</span>
									<div class="_city">
										<xsl:value-of select="@city_title"/>
										<xsl:if test="@group_title != ''">
											<xsl:text>, </xsl:text>
											<xsl:value-of select="@group_title"/>
										</xsl:if>
									</div>
								</div>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="draw_study_material_status_controls">
		<xsl:param name="status" select="@status"/>
		<div class="js_status_controls status_controls study_material_status_controls">
			<xsl:choose>
				<xsl:when test="$status = 'read'">
					<xsl:call-template name="draw_study_material_status">
						<xsl:with-param name="status" select="$status"/>
					</xsl:call-template>
					<span class="js_btn _cancel_link" data-btn-status="default">
						<xsl:value-of select="php:function('trans', 'Cancel', 'VERB')"/>
					</span>
				</xsl:when>
				<xsl:when test="$status = 'skip'">
					<xsl:call-template name="draw_study_material_status">
						<xsl:with-param name="status" select="$status"/>
					</xsl:call-template>
					<span class="js_btn _cancel_link" data-btn-status="default">
						<xsl:value-of select="php:function('trans', 'Cancel', 'VERB')"/>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<button type="submit" class="js_btn ctrl ctrl__button" data-btn-status="read">
						<xsl:value-of select="php:function('trans', 'Mark as studied')"/>
						<i class="_icon fa fa-chevron-right"/>
					</button>
					<div class="_paragraph">
						<span class="js_btn _cancel_link" data-btn-status="skip">
							<xsl:value-of select="php:function('trans', 'Did not study but wish to hide it from the list')"/>
						</span>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="draw_study_material_status">
		<xsl:param name="status" select="@status"/>
		<xsl:param name="mode" select="'full'"/>
		<xsl:param name="no_default" select="false()"/>
		<xsl:param name="dot" select="false()"/>
		<xsl:if test="$status">
			<xsl:variable name="class" select="concat('status_', $status)"/>
			<span class="status {$class}">
				<xsl:choose>
					<xsl:when test="$status = 'read'">
						<i class="fa fa-check"/>
					</xsl:when>
					<xsl:when test="$status = 'skip'">
						<i class="fa fa-times"/>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="$mode != 'icon'">
					<xsl:text>&#160;</xsl:text>
					<xsl:choose>
						<xsl:when test="$status = 'read'">
							<xsl:value-of select="php:function('trans', 'Studied', 'MATERIAL_STATUS')"/>
						</xsl:when>
						<xsl:when test="$status = 'skip'">
							<xsl:value-of select="php:function('trans', 'Hidden', 'MATERIAL_STATUS')"/>
						</xsl:when>
						<xsl:when test="not($no_default)">
							<xsl:value-of select="php:function('trans', 'Not studied', 'MATERIAL_STATUS')"/>
						</xsl:when>
					</xsl:choose>
					<xsl:if test="$dot">.</xsl:if>
				</xsl:if>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="draw_study_material_potential">
		<xsl:param name="potential" select="@potential"/>
		<xsl:param name="class" select="''"/>
		<xsl:param name="regular" select="false()"/>
		<xsl:variable name="potential_cropped">
			<xsl:choose>
				<xsl:when test="$potential = '?' or $regular">10</xsl:when>
				<xsl:when test="$potential &gt; 10">10</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$potential"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="delta" select="(10 - $potential_cropped) * 15"/>
		<span class="potential {$class}" style="color: rgb(255, {127 + round($delta div 2)}, {$delta})">
			<i class="_icon fa fa-bolt"/>
			<span class="_count">
				<xsl:choose>
					<xsl:when test="$potential_cropped >= 6 and not($regular)">
						<b>
							<xsl:value-of select="$potential"/>
						</b>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$potential"/>
					</xsl:otherwise>
				</xsl:choose>
			</span>
		</span>
	</xsl:template>
	<xsl:template name="draw_study_material_potential_legend_top">
		<p>
			<xsl:call-template name="draw_study_material_potential"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'potential of the material (in accordance with your focus positions).')"/>
		</p>
	</xsl:template>
	<xsl:template name="draw_study_material_potential_legend_impact">
		<p>
			<span class="potential potential__small">
				<i class="_icon fa fa-bolt"/>
				<span class="_count">
					<xsl:choose>
						<xsl:when test="/root/rate[@id = current()/@rate_id]/competence[@potential_impact &gt; 0]">
							<xsl:value-of select="/root/rate[@id = current()/@rate_id]/competence[@potential_impact &gt; 0]/@potential_impact"/>
						</xsl:when>
						<xsl:otherwise>1.0</xsl:otherwise>
					</xsl:choose>
				</span>
			</span>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', '(after a competence)')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'DASH')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'contribution of the competence to the material potential.')"/>
		</p>
	</xsl:template>
</xsl:stylesheet>
