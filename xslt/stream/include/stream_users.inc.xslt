<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_stream_users">
		<xsl:param name="small" select="false()"/>
		<xsl:param name="draw_count" select="true()"/>
		<xsl:param name="base_url" select="concat($users_prefix, '/')"/>
		<xsl:if test="not(user)">
			<p>
				<xsl:value-of select="php:function('trans', 'Nobody matches the selected search criteria.')"/>
			</p>
		</xsl:if>
		<xsl:if test="user">
			<xsl:if test="$draw_count">
				<p>
					<xsl:value-of select="php:function('trans', 'Users shown:')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="pages/@from_row"/>
					<xsl:text>&#8211;</xsl:text>
					<xsl:value-of select="pages/@to_row"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="php:function('trans', 'of')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="pages/@row_count"/>
					<xsl:text>.</xsl:text>
				</p>
			</xsl:if>
			<xsl:if test="filters/filter[@name = 'stream']/@is_active = 1">
				<div class="box p">
					<p>
						<xsl:value-of select="php:function('trans', 'The number of marks is shown for the selected stream.')"/>
					</p>
				</div>
			</xsl:if>
			<table class="table maxw600">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'name'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Name')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'mark-count'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Marks', 'COUNT')"/>
						<xsl:with-param name="class" select="'p10 center'"/>
					</xsl:call-template>
				</tr>
				<xsl:variable name="cell_class">
					<xsl:choose>
						<xsl:when test="$small">cell_user</xsl:when>
						<xsl:otherwise>cell_user_big</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:for-each select="user">
					<xsl:variable name="context" select="."/>
					<xsl:for-each select="/root/stream_user_short[@id = current()/@id]">
						<tr>
							<td class="{$cell_class}">
								<div class="_img">
									<a href="{$base_url}{@login}/">
										<xsl:choose>
											<xsl:when test="$small">
												<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.4)}" height="{round(photo_big/@height * 0.4)}" alt="{@login}"/>
											</xsl:when>
											<xsl:otherwise>
												<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.6)}" height="{round(photo_big/@height * 0.6)}" alt="{@login}"/>
											</xsl:otherwise>
										</xsl:choose>
									</a>
								</div>
								<div class="_title">
									<a href="{$base_url}{@login}/">
										<xsl:value-of select="@visible_name"/>
									</a>
									<span class="_login">
										<xsl:text> (</xsl:text>
										<xsl:value-of select="@login"/>
										<xsl:text>)</xsl:text>
									</span>
								</div>
								<div class="_city">
									<xsl:value-of select="@city_title"/>
									<xsl:if test="@group_title != ''">
										<xsl:text>, </xsl:text>
										<xsl:value-of select="@group_title"/>
									</xsl:if>
								</div>
							</td>
							<td class="cell_num p10 p_sort center">
								<xsl:value-of select="$context/@mark_count_calc"/>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:for-each>
			</table>
			<xsl:apply-templates select="pages"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
