<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template name="draw_stream_study_materials">
		<div class="widgets">
			<xsl:apply-templates select="filters" mode="filters"/>
		</div>
		<xsl:if test="$user/@id">
			<div class="submenu p">
				<span class="_item">
					<xsl:choose>
						<xsl:when test="@filter = 'recommended'">
							<xsl:attribute name="class">_item _item__current</xsl:attribute>
							<xsl:value-of select="php:function('trans', 'Recommended', 'MATERIALS')"/>
						</xsl:when>
						<xsl:otherwise>
							<a href="{$base_url}{$request/@query_string}">
								<xsl:value-of select="php:function('trans', 'Recommended', 'MATERIALS')"/>
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
				<xsl:text> </xsl:text>
				<span class="_item">
					<xsl:choose>
						<xsl:when test="@filter = 'studied'">
							<xsl:attribute name="class">_item _item__current</xsl:attribute>
							<xsl:value-of select="php:function('trans', 'Studied', 'MATERIALS')"/>
						</xsl:when>
						<xsl:otherwise>
							<a href="{$base_url}studied/{$request/@query_string}">
								<xsl:value-of select="php:function('trans', 'Studied', 'MATERIALS')"/>
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
				<xsl:text> </xsl:text>
				<span class="_item">
					<xsl:choose>
						<xsl:when test="@filter = 'all'">
							<xsl:attribute name="class">_item _item__current</xsl:attribute>
							<xsl:value-of select="php:function('trans', 'All', 'MATERIALS')"/>
						</xsl:when>
						<xsl:otherwise>
							<a href="{$base_url}all/{$request/@query_string}">
								<xsl:value-of select="php:function('trans', 'All', 'MATERIALS')"/>
							</a>
						</xsl:otherwise>
					</xsl:choose>
				</span>
			</div>
		</xsl:if>
		<xsl:apply-templates select="filters" mode="filters_show"/>
		<xsl:choose>
			<xsl:when test="not($user/@id)">
				<div class="box p">
					<p>
						<xsl:value-of select="php:function('trans', 'Please', 'AUTH_FOR_RECOMMENDATIONS')"/>
						<xsl:text> </xsl:text>
						<a href="{$login_url}">
							<xsl:value-of select="php:function('trans', 'login', 'AUTH_FOR_RECOMMENDATIONS')"/>
						</a>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'to see personal recommendations', 'AUTH_FOR_RECOMMENDATIONS')"/>
						<xsl:text>.</xsl:text>
					</p>
				</div>
			</xsl:when>
			<xsl:when test="@positions_checked = 0">
				<div class="box p">
					<p>
						<xsl:value-of select="php:function('trans', 'Please add some positions to your', 'FOCUS_FOR_RECOMMENDATIONS')"/>
						<xsl:text> </xsl:text>
						<a href="{$current_stream/@url}positions/">
							<xsl:value-of select="php:function('trans', 'focus', 'FOCUS_FOR_RECOMMENDATIONS')"/>
						</a>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'to see personal recommendations', 'FOCUS_FOR_RECOMMENDATIONS')"/>
						<xsl:text>.</xsl:text>
					</p>
				</div>
			</xsl:when>
		</xsl:choose>
		<div class="maxw800">
			<xsl:if test="not(study_material)">
				<p>
					<xsl:value-of select="php:function('trans', 'Nothing was found.')"/>
				</p>
			</xsl:if>
			<xsl:for-each select="study_material">
				<div class="list_item">
					<div class="list_item_title list_item_title__smaller">
						<span class="for_icon_right">
							<a href="{$current_stream/@url}materials/{@id}/">
								<xsl:value-of select="@title"/>
							</a>
							<xsl:if test="@is_outer = 1">
								<xsl:text> </xsl:text>
								<xsl:text>(</xsl:text>
								<a href="{@outer_url}">
									<i>
										<xsl:value-of select="php:function('trans', 'external')"/>
									</i>
								</a>
								<xsl:text>)</xsl:text>
							</xsl:if>
						</span>
						<xsl:call-template name="draw_study_material_potential">
							<xsl:with-param name="potential">
								<xsl:choose>
									<xsl:when test="not(@potential) or ../@positions_checked = 0">?</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@potential"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="class">
								<xsl:text>list_item_title_regular_text</xsl:text>
								<xsl:if test="/root/stream_access/@has_moderator_rights = 1"> for_icon_right</xsl:if>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:if test="/root/stream_access/@has_moderator_rights = 1">
							<a href="{$current_stream/@url}admin/materials/{@id}/">
								<i class="red fa fa-gear"/>
							</a>
						</xsl:if>
					</div>
					<div class="props">
						<xsl:if test="@read_user_count_calc > 0">
							<div class="prop">
								<span class="prop_for_right">
									<span class="fa fa-users"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="@read_user_count_calc"/>
									<xsl:text> </xsl:text>
									<xsl:call-template name="count_case_lang">
										<xsl:with-param name="number" select="@read_user_count_calc"/>
										<xsl:with-param name="word" select="'student#MATERIALS'"/>
									</xsl:call-template>
								</span>
							</div>
						</xsl:if>
						<xsl:if test="@status != '' and @status != 'default'">
							<div class="prop">
								<xsl:call-template name="draw_study_material_status">
									<xsl:with-param name="mode" select="'shorter'"/>
									<xsl:with-param name="no_default" select="true()"/>
								</xsl:call-template>
							</div>
						</xsl:if>
					</div>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>
