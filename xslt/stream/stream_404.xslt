<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'404'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:template match="error_404">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="$current_stream/@title_short"/>
			<xsl:with-param name="url" select="$current_stream/@url"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<p style="font-size: 40px;">
			<xsl:value-of select="php:function('trans', 'Error 404')"/>
			<xsl:text> :-(</xsl:text>
		</p>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			var referrer = '<xsl:value-of select="$request/@referrer"/>';
			_gaq.push(['_trackEvent', 'Error 404', location.href, referrer ? referrer : ':direct:', 0, true]);
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Error 404')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="$current_stream/@title_short"/>
		<xsl:text> (</xsl:text>
		<xsl:value-of select="$current_stream/@title"/>
		<xsl:text>)</xsl:text>
	</xsl:template>
</xsl:stylesheet>
