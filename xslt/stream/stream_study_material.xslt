<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/stream.menu.xslt"/>
	<xsl:include href="include/stream_study_material_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'stream'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="stream_section" select="'study_materials'"/>
	<xsl:variable name="stream_section_main_page" select="false()"/>
	<xsl:variable name="study_material" select="/root/study_material_full[1]"/>
	<xsl:variable name="study_material_list_url" select="concat($current_stream/@url, 'materials/')"/>
	<xsl:variable name="module_url" select="concat($current_stream/@url, 'materials/', $study_material/@id, '/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="stream_study_material">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Materials')"/>
			<xsl:with-param name="url" select="concat($current_stream/@url, 'materials/')"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1>
				<span>
					<xsl:attribute name="class">for_icon_right</xsl:attribute>
					<xsl:value-of select="$study_material/@title"/>
				</span>
				<xsl:call-template name="draw_study_material_potential">
					<xsl:with-param name="potential">
						<xsl:choose>
							<xsl:when test="not(@potential) or ../@positions_checked = 0">?</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="@potential"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="class">
						<xsl:text>list_item_title_regular_text</xsl:text>
						<xsl:if test="/root/stream_access/@has_moderator_rights = 1"> for_icon_right</xsl:if>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:if test="$current_stream_access/@has_moderator_rights = 1">
					<a href="{$current_stream/@url}admin/materials/{@study_material_id}/">
						<i class="red fa fa-gear"/>
					</a>
				</xsl:if>
			</h1>
			<xsl:call-template name="draw_study_material"/>
		</div>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript">
			window.post_vars = {
				stream_id: <xsl:value-of select="$current_stream/@id"/>
			}
		</script>
		<script type="text/javascript" src="{$main_prefix}/js/stream_study_material.js?v3"/>
	</xsl:template>
</xsl:stylesheet>
