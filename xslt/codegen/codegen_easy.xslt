<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="include/codegen.inc.xslt"/>
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'codegen'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:template match="codegen_easy">
		<h2>Генерация easy-контролов</h2>
		<xsl:call-template name="codegen_pass_info"/>
		<form method="post" action="{$save_prefix}/codegen_easy/">
			<div>
				Название контрола:
				<input name="ctrl_name"/>
			</div>
			<div>
				Название модуля:
				<input name="module_name"/>
			</div>
			<div>
				<input type="checkbox" value="1" name="with_xslt" checked="checked"/> с xslt
			</div>
			<div>
				<input type="checkbox" value="1" name="with_save_page"/> с save_page
			</div>
			<div>
				<input type="submit" value="Сгенерировать"/>
			</div>
		</form>
	</xsl:template>
</xsl:stylesheet>
