<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="codegen_pass_info">
		<xsl:for-each select="$pass_info/info[@name = 'FILE_CREATED']">
			<div>Создан файл <xsl:value-of select="."/></div>
		</xsl:for-each>
		<xsl:for-each select="$pass_info/error[@name = 'WRONG_INPUT']">
			<div>Ошибка входных данных!</div>
		</xsl:for-each>
		<xsl:for-each select="$pass_info/error[@name = 'FILE_ALREADY_EXISTS']">
			<div>Файл <xsl:value-of select="."/> уже существует!</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
