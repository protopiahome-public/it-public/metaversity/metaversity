<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="include/codegen.inc.xslt"/>
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'codegen'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:template match="codegen">
		<h1>Кодогенерация</h1>
		<ul>
			<li>
				<a href="{$main_prefix}/codegen/blank/">
					Простые контролы
				</a>
			</li>
			<li>
				<a href="{$main_prefix}/codegen/easy/">
					easy-контролы
				</a>
			</li>
			<li>
				<a href="{$main_prefix}/codegen/dt/">
					dt-контролы
				</a>
			</li>
			<li>
				<a href="{$main_prefix}/codegen/dt-elements/">
					Конструктор dt
				</a>
			</li>
			<li>
				<a href="{$main_prefix}/codegen/dt-show/">
					dt_show
				</a>
			</li>
		</ul>
	</xsl:template>
</xsl:stylesheet>
