<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="include/codegen.inc.xslt"/>
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'codegen'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:template match="codegen_dt_elements">
		<h2>Генерация dt-контролов</h2>
		<xsl:call-template name="codegen_pass_info"/>
		<form method="post" action="{$save_prefix}/codegen_dt_elements/">
			<p>
				Название dt:
				<input name="dt_name"/>
			</p>
			<p>
				Название модуля:
				<input name="module_name"/>
			</p>
			<xsl:for-each select="dtf">
				<textarea id="jq-dtf-code-{@dtf}" class="dn">
					<xsl:value-of select="."/>
				</textarea>
			</xsl:for-each>
			<textarea id="jq-axis-code" class="dn">
				<xsl:value-of select="axis"/>
			</textarea>
			<p>
				<select id="jq-dtf">
					<xsl:for-each select="dtf">
						<option name="@dtf">
							<xsl:value-of select="@dtf"/>
						</option>
					</xsl:for-each>
				</select>
			
				dtf name:
				<input id="jq-dtf-name"/>
			
				dtf title:
				<input id="jq-dtf-title"/>
			
				<input type="button" id="jq-dt-elements-generate" value="Сгенерировать dtf"/>
			</p>
			<p>
				axis name:
				<input id="jq-axis-name"/>
				
				axis fields:
				<textarea id="jq-axis-fields" class="dt-elements-axis-fields"/>

				<input type="button" id="jq-dt-elements-generate-axis" value="Сгенерировать axis"/>
			</p>
			<p>
				<textarea name="elements" id="jq-dt-elements-preview" class="dt-elements-preview"/>
			</p>
			<p>
				<input type="submit" value="Сгенерировать"/>
			</p>
		</form>
	</xsl:template>
	<xsl:template match="/root" mode="head">
		<script src="{$main_prefix}/js/codegen_dt_elements.js?v1"/>
	</xsl:template>
</xsl:stylesheet>
