<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="include/codegen.inc.xslt"/>
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'codegen'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:template match="codegen_dt_show">
		<h2>Генерация dt_show</h2>
		<xsl:call-template name="codegen_pass_info"/>
		<form method="post" action="{$save_prefix}/codegen_dt_show/">
			<div>
				Название dt:
				<input name="dt_name"/>
			</div>
			<div>
				Название модуля:
				<input name="module_name"/>
			</div>
			<div>
				Название оси:
				<input name="axis_name"/>
			</div>
			<div>
				<input type="submit" value="Сгенерировать"/>
			</div>
		</form>
	</xsl:template>
</xsl:stylesheet>
