<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="include/codegen.inc.xslt"/>
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'codegen'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:template match="codegen_dt">
		<h2>Генерация dt-контролов</h2>
		<xsl:call-template name="codegen_pass_info"/>
		<form method="post" action="{$save_prefix}/codegen_dt/">
			<div>
				Название dt:
				<input name="dt_name"/>
			</div>
			<div>
				Название модуля:
				<input name="module_name"/>
			</div>
			<div>
				Ось добавления:
				<input name="axis_add" value="add"/>
			</div>
			<div>
				Ось редактирования:
				<input name="axis_edit" value="edit"/>
			</div>
			<div>
				<input type="submit" value="Сгенерировать"/>
			</div>
		</form>
	</xsl:template>
</xsl:stylesheet>
