<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'streams'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="stream_id" select="/root/admin_stream_edit/document/@id"/>
	<xsl:variable name="stream_title" select="/root/admin_stream_edit/document/field[@name = 'title']"/>
	<xsl:variable name="current_stream" select="/root/stream_current[1]"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/streams/')"/>
	<xsl:template match="admin_stream_edit">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Streams')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Streams')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="$current_stream/@title"/>
			</h2>
			<div class="box p">
				<p>
					<xsl:value-of select="php:function('trans', 'Additional fields can be edited in the', 'ADDITIONAL_FIELDS_STREAM_ADMIN')"/>
					<xsl:text> </xsl:text>
					<a href="{$current_stream/@url}admin/">
						<xsl:value-of select="php:function('trans', 'stream settings', 'ADDITIONAL_FIELDS_STREAM_ADMIN')"/>
					</a>
					<xsl:text>. </xsl:text>
					<xsl:value-of select="php:function('trans', 'See also')"/>
					<xsl:text> </xsl:text>
					<a href="{$current_stream/@url}">
						<xsl:value-of select="php:function('trans', 'stream home page', 'SEE')"/>
					</a>
					<xsl:text>.</xsl:text>
				</p>
			</div>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/admin_stream_edit/" method="post" enctype="multipart/form-data">
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Editing')"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$stream_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
