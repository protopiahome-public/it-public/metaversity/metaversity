<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'users'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="user_id" select="/root/admin_user_edit/document/@id"/>
	<xsl:variable name="user_title" select="/root/user_short[@id = $user_id]/@visible_name"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/users/')"/>
	<xsl:template match="admin_user_edit">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Users')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Users')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="$user_title"/>
			</h2>
			<div class="prop p">
				<a class="underline_inner_span" href="{$users_prefix}/{/root/user_short[@id = $user_id]/@login}/">
					<span>
						<xsl:value-of select="php:function('trans', 'User profile')"/>
					</span>
					<xsl:text> </xsl:text>
					<i class="fa fa-external-link"/>
				</a>
			</div>
			<h3>
				<xsl:value-of select="php:function('trans', 'Statuses in streams')"/>
			</h3>
			<xsl:call-template name="_draw_streams"/>
			<h3>
				<xsl:value-of select="php:function('trans', 'Profile settings')"/>
			</h3>
			<xsl:call-template name="_draw_form"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw_streams">
		<xsl:if test="/root/stream_short">
			<table class="table table__no_th p">
				<xsl:for-each select="/root/stream_statuses/stream">
					<tr>
						<td class="cell_text pr10">
							<xsl:for-each select="/root/stream_short[@id = current()/@id]">
								<a href="{@url}">
									<xsl:value-of select="@title"/>
									<xsl:text> (</xsl:text>
									<xsl:value-of select="@title_short"/>
									<xsl:text>)</xsl:text>
								</a>
							</xsl:for-each>
							<div class="cell_text_line hide600">
								<xsl:call-template name="draw_stream_status">
									<xsl:with-param name="you" select="false()"/>
									<xsl:with-param name="mode" select="'silent'"/>
									<xsl:with-param name="url">
										<xsl:if test="@status != 'deleted'">
											<xsl:value-of select="concat(/root/stream_short[@id = current()/@id]/@url, '/admin/members/', $user_id, '/')"/>
										</xsl:if>
									</xsl:with-param>
									<xsl:with-param name="force_url" select="true()"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="cell_text pr10 show600">
							<xsl:call-template name="draw_stream_status">
								<xsl:with-param name="you" select="false()"/>
								<xsl:with-param name="mode" select="'short'"/>
								<xsl:with-param name="url">
									<xsl:if test="@status != 'deleted'">
										<xsl:value-of select="concat(/root/stream_short[@id = current()/@id]/@url, '/admin/members/', $user_id, '/')"/>
									</xsl:if>
								</xsl:with-param>
								<xsl:with-param name="force_url" select="true()"/>
							</xsl:call-template>
						</td>
						<td class="cell_btn center">
							<form action="{$save_prefix}/admin_user_edit_status/" method="post">
								<input type="hidden" name="stream_id" value="{@id}"/>
								<xsl:choose>
									<xsl:when test="@status = 'admin' or @status = 'moderator' or @status = 'member'">
										<input type="hidden" name="decline-user-{$user_id}" value="1"/>
										<button class="ctrl ctrl__button ctrl__button__red">
											<xsl:value-of select="php:function('trans', 'Remove')"/>
										</button>
									</xsl:when>
									<xsl:otherwise>
										<input type="hidden" name="accept-user-{$user_id}" value="1"/>
										<button class="ctrl ctrl__button ctrl__button__green">
											<xsl:value-of select="php:function('trans', 'Add')"/>
										</button>
									</xsl:otherwise>
								</xsl:choose>
							</form>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template name="_draw_form">
		<form action="{$save_prefix}/admin_user_edit/" method="post" enctype="multipart/form-data">
			<xsl:call-template name="draw_dt_edit"/>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="$user_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
