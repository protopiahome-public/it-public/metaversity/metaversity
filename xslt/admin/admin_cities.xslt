<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'cities'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/cities/')"/>
	<xsl:template match="admin_cities">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Cities')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Cities')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$module_url}add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a city')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(city)">
			<p>
				<xsl:value-of select="php:function('trans', 'No cities were found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="city">
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'streams'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Streams', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'groups'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Groups', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'fake'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Not a city')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="city">
					<tr>
						<td class="cell_text">
							<div class="_title">
								<a href="{$module_url}{@id}/">
									<xsl:value-of select="@title"/>
								</a>
								<span class="note">
									<xsl:text> / </xsl:text>
									<xsl:value-of select="@title_en"/>
								</span>
							</div>
							<div class="props">
								<xsl:if test="@is_fake = 1">
									<div class="prop smaller hide600">
										<b>
											<xsl:value-of select="php:function('trans', 'Not a city')"/>
										</b>
									</div>
								</xsl:if>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Streams', 'COUNT')"/>
									<xsl:with-param name="text" select="@stream_count_calc"/>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Groups', 'COUNT')"/>
									<xsl:with-param name="text" select="@group_count_calc"/>
									<xsl:with-param name="class" select="'prop hide600'"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="cell_num p6 show600 center">
							<xsl:value-of select="@stream_count_calc"/>
						</td>
						<td class="cell_num p6 show600 center">
							<xsl:value-of select="@group_count_calc"/>
						</td>
						<td class="cell_num p6 show600 center">
							<xsl:if test="@is_fake = 1">
								<xsl:value-of select="php:function('trans', 'Yes')"/>
							</xsl:if>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$module_url}{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Cities')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
