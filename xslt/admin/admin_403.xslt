<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:variable name="top_section" select="'403'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="''"/>
	<xsl:template match="error_403">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Metaversity')"/>
			<xsl:with-param name="url" select="concat($main_prefix, '/')"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="access_denied">
			<i class="fa fa-ban red"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="php:function('trans', 'Access denied')"/>
		</div>
		<xsl:if test="not($user/@id != '')">
			<p>
				<xsl:value-of select="php:function('trans', 'Please', 'PLEASE_LOGIN')"/>
				<xsl:text> </xsl:text>
				<a href="{$login_url}">
					<xsl:value-of select="php:function('trans', 'login', 'PLEASE_LOGIN')"/>
				</a>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'to access this page.', 'PLEASE_LOGIN')"/>
			</p>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Access denied')"/>
	</xsl:template>
</xsl:stylesheet>
