<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'merge-users'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/merge-users/')"/>
	<xsl:template match="admin_merge_users">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Account merge')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<xsl:choose>
				<xsl:when test="$admin_section_main_page">
					<h1 class="head_duplicate">
						<xsl:value-of select="php:function('trans', 'Account merge')"/>
					</h1>
				</xsl:when>
				<xsl:otherwise>
					<h1 class="head_duplicate">
						<a href="{$module_url}">
							<xsl:value-of select="php:function('trans', 'Account merge')"/>
						</a>
					</h1>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="@step = 1">
				<xsl:call-template name="_draw_step_1"/>
			</xsl:if>
			<xsl:if test="@step = 2">
				<xsl:call-template name="_draw_step_2"/>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="_draw_step_1">
		<xsl:if test="$pass_info/info[@name = 'MERGED']">
			<div class="info">
				<span>
					<xsl:value-of select="php:function('trans', 'Accounts were merged. Login:')"/>
					<xsl:text> </xsl:text>
					<a href="{$users_prefix}/{$pass_info/info[@name = 'main_login']}/">
						<b>
							<xsl:value-of select="$pass_info/info[@name = 'main_login']"/>
						</b>
					</a>
					<xsl:text>.</xsl:text>
				</span>
			</div>
		</xsl:if>
		<xsl:if test="$pass_info/info[@name = 'SQL_UPDATE_FAILED']">
			<div class="error">
				<span>
					<xsl:value-of select="php:function('trans', 'Some scripts which have to update database after user merging have failed. Some non-important information might require re-calculation. Please write to developers about it.')"/>
				</span>
			</div>
		</xsl:if>
		<div class="box p">
			<p>
				<xsl:value-of select="php:function('trans', 'User merging cannot be rolled back.')"/>
			</p>
			<p>
				<xsl:value-of select="php:function('trans', 'Information from the mergee profile (contacts, photo) will be lost.')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="php:function('trans', 'Other information, including selected positions and marks, will be merged.')"/>
			</p>
		</div>
		<form action="{$module_url}" method="get">
			<div class="field2">
				<label for="main_account_login">
					<xsl:value-of select="php:function('trans', 'Main account (login):')"/>
				</label>
				<input id="main_account_login" class="ctrl ctrl__text" name="main_account_login" value="{$get_vars[@name = 'main_account_login']}" maxlength="255"/>
				<xsl:if test="$get_vars[@name = 'main_account_login'] and not(@main_account_user_id)">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Account is not found.')"/>
						</span>
					</div>
				</xsl:if>
			</div>
			<div class="field2 field2__error">
				<xsl:if test="$get_vars[@name = 'account_to_merge_login'] and not(@account_to_merge_user_id)">
					<xsl:attribute name="class">field2 field2__error</xsl:attribute>
				</xsl:if>
				<label for="account_to_merge_login">
					<xsl:value-of select="php:function('trans', 'Account to merge and remove (login):')"/>
				</label>
				<input id="account_to_merge_login" class="ctrl ctrl__text" name="account_to_merge_login" value="{$get_vars[@name = 'account_to_merge_login']}" maxlength="255"/>
				<xsl:if test="$get_vars[@name = 'account_to_merge_login'] and not(@account_to_merge_user_id)">
					<div class="_error">
						<span>
							<xsl:value-of select="php:function('trans', 'Account is not found.')"/>
						</span>
					</div>
				</xsl:if>
			</div>
			<br/>
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Next')"/>
				<xsl:text> →</xsl:text>
			</button>
		</form>
	</xsl:template>
	<xsl:template name="_draw_step_2">
		<xsl:choose>
			<xsl:when test="$pass_info/error[@name = 'IDENTICAL_IDS']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'Why do you try to do such a stupid thing?')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'INITIAL_DB_CHECK_FAILED']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'DB consistency check failed.')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Merging can be dangerous. Please write to developers.')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'ALL_COLUMNS_INCLUDED_EXTRA_COLUMN_DB_CHECK_FAILED']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'DB was updated without updating the merging algorithm (error: unexpected DB fields).')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Merging can be dangerous. Please write to developers.')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'ALL_COLUMNS_INCLUDED_ALGO_OUTDATED_DB_CHECK_FAILED']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'DB was updated without updating the merging algorithm (error: no information about some DB fields).')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Merging can be dangerous. Please write to developers.')"/>
					</span>
				</div>
			</xsl:when>
			<xsl:when test="$pass_info/error[@name = 'INSERT_IGNORE_UNALLOWED_DEPENDANCE_DB_CHECK_FAILED']">
				<div class="error">
					<span>
						<xsl:value-of select="php:function('trans', 'Some dependencies in DB which were not taken into consideration by the merging algorithm were found.')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="php:function('trans', 'Merging can be dangerous. Please write to developers.')"/>
					</span>
				</div>
			</xsl:when>
		</xsl:choose>
		<xsl:for-each select="/root/user_short[@id = current()/@main_account_user_id]">
			<h3>
				<xsl:value-of select="php:function('trans', 'Main account (will remain in the database)')"/>
			</h3>
			<table class="table table__no_th p">
				<tr>
					<td class="cell_user_big">
						<div class="_img">
							<a target="_blank" href="{$users_prefix}/{@login}/">
								<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.6)}" height="{round(photo_big/@height * 0.6)}" alt="{@login}"/>
							</a>
						</div>
						<div class="_title">
							<a target="_blank" href="{$users_prefix}/{@login}/">
								<xsl:value-of select="@visible_name"/>
							</a>
							<span class="_login">
								<xsl:text> (</xsl:text>
								<xsl:value-of select="@login"/>
								<xsl:text>)</xsl:text>
							</span>
						</div>
						<div class="_city">
							<xsl:value-of select="@city_title"/>
							<xsl:if test="@group_title != ''">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="@group_title"/>
							</xsl:if>
						</div>
					</td>
				</tr>
			</table>
		</xsl:for-each>
		<xsl:for-each select="/root/user_short[@id = current()/@account_to_merge_user_id]">
			<h3>
				<xsl:value-of select="php:function('trans', 'Account to be merged and removed')"/>
			</h3>
			<table class="table table__no_th p">
				<tr>
					<td class="cell_user_big">
						<div class="_img">
							<a target="_blank" href="{$users_prefix}/{@login}/">
								<img src="{photo_big/@url}" width="{round(photo_big/@width * 0.6)}" height="{round(photo_big/@height * 0.6)}" alt="{@login}"/>
							</a>
						</div>
						<div class="_title">
							<a target="_blank" href="{$users_prefix}/{@login}/">
								<xsl:value-of select="@visible_name"/>
							</a>
							<span class="_login">
								<xsl:text> (</xsl:text>
								<xsl:value-of select="@login"/>
								<xsl:text>)</xsl:text>
							</span>
						</div>
						<div class="_city">
							<xsl:value-of select="@city_title"/>
							<xsl:if test="@group_title != ''">
								<xsl:text>, </xsl:text>
								<xsl:value-of select="@group_title"/>
							</xsl:if>
						</div>
					</td>
				</tr>
			</table>
		</xsl:for-each>
		<form action="{$save_prefix}/admin_merge_users/" method="post">
			<input type="hidden" name="main_account_user_id" value="{@main_account_user_id}"/>
			<input type="hidden" name="account_to_merge_user_id" value="{@account_to_merge_user_id}"/>
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<br/>
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Merge these users')"/>
			</button>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Account merge')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
