<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'streams'"/>
	<xsl:variable name="admin_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/streams/')"/>
	<xsl:template match="admin_streams">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Streams')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Streams')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$module_url}add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Create a stream')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(stream)">
			<p>
				<xsl:value-of select="php:function('trans', 'No streams were created.')"/>
			</p>
		</xsl:if>
		<div class="box p">
			<p>
				<xsl:call-template name="trans">
					<xsl:with-param name="html">The order reflects priorities for the <a href="%1%">streams page</a>.</xsl:with-param>
					<xsl:with-param name="var1" select="concat($main_prefix, '/streams/')"/>
				</xsl:call-template>
			</p>
			<p>
				<xsl:call-template name="trans">
					<xsl:with-param name="html">Creating a new stream OR changing URL of an existing one starts the process of <b>SSL certificate reissuing</b>. Usually it requires 1-2 minutes. Please wait until the streams can be opened at the new address without errors. If this does not happen - write to support team.</xsl:with-param>
				</xsl:call-template>
			</p>
		</div>
		<xsl:if test="stream">
			<table class="table">
				<tr>
					<th/>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Language')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="''"/>
						<xsl:with-param name="title" select="php:function('trans', 'Cities', 'COUNT')"/>
						<xsl:with-param name="class" select="'p6 center'"/>
					</xsl:call-template>
				</tr>
				<tbody class="js_sort">
					<xsl:for-each select="stream">
						<tr id="i-{@id}">
							<td class="cell_sort">
								<div class="js_sort_handle _handle">
									<i class="_icon fa fa-arrows-v"/>
								</div>
							</td>
							<td class="cell_text">
								<a href="{$module_url}{@id}/">
									<xsl:if test="@list_show = 0">
										<xsl:attribute name="class">gray</xsl:attribute>
										<xsl:value-of select="php:function('trans', '[Hidden]')"/>
										<xsl:text> </xsl:text>
									</xsl:if>
									<xsl:value-of select="@title"/>
									<xsl:text> (</xsl:text>
									<xsl:value-of select="@title_short"/>
									<xsl:text>)</xsl:text>
								</a>
								<div class="props hide600">
									<xsl:call-template name="draw_kv">
										<xsl:with-param name="key" select="php:function('trans', 'Language')"/>
										<xsl:with-param name="text" select="@lang_title"/>
										<xsl:with-param name="class" select="'prop'"/>
									</xsl:call-template>
								</div>
							</td>
							<td class="cell_text p6 show600">
								<xsl:value-of select="@lang_title"/>
							</td>
							<td class="cell_num p6 center">
								<xsl:value-of select="@city_count_calc"/>
							</td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/jquery-ui.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/jquery.ui.touch-punch.min.js?v1"/>
		<script type="text/javascript" src="{$main_prefix}/js/sort.js?v1"/>
		<script type="text/javascript">
			var post_vars = {};
			var sort_ajax_ctrl = 'admin_streams_sort';
		</script>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Streams')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
