<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:include href="../_core/include/dt_edit.inc.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'streams'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/streams/')"/>
	<xsl:template match="admin_stream_add">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Streams')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Streams')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="php:function('trans', 'Creating a stream')"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/admin_stream_add/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<xsl:call-template name="draw_dt_edit">
				<xsl:with-param name="draw_button" select="false()"/>
			</xsl:call-template>
			<fieldset class="p">
				<h3><xsl:value-of select="php:function('trans', 'Data source')"/></h3>
				<xsl:if test="$pass_info/error[@name = 'COPY_FROM_NOT_SELECTED']">
					<div class="error">
						<span>
							<xsl:value-of select="php:function('trans', 'Since you checked some checkboxes – please select a stream to copy from.')"/>
						</span>
					</div>
				</xsl:if>
				<div class="field2">
					<label for="f-copy-from">
						<xsl:value-of select="php:function('trans', 'Stream')"/>
						<xsl:text>:</xsl:text>
					</label>
					<select id="f-copy-from" name="copy-from" class="js_ctrl_select_with_front ctrl ctrl__select">
						<option value="0">
							<xsl:value-of select="php:function('trans', 'Not selected')"/>
						</option>
						<xsl:for-each select="streams/stream">
							<option value="{@id}">
								<xsl:choose>
									<xsl:when test="$pass_info/vars/var[@name = 'copy-from'] = @id">
										<xsl:attribute name="selected">selected</xsl:attribute>
									</xsl:when>
									<xsl:when test="$pass_info/vars/var[@name = 'copy-from']"/>
								</xsl:choose>
								<xsl:value-of select="@title"/>
								<xsl:text> (</xsl:text>
								<xsl:value-of select="@title_short"/>
								<xsl:text>)</xsl:text>
							</option>
						</xsl:for-each>
					</select>
				</div>
				<h3>
					<xsl:value-of select="php:function('trans', 'Data to copy')"/>
				</h3>
				<xsl:if test="$pass_info/error[@name = 'COPY_TREE']">
					<div class="error">
						<span>
							<xsl:value-of select="php:function('trans', 'You have to copy competence tree to copy other items (please check the top checkbox).')"/>
						</span>
					</div>
				</xsl:if>
				<xsl:if test="$pass_info/error[@name = 'COPY_NOTHING_CHECKED']">
					<div class="error">
						<span>
							<xsl:value-of select="php:function('trans', 'You have chosen a stream to copy from – but did not check any checkboxes.')"/>
						</span>
					</div>
				</xsl:if>
				<div class="field2">
					<input id="f-copy-tree" class="ctrl ctrl__checkbox" type="checkbox" name="copy-tree" value="1">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = 'copy-tree']">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:when>
						</xsl:choose>
					</input>
					<label for="f-copy-tree" class="_inline">
						<xsl:value-of select="php:function('trans', 'Competence tree')"/>
					</label>
				</div>
				<div class="field2">
					<input id="f-copy-positions" class="ctrl ctrl__checkbox" type="checkbox" name="copy-positions" value="1">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = 'copy-positions']">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:when>
						</xsl:choose>
					</input>
					<label for="f-copy-positions" class="_inline">
						<xsl:value-of select="php:function('trans', 'Positions')"/>
					</label>
				</div>
				<div class="field2">
					<input id="f-copy-roles" class="ctrl ctrl__checkbox" type="checkbox" name="copy-roles" value="1">
						<xsl:choose>
							<xsl:when test="$pass_info/vars/var[@name = 'copy-roles']">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:when>
						</xsl:choose>
					</input>
					<label for="f-copy-roles" class="_inline">
						<xsl:value-of select="php:function('trans', 'Formats and roles')"/>
					</label>
				</div>
			</fieldset>
			<button class="ctrl ctrl__button ctrl__button__big">
				<xsl:value-of select="php:function('trans', 'Create a stream')"/>
			</button>
		</form>
	</xsl:template>
	<xsl:template mode="head" match="/root">
		<script type="text/javascript" src="{$main_prefix}/js/dt.js?v8"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Creating a stream')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
