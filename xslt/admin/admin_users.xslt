<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:include href="../users/include/users_common.inc.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'users'"/>
	<xsl:variable name="admin_section_main_page" select="/root/users/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/users/')"/>
	<xsl:template match="/root" mode="body_class_2">with_widgets</xsl:template>
	<xsl:template match="users"/>
	<xsl:template match="admin_users">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Users')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'Users')"/>
			</h1>
			<xsl:for-each select="/root/users[1]">
				<xsl:call-template name="_draw"/>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="widgets">
			<xsl:apply-templates select="filters" mode="filters"/>
		</div>
		<xsl:call-template name="draw_users">
			<xsl:with-param name="small" select="true()"/>
			<xsl:with-param name="narrow_move_marks" select="true()"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="users_th_left" match="users">
		<xsl:call-template name="draw_th">
			<xsl:with-param name="name" select="'id'"/>
			<xsl:with-param name="title" select="php:function('trans', 'ID')"/>
			<xsl:with-param name="class" select="'pr6 show600'"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template mode="users_td_left" match="user">
		<td class="cell_text pr6 show600">
			<xsl:value-of select="@id"/>
		</td>
	</xsl:template>
	<xsl:template mode="users_line" match="user">
		<div class="_line hide600">
			<xsl:call-template name="draw_kv">
				<xsl:with-param name="key" select="php:function('trans', 'ID')"/>
				<xsl:with-param name="text" select="@id"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template mode="users_th_right" match="users">
		<th/>
	</xsl:template>
	<xsl:template mode="users_td_right" match="user">
		<td class="cell_btn">
			<a class="_btn" href="{$module_url}{@id}/">
				<i class="_btn_icon red fa fa-gear"/>
			</a>
		</td>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Users')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
