<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:include href="../_core/include/delete.inc.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'cities'"/>
	<xsl:variable name="admin_section_main_page" select="false()"/>
	<xsl:variable name="city_id" select="/root/admin_city_delete/@id"/>
	<xsl:variable name="city_title" select="/root/admin_city_delete/@title"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/cities/')"/>
	<xsl:template match="admin_city_delete">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'Cities')"/>
			<xsl:with-param name="url" select="$module_url"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<a href="{$module_url}">
					<xsl:value-of select="php:function('trans', 'Cities')"/>
				</a>
			</h1>
			<h2>
				<xsl:value-of select="$city_title"/>
			</h2>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<form action="{$save_prefix}/admin_city_delete/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="retpath" value="{$module_url}"/>
			<xsl:choose>
				<xsl:when test="@user_count &gt; 0">
					<div class="box p">
						<p>
							<xsl:value-of select="php:function('trans', 'You cannot delete city')"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="php:function('trans', 'LQ')"/>
							<xsl:value-of select="@title"/>
							<xsl:value-of select="php:function('trans', 'RQ')"/>
							<xsl:text>. </xsl:text>
							<xsl:value-of select="php:function('trans', 'There are users connected to it:')"/>
						</p>
						<ul>
							<xsl:for-each select="user">
								<li>
									<a href="{$main_prefix}/admin/users/{@id}/">
										<xsl:value-of select="@login"/>
									</a>
								</li>
							</xsl:for-each>
							<xsl:if test="@user_count &gt; 10">
								<li>
									<xsl:text>... </xsl:text>
									<xsl:value-of select="php:function('trans', 'total count:')"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="@user_count"/>
									<xsl:text> </xsl:text>
									<xsl:call-template name="count_case_lang">
										<xsl:with-param name="word" select="'user'"/>
										<xsl:with-param name="number" select="@user_count"/>
									</xsl:call-template>
									<xsl:text>.</xsl:text>
								</li>
							</xsl:if>
						</ul>
					</div>
					<p>
						<input class="ctrl ctrl__button ctrl__button__big ctrl__button__cancel" type="submit" name="cancel" value="{php:function('trans', 'Cancel', 'VERB')}"/>
					</p>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="draw_delete">
						<xsl:with-param name="title_akk" select="php:function('trans', 'the city', 'AKK')"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</form>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'Deleting')"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$city_title"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
