<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:include href="../_site/base_page_layout.xslt"/>
	<xsl:include href="menu/admin.menu.xslt"/>
	<xsl:variable name="top_section" select="'admin'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="admin_section" select="'news'"/>
	<xsl:variable name="admin_section_main_page" select="/root/admin_system_news/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($main_prefix, '/admin/news/')"/>
	<xsl:template match="admin_system_news">
		<xsl:apply-templates mode="header" select="/root">
			<xsl:with-param name="title" select="php:function('trans', 'News')"/>
			<xsl:with-param name="url" select="$module_url"/>
			<xsl:with-param name="use_url" select="not($admin_section_main_page)"/>
		</xsl:apply-templates>
		<div class="content text">
			<h1 class="head_duplicate">
				<xsl:value-of select="php:function('trans', 'News')"/>
			</h1>
			<xsl:call-template name="_draw"/>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<div class="p">
			<a class="link_btn link_btn__add" href="{$module_url}add/">
				<i class="_icon fa fa-plus"/>
				<span class="_text">
					<xsl:value-of select="php:function('trans', 'Add a news item')"/>
				</span>
			</a>
		</div>
		<xsl:if test="not(system_news_item)">
			<p>
				<xsl:value-of select="php:function('trans', 'News were not found.')"/>
			</p>
		</xsl:if>
		<xsl:if test="system_news_item">
			<table class="table">
				<tr>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'title'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Title')"/>
						<xsl:with-param name="class" select="''"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'moderators'"/>
						<xsl:with-param name="title" select="php:function('trans', 'For moderators')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<xsl:call-template name="draw_th">
						<xsl:with-param name="name" select="'time'"/>
						<xsl:with-param name="title" select="php:function('trans', 'Added')"/>
						<xsl:with-param name="class" select="'p6 show600'"/>
					</xsl:call-template>
					<th/>
				</tr>
				<xsl:for-each select="system_news_item">
					<tr>
						<td class="cell_text">
							<div class="_title">
								<a href="{$module_url}{@id}/">
									<xsl:value-of select="@title"/>
								</a>
							</div>
							<div class="props hide600">
								<xsl:if test="@is_for_moderators = 1">
									<div class="prop smaller">
										<b>
											<xsl:value-of select="php:function('trans', 'For moderators')"/>
										</b>
									</div>
								</xsl:if>
								<xsl:call-template name="draw_kv">
									<xsl:with-param name="key" select="php:function('trans', 'Added')"/>
									<xsl:with-param name="text">
										<xsl:call-template name="get_time">
											<xsl:with-param name="datetime" select="@add_time"/>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="class" select="'prop smaller'"/>
								</xsl:call-template>
							</div>
						</td>
						<td class="cell_text center p6 show600">
							<xsl:if test="@is_for_moderators = 1">
								<xsl:value-of select="php:function('trans', 'Yes')"/>
							</xsl:if>
						</td>
						<td class="cell_text cell_text__smaller p6 nobr show600">
							<xsl:call-template name="get_time">
								<xsl:with-param name="datetime" select="@add_time"/>
							</xsl:call-template>
						</td>
						<td class="cell_btn">
							<a class="_btn" href="{$module_url}{@id}/delete/" title="{php:function('trans', 'Delete', 'VERB')}">
								<i class="_btn_icon red fa fa-times"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
		<xsl:apply-templates select="pages"/>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="php:function('trans', 'News')"/>
		<xsl:call-template name="trans">
			<xsl:with-param name="html">_TITLE_SEP_</xsl:with-param>
		</xsl:call-template>
		<xsl:value-of select="php:function('trans', 'Administration')"/>
	</xsl:template>
</xsl:stylesheet>
