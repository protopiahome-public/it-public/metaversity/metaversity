<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
	<xsl:template mode="body_class" match="/root">with_menu_left with_menu_top</xsl:template>
	<xsl:template mode="menu_left" match="/root">
		<xsl:param name="base_url" select="concat($main_prefix, '/admin/')"/>
		<div class="menu_left">
			<div class="_submenu">
				<div class="js_btn_menu_left_open_main menu_item menu_item__clickable">
					<span class="_icon fa fa-home"/>
					<xsl:text> </xsl:text>
					<span class="_text _text__dashed">
						<xsl:value-of select="php:function('trans', 'Main menu')"/>
					</span>
					<xsl:text> </xsl:text>
					<span class="_down fa fa-caret-down"/>
				</div>
				<div class="js_menu_left_main _submenu_contents hide">
					<xsl:apply-templates mode="menu_items_main" select="/root"/>
				</div>
			</div>
			<div class="_submenu">
				<xsl:choose>
					<xsl:when test="$user/@id">
						<xsl:apply-templates mode="menu_items_user_title" select="/root">
							<xsl:with-param name="clickable" select="true()"/>
						</xsl:apply-templates>
						<div class="js_menu_left_user _submenu_contents hide">
							<xsl:apply-templates mode="menu_items_lang" select="/root"/>
							<xsl:apply-templates mode="menu_items_user" select="/root"/>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates mode="menu_items_lang" select="/root"/>
						<xsl:apply-templates mode="menu_items_guest" select="/root"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<div class="_sep"/>
			<div class="_title_admin">
				<i class="fa fa-gears"/>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="$top_section_main_page">
						<xsl:value-of select="php:function('trans', 'Administration')"/>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$main_prefix}/admin/">
							<xsl:value-of select="php:function('trans', 'Administration')"/>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<xsl:for-each select="/root/admin_menu/item">
				<xsl:choose>
					<xsl:when test="$admin_section = @section_name and $admin_section_main_page">
						<span class="menu_item menu_item__current">
							<span class="_text">
								<xsl:value-of select="@title"/>
							</span>
						</span>
					</xsl:when>
					<xsl:otherwise>
						<a href="{@path}" class="menu_item">
							<xsl:if test="$admin_section = @section_name">
								<xsl:attribute name="class">menu_item menu_item__current</xsl:attribute>
							</xsl:if>
							<span class="_text">
								<xsl:value-of select="@title"/>
							</span>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</div>
	</xsl:template>
</xsl:stylesheet>
