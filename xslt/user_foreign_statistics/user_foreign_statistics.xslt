<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="false()"/>
	<xsl:variable name="user_section" select="'results'"/>
	<xsl:variable name="user_section_main_page" select="not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/', $current_user/@login, '/results/foreign/')"/>
	<xsl:template match="user_foreign_statistics">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<xsl:call-template name="draw_user_head2"/>
							<h2>Результаты с других сайтов</h2>
							<div class="content">
								<xsl:if test="$pass_info/info[@name = 'STATISTIC_ADDED']">
									<div class="info">
										<span>Новый результат добавлен</span>
									</div>
								</xsl:if>
								<xsl:call-template name="draw_user_results_submenu"/>
								<xsl:call-template name="_draw"/>
							</div>
						</td>
						<td class="right-column">
							
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:call-template name="_draw_provider">
			<xsl:with-param name="provider_name" select="'cardspuzzles'"/>
			<xsl:with-param name="provider_title" select="'Карты-паззлы'"/>
		</xsl:call-template>
		<xsl:call-template name="_draw_provider">
			<xsl:with-param name="provider_name" select="'tests'"/>
			<xsl:with-param name="provider_title" select="'Тесты'"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="_draw_provider">
		<xsl:param name="provider_name"/>
		<xsl:param name="provider_title"/>
		<h3>
			<xsl:value-of select="$provider_title"/>
		</h3>
		<xsl:for-each select="statistic[@provider = $provider_name]">
			<div>
				<xsl:value-of select="@test_title"/>
				<xsl:text>: </xsl:text>
				<xsl:value-of select="@success_percent"/>
				<xsl:text>% (</xsl:text>
				<xsl:value-of select="@add_time"/>
				<xsl:text>)</xsl:text>
			</div>
		</xsl:for-each>
		<br/>
	</xsl:template>
</xsl:stylesheet>
