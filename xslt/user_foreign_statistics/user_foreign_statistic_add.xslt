<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="top_section" select="'users'"/>
	<xsl:variable name="top_section_main_page" select="/root/users/pages/@current_page = 1 and not($get_vars)"/>
	<xsl:variable name="module_url" select="concat($users_prefix, '/foreign-statistic-add/')"/>
	<xsl:template match="user_foreign_statistic_add">
		<div class="color-blue">
			<div class="columns-wrap columns-wrap-default">
				<table cellspacing="0" class="columns">
					<tr>
						<td class="center-column">
							<div class="content">
								<xsl:call-template name="_draw"/>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="_draw">
		<xsl:choose>
			<xsl:when test="$pass_info/error[@name = 'TEST_TIME_LIMIT']">
				<div class="error">
					<span>Отправлять результаты тестов в Метаверситет можно только раз в две недели.</span>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<form action="{$request/@main_prefix}/save/user_foreign_statistic_add/" method="post">
					<input type="hidden" name="test_url" value="{$pass_info/vars/var[@name = 'test_url']}"/>
					<input type="hidden" name="test_title" value="{$pass_info/vars/var[@name = 'test_title']}"/>
					<input type="hidden" name="provider" value="{$pass_info/vars/var[@name = 'provider']}"/>
					<input type="hidden" name="success_percent" value="{$pass_info/vars/var[@name = 'success_percent']}"/>
					<input type="hidden" name="retpath" value="{$pass_info/vars/var[@name = 'retpath']}"/>
					<xsl:choose>
						<xsl:when test="$user/@id &gt; 0">
							<button class="button">Отправить результат</button>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$get_vars[@name='retpath']">
								<input type="hidden" name="retpath" value="{$get_vars[@name='retpath']}"/>
							</xsl:if>
							<xsl:if test="$pass_info/error[@name = 'BAD_LOGIN_OR_PASSWORD']">
								<div class="error">
									<span>Неверный логин или пароль. Попробуйте ещё раз.</span>
								</div>
							</xsl:if>
							<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
								<label class="title-" for="f-{@name}">Логин:</label>
								<div class="input-">
									<input class="input-text narrow-" type="text" name="login" maxlength="40" value="{$pass_info/vars/var[@name = 'login']}"/>
								</div>
							</div>
							<div id="field-{@name}" class="field" jq-name="{@name}" jq-type="{@type}">
								<label class="title-" for="f-{@name}">Пароль:</label>
								<div class="input-">
									<input class="input-text narrow-" type="password" name="password" maxlength="255" value="{$pass_info/vars/var[@name = 'password']}"/>
								</div>
							</div>
							<button class="button">Войти и отправить результат</button>
						</xsl:otherwise>
					</xsl:choose>
				</form>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template mode="title" match="/root">
		<xsl:value-of select="'Добавить внешний результат'"/>
	</xsl:template>
	<xsl:template mode="nav" match="/root">
		<div class="level1- selected-">
			<xsl:choose>
				<xsl:when test="$top_section_main_page">Люди</xsl:when>
				<xsl:otherwise>
					<a href="{$users_prefix}/">Люди</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
</xsl:stylesheet>
