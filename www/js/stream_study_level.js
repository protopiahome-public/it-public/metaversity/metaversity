$(function() {
	$('#field-study_level .jq-dtf-multi-link-item').click(function() {
		refresh_tree();
	});

	refresh_tree();

	function is_nonempty(obj) {
		if (typeof(obj) !== 'object') {
			return false;
		}
		for (var i in obj) {
			return true;
		}
		return false;
	}
	function refresh_tree() {
		if (!$('.js_tree').length) return;
		var study_levels = [];
		$('#field-study_level .jq-dtf-multi-link-item:checked').each(function() {
			study_levels.push($(this).data('id'));
		});
		function intersects(restrictions) {
			if (!restrictions || !restrictions.study_levels || !is_nonempty(restrictions.study_levels) || !is_nonempty(study_levels)) {
				return true;
			}
			for (var r_study_level_key in restrictions.study_levels) {
				var r_study_level_id = restrictions.study_levels[r_study_level_key];
				if ($.inArray(r_study_level_id, study_levels) !== -1) {
					return true;
				}
			}
			return false;
		}
		$('.js_tree_group_children').each(function() {
			var group_id = $(this).data('group-id');
			var restrictions = $(this).data('restrictions');
			var show = intersects(restrictions);
			$('.js_tree_group_' + group_id + '_content').toggle(show);
		});
		$('.js_tree_competence').each(function() {
			var competence_id = $(this).data('competence-id');
			var group_id = $(this).data('group-id');
			var restrictions = $(this).data('restrictions');
			var show = intersects(restrictions);
			$(this).toggleClass('row_competence_disabled', !show);
		});
	}
});