(function($) {
	$.fn.sortElements = function(comparator) {
		var data = [];
		this.each(function(idx, el) {
			data.push($(this));
		});
		data.sort(comparator);
		data.reverse();
		var $parent = this.first().parent();
		$.each(data, function(idx, $item) {
			$item.prependTo($parent);
		});
		return this;
	}
})(jQuery);