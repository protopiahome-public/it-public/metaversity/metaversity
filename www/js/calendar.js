$(function() {
	$('.js_calendar').each(function() {
		var $cal = $(this);
		var waiting = false;
		$cal.on('click', '.js_link_prev, .js_link_next', function() {
			if (waiting) return;
			$cal.find('table').css('opacity', 0.5);
			var year = $(this).data('year');
			var month = $(this).data('month');
			var post_data = {
				year: year,
				month: month
			};
			waiting = true;
			xpost({
				ajax_ctrl_name: calendar_ajax_page,
				query_string: calendar_query_string,
				data: post_data,
				custom_loading_msg: '',
				success_callback: function(result, options) {
					if (result.html) {
						$cal.find('table').replaceWith(result.html);
						waiting = false;
					}
				}
			});
		});
	});
});