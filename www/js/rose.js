$(function() {
	$(window).on('markuprefresh', draw);
	draw();
	function draw() {
		$('.rose').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function() {
			$('body').append('<div class="css_connect css_connect__rose_double_resolution"></div>');
			var scale = cssConnectIsActive('rose_double_resolution') ? 2 : 1;
			var wFinal = 300;
			var hFinal = 300;
			var w = wFinal * scale;
			var h = hFinal * scale;
			var $canvas = $('<canvas width="' + w + '" height="' + h + '"></canvas>');
			$canvas.width(wFinal + 'px');
			$canvas.height(hFinal + 'px');
			$(this).append($canvas);
			var canvasEl = $canvas.get(0);
			// Canvas support
			if (!canvasEl.getContext) {
				$canvas.replaceWith('<p>(ERROR: Canvas is not supported by your browser)</p>')
				return;
			}
			// Data
			var data = [];
			$(this).find('[data-row]').each(function() {
				data.push($(this).data('row'));
			});
			if (data.length < 3) {
				$canvas.remove();
				return;
			}
			// Columns
			var cols = [];
			$(this).find('[data-column]').each(function() {
				cols.push($(this).data('column'));
			});
			// Init 
			var ctx = canvasEl.getContext("2d");
			var cx = w / 2;
			var cy = h / 2;
			var r2 = Math.min(cx, cy);
			var count = data.length;
			// Common vars
			var i, x, y;
			// Coefficients
			var dx = [];
			var dy = [];
			var angle = [];
			for (i = 0; i < count; ++i) {
				angle[i] = i / count * 2 * Math.PI;
				dx[i] = Math.sin(angle[i]) * r2;
				dy[i] = Math.cos(angle[i]) * r2;
			}
			// Fraw bg
			ctx.fillStyle = "#ffffff";
			ctx.fillRect(0, 0, w, h);
			// Draw radial lines
			ctx.strokeStyle = "#e0e0e0";
			ctx.beginPath();
			for (i = 0; i < count; ++i) {
				ctx.moveTo(cx, cy);
				ctx.lineTo(cx + 0.75 * dx[i], cy - 0.75 * dy[i]);
			}
			ctx.stroke();
			// Draw arcs
			ctx.beginPath();
			ctx.arc(cx, cy, 1 * r2 / 4, 0, 2 * Math.PI);
			ctx.stroke();
			ctx.beginPath();
			ctx.arc(cx, cy, 2 * r2 / 4, 0, 2 * Math.PI);
			ctx.stroke();
			ctx.beginPath();
			ctx.arc(cx, cy, 3 * r2 / 4, 0, 2 * Math.PI);
			ctx.stroke();
			// Draw numbers
			ctx.textBaseline = "middle";
			ctx.fillStyle = "#000000";
			var fs, thw, thh, text, angle1;
			for (i = 0; i < count; ++i) {
				if (!data[i].id) continue;
				text = data[i].id + '';
				fs = ((text.length <= 4 && count <= 20) ? 14 : (count < 30) ? 12 : 11) * scale;
				ctx.font = fs + 'px Arial';
				thw = ctx.measureText(text).width / 2;
				thh = fs / 2;
				angle1 = Math.atan2(dx[i], dy[i]);
				ctx.fillText(text, cx + 0.75 * dx[i] - thw + (thw + r2 / 50) * Math.sin(angle1), cy - 0.75 * dy[i] - (thh + r2 / 50) * Math.cos(angle1));
			}
			// Draw columns
			var colIdx, colName, x0, y0, k;
			for (var colIdx in cols) {
				colName = cols[colIdx].name;
				ctx.strokeStyle = cols[colIdx].color;
				ctx.fillStyle = cols[colIdx].fillColor;
				ctx.beginPath();
				for (i = 0; i < count; ++i) {
					k = Math.max(data[i][colName] / 3, 0.03);
					x = cx + 0.75 * dx[i] * k;
					y = cy - 0.75 * dy[i] * k;
					if (i == 0) {
						ctx.moveTo(x, y);
						x0 = x;
						y0 = y;
					}
					else {
						ctx.lineTo(x, y);
					}
				}
				ctx.lineTo(x0, y0);
				ctx.stroke();
				ctx.fill();
			}
			// Draw scale
			ctx.font = 12 * scale + 'px Arial';
			ctx.fillStyle = "#000000";
			ctx.textBaseline = "top";
			ctx.fillText('1', cx - ctx.measureText('1').width / 2, cy - 0.25 * r2);
			ctx.fillText('2', cx - ctx.measureText('2').width / 2, cy - 0.50 * r2);
			ctx.fillText('3', cx - ctx.measureText('3').width / 2, cy - 0.75 * r2);
			// Link click event
			//$a.click(function() {
			//	var dataURL = $canvas.get(0).toDataURL('image/png');
			//	$(this).attr('href', dataURL);
			//	return true;
			//});
			// Mouse move event
			var $hint = $('<span class="hint hide"></span>');
			var $tester = $('<span class="hint preload"></span>');
			$('body').append($hint);
			$('body').append($tester);
			$canvas.on('mousemove', function(e) {
				var delta = 2 * Math.PI / count;
				var angle = Math.PI - Math.atan2(e.offsetX - cx, e.offsetY - cy) - delta / 2;
				var dataIdx = Math.ceil(angle / delta) % count;
				var html = imHtmlize(data[dataIdx].title);
				if (typeof(data[dataIdx].hint_line2) !== 'undefined') {
					html += '<br/>' + imHtmlize(data[dataIdx].hint_line2);
				}
				$tester.html(html);
				$hint.html(html);
				var x = e.pageX + 10;
				var y = e.pageY + 20;
				if (x + $tester.width() > $(window).width()) {
					x = $(window).width() - $tester.width() - 10;
				}
				$hint.css('left', x + 'px');
				$hint.css('top', y + 'px');
			});
			$canvas.on('mouseover', function(e) {
				$hint.show();
			});
			$canvas.on('mouseout', function(e) {
				$hint.hide();
			});
		});
	}
});