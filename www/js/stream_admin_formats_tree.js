/**
 * @todo escape when renaming not working
 * @todo move between nodes not work (dnd)
 */
function delete_dialog(remove_function) {
	xlightbox.show({
		content: $('#dialog-confirm-delete'),
		red: true,
		success: function (options) {
			remove_function();
			return true;
		}
	});
}

$(function () {
	$.jstree._themes = global.main_prefix + "lib/jstree/themes/";
	$('#tree_button_add_group').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		var $jstree = $("#format_tree").jstree(true);
		var obj = $jstree.get_selected()[0];
		$jstree.create_node(obj, {
			"text": trans['New group'],
			"type": "group",
		}, "last");
	});
	$('#tree_button_add_item').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		var $jstree = $("#format_tree").jstree(true);
		var obj = $jstree.get_selected()[0];
		$jstree.create_node(obj, {
			"text": trans['New format'],
			"type": "format",
		}, "last");
	});
	$('#tree_button_clone_item').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		clone_format();
	});
	$('#tree_button_edit').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		var $jstree = $("#format_tree").jstree(true);
		var obj = $jstree.get_selected()[0];
		$jstree.edit(obj);
	});
	$('#tree_button_edit_roles').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		location.href = base_format_roles_url + $(this).data('format-id') + '/';
	});
	$('#tree_button_delete').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		var $jstree = $("#format_tree").jstree(true);
		var obj = $jstree.get_selected()[0];
		delete_dialog(function () {
			$jstree.delete_node(obj);
		});
	});
	$("#format_tree").jstree({
		// List of active plugins
		"plugins": ["state", "themes", "massload", "dnd", "search", "types", "contextmenu", "conditionalselect"],
		"dnd": {
			"copy_modifier": false
		},
		"massload": {
			"url": formats_ajax_url,
			// the `data` function is executed in the instance's scope
			// the parameter is the node being loaded
			// (may be -1, 0, or undefined when loading the root nodes)
			"data": function (n) {
				// the result is fed to the AJAX request `data` option
				return {
					"operation": "get_nodes",
					"stream_id": stream_id,
					"id": n.attr ? n.attr("id").replace(/[a-z\_]/g, "") : "-"
				};
			}
		},
		// Using types - most of the time this is an overkill
		// read the docs carefully to decide whether you need types
		"types": {
			"#": {
				"valid_children": ["root"],
				"max_children": 1
			},
			"root": {
				"valid_children": ["group"],
			},
			// The default type
			"format": {
				// I want this type to have no children (so only leaf nodes)
				// In my case - those are files
				"valid_children": [],
				// If we specify an icon for the default type it WILL OVERRIDE the theme icons
				"icon": global.main_prefix + "lib/jstree/file.png"
			},
			// The `folder` type
			"group": {
				"valid_children": ["format", "group"]
			}
		},
		// the core plugin - not many options here
		"core": {
			"data": {
				"url": formats_ajax_url,
				// the `data` function is executed in the instance's scope
				// the parameter is the node being loaded
				// (may be -1, 0, or undefined when loading the root nodes)
				"data": function (n) {
					// the result is fed to the AJAX request `data` option
					return {
						"operation": "get_nodes",
						"stream_id": stream_id,
						"id": n.attr ? n.attr("id").replace(/[a-z\_]/g, "") : "-"
					};
				}
			},
			check_callback: function (operation, node, node_parent, node_position, more) {
				return operation != 'copy_node';
			}
		},
		"conditionalselect": function (node, event) {
			$("#format_tree").jstree(true).deselect_all();
			$("#format_tree").jstree(true).select_node(node, true);
			return true;
		},
		"contextmenu": {
			"items": function (node) {
				var $jstree = $("#format_tree").jstree(true);
				var can_create_group = true;
				var can_create_item = true;
				var can_delete = true;
				if (node.type == "root") {
					can_create_item = false;
					can_create_group = true;
					can_delete = false;
				} else if (node.type == "format") {
					can_create_item = false;
					can_create_group = false;
					can_delete = true;
				} else {
					can_create_item = true;
					can_create_group = true;
					can_delete = true;
					for (var i in node.children) {
						if ($jstree.get_node(node.children[i]).type == "group") {
							can_delete = false;
						}
						if ($jstree.get_node(node.children[i]).type == "format") {
							can_delete = false;
						}
					}
				}
				return {
					"create_item": {
						"label": trans['Add a format'],
						"action": function (data) {
							var $jstree = $.jstree.reference(data.reference);
							var obj = $jstree.get_node(data.reference);
							$jstree.create_node(obj, {
								"text": trans['New format'],
								"type": "format",
							}, "last");
						},
						"_disabled": !can_create_item
					},
					"clone_item": {
						"label": trans['Clone format'],
						"action": function (data) {
							clone_format();
						},
						"_disabled": node.type != 'format'
					},
					"create_group": {
						"label": trans['Add a group'],
						"action": function (data) {
							var $jstree = $.jstree.reference(data.reference);
							var obj = $jstree.get_node(data.reference);
							$jstree.create_node(obj, {
								"text": trans['New group'],
								"type": "group",
							}, "last");
						},
						"_disabled": !can_create_group
					},
					"rename": {
						"label": trans['Edit'],
						"action": function (data) {
							var $jstree = $.jstree.reference(data.reference);
							var obj = $jstree.get_node(data.reference);
							$jstree.edit(obj);
						},
						"_disabled": node.type == "root"
					},
					"delete": {
						"label": trans['Delete'],
						"action": function (data) {
							var $jstree = $.jstree.reference(data.reference);
							var obj = $jstree.get_node(data.reference);
							delete_dialog(function () {
								$jstree.delete_node(obj);
							});
						},
						"_disabled": !can_delete
					}
				}
			}
		}
	})
			.bind("create_node.jstree", function (e, data) {
				var operation = "";
				if (data.node.type == "group") {
					operation = "create_group";
				}
				if (data.node.type == "format") {
					operation = "create_item";
				}
				var post_data = {
					"operation": operation,
					"stream_id": stream_id,
					"group_id": data.parent.replace(/[a-z\_]/g, ""),
					"position": data.node.position,
					"title": data.node.text,
					"type": data.node.type
				};
				xpost({
					ajax_ctrl_name: formats_ajax_ctrl,
					data: post_data,
					success_callback: function (result, options) {
						data.instance.set_id(data.node, data.node.type + '_' + result.id);
						data.instance.set_text(data.node, result.title);
						data.instance.deselect_all();
						data.instance.edit(data.node);
						data.instance.select_node(data.node);
						if (operation === "create_group") {
							//data.instance.refresh();
						}
					},
					error_callback: function (result, options) {
						data.instance.refresh();
						$('.link_btn').addClass('.link_btn__disabled');
					}
				});
			})
			.bind("rename_node.jstree", function (e, data) {
				if (data.node.type == "group") {
					var operation = "rename_group";
				}
				if (data.node.type == "format") {
					var operation = "rename_item";
				}
				var post_data = {
					"operation": operation,
					"stream_id": stream_id,
					"id": data.node.id.replace(/[a-z\_]/g, ""),
					"title": data.text
				};
				xpost({
					ajax_ctrl_name: formats_ajax_ctrl,
					data: post_data,
					success_callback: function (result, options) {
						data.instance.set_text(data.node, result.title);
					},
					error_callback: function (result, options) {
						data.instance.refresh();
						$('.link_btn').addClass('.link_btn__disabled');
					}
				});
			})
			.bind("delete_node.jstree", function (e, data) {
				if (data.node.type == "group") {
					var operation = "delete_group";
				}
				if (data.node.type == "format") {
					var operation = "delete_item";
				}
				var post_data = {
					"operation": operation,
					"stream_id": stream_id,
					"id": data.node.id.replace(/[a-z\_]/g, "")
				};
				xpost({
					ajax_ctrl_name: formats_ajax_ctrl,
					data: post_data,
					success_callback: function (result, options) {
						data.instance.select_node(data.node.parent);
					},
					check_fail_callback: function (result, options) {
						if ("activity_exists" === result.error_descr) {
							show_error('#dialog-cannot-delete');
							data.instance.refresh();
						} else {
							show_error_ajax();
							data.instance.refresh();
						}
					},
					error_callback: function (result, options) {
						data.instance.refresh();
						$('.link_btn').addClass('.link_btn__disabled');
					}
				});
			})
			.bind("select_node.jstree", function (e, data) {
				var $jstree = $("#format_tree").jstree(true);
				var can_create_group = true;
				var can_create_item = true;
				var can_delete = true;
				var can_edit = true;
				var can_edit_roles = false;
				var node = data.node;
				if (node.type == "root") {
					can_create_item = false;
					can_edit = false;
					can_delete = false;
				} else if (node.type == "format") {
					can_create_item = false;
					can_create_group = false;
					can_edit_roles = true;
				} else {
					for (var i in node.children) {
						if ($jstree.get_node(node.children[i]).type == "group") {
							can_delete = false;
						}
						if ($jstree.get_node(node.children[i]).type == "format") {
							can_delete = false;
						}
					}
				}
				$("#tree_button_add_group").toggleClass("link_btn__disabled", !can_create_group);
				$("#tree_button_add_item").toggleClass("link_btn__disabled", !can_create_item);
				$("#tree_button_clone_item").toggleClass("link_btn__disabled", node.type != "format");
				$("#tree_button_edit").toggleClass("link_btn__disabled", !can_edit);
				$("#tree_button_edit_roles").toggleClass("link_btn__disabled", !can_edit_roles);
				$("#tree_button_edit_roles").data("format-id", node.id.replace(/[a-z\_]/g, ""));
				$("#tree_button_delete").toggleClass("link_btn__disabled", !can_delete);
			})
			.bind("move_node.jstree", function (e, data) {
				var $jstree = $("#format_tree").jstree(true);
				var parent_node = $jstree.get_node(data.parent);
				var old_parent_node = $jstree.get_node(data.old_parent);
				var position, operation;
				position = data.position;

				var can_create_group = true;
				var can_create_item = true;
				if (parent_node.type == "root") {
					can_create_item = false;
					can_create_group = true;
				} else if (parent_node.type == "format") {
					can_create_item = false;
					can_create_group = false;
				}

				if (data.node.type == "group") {
					if (!can_create_group) {
						data.instance.refresh();
						return false;
					}
					operation = "move_group";
				}
				if (data.node.type == "format") {
					if (!can_create_item) {
						data.instance.refresh();
						return false;
					}
					operation = "move_item";
				}

				var post_data = {
					"operation": operation,
					"stream_id": stream_id,
					"id": data.node.id.replace(/[a-z\_]/g, ""),
					"old_group_id": data.old_parent.replace(/[a-z\_]/g, ""),
					"group_id": data.parent.replace(/[a-z\_]/g, ""),
					"position": position,
					"title": data.node.text,
				};
				xpost({
					ajax_ctrl_name: formats_ajax_ctrl,
					data: post_data,
					success_callback: function (result, options) {
						data.instance.open_node(parent_node);
						data.instance.select_node(data.node);
					},
					error_callback: function (result, options) {
						data.instance.refresh();
						$('.link_btn').addClass('.link_btn__disabled');
					}
				});
			});

	function clone_format() {
		var $jstree = $("#format_tree").jstree(true);
		var obj = $jstree.get_node($jstree.get_selected()[0]);
		var post_data = {
			"operation": "clone_item",
			"stream_id": stream_id,
			"original_id": obj.id.replace(/[a-z\_]/g, ""),
		};
		xpost({
			ajax_ctrl_name: formats_ajax_ctrl,
			data: post_data,
			success_callback: function (result, options) {
				$jstree.refresh();
			},
			error_callback: function (result, options) {
				$jstree.refresh();
				$('.link_btn').addClass('.link_btn__disabled');
			}
		});
	}
});