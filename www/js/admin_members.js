$(function() {
	$('.js_btn_delete').click(function() {
		var $form_el = $(this).parents('form');
		$('.js_input_user_id').val($(this).attr('data-user-id'));
		$('.js_dialog_confirm_login').text($(this).attr('data-user-login'));
		xlightbox.show({
			content: $('.js_dialog_confirm'),
			show_cancel_button: true,
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
		return false;
	});
	$('.js_btn_bulk_delete').click(function() {
		var $form_el = $(this).parents('form');
		$('.js_input_user_id').val(0);
		var checked_count = $('.js_checkbox:checked').length;
		if (!checked_count) {
			return false;
		}
		$('.js_dialog_bulk_confirm_count').text(checked_count);
		xlightbox.show({
			content: $('.js_dialog_bulk_confirm'),
			show_cancel_button: true,
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
		return false;
	});
});
