$(function() {
	function rebuild_submenu_counts(result) {
		var selector = '#submenu_participants_subscription_count';
		$(selector).find('.js_number').text(result.count);
		$(selector).toggle(result.count > 0);
	}
	$('.js_subscription_select').xselect({
		on_select: function(value, $item, $select, is_user_action, old_value) {
			if (!is_user_action) {
				return;
			}

			if ($select.data('is_regular') !== 1 && old_value === 1 && value === 0) {
				$select.xselect('set', old_value);
				xlightbox.show({
					title: global.trans['Confirmation'],
					content: $('.js_unsubscribe_warning'),
					button_title: global.trans['Confirm'],
					show_cancel_button: true,
					red: true,
					success: function(options) {
						$select.xselect('set', 0);
						save();
						return true;
					}
				});
			}
			else {
				save();
			}

			function save() {
				var data = {
					user_id: $select.data('user_id'),
					is_subscribed: value
				};
				xpost({
					ajax_ctrl_name: 'stream_admin_activity_participants_subscription',
					data: data,
					check_fail_callback : function(result, options) {
						if (old_value === 0 && value === 1) {
							$select.xselect('set', 0);
							show_error('.js_cannot_subscribe_info');
						}
					},
					success_callback: function(result) {
						rebuild_submenu_counts(result);
					}
				});
			}
		}
	});
});