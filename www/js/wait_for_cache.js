$(function() {
	if (window.wait_for_cache_ajax_page) {
		var delta = 1000;
		ask_for_updates();
		function ask_for_updates() {
			setTimeout(function() {
				xpost({
					ajax_ctrl_name: wait_for_cache_ajax_page,
					success_callback: function(result, options) {
						if (result.ready) {
							location.href = location.href;
						} else {
							delta = delta < 4000 ? delta + 1000 : delta;
							ask_for_updates();
						}
					},
					custom_loading_msg : ''
				});
			}, delta);
		}
	}
});