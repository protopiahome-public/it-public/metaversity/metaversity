$(function() {
	
	$('input[readonly], select[readonly], textarea[readonly]').addClass('read-only');
	
	/* Deps */
	
	var DEP_ACTION_SHOW = 1;
	var DEP_ACTION_ENABLE = 2;
	var DEP_ACTION_IMPORTANT = 4;
	var DEP_ACTION_DO_NOT_STORE = 8;
	
	function get_inputs_selector(field_name) {
		var selectors = [];
		var inputs = ['input', 'select', 'textarea'];
		for (var input_idx in inputs) {
			selectors.push((field_name ? '#field-' + field_name : '.field') + ' ' + inputs[input_idx]);
		}
		return selectors.join(', ');
	}
	function dt_update_deps(field_name) {
		var value = null;
		if ($('#f-' + field_name + ':checkbox').length) {
			value = $('#f-' + field_name + ':checked').length ? 1 : 0;
		} else if ($('select#f-' + field_name).length) {
			value = $('select#f-' + field_name).val();
		}
		var activated_cache = [];
		for (var i in dt_deps) {
			if (dt_deps[i].depends_from == field_name) {
				var activate = value == dt_deps[i].activation_value;
				var selector;
				if (dt_deps[i].actions & DEP_ACTION_ENABLE && !activated_cache[dt_deps[i].dependee] & DEP_ACTION_ENABLE) {
					selector = get_inputs_selector(dt_deps[i].dependee);
					if (activate) {
						$(selector).removeAttr('disabled');
					} else {
						$(selector).attr('disabled', 'disabled');
					}
				}
				if (dt_deps[i].actions & DEP_ACTION_IMPORTANT && !activated_cache[dt_deps[i].dependee] & DEP_ACTION_IMPORTANT) {
					selector = '#field-' + dt_deps[i].dependee + ' .star';
					$(selector).toggle(activate);
				}
				if (dt_deps[i].actions & DEP_ACTION_SHOW && !activated_cache[dt_deps[i].dependee] & DEP_ACTION_SHOW) {
					selector = '#field-' + dt_deps[i].dependee;
					$(selector).toggle(activate);
				}
				if (activate) {
					activated_cache[dt_deps[i].dependee] = dt_deps[i].actions;
				}
			}
		}
	}
	
	var for_events = {};
	if (typeof(dt_deps) != 'undefined') {
		for (var i in dt_deps) {
			var key = '_' + dt_deps[i].depends_from;
			if (!for_events[key]) {
				dt_update_deps(dt_deps[i].depends_from);
				$(get_inputs_selector(dt_deps[i].depends_from)).change(function(field_name) {
					return function() {
						dt_update_deps(field_name);
					}
				}(dt_deps[i].depends_from));
				for_events[key] = true;
			}
		}
	}
	
	/* Access deps */
	
	$('.field[jq-type=access_level] select').change(function() {
		window.dt_access_deps = window.dt_access_deps || [];
		for (var i in window.dt_access_deps) {
			var $restrict_select = $('#f-' + window.dt_access_deps[i].depends_from);
			var restrict_level = $restrict_select.val();
			var $select = $('#f-' + window.dt_access_deps[i].field);
			var current_level = $select.val();
			var $option = null;
			var option_level = null;
			// disabling of options
			$select.find('option').each(function() {
				$option = $(this);
				option_level = $option.attr('value');
				if (option_level >= restrict_level) {
					$option.removeAttr('disabled');
				} else {
					$option.attr('disabled', 'disabled');
				}
			});
			// changing current option
			if (current_level < restrict_level) {
				var is_option_fixed = false;
				$select.find('option').each(function() {
					$option = $(this);
					option_level = $option.attr('value');
					if (!is_option_fixed && option_level >= restrict_level) {
						$select.val(option_level);
						is_option_fixed = true;
					}
				});
				if (!is_option_fixed && option_level) {
					$select.val(option_level);
				}
			}
		}
	});
	$('.field[jq-type=access_level] select').change();
	
	/* Datepicker */
	
	$('.field .jq-datetime-datepicker-input').each(function (idx, el) {
		var $field = $(el).parents('.field');
		var name = $field.attr('jq-name');
		var type = $field.attr('jq-type');
		var $year_select = $('#f-' + name + '_year');
		var min_year = $year_select.find('option[value!=""]:first').attr('value');
		var max_year = $year_select.find('option:last').attr('value');
		function get_current_value() {
			var y = $('#f-' + name + '_year').val();
			var m = $('#f-' + name + '_month').val();
			var d = $('#f-' + name + '_day').val();
			var value = y && m && d ? (y + '-' + m + '-' + d) : '';
			return value;
		}
		$('#f-' + name + '-datepicker').val(get_current_value());
		$('#f-' + name + '-datepicker').datepicker({
			showOn: "button",
			buttonImage: global.main_prefix + "img/calendar.png",
			buttonImageOnly: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: min_year + ':' + max_year,
			minDate: min_year + '-01-01',
			maxDate: max_year + '-12-31'
		});
		$('#f-' + name + '-datepicker').change(function () {
			$('#f-' + name + '_year').val(this.value.substr(0, 4));
			$('#f-' + name + '_month').val(this.value.substr(5, 2));
			$('#f-' + name + '_day').val(this.value.substr(8, 2));
		});
		$('#f-' + name + '_year' + ',' + '#f-' + name + '_month' + ',' + '#f-' + name + '_day').change(function () {
			$('#f-' + name + '-datepicker').datepicker("setDate" , get_current_value());
		});
	});
	
	/* Datepicker Enabler */
	
	$('.field .jq-datetime-enabler-link').click(function () {
		var $enabler = $(this).closest('.jq-datetime-enabler');
		var $field = $enabler.closest('.field');
		var $content = $field.find('.jq-datetime-field-content');
		var name = $field.attr('jq-name');
		$enabler.hide();
		$content.show();
		if ($field.attr('jq-type') == 'datetime') {
			var later_than_name = $field.attr('jq-later-than');
			if (later_than_name) {
				var value = $('#f-' + later_than_name + '-datepicker').val();
				$('#f-' + name + '-datepicker').val(value);
				$('#f-' + name + '-datepicker').change();
				$('#f-' + name + '-datepicker').datepicker("setDate" , value);
				$('#f-' + name + '_hours').val($('#f-' + later_than_name + '_hours').val());
				$('#f-' + name + '_minutes').val($('#f-' + later_than_name + '_minutes').val());
			}
		}
		$('#f-' + name + '_hide').val('0');
	});
	
	$('.field .jq-datetime-disabler-link').click(function () {
		var $disabler = $(this).closest('.jq-datetime-disabler');
		var $field = $disabler.closest('.field');
		var $enabler = $field.find('.jq-datetime-enabler');
		var $content = $field.find('.jq-datetime-field-content');
		var name = $field.attr('jq-name');
		$enabler.show();
		$content.hide();
		$('#f-' + name + '_hide').val('1');
	});
	
	$('.field[jq-type=datetime] .jq-datetime-cleaner-link').each(function() {
		var $this = $(this);
		var $cleaner = $this.closest('.jq-datetime-cleaner');
		var $field = $this.closest('.field');
		var $datepicker_input = $field.find('.jq-datetime-datepicker-input');
		var $selects = $field.find('select');
		$this.click(function() {
			$selects.val('');
			$datepicker_input.val('');
			$cleaner.hide();
		});
		function toggle_cleaner() {
			var blank = true;
			$selects.each(function() {
				blank = blank && $(this).val() == '';
			});
			$cleaner.toggle(!blank);
		};
		$selects.change(toggle_cleaner);
		$datepicker_input.change(toggle_cleaner);
	});

	/* WYSWYG */
	
	if ($.fn.tinymce) {
		var tinymce_config_full = {
			script_url: global.local_prefix + 'editors/tiny_mce/tiny_mce.js',
			theme: 'modern',
			language: 'en',
			content_css: global.prefix + 'css/editor.css?v3',
			plugins: 'anchor,code,autolink,charmap,fullscreen,image,link,lists,media,nonbreaking,paste,table,textcolor',
			menubar: 'edit insert view format table tools',
			toolbar: 'bold italic | bullist numlist | link unlink image media',
			fix_list_elements : true,
			convert_fonts_to_spans: true,
			relative_urls: false,
			remove_script_host: true,
			force_hex_style_colors: true,
			resize: 'both',
			style_formats: [
				{title: 'Header', block: 'h2'},
				{title: 'Subheader', block: 'h3'}
			]
		}
		var tinymce_config_full_mobile = $.extend(true, {}, tinymce_config_full);
		var tinymce_config_short = $.extend(true, {}, tinymce_config_full);
		var tinymce_config_full_cut = $.extend(true, {}, tinymce_config_full);
		var tinymce_config_full_mobile_cut = $.extend(true, {}, tinymce_config_full_mobile);
		var tinymce_config_short_cut = $.extend(true, {}, tinymce_config_short);
		function tinymce($textarea, config) {
			var body_class = $textarea.attr('data-body-class');
			var final_config = $.extend(true, {}, config);
			if (body_class) {
				final_config.body_class = body_class;
			}
			$textarea.tinymce(final_config);
		}
		tinymce($('.xhtml-editor-full'), tinymce_config_full);
		tinymce($('.xhtml-editor-full-cut'), tinymce_config_full_cut);
		tinymce($('.xhtml-editor-full-mobile'), tinymce_config_full_mobile);
		tinymce($('.xhtml-editor-full-mobile-cut'), tinymce_config_full_mobile_cut);
		tinymce($('.xhtml-editor-short'), tinymce_config_short);
		tinymce($('.xhtml-editor-short-cut'), tinymce_config_short_cut);
	}
	
	/* Multi link */
	
	function multi_link_change(element, only_check) {
		var ancestors = $(element).attr('jq-ancestors').split(',');
		var descendants = $(element).attr('jq-descendants').split(',');

		if (element.checked) {
			for (var key in ancestors) {
				if (ancestors[key]){
					$('#f-' + $(element).attr('jq-dtf-name') + '-' + ancestors[key]).attr('checked', 'checked');
				}
			}
		}

		if (!element.checked && !only_check) {
			for (var key in descendants) {
				if (descendants[key]) {
					$('#f-' + $(element).attr('jq-dtf-name') + '-' + descendants[key]).removeAttr('checked');
				}
			}
		}

		return true;
	}

	$('.jq-dtf-multi-link-item').each(function() {
		$(this).change(function() {
			multi_link_change(this, false);
		})
		multi_link_change(this, true);
	});
	
	/* Multiselect */
	
	if ($('.jq-dtf-multi-link-chosen').length) {
		$('.jq-dtf-multi-link-chosen').chosen();
	}

	/* Intmarkup upload */

	$('.js_intmarkup_upload').each(function(idx, el) {
		var $upload = $(el);
		var $input = $upload.find('.js_intmarkup_upload_files_input');
		var $button = $upload.find('.js-fileapi-wrapper');
		var $files = $upload.find('.js_intmarkup_upload_files');
		var $file_template = $upload.find('.js_file_template');
		var $textarea = $upload.closest('.field').find('textarea').first();
		var url_base = $upload.data('url-base');
		var max_file_size = parseInt($upload.data('max-file-size'));
		var max_file_size_human = $upload.data('max-file-size-human');
		var error_message_file_size = $upload.find('.js_error_message_file_size').text();
		var error_message_general = $upload.find('.js_error_message_general').text();
		function update_list() {
			var files = [];
			$files.children(':visible').each(function() {
				files.push($(this).data('file-id'));
			})
			$input.val(files.join(','));
		}
		$files.on('click', '.js_file_remove', function() {
			$file_to_remove = $(this).closest('.js_file');
			$file_to_remove.fadeOut(function() {
				$file_to_remove.remove();
				update_list();
			});
		});
		$files.on('click', '.js_file_insert_name', function() {
			var $a = $(this).closest('.js_file').find('a').first();
			var cursorPos = $textarea.prop('selectionStart');
			var val = $textarea.val();
			var before = val.substring(0, cursorPos);
			var after = val.substring(cursorPos, val.length);
			var link = $a.attr('href') + '##image';
			var new_val = before 
				+ (before.length && !before.match(/\s$/) ? (!after.length || after.match(/^\s/) ? '\r\n' : ' ') : '') 
				+ link 
				+ (after.length && !after.match(/^\s/) ? ' ' : '') 
				+ after;
			$textarea.val(new_val);
			//$textarea.val($textarea.val().replace(/[\r\n]+$/, '') + ($textarea.val() ? '\n' : '') + $a.attr('href') + '##image');
		});
		$files.sortable({
			//handle : ''
			update : update_list
		});
		$button.fileapi({
			url: global.ajax_prefix + 'intmarkup_upload_file/',
			multiple: true,
			maxSize: max_file_size,
			autoUpload: true,
			duplicate: true,
			//data: post_data,
			elements: {
				size: '.js_size',
				active: {show: '.js_upload', hide: '.js_browse'},
				progress: '.js_progress'
			},
			onSelect: function (evt, data){
				if (data.other.length) {
					for (var i in data.other)
					{
						var errors = data.other[i].errors;
						if (errors) {
							if (errors.maxSize) {
								alert(error_message_file_size.replace(/%FILE_NAME%/, data.other[i].name));
							}
						}
					}
				}
			},
			onFileComplete: function (evt, uiEvt){
				var error = uiEvt.error;
				var result = uiEvt.result;
				if (error) {
					alert(error_message_general.replace(/%FILE_NAME%/, uiEvt.file.name).replace(/%CODE%/, -1000) + ' Message: ' + error + '.');
					return;
				}
				if (result.error_code)
				{
					alert(error_message_general.replace(/%FILE_NAME%/, uiEvt.file.name).replace(/%CODE%/, result.error_code))
					return;
				}
				$file = $file_template.clone().appendTo($files).show();
				$file.find('.js_file_template_file_name').text(result.file_name);
				$file.find('.js_file_template_size').text(result.size_human);
				if (!result.is_image) {
					$file.find('.js_file_insert_name').remove();
				}
				var $a = $file.find('a');
				$a.attr('href', $a.attr('href') + result.file_id + '/');
				$file.data('file-id', result.file_id);
				update_list();
			},
			onFilePrepare: function (evt, uiEvt){
				uiEvt.options.data.folder = 'q';
			}
		});
	});
	
	/* Link dependencies */

	$('.field[jq-type="foreign_key"]').filter('[data-dependency-data]').each(function(idx, el) {
		var $field = $(this);
		var data = $field.data('dependency-data');
		var strategy = $field.data('dependency-strategy');
		var name = $field.attr('jq-name');
		var $select = $('#f-' + name);
		function change() {
			var values = [], foreign_name, foreign_select, foreign_value, allowed, i, values_new;
			var $options = $select.find('option[value!=""]');
			$options.each(function() {
				values.push($(this).val());
			});
			for (foreign_name in data) {
				$foreign_select = $('#f-' + foreign_name);
				foreign_value = $foreign_select.val();
				allowed = data[foreign_name][foreign_value];
				values_new = [];
				for (i in values) {
					if ($.inArray(values[i], allowed) > -1) {
						values_new.push(values[i]);
					}
				}
				values = values_new;
			}
			$options.each(function() {
				var value = $(this).val();
				if (strategy === "DISABLE") {
					if ($.inArray(value, values) > -1) {
						$(this).removeAttr('disabled');
					} else {
						$(this).attr('disabled', 'disabled');
					}
				} else if (strategy === "HIDE") {
					var show = $.inArray(value, values) > -1;
					$(this).toggle(show);
					if (show) {
						if ($(this).parent('span.toggle_option').length) {
							$(this).unwrap();
						}
					} else {
						if ($(this).parent('span.toggle_option').length == 0) {
							$(this).detach().appendTo($select).wrap('<span class="toggle_option" style="display: none;" />');
						}
					}

				}
			});
			$field.toggleClass('preload', !values.length && strategy === "HIDE");
		}
		for (var foreign_name in data) {
			var $foreign_select = $('#f-' + foreign_name);
			$foreign_select.change(change);
		}
		change();
	});
});