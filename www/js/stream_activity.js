$(function() {
	// 'Apply' buttons
	$('.js_role').not('.JS_PROCESSED').on('click', '.js_btn', function() {
		$(this).addClass('JS_PROCESSED');
		var $btn = $(this);
		var action = $btn.data('btn-action');
		var $role = $btn.closest('.js_role');
		var data = $role.data();
		data.action = action;
		xpost({
			ajax_ctrl_name : 'stream_activity_request',
			data: data,
			success_callback : function(result, options) {
				var status = result.new_status;
				var $status = $('.js_status_' + status).children().first().children().clone();
				var $role = $('.js_role').filter('[data-role_id="' + data.role_id + '"]');
				$role.find('.js_activity_status_controls').html($status);
			},
			error_callback : function(result, options) {
				//dd(result);
			}, 
		});
	});

	// Submenu
	var block_default = 'descr';
	$('.js_submenu_item').click(function(e, no_push) {
		$('.js_submenu_item').removeClass('_item__current');
		$('.js_submenu_item').addClass('_item__clickable');
		$(this).removeClass('_item__clickable');
		$(this).addClass('_item__current');
		var block = $(this).data('block');
		$('.js_block').hide();
		$('#js_block_' + block).show();
		if (!no_push) {
			dynurls.push_hash(block != block_default ? 'block=' + block : '');
		}
	});

	// Dunurls
	var dynurls = new dynurls_class({}, function(state) {
		var params = dynurls.parse_hash(state.hash);
		var block = params.block ? params.block : block_default;
		$('#js_submenu_item_' + block).trigger('click', true);
	}, true);
});