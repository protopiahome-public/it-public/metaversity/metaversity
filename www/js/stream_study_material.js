$(function() {
	function markup_change() {
		$('.js_button_menu_link').not('.JS_PROCESSED').click(function() {
			$(this).addClass('JS_PROCESSED');
			var header = $(this).data('header');
			if (header) {
				var $header = $('.js_header_' + header);
				if ($header.length) {
					$('.js_content').get(0).scrollTop += $header.offset().top;
					if (!$('.js_content').get(0).scrollTop) {
						$(window).scrollTop($header.offset().top);
					}
				}
			}
		});

		$('.js_change_status').not('.JS_PROCESSED').on('click', '.js_btn', function() {
			$(this).addClass('JS_PROCESSED');
			var $btn = $(this);
			var new_status = $btn.data('btn-status');
			var $manage = $btn.closest('.js_change_status');
			var data = $manage.data();
			data.new_status = new_status;
			xpost({
				ajax_ctrl_name : 'stream_study_material_status',
				data: data,
				success_callback : function(result, options) {
					var status = result.new_status;
					var $status = $('.js_status_' + status).children().clone();
					$manage.html($status);
				},
				error_callback : function(result, options) {
					//dd(result);
				}, 
			});
		});
	}

	markup_change();
	$(document).bind('markup_change', {}, markup_change);
});