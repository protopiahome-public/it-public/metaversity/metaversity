var dynurls_class = function(initial_state, on_url_change, trigger_urL_change) {
	if (typeof(history.pushState) != "function") {
		$.extend(this, dynurls_class_fallback_prototype);
	}
	this.state = initial_state;
	this.on_url_change = on_url_change;
	this._init(trigger_urL_change);
}

var dynurls_class_fallback_prototype = {
	state: null,
	on_url_change: function() {},
	initial_url: null,
	_init: function() {
		this.state.url = this._get_url(location.href);
		this.state.hash = this._normalize_hash(location.hash);
		this.initial_url = this.state.url;
		this._update_href();
		this._init_events();
	},
	push_url: function(state, url, hash) {},
	push_hash: function(hash, replace) {},
	replace_state: function(state) {},
	_init_events: function() {}
}

dynurls_class.prototype = {
	state: null,
	on_url_change: function() {},
	push_url: function(state, url, hash) {
		this.state = state;
		this.state.url = url;
		this.state.hash = this._normalize_hash(hash);
		this._update_href();
		this._push_state();
	},
	push_hash: function(hash, replace) {
		hash = this._normalize_hash(hash);
		if (this.state.hash == hash) {
			return;
		}
		this.state.hash = hash;
		this._update_href();
		this._push_state(replace);
	},
	replace_state: function(state) {
		state.url = this.state.url;
		state.hash = this.state.hash;
		this.state = state;
		this._update_href();
		this._push_state(true);
	},
	change_page_properties: function($prop_el) {
		var url = $prop_el.data('url');
		$('meta[property="og:url"]').attr('content', url);
		$('link[rel="canonical"]').attr('href', url);
		
		$(window).trigger('urlChanged');
		
		var title = $prop_el.data('title');
		$('meta[property="og:title"]').attr('content', title);
		$('meta[name="title"]').attr('content', title);
		document.title = title;
		//$('title').text(title); // IE8 does not support it on current jquery version
		
		var description = $prop_el.data('description');
		$('meta[property="og:description"]').attr('content', description);
		$('meta[name="description"]').attr('content', description);
	},
	parse_params: function(url) {
		var question_idx = url.indexOf('?');
		if (question_idx === -1) {
			return [];
		}
		url = url.substr(question_idx + 1);
		var hash_idx = url.indexOf('#');
		if (hash_idx !== -1) {
			url = url.substr(0, hash_idx);
		}
		return this._parse_str(url);
	},
	build_url: function(url_base, remove_params, params) {
		var url = url_base === null ? location.href : url_base;
		if (remove_params) {
			var question_idx = url.indexOf('?');
			if (question_idx != -1) {
				url = url.substr(0, question_idx);
			}
		}
		var concat_char = '&';
		if (url.slice(-1) == '?') {
			concat_char = '';
		}
		else if (url.indexOf('?') == -1) {
			concat_char = '?';
		}
		else if (url.slice(-1) == '&') {
			concat_char = ''
		}
		for (var name in params) {
			if (params[name] !== null) {
				url += concat_char;
				url += encodeURIComponent(name);
				if (params[name] !== true) {
					url += '=';
					url += encodeURIComponent(params[name]);
				}
				concat_char = '&';
			}
		}
		return url;
	},
	parse_hash: function(hash) {
		if (typeof(hash) == 'undefined') {
			hash = location.hash;
		}
		hash = this._normalize_hash(hash);
		hash = hash.replace(/^!\//, '');
		return this._parse_str(hash);
	},
	_parse_str: function(str) {
		var parts = str.split('&');
		var parsed = [];
		var kv;
		for (var i in parts) {
			if (!parts[i]) continue;
			kv = decodeURIComponent(parts[i]).split('=', 2);
			if (kv.length == 2) {
				parsed[kv[0]] = kv[1];
			} else if (kv.length == 1) {
				parsed[kv[0]] = true;
			}
		}
		return parsed;
	},
	_init: function(trigger_urL_change) {
		var th = this;
		this.state.url = this._get_url(location.href);
		this.state.hash = this._normalize_hash(location.hash);
		this._update_href();
		this._init_events();
		this._push_state(true);
		if (trigger_urL_change) {
			setTimeout(function() {
				th.on_url_change(th.state);
			}, 0);
		}
	},
	_init_events: function() {
		var th = this;
		$(window).on('popstate', function(e) {
			var state = e.originalEvent.state;
			if (state) {
				th.state = state;
				th.on_url_change(th.state);
			}
		});
		$(window).on('hashchange', function() {
			var new_hash = th._normalize_hash(location.hash);
			if (th.state.hash != new_hash) {
				th.state.hash = new_hash;
				th._update_href();
				th._push_state(true);
				th.on_url_change(th.state);
			}
		});
	},
	_push_state: function(replace) {
		if (replace) {
			history.replaceState(this.state, null, this.state.href);
		} else {
			history.pushState(this.state, null, this.state.href);
		}
	},
	_normalize_hash: function(hash) {
		return hash ? hash.replace(/^#/, '') : '';
	},
	_update_href: function() {
		this.state.href = this.state.url + (this.state.hash ? '#' + this.state.hash : '');
	},
	_get_url: function(href) {
		var url = href;
		if (url.indexOf('#') != -1) {
			url = url.split('#')[0];
		}
		return url;
	}
}