/*
Lightbox

// Usage example:
$(function () {
	xlightbox.show({
		title: 'Some crazy box',
		content: '<p>Content</p>',
		button_title: 'Save this',
		show_cancel_button: true,
		width: '200px',
		red: false, // true for delete dialogs
		success: function(options) {
			dd(options);
			return true; // 'true' closes the dialog
		},
		close: function () {
			tinymce.remove();
		}
	});
});

*/

window.xlightbox = function() {}
$.extend(window.xlightbox, {
	options_default: {
		content: '',
		title: '',
		button_title: '',
		show_cancel_button: true,
		width: '400px',
		red: false,
		top_min_offset: 45,
		success: null,
		close: null
	},
	submit_disabled: false,
	options: {},
	is_shown: false,
	$content_parent: null,
	body_scroll_left: null,
	body_scroll_top: null,
	show: function(options) {
		if (this.is_shown) {
			this._do_hide();
		}
		this.options = $.extend({}, this.options_default, options);
		this._init_elements();
		this._init_events();
		this._init_window();
		this.button_enable();
		this._show_window();
		this._init_keyboard();
		this.is_shown = true;
	},
	hide: function() {
		this._do_hide();
	},
	button_disable: function() {
		this.$button.addClass('button__disabled');
		this.submit_disabled = true;
	},
	button_enable: function() {
		this.$button.removeClass('button__disabled');
		this.submit_disabled = false;
	},
	set_success: function(callback) {
		this.options.success = callback;
	},
	set_close: function(callback) {
		this.options.close = callback;
	},
	_init_elements: function () {
		this.$window = $(window);
		this.$lightbox = $('#js_lightbox');
		this.$dialog_box = $('#js_lightbox_dialog_box');
		this.$dialog = $('#js_lightbox_dialog');
		this.$title = $('#js_lightbox_title');
		this.$close = $('#js_lightbox_close');
		this.$content = $('#js_lightbox_content');
		this.$button_box = $('#js_lightbox_button_box');
		this.$button = $('#js_lightbox_button');
		this.$button_cancel = $('#js_lightbox_button_cancel');
		this.$lightbox_outer = $('.lightbox_outer');
	},
	_init_events: function () {
		var th = this;
		this.$button.unbind('click').bind('click', function(e) {
			if (th.submit_disabled) {
				return false;
			}
			th.button_disable();
			return th._do_submit.apply(th, arguments);
		});
		this.$close.unbind('click').bind('click', function(e) {
			return th._do_hide.apply(th, arguments);
		});
		this.$button_cancel.unbind('click').bind('click', function(e) {
			return th._do_hide.apply(th, arguments);
		});
	},
	_init_window: function () {

	},
	_show_window: function () {
		var W = this.$window.width();
		var H = this.$window.height();
		var w = parseInt(this.options.width);
		var $content = $(this.options.content);
		var inline = w >= W;
		this.$lightbox.toggleClass('lightbox__inline', inline);
		if (inline) {
			this.$dialog.css('width', '100%');
			this.body_scroll_left = this.$window.scrollLeft();
			this.body_scroll_top = this.$window.scrollTop();
			this.$lightbox_outer.hide();
		}
		else {
			this.$dialog.css('width', this.options.width);
			this.body_scroll_left = -1;
			this.body_scroll_top = -1;
		}
		this.$lightbox.toggleClass('lightbox__red', this.options.red);
		this.$button.toggleClass('ctrl__button__red', this.options.red);
		this.$button_cancel.toggle(this.options.show_cancel_button);
		if (!this.options.title) {
			this.options.title = $content.attr('data-title');
		}
		if (this.options.title) {
			this.$title.text(this.options.title);
		}
		this.$content_parent = null;
		if (this.options.content.jquery) {
			this.$content_parent = this.options.content;
			this.$content.empty();
			this.$content.append(this.options.content.children());
		}
		else {
			this.$content.html($content);
		}
		if (!this.options.button_title) {
			this.options.button_title = $content.attr('data-button-title');
		}
		if (this.options.button_title) {
			this.$button_box.show();
			this.$button.text(this.options.button_title);
		} 
		else {
			this.$button_box.hide();
		}
		var $autoheight_blocks = this.$content.find('.js_lightbox_autoheight');
		this.$content.toggleClass('lightbox_content__autoheight', $autoheight_blocks.length > 0)
		this.$window.trigger('markuprefresh');
		this.$lightbox.show();
		if (inline) {
			this.$window.scrollLeft(0);
			this.$window.scrollTop(0);
		}
		else {
			var top = Math.round((H - 40 - this.$dialog_box.height()) / 2);
			this.$dialog_box.css('top', (top > this.options.top_min_offset ? top : this.options.top_min_offset) + 'px');
			if (top < this.options.top_min_offset) {
				if ($autoheight_blocks.length) {
					var diff = this.$dialog_box.height() - $autoheight_blocks.height();
					var h = H - diff - 2 * this.options.top_min_offset;
					$autoheight_blocks.height(h > 80 ? h : 80);
				}
			}
			if (H - this.$dialog_box.position().top - this.$dialog_box.height() < this.options.top_min_offset) {
				this.$lightbox.toggleClass('lightbox__inline', true);
				this.body_scroll_left = this.$window.scrollLeft();
				this.body_scroll_top = this.$window.scrollTop();
				this.$lightbox_outer.hide();
				inline = true;
			}
		}
	},
	_init_keyboard: function () {
		var th = this;
		$('input:text, select', this.$dialog).unbind('keydown.xlightbox').bind('keydown.xlightbox', function(e) {
			if (e.keyCode == 13 || e.keyCode == 10) { // Enter
				th._do_submit.apply(th, arguments);
				return false;
			}
		});
		$(document).unbind('keydown.xlightbox').bind('keydown.xlightbox', function(e) {
			if (e.keyCode == 27) { // Escape
				th._do_hide();
				return true;
			}
		});
	},
	_do_submit: function() {
		var ok = true;
		if (this.options.success) {
			ok = this.options.success(this.options);
		}
		if (ok) {
			this._do_hide();
		}
		else {
			this.button_enable();
		}
	},
	_do_hide: function() {
		$(document).unbind('keydown');
		if (this.options.close) {
			this.options.close();
		}
		this.$lightbox.hide();
		if (this.$content_parent) {
			this.$content_parent.append(this.$content.children());
		}
		this.$lightbox_outer.show();
		if (this.body_scroll_left >= 0) {
			this.$window.scrollLeft(this.body_scroll_left);
		}
		if (this.body_scroll_top >= 0) {
			this.$window.scrollTop(this.body_scroll_top);
		}
		this.is_shown = false;
	}
});