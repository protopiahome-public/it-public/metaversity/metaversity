$(function () {
	window.show_error = function (content, title) {
		var _content = content || '#error_general';
		var _title = title;
		if (_content.match(/^[.#]/)) {
			_content = $(_content);
			_title = _title || _content.data('title');
		}
		_title = _title || global.trans['Error'];
		xlightbox.show({
			content: _content,
			title: _title,
			show_cancel_button: false,
			red: true,
			button_title: 'OK'
		});
	};
	window.show_error_ajax = function (content, title) {
		window.show_error(content || '#error_ajax', title);
	};
	window.show_error_access_denied = function (content, title) {
		window.show_error(content || '#error_access_denied', title);
	};

	// Emails - decoding
	$('a.email').each(function (idx, el) {
		$(this).text($(this).text().replace(/\ssobaka\s/g, '@').replace(/\stochka\s/g, '.'));
		$(this).attr('href', $(this).attr('href').replace(/%20sobaka%20/g, '@').replace(/%20tochka%20/g, '.'));
	});

	// Competence tree expanders
	$('.js_tree_group_head').click(function () {
		var group_id = $(this).data('group-id');
		$('.js_tree_group_' + group_id + '_content').toggle();
	});
	$('#tree-expand').click(function () {
		$('.js_tree_group_content').show();
	});
	$('#tree-collapse').click(function () {
		$('.js_tree_group_content').hide();
	});
	$('#tree-groups').click(function () {
		$('.js_tree_group_content').hide();
		$('.js_tree_group_content').parents('.js_tree_group_content').show();
	});

	// Checkbox bulk checker
	$('.js_check_all').each(function (idx, el) {
		var $el = $(el);
		var children_class = $el.attr('data-check-all-class');
		if (children_class) {
			var children_selector = '.' + children_class;
			var $children = $(children_selector);
			$el.click(function () {
				$children.prop('checked', $el.is(':checked'));
			});
			$children.click(function () {
				var checked_count = $(children_selector + ':checked').length;
				var all_checked = checked_count == $children.length;
				$el.prop('checked', all_checked);
			});
		}
	});
});
// After fonts and images are loaded...
$(window).load(function () {
	$('.js_menu_left_user_adjust').imAdjustText({
		fitTo: '.menu_item',
		strategy: [{
				strategy: 'decreaseFontSize',
				minValue: 12
			}, 'cutText'],
		reapplyIfZeroWidth: true
	});
	$('.js_menu_top_user_adjust').imAdjustText({
		fitToWidth: 160,
		strategy: [{
				strategy: 'decreaseFontSize',
				minValue: 14
			}, 'cutText'],
		reapplyIfZeroWidth: true
	});
});