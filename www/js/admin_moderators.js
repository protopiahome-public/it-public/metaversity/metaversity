$(function() {
	$('.js_btn_delete').click(function () {
		var $form_el = $('#js_form_delete_' + $(this).attr('data-user-id'));
		$('.js_dialog_confirm_login').text($(this).attr('data-user-login'));
		xlightbox.show({
			content: $('.js_dialog_confirm'),
			show_cancel_button: true,
			red: true,
			success: function(options) {
				$form_el.submit();
				return true;
			}
		});
	});
});