$(function() {
	function sync() {
		var data = {
			order: $('.js_sort').sortable('serialize')
		}
		
		xpost({
			ajax_ctrl_name : sort_ajax_ctrl,
			data : data, 
			success_callback :function(r) {}
		});
	}
	
	$('.js_sort').sortable({
		handle : '.js_sort_handle',
		update : sync
	});
});