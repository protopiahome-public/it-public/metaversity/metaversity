new function($) {
	$.fn.adjust = function(settings) {
		this.each(function(idx, el) {
			var $el = $(el);
			if (!$el.children().length) return;
			$el.css('overflow', 'hidden');
			var scroll_height_limit = $el.children().eq(0).height() * 2 - 1;
			function ok() {
				return el.scrollHeight <= scroll_height_limit;
			}
			function init(settings) {
				function init_el(settings) {
					settings.val = settings.init;
					settings.suffix = settings.suffix || 'px';
					$el.find(settings.el).css(settings.css, settings.val + settings.suffix);
				}
				for (var sidx in settings) {
					if (settings[sidx].el) {
						init_el(settings[sidx]);
					} else if (settings[sidx].set) {
						init(settings[sidx].set);
					} else if (settings[sidx].rotate) {
						init(settings[sidx].rotate);
					}
				}
			}
			function recalc() {
				function decrease(settings) {
					var s = settings.set ? settings.set : [settings];
					var sidx;
					for (sidx in s) {
						if (s[sidx].val <= s[sidx].min) {
							return false;
						}
					}
					for (sidx in s) {
						--s[sidx].val;
						$el.find(s[sidx].el).css(s[sidx].css, s[sidx].val + s[sidx].suffix);
					}
					return true;
				}
				for (var sidx in settings) {
					if (settings[sidx].rotate) {
						var can_continue = true;
						while (!ok() && can_continue) {
							can_continue = false;
							for (var rsidx in settings[sidx].rotate) {
								can_continue = decrease(settings[sidx].rotate[rsidx]) || can_continue;
							}
						}
					} else {
						while (!ok() && decrease(settings[sidx]));
					}
				}
				
			}
			init(settings);
			recalc(settings);
			if (!$.browser.msie) { // @todo check IE9/10
				$(window).resize(function () {
					init(settings);
					recalc(settings);
				});
			}
		});
	};
}(jQuery);