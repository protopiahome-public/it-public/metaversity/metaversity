$(function () {
	$('#jq-dt-elements-generate').click(function () {
		var $dtf = $('#jq-dtf').val();
		var $code = $('#jq-dtf-code-' + $dtf);
		var code = $code.val().replace('DTF_NAME', $('#jq-dtf-name').val()).replace('DTF_TITLE', $('#jq-dtf-title').val());
		var $preview = $('#jq-dt-elements-preview');
		$preview.val($preview.val() + code + "\n\n");
	});

	$('#jq-dt-elements-generate-axis').click(function () {
		var $code = $('#jq-axis-code');
		var axis_fields_array = $('#jq-axis-fields').val().split('\n');
		for (var i in axis_fields_array) {
			axis_fields_array[i] = '"' + axis_fields_array[i] + '"';
		}
		var axis_fields = axis_fields_array.join(', ');
		var code = $code.val().replace('AXIS_NAME', $('#jq-axis-name').val()).replace('AXIS_FIELDS', axis_fields);
		var $preview = $('#jq-dt-elements-preview');
		$preview.val($preview.val() + code + "\n\n");
	});
});