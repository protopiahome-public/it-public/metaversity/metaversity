/**
 * @todo escape when renaming does not work
 * @todo moving between nodes does not work (dnd)
 */

function delete_dialog(remove_function) {
	xlightbox.show({
		content: $('#dialog-confirm-delete'),
		red: true,
		success: function (options) {
			remove_function();
			return true;
		}
	});
}

$(function () {
	var selected_node = null;
	function is_nonempty(obj) {
		if (typeof (obj) !== 'object') {
			return false;
		}
		for (var i in obj) {
			return true;
		}
		return false;
	}
	function refresh_restrictions(restrictions) {
		var $jstree = $('#competence_tree').jstree(true);
		for (var i in $jstree._model.data) {
			var node = $jstree._model.data[i];
			var id = node.id;
			var type, key;
			type = node.type;
			if (node.type == 'group') {
				key = 'g';
			}
			else if (node.type == 'competence') {
				key = 'c';
			}
			else {
				continue;
			}
			id = id.replace(/[a-z\_]/g, '');
			var title = node.original.title ? node.original.title : node.text;
			if (restrictions[key][key + id]) {
				node.original.metadata = restrictions[key][key + id];
				if (restrictions[key][key + id].prefix) {
					title = restrictions[key][key + id].prefix + title;
				}
			}
			$jstree.set_text($jstree.get_node(node.id), title);
		}
	}
	$.jstree._themes = global.main_prefix + "lib/jstree/themes/";
	$('#tree_button_add_group').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		var $jstree = $("#competence_tree").jstree(true);
		var obj = $jstree.get_selected()[0];
		$jstree.create_node(obj, {
			"text": trans['New group'],
			"type": "group",
		}, "last");
	});
	$('#tree_button_add_item').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		var $jstree = $("#competence_tree").jstree(true);
		var obj = $jstree.get_selected()[0];
		$jstree.create_node(obj, {
			"text": trans['New competence'],
			"type": "competence",
		}, "last");
	});
	$('#tree_button_edit').click(function () {
		if ($(this).hasClass('link_btn__disabled') || !selected_node) {
			return;
		}
		var $jstree = $("#competence_tree").jstree(true);
		var title = selected_node.original.title;
		if (selected_node.type !== 'group' && selected_node.type !== 'competence') {
			return;
		}
		var operation = selected_node.type === "group" ? "rename_group" : "rename_item";
		var id = selected_node.id.replace(/[a-z_]/g, '');
		var ok = (function adjust_checkboxes() {
			//if (rel !== 'group') {
			//	$('#tree_edit_form_study_levels').hide();
			//	return;
			//}
			if (!$('#tree_edit_form_study_levels').find('input').length) {
				$('#tree_edit_form_study_levels').hide();
				return;
			}
			$('#tree_edit_form_study_levels').show();
			var restrictions = selected_node.original.metadata;
			var parent_restrictions = [];
			var path = $jstree.get_path(selected_node, null, true).reverse();
			if (!path || !path[0] || path[0] !== (selected_node.type === 'group' ? 'group_' : 'competence_') + id) {
				show_error('Unexpected error');
				return false;
			}
			if (path[1] && path[1].match(/^group_\d+$/)) {
				parent_restrictions = $jstree.get_node(path[1]).original.metadata;
			}
			// Reset begin
			$('.jq-tree-edit-form-study-level').prop('checked', false);
			$('.jq-tree-edit-form-study-level').removeAttr('disabled');
			// Reset end
			$('.jq-tree-edit-form-study-level').each(function () {
				var study_level_id = $(this).data('id');
				var study_level_disabled = false;
				if (parent_restrictions && is_nonempty(parent_restrictions.study_levels)) {
					if (!parent_restrictions.study_levels['l' + study_level_id]) {
						study_level_disabled = true;
					}
				}
				if (study_level_disabled) {
					$(this).attr('disabled', 'disabled');
				}
				else {
					if (!restrictions || restrictions.inherited || !is_nonempty(restrictions.study_levels)) {
						// leave unchecked
					}
					else if (restrictions.study_levels['l' + study_level_id]) {
						$(this).prop('checked', true);
					}
				}
			});
		})();
		if (ok === false) {
			return;
		}
		xlightbox.show({
			content: $('#tree_edit_form'),
			width: '480px',
			success: function () {
				var new_title = $.trim($('#tree_edit_form_title').val());
				var study_levels = [];
				$('.jq-tree-edit-form-study-level:checked').each(function () {
					study_levels.push($(this).data('id'));
				});
				var post_data = {
					operation: operation,
					competence_set_id: competence_set_id,
					stream_id: stream_id,
					id: id,
					title: new_title,
					study_levels: study_levels.join(',')
				};
				xpost({
					ajax_ctrl_name: competences_ajax_ctrl,
					data: post_data,
					success_callback: function (result, options) {
						selected_node.original.title = result.title;
						$jstree.set_text(selected_node, (selected_node.original.prefix ? selected_node.original.prefix : '') + result.title);
						if (is_nonempty(result.restrictions)) {
							refresh_restrictions(result.restrictions);
						}
						xlightbox.hide();
					},
					error_callback: function(result, options) {
						$('#competence_tree').jstree('refresh');
						$('.link_btn').addClass('.link_btn__disabled');
					}
				});
				return false;
			}
		});
		$('#tree_edit_form_title').val(title);
	});
	$('#tree_button_delete').click(function () {
		if ($(this).hasClass('link_btn__disabled')) {
			return;
		}
		var $jstree = $("#competence_tree").jstree(true);
		var obj = $jstree.get_selected()[0];
		delete_dialog(function () {
			$jstree.delete_node(obj);
		});
	});
	$("#competence_tree").jstree({
		// List of active plugins
		"plugins": ["state", "themes", "massload", "dnd", "search", "types", "contextmenu", "conditionalselect"],
		"dnd": {
			"copy_modifier": false
		},
		"massload": {
			"url": competences_ajax_url,
			// the `data` function is executed in the instance's scope
			// the parameter is the node being loaded
			// (may be -1, 0, or undefined when loading the root nodes)
			"data": function (n) {
				// the result is fed to the AJAX request `data` option
				return {
					"operation": "get_nodes",
					"competence_set_id": competence_set_id,
					"stream_id": stream_id,
					"id": n.attr ? n.attr("id").replace(/[a-z\_]/g, "") : "-"
				};
			}
		},
		"types": {
			"#": {
				"valid_children": ["root"],
				"max_children": 1
			},
			"root": {
				"valid_children": ["group"],
			},
			// The default type
			"competence": {
				// I want this type to have no children (so only leaf nodes)
				// In my case - those are files
				"valid_children": [],
				// If we specify an icon for the default type it WILL OVERRIDE the theme icons
				"icon": global.main_prefix + "lib/jstree/file.png"
			},
			// The `folder` type
			"group": {
				"valid_children": ["competence", "group"]
			}
		},
		// the core plugin - not many options here
		"core": {
			"data": {
				"url": competences_ajax_url,
				// the `data` function is executed in the instance's scope
				// the parameter is the node being loaded
				// (may be -1, 0, or undefined when loading the root nodes)
				"data": function (n) {
					// the result is fed to the AJAX request `data` option
					return {
						"operation": "get_nodes",
						"competence_set_id": competence_set_id,
						"stream_id": stream_id,
						"id": n.attr ? n.attr("id").replace(/[a-z\_]/g, "") : "-"
					};
				}
			},
			check_callback: function (operation, node, node_parent, node_position, more) {
				return operation != 'rename_node' && operation != 'copy_node';
			}
		},
		"conditionalselect": function (node, event) {
			$("#competence_tree").jstree(true).deselect_all();
			$("#competence_tree").jstree(true).select_node(node, true);
			return true;
		},
		"contextmenu": {
			"items": function (node) {
				var $jstree = $("#competence_tree").jstree(true);
				var can_create_group = true;
				var can_create_item = true;

				if (node.type == "root") {
					can_create_item = false;
					can_create_group = true;
				} else if (node.type == "competence") {
					can_create_item = false;
					can_create_group = false;
				} else {
					for (var i in node.children) {
						if ($jstree.get_node(node.children[i]).type == "group") {
							can_create_item = false;
						}
						if ($jstree.get_node(node.children[i]).type == "competence") {
							can_create_group = false;
						}
					}
				}
				return {
					"create_item": {
						"label": trans['Add a competence'],
						"action": function (data) {
							var $jstree = $.jstree.reference(data.reference);
							var obj = $jstree.get_node(data.reference);
							$jstree.create_node(obj, {
								"text": trans['New competence'],
								"type": "competence",
							}, "last");
						},
						"_disabled": !can_create_item
					},
					"create_group": {
						"label": trans['Add a group'],
						"action": function (data) {
							var $jstree = $.jstree.reference(data.reference);
							var obj = $jstree.get_node(data.reference);
							$jstree.create_node(obj, {
								"text": trans['New group'],
								"type": "group",
							}, "last");
						},
						"_disabled": !can_create_group
					},
					"rename": {
						"label": trans['Edit'],
						"action": function (obj) {
							//this.rename(obj);
							$('#tree_button_edit span').click();
						},
						"_disabled": node.type == "root"
					},
					"delete": {
						"label": trans['Delete'],
						"action": function (data) {
							var $jstree = $.jstree.reference(data.reference);
							var obj = $jstree.get_node(data.reference);
							delete_dialog(function () {
								$jstree.delete_node(obj);
							});
						},
						"_disabled": node.type == "root"
					}
				}
			}
		}
	})
	.bind("create_node.jstree", function (e, data) {
		var operation = "";
		if (data.node.type == "group") {
			operation = "create_group";
		}
		if (data.node.type == "competence") {
			operation = "create_item";
		}
		var post_data = {
			"operation": operation,
			"competence_set_id": competence_set_id,
			"stream_id": stream_id,
			"group_id": data.parent.replace(/[a-z\_]/g, ""),
			"position": data.node.position,
			"title": data.node.text,
			"type": data.node.type
		};
		xpost({
			ajax_ctrl_name: competences_ajax_ctrl,
			data: post_data,
			success_callback: function (result, options) {
				data.instance.set_id(data.node, data.node.type + '_' + result.id);
				data.instance.set_text(data.node, result.title);
				data.node.original.title = result.title;
				data.instance.deselect_all();
				data.instance.open_node(data.parent);
				data.instance.select_node(data.node);
				//data.instance.edit(data.node);
			},
			error_callback: function(result, options) {
				$('#competence_tree').jstree('refresh');
				$('.link_btn').addClass('.link_btn__disabled');
			}
		});
	})
	.bind("delete_node.jstree", function (e, data) {
		if (data.node.type == "group") {
			var operation = "delete_group";
		}
		if (data.node.type == "competence") {
			var operation = "delete_item";
		}
		var post_data = {
			"operation": operation,
			"competence_set_id": competence_set_id,
			"stream_id": stream_id,
			"id": data.node.id.replace(/[a-z\_]/g, "")
		};
		xpost({
			ajax_ctrl_name: competences_ajax_ctrl,
			data: post_data,
			success_callback: function (result, options) {
				data.instance.select_node(data.node.parent);
			},
			error_callback: function(result, options) {
				$('#competence_tree').jstree('refresh');
				$('.link_btn').addClass('.link_btn__disabled');
			}
		});
	})
	.bind("select_node.jstree", function (e, data) {
		var $jstree = $("#competence_tree").jstree(true);
		var can_create_group = true;
		var can_create_item = true;
		var can_delete = true;
		var can_edit = true;

		var node = data.node;
		selected_node = node;
		if (node.type == "root") {
			can_create_item = false;
			can_create_group = true;
			can_edit = false;
			can_delete = false;
		}
		else if (node.type == "competence") {
			can_create_item = false;
			can_create_group = false;
		}
		else {
			for (var i in node.children) {
				if ($jstree.get_node(node.children[i]).type == "group") {
					can_create_item = false;
				}
				if ($jstree.get_node(node.children[i]).type == "competence") {
					can_create_group = false;
				}
			}
		}
		$("#tree_button_add_group").toggleClass("link_btn__disabled", !can_create_group);
		$("#tree_button_add_item").toggleClass("link_btn__disabled", !can_create_item);
		$("#tree_button_edit").toggleClass("link_btn__disabled", !can_edit);
		$("#tree_button_delete").toggleClass("link_btn__disabled", !can_delete);
	})
	.bind("move_node.jstree", function (e, data) {
		var $jstree = $("#competence_tree").jstree(true);
		var parent_node = $jstree.get_node(data.parent);
		var old_parent_node = $jstree.get_node(data.old_parent);
		var position, operation;
		position = data.position;

		var can_create_group = true;
		var can_create_item = true;
		if (parent_node.type == "root") {
			can_create_item = false;
			can_create_group = true;
		} else if (parent_node.type == "competence") {
			can_create_item = false;
			can_create_group = false;
		} else {
			for (var i in parent_node.children) {
				if ($jstree.get_node(parent_node.children[i]).type == "group") {
					can_create_item = false;
				}
				if ($jstree.get_node(parent_node.children[i]).type == "competence") {
					can_create_group = false;
				}
			}
		}

		if (data.node.type == "group") {
			if (!can_create_group) {
				data.instance.refresh();
				return false;
			}

			operation = "move_group";
		}
		if (data.node.type == "competence") {
			if (!can_create_item) {
				data.instance.refresh();
				return false;
			}

			operation = "move_item";
		}

		xpost({
			ajax_ctrl_name: competences_ajax_ctrl,
			data: {
				"operation": operation,
				"competence_set_id": competence_set_id,
				"stream_id": stream_id,
				"id": data.node.id.replace(/[a-z\_]/g, ""),
				"old_group_id": data.old_parent.replace(/[a-z\_]/g, ""),
				"group_id": data.parent.replace(/[a-z\_]/g, ""),
				"position": position,
				"title": data.node.text,
			},
			success_callback: function (result, options) {
				if (is_nonempty(result.restrictions)) {
					refresh_restrictions(result.restrictions);
				}
				data.instance.open_node(parent_node);
				data.instance.select_node(data.node);
			},
			error_callback: function(result, options) {
				$('#competence_tree').jstree('refresh');
				$('.link_btn').addClass('.link_btn__disabled');
			}
		});
	});
});