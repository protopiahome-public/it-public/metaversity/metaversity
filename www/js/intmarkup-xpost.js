/*
 * Example: 
 *	  xpost({
 *	    ajax_ctrl_name : 'test_ctrl',
 *	    query_string : '?a=b',
 *	    data : {name1 : 'value1'}, 
 *	    success_callback : function(result, options){}, 
 *	    check_fail_callback : function(result, options){}, 
 *	    error_callback : function(result, options){}, 
 *	    custom_loading_msg : 'Loading...'
 *	  });
 *  
 * If window.post_vars is defined, it is automatically merged into options.data
 * (with priority of options.data).
 */

$(function() {
	window.xpost = function (options) { 
		var th = window.xpost;
		th._call.apply(th, arguments);
	}
	
	var th = window.xpost;
	$.extend(th, {
		options_default : {
			ajax_ctrl_name : 'ctrl_was_not_set',
			query_string : '',
			data : {}, 
			success_callback : function(r, options) {}, 
			check_fail_callback : function(r, options) {}, 
			error_callback: function(r, options) {}, 
			custom_loading_msg : global.trans && global.trans['Saving...'] ? global.trans['Saving...'] : ''
		},
		_loading_count : 0,
		_loading_show : function(options) {
			if (options.custom_loading_msg === false || options.custom_loading_msg === '') {
				return;
			}
			$('.xpost_loading .js_content').text(options.custom_loading_msg);
			$('.xpost_loading').show();
			++th._loading_count;
		},
		_loading_hide : function(options) {
			if (options.custom_loading_msg === false || options.custom_loading_msg === '') {
				return;
			}
			--th._loading_count;
			if (th._loading_count == 0) {
				$('.xpost_loading').hide();
			}
		},
		_call: function(options) {
			options = $.extend({}, th.options_default, options);
			if (typeof(window.post_vars) != 'undefined') {
				options.data = $.extend({}, window.post_vars, options.data);
			}
			th._loading_show(options);
			$.ajax({
				async: true,
				type: 'POST',
				url: global.ajax_prefix + options.ajax_ctrl_name + '/' + options.query_string,
				data: options.data,
				success: function(r) {
					if (r.status == 'OK') {
						options.success_callback(r, options);
					} else if (r.status == 'BAD_RIGHTS') {
						show_error_access_denied();
						options.error_callback(r, options);
					} else if (r.error_code == 'CHECK') {
						options.check_fail_callback(r, options);
					} else {
						show_error_ajax();
						options.error_callback(r, options);
					}
					th._loading_hide(options);
				},
				error: function(ret) {
					show_error_ajax();
					if (typeof(debug) != 'undefined' && debug) {
						ddin(ret.responseText);
					}
					options.error_callback(ret, options);
					th._loading_hide(options);
				},
				dataType: 'json'
			});
		}
	});
});