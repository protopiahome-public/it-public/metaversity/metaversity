$(function() {
	$('.select').not('.select__no_auto_init').xselect();
	window.rebuild_menu_counts = function(results) {
		if (!results) return;
		var stat = results.stat;
		if (!stat) return;
		process('#submenu_participants_accepted_count', stat.accepted_count);
		process('#submenu_participants_premoderation_count', stat.premoderation_count);
		process('#submenu_mark_count', stat.mark_count);
		process('#submenu_unset_mark_count', stat.unset_mark_count);
		function process(selector, count) {
			$(selector).find('.js_number').text(count);
			$(selector).toggle(count > 0);
		}
	}
	function xselect_change(event, old_value) {
		var $select = $(this);
		var $row = $select.parents('.js_user');
		var $edit = $row.find('.js_mark_comment_edit');
		if ($edit.length) {
			$edit.find('.js_for_mark_3').toggle($select.xselect('get') == 3);
			return; // Already shown
		}
		var $comment = $row.find('.js_comment');
		var $edit_blank = $('.js_mark_comment_edit_blank');
		var old_comment = $comment.text();
		$select.data('old_value', old_value);
		$select.data('old_comment', old_comment);
		$comment.hide();
		var $edit = $edit_blank.clone().removeClass('js_mark_comment_edit_blank').addClass('js_mark_comment_edit');
		$edit.find('.js_for_mark_3').toggle($select.xselect('get') == 3);
		var $textarea = $edit.find('.js_mark_comment_edit_text');
		$textarea.val(old_comment);
		$edit.insertAfter($comment).show();
	}
	window.init_just_added_select = function($new_select) {
		$new_select.xselect();
		$new_select.on('change.xselect', xselect_change);
		$new_select.addClass('js_mark_select');
	}
	$('.js_mark_select').on('change.xselect', xselect_change);
	$('.js_existing_users, .js_search_results').on('click', '.js_mark_comment_edit_save', function() {
		var $row = $(this).parents('.js_user');
		var $select = $row.find('.js_mark_select');
		var $comment = $row.find('.js_comment');
		var $changer = $row.find('.js_changer');
		var $changer_new = $row.find('.js_changer_new');
		var $edit = $row.find('.js_mark_comment_edit');
		var $textarea = $edit.find('.js_mark_comment_edit_text');
		var new_comment = $textarea.val();
		var data = {
			user_id: $select.data('user_id'),
			role_id: $select.data('role_id'),
			mark: $select.xselect('get'),
			comment: new_comment
		};
		if (data.mark == 3 && !$.trim(new_comment)) {
			show_error('.js_mark_comment_edit_error');
		}
		else {
			xpost({
				ajax_ctrl_name: 'stream_admin_activity_participant_mark',
				data: data,
				success_callback: function(results) {
					$comment.text(new_comment);
					$comment.show();
					$changer.remove();
					$changer_new.show();
					$edit.remove();
					rebuild_menu_counts(results);
				}
			});
		}
	});
	$('.js_existing_users, .js_search_results').on('click', '.js_mark_comment_edit_cancel', function() {
		var $row = $(this).parents('.js_user');
		var $select = $row.find('.js_mark_select');
		var $comment = $row.find('.js_comment');
		var $edit = $row.find('.js_mark_comment_edit');
		$select.xselect('set', $select.data('old_value'));
		$comment.show();
		$edit.remove();
	});
});