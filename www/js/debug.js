function dd(e) {
	alert(ddd(e));
}
function ddin(e) {
	function htmlize(str) {
		return str.toString().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
	}
	$('#debug').html('<pre><code>' + htmlize(ddd(e)) + '</code></pre>');
}
function ddd(e, idx, level) {
	var tabs = ''
	if (typeof(level) != 'undefined') {
		for (var i = 1; i < level; i++) {
			tabs += '\t';
		}
	} else {
		level = 1;
	}
	
	var result = tabs;
	
	if (level > 5) {
		result += "Too deep recursion";
		return result;
	}
	
	if (typeof(idx) != 'undefined') {
		result += idx + ' : ';
	}
	
	if (typeof(e) == 'undefined') {
		result += '[Undefined]';
	} else if (typeof(e) == 'array') {
		result += '[]';
	} else if (typeof(e) == 'object') {
		var i;
		var pos = 1;
		result += '{';
		for (i in e) {
			if (pos == 1) {
				result += '\n';
			}
			if (pos <= 20) {
				result += ddd(e[i], i, level + 1) + '\n';
			}
			if (pos == 21) {
				result += tabs + '\t...\n';
			}
			pos++;
		}
		result += (pos > 1 ? tabs : '') + '}';
	} else if (typeof(e) == 'function') {
		result += '[Function]';
	} else {
		result += e.toString();
	}
	return result;
}
