$(function() {
	var $users = $('.js_users').find('.js_user');
	var $just_added_select = $('.js_just_added_select');
	var $users_to_index = $('.js_user'); // some users may not be in $users - but they will still be shown.
	var index = [];
	$users_to_index.each(function(idx, el) {
		var $user = $(el);
		var id = $user.data('id');
		var str = ($user.find('.js_search_plus').text() + '|' + $user.find('.js_user_name').text() + '|' + $user.find('.js_user_city').text()).toLowerCase();
		index.push([str, id]);
	});
	window.rebuild_menu_counts = function(results) {
		if (!results) return;
		var stat = results.stat;
		if (!stat) return;
		process('#submenu_participants_accepted_count', stat.accepted_count);
		process('#submenu_participants_premoderation_count', stat.premoderation_count);
		process('#submenu_mark_count', stat.mark_count);
		process('#submenu_unset_mark_count', stat.unset_mark_count);
		function process(selector, count) {
			$(selector).find('.js_number').text(count);
			$(selector).toggle(count > 0);
		}
	}
	window.init_just_added_select = function($new_select) {
		$new_select.xselect({on_complete: rebuild_menu_counts});
	}
	$('.js_status_select_init').xselect({on_complete: rebuild_menu_counts});
	$('.js_role').each(function() {
		var $role = $(this);
		var $search_btn = $role.find('.js_search_btn');
		var $search = $role.find('.js_search');
		var $search_text = $role.find('.js_search_text');
		var $search_results = $role.find('.js_search_results');
		var $search_results_too_many = $role.find('.js_search_results_too_many');
		var $search_results_nothing_found = $role.find('.js_search_results_nothing_found');
		var $search_close_btn = $role.find('.js_search_close_btn');
		var $existing_users = $role.find('.js_existing_users');
		var $existing_users_nothing_found = $role.find('.js_existing_users_nothing_found');
		var search_inited = false;
		var search_shown = false;
		var existing_users = []; // id -> $user
		var just_added_users = []; // id -> $user
		var internal_index = []; // id -> $user
		var last_search_text = "";
		function init_search() {
			search_inited = true;
			$existing_users.find('.js_user').each(function(idx, el) {
				$user = $(el);
				var id = $user.data('id');
				existing_users[id] = $user;
				internal_index[id] = $user;
			});
			$users.clone().hide().appendTo($search_results);
			$search_results.find('.js_user').each(function(idx, el) {
				$user = $(el);
				var id = $user.data('id');
				if (existing_users[id]) {
					$user.remove();
				}
				else {
					internal_index[id] = $user;
				}
			});
			$search_results.on('click', '.js_add_btn', function() {
				var $button = $(this);
				var $user = $button.parents('.js_user');
				var id = $user.data('id');
				var $new_select = $just_added_select.clone();
				$new_select.data('role_id', $role.data('id'));
				$new_select.data('user_id', $user.data('id'));
				$button.replaceWith($new_select.show());
				just_added_users.push(id);
				xpost({
					ajax_ctrl_name: 'stream_admin_activity_participant_status',
					data: {
						user_id: $user.data('id'),
						role_id: $role.data('id'),
						status: 'accepted'
					},
					success_callback: function(results) {
						rebuild_menu_counts(results);
					}
				});
				window.init_just_added_select($new_select);
			});
		}
		function search_show() {
			if (search_shown) return;
			search_shown = true;
			$search.show();
			$existing_users_nothing_found.hide();
			if (!search_inited) {
				init_search();
			}
			last_search_text = '';
			refresh_results();
		}
		function search_hide() {
			if (!search_shown) return;
			search_shown = false;
			move_just_added_to_existing_users();
			$search.hide();
			$existing_users.find('.js_user').show();
			$existing_users.show();
			$existing_users_nothing_found.toggle($existing_users.find('.js_user').length == 0);
		}
		function move_just_added_to_existing_users() {
			for (var i = 0; i < just_added_users.length; ++i) {
				var id = just_added_users[i];
				internal_index[id].prependTo($existing_users);
				existing_users[id] = internal_index[id];
			}
			just_added_users = [];
		}
		$search_btn.click(function() {
			search_shown ? search_hide() : search_show();
		});
		$search_close_btn.click(function() {
			search_hide();
		});
		function get_first_color_index() {
			var index = $existing_users.find('.js_user').first().data('color-index');
			return index ? index : 0;
		}
		function refresh_results() {
			var text = $search_text.hasClass('placeholder') ? '' : $.trim($search_text.val()).toLowerCase();
			if (text === last_search_text) return;
			last_search_text = text;
			move_just_added_to_existing_users();
			var results = [];
			for (var i = 0; i < index.length; ++i) {
				if (index[i][0].indexOf(text) != -1) {
					var id = index[i][1];
					results.push(id);
				}
			}
			if (results.length == 0) {
				$search_results.hide();
				$existing_users.hide();
				$search_results_too_many.hide();
				$search_results_nothing_found.show();
			}
			else if (results.length > 100) {
				$search_results.hide();
				$search_results_nothing_found.hide();
				$search_results_too_many.toggle(text !== '');
				$existing_users.hide();
				$existing_users.find('.js_user').hide();
				for (var i = 0; i < results.length; ++i) {
					var id = results[i];
					if (existing_users[id]) {
						internal_index[id].show();
					}
				}
				$existing_users.show();
			}
			else {
				$search_results.hide();
				$existing_users.hide();
				$search_results_too_many.hide();
				$search_results_nothing_found.hide();
				$search_results.find('.js_user').hide();
				$existing_users.find('.js_user').hide();
				var color_index = get_first_color_index() - results.length % 2;
				for (var i = 0; i < results.length; ++i) {
					var id = results[i];
					if (internal_index[id]) {
						internal_index[id].show();
					}
				}
				$search_results.toggle(text !== '');
				$existing_users.show();
			}
		}
		var refresh_results_debounced = imDebounce(function() {
			refresh_results();
		}, 200);
		$search_text.change(refresh_results_debounced);
		$search_text.keyup(refresh_results_debounced);
	});
});