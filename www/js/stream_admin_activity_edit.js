$(function() {
	function update_start_time_caption() {
		var is_deadline = $('#f-date_type').val() == 'deadline';
		$('.field[jq-name^="start_time"] label').text((is_deadline ? global.trans['Deadline'] : global.trans['Start time']) + ':');
	}
	$('#f-date_type').change(update_start_time_caption);
	update_start_time_caption();

	(function groups_issues() {
		function change() {
			$('#field-groups').show();
			$('#field-groups').find('._set_item').show();
			var study_level_id = $('#f-study_level_id').val();
			var city_id = $('#f-city_id').val();
			for (var i in groups_info) {
				var group = groups_info[i];
				var $group_input = $('#f-groups-' + group['id']);
				var $group_item = $group_input.closest('._set_item');
				var show = true;
				show = show && (!city_id || !group['city_id'] || city_id == group['city_id']);
				show = show && (!study_level_id || !group['study_level_id'] || study_level_id == group['study_level_id']);
				$group_item.toggle(show);
			}
			$('#field-groups').toggle(!!$('#field-groups').find('._set_item:visible').length);
		}
		change();
		$('#f-study_level_id').change(change);
		$('#f-city_id').change(change);
	})();
});