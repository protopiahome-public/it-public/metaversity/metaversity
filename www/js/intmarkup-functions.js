function imDebounce(func, timeout, wait) {
	var t = null;
	var finalize = false;
	return function() {
		var obj = this;
		var args = arguments;
		if (!t) {
			function call() {
				func.apply(obj, args);
			}
			function callback() {
				t = null;
				if (finalize) {
					call();
					!wait ? start() : null;
				}
			}
			function start() {
				finalize = wait;
				t = setTimeout(callback, timeout || 100);
			}
			!wait ? call() : null;
			start();
		}
		else {
			finalize = true;
		}
	}
}

function imHtmlize(str) {
	return str.toString().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
}

function cssConnectIsActive(name) {
	return $('.css_connect__' + name).width() >= 100;
}

function imCountCaseEn(count, t1, tN) {
	return count == 1 ? t1 : tN;
}

function imCountCaseRu(count, t1, t2, t5) {
	var last = count % 10;
	var last2 = Math.floor(count / 10) % 10;
	if (last == 0) return t5;
	else if (last2 == 1 || last >= 5 && last <= 9) return t5;
	else if (last >= 2 && last <= 4) return t2;
	else return t1;
}

function imIsHorizontalScrollingEnabled(el) {
	return el.scrollWidth > el.offsetWidth;
}

function imIsVerticalScrollingEnabled(el) {
	return el.scrollHeight > el.offsetHeight;
}

/* iOS-related workaround */
function imHorizontalScrollingActivate(e) {
	if (!imIsHorizontalScrollingEnabled(e.currentTarget)) {
		return;
	}
	if (e.currentTarget.scrollLeft === 0) {
		e.currentTarget.scrollLeft = 1;
	}
	else if (e.currentTarget.scrollWidth === e.currentTarget.scrollLeft + e.currentTarget.offsetWidth) {
		e.currentTarget.scrollLeft -= 1;
	}
}

/* iOS-related workaround */
function imVerticalScrollingActivate(e) {
	if (!imIsVerticalScrollingEnabled(e.currentTarget)) {
		return;
	}
	if (e.currentTarget.scrollTop === 0) {
		e.currentTarget.scrollTop = 1;
	}
	else if (e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight) {
		e.currentTarget.scrollTop -= 1;
	}
}

function imCookie(name, value) {
	if (typeof(value) === 'undefined') {
		return $.cookie ? $.cookie(name) : null;
	}
	else if ($.cookie) {
		var params = {};
		if (imGlobal && imGlobal.cookieDomain) {
			params.domain = imGlobal.cookieDomain;
		}
		if (imGlobal && imGlobal.cookiePath) {
			params.path = imGlobal.cookiePath;
		}
		$.cookie(name, value, params);
	}
}

// @todo store initial values in data-adjust-text, data-adjust-font-size, ...
// @todo allow manual re-apply
// @todo allow automatic re-apply after window.load
$.fn.imAdjustText = function() {
	var args = arguments;
	var userOptions = args[0];
	var defaultOptions = {
		strategy: [],
		reapplyIfZeroWidth: false
	};
	var options = $.extend({}, defaultOptions, userOptions);
	var strategyDefaultOptions = {
		'decreaseFontSize': {
			cssProp: 'font-size',
			minValue: 8
		},
		'cutText': {}
	};
	function ok($this, prop) {
		var result = true;
		if (prop.$probe && prop.rightPoint) {
			result = result && prop.$probe.offset().left <= prop.rightPoint;
		}
		if (prop.maxWidth) {
			result = result && $this.width() <= prop.maxWidth;
		}
		return result;
	}
	return this.each(function () {
		var $this = $(this);
		if (options.reapplyIfZeroWidth) {
			if ($this.width() === 0) {
				if (!$this.data('adjust-reapply-on-resize')) {
					$(window).bind('resize', imDebounce(function(e) {
						if (args[0] && typeof(args[0]) === 'object') {
							args[0]['event'] = e;
						}
						$this.imAdjustText.apply($this, args);
					}));
					$this.data('adjust-reapply-on-resize', true);
				}
				return;
			}
			else {
				$this.data('adjust-reapply-on-resize', false);
				if (options.event) {
					$(window).unbind(options.event);
				}
			}
		}
		var prop = {};
		if (options.fitTo) {
			prop.$parent = $this.closest(options.fitTo);
			if (!prop.$parent.length) return;
			prop.rightPoint = prop.$parent.offset().left + prop.$parent.outerWidth() - parseInt(prop.$parent.css('padding-right'));
			prop.$probe = $('<span></span>');
			prop.$probe.css('padding-left', '1px');
			$this.parent().append(prop.$probe);
		}
		else if (options.fitToWidth) {
			prop.maxWidth = parseInt(options.fitToWidth);
		}
		else {
			return;
		}
		var i, s, sOptions;
		for (i in options.strategy) {
			if (ok($this, prop)) break;
			s = options.strategy[i];
			// Select a strategy
			if (typeof(s) === 'string' && strategyDefaultOptions[s]) {
				sOptions = $.extend({strategy: s}, strategyDefaultOptions[s]);
			}
			else if (typeof(s) === 'object' && s.strategy && strategyDefaultOptions[s.strategy]) {
				sOptions = $.extend({strategy: s.strategy}, strategyDefaultOptions[s.strategy], s);
			}
			else {
				continue;
			}
			// Check options
			switch (sOptions.strategy) {
				case 'decreaseFontSize':
					if (!sOptions.minValue) {
						switch (sOptions.cssProp) {
							case 'font-size': sOptions.minValue = 8; break;
						}
					}
					break;
			}
			// Apply
			switch (sOptions.strategy) {
				case 'decreaseFontSize':
					(function() {
						var val = parseInt($this.css(sOptions.cssProp));
						if (!val) return;
						while (!ok($this, prop) && val > sOptions.minValue) {
							--val;
							$this.css(sOptions.cssProp, val);
						}
					})();
					break;
				case 'cutText':
					(function() {
						var $contents = $this.contents();
						var endIndex = /*prop.$probe ? 2 :*/ 1;
						if ($contents.length && $contents[$contents.length - endIndex]) {
							var node = $contents[$contents.length - endIndex];
							if (node.nodeValue) {
								node.nodeValue += '...';
								while (!ok($this, prop) && node.nodeValue.length >= 4) {
									node.nodeValue = node.nodeValue.substr(0, node.nodeValue.length - 4) + '...';
								}
							}
						}
					})();
					break;
			}
		}
		if (prop.$probe) {
			prop.$probe.remove();
		}
	});
}
