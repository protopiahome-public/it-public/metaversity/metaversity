(function($) {
	$.fn.xselect = function(options) {
		var args = arguments;
		var options_default = {
			on_select: function(value, $item, $select, is_user_action) {},
			on_complete: function(result, value, $item, $select) {}
		};
		if (args[0] === 'get') {
			return this.first().data('value');
		}
		return this.each(function () {
			var $select = $(this);
			if (args[0] === 'on_change') {
				$select.trigger('changeInternal.xselect');
				return;
			}
			else if (args[0] === 'set') {
				$select.trigger('setInternal.xselect', args[1]);
				return;
			}
			else if (args[0] === 'foreach') {
				$select.trigger('getViaCallbackInternal.xselect', args[1]);
				return;
			}
			var options = $.extend({}, options_default, args[0]);
			var $list = $select.find('._list');
			var $items = $list.children();
			var $item_selected = null;
			var value = null;
			var height = $select.height();
			var opened = false;
			function init() {
				$item_selected = $items.filter('._item__selected').first();
				if (!$item_selected.length) {
					$item_selected = $items.first();
				}
				$select.width($select.width() + 'px');
				$list.width($select.width() + 'px');
				$list.css('position', 'absolute');
				$list.removeClass('box_shadow');
				$select.addClass('box_shadow');
				$list.removeClass('box-shadow');
				$select.addClass('box-shadow');
				$items.css('visibility', 'hidden');
				$items.height(height + 'px');
				refresh_selection();
			};
			function refresh_selection() {
				var height_offset = -height;
				var found = false;
				$items.each(function(idx, el) {
					if (found) return;
					var $item = $(el);
					if ($item.is($item_selected)) {
						found = true;
					}
					height_offset += height;
				});
				$list.css('top', '-' + height_offset + 'px');
				$items.removeClass('_item__selected');
				$item_selected.addClass('_item__selected');
				$items.css('visibility', 'visible');
				value = $item_selected.data('value');
				$select.data('value', value);
			}
			function select_close() {
				if (!opened) return;
				opened = false;
				$select.css('overflow', 'hidden');
				$list.removeClass('box_shadow');
				$select.addClass('box_shadow');
				$list.removeClass('box-shadow');
				$select.addClass('box-shadow');
				$select.removeClass('select__opened');
				$(document).unbind('keydown.xselect');
				$('body').unbind('click.xselect');
			}
			function attach_body_event() {
				$('body').one('click.xselect', function(ev) {
					if (!$(ev.target).parents().is($select)) {
						select_close();
					} else {
						attach_body_event();
					}
				});
			}
			function select_open() {
				if (opened) return;
				opened = true;
				$select.addClass('select__opened');
				$select.removeClass('box_shadow');
				$list.addClass('box_shadow');
				$select.removeClass('box-shadow');
				$list.addClass('box-shadow');
				$select.css('overflow', 'visible');
				$(document).unbind('keydown.xselect').bind('keydown.xselect', function(e) {
					if (e.keyCode == 27) { // Escape
						select_close();
						return true;
					}
				});
				attach_body_event();
			}
			function on_change(old_value) {
				$select.trigger('change.xselect', old_value);
				if (!$select.data('change-ajax-ctrl')) {
					return;
				}
				var data = $select.data();
				var ajax_ctrl_name = $select.data('change-ajax-ctrl');
				var value_name = $select.data('value-name') ;
				value_name = value_name ? value_name : 'value';
				data[value_name] = value;
				delete data['changeAjaxCtrl'];
				delete data['valueName'];
				delete data['value'];
				xpost({
					ajax_ctrl_name: ajax_ctrl_name,
					data: data,
					success_callback: function(result) {
						options.on_complete(result, value, $item_selected, $select);
					}
				});
			}
			function select_item($item, is_user_action) {
				select_close();
				$item_selected = $item;
				var old_value = value;
				refresh_selection();
				if (typeof(options.on_select) == "function") {
					options.on_select(value, $item_selected, $select, !!is_user_action, old_value);
				}
				if (value !== old_value) {
					on_change(old_value);
				}
			}

			init();
			$items.click(function() {
				if (!opened) {
					select_open();
				} else {
					select_item($(this), true);
				}
			});
			$select.on('changeInternal.xselect', function() {
				on_change();
			});
			$select.on('setInternal.xselect', function(event, new_value) {
				var found = false;
				var $new_item = null;
				$items.each(function(idx, el) {
					if (found) return;
					var value_current = $(el).data('value');
					if (value_current == new_value) {
						$new_item = $(el);
						found = true;
					}
				});
				if ($new_item) {
					select_item($new_item);
				}
			});
			$select.on('getViaCallbackInternal.xselect', function(event, callback) {
				callback(value, $item_selected, $select);
			});
		});
	}
})(jQuery);