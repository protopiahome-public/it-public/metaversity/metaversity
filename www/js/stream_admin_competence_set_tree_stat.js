$(function() {
	$('#only-important').click(function() {
		$('.js_line').toggle();
		//var checked = $(this).is(':checked');
		//$('.line-only-important-1-').toggle(checked);
		//$('.line-only-important-0-').toggle(!checked);
	});
	function get_simple_list_html(stat, index, url) {
		var stat_splitted = stat.split(',');
		var html = '';
		html += '<ul>';
		var i, parts, key, mark;
		for (i in stat_splitted) {
			if (!stat_splitted[i]) continue;
			parts = stat_splitted[i].split(':');
			if (parts.length !== 2) continue;
			key = parts[0];
			mark = parts[1];
			if (!index[key]) continue;
			html += '<li>';
			if (url) {
				html += '<a target="_blank" href="' + htmlize(base_stream_url + url + key + '/') + '">';
			}
			html += htmlize(index[key]);
			if (url) {
				html += '</a>';
			}
			html += ' (' + mark + ')';
			html += '</li>';
		}
		html += '</ul>';
		return html;
	}
	function get_double_list_html(stat, index1, index1_plus, index2, url1, url1_plus) {
		var stat_splitted = stat.split('|');
		var html = '';
		html += '<ul>';
		var i, parts, key1, key1_plus, stat2;
		for (i in stat_splitted) {
			if (!stat_splitted[i]) continue;
			parts = stat_splitted[i].split('@');
			if (index1_plus) {
				if (parts.length !== 3) continue;
				key1 = parts[0];
				key1_plus = parts[1];
				stat2 = parts[2];
			}
			else {
				if (parts.length !== 2) continue;
				key1 = parts[0];
				key1_plus = null;
				stat2 = parts[1];
			}
			if (!index1[key1]) continue;
			html += '<li>';
			if (url1) {
				html += '<a target="_blank" href="' + htmlize(base_stream_url + url1 + key1 + '/') + '">';
			}
			html += htmlize(index1[key1]);
			if (url1) {
				html += '</a>';
			}
			if (key1_plus && index1_plus[key1_plus]) {
				html += ' (';
				if (url1_plus) {
					html += '<a target="_blank" href="' + htmlize(base_stream_url + url1_plus + key1_plus + '/') + '">';
				}
				html += htmlize(index1_plus[key1_plus]);
				if (url1_plus) {
					html += '</a>';
				}
				html += ')';
			}
			html += get_simple_list_html(stat2, index2);
			html += '</li>';
		}
		html += '</ul>';
		return html;
	}
	$('.js_expand_stat').click(function() {
		var competence_id = $(this).data('competence-id');
		var competence_title = $(this).data('competence-title');
		var type = $(this).data('type');
		var stat = $(this).data('stat');
		var title = '';
		var html = '';
		html += '<div class="js_lightbox_autoheight lightbox_autoheight">';
		html += '<p>' + htmlize(competence_title) + '</p>';
		switch (type) {
			case 'position':
				html += get_simple_list_html(stat, position_index, 'admin/positions/');
				title = global.trans['Positions'];
				break;
			case 'format':
				html += get_double_list_html(stat, format_index, null, role_index, 'admin/formats/');
				title = global.trans['Formats'];
				break;
			case 'activity':
				html += get_double_list_html(stat, activity_index, format_index, role_index, 'activities/', 'admin/formats/');
				title = global.trans['Schedule'];
				break;
			default:
				return false;
		}
		html += '</div>';
		xlightbox.show({
			title: title,
			content: html,
			button_title: title = global.trans['Close'],
			show_cancel_button: false,
			width: '600px',
			success: function(options) {
				return true; // 'true' closes the dialog
			}
		});
		return false;
	});

	function htmlize(str) {
		return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
	}
});