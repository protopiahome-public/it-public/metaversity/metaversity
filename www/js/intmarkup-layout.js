$(function() {
	var $w = $(window);
	var $body = $('body');
	$w.resize(imDebounce(function() {
		$w.trigger('layoutrefresh');
	}));
	$w.on('orientationchange', function() {
		$w.trigger('layoutrefresh');
	});

	if (window.FastClick) {
		FastClick.attach(document.body);
	}

	$('.js_btn_menu_left_open_main').click(function() {
		$(this).toggleClass('menu_item__opened');
		$('.js_menu_left_main').slideToggle(200);
		$('.js_menu_left_main').toggleClass('menu_item__opened');
	});

	$('.js_btn_menu_left_open_user').click(function() {
		$(this).toggleClass('menu_item__opened');
		$('.js_menu_left_user').slideToggle(200);
		$('.js_menu_left_user').toggleClass('menu_item__opened');
	});

	(function initMenuTop() {
		var $menu = $('.menu_top_dd');
		if (!$menu.length) return;
		$('body').addClass('with_menu_top');
		(function fixAnchors() {
			$w.on('markuprefresh', function() {
				$('a[name]').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function() {
					var $a = $(this);
					var $a2 = $('<a class="JS_IGNORE"></a>');
					$a2.attr('name', $a.attr('name'));
					$a2.insertBefore($a);
					$a2.wrap('<span class="anchor_fix"></span>');
					$a.replaceWith($a.html());
				});
			});
		})();
		var $btn = $('.js_btn_menu_top_user_dd');
		var shown = false;
		function show() {
			if (shown) return;
			shown = true;
			$menu.stop();
			var h = $menu.height();
			$btn.addClass('menu_item_user__opened');
			$menu.css({
				'top': (44 - h) + 'px'
			});
			$menu.animate({
				'top': '+=' + h + 'px'
			}, {
				duration: 200
			});
			$w.on('resize.imMenuTopUser', function() {
				hide(true);
			});
			$w.on('orientationchange.imMenuTopUser', function() {
				hide(true);
			});
			$(document).on('click.imMenuTopUser', function(e) {
				if (!$(e.target).closest($menu.add($btn)).length) {
					hide(true);
					return false;
				}
			});
			$(document).on('touchstart.imMenuTopUser', function(e) {
				if (!$(e.target).closest($menu.add($btn)).length) {
					hide(true);
					return false;
				}
			});
			$(document).on('keydown.imMenuTopUser', function(e) {
				if (27 == e.keyCode) { // Escape
					hide(true);
					return true;
				}
			});
		}
		function end() {
			$menu.css({
				'top': ''
			});
			$btn.removeClass('menu_item_user__opened');
		}
		function hide(fast) {
			if (!shown) return;
			shown = false;
			$menu.stop();
			if (fast) {
				end();
			}
			else {
				var h = $menu.height();
				$menu.animate({
					'top': '-=' + h + 'px'
				}, {
					duration: 200,
					complete: function() {
						end();
					}
				});
			}
			$w.unbind('orientationchange.imMenuTopUser');
			$w.unbind('resize.imMenuTopUser');
			$(document).unbind('click.imMenuTopUser');
			$(document).unbind('touchstart.imMenuTopUser');
			$(document).unbind('keydown.imMenuTopUser');
		}
		$btn.click(function() {
			shown ? hide() : show();
		});
	})();

	(function initMenuLeft() {
		var $menu = $('.menu_left');
		if (!$menu.length) return;
		var $main = $('.main');
		var $btn = $('.js_btn_menu_left_open');
		var shown = false;
		var touched = false;
		$('body').append('<div class="css_connect css_connect__menu_left_active"></div>');
		$('body').addClass('');
		if ($menu.hasClass('menu_left__reserve')) {
			$('body').addClass('with_menu_left__reserve');
		}
		$menu.on('touchstart', imVerticalScrollingActivate);
		$w.on('layoutrefresh', function() {
			// iPad fix - http://stackoverflow.com/questions/4444473/window-height-not-working-in-ipad-fullscreen-web-app
			var wh = window.innerHeight ? window.innerHeight : $w.height();
			$menu.css('height', wh - ($('.menu_top:visible').length ? 44 : 0));
		});
		function show() {
			if (cssConnectIsActive('menu_left_active')) return;
			if (shown) return;
			shown = true;
			$main.stop();
			$menu.stop();
			var w = $menu.width();
			$body.css({
				'overflow': 'hidden'
			});
			$main.css({
				'position': 'absolute',
				'width': $w.width() + 'px',
				'left': '0'
			});
			$menu.css({
				'left': '-' + w + 'px'
			});
			$main.animate({
				'left': '+=' + w + 'px'
			}, {
				duration: 200
			});
			$menu.animate({
				'left': '+=' + w + 'px'
			}, {
				duration: 200
			});
			setTimeout(function() {
				$w.bind('orientationchange.imMenuLeft', function() {
					hide(true);
				});
				$w.bind('resize.imMenuLeft', function() {
					if (!touched) {
						hide(true);
					}
				});
				$(document).bind('touchstart.imMenuLeft', function(e) {
					if (!$(e.target).closest($btn.add($menu)).length) {
						hide();
						return false;
					}
					touched = true;
				});
				$(document).bind('touchend.imMenuLeft', function(e) {
					touched = false;
					if ($body.get(0).scrollLeft > 0) {
						hide(); // iPhone case
					}
				});
				$(document).bind('click.imMenuLeft', function(e) {
					if (!$(e.target).closest($menu.add($btn)).length) {
						hide();
						return false;
					}
					return true;
				});
				$(document).bind('keydown.imMenuLeft', function(e) {
					if (27 == e.keyCode) { // Escape
						hide(true);
						return true;
					}
				});
			}, 0);
		}
		function end() {
			$body.css({
				'overflow': 'auto'
			});
			$main.css({
				'position': 'static',
				'width': 'auto',
				'left': ''
			});
			$menu.css({
				'left': ''
			});
			touched = false;
		}
		function hide(fast) {
			if (!shown) return;
			shown = false;
			$main.stop();
			$menu.stop();
			if (fast) {
				end();
			}
			else {
				var w = parseInt($main.css('left'));
				$main.animate({
					'left': '-=' + w + 'px'
				}, {
					duration: 200
				});
				$menu.animate({
					'left': '-=' + w + 'px'
				}, {
					duration: 200,
					complete: function() {
						end();
					}
				});
			}
			$w.unbind('orientationchange.imMenuLeft');
			$w.unbind('resize.imMenuLeft');
			$(document).unbind('keydown.imMenuLeft');
			$(document).unbind('click.imMenuLeft');
			$(document).unbind('touchstart.imMenuLeft');
			$(document).unbind('touchend.imMenuLeft');
		}
		$btn.click(function() {
			shown ? hide() : show();
		});
	})();

	(function initMenuLeftGroups() {
		$('.menu_left ._group .menu_item__clickable').click(function() {
			$(this).toggleClass('menu_item__opened');
			var $contents = $(this).closest('._group').find('._group_contents');
			$contents.slideToggle(200);
			$contents.toggleClass('menu_item__opened');
		});
	})();

	(function initForms() {
		$('.js_ctrl_search_input').each(function() {
			var $input = $(this);
			var initialValue = $input.val();
			var $ctrl = $input.closest('.js_ctrl_search');
			function change() {
				var is_active = $input.val() !== initialValue;
				$ctrl.toggleClass('ctrl__search__active', is_active);
			}
			$input.on('change', change);
			$input.on('keyup', change);
			$input.on('paste', change);
			change();
		});

		$('.js_ctrl_select_with_front').each(function() {
			var $select = $(this);
			$select.css('width', '100%');
			$select.css('cursor', 'pointer');
			var $front = $('<span class="ctrl ctrl__select_front box_shadow"><span>');
			var $icon = $('<i class="_dd_icon fa fa-caret-down"><i>');
			var $value = $('<span class="_value"><span>');
			$front.append($icon);
			$front.append($value);
			$front.insertAfter($select);
			$select.css('opacity', 0);
			$select.css('position', 'absolute');
			function resetVal() {
				var val = $select.val();
				$value.text($select.find('option[value="' + val + '"]').text());
			}
			$select.change(resetVal);
			resetVal();
		});

		$('.js_ctrl_file_with_front').each(function() {
			var $file = $(this);
			$file.css('width', '100%');
			$file.css('cursor', 'pointer');
			var $front = $('<span class="ctrl ctrl__file_front"></span>');
			var $button = $('<span class="_browse_btn">…<span>');
			var $input = $('<input type="text" class="ctrl ctrl__text" readonly="readonly"/>');
			$input.attr('placeholder', $file.attr('placeholder'));
			$front.append($button);
			$front.append($input);
			$front.insertAfter($file);
			$file.css('opacity', 0);
			$file.css('position', 'absolute');
			function resetVal() {
				$input.val($file.val().replace(/^.*[\/\\]/, ''));
			}
			$file.change(resetVal);
			setTimeout(resetVal, 0); // Incorrect font (?)
		});

		$('.head_dd').each(function() {
			var $dd = $(this);
			var $select = $dd.find('select');
			var $front = $('<span class="_front"></span>');
			var $content = $('<span class="_content"></span>');
			var $value = $('<span class="_value"></span>');
			var $icon = $('<i class="_icon fa fa-caret-down"></i>');
			var $prefix_html = $dd.find('._prefix_html').show();
			$content.append($prefix_html);
			$content.append($value);
			$front.append($content);
			$front.append($icon);
			$front.insertAfter($select);
			$select.css('opacity', 0);
			$select.css('position', 'absolute');
			$select.find('option').each(function() {
				var title = $(this).data('title-full');
				if (title) {
					$(this).text(title);
				}
			});
			$select.on('mouseover', function() {
				$front.addClass('box_shadow');
			});
			$select.on('mouseout', function() {
				$front.removeClass('box_shadow');
			});
			function resetVal() {
				var val = $select.val();
				var $option = $select.find('option[value="' + val + '"]');
				var title = $option.data('title-short');
				title = title ? title : $option.text();
				$value.text(title);
				$select.width($front.outerWidth() + 'px');
			}
			$select.change(resetVal);
			setTimeout(resetVal, 0); // Incorrect font (?)
		});
	})();

	$w.on('markuprefresh', function() {
		$('.table').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function() {
			$(this).on('click', '.cell_expand_text', function() {
				var $cell = $(this).closest('.cell_expand');
				if ($cell.hasClass('cell_expand__no_content')) {
					// Do nothing
				}
				else if ($cell.hasClass('cell_expand__content')) {
					$content = $cell.find('.cell_expand_content');
					$content.toggle();
					$cell.toggleClass('cell_expand__expanded');
				}
				else {
					var $tr = $cell.closest('tr');
					$next = $tr.next();
					$next.toggle();
					$cell.toggleClass('cell_expand__expanded');
					$tr.toggleClass('expanded');
				}
			});
		});

		$('.js_toggle_freeze').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function() {
			$(this).find("img, iframe").each(function () {
				$(this).data("src", this.src);
				this.src = "";
			});
		});

		$('.js_toggle').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function() {
			$(this).click(function() {
				var $this = $(this);
				if ($this.data('toggle-for')) {
					var $el = $('#' + $this.data('toggle-for'));
					if ($el.hasClass('js_toggle_freeze')) {
						$el.removeClass('js_toggle_freeze');
						$el.find("img, iframe").each(function () {
							var src = $(this).data("src");
							if (src) {
								this.src = src;
							}
						});
					}
					$el.toggle();
				}
			});
		});

		$('.widget').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function() {
			var $widget = $(this);
			var $title = $widget.find('.widget_head_title');
			var $content = $widget.find('.widget_content');
			$title.click(function() {
				if ($widget.find('.widget_head_expand_icon').is(':visible')) {
					$widget.toggleClass('widget__expanded');
				}
			});
		});

		$('.filter, .inline_filter').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function() {
			var $form = $(this);
			$form = $form.is('form') ? $form : $form.closest('form');
			function submit() {
				$form.submit();
				return false;
			}
			$(this).find('select').change(function() {
				var reset_on_change = ($(this).data('reset-on-change') || '').split(' ');
				var $ctrl;
				for (var i in reset_on_change) {
					$form.find('#filter-ctrl-' + reset_on_change[i]).val(reset_on_change[i]);
				}
				submit();
			});
			$(this).find('input:checkbox').click(submit);
		});

		$('.intmarkup iframe').not('.JS_PROCESSED, .JS_IGNORE').addClass('JS_PROCESSED').each(function(idx, el) {
			$iframe = $(el);
			var w = parseInt($iframe.attr('width'), 10);
			var h = parseInt($iframe.attr('height'), 10);
			if (!w || !h) {
				return;
			}
			var ratio = h / w;
			$iframe.wrap('<div class="intmarkup_iframe_wrap" style="padding-top: ' + Math.round(ratio * 100) + '%; max-height: ' + Math.round(640 * ratio) + 'px"></div>');
		});

		$('.js_submenu_clickable').each(function() {
			var $submenu = $(this);
			var $items = $submenu.find('._item');
			$submenu.on('click', '._item__clickable', function() {
				$items.removeClass('_item__current');
				$items.addClass('_item__clickable');
				$(this).removeClass('_item__clickable');
				$(this).addClass('_item__current');
				$items.each(function() {
					var sel = $(this).data('show');
					$(sel).toggle($(this).hasClass('_item__current'));
				});
			});
		});
	});

	$w.trigger('markuprefresh');
	$w.trigger('layoutrefresh');
});
