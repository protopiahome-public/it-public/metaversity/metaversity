$(function() {
	// Checkboxes
	var $cb = $('.js_position_checkbox');
	$cb.change(function() {
		var checked = $(this).is(":checked");
		var position_id = $(this).data('position-id');
		data = {
			position_id: position_id,
			checked: checked ? 1 : 0
		}
		xpost({
			ajax_ctrl_name: 'stream_position',
			data: data
		});
		var count = $cb.filter(':checked').length;
		$('.js_position_count').text(count);
		var ok = count >= 1 && count <= 2;
		$('.js_position_count_text').removeClass(ok ? 'red' : 'green');
		$('.js_position_count_text').addClass(!ok ? 'red' : 'green');
	});
	if (typeof(Switchery) !== 'undefined') {
		$cb.each(function(idx, el) {
			var init = new Switchery(el);
		});
	}

	// Study level filter
	$('.js_study_level_select').change(function() {
		var study_level_id = $(this).val();
		var any_shown = false;
		$('.js_other_position').each(function() {
			var re = new RegExp(',' + study_level_id + ',');
			var levels = $(this).data('study-levels');
			var show = !study_level_id || levels.match(/^,*$/) || levels.match(re);
			$(this).toggleClass('hide2', !show);
			$(this).next().toggleClass('hide2', !show);
			any_shown = any_shown || show;
		});
		$('.js_other_positions').toggle(!!any_shown);
		$('.js_other_positions_not_found').toggle(!any_shown);
	});

	// Request position details
	var pending_position_id = null;
	$('.js_position_clickable').click(function() {
		var position_id = $(this).closest('.js_position').data('id');
		pending_position_id = position_id;
		dynurls.push_hash('position=' + position_id);
		xpost({
			ajax_ctrl_name: 'stream_position_get',
			data: {position_id: position_id},
			custom_loading_msg: global.trans['Loading...'],
			success_callback: function(result, options) {
				if (result.position_id == pending_position_id && result.html) {
					xlightbox.show({
						content: result.html,
						button_title: title = global.trans['Close'],
						show_cancel_button: false,
						width: '600px',
						success: function(options) {
							return true; // 'true' closes the dialog
						},
						close: function() {
							dynurls.push_hash('');
						}
					});
					pending_position_id = null;
				}
			}
		});
	});

	// Dynurls
	var dynurls = new dynurls_class({}, function(state) {
		var params = dynurls.parse_hash(state.hash);
		var position_id = params.position;
		if (position_id && $('#js_position_clickable_' + position_id).length) {
			$('#js_position_clickable_' + position_id).click();
		}
		else {
			xlightbox.hide();
		}
	}, true);
});